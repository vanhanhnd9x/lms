<?php

/**
 * Connect lms to db portal
 */
class Portal {

    var $host = "localhost";
    var $username = "root";    // specify the sever details for mysql
    Var $password = "";
    var $database = "portal";
    var $myconn;

    function connectToDatabase() { // create a function for connect database
        $conn = mysql_connect($this->host, $this->username, $this->password);

        if (!$conn) {// testing the connection
            die("Cannot connect to the database");
        } else {

            $this->myconn = $conn;

            //echo "Connection established";
        }

        return $this->myconn;
    }

    function selectDatabase() { // selecting the database.
        mysql_select_db($this->database);  //use php inbuild functions for select database

        if (mysql_error()) { // if error occured display the error message
            echo "Cannot find the database " . $this->database;
        }
        //echo "Database selected..";
    }

    function getUser($username) {
        // Query
        $query = sprintf("SELECT * FROM users WHERE name='%s'", mysql_real_escape_string($username));

        $result = mysql_query($query);
        if (!$result) {
            echo 'Could not run query: ' . mysql_error();
            exit;
        }
        $row=mysql_fetch_object($result);
        return $row;
    }

    function closeConnection() { // close the connection
        mysql_close($this->myconn);

        //echo "Connection closed";
    }

}

?>