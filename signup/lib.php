<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define("TEACHER_ROLE", 3);

function update_user_logo($user_id,$file_name){
    global $DB;
    $record = new stdClass();
	$record->id = $user_id;
	$record->picture = 1;
	$record->url = $file_name;

	return $DB->update_record('user', $record, false);
}

function randPass($length, $strength = 8) {
	$vowels = 'aeuy';
	$consonants = 'bdghjmnpqrstvz';
	if ($strength >= 1) {
		$consonants .= 'BDGHJLMNPQRSTVWXZ';
	}
	if ($strength >= 2) {
		$vowels .= "AEUY";
	}
	if ($strength >= 4) {
		$consonants .= '23456789';
	}
	if ($strength >= 8) {
		$consonants .= '@#$%';
	}

	$password = '';
	$alt = time() % 2;
	for ($i = 0; $i < $length; $i++) {
		if ($alt == 1) {
			$password .= $consonants[(rand() % strlen($consonants))];
			$alt = 0;
		} else {
			$password .= $vowels[(rand() % strlen($vowels))];
			$alt = 1;
		}
	}
	return $password;
}
include ('../lib/phpmailer/class.phpmailer.php');
function sendEmail($to, $from, $from_name, $subject, $body) {
	
        global $CFG;
        include($CFG->dirroot . '/teams/config.php');
	
        global $error;

	$mail = new PHPMailer();	// create a new object
        
        $mail->IsSMTP(); // enable SMTP
	//$mail->IsHTML=true;
	$mail->IsHTML(true);
	$mail->SMTPDebug = 1;	// debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true;	// authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465;
	$mail->Username = 'tung@thienhoang.com.vn';//$mailUsername;
	$mail->Password = 'T12345678';//$pass;
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->AddAddress($to);
	$mail->CharSet = "utf-8";
        
	if (!$mail->Send()) {
		$error = 'Mail error: ' . $mail->ErrorInfo;
		return false;
	} else {
		$error = 'Message sent!';
		return true;
	}
}
function updateSalesmanPassword($salesmanid) {
	global $DB;
	$passwordNoEncode = randPass(8);
	$password = hash_internal_user_password($passwordNoEncode);
	if ($DB->set_field('user', 'password', $password, array('id' => $salesmanid))) {
		return $passwordNoEncode;
	}
}
function signup($first_name,$last_name,$username,$email,$company,$country){
    global $DB,$CFG;
    	$user = new stdClass();
	
	$user->auth = 'manual';
	$user->confirmed = 1;
	$user->deleted = 0;
	$user->timecreated = time();
	$user->username = $username;
	$user->mnethostid = 1;
	$user->firstname = $first_name;
	$user->lastname = $last_name;
	$user->country = $country;
        $user->company = $company;
	$user->email = $email;
        $user->parent = 0;        
	$lastinsertid = $DB->insert_record('user', $user);                
        $context = get_context_instance(CONTEXT_SYSTEM);
        role_assign(TEACHER_ROLE, $lastinsertid, $context->id);
	return $lastinsertid;
}
function checkUserNameExist($name) {

    global $DB;
    $salesman = $DB->get_record('user', array(
        'username' => $name
            ));

    return $salesman->id;
}
function checkEmailExist($email) {
    global $DB;
    $salesman = $DB->get_record('user', array(
        'email' => $email
            ));
    return $salesman->id;
}
?>
