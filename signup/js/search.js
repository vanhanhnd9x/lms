$(document).ready(function(){
    $("#username").keyup(function() {
        var userName = $(this).val();
        searchWhenKeyUpUserName(userName);
    });
    $("#username").focusout(function() {
        var userName = $(this).val();
        searchWhenKeyUpUserName(userName);
    });
    $("#txtEmail").keyup(function() {
        var email = $(this).val();
        searchWhenKeyUpEmail(email);
    });
    $("#txtEmail").focusout(function() {
        var email = $(this).val();
        searchWhenKeyUpEmail(email);
    });
		
    $('#agreeterms').click(function() {
        if(this.checked) {
           
            document.getElementById("agreeterms").value=1;
        }
        else { 
            document.getElementById("agreeterms").value=0;
        }
    });
		
});

function validate(){
    var error=0;
    
    if(document.getElementById('txtFirstName')&&document.getElementById('txtFirstName').value==''){
        $("#erFirst").show();
        error=1;
    }else{
        $("#erFirst").hide();
    }
    if(document.getElementById('txtLastName')&&document.getElementById('txtLastName').value==''){
        $("#erLast").show();
        error=1;
    }else{
        $("#erLast").hide();
    }
    
    if(document.getElementById('txtOrganisationName')&&document.getElementById('txtOrganisationName').value==''){
        $("#erOrg").show();
        error=1;
    }else{
        $("#erOrg").hide();
    }
		
    
    
    if(document.getElementById('agreeterms')&&(document.getElementById('agreeterms').value == 0||document.getElementById('agreeterms').value == '0')) {
        $("#erTerms").show();
        error=1;
      
    }else{
        $("#erTerms").hide();
    }
    if(document.getElementById("dupUsername")&&(document.getElementById("dupUsername").value==1||document.getElementById("dupUsername").value=='1')){
        document.getElementById("erUsername").innerHTML='Username is already existed';
        $("#erUsername").show();
        error=1;	
    }
    else if(document.getElementById('username')&&document.getElementById('username').value==''){
        $("#erUsername").show();
        error=1;
    }
    else{
        $("#erUsername").hide();
    }
    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    
    if(document.getElementById("dupEmail")&&(document.getElementById("dupEmail").value==1||document.getElementById("dupEmail").value=='1')){
        error=1;
        document.getElementById("erEmail").innerHTML='Email is already existed';
        $("#erEmail").show();
    }
    else if(document.getElementById('txtEmail')&&reg.test(document.getElementById('txtEmail').value) == false) {
        $("#erEmail").show();
        error=1;
      
    }
    else{
        $("#erEmail").hide();
    
    }
   
    if(error==1){
        return false;
    }
    return true;
    
}
function searchWhenKeyUpUserName(username)
{
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            if(xmlhttp.responseText=='1'||xmlhttp.responseText==1){
                document.getElementById("erUsername").innerHTML='Username is already existed';
                $("#erUsername").show();
                document.getElementById("dupUsername").value=1;
								
            }
            else{
                $("#erUsername").hide();
                document.getElementById("dupUsername").value=0;
            }
						
            

        }
    }
    
    xmlhttp.open("GET","ajaxcheckusername.php?q="+username,true);
    xmlhttp.send();
}
function searchWhenKeyUpEmail(email){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            
            if(xmlhttp.responseText=='1'||xmlhttp.responseText==1){

                document.getElementById("erEmail").innerHTML='Email is already existed';
                $("#erEmail").show();
                document.getElementById("dupEmail").value=1;
                
            }
            else{
                $("#erEmail").hide();
                document.getElementById("dupEmail").value=0;
            }
            

        }
    }
    
    xmlhttp.open("GET","ajaxcheckemail.php?q="+email,true);
    xmlhttp.send();
}