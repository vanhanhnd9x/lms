<?php 
require('../config.php');
require('lib.php');

global $CFG;
require($CFG->dirroot.'/teams/config.php');

$action = optional_param('action', '', PARAM_TEXT);

if($action=='signup'){
$first_name = optional_param('txtFirstName', '', PARAM_TEXT);    
$last_name = optional_param('txtLastName', '', PARAM_TEXT);    
$username = optional_param('username', '', PARAM_TEXT);  
$email = optional_param('txtEmail', '', PARAM_TEXT);  
$company = optional_param('txtOrganisationName', '', PARAM_TEXT);  
$country = optional_param('ddlCountry', '', PARAM_TEXT); 

$lastinsertid=signup($first_name,$last_name,$username,$email,$company,$country);

$mailto = $email;
            $username = $username;
            $newPass = updateSalesmanPassword($lastinsertid);
            $invitationContenNoEmail = $emailContentNo;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{PASSWORD}", $newPass, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
            $invitationContenNoEmail = $emailContentNo;

						if($_FILES["file"]["name"]!=''){
    if ($_FILES["file"]["error"] > 0)
    {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
  else
    {
      $file_extension=end(explode('.',$_FILES["file"]["name"]));
      
      
      if (strtolower($file_extension) == "jpg"||strtolower($file_extension) == "jpeg"
              ||strtolower($file_extension) == "gif"
              ||strtolower($file_extension) == "png") {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $CFG->dirroot."/account/logo/" . $lastinsertid."_".$_FILES["file"]["name"]);
      update_user_logo($lastinsertid,$CFG->wwwroot."/account/logo/" . $lastinsertid."_".$_FILES["file"]["name"]);
      }
      else{
          
          //displayJserror("File type for logo image must be : .jpg, .jpeg, .gif, .png", '');
      }
    }
}

echo displayJsAlert("", $CFG->wwwroot."/login/index.php");						
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1">
    <title>
        Online Training System, Free Trial Sign Up : Alphatechnologies
    </title>
    <meta content="Alphatechnologies, online training system, LMS, elearning, price plans, free trial, sign-up" name="keywords" />
    <meta name="description" content="Sign-up and trial Alphatechnologies online training system" />
    <meta charset="UTF-8" />
    <!-- Latest compiled and minified CSS & JS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot ?>/theme/nimble/style/styles.css" />
</head>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
</head>

<body>
    <form name="Form1" method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/signup/index.php?action=signup" id="Form1" enctype="multipart/form-data">
        <div id="content">
            <div class="container_16">
                <div class="grid_16">
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <div class="container_16">
                <div class="grid_10">
                    <div class="mod">
                        <div class="signup-contain">
                            <div class="panel-register">
                                <h3 style="color: #fff"><?php print_r(get_string('createyour')) ?></h3>
                            </div>
                            
                            <div id="erFirst" class="error" style="display: none;">
                                <?php print_string('pleasefirst') ?>
                            </div>
                            <input name="txtFirstName" type="text" maxlength="100" id="txtFirstName" class="form-control" placeholder="Nhập vào Tên" />
                            
                            <div id="erLast" class="error" style="display: none;">
                                <?php print_string('pleaselast') ?>
                            </div>
                            <input name="txtLastName" type="text" maxlength="100" id="txtLastName" class="form-control" placeholder="Nhập vào Họ" />
                            

                            <div id="erUsername" class="error" style="display: none;">
                                <?php print_string('pleaseuser') ?>
                            </div>
                            <input name="username" type="text" maxlength="100" id="username" class="form-control" placeholder="Username" />
                            <input type="hidden" name="dupUsername" id="dupUsername" value="0" />
                            
                            <div id="erEmail" class="error" style="display: none;">
                                <?php print_string('pleaseemail') ?>
                            </div>
                            <input name="txtEmail" type="text" maxlength="255" id="txtEmail" class="form-control" placeholder="Email" />
                            <input type="hidden" name="dupEmail" id="dupEmail" value="0" />
                            <div class="tip" style="text-align: center;">
                                <?php print_string('wewillsent') ?>
                            </div>
                            
                            <div id="erOrg" class="error" style="display: none;">
                                <?php print_string('pleasecompany') ?>
                            </div>
                            <input name="txtOrganisationName" type="text" maxlength="255" id="txtOrganisationName" class="form-control" placeholder="Công ty" />
                            
                            <div id="erCountry" class="error" style="display: none;">
                                <?php print_r(get_string('pleaseselect')) ?>
                            </div>
                            <select name="ddlCountry" id="ddlCountry" class="form-control">
                                <option selected="selected" value="">
                                    --- Chọn Quốc Gia ---
                                </option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua and Barbuda</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivia</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="BR">Brazil</option>
                                <option value="BN">Brunei Darussalam</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CA">Canada</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Central African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos Islands</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CD">Congo</option>
                                <option value="CG">Congo</option>
                                <option value="CK">Cook Islands</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Cote D&#39;Ivoire</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DK">Denmark</option>
                                <option value="DJ">Djibouti</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="KR">Korea, North</option>
                                <option value="KP">Korea, South</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Lao</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libyan Arab Jamahiriya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macao</option>
                                <option value="MK">Macedonia</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia</option>
                                <option value="MD">Moldova</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanmar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="NL">Netherlands</option>
                                <option value="AN">Netherlands Antilles</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PS">Palestinian</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PN">Pitcairn</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russian Federation</option>
                                <option value="RW">Rwanda</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">Sao Tome and Principe</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SN">Senegal</option>
                                <option value="CS">Serbia and Montenegro</option>
                                <option value="SC">Seychelles</option>
                                <option value="SL">Sierra Leone</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SO">Somalia</option>
                                <option value="ZA">South Africa</option>
                                <option value="GS">South Georgia</option>
                                <option value="ES">Spain</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="SH">St. Helena</option>
                                <option value="KN">St. Kitts and Nevis</option>
                                <option value="LC">St. Lucia</option>
                                <option value="PM">St. Pierre and Miquelon</option>
                                <option value="VC">St. Vincent</option>
                                <option value="SD">Sudan</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbard and Jan Mayen</option>
                                <option value="SZ">Swaziland</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="SY">Syrian Arab Republic</option>
                                <option value="TW">Taiwan</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TZ">Tanzania</option>
                                <option value="TH">Thailand</option>
                                <option value="TL">Timor-Leste</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="TV">Tuvalu</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="UK">United Kingdom</option>
                                <option value="US">United States</option>
                                <option value="UY">Uruguay</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VE">Venezuela</option>
                                <option value="VN">Viet Nam</option>
                                <option value="VG">Virgin Islands, British</option>
                                <option value="VI">Virgin Islands, U.s.</option>
                                <option value="WF">Wallis and Futuna</option>
                                <option value="EH">Western Sahara</option>
                                <option value="YE">Yemen</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                            </select>
                            
                            <input name="txtPhone" type="text" maxlength="50" id="txtPhone" class="form-control" placeholder="Số điện thoại" />
                            &nbsp;
                            &nbsp;


                           	<td colspan="2"><h3 class="up_register"><?php print_string('uploadyour') ?></h3>
                           	<div style="padding: 15px; line-height: 2.5em">
	                            <label>
	                                <?php print_string('selectlogo') ?>
	                            </label>
	                            <input type="file" name="file" id="file"/>
	                            
	                            <div class="tip">
	                                <?php print_string('note') ?>
	                            </div>
	                            &nbsp;
	                            <div id="erTerms" class="error" style="display: none;">
	                                <?php print_string('youmust') ?>
	                            </div>
	                            <input id="agreeterms" type="checkbox" checked="checked" value="1" />
	                            <?php print_string('iagree') ?> <a target="_blank" href="" class="signup-link">
		                                       <?php print_string('termsof') ?></a>
		                                       <br>
	                            <button onclick="return validate();" class="signup-continue"></button>
	                            <input type="submit" name="btnSignUp" value="" id="btnSignUp" style="display: none;" />
                           	</div>
                            
                        </div>
                    </div>
                </div>
                <div class="grid_6">
                    <h3><?php print_string('included') ?></h3>
                    <p>
                        <?php print_string('tryafull') ?>
                    </p>
                    <div>
                        <?php print_string('included2') ?>
                    </div>
                    <div style="clear: both;">
                    </div>
                </div>
            </div>
            <div style="clear: both;">
            </div>
        </div>
        <div style="clear: both;">
        </div>
    </form>
    <!-- LeadFormix -->
    <a href="http://www.leadformix.com" title="Marketing Automation" onclick="window.open(this.href);return(false);">

<noscript><p>Market analytics <img src="http://vlog.leadformix.com/bf/bf.php" style="border:0" alt="bf"/></p>
</noscript>
</a>
    <!-- /LeadFormix -->
</body>

</html>