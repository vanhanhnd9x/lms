<?php $this->cache['vi']['block_course_list'] = array (
  'adminview' => 'Xem với tư cách quản trị',
  'allcourses' => 'Quản trị viên thấy được tất cả các khoá học',
  'configadminview' => 'Cấu hình hiển thị khối danh sách khoá học đối với quản trị viên',
  'confighideallcourseslink' => 'Ẩn liên kết "Tất cả các khoá học" bên dưới khối. Thiết lập này không có hiệu lực đối với quản trị viên',
  'hideallcourseslink' => 'Ẩn liên kết "Tất cả các khoá học"',
  'owncourses' => 'Quản trị viên chỉ thấy được các khoá học của mình',
  'pluginname' => 'Danh sách khoá học',
);