<?php $this->cache['vi']['repository_user'] = array (
  'configplugin' => 'Configuration for user private files repository',
  'pluginname_help' => 'Files in user private area',
  'pluginname' => 'Các tệp tin cá nhân',
  'emptyfilelist' => 'There are no files to show',
  'user:view' => 'View user private files',
);