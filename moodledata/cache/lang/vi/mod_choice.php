<?php $this->cache['vi']['mod_choice'] = array (
  'addmorechoices' => 'Thêm phương án lựa chọn',
  'allowupdate' => 'Cho phép cập nhật câu hỏi thăm dò',
  'answered' => 'Đã trả lời',
  'completionsubmit' => 'Show as complete when user makes a choice',
  'displayhorizontal' => 'Hiển thị theo chiều ngang',
  'displaymode' => 'Kiểu hiển thị',
  'displayvertical' => 'Hiển thị theo chiều dọc',
  'expired' => 'Xin lỗi, hoạt động này đã kết thúc vào ngày {$a}.',
  'fillinatleastoneoption' => 'You need to provide at least two possible answers.',
  'atleastoneoption' => 'You need to provide at least one possible answer.',
  'full' => '(đã đủ)',
  'havetologin' => 'Bạn phải đăng nhập trước khi có thể trả lời câu hỏi này',
  'choice' => 'Phương án lựa chọn',
  'choiceclose' => 'Ngày kết thúc',
  'choice:deleteresponses' => 'Xoá câu trả lời',
  'choice:downloadresponses' => 'Tải câu trả lời xuống',
  'choicefull' => 'Câu hỏi thăm dò này đã có đủ câu trả lời. Bạn không thể trả lời thêm được nữa.',
  'choice:choose' => 'Trả lời câu hỏi thăm dò',
  'choicename' => 'Tên câu hỏi',
  'choiceopen' => 'Ngày bắt đầu',
  'choiceoptions' => 'Choice options',
  'choiceoptions_help' => '<p>Đây là nơi bạn xác định những tùy chọn mà người tham gia cần chọn.</p>

<p>Bạn có thể điền vào bằng bất cứ số nào: Để các ô trống cũng không sao.</p>',
  'limitanswers_help' => '<p>Tùy chọn này cho phép bạn giới hạn số lần người tham gia có thể chọn đối với một khả năng lựa chọn cụ thể.</p>

<p>Một khi Tuỳ chọn giới hạn được kích hoạt, thì mỗi tùy chọn có thể đặt một giới hạn.
   Khi đã đến giới hạn thì không người nào có thể chọn được tùy chọn này nữa.
  Giới hạn không (0) có nghĩa là không ai được chọn lựa mục đó.</p>

<p>Nếu các giới hạn được tắt thì một số lượng người tham gia bất kỳ có thể chọn các khả năng bất kỳ.</p>',
  'choice:readresponses' => 'Đọc câu trả lời',
  'choicesaved' => 'Your choice has been saved',
  'choicetext' => 'Nội dung câu hỏi',
  'chooseaction' => 'Choose an action ...',
  'limit' => 'Giới hạn',
  'limitanswers' => 'Giới hạn số câu trả lời',
  'modulename' => 'Câu hỏi thăm dò',
  'modulename_help' => 'Các mô-đun hoạt động lựa chọn cho phép một giáo viên hỏi một câu hỏi duy nhất và cung cấp một lựa chọn câu trả lời duy nhất.

Kết quả lựa chọn có thể được công bố sau khi học sinh trả lời, sau một ngày nào đó, hoặc không gì cả. Kết quả có thể được xuất bản với tên sinh viên hoặc nặc danh.

Một hoạt động lựa chọn có thể được sử dụng

* Như một cuộc thăm dò nhanh để kích thích suy nghĩ về một chủ đề
* Để nhanh chóng kiểm tra sự hiểu biết của học sinh
* Để tạo điều kiện cho sinh viên ra quyết định, ví dụ như cho phép sinh viên bầu chọn một hướng đi cho các khóa học',
  'modulenameplural' => 'Câu hỏi thăm dò',
  'mustchooseone' => 'Chưa lưu được. Bạn phải chọn một câu trả lời trước khi lưu.',
  'noguestchoose' => 'Xin lỗi, khách vãng lai không thể trả lời câu hỏi thăm dò.',
  'noresultsviewable' => 'Hiện không thể xem được kết quả.',
  'notanswered' => 'Chưa có câu trả lời nào',
  'notenrolledchoose' => 'Sorry, only enrolled users are allowed to make choices.',
  'notopenyet' => 'Xin lỗi, hoạt động này chỉ được mở vào ngày {$a}',
  'option' => 'Option',
  'page-mod-choice-x' => 'Any choice module page',
  'pluginadministration' => 'Choice administration',
  'pluginname' => 'Câu hỏi thăm dò',
  'privacy' => 'Tính riêng tư của kết quả',
  'publish' => 'Publish results',
  'publishafteranswer' => 'Cho học viên xem kết quả sau khi họ trả lời',
  'publishafterclose' => 'Chỉ cho học viên xem kết quả sau khi kết thúc câu hỏi',
  'publishalways' => 'Luôn cho học viên xem kết quả',
  'publishanonymous' => 'Công bố kết quả khuyết danh, không hiển thị tên học viên',
  'publishnames' => 'Công bố kết quả đầy đủ, hiển thị tên và câu trả lời của học viên',
  'publishnot' => 'Không công bố kết quả cho học viên',
  'removemychoice' => 'Xoá các câu trả lời của tôi',
  'removeresponses' => 'Xoá hết các câu trả lời',
  'responses' => 'Câu trả lời',
  'responsesto' => 'Câu trả lời cho {$a}',
  'savemychoice' => 'Lưu câu trả lời của tôi',
  'showunanswered' => 'Hiển thị cột "không trả lời"',
  'spaceleft' => 'khoảng trống còn lại',
  'spacesleft' => 'khoảng trống còn lại',
  'taken' => 'Đã thực hiện',
  'timerestrict' => 'Giới hạn thời gian cho câu hỏi',
  'viewallresponses' => 'View {$a} responses',
  'withselected' => 'Với các lựa chọn',
  'yourselection' => 'Lựa chọn của bạn',
  'skipresultgraph' => 'Skip result graph',
  'moveselectedusersto' => 'Move selected users to...',
  'numberofuser' => 'The number of user',
);