<?php $this->cache['vi']['qtype_description'] = array (
  'addingdescription' => 'Thêm mô tả',
  'description' => 'Mô tả',
  'description_help' => 'Một mô tả không thực sự là một loại câu hỏi. Nó chỉ đơn giản cho phép văn bản được hiển thị mà không đòi hỏi bất kỳ câu trả lời, tương tự như một mô tả trên trang khóa học.

Các văn bản câu hỏi được hiển thị trong suốt quá trình làm bài và trên trang xem lại. Bất kỳ thông tin phản hồi chung chỉ được hiển thị trên trang.',
  'descriptionsummary' => 'Đây không thực sự là một câu hỏi. Thay vào đó là một cách để thêm một số hướng dẫn, phiếu đánh giá hoặc nội dung khác để thực hành. Điều này cũng tương tự như cách mà nó có thể được sử dụng để thêm nội dung vào trang khóa học.',
  'editingdescription' => 'Chỉnh sửa mô tả',
  'informationtext' => 'Văn bản thông thin',
);