<?php $this->cache['vi']['auth_pam'] = array (
  'auth_pamdescription' => 'Phương pháp này sử dụng PAM để truy cập tên đăng nhập tự nhiên trên máy chủ này. Bạn phải cài đặt <a href="http://www.math.ohio-state.edu/~ccunning/pam_auth/" target="_blank">PHP4 PAM Authentication</a> theo thứ tự sử dụng môdun này.',
  'auth_passwordisexpired' => 'Mật khẩu của bạn bị vô hiệu hoá. Bạn có muốn thay đổi mật khẩu của bạn bây giờ ?',
  'auth_passwordwillexpire' => 'Mật khẩu của bạn sẽ bị vô hiệu hóa trong {$a} ngày. Bạn có muốn thay đổi mật khẩu của bạn bây giờ không ?',
  'pluginname' => 'PAM (Pluggable Authentication Modules)',
);