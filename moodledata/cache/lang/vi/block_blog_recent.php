<?php $this->cache['vi']['block_blog_recent'] = array (
  'norecentblogentries' => 'No recent entries',
  'numentriestodisplay' => 'Number of recent entries to display',
  'pluginname' => 'Recent blog entries',
  'recentinterval' => 'Interval of time considered "recent"',
);