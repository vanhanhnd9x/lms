<?php $this->cache['vi']['mod_noticeboard'] = array (
  'ajouter' => 'Thêm một thông báo mới',
  'addnoticeboard' => 'Thêm một thông báo mới',
  'changestyle' => 'Sửa trang',
  'clicktoopen' => 'Nhấn để mở',
  'content' => 'Nội dung thông báo',
  'content_help' => 'Bạn có thể nhúng một tập tin pdf bằng cách làm theo các hướng dẫn sau: 
                            <br><ul>
                             <li>Gõ tên mà bạn muốn sử dụng cho các liên kết.</li>
                             <li>Chọn văn bản và nhấn vào nút liên kết inser/edit.</li>
                             <li>Nhấp vào nút duyệt ở cuối các trường liên kết url</li>
                             <li>Tải lên tập tin pdf của bạn và sau đó nhấp chèn </li>
                             </ul><br>
                             KHÔNG THÊM BẤT CỨ ĐIỀU GÌ KHÁC TRONG EDITOR.<br>
                              Lưu và hiển thị. Tập tin pdf sẽ được nhúng.',
  'css' => 'Stylesheet',
  'displaymenu' => 'Hiển thị menu thông báo',
  'displaymenuagree' => 'Kiểm tra nếu bạn muốn hiển thị menu',
  'externalurl' => 'Nhúng một trang Web',
  'format' => 'định dạng bảng thông báo',
  'menucss' => 'Sửa đổi kiểu menu',
  'menuname' => 'Tên menu',
  'modulename' => 'Hiển thị thông báo',
  'modulenameplural' => 'Hiển thị bảng thông báo',
  'morenoticeboards' => 'Sử dụng nhiều bảng thông báo',
  'name' => 'Tên',
  'noformating' => 'Không có định dạng',
  'order' => 'Thứ tự bạn muốn bảng thông báo',
  'pluginname' => 'Hiển thị thông báo',
  'pluginadministration' => 'Quản lý bảng thông báo',
  'noticeboard' => 'Thông báo',
  'noticeboardadministration' => 'Quản lý bảng thông báo',
  'noticeboardcontent' => 'Nội dung thông báo',
  'noticeboardname' => 'Tên thông báo',
  'noticeboardorder' => 'Hiển thị thông báo dạng menu theo thứ tự',
  'noticeboard:view' => 'Xem thông báo',
  'updatethis' => 'Cập nhật hiển thị bảng thông báo',
);