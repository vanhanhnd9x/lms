<?php $this->cache['vi']['auth_email'] = array (
  'auth_emaildescription' => 'Sự xác nhận Email là phương pháp chứng thực mặc đinh. Khi người dùng đăng ký, chọn tên đăng nhập và mật khẩu mới của riêng họ, một Email xác nhận được gửi tới địa chỉ Email của người dùng. Email này bao gồm một đường kết nối bảo đảm tới một trang mà ở đó người dùng có thể xác nhận tài khoản của họ. Các đăng nhập trong tương lai sẽ kiểm tra tên đăng nhập và mật khẩu lại, các giá trị được lưu trữ trong cơ sở dữ liệu của Moodle.',
  'auth_emailnoemail' => 'Đã cố gắng gửi cho bạn một email nhưng không thành công!',
  'auth_emailrecaptcha' => 'Adds a visual/audio confirmation form element to the signup page for email self-registering users. This protects your site against spammers and contributes to a worthwhile cause. See http://www.google.com/recaptcha/learnmore for more details. <br /><em>PHP cURL extension is required.</em>',
  'auth_emailrecaptcha_key' => 'Enable reCAPTCHA element',
  'auth_emailsettings' => 'Cài đặt',
  'pluginname' => 'Chứng thực dựa trên Email',
);