<?php $this->cache['vi']['auth_imap'] = array (
  'auth_imapdescription' => 'Phương pháp này sử dụng một máy chủ IMAP để kiểm tra khi nào tên đăng nhập và mật khẩu là hợp lệ.',
  'auth_imaphost' => 'Đia chỉ máy chủ IMAP. Sử dụng địa chỉ IP, không sử dụng tên DNS.',
  'auth_imaphost_key' => 'Host',
  'auth_imapchangepasswordurl_key' => 'Password-change URL',
  'auth_imapnotinstalled' => 'Cannot use IMAP authentication. The PHP IMAP module is not installed.',
  'auth_imapport' => 'Số cổng máy chủ IMAP. Thường thì nó là 143 hoặc 993.',
  'auth_imapport_key' => 'Port',
  'auth_imaptype' => 'Kiểu máy chủ IMAP. Các máy chủ IMAP có thể có các kiểu chứng thực và thoả thuận khác nhau.',
  'auth_imaptype_key' => 'Type',
  'pluginname' => 'Sử dụng một máy chủ IMAP',
);