<?php $this->cache['vi']['repository_recent'] = array (
  'configplugin' => 'Configuration for recent files repository',
  'recentfilesnumber' => 'Number of recent files',
  'emptyfilelist' => 'There are no files to show',
  'notitle' => 'notitle',
  'recent:view' => 'View recent files repsitory plugin',
  'pluginname_help' => 'Files recently used by current user',
  'pluginname' => 'Các tệp tin gần đây',
);