<?php $this->cache['vi']['auth_manual'] = array (
  'auth_manualdescription' => 'Phương pháp này xoá bỏ bất kỳ cách thức nào của những người sử dụng để tạo các tài khoản của riêng họ. Tất cả các tài khoản phải được tạo bằng tay bởi người quản trị.',
  'pluginname' => 'Manual accounts',
);