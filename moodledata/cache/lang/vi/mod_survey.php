<?php $this->cache['vi']['mod_survey'] = array (
  'actual' => 'Hiện tại',
  'actualclass' => 'Lớp học hiện tại',
  'actualstudent' => '{$a} hiện tại',
  'allquestions' => 'Tất cả câu hỏi theo thứ tự, tất cả sinh viên',
  'allscales' => 'Tất cả điểm, tất cả sinh viên',
  'alreadysubmitted' => 'Bạn sẵn sàng nộp bài khảo sát này',
  'analysisof' => 'Phân tích của {$a}',
  'answers' => 'Câu trả lời',
  'attlsintro' => 'Mục đích của những câu hỏi điều tra này là giúp chúng tôi đánh giá các quan điểm của bạn đối với ý kiến và sự học tập.


Không có câu trả lời \'đúng\' hoặc \'sai\' ; chúng tôi chỉ quan tâm đến quan điểm của bạn. Vui lòng chắc chắn rằng các câu trả lời của bạn được cân nhắc kỹ lưỡng, và sẽ không ảnh hưởng đến đánh giá của bạn.',
  'attlsmintro' => 'Theo sự thảo luận ...',
  'attlsm1' => 'Quan điểm theo hướng suy nghĩ và học tập',
  'attlsm2' => 'Kiến thức mạch lạc',
  'attlsm3' => 'Kiến thức gián đoạn',
  'attlsname' => 'ATTLS ( phiên bản 20 mục )',
  'attls1' => 'Theo đánh giá những gì mà ai đó nói, tôi tập trung vào nội dung chủ đề của họ đưa ra.',
  'attls1short' => 'Tập trung vào chất lượng của chủ đề',
  'attls10' => 'Nó quan trọng đối với tôi để giữ nguyên mục tiêu khi phân tích mọi thứ.',
  'attls10short' => 'Giữ nguyên mục tiêu',
  'attls11' => 'Tôi cố gắng hiểu mọi người thay vì chống đối lại họ.',
  'attls11short' => 'Hiểu mọi người',
  'attls12' => 'Tôi có một số tiêu chuẩn đánh được sử dụng để đánh giá các ý kiến đưa ra.',
  'attls12short' => 'Sử dụng tiêu chuẩn để đánh giá',
  'attls13' => 'Tôi hầu như cố gắng để hiểu quan điểm của những ngưới khác hơn là cố gắng đánh giá nó.',
  'attls13short' => 'Cố gắng để hiều',
  'attls14' => 'Tôi có gắng chỉ ra điểm yếu trong suy nghĩ của những người khác để giúp họ làm sáng tỏ chủ đề của họ.',
  'attls14short' => 'Chỉ ra các điểm yếu',
  'attls15' => 'Tôi đặt mình vào trong hoàn cảnh của người khác khi thảo luận các vấn đề, để xem tại sao họ lại  nghĩ ra cách họ làm .',
  'attls15short' => 'Đặt mình vào hoàn cảnh của họ',
  'attls16' => 'Ai đó có thể gọi phương pháp phân tích mọi thứ của tôi  \'giả thiết là dùng thử \' bởi vì tôi là người cẩn thận đối với tất cả các vấn đề.',
  'attls16short' => 'Đưa ra dùng thử',
  'attls17' => 'Tôi đánh giá tính hợp lý thông qua  sự đồng ý của các đồng nghiệp khi giải quyết các vấn đề.',
  'attls17short' => 'Tôi đánh giá tính hợp lý nhiều nhất',
  'attls18' => 'Tôi có thể đạt được sự hiểu biết sâu sắc thông qua các quan điểm dựa vào sự hiểu biết của tôi.',
  'attls18short' => 'Sự hiểu biết sâu sắc dựa vào sự thấu cảm',
  'attls19' => 'Khi tôi gặp mọi người mà quan điểm của họ trái ngược với tôi, tôi sẽ cố gắng giải thích cho họ hiểu quan điểm của tôi và tôi cố gắng tìm hiểu xem tại sao họ lại có những quan điểm đó.',
  'attls19short' => 'Tạo một kết qủa để mở rộng',
  'attls2' => 'Tôi thích tranh luận ngược lại với những gì ai đó đang nói.',
  'attls2short' => 'Đùa giỡn với người biện hộ của ngưòi khác',
  'attls20' => 'Tôi sử dụng thời gian để tìm ra điều gì là sai. Ví dụ tôi tìm mọi thứ theo nghĩa đen mà không đủ để tranh luận.',
  'attls20short' => 'Điểu sai là gì ?',
  'attls3' => 'Tôi muốn biết những người khác đến từ đâu, kinh nghiệm gì chỉ huy họ ảnh hưởng đến cách mà họ làm.',
  'attls3short' => 'Mọi người đến từ đâu',
  'attls4' => 'Nhiệm vụ quan trọng nhất trong việc học tập của tôi là học tập để hiểu được người khác.',
  'attls4short' => 'Nhận thức được cách giải thích của những người khác nhau.',
  'attls5' => 'Tôi nhận thấy rằng cách tốt nhất để tự đánh giá mình là giao tiếp nhiều với mọi người.',
  'attls5short' => 'Tương tác với nhiều thứ',
  'attls6' => 'Tôi thích nghe các quan điểm của mọi người đến từ các tầng lớp xã hội khác nhau - điều đó giúp tôi hiểu được những thứ giống nhau như thế nào thì có thể được hiểu theo các cách khác nhau.',
  'attls6short' => 'Thích nghe các quan điểm',
  'attls7' => 'Tôi nhận thấy rằng tôi có thể củng cố vị trí của tôi thông qua tranh luận với những người không đồng ý với tôi.',
  'attls7short' => 'Củng cố bởi sự tranh luận',
  'attls8' => 'Tôi luôn luôn quan tâm đến việc nhận ra tại sao mọi người nói và tin tưởng những điều mà họ làm.',
  'attls8short' => 'Nhận biết tại sao mọi người làm nó',
  'attls9' => 'Tôi thường đặt mình vào để tranh luận với các tác giả của các cuốn sách mà tôi đọc, cố gắng hình dung ra xem tại sao chúng sai.',
  'attls9short' => 'Tranh luận với các tác giả',
  'cannotfindanswer' => 'There are no answers for this survey yet.',
  'cannotfindquestion' => 'Question doesn\'t exist',
  'cannotfindsurveytmpt' => 'No survey templates found!',
  'ciqintro' => 'Trong khi suy nghĩ về các sự kiện hiện tại trong lớp học này, trả lời các câu hỏi dưới đây.',
  'ciqname' => 'Những đoạn then chốt',
  'ciq1' => 'Ở một thời điểm nào trong lớp học bạn hoạt động sôi nổi nhất với tư cách một học viên ?',
  'ciq1short' => 'Bận rộn nhất',
  'ciq2' => 'Ở thời điểm gì trong lớp học bạn bị lạc lõng với tư cách một học viên ?',
  'ciq2short' => 'Lạnh nhạt nhất',
  'ciq3' => 'Hoạt động gì từ ai đó trong các diễn đàn bạn nhận thấy hữu ích hoặc xác thực nhất ?',
  'ciq3short' => 'Khoảng thời gian có ích',
  'ciq4' => 'Hành động gì từ ai đó trong diễn đàn bạn nhận thấy khó hiểu nhất ?',
  'ciq4short' => 'Khoảng thời gian khó hiểu',
  'ciq5' => 'Sự kiện gì bạn ngặc nhiên nhất ?',
  'ciq5short' => 'Khoảng thời gian ngạc nhiên',
  'clicktocontinue' => 'kích chuột vào đây để tiếp tục',
  'clicktocontinuecheck' => 'Kích chuột vào đây để kiểm tra và tiếp tục',
  'collesaintro' => 'Mục đích của cuộc khảo sát này là giúp chúng tôi hiểu phân phối bài học trực tuyến này làm cho bạn có khả năng học tập.


Mỗi câu dưới đây đòi hỏi kinh nghiệm của bạn trong bài này .


Không có câu trả lời \'đúng\' hoặc \'sai\'; chúng tôi chỉ quan tâm đến quan điểm của bạn.Vui lòng đảm bảo rằng các câu trả lời của bạn được cân nhắc kỹ lưỡng .


Các câu trả lời được cân nhắc một cách cẩn thận sẽ giúp chúng tôi cải tiến phương pháp trình bày bài học trực tuyến trong tương lai.


Xin chân thành cảm ơn bạn.',
  'collesaname' => 'COLLES (Thực tế)',
  'collesapintro' => 'Mục đích của các câu hỏi khảo sát này là nhằm giúp chúng tôi khảo sát xem sự phân phối học tập trực tuyến thì tốt như thế nào để bạn học.


Mỗi câu dưới đây yêu câu bạn so sánh các ý kiến được ưa thích hơn của bạn và <b> thực tế </b> kinh nghiệm trong bài học này.


Không có câu trả lời \'đúng\' hoặc \'sai\'; chúng tôi chỉ quan tâm đến quan điểm của bạn.Vui lòng đảm bảo rằng các câu trả lời của bạn được cân nhắc kỹ lưỡng.


Các câu trả lời kỹ lưỡng của bạn giúp chúng tôi cải tiến cách thể hiện các bài học trực tuyến trong tương lai.


Xin chân thành cảm ơn bạn.',
  'collesapname' => 'COLLES ( Ưa chuộng và thực tế )',
  'collesmintro' => 'Trong bài học trực tuyến này...',
  'collesm1' => 'Có liên quan',
  'collesm1short' => 'Có liên quan',
  'collesm2' => 'Có sự suy nghĩ ý kiến',
  'collesm2short' => 'suy nghĩ ý kiến',
  'collesm3' => 'Tương tác',
  'collesm3short' => 'Tương tác',
  'collesm4' => 'Có sự hỗ trợ của giảng viên',
  'collesm4short' => 'Giảng viên hỗ trợ',
  'collesm5' => 'Bạn bè hỗ trợ',
  'collesm5short' => 'Bạn bè hỗ trợ',
  'collesm6' => 'Sự giải thích',
  'collesm6short' => 'Sự giải thích',
  'collespintro' => 'Mục đích của cuộc khảo sát này giúp chúng tôi hiểu những gì mà bạn đánh giá theo kinh nghiệm học tập trực tuyến .


 Mỗi câu trong số 24 câu dưới đây yêu cầu về các ý kiến được ưa thích của bạn </b> kinh nghiệm trong bài học này .


Không có các câu trả lời \'đúng \' hoặc \'sai \'; chúng tôi chỉ quan tâm đến quan điểm của bạn. Vui lòng chắc chắn rằng các câu trả lời của bạn được cân nhắc kỹ lưỡng.


Các câu trả lời xác thực của bạn sẽ giúp chúng tôi cải tiến cách trình bày các bài học trực tuyến trong tương lai.


 Xin chân thành cảm ơn bạn.',
  'collespname' => 'COLLES (Ưa chuộng)',
  'colles1' => 'Việc học tập của tôi tập trung vào các vấn đề mà liên quan đến tôi.',
  'colles1short' => 'Tập trung vào vấn đề đáng chú ý',
  'colles10' => 'Tôi yêu cầu các sinh viên khác giải thích các ý kiến của họ.',
  'colles10short' => 'Tôi yêu cầu các lới giải thích',
  'colles11' => 'Các sinh viên khác yêu cầu tôi giải thích ý kiến của tôi.',
  'colles11short' => 'Họ yêu cầu tôi giải thích',
  'colles12' => 'Các sinh viên khác trả lời các ý kiến của tôi.',
  'colles12short' => 'Các sinh viên trả lời tôi',
  'colles13' => 'Giáo viên khuyến khích ý kiến của tôi.',
  'colles13short' => 'Giáo viên khuyến khích ý kiến',
  'colles14' => 'Giáo viên khuyên khích tôi tham gia .',
  'colles14short' => 'Giáo viên khuyến khích tôi',
  'colles15' => 'Giáo viên làm mẫu bài thuyết trình tốt .',
  'colles15short' => 'Giáo viên làm mẫu bài thuyết trình',
  'colles16' => 'Giáo viên làm mẫu tự đánh giá bình phẩm .',
  'colles16short' => 'Giáo viên làm mẫu tự đánh giá',
  'colles17' => 'Các sinh viên khác khuyến khích tôi tham gia.',
  'colles17short' => 'Các sinh viên khuyến khích tôi',
  'colles18' => 'Các sinh viên khác ca ngợi sự đóng góp của tôi.',
  'colles18short' => 'các sinh viên ca ngợi tôi',
  'colles19' => 'Các sinh viên khác đánh giá sự đóng góp của tôi.',
  'colles19short' => 'Các sinh viên đánh giá tôi',
  'colles2' => 'Những gì tôi nghiên cứu thì quan trọng đối với thực hành chuyên môn của tôi.',
  'colles2short' => 'Quan trọng đối với sự thực hành của tôi',
  'colles20' => 'Các sinh viên khác nhấn mạnh sự cố gắng của tôi để học.',
  'colles20short' => 'Các sinh viên nhấn mạnh',
  'colles21' => 'Tôi thực hiện theo đúng thông báo của các sinh viên khác.',
  'colles21short' => 'Tôi hiểu các sinh viên khác',
  'colles22' => 'Các sinh viên khác thực hiện theo đúng trong các thông báo của tôi.',
  'colles22short' => 'Các sinh viên hiểu tôi',
  'colles23' => 'Tôi thực hiện theo đúng trong các thông báo của giáo viên hướng dẫn.',
  'colles23short' => 'Tôi hiểu hướng dẫn dạy',
  'colles24' => 'Giáo viên hướng dẫn thực hiện theo đúng các thông báo của tôi.',
  'colles24short' => 'Giáo viên hiểu tôi',
  'colles3' => 'Tôi nghiên cứu làm thế nào để nâng cao kỹ năng chuyên môn của tôi.',
  'colles3short' => 'Nâng cao kỹ năng chuyên môn của tôi',
  'colles4' => 'Những gì tôi nghiên cứu tốt cho việc chuyển tiếp sang thực hành chuyên môn của tôi.',
  'colles4short' => 'Liên kết với sự thực hành của tôi',
  'colles5' => 'Tôi suy nghĩ về làm thế nào để tôi học tốt.',
  'colles5short' => 'Tôi bình phẩm về kiến thức của tôi',
  'colles6' => 'Tôi suy nghĩ bình phẩm về các ý kiến của chính tôi.',
  'colles6short' => 'Tôi bình phẩm về các ý kiến của bản thân tôi.',
  'colles7' => 'Tôi suy nghĩ bình phẩm về các ý kiến của các học viên khác.',
  'colles7short' => 'Tôi bình phẩm về các bài viết của các sinh viên khác',
  'colles8' => 'Tôi suy nghĩ bình phẩm về các ý kiến trong các bài đọc .',
  'colles8short' => 'Tôi bình phẩm về các bài đọc',
  'colles9' => 'Tôi giải thích các ý kiến của tôi cho các sinh viên khác.',
  'colles9short' => 'Tôi giải thích các ý kiến của tôi',
  'customintro' => 'Thông tin giới thiệu',
  'deleteallanswers' => 'Delete all survey responses',
  'deleteanalysis' => 'Delete response analysis',
  'done' => 'Thực hiện',
  'download' => 'Tải xuống',
  'downloadexcel' => 'Tải dữ liệu xuống theo bảng tính Excel',
  'downloadinfo' => 'Bạn có thể tải dữ liệu xuống theo một biểu mẫu thích hợp với bản phân tích trong Excel, SPSS hoặc gói khác.',
  'downloadresults' => 'Download results',
  'downloadtext' => 'Tải dữ liệu như một file văn bản đơn giản',
  'editingasurvey' => 'Soạn thảo một cuộc khảo sát',
  'guestsnotallowed' => 'Khách không được phép đưa các cuộc khảo sát',
  'howlong' => 'Bạn hoàn thành cuộc khảo sát này trong bao lâu ?',
  'howlongoptions' => 'dưới 1 phút,1-2 phút ,2-3 phút,3-4 phút,4-5 phút,5-10 phút, nhiều hơn 10',
  'ifoundthat' => 'Tôi nhận thấy rằng',
  'introtext' => 'Văn bản giới thiệu',
  'invalidsurveyid' => 'Survey ID was incorrect',
  'invalidtmptid' => 'Invalid template id',
  'ipreferthat' => 'Tôi muốn rằng',
  'modulename' => 'Cuộc khảo sát',
  'modulename_help' => 'Các mô-đun hoạt động khảo sát cung cấp một số công cụ điều tra xác minh đã được tìm thấy hữu ích trong việc đánh giá và kích thích học tập trong môi trường trực tuyến. Một giáo viên có thể sử dụng chúng để thu thập dữ liệu từ các sinh viên của họ sẽ giúp họ tìm hiểu về lớp học của họ và phản ánh vào giảng dạy của mình.

Lưu ý rằng các công cụ khảo sát là trước đông người và có câu hỏi. Giáo viên, người muốn tạo khảo sát riêng của họ nên sử dụng các mô-đun hoạt động thông tin phản hồi.',
  'modulenameplural' => 'Cuộc khảo sát',
  'name' => 'Tên',
  'newsurveyresponses' => 'Các câu trả lời cuộc khảo sát mới',
  'nobodyyet' => 'Không ai hoàn thành cuộc khảo sát này',
  'notdone' => 'Chưa hoàn thành',
  'notes' => 'Các ghi chú và bản phân tích của riêng bạn',
  'othercomments' => 'Có phải bạn có các bình luận khác ?',
  'page-mod-survey-x' => 'Any survey module page',
  'peoplecompleted' => 'Đến thời điểm này có {$a} người hoàn thành cuộc khảo sát',
  'pluginadministration' => 'Survey administration',
  'pluginname' => 'Cuộc khảo sát',
  'preferred' => 'Được ưa thích',
  'preferredclass' => 'Giờ học được ưa thích',
  'preferredstudent' => '{$a} được ưa thích hơn',
  'question' => 'Câu hỏi',
  'questions' => 'Câu hỏi',
  'questionsnotanswered' => 'Một số câu hỏi chưa được trả lời .',
  'report' => 'Báo cáo cuộc khảo sát',
  'responsereports' => 'Response reports',
  'responses' => 'Responses',
  'savednotes' => 'Các ghi chú của bạn đã được cất',
  'scaleagree5' => 'Không đồng ý, Không đồng ý đến mức độ nào đó, Hoặc đồng ý hoặc không đồng ý, Đồng ý đến mức độ nào đó, Đồng ý',
  'scales' => 'Các tỷ lệ',
  'scaletimes5' => 'Không bao giờ, Hiếm khi, Thỉnh thoảng, thường xuyên, Luôn luôn',
  'seemoredetail' => 'Kích vào đây để xem chi tiết hơn.',
  'selectedquestions' => 'Các câu hỏi được lựa chọn theo một tỷ lệ, tất cả các sinh viên.',
  'summary' => 'Tổng kết',
  'surveycompleted' => 'Bạn vừa hoàn thành cuộc khảo sát này .Biểu đồ dưới đây tổng kết kết quả của bạn được so sánh với giá trị trung bình.',
  'survey:download' => 'Download responses',
  'surveygraph' => 'Survey graph',
  'surveyname' => 'Tên cuộc khảo sát',
  'survey:participate' => 'Respond to survey',
  'survey:readresponses' => 'View responses',
  'surveysaved' => 'Cuộc khảo sát được cất',
  'surveytype' => 'Kiểu khảo sát',
  'surveytype_help' => 'There are 3 available survey types:

* Attitudes to Thinking and Learning Survey (ATTLS) - For measuring the extent to which a person is a \'connected knower\' (tends to find learning more enjoyable, and is often more cooperative, congenial and more willing to build on the ideas of others) or a \'separate knower\' (tends to take a more critical and argumentative stance to learning)
* Critical incidents survey
* Constructivist On-line Learning Environment Survey (COLLES) - For monitoring the extent to which the interactive capacity of the World Wide Web may be exploited for engaging students in dynamic learning practices',
  'surveytype_link' => 'mod/survey/mod',
  'thanksforanswers' => 'Cám ơn bạn {$a} đã trả lời cuộc khảo sát này',
  'time' => 'Thời gian',
  'viewsurveyresponses' => 'Quan sát {$a} các câu trả lời cuộc khảo sát',
  'notyetanswered' => 'Not yet answered',
  'allquestionrequireanswer' => 'All questions are required and must be answered',
);