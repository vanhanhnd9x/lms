<?php $this->cache['vi']['core_group'] = array (
  'addgroup' => 'Add user into group',
  'addgroupstogrouping' => 'Add group to grouping',
  'addgroupstogroupings' => 'Add/remove groups',
  'adduserstogroup' => 'Add/remove users',
  'allocateby' => 'Allocate members',
  'anygrouping' => '[Any grouping]',
  'autocreategroups' => 'Tạo nhóm tự động',
  'backtogroupings' => 'Back to groupings',
  'backtogroups' => 'Back to groups',
  'badnamingscheme' => 'Must contain exactly one \'@\' or one \'#\'  character',
  'byfirstname' => 'Alphabetically by first name, last name',
  'byidnumber' => 'Alphabetically by ID number',
  'bylastname' => 'Alphabetically by last name, first name',
  'createautomaticgrouping' => 'Create automatic grouping',
  'creategroup' => 'Tạo nhóm',
  'creategrouping' => 'Tạo cách chọn nhóm',
  'creategroupinselectedgrouping' => 'Create group in grouping',
  'createingrouping' => 'Create in grouping',
  'createorphangroup' => 'Create orphan group',
  'databaseupgradegroups' => 'Groups version is now {$a}',
  'defaultgrouping' => 'Mặc định nhóm',
  'defaultgroupingname' => 'Nhóm',
  'defaultgroupname' => 'Group',
  'deleteallgroupings' => 'Delete all groupings',
  'deleteallgroups' => 'Delete all groups',
  'deletegroupconfirm' => 'Are you sure you want to delete group \'{$a}\'?',
  'deletegrouping' => 'Delete grouping',
  'deletegroupingconfirm' => 'Are you sure you want to delete grouping \'{$a}\'? (Groups in the grouping are not deleted.)',
  'deletegroupsconfirm' => 'Are you sure you want to delete the following groups?',
  'deleteselectedgroup' => 'Xóa nhóm đã chọn',
  'editgroupingsettings' => 'Edit grouping settings',
  'editgroupsettings' => 'Edit group settings',
  'enrolmentkey' => 'Enrolment key',
  'enrolmentkey_help' => 'An enrolment key enables access to the course to be restricted to only those who know the key. If a group enrolment key is specified, then not only will entering that key let the user into the course, but it will also automatically make them a member of this group.',
  'erroraddremoveuser' => 'Error adding/removing user {$a} to group',
  'erroreditgroup' => 'Error creating/updating group {$a}',
  'erroreditgrouping' => 'Error creating/updating grouping {$a}',
  'errorinvalidgroup' => 'Error, invalid group {$a}',
  'errorselectone' => 'Please select a single group before choosing this option',
  'errorselectsome' => 'Please select one or more groups before choosing this option',
  'evenallocation' => 'Note: To keep group allocation even, the actual number of members per group differs from the number you specified.',
  'existingmembers' => 'Existing members: {$a}',
  'filtergroups' => 'Filter groups by:',
  'group' => 'Group',
  'groupaddedsuccesfully' => 'Group {$a} added successfully',
  'groupby' => 'Specify',
  'groupdescription' => 'Group description',
  'groupinfo' => 'Info about selected group',
  'groupinfomembers' => 'Info about selected members',
  'groupinfopeople' => 'Info about selected people',
  'grouping' => 'Nhóm',
  'grouping_help' => 'A grouping is a collection of groups within a course. If a grouping is selected, students assigned to groups within the grouping will be able to work together.',
  'groupingdescription' => 'Grouping description',
  'groupingname' => 'Grouping name',
  'groupingnameexists' => 'The grouping name \'{$a}\' already exists in this course, please choose another one.',
  'groupings' => 'Groupings',
  'groupingsonly' => 'Groupings only',
  'groupmember' => 'Group member',
  'groupmemberdesc' => 'Standard role for a member of a group.',
  'groupmembers' => 'Group members',
  'groupmembersonly' => 'Available for group members only',
  'groupmembersonly_help' => 'If this checkbox is ticked, the activity (or resource) will only be available to students assigned to groups within the selected grouping.',
  'groupmembersonlyerror' => 'Sorry, you must be member of at least one group that is used in this activity.',
  'groupmemberssee' => 'See group members',
  'groupmembersselected' => 'Members of selected group',
  'groupmode' => 'Kiểu nhóm',
  'groupmode_help' => '<p>Chế độ nhóm có thể một trong ba chế độ:
   <ul>
      <li>Không có nhóm - không có các nhóm con, mọi người là một phần của cộng đồng lớn</li>
      <li>Các nhóm riêng rẽ - mỗi nhóm can chỉ có thể nhìn thấy các nhóm riêng của họ, các người khác sẽ không nhìn thấy</li>
      <li>Các nhóm nhìn thấy - mỗi nhóm làm việc trong nhóm riêng của họ, nhưng họ vẫn nhìn thấy các nhóm khác</li>
   </ul>
</p>

<p>Chế độ nhóm được xác định ở hai mức:</p>

<dl>
   <dt><b>1. Mức cua học</b></dt>
   <dd>Chế độ nhóm xác định tại mức cua học là chế độ mặc định
       cho tất cả các hoạt động xác định bên trong cua<br /><br /></dd>
   <dt><b>2. Mức hoạt động</b></dt>
   <dd>Mỗi hoạt động hỗ trợ nhóm có thể xác định chế độ nhóm
       riêng của nó.  Nếu cua học được đặt là "<a href="help.php?module=moodle&file=groupmodeforce.html">bắt buộc chế độ nhóm</a>" thì
       mọi thiết lập cho chế độ nhóm của mỗi hoạt động bị bỏ qua.</dd>
</dl>

<p>',
  'groupmodeforce' => 'Chế độ nhóm',
  'groupmodeforce_help' => 'If group mode is forced, then the course group mode is applied to every activity in the course. Group mode settings in each activity are then ignored.',
  'groupmy' => 'My group',
  'groupname' => 'Group name',
  'groupnameexists' => 'The group name \'{$a}\' already exists in this course, please choose another one.',
  'groupnotamember' => 'Sorry, you are not a member of that group',
  'groups' => 'Nhóm',
  'groupscount' => 'Groups ({$a})',
  'groupsgroupings' => 'Groups &amp; groupings',
  'groupsinselectedgrouping' => 'Groups in:',
  'groupsnone' => 'No groups',
  'groupsonly' => 'Groups only',
  'groupspreview' => 'Groups preview',
  'groupsseparate' => 'Separate groups',
  'groupsvisible' => 'Visible groups',
  'grouptemplate' => 'Group @',
  'hidepicture' => 'Hide picture',
  'importgroups' => 'Nhập dữ liệu nhóm',
  'importgroups_help' => 'Groups may be imported via text file. The format of the file should be as follows:

* Each line of the file contains one record
* Each record is a series of data separated by commas
* The first record contains a list of fieldnames defining the format of the rest of the file
* Required fieldname is groupname
* Optional fieldnames are description, enrolmentkey, picture, hidepicture',
  'importgroups_link' => 'group/import',
  'javascriptrequired' => 'This page requires JavaScript to be enabled.',
  'members' => 'Thành viên mỗi nhóm',
  'membersofselectedgroup' => 'Members of:',
  'namingscheme' => 'Kiểu tên nhóm',
  'namingscheme_help' => 'The at symbol (@) may be used to create groups with names containing letters. For example Group @ will generate groups named Group A, Group B, Group C, ...

The hash symbol (#) may be used to create groups with names containing numbers. For example Group # will generate groups named Group 1, Group 2, Group 3, ...',
  'newgrouping' => 'New grouping',
  'newpicture' => 'New picture',
  'newpicture_help' => 'Select an image in JPG or PNG format. The image will be cropped to a square and resized to 100x100 pixels.',
  'noallocation' => 'No allocation',
  'nogroups' => 'There are no groups set up in this course yet',
  'nogroupsassigned' => 'No groups assigned',
  'nopermissionforcreation' => 'Can\'t create group "{$a}" as you don\'t have the required permissions',
  'nosmallgroups' => 'Prevent last small group',
  'notingrouping' => '[Not in a grouping]',
  'nousersinrole' => 'There are no suitable users in the selected role',
  'number' => 'Số nhóm/thành viên',
  'numgroups' => 'Số nhóm',
  'nummembers' => 'Thành viên mỗi nhóm',
  'overview' => 'Overview',
  'potentialmembers' => 'Potential members: {$a}',
  'potentialmembs' => 'Potential members',
  'printerfriendly' => 'Printer-friendly display',
  'random' => 'Randomly',
  'removegroupfromselectedgrouping' => 'Remove group from grouping',
  'removefromgroup' => 'Remove user from group {$a}',
  'removefromgroupconfirm' => 'Do you really want to remove user "{$a->user}" from group "{$a->group}"?',
  'removegroupingsmembers' => 'Remove all groups from groupings',
  'removegroupsmembers' => 'Remove all group members',
  'removeselectedusers' => 'Remove selected users',
  'selectfromrole' => 'Select members from role',
  'showgroupsingrouping' => 'Show groups in grouping',
  'showmembersforgroup' => 'Show members for group',
  'toomanygroups' => 'Insufficient users to populate this number of groups - there are only {$a} users in the selected role.',
  'usercount' => 'User count',
  'usercounttotal' => 'User count ({$a})',
  'usergroupmembership' => 'Selected user\'s membership:',
);