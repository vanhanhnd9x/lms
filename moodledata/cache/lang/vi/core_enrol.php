<?php $this->cache['vi']['core_enrol'] = array (
  'actenrolshhdr' => 'Những plugins ghi danh có sẵn',
  'addinstance' => 'Thêm phương thức',
  'ajaxoneuserfound' => 'tìm thấy 1 người dùng',
  'ajaxxusersfound' => 'tìm thấy {$a} người dùng',
  'ajaxnext25' => '25 tiếp theo',
  'assignnotpermitted' => 'Bạn không có quyền hay không thể gán các vai trong khóa học này.',
  'bulkuseroperation' => 'Thao tác trên nhiều người dùng',
  'configenrolplugins' => 'Hãy chọn tất cả những plugins yêu cầu và sắp xếp chúng theo thứ tự hợp lý.',
  'custominstancename' => 'Tên instance có chỉnh sửa',
  'defaultenrol' => 'Thêm instance cho các khóa mới',
  'defaultenrol_desc' => 'Có thể thêm plugin này vào tất cả các khóa học mới một cách mặc định.',
  'deleteinstanceconfirm' => 'Do you really want to delete enrol plugin instance "{$a->name}" with {$a->users} enrolled users?',
  'durationdays' => '{$a} ngày',
  'enrol' => 'Ghi danh',
  'enrolcandidates' => 'Người dùng không ghi danh',
  'enrolcandidatesmatching' => 'Không phù hợp người dùng ghi danh',
  'enrolcohort' => 'Ghi danh vào khóa người học',
  'enrolcohortusers' => 'Ghi danh người dùng',
  'enrollednewusers' => 'Thành công ghi danh {$a} người dùng mới',
  'enrolledusers' => 'Người dùng đã ghi danh',
  'enrolledusersmatching' => 'Người dùng ghi danh phù hợp',
  'enrolme' => 'Ghi danh tôi vào khoá học này',
  'enrolmentinstances' => 'Phương thức ghi danh',
  'enrolmentnew' => 'Lượt ghi danh mới trong {$a}',
  'enrolmentnewuser' => '{$a->user} đã ghi danh vào khoá học "{$a->course}"',
  'enrolments' => 'Ghi danh',
  'enrolmentoptions' => 'Các tùy biến ghi danh',
  'enrolnotpermitted' => 'Bạn không có quyền hay không được phép ghi danh ai vào khóa học này cả',
  'enrolperiod' => 'Thời hạn ghi danh',
  'enrolusage' => 'Các instances / Các ghi danh',
  'enrolusers' => 'Ghi danh người dùng',
  'enroltimeend' => 'Thời hạn ghi danh kết thúc',
  'enroltimestart' => 'Thời hạn ghi danh bắt đầu',
  'errajaxfailedenrol' => 'Ghi danh người dùng không thành công',
  'errajaxsearch' => 'Lỗi khi tìm người dùng',
  'erroreditenrolment' => 'Lỗi xuất hiện khi cố gắng chỉnh sửa việc ghi danh của một người dùng',
  'errorenrolcohort' => 'Lỗi tạo instance ghi danh cho cùng một khóa người dùng trong khóa học này.',
  'errorenrolcohortusers' => 'Lỗi ghi danh thành viên khóa người học trong khóa học này',
  'errorwithbulkoperation' => 'Có lỗi troong quá trình xử lý việc thay đổi ghi danh hàng loạt',
  'finishenrollingusers' => 'Kết thúc việc ghi danh người dùng',
  'invalidenrolinstance' => 'Instance ghi danh không có giá trị',
  'invalidrole' => 'Vai không có giá trị',
  'manageenrols' => 'Quản lý các plugins ghi danh',
  'manageinstance' => 'Quản lý',
  'nochange' => 'Không thay đổi',
  'noexistingparticipants' => 'Không có thành viên',
  'noguestaccess' => 'Khách không thể truy cập khóa học này, làm ơn đăng nhập.',
  'none' => 'Không',
  'notenrollable' => 'Khoá học này hiện không cho phép ghi danh',
  'notenrolledusers' => 'Những người dùng khác',
  'otheruserdesc' => 'Những người dùng sau không được ghi danh vào khóa học này nhưng lại có các vai được gán hay thừa hưởng ở bên trong khóa học.',
  'participationactive' => 'Đang hoạt động',
  'participationstatus' => 'Trạng thái',
  'participationsuspended' => 'Đang bị treo',
  'periodend' => 'đến {$a}',
  'periodstart' => 'từ {$a}',
  'periodstartend' => 'từ {$a->start} đến {$a->end}',
  'recovergrades' => 'Phục hồi điểm số cũ của người dùng nếu có thể',
  'rolefromthiscourse' => '{$a->role} (Được gán ở mức khóa học này)',
  'rolefrommetacourse' => '{$a->role} (Thừa hưởng từ khóa học bậc trên)',
  'rolefromcategory' => '{$a->role} (Thừa hưởng từ phụ mục khóa học)',
  'rolefromsystem' => '{$a->role} (Được gán ở mức hệ thống site)',
  'startdatetoday' => 'Hôm nay',
  'synced' => 'Đồng bộ',
  'totalenrolledusers' => '{$a} người dùng đã được ghi danh',
  'totalotherusers' => '{$a} người dùng khác',
  'unassignnotpermitted' => 'Bạn không có quyền để bỏ vai trong khóa học này',
  'unenrol' => 'Rút tên',
  'unenrolconfirm' => 'Bạn có thật sự muốn bỏ ghi danh của người dùng "{$a->user}" ra khỏi khóa"{$a->course}"?',
  'unenrolme' => 'Rút tên tôi khỏi khoá học {$a}',
  'unenrolnotpermitted' => 'Bạn không có quyền hay không thể hủy ghi danh người dùng từ khóa học này',
  'unenrolroleusers' => 'Rút tên học viên',
  'uninstallconfirm' => 'You are about to completely delete the enrol plugin \'{$a}\'.  This will completely delete everything in the database associated with this enrolment type.  Are you SURE you want to continue?',
  'uninstalldeletefiles' => 'All data associated with the enrol plugin \'{$a->plugin}\' has been deleted from the database.  To complete the deletion (and prevent the plugin re-installing itself), you should now delete this directory from your server: {$a->directory}',
  'unknowajaxaction' => 'Yêu cầu thao tác không xác định',
  'unlimitedduration' => 'Không giới hạn',
  'usersearch' => 'Tìm kiếm',
  'withselectedusers' => 'Với những người dùng được chọn',
  'extremovedaction' => 'Thao tác bỏ ghi danh từ bên ngoài',
  'extremovedaction_help' => 'Select action to carry out when user enrolment disappears from external enrolment source. Please note that some user data and settings are purged from course during course unenrolment.',
  'extremovedsuspend' => 'Tắt chức năng ghi danh vào khóa học',
  'extremovedsuspendnoroles' => 'Tắt chức năng ghi danh vào khóa học và xóa các vai',
  'extremovedkeep' => 'Giữ trạng thái ghi danh cho người dùng',
  'extremovedunenrol' => 'Bỏ người dùng khỏi khóa học',
);