<?php $this->cache['vi']['block_online_users'] = array (
  'configtimetosee' => 'Khoảng thời gian (phút) để xem như thành viên không còn trực tuyến nếu không có hoạt động gì',
  'online_users:viewlist' => 'Xem danh sách thành viên trực tuyến',
  'periodnminutes' => 'cách đây {$a} phút',
  'pluginname' => 'Thành viên trực tuyến',
  'timetosee' => 'Thời gian gỡ tên (phút) nếu không có hoạt động gì',
);