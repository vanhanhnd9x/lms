<?php $this->cache['vi']['auth_none'] = array (
  'auth_nonedescription' => 'Người dùng có thể đăng ký va tạo một tài khoản hợp lệ ngay lập tức, không chứng thực dựa vào một máy chủ ở bên ngoài và không xác nhận qua Email. Cẩn thận sử dụng tuỳ chọn này - suy nghĩ về các vấn đề bảo mật và quản trị điều này có thể là nguyên nhân.',
  'pluginname' => 'Không chứng thực',
);