<?php $this->cache['vi']['mod_folder'] = array (
  'contentheader' => 'Nội dung',
  'folder:managefiles' => 'Quản lý các file trong module thư mục ',
  'folder:view' => 'Xem nội dung thư mục',
  'foldercontent' => 'Tập tin và thư mục con',
  'modulename' => 'Thư mục',
  'modulenameplural' => 'Thư mục',
  'neverseen' => 'Chưa từng có',
  'page-mod-folder-x' => 'Bất kỳ trang module thư mục',
  'page-mod-folder-view' => 'Trang chính module thư mục',
  'pluginadministration' => 'Quản lý thư mục',
  'pluginname' => 'Thư mục',
);