<?php $this->cache['vi']['report_questioninstances'] = array (
  'editquestionshere' => 'Chỉnh sửa câu hỏi trong trường hợp này',
  'getreport' => 'Báo cáo',
  'hiddenquestions' => 'Ẩn',
  'intro' => 'Báo cáo này liệt kê tất cả các bối cảnh trong hệ thống, nơi có những câu hỏi của một loại cụ thể.',
  'pluginname' => 'Câu hỏi',
  'questioninstances:view' => 'Xem báo cáo câu hỏi',
  'reportforallqtypes' => 'Báo cáo cho tất cả dạng câu hỏi',
  'reportformissingqtypes' => 'Report for question of unknown types',
  'reportforqtype' => 'Báo cáo cho dạng câu hỏi \'{$a}\'',
  'reportsettings' => 'Thiết lập báo cáo',
  'totalquestions' => 'Tổng',
  'visiblequestions' => 'Hiển thị',
);