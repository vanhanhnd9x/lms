<?php $this->cache['vi']['mod_chat'] = array (
  'ajax' => 'Version using Ajax',
  'autoscroll' => 'Cuốn tự động',
  'beep' => 'bíp',
  'cantlogin' => 'Không thể đăng nhập phòng chát',
  'configmethod' => 'Với kiểu chat bình thường, trình duyệt sẽ nối kết thường xuyên tới máy chủ để cập nhật thông tin. Không cần phải thiết lập gì cả, và ở đâu cũng hoạt động được. Tuy nhiên, máy chủ sẽ có thể bị nặng tải, đặc biệt là khi có nhiều người cùng tham gia họp. Sử dụng máy chủ daemon đòi hỏi bạn phải truy cập vào môi trường dòng lệnh Unix, nhưng bù lại các phiên chat sẽ có thể diễn ra nhanh chóng và nhịp nhàng hơn.',
  'confignormalupdatemode' => 'Bạn có thể dùng chức năng <em>Kết nối Thường trực</em> của HTTP 1.1 để cập nhật các phòng họp một cách hiệu quả, nhưng việc đó sẽ làm nặng tải trên máy chủ của bạn. Phương pháp cập nhật <em>Dòng</em> có thể cho kết quả tốt hơn khi có nhiều người truy cập cùng lúc (tương tự cách dùng chatd), nhưng có thể là không được hỗ trợ trên máy chủ của bạn.',
  'configoldping' => 'Thời gian tối đa (giây) trước khi kiểm tra lại xem thành viên có kết nối hay không? Đây là mức giới hạn trần, vì thông thường hệ thống có thể nhanh chóng phát hiện ra thành viên thoát ra ngoài. Thời gian càng ngắn máy chủ sẽ tải càng nặng. Nếu dùng chế độ bình thường, không bao giờ để dưới 2 * chat_refresh_room.',
  'configrefreshroom' => 'Phòng họp cần được tải lại sau mỗi bao lâu (giây)? Để số thấp có thể giúp bạn có cảm giác phòng họp hoạt động nhanh hơn, nhưng sẽ làm nặng tải máy chủ khi có nhiều người tham gia cung lúc. Nếu đang dùng phương pháp cập nhật <em>Dòng</em>, bạn có thể chọn số cao hơn -- hãy thử ở mức 2.',
  'configrefreshuserlist' => 'Danh sách thành viên cần được tải lại sau mỗi bao lâu (giây)?',
  'configserverhost' => 'Tên máy tính (hostname) nơi chạy chương trình daemon',
  'configserverip' => 'Địa chỉ IP của tên máy ở trên',
  'configservermax' => 'Số máy khách tối đa cho phép truy cập',
  'configserverport' => 'Cổng dùng cho máy chủ daemon',
  'currentchats' => 'Phiên chat đang mở',
  'currentusers' => 'Thành viên đang có mặt',
  'deletesession' => 'Xoá phiên họp này',
  'deletesessionsure' => 'Bạn có chắc chắn muốn xoá phiên họp này?',
  'donotusechattime' => 'Không nêu thời gian họp',
  'enterchat' => 'Nhấn vào đây để tham gia',
  'errornousers' => 'Không tìm thấy thành viên nào!',
  'explaingeneralconfig' => '<strong>Luôn luôn</strong> sử dụng các thiết lập này',
  'explainmethoddaemon' => 'Các thiết lập này <strong>chỉ</strong> có hiệu lực sau khi bạn đã chọn kiểu "Máy chủ daemon"',
  'explainmethodnormal' => 'Các thiết lập này <strong>chỉ</strong> có hiệu lực sau khi bạn đã chọn kiểu "Họp bình thường"',
  'generalconfig' => 'General configuration',
  'chat:deletelog' => 'Xoá nhật kí các buổi chat',
  'chat:exportparticipatedsession' => 'Export chat session which you took part in',
  'chat:exportsession' => 'Export any chat session',
  'chat:chat' => 'Tham gia chat',
  'chatintro' => 'Nội dung giới thiệu',
  'chatname' => 'Tên phòng',
  'chat:readlog' => 'Xem nhật kí các buổi chat',
  'chatreport' => 'Chat sessions',
  'chat:talk' => 'Talk in a chat',
  'chattime' => 'Phiên họp tiếp theo',
  'idle' => 'Im re',
  'inputarea' => 'Input area',
  'invalidid' => 'Could not find that chat room!',
  'list_all_sessions' => 'List all sessions.',
  'list_complete_sessions' => 'List just complete sessions.',
  'listing_all_sessions' => 'Listing all sessions.',
  'messagebeepseveryone' => '{$a} gọi tất cả mọi người!',
  'messagebeepsyou' => '{$a} vừa gọi bạn!',
  'messageenter' => '{$a} vừa vào phòng họp',
  'messageexit' => '{$a} vừa ra khỏi phòng họp này',
  'messages' => 'Thông điệp',
  'messageyoubeep' => 'You beeped {$a}',
  'method' => 'Kiểu họp',
  'methoddaemon' => 'Máy chủ daemon',
  'methodnormal' => 'Kiểu bình thường',
  'methodajax' => 'Ajax method',
  'modulename' => 'Phòng họp trực tuyến',
  'modulename_help' => 'Các mô-đun hoạt động trò chuyện cho phép người tham gia để có dựa trên văn bản, thời gian thực các cuộc thảo luận, đồng bộ.

Các cuộc trò chuyện có thể là một hoạt động thời gian hoặc nó có thể được lặp đi lặp lại cùng một lúc mỗi ngày hoặc mỗi tuần. Các buổi trò chuyện được lưu lại và có thể được làm sẵn có cho tất cả mọi người để xem hoặc hạn chế người sử dụng với khả năng để xem phiên bản ghi trò chuyện.

Các cuộc trò chuyện đặc biệt hữu ích khi trò chuyện nhóm không có khả năng đáp ứng mặt-đối-mặt, chẳng hạn như

* Các cuộc họp thường xuyên của sinh viên tham gia các khóa học trực tuyến để giúp họ có thể chia sẻ kinh nghiệm với những người khác trong quá trình tương tự nhưng ở một vị trí khác nhau
* Một học viên tạm thời không thể đích thân tham dự trò chuyện với giáo viên của họ để bắt đầu với công việc
* Học viên trao đổi trên kinh nghiệm làm việc với nhau và giáo viên của họ
* Một câu hỏi và câu trả lời của phiên họp với các diễn giả được mời vào một vị trí khác
* Phiên làm việc để giúp học sinh chuẩn bị cho bài kiểm tra mà các giáo viên, hoặc sinh viên khác, sẽ đặt ra câu hỏi mẫu',
  'modulenameplural' => 'Phòng họp trực tuyến',
  'neverdeletemessages' => 'Không bao giờ xoá bài viết',
  'nextsession' => 'Phiên họp sắp tới',
  'no_complete_sessions_found' => 'No complete sessions found.',
  'noguests' => 'Phiên họp này không mở cho khách vãng lai',
  'nochat' => 'Không tìm thấy phòng chát nào',
  'nomessages' => 'Chưa có ai viết gì',
  'normalkeepalive' => 'Nối thường trực',
  'normalstream' => 'Dòng',
  'noscheduledsession' => 'Không có phiên họp nào được lên lịch',
  'notallowenter' => 'You are not allow to enter the chat room.',
  'notlogged' => 'You are not logged in!',
  'nopermissiontoseethechatlog' => 'You don\'t have permission to see the chat logs.',
  'oldping' => 'Thời hạn ngưng kết nối',
  'page-mod-chat-x' => 'Any chat module page',
  'pastchats' => 'Các phiên họp trước',
  'pluginadministration' => 'Quản lý chức năng chát',
  'pluginname' => 'Phòng họp trực tuyến',
  'refreshroom' => 'Tải lại phòng họp',
  'refreshuserlist' => 'Tải lại danh sách thành viên',
  'removemessages' => 'Xoá hết bài viết',
  'repeatdaily' => 'Mỗi ngày, cùng giờ',
  'repeatnone' => 'Không lặp lại - chỉ mở ngày đã chọn',
  'repeattimes' => 'Mở lại phiên họp',
  'repeatweekly' => 'Hàng tuần, cùng giờ',
  'saidto' => 'said to',
  'savemessages' => 'Lưu các phiên họp trước',
  'seesession' => 'Xem phiên họp này',
  'send' => 'Send',
  'sending' => 'Sending',
  'serverhost' => 'Tên máy chủ',
  'serverip' => 'Địa chỉ IP máy chủ',
  'servermax' => 'Số thành viên tối đa',
  'serverport' => 'Cổng máy chủ',
  'sessions' => 'Phiên họp',
  'sessionstart' => 'Chat session will be start in: {$a}',
  'strftimemessage' => '%H:%M',
  'studentseereports' => 'Mọi người đều xem được các phiên họp trước',
  'studentseereports_help' => 'If set to No, only users have mod/chat:readlog capability are able to see the chat logs',
  'talk' => 'Talk',
  'updatemethod' => 'Phương pháp cập nhật',
  'updaterate' => 'Update rate:',
  'userlist' => 'User list',
  'usingchat' => 'Sử dụng Chát',
  'usingchat_help' => '<p>Mô đun chát bao gồm một số các tính năng giúp cho buổi nói chuyện thêm hứng thú.</p>

<dl>
<dt><b>Các mặt cười</b></dt>
<dd>Bất cứ mặt cười nào (Biểu tượng biểu cảm - emoticons) mà bạn có thể dùng ở nơi khác thì đều có thể dùng ở Moodle, và chúng sẽ hiển thị chính xác.  Thí dụ,  :-) = <img alt src="pix/s/smiley.gif" />  </dd>

<dt><b>Các liên kết</b></dt>
<dd>Các địa chỉ Internet sẽ được tự động chuyển thành liên kết.</dd>

<dt><b>Biểu cảm</b></dt>
<dd>Bạn có thể bắt đầu một dòng với "/me" hay ":" để biểu hiện cảm xúc.  Thí dụ, nếu tên bạn là Thúy và bạn nhập vào ":laughs!" hay "/me laughs!" thì mọi người sẽ thấy hiện lên "Thúy laughs!"</dd>

<dt><b>Tiếng bíp</b></dt>
<dd>Bạn có thể gửi một âm thanh cho người khác bằng cách nhấn vào liên kết "beep" bên cạnh tên của họ.  Một lối tắt (shortcut) tiện lợi để gửi "bíp" tới tất cả mọi người cùng một lúc là dòng "beep all".</dd>

<dt><b>HTML</b></dt>
<dd>Nếu bạn có biết các mã HTML, thì bạn có thể dùng chúng để chèn một hình ảnh, cho phát âm thanh hay tạo các màu hay các cỡ chữ khác nhau.</dd>

</dl>',
  'viewreport' => 'Xem các phiên họp trước',
);