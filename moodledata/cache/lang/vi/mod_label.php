<?php $this->cache['vi']['mod_label'] = array (
  'labeltext' => 'Nhãn',
  'modulename' => 'Nhãn',
  'modulename_help' => 'Một nhãn hiệu cho phép văn bản và hình ảnh sẽ được chèn vào trong các liên kết hoạt động trên các trang khóa học.',
  'modulenameplural' => 'Nhãn',
  'pluginadministration' => 'Quản lý nhãn',
  'pluginname' => 'Nhãn',
);