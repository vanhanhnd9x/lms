<?php $this->cache['vi']['qtype_multianswer'] = array (
  'addingmultianswer' => 'Thêm một câu hỏi (Cloze) trả lời nhúng',
  'confirmquestionsaveasedited' => 'Tôi xác nhận rằng tôi muốn các câu hỏi được lưu khi chỉnh sửa nội dung',
  'confirmsave' => 'Xác nhận sau đó lưu {$a}',
  'correctanswer' => 'Câu trả lời đúng',
  'correctanswerandfeedback' => 'Câu trả lời và phản hồi chính xác',
  'decodeverifyquestiontext' => 'Giải mã và kiểm tra nội dung câu hỏi',
  'editingmultianswer' => 'Chỉnh sửa một câu hỏi (Cloze) trả lời nhúng',
  'layout' => 'Bố trí',
  'layouthorizontal' => 'Nút chọn hàng ngang',
  'layoutselectinline' => 'Menu trôi xuống trong nội dung',
  'layoutundefined' => 'Không bố trí',
  'layoutvertical' => 'Nút chọn hàng dọc',
  'multianswer' => 'Câu trả lời nhúng (Cloze)',
  'multianswersummary' => 'Câu hỏi của loại này là rất linh hoạt, nhưng chỉ có thể được tạo ra bằng cách nhập văn bản có chứa đoạn mã đặc biệt để tạo ra nhúng nhiều lựa chọn, câu trả lời ngắn và các câu hỏi số.',
  'multianswer_help' => 'Câu hỏi câu trả lời nhúng (Cloze) bao gồm một đoạn văn bản với những câu hỏi như nhiều lựa chọn và câu trả lời ngắn nhúng bên trong nó.',
  'multianswer_link' => 'question/type/multianswer',
  'nooptionsforsubquestion' => 'Không thể có được lựa chọn cho phần câu hỏi # {$a->sub} (question->id={$a->id})',
  'noquestions' => 'Câu hỏi Cloze(nhiều câu trả lời) "<strong>{$a}</strong>" không chứa bất kỳ câu hỏi',
  'qtypenotrecognized' => 'kiểu câu hỏi {$a} không được công nhận',
  'questionnadded' => 'câu hỏi thêm',
  'questiondefinition' => 'Định nghĩa câu hỏi',
  'questiondeleted' => 'Câu hỏi đã bị xóa',
  'questioninquiz' => '

<ul>
  <li>thêm hoặc xóa các câu hỏi, </li>
  <li>thay đổi thứ tự các câu hỏi trong văn bản,</li>
  <li>thay đổi loại câu hỏi của (số, trả lời ngắn, nhiều lựa chọn). </li></ul>
',
  'questionsless' => '{$a} câu hỏi (s) ít hơn so với các câu hỏi nhiều câu trả lời được lưu trữ trong cơ sở dữ liệu',
  'questionsmissing' => 'Không có câu hỏi hợp lệ, tạo ra ít nhất một câu hỏi',
  'questionsmore' => '{$a} câu hỏi (s) nhiều hơn so với các câu hỏi nhiều câu trả lời được lưu trữ trong cơ sở dữ liệu',
  'questionnotfound' => 'Không thể tìm thấy câu hỏi của phần #{$a}',
  'questionsaveasedited' => 'Câu hỏi đặt ra sẽ được lưu khi thay đổi nội dung',
  'questiontypechanged' => 'Kiểu câu hỏi đã thay đổi',
  'questiontypechangedcomment' => 'Ít nhất một loại câu hỏi đã được thay đổi.<br \\>Bạn đã thêm, xóa hoặc di chuyển một câu hỏi?<br \\>Nhìn về phía trước.',
  'questionusedinquiz' => 'Câu hỏi này được sử dụng trong {$a->nb_of_quiz} quiz(s), Tổng số trả lời : {$a->nb_of_attempts} ',
  'storedqtype' => 'Lưu trữ câu hỏi loại {$a}',
  'subqresponse' => 'Phần {$a->i}: {$a->response}',
  'unknownquestiontypeofsubquestion' => 'Không biết loại câu hỏi: {$a->type} của câu hỏi phần # {$a->sub}',
  'warningquestionmodified' => '<b>CẢNH BÁO</b>',
  'youshouldnot' => '<b>KHÔNG PHẢI BẠN</b>',
);