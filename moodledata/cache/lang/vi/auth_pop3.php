<?php $this->cache['vi']['auth_pop3'] = array (
  'auth_pop3description' => 'Phương pháp này sử dụng một POP3 server để kiểm tra khi nào tên đăng nhập và mật khẩu đưa ra là hợp lệ.',
  'auth_pop3host' => 'Địa chỉ POP3 server. Sử dụng địa chỉ IP, không sử dụng tên DNS.',
  'auth_pop3host_key' => 'Host',
  'auth_pop3changepasswordurl_key' => 'Password-change URL',
  'auth_pop3mailbox' => 'Tên của hộp thư để thử một kết nối.  (thông thường INBOX)',
  'auth_pop3mailbox_key' => 'Mailbox',
  'auth_pop3notinstalled' => 'Cannot use POP3 authentication. The PHP IMAP module is not installed.',
  'auth_pop3port' => 'Cổng máy chủ (110 là phổ biến nhât, 995 thì thông dụng đối với SSL)',
  'auth_pop3port_key' => 'Port',
  'auth_pop3type' => 'Kiểu máy chủ. Nếu máy chủ của bạn sử dụng bảo mật hợp lý, chọn pop3cert.',
  'auth_pop3type_key' => 'Type',
  'pluginname' => 'POP3 server',
);