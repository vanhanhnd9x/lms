<?php $this->cache['vi']['qtype_numerical'] = array (
  'acceptederror' => 'Chấp nhận lỗi',
  'addingnumerical' => 'Thêm câu hỏi Số học',
  'addmoreanswerblanks' => 'Blanks for {no} More Answers',
  'addmoreunitblanks' => 'Blanks for {no} More Units',
  'answermustbenumberorstar' => 'Câu trả lời phải là một số, ví dụ -1.234 hay 3e8, hay \'*\'.',
  'answerno' => 'Câu trả lời {$a}',
  'decfractionofquestiongrade' => 'Như một phần (0-1) của đánh giá câu hỏi',
  'decfractionofresponsegrade' => 'Như một phần (0-1) của đánh giá câu trả lời',
  'decimalformat' => 'Số thập phân',
  'editableunittext' => 'Các yếu tố đầu vào văn bản',
  'editingnumerical' => 'Chỉnh sửa câu hỏi dạng số',
  'errornomultiplier' => 'Bạn phải xác định một số nhân cho đơn vị này.',
  'errorrepeatedunit' => 'Bạn không thể có hai đơn vị có cùng tên.',
  'geometric' => 'Hình học',
  'invalidnumber' => 'Bạn phải nhập một số hợp lệ.',
  'invalidnumbernounit' => 'Bạn phải nhập một số hợp lệ. Không bao gồm đơn vị trong trả lời của bạn.',
  'invalidnumericanswer' => 'Một trong những câu trả lời mà bạn đã nhập không phải là một số hợp lệ.',
  'invalidnumerictolerance' => 'Một trong những dung sai mà bạn đã nhập không phải là một số hợp lệ.',
  'leftexample' => 'Bên trái, ví dụ $1.00 or Â£1.00',
  'multiplier' => 'Nhân',
  'noneditableunittext' => 'Không chỉnh sửa văn bản của đơn vị số 1',
  'nonvalidcharactersinnumber' => 'Không có ký tự hợp lệ trong số',
  'notenoughanswers' => 'Bạn phải nhập ít nhất một câu trả lời.',
  'nounitdisplay' => 'Không có đơn vị chấm điểm',
  'numerical' => 'Số',
  'numerical_help' => 'Từ quan điểm của học viên, một câu hỏi số trông giống như một câu hỏi trả lời ngắn. Sự khác biệt là câu trả lời số được phép chấp nhận có một lỗi. This allows a fixed range of answers to be evaluated as one answer. For example, if the answer is 10 with an accepted error of 2, then any number between 8 and 12 will be accepted as correct. ',
  'numerical_link' => 'question/type/numerical',
  'numericalsummary' => 'Cho phép một phản ứng số, có thể với các đơn vị, đó là phân loại bằng cách so sánh với mô hình câu trả lời khác nhau, có thể có dung sai.',
  'numericalmultiplier' => 'Nhân',
  'numericalmultiplier_help' => 'Số nhân là yếu tố mà các đáp án số chính xác sẽ được nhân.

Đơn vị đầu tiên (Đơn vị 1) có một số nhân mặc định của 1. Vì vậy, nếu các phản ứng số chính xác là 5500 và bạn thiết lập W là đơn vị trong đó có 1 số nhân như mặc định, câu trả lời đúng là 5500 W.

Nếu bạn thêm các đơn vị kW với một số nhân của 0.001, điều này sẽ thêm một đáp án chính xác là 5,5 kW. Điều này có nghĩa rằng các câu trả lời 5500W hoặc 5.5kW sẽ được đánh dấu chính xác.

Lưu ý rằng các lỗi được chấp nhận cũng được nhân, do đó, một lỗi cho phép của 100W sẽ trở thành một lỗi của 0.1kW.',
  'manynumerical' => 'Đơn vị là tùy chọn. Nếu một đơn vị được nhập vào, nó được sử dụng để chuyển đổi các đáp án để đơn vị trước khi chấm điểm.',
  'nominal' => 'định danh',
  'onlynumerical' => 'Đơn vị không được sử dụng ở tất cả. Chỉ số giá trị được chấm điểm.',
  'oneunitshown' => 'Một đơn vị được tự động hiển thị bên cạnh hộp câu trả lời.',
  'pleaseenterananswer' => 'Xin vui lòng nhập một câu trả lời.',
  'pleaseenteranswerwithoutthousandssep' => 'Xin vui lòng điền câu trả lời của bạn mà không sử dụng các dấu phân cách nghìn ({$a}).',
  'relative' => 'Tương đối',
  'rightexample' => 'Bên phải, ví dụ 1.00cm hay 1.00km',
  'selectunits' => 'Chọn đơn vị',
  'selectunit' => 'Chọn 1 đơn vị',
  'studentunitanswer' => 'Đơn vị là đầu vào sử dụng',
  'tolerancetype' => 'Kiểu khoan dung',
  'unit' => 'Đơn vị',
  'unitappliedpenalty' => 'Các nhãn hiệu này bao gồm một hình phạt của {$a} cho đơn vị sai.',
  'unitchoice' => 'Lựa chọn nhiều',
  'unitedit' => 'Chỉnh sửa đơn vị',
  'unitgraded' => 'Đơn vị phải được đưa ra, và sẽ được chấm điểm.',
  'unithandling' => 'Đơn vị xử lý',
  'unithdr' => 'Đơn vị {$a}',
  'unitincorrect' => 'Bạn không cung cấp cho các đơn vị chính xác.',
  'unitmandatory' => 'Bắt buộc',
  'unitmandatory_help' => '

* Đáp án sẽ được chấm điểm bằng cách sử dụng đơn vị bằng văn bản.

* Hình phạt sẽ được áp dụng nếu trường đơn vị bị bỏ trống

',
  'unitnotselected' => 'Bạn phải chọn một đơn vị.',
  'unitonerequired' => 'Bạn phải nhập ít nhất một đơn vị',
  'unitoptional' => 'Tuỳ chọn đơn vị',
  'unitoptional_help' => '
* Nếu trường đơn vị là trắng, đáp án sẽ được chấm điểm sử dụng đơn vị.

* Nếu đơn vị là khó viết hoặc không biết, đáp án sẽ được coi là không hợp lệ.
',
  'unitpenalty' => 'Phạt đơn vị',
  'unitpenalty_help' => 'Hình phạt được áp dụng nếu

* tên đơn vị được nhập vào sai, hay
* một đơn vị được nhập vào ô giá trị',
  'unitposition' => 'Về đơn vị',
  'unitselect' => 'một trình đơn thả xuống',
  'validnumberformats' => 'Định dạng số hợp lệ',
  'validnumberformats_help' => '
* số đúng 13500.67, 13 500.67, 13500,67 hoặc 13 500,67

* nếu bạn sử dụng, như tách số hàng nghìn đưa về số thập phân. như trong
 13,500.67 : 13,500.

* Cho hình thức số mũ, gọi 1.350067 * 10<sup>4</sup>, dùng
 1.350067 E4 : 1.350067 E04 ',
  'validnumbers' => '13500.67, 13 500.67, 13,500.67, 13500,67, 13 500,67, 1.350067 E4 hoặc 1.350067 E04',
);