<?php $this->cache['vi']['mod_page'] = array (
  'configdisplayoptions' => 'Chọn tất cả các tùy chọn mà có sẵn, cài đặt hiện có không được sửa đổi. Giữ phím Ctrl để chọn nhiều lĩnh vực.',
  'content' => 'Nội dung trang',
  'contentheader' => 'Nội dung',
  'displayoptions' => 'Tùy chọn hiển thị có sẵn',
  'displayselect' => 'Hiển thị',
  'displayselectexplain' => 'Chọn kiểu hiển thị.',
  'legacyfiles' => 'Migration of old course file',
  'legacyfilesactive' => 'Hoạt động',
  'legacyfilesdone' => 'Hoàn thành',
  'modulename' => 'Trang',
  'modulename_help' => 'Một trang cho phép một trang web sẽ được hiển thị và chỉnh sửa trong khóa học.',
  'modulenameplural' => 'Trang',
  'neverseen' => 'Chưa từng có',
  'optionsheader' => 'Tuỳ chọn',
  'page-mod-page-x' => 'Bất kỳ module trang thông tin',
  'page:view' => 'Xem nội dung trang',
  'pluginadministration' => 'Quản lý module trang',
  'pluginname' => 'Trang',
  'popupheight' => 'Chiều cao Popup (bằng pixels)',
  'popupheightexplain' => 'Xác định chiều cao mặc định của cửa sổ popup.',
  'popupwidth' => 'Chiều rộng Popup (bằng pixels)',
  'popupwidthexplain' => 'Xác định chiều rộng mặc định của cửa sổ popup.',
  'printheading' => 'Tên trang hiển thị',
  'printheadingexplain' => 'Hiển thị tên trang trên nội dung?',
  'printintro' => 'Hiển thị mô tả trang',
  'printintroexplain' => 'Hiển thị mô tả trang trên nội dung?',
);