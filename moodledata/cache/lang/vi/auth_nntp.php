<?php $this->cache['vi']['auth_nntp'] = array (
  'auth_nntpdescription' => 'Phương pháp này sử dụng một máy chủ NNTP để kiểm tra khi nào tên đăng nhập và mật khẩu đưa ra là hợp lệ.',
  'auth_nntphost' => 'Các địa chỉ máy chủ NNTP. Sử dụng địa chỉ IP, không sử dụng tên DNS.',
  'auth_nntphost_key' => 'Host',
  'auth_nntpchangepasswordurl_key' => 'Password-change URL',
  'auth_nntpnotinstalled' => 'Cannot use NNTP authentication. The PHP IMAP module is not installed.',
  'auth_nntpport' => 'Cổng máy chủ (119 là phổ biến nhất )',
  'auth_nntpport_key' => 'Port',
  'pluginname' => 'Sử dụng một máy chủ NNTP',
);