<?php $this->cache['vi']['mod_tab'] = array (
  'ajouter' => 'Add a new tab display',
  'addtab' => 'Add a new tab',
  'changestyle' => 'Modify stylesheet',
  'clicktoopen' => 'Click to open',
  'content' => 'Tab content',
  'content_help' => 'You can embed a pdf file by following these instructions:
                            <br><ul>
                             <li>type the name you would like to use for the link.</li>
                             <li>Select the text and click on the inser/edit link button.</li>
                             <li>Click on the browse button at the end of the link url field</li>
                             <li>Upload your pdf file and then click insert</li>
                             </ul><br>
                             DO NOT ADD ANYTHING ELSE IN THE EDITOR.<br>
                             Save and display. The pdf file will be embedded.',
  'css' => 'Stylesheet',
  'displaymenu' => 'Display tab menu',
  'displaymenuagree' => 'Check if you would like to display menu',
  'externalurl' => 'Embed a Web page',
  'format' => 'Tab formatting',
  'menucss' => 'Modify menu stylesheet',
  'menuname' => 'Menu name',
  'modulename' => 'Tab display',
  'modulenameplural' => 'Tab displays',
  'moretabs' => 'Use more tabs',
  'name' => 'Name',
  'noformating' => 'No formatting',
  'order' => 'The order you would like this tab in',
  'pluginname' => 'Tab display',
  'pluginadministration' => 'Tab Administration',
  'tab' => 'Tab',
  'tabadministration' => 'Tab Administration',
  'tabcontent' => 'Tab content',
  'tabname' => 'Tab name',
  'taborder' => 'Tab display activity order within menu',
  'tab:view' => 'View Tabs',
  'updatethis' => 'Update this tab display',
);