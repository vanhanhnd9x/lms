<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://117.6.132.155/lmsvuf_completion
 *
 * @package    core
 * @subpackage completion
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activitiescompletednote'] = 'Khóa học được hoàn thành nếu các nội dung bài học trên được hoàn thành';
$string['completionicons_help'] = 'Tích vào các ô vuông để đánh dấu đã hoàn thành nội dung tương ứng,nếu ô vuông là đường kẻ không liền mạch, hệ thống sẽ tự động tích khi hoàn thành.Nếu ô vuông là đường kẻ liền mạch, bạn phải tự tích hoặc bỏ tích vào ô để đánh dấu đã hoàn thành, hoặc chưa hoàn thành.';
$string['courseaggregation'] = 'Điều kiện yêu cầu';
$string['coursecompletioncondition'] = 'Điều kiện: {$a}';
$string['coursesavailableexplaination'] = 'Điều kiện hoàn thành khóa học phải được chọn ở dưới danh sách sau đây';
$string['err_noactivities'] = 'Bạn cần tạo nội dung cho khóa học trước khi cài đặt cấu hình này';
$string['manualselfcompletionnote'] = 'Người dùng có thể tự đánh dấu hoàn thành khóa học';
$string['overallaggregation'] = 'Yêu cầu hoàn thành khóa học';
$string['overallaggregation_all'] = 'Khóa học hoàn thành khi TẤT CẢ điều kiện hoàn thành';
$string['overallaggregation_any'] = 'Khóa học hoàn thành khi một trong các điều kiện được hoàn thành';
$string['roleaggregation'] = 'Điều kiện yêu cầu';
$string['roleaggregation_all'] = 'Đánh dấu tất cả các vai trò khi đáp ứng được điều kiện';
$string['roleaggregation_any'] = 'Đánh dấu bất cứ vai trò nào khi đáp ứng được điều kiện';
