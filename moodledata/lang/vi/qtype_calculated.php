<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['calculatedsummary'] = 'Câu hỏi tính toán giống như câu hỏi số nhưng với những con số sử dụng lựa chọn ngẫu nhiên từ một tập hợp các bài kiểm tra khi được thực hiện.';
$string['calculated'] = 'Tính toán';
$string['sharedwildcards']='Chia sẻ thẻ';
$string['sharedwildcardname']='Chia sẻ thẻ ';
$string['nosharedwildcard'] = 'Không có thẻ được chia sẻ trong danh mục';
$string['updatecategory'] = 'Cập nhật vào danh mục';
$string['answerhdr'] = 'Câu trả lời';
$string['correctanswerformula'] = 'Công thức câu trả lời đúng';
$string['tolerance'] = 'Dung sai &plusmn;';
$string['correctanswershows'] = 'Hiển thị câu trả lời đúng';
$string['correctanswershowsformat'] = 'Định dạng';
$string['addmoreunitblanks'] = 'Thêm câu trả lời';
?>
