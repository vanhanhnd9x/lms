<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'message', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   message
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['savemysettings'] = 'Lưu cài đặt của tôi';
$string['settings'] = 'Cài đặt';
$string['unreadnewmessage'] = 'Tin nhắn mới từ {$a}';
$string['unreadmessages'] = 'Tin nhắn chưa đọc ({$a})';
$string['unreadnewmessages'] = 'Tin nhắn mới ({$a})';
$string['unreadnewnotification'] = 'Thông báo mới';
$string['unreadnewnotifications'] = 'Thông báo mới ({$a})';
$string['userisblockingyou'] = 'Người dùng này đã chặn tin nhắn của bạn gửi cho họ';
$string['userisblockingyounoncontact'] = 'Người sử dụng này chỉ chấp nhận các tin nhắn từ những người được liệt kê như địa chỉ liên lạc, và bạn không có trên danh sách liên lạc.';
$string['userssearchresults'] = 'Tìm thấy người dùng: {$a}';
$string['discussion'] = 'Thảo luận';
$string['messages'] = 'Tin nhắn';

