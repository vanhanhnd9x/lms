<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'glossary', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   glossary
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['currentglossary'] = 'Bảng từ hiện hành';
$string['filtername'] = 'Bảng từ tự động liên kết';
$string['glossarytype'] = 'Kiểu bảng từ';
$string['mainglossary'] = 'Bảng từ chính';
$string['modulename'] = 'Bảng từ';
$string['newentries'] = 'Các mục từ mới';
$string['newglossary'] = 'Bảng từ mới';
$string['newglossarycreated'] = 'Bảng từ mới đã được tạo';
$string['pluginadministration'] = 'Quản lý bảng từ';
$string['pluginname'] = 'Bảng từ';
$string['secondaryglossary'] = 'Bảng từ phụ';
$string['usedynalink'] = 'Tự động liên kết các mục từ';
$string['modulename_help'] = 'Các mô-đun hoạt động chú giải thuật ngữ cho phép người tham gia có thể tạo ra và duy trì một danh sách các định nghĩa, như một từ điển, hoặc để thu thập và tổ chức các tài nguyên hay thông tin.

Một giáo viên có thể cho phép các tập tin được gắn liền với các mục chú giải. Hình ảnh đính kèm được hiển thị trong từng mục. Mục có thể được tìm kiếm hoặc duyệt theo thứ tự abc hoặc theo nhóm, ngày hoặc tác giả. Mục có thể được phê duyệt mặc định hoặc yêu cầu chính của một giáo viên trước khi họ có thể xem được tất cả mọi người.

Nếu các bộ lọc tự động liên kết các thuật ngữ được kích hoạt, các mục sẽ được tự động liên kết hợp từ và / hoặc cụm từ khái niệm xuất hiện trong khóa học.

Một giáo viên có thể cho phép bình luận về mục. Mục cũng có thể được đánh giá bởi các giáo viên hoặc sinh viên (đánh giá ngang hàng). Xếp hạng có thể được tổng hợp để tạo thành một lớp cuối cùng được ghi trong sổ điểm

Bảng thuật ngữ có nhiều ứng dụng, chẳng hạn như

* Một ngân hàng hợp tác của các điều khoản quan trọng
* Nhận được để biết bạn không gian nơi học sinh mới thêm tên của họ và chi tiết cá nhân
* Mẹo tiện dụng thực hành tốt nhất trong một chủ đề cụ thể
* Một khu vực chia sẻ các video hữu ích, hình ảnh hoặc tập tin âm thanh
* Một nguồn lực sửa đổi các sự kiện cần nhớ';
