<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['acceptederror'] = 'Chấp nhận lỗi';
$string['addingnumerical'] = 'Thêm câu hỏi Số học';
$string['addmoreanswerblanks'] = 'Blanks for {no} More Answers';
$string['addmoreunitblanks'] = 'Blanks for {no} More Units';
$string['answermustbenumberorstar'] = 'Câu trả lời phải là một số, ví dụ -1.234 hay 3e8, hay \'*\'.';
$string['answerno'] = 'Câu trả lời {$a}';
$string['decfractionofquestiongrade'] = 'Như một phần (0-1) của đánh giá câu hỏi';
$string['decfractionofresponsegrade'] = 'Như một phần (0-1) của đánh giá câu trả lời';
$string['decimalformat'] = 'Số thập phân';
$string['editableunittext'] = 'Các yếu tố đầu vào văn bản';
$string['editingnumerical'] = 'Chỉnh sửa câu hỏi dạng số';
$string['errornomultiplier'] = 'Bạn phải xác định một số nhân cho đơn vị này.';
$string['errorrepeatedunit'] = 'Bạn không thể có hai đơn vị có cùng tên.';
$string['geometric'] = 'Hình học';
$string['invalidnumber'] = 'Bạn phải nhập một số hợp lệ.';
$string['invalidnumbernounit'] = 'Bạn phải nhập một số hợp lệ. Không bao gồm đơn vị trong trả lời của bạn.';
$string['invalidnumericanswer'] = 'Một trong những câu trả lời mà bạn đã nhập không phải là một số hợp lệ.';
$string['invalidnumerictolerance'] = 'Một trong những dung sai mà bạn đã nhập không phải là một số hợp lệ.';
$string['leftexample'] = 'Bên trái, ví dụ $1.00 or Â£1.00';
$string['multiplier'] = 'Nhân';
$string['noneditableunittext'] = 'Không chỉnh sửa văn bản của đơn vị số 1';
$string['nonvalidcharactersinnumber'] = 'Không có ký tự hợp lệ trong số';
$string['notenoughanswers'] = 'Bạn phải nhập ít nhất một câu trả lời.';
$string['nounitdisplay'] = 'Không có đơn vị chấm điểm';
$string['numerical'] = 'Số';
$string['numerical_help'] = 'Từ quan điểm của học viên, một câu hỏi số trông giống như một câu hỏi trả lời ngắn. Sự khác biệt là câu trả lời số được phép chấp nhận có một lỗi. This allows a fixed range of answers to be evaluated as one answer. For example, if the answer is 10 with an accepted error of 2, then any number between 8 and 12 will be accepted as correct. ';
$string['numerical_link'] = 'question/type/numerical';
$string['numericalsummary'] = 'Cho phép một phản ứng số, có thể với các đơn vị, đó là phân loại bằng cách so sánh với mô hình câu trả lời khác nhau, có thể có dung sai.';
$string['numericalmultiplier'] = 'Nhân';
$string['numericalmultiplier_help'] = 'Số nhân là yếu tố mà các đáp án số chính xác sẽ được nhân.

Đơn vị đầu tiên (Đơn vị 1) có một số nhân mặc định của 1. Vì vậy, nếu các phản ứng số chính xác là 5500 và bạn thiết lập W là đơn vị trong đó có 1 số nhân như mặc định, câu trả lời đúng là 5500 W.

Nếu bạn thêm các đơn vị kW với một số nhân của 0.001, điều này sẽ thêm một đáp án chính xác là 5,5 kW. Điều này có nghĩa rằng các câu trả lời 5500W hoặc 5.5kW sẽ được đánh dấu chính xác.

Lưu ý rằng các lỗi được chấp nhận cũng được nhân, do đó, một lỗi cho phép của 100W sẽ trở thành một lỗi của 0.1kW.';
$string['manynumerical'] = 'Đơn vị là tùy chọn. Nếu một đơn vị được nhập vào, nó được sử dụng để chuyển đổi các đáp án để đơn vị trước khi chấm điểm.';
$string['nominal'] = 'định danh';
$string['onlynumerical'] = 'Đơn vị không được sử dụng ở tất cả. Chỉ số giá trị được chấm điểm.';
$string['oneunitshown'] = 'Một đơn vị được tự động hiển thị bên cạnh hộp câu trả lời.';
$string['pleaseenterananswer'] = 'Xin vui lòng nhập một câu trả lời.';
$string['pleaseenteranswerwithoutthousandssep'] = 'Xin vui lòng điền câu trả lời của bạn mà không sử dụng các dấu phân cách nghìn ({$a}).';
$string['relative'] = 'Tương đối';
$string['rightexample'] = 'Bên phải, ví dụ 1.00cm hay 1.00km';
$string['selectunits'] = 'Chọn đơn vị';
$string['selectunit'] = 'Chọn 1 đơn vị';
$string['studentunitanswer'] = 'Đơn vị là đầu vào sử dụng';
$string['tolerancetype'] = 'Kiểu khoan dung';
$string['unit'] = 'Đơn vị';
$string['unitappliedpenalty'] = 'Các nhãn hiệu này bao gồm một hình phạt của {$a} cho đơn vị sai.';
$string['unitchoice'] = 'Lựa chọn nhiều';
$string['unitedit'] = 'Chỉnh sửa đơn vị';
$string['unitgraded'] = 'Đơn vị phải được đưa ra, và sẽ được chấm điểm.';
$string['unithandling'] = 'Đơn vị xử lý';
$string['unithdr'] = 'Đơn vị {$a}';
$string['unitincorrect'] = 'Bạn không cung cấp cho các đơn vị chính xác.';
$string['unitmandatory'] = 'Bắt buộc';
$string['unitmandatory_help'] = '

* Đáp án sẽ được chấm điểm bằng cách sử dụng đơn vị bằng văn bản.

* Hình phạt sẽ được áp dụng nếu trường đơn vị bị bỏ trống

';
$string['unitnotselected'] = 'Bạn phải chọn một đơn vị.';
$string['unitonerequired'] = 'Bạn phải nhập ít nhất một đơn vị';
$string['unitoptional'] = 'Tuỳ chọn đơn vị';
$string['unitoptional_help'] = '
* Nếu trường đơn vị là trắng, đáp án sẽ được chấm điểm sử dụng đơn vị.

* Nếu đơn vị là khó viết hoặc không biết, đáp án sẽ được coi là không hợp lệ.
';
$string['unitpenalty'] = 'Phạt đơn vị';
$string['unitpenalty_help'] = 'Hình phạt được áp dụng nếu

* tên đơn vị được nhập vào sai, hay
* một đơn vị được nhập vào ô giá trị';
$string['unitposition'] = 'Về đơn vị';
$string['unitselect'] = 'một trình đơn thả xuống';
$string['validnumberformats'] = 'Định dạng số hợp lệ';
$string['validnumberformats_help'] = '
* số đúng 13500.67, 13 500.67, 13500,67 hoặc 13 500,67

* nếu bạn sử dụng, như tách số hàng nghìn đưa về số thập phân. như trong
 13,500.67 : 13,500.

* Cho hình thức số mũ, gọi 1.350067 * 10<sup>4</sup>, dùng
 1.350067 E4 : 1.350067 E04 ';

$string['validnumbers'] = '13500.67, 13 500.67, 13,500.67, 13500,67, 13 500,67, 1.350067 E4 hoặc 1.350067 E04';
?>
