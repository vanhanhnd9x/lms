<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['addingmultichoice'] = 'Thêm câu hỏi nhiều lựa chọn';
$string['addmorechoiceblanks'] = 'Thêm lựa chọn';
$string['answerhowmany'] = 'Một hoặc nhiều câu trả lời?';
$string['answernumbering'] = 'Đánh số các sự lựa chọn?';
$string['answernumbering123'] = '1., 2., 3., ...';
$string['answernumberingabc'] = 'a., b., c., ...';
$string['answernumberingABCD'] = 'A., B., C., ...';
$string['answernumberingiii'] = 'i., ii., iii., ...';
$string['answernumberingIIII'] = 'I., II., III., ...';
$string['answernumberingnone'] = 'không đánh số';
$string['answersingleno'] = 'Cho phép nhiều câu trả lời';
$string['answersingleyes'] = 'Một câu trả lời duy nhất';
$string['choiceno'] = 'Chọn {$a}';
$string['choices'] = 'Lựa chọn có sẵn';
$string['clozeaid'] = 'Nhập từ bị thiếu';
$string['correctansweris'] = 'Câu trả lời đúng là: {$a}.';
$string['correctfeedback'] = 'Đối với bất kỳ phản ứng chính xác';
$string['editingmultichoice'] = 'Chỉnh sửa câu hỏi nhiều lựa chọn';
$string['errfractionsaddwrong'] = 'Đáp án bạn chọn không lên tới 100%<br />Thay vào đó, đáp án lên đến {$a}%';
$string['errfractionsnomax'] = 'Một trong những lựa chọn là 100%, để nó là <br />đáp án chính xác của câu hỏi.';
$string['feedback'] = 'Thông tin phản hồi';
$string['fillouttwochoices'] = 'Bạn phải điền ít nhất hai sự lựa chọn. Không được để trống lựa chọn';
$string['fractionsaddwrong'] = 'Các đáp án bạn chọn không đúng 100%<br />Thay vào đó, chọn tới {$a}%<br />Bạn có muốn quay trở lại và sửa chữa câu hỏi này?';
$string['fractionsnomax'] = '1 trong những lựa chọn nên là 100%, Nó sẽ là <br />đáp án của câu hỏi.<br />Bạn có muốn quay trở lại và sửa chữa câu hỏi này?';
$string['incorrectfeedback'] = 'Đối với bất kỳ phản ứng không chính xác';
$string['multichoice'] = 'Nhiều sự lựa chọn';
$string['multichoice_help'] = 'Để trả lời với một câu hỏi (mà có thể bao gồm hình ảnh) người trả lời chọn từ nhiều câu trả lời. Có hai loại câu hỏi trắc nghiệm - một câu trả lời và nhiều câu trả lời.';
$string['multichoice_link'] = 'question/type/multichoice';
$string['multichoicesummary'] = 'Cho phép lựa chọn một câu trả lời một hoặc nhiều từ một danh sách được xác định trước.';
$string['notenoughanswers'] = 'Đây là loại câu hỏi đòi hỏi phải có ít nhất {$a} lựa chọn';
$string['overallcorrectfeedback'] = 'Thông tin phản hồi cho bất kỳ phản ứng chính xác';
$string['overallfeedback'] = 'thông tin phản hồi tổng thể';
$string['overallincorrectfeedback'] = 'Thông tin phản hồi cho bất kỳ phản ứng không chính xác';
$string['overallpartiallycorrectfeedback'] = 'Thông tin phản hồi cho bất kỳ phản ứng một phần đúng';
$string['partiallycorrectfeedback'] = 'Đối với bất kỳ phản ứng một phần đúng';
$string['pleaseselectananswer'] = 'Hãy chọn một câu trả lời.';
$string['pleaseselectatleastoneanswer'] = 'Hãy chọn ít nhất một câu trả lời.';
$string['selectmulti'] = 'Chọn một hoặc nhiều:';
$string['selectone'] = 'Chọn một:';
$string['shuffleanswers'] = 'Lựa chọn ngẫu nhiên?';
$string['shuffleanswers_help'] = 'Nếu được kích hoạt, thứ tự của các câu trả lời được xáo trộn ngẫu nhiên cho mỗi bài làm, với điều kiện là "Câu hỏi ngẫu nhiên" trong các thiết lập hoạt động cũng được kích hoạt.';
$string['singleanswer'] = 'Chọn một câu trả lời.';
$string['toomanyselected'] = 'Bạn đã chọn quá nhiều lựa chọn.';
?>
