<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'gradereport_history', language 'en'
 *
 * @package    gradereport_history
 * @copyright  2013 NetSpot Pty Ltd (https://www.netspot.com.au)
 * @author     Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allgradeitems'] = 'Tất cả các mục lớp';
$string['allgraders'] = 'Tất cả lớp';
$string['datefrom'] = 'Từ ngày';
$string['dateto'] = 'Đến ngày';
$string['datetime'] = 'Ngày và giờ';
$string['deleteditemid'] = 'Xoá mục với id {$a}';
$string['errajaxsearch'] = 'Có lỗi khi tìm kiếm người dùng';
$string['eventgradereportviewed'] = 'Báo cáo lịch sử lớp đã xem';
$string['excluded'] = 'Loại trừ khỏi tính toán';
$string['exportfilename'] = 'grade_history';
$string['foundoneuser'] = 'Tìm thấy 1 người dùng';
$string['foundnusers'] = 'Tìm thấy {$a} người dùng';
$string['feedbacktext'] = 'Phản hồi';
$string['finishselectingusers'] = 'Kết thúc lựa chọn người dùng';
$string['gradenew'] = 'Điều chỉnh lớp';
$string['gradeold'] = 'Lớp ban đầu';
$string['grader'] = 'Học viên lớp';
$string['history:view'] = 'Xem lịch sử lớp';
$string['historyperpage'] = 'Lịch sử mỗi trang';
$string['historyperpage_help'] = 'This setting determines the number of history entries displayed per page in the history report.';
$string['loadmoreusers'] = 'Tải thêm người dùng...';
$string['pluginname'] = 'Lịch sử lớp';
$string['preferences'] = 'Tham chiếu lịch sử lớp';
$string['revisedonly'] = 'Chỉ lớp điều chỉnh';
$string['revisedonly_help'] = 'Chỉ hiển thị các lớp đã được sửa đổi.

Điều này có nghĩa chỉ liệt kê lớp thay đổi.';
$string['selectuser'] = 'Lựa chọn người dùng';
$string['selectusers'] = 'Lựa chọn người dùng';
$string['selectedusers'] = 'Lựa chọn người dùng';
$string['source'] = 'Nguồn';
$string['useractivitygrade'] = '{$a} lớp';
$string['useractivityfeedback'] = '{$a} phản hồi';
