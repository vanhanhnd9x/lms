<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();
$string['addingmultianswer'] = 'Thêm một câu hỏi (Cloze) trả lời nhúng';
$string['confirmquestionsaveasedited'] = 'Tôi xác nhận rằng tôi muốn các câu hỏi được lưu khi chỉnh sửa nội dung';
$string['confirmsave'] = 'Xác nhận sau đó lưu {$a}';
$string['correctanswer'] = 'Câu trả lời đúng';
$string['correctanswerandfeedback'] = 'Câu trả lời và phản hồi chính xác';
$string['decodeverifyquestiontext'] = 'Giải mã và kiểm tra nội dung câu hỏi';
$string['editingmultianswer'] = 'Chỉnh sửa một câu hỏi (Cloze) trả lời nhúng';
$string['layout'] = 'Bố trí';
$string['layouthorizontal'] = 'Nút chọn hàng ngang';
$string['layoutselectinline'] = 'Menu trôi xuống trong nội dung';
$string['layoutundefined'] = 'Không bố trí';
$string['layoutvertical'] = 'Nút chọn hàng dọc';
$string['multianswer'] = 'Câu trả lời nhúng (Cloze)';
$string['multianswersummary'] = 'Câu hỏi của loại này là rất linh hoạt, nhưng chỉ có thể được tạo ra bằng cách nhập văn bản có chứa đoạn mã đặc biệt để tạo ra nhúng nhiều lựa chọn, câu trả lời ngắn và các câu hỏi số.';
$string['multianswer_help'] = 'Câu hỏi câu trả lời nhúng (Cloze) bao gồm một đoạn văn bản với những câu hỏi như nhiều lựa chọn và câu trả lời ngắn nhúng bên trong nó.';
$string['multianswer_link'] = 'question/type/multianswer';
$string['nooptionsforsubquestion'] = 'Không thể có được lựa chọn cho phần câu hỏi # {$a->sub} (question->id={$a->id})';
$string['noquestions'] = 'Câu hỏi Cloze(nhiều câu trả lời) "<strong>{$a}</strong>" không chứa bất kỳ câu hỏi';
$string['qtypenotrecognized'] = 'kiểu câu hỏi {$a} không được công nhận';
$string['questionnadded'] = 'câu hỏi thêm';
$string['questiondefinition'] = 'Định nghĩa câu hỏi';
$string['questiondeleted'] = 'Câu hỏi đã bị xóa';
$string['questioninquiz'] = '

<ul>
  <li>thêm hoặc xóa các câu hỏi, </li>
  <li>thay đổi thứ tự các câu hỏi trong văn bản,</li>
  <li>thay đổi loại câu hỏi của (số, trả lời ngắn, nhiều lựa chọn). </li></ul>
';
$string['questionsless'] = '{$a} câu hỏi (s) ít hơn so với các câu hỏi nhiều câu trả lời được lưu trữ trong cơ sở dữ liệu';
$string['questionsmissing'] = 'Không có câu hỏi hợp lệ, tạo ra ít nhất một câu hỏi';
$string['questionsmore'] = '{$a} câu hỏi (s) nhiều hơn so với các câu hỏi nhiều câu trả lời được lưu trữ trong cơ sở dữ liệu';
$string['questionnotfound'] = 'Không thể tìm thấy câu hỏi của phần #{$a}';
$string['questionsaveasedited'] = 'Câu hỏi đặt ra sẽ được lưu khi thay đổi nội dung';
$string['questiontypechanged'] = 'Kiểu câu hỏi đã thay đổi';
$string['questiontypechangedcomment'] = 'Ít nhất một loại câu hỏi đã được thay đổi.<br \>Bạn đã thêm, xóa hoặc di chuyển một câu hỏi?<br \>Nhìn về phía trước.';
$string['questionusedinquiz'] = 'Câu hỏi này được sử dụng trong {$a->nb_of_quiz} quiz(s), Tổng số trả lời : {$a->nb_of_attempts} ';
$string['storedqtype'] = 'Lưu trữ câu hỏi loại {$a}';
$string['subqresponse'] = 'Phần {$a->i}: {$a->response}';
$string['unknownquestiontypeofsubquestion'] = 'Không biết loại câu hỏi: {$a->type} của câu hỏi phần # {$a->sub}';
$string['warningquestionmodified'] = '<b>CẢNH BÁO</b>';
$string['youshouldnot'] = '<b>KHÔNG PHẢI BẠN</b>';
?>
