<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'role', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   role
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['calendar:manageentries'] = 'Quản lý tất cả các mục của lịch';
$string['currentrole'] = 'Vai trò hiện tại';
$string['calendar:managegroupentries'] = 'Quản lý tất cả các mục của lịch nhóm';
$string['calendar:manageownentries'] = 'Quản lý tất cả các mục của bản thân';
$string['checkglobalpermissions'] = 'Kiểm tra quyền hệ thống';
$string['checkpermissions'] = 'Kiểm tra quyền';
$string['permission'] = 'Quyền';
$string['permissions'] = 'Các quyền';
$string['rolefullname'] = 'Tên';
$string['roleshortname'] = 'Tên tắt';
$string['localroles'] = 'Gán vai trò';
$string['userswiththisrole'] = 'Người dùng với vai trò';
$string['chooseroletoassign'] = 'Vui lòng chọn một vai trò để gán';
$string['selectauser'] = 'Lựa chọn người dùng';
$string['assignrole'] = 'Gán vai trò';
$string['assignroles'] = 'Gán vai trò';
$string['assignrolesin'] = 'Gán vai trò trong {$a}';
$string['permissionsincontext'] = 'Phân quyền trong {$a}';
$string['advancedoverride'] = 'Ghi đè vai trò nâng cao';
$string['checkpermissionsin'] = 'Kiểm tra phân quyền trong {$a}';
$string['showthisuserspermissions'] = 'Hiển thị quyền của người dùng này';
$string['manageroles'] = 'Quản lý vai trò';
$string['addrole'] = 'Thêm một vai trò';
$string['allowassign'] = 'Cho phép gán vai trò';
$string['allowoverride'] = 'Cho phép ghi đè vai trò';
$string['allowswitch'] = 'Cho phép chuyển vai trò';
$string['defineroles'] = 'Định nghĩa vai trò';
$string['assignglobalroles'] = 'Gán vai trò trong hệ thống';
$string['globalroleswarning'] = 'CẢNH BÁO! Bất kỳ vai trò bạn gán từ trang này sẽ áp dụng đối với người sử dụng trong suốt toàn bộ hệ thống, bao gồm cả các trang trước và tất cả các khóa học.';
$string['restore:restorecourse'] = 'Khôi phục khoá học';
$string['site:restore'] = 'Khôi phục khoá học';