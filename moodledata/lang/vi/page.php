<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'page', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   page
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configdisplayoptions'] = 'Chọn tất cả các tùy chọn mà có sẵn, cài đặt hiện có không được sửa đổi. Giữ phím Ctrl để chọn nhiều lĩnh vực.';
$string['content'] = 'Nội dung trang';
$string['contentheader'] = 'Nội dung';
$string['displayoptions'] = 'Tùy chọn hiển thị có sẵn';
$string['displayselect'] = 'Hiển thị';
$string['displayselectexplain'] = 'Chọn kiểu hiển thị.';
$string['legacyfiles'] = 'Migration of old course file';
$string['legacyfilesactive'] = 'Hoạt động';
$string['legacyfilesdone'] = 'Hoàn thành';
$string['modulename'] = 'Trang';
$string['modulename_help'] = 'Một trang cho phép một trang web sẽ được hiển thị và chỉnh sửa trong khóa học.';
$string['modulenameplural'] = 'Trang';
$string['neverseen'] = 'Chưa từng có';
$string['optionsheader'] = 'Tuỳ chọn';
$string['page-mod-page-x'] = 'Bất kỳ module trang thông tin';
$string['page:view'] = 'Xem nội dung trang';
$string['pluginadministration'] = 'Quản lý module trang';
$string['pluginname'] = 'Trang';
$string['popupheight'] = 'Chiều cao Popup (bằng pixels)';
$string['popupheightexplain'] = 'Xác định chiều cao mặc định của cửa sổ popup.';
$string['popupwidth'] = 'Chiều rộng Popup (bằng pixels)';
$string['popupwidthexplain'] = 'Xác định chiều rộng mặc định của cửa sổ popup.';
$string['printheading'] = 'Tên trang hiển thị';
$string['printheadingexplain'] = 'Hiển thị tên trang trên nội dung?';
$string['printintro'] = 'Hiển thị mô tả trang';
$string['printintroexplain'] = 'Hiển thị mô tả trang trên nội dung?';
