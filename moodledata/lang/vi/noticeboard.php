<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$string['ajouter'] = 'Thêm một thông báo mới';
$string['addnoticeboard'] = 'Thêm một thông báo mới';
$string['changestyle'] = 'Sửa trang';
$string['clicktoopen'] = 'Nhấn để mở';
$string['content'] = 'Nội dung thông báo';
$string['content_help'] = 'Bạn có thể nhúng một tập tin pdf bằng cách làm theo các hướng dẫn sau: 
                            <br><ul>
                             <li>Gõ tên mà bạn muốn sử dụng cho các liên kết.</li>
                             <li>Chọn văn bản và nhấn vào nút liên kết inser/edit.</li>
                             <li>Nhấp vào nút duyệt ở cuối các trường liên kết url</li>
                             <li>Tải lên tập tin pdf của bạn và sau đó nhấp chèn </li>
                             </ul><br>
                             KHÔNG THÊM BẤT CỨ ĐIỀU GÌ KHÁC TRONG EDITOR.<br>
                              Lưu và hiển thị. Tập tin pdf sẽ được nhúng.';
$string['css'] = 'Stylesheet';
$string['displaymenu'] = 'Hiển thị menu thông báo';
$string['displaymenuagree'] = 'Kiểm tra nếu bạn muốn hiển thị menu';
$string['externalurl'] = 'Nhúng một trang Web';
$string['format'] = 'định dạng bảng thông báo';
$string['menucss'] = 'Sửa đổi kiểu menu';
$string['menuname'] = 'Tên menu';
$string['modulename'] = 'Hiển thị thông báo';
$string['modulenameplural'] = 'Hiển thị bảng thông báo';
$string['morenoticeboards'] = 'Sử dụng nhiều bảng thông báo';
$string['name'] = 'Tên';
$string['noformating'] = 'Không có định dạng';
$string['order'] = 'Thứ tự bạn muốn bảng thông báo';
$string['pluginname'] = 'Hiển thị thông báo';
$string['pluginadministration'] = 'Quản lý bảng thông báo';
$string['noticeboard'] = 'Thông báo';
$string['noticeboardadministration'] = 'Quản lý bảng thông báo';
$string['noticeboardcontent'] = 'Nội dung thông báo';
$string['noticeboardname'] = 'Tên thông báo';
$string['noticeboardorder'] = 'Hiển thị thông báo dạng menu theo thứ tự';
$string['noticeboard:view'] = 'Xem thông báo';
$string['updatethis'] = 'Cập nhật hiển thị bảng thông báo';
?>
