<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['configselectmanualquestions'] = 'Các loại câu hỏi ngẫu nhiên có thể chọn một câu hỏi tự chấm điểm khi nó được đưa ra lựa chọn ngẫu nhiên của một câu hỏi từ một danh mục?';
$string['editingrandom'] = 'Chỉnh sửa câu hỏi ngẫu nhiên';
$string['includingsubcategories'] = 'Có thêm danh mục cấp dưới';
$string['random'] = 'Ngẫu nhiên';
$string['random_help'] = 'Một câu hỏi ngẫu nhiên không phải là một loại câu hỏi như vậy, nhưng là một cách để chèn một câu hỏi ngẫu nhiên được lựa chọn từ một danh mục nào đó vào một bài kiểm tra.';
$string['randomqname'] = 'Ngẫu nhiên ({$a})';
$string['randomqplusname'] = 'Ngẫu nhiên ({$a} và danh mục cấp dưới)';
$string['selectedby'] = '{$a->questionname} lựa chọn {$a->randomname}';
$string['selectmanualquestions'] = 'Câu hỏi ngẫu nhiên có thể sử dụng các câu hỏi chấm điểm bằng tay';
?>
