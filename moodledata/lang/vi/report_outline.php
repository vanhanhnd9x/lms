<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings
 *
 * @package    report
 * @subpackage outline
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['eventactivityreportviewed'] = 'Báo cáo hoạt động đã xem';
$string['eventoutlinereportviewed'] = 'Báo cáo sơ lược đã xem';
$string['neverseen'] = 'Chưa từng thấy';
$string['nologreaderenabled'] = 'Kịch hoạt không đọc log';
$string['outline:view'] = 'Xem báo cáo hoạt động';
$string['page-report-outline-x'] = 'Báo cáo sơ lược bất kỳ';
$string['page-report-outline-index'] = 'Báo cáo sơ lượng khoá học';
$string['page-report-outline-user'] = 'Báo cáo sơ lược người dùng khoá học';
$string['pluginname'] = 'Báo cáo hoạt động';
