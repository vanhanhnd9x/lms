<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'folder', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    mod
 * @subpackage folder
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['contentheader'] = 'Nội dung';
$string['folder:managefiles'] = 'Quản lý các file trong module thư mục ';
$string['folder:view'] = 'Xem nội dung thư mục';
$string['foldercontent'] = 'Tập tin và thư mục con';
$string['modulename'] = 'Thư mục';
$string['modulenameplural'] = 'Thư mục';
$string['neverseen'] = 'Chưa từng có';
$string['page-mod-folder-x'] = 'Bất kỳ trang module thư mục';
$string['page-mod-folder-view'] = 'Trang chính module thư mục';
$string['pluginadministration'] = 'Quản lý thư mục';
$string['pluginname'] = 'Thư mục';
$string['display'] = 'Hiển thị nội dung thư mục';
$string['displaypage'] = 'Trên một trang riêng biệt';
$string['displayinline'] = 'Trên cùng 1 trang';
$string['showexpanded'] = 'Hiển thị các thư mục con mở rộng';