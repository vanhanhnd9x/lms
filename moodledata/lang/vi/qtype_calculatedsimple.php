<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

defined('MOODLE_INTERNAL') || die();

$string['calculatedsimple'] = 'Tính toán đơn giản';
$string['calculatedsimplesummary'] = 'Một phiên bản đơn giản của câu hỏi tính toán mà cũng giống như câu hỏi số nhưng với những con số sử dụng lựa chọn ngẫu nhiên từ một tập hợp các bài kiểm tra khi được thực hiện.';
$string['addingcalculatedsimple'] = 'Thêm một câu hỏi tính toán đơn giản';
$string['findwildcards'] = 'Tìm biến {x} .. hiện diện trong các công thức câu trả lời đúng';
$string['wildcardrole'] = 'Biến <strong>{x..}</strong> sẽ được thay thế bằng một giá trị số từ các giá trị được tạo ra';
$string['atleastonewildcard'] = 'Phải có ít nhất một biến <strong>{x..}</strong> hiện diện trong các công thức câu trả lời đúng';
?>
