<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$string['preferencespage'] = 'Tuỳ chọn chỉ cho trang này';
$string['show'] = 'Hiển thị / Tải về';
$string['showattempts'] = 'Chỉ hiển thị / Số lần tải về';
$string['optonlygradedattempts'] = 'chấm điểm cho từng người dùng ({$a})';
$string['optonlyregradedattempts'] = 'đã được chấm điểm lại/ được đánh dấu chấm lại';
$string['preferencesuser'] = 'Tuỳ chọn của bạn cho báo cáo này';
$string['pagesize'] = 'Số trang';
$string['showdetailedmarks'] = 'Hiển thị / tải về điểm của mỗi câu hỏi';
$string['preferencessave'] = 'Hiển thị báo cáo';
$string['regradeall'] = 'Đánh giá lại tất cả';
$string['showinggradedandungraded'] = 'Hiển thị số lần làm bài và chưa làm bài của từng học viên. Các lượt trả lời của mỗi học viên sẽ được tô sáng lên. Phương thức lấy điểm của bài kiểm tra này là {$a}.';
$string['regradealldry'] = 'Đánh giá lại lần lượt';
$string['overviewreportgraph'] = 'Tổng số học viên trong phạm vi đạt đánh giá';
$string['optallattempts'] = 'Tất cả số lần thực hiện';