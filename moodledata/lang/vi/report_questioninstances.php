<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * lang strings
 *
 * @package    report
 * @subpackage questioninstances
 * @copyright  2008 Tim Hunt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['editquestionshere'] = 'Chỉnh sửa câu hỏi trong trường hợp này';
$string['eventreportviewed'] = 'Báo cáo đã hiển thị';
$string['getreport'] = 'Báo cáo';
$string['hiddenquestions'] = 'Ẩn';
$string['intro'] = 'Báo cáo này liệt kê tất cả các bối cảnh trong hệ thống, nơi có những câu hỏi của một loại cụ thể.';
$string['pluginname'] = 'Câu hỏi';
$string['questioninstances:view'] = 'Xem báo cáo câu hỏi';
$string['reportforallqtypes'] = 'Báo cáo cho tất cả dạng câu hỏi';
$string['reportformissingqtypes'] = 'Report for question of unknown types';
$string['reportforqtype'] = 'Báo cáo cho dạng câu hỏi \'{$a}\'';
$string['reportsettings'] = 'Thiết lập báo cáo';
$string['totalquestions'] = 'Tổng';
$string['visiblequestions'] = 'Hiển thị';
