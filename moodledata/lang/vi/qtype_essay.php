<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

defined('MOODLE_INTERNAL') || die();

$string['essay'] = 'Bài luận';
$string['essaysummary'] = 'Cho phép trả lời bằng một vài câu hay đoạn văn. Điều này sau đó phải được đánh giá bằng tay.';
$string['addingessay'] = 'Thêm một câu hỏi luận';
$string['addingessay_link'] = 'question/type/essay';
$string['allowattachments'] = 'Cho phép đính kèm tệp tin';
$string['editingessay'] = 'Chỉnh sửa một câu hỏi tiểu luận';
$string['essay_help'] = 'Để đáp ứng với một câu hỏi (có thể bao gồm một hình ảnh) học viên viết một câu trả lời của một hoặc hai đoạn văn. Các câu hỏi tiểu luận sẽ không được chấm điểm cho đến khi nó đã được xem xét bởi một giáo viên và chấm điểm.';
$string['formateditor'] = 'Trình soạn thảo HTML';
$string['formateditorfilepicker'] = 'Trình biên tập HTML với bảng chọn';
$string['formatmonospaced'] = 'Văn bản thuần, định dạng đơn';
$string['formatplain'] = 'Văn bản thuần';
$string['graderinfo'] = 'Thông tin cho học viên';
$string['nlines'] = '{$a} dòng';
$string['responsefieldlines'] = 'Kích thước hộp nhập';
$string['responseformat'] = 'Yêu cầu định dạng';
?>
