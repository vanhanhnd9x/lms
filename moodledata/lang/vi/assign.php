<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assign', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   assign
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['assign:grade'] = 'Chấm điểm bài tập';
$string['assignmentname'] = 'Tên bài tập';
$string['assign:submit'] = 'Gửi bài';
$string['activityoverview'] = 'Bạn có bài tập được giao';
$string['assign:view'] = 'Xem bài nộp';
$string['comment'] = 'Nhận xét';
$string['configshowrecentsubmissions'] = 'Mọi người có thể xem các thông báo nộp bài trong các báo cáo những hoạt động gần nhất.';
$string['duedate'] = 'Hạn chót';
$string['duedateno'] = 'Vô thời hạn';
$string['editsubmission'] = 'Sửa bài nộp';
$string['feedback'] = 'Nhận xét';
$string['filtersubmitted'] = 'Bài đã nộp';
$string['mysubmission'] = 'Bài gửi: ';
$string['notsubmittedyet'] = 'Chưa gửi bài';
$string['graded'] = 'Đã cho điểm';
$string['gradersubmissionupdatedhtml'] = 'Học viên {$a->username} đã cập nhật bài nộp cho bài tập \'{$a->assignment}\'.

Bài nộp đó có thể xem <a href="{$a->url}">trên website</a>';
$string['gradersubmissionupdatedtext'] = 'Học viên {$a->username} đã cập nhật bài nộp cho bài tập \'{$a->assignment}\'.

Bài nộp đó có thể xem tại đây:

{$a->url}';
$string['modulenameplural'] = 'Bài tập';
$string['newsubmissions'] = 'Bài tập đã nộp';
$string['introattachments'] = 'Thêm tệp tin';
$string['availability'] = 'Hiệu lực';
$string['allowsubmissionsfromdate'] = 'Có hiệu lực từ';
$string['cutoffdate'] = 'Ngày rút gọn';
$string['alwaysshowdescription'] = 'Luôn luôn hiển thị mô tả';
$string['submissiontypes'] = 'Kiểu nộp bài';
$string['filesubmissions'] = 'Đính kèm tệp tin';
$string['feedbacktypes'] = 'Kiểu phản hồi';
$string['submissionsettings'] = 'Thiết lập gửi bài';
$string['groupsubmissionsettings'] = 'Thiết lập nhóm gửi bài';
$string['notifications'] = 'Thông báo';
$string['submissiondrafts'] = 'Yêu cầu học viên nhấn nút Gửi';
$string['requiresubmissionstatement'] = 'Yêu cầu học viên chấp nhận báo cáo nộp bài';
$string['attemptreopenmethod'] = 'Cho phép nộp lại';
$string['unlimitedattempts'] = 'Không giới hạn';
$string['teamsubmission'] = 'Học viên nộp theo nhóm';
$string['preventsubmissionnotingroup'] = 'Yêu cầu gửi theo nhóm';
$string['requireallteammemberssubmit'] = 'Yêu cầu tất cả thành viên trong nhóm nộp bài';
$string['teamsubmissiongroupingid'] = 'Phân nhóm cho các nhóm học viên';
$string['sendnotifications'] = 'Thông báo cho học viên thông tin nộp bài';
$string['sendlatenotifications'] = 'Thông báo cho học viên thông tin nộp bài muộn';
$string['sendstudentnotificationsdefault'] = 'Thiết lập mặc định cho "Thông báo cho học viên"';
$string['attemptreopenmethod_none'] = 'Không bao giờ';
$string['attemptreopenmethod_manual'] = 'Thủ công';
$string['attemptreopenmethod_untilpass'] = 'Tự động cho đến khi vượt qua';
$string['maxattempts'] = 'Nộp bài nhiều lần';
$string['nosavebutnext'] = 'Tiếp theo';
$string['notgradedyet'] = 'Chưa có điểm';
$string['previous'] = 'Trước';
$string['reviewed'] = 'Đã xem lại';
$string['savechanges'] = 'Lưu những thay đổi';
$string['filesubmissions'] = 'Đính kèm tệp tin';
$string['showrecentsubmissions'] = 'Mở các bài nộp gần nhất';
$string['blindmarking'] = 'Đánh dấu ẩn';
$string['markingallocation'] = 'Phân bổ đánh dấu';
$string['description'] = 'Nội dung';
$string['gradingsummary'] = 'Thông tin tóm tắt';
$string['numberofparticipants'] = 'Tham gia';
$string['numberofsubmittedassignments'] = 'Đã nộp bài';
$string['numberofsubmissionsneedgrading'] = 'Cần chấm điểm';
$string['timeremaining'] = 'Thời gian còn lại';
$string['viewgrading'] = 'Xem / chấm điểm các bài đã nộp';
$string['submissionstatusheading'] = 'Trạng thái nộp bài';
$string['submissionstatus'] = 'Trạng thái nộp bài';
$string['noattempt'] = 'Không có bài nộp';
$string['notgraded'] = 'Chưa chấm điểm';
$string['timemodified'] = 'Sửa đổi mới nhất';
$string['addsubmission'] = 'Nộp bài';
$string['editsubmission_help'] = 'Thay đổi bài nộp của bạn';
$string['choosegradingaction'] = 'Hành động';
$string['viewgradebook'] = 'Xem sổ điểm';
$string['submissionstatus_new'] = 'Chưa nộp bài';
$string['submissionstatus_'] = 'Chưa nộp bài';
$string['lastmodifiedsubmission'] = 'Chỉnh sửa mới (Nộp bài)';
$string['lastmodifiedgrade'] = 'Chỉnh sửa mới (Chấm điểm)';
$string['filesubmissions'] = 'Tệp tin đính kèm';
$string['batchoperationsdescription'] = 'Lựa chọn...';
$string['batchoperationlock'] = 'Khoá nộp bài';
$string['batchoperationunlock'] = 'Mở khoá nộp bài';
$string['grantextension'] = 'Mở rộng';
$string['gradingoptions'] = 'Tuỳ chọn';
$string['assignmentsperpage'] = 'Bài tập cho mỗi trang';
$string['filter'] = 'Bộ lọc';
$string['filternone'] = 'Không lọc';
$string['filternotsubmitted'] = 'Không nộp bài';
$string['filterrequiregrading'] = 'Yêu cầu chấm điểm';
$string['quickgrading'] = 'Chấm nhanh';
$string['quickgradingresult'] = 'Chấm nhanh';
$string['markingworkflow'] = 'Sử dụng đánh dấu công việc';
$string['status'] = 'Trạng thái';
$string['submission'] = 'Bài nộp';
$string['submissions'] = 'Bài nộp';
$string['submissionstatus_marked'] = 'Đã cho điểm';
$string['submitassignment'] = 'Gửi bài';
$string['submitted'] = 'Bài đã nộp';
$string['submittedlateshort'] = 'Trễ hạn {$a}';
$string['modulename_help'] = 'Các mô-đun hoạt động bài tập cho phép một giáo viên có thể giao nhiệm vụ, công việc và bài tập và nhận thông tin phản hồi.

Học sinh có thể tải lên bất cứ nội dung kỹ thuật số (file), như các tài liệu word, Exell, hình ảnh, hoặc âm thanh và video clip. Ngoài ra, module có thể yêu cầu học sinh phải gõ văn bản trực tiếp vào trình soạn thảo văn bản. Module bài tập cũng có thể được sử dụng để nhắc nhở học sinh làm các bài tập mà họ cần phải hoàn thành ở nhà, chẳng hạn như tác phẩm nghệ thuật, và do đó không yêu cầu bất kỳ nội dung kỹ thuật số. Sinh viên có thể gửi tác phẩm riêng lẻ hoặc như một thành viên của một lớp.

Khi xem xét các bài tập, giáo viên có thể để lại ý kiến phản hồi và các tập tin tải lên, như đăng ký vào học viên đánh dấu-up, tài liệu với ý kiến hoặc thông tin phản hồi bởi âm thanh. Bài tập có thể được phân loại bằng cách sử dụng quy mô về số lượng, hoặc tùy chỉnh hoặc một phương pháp chấm điểm bởi phiếu đánh giá. Điểm số cuối cùng được ghi trong sổ điểm.';