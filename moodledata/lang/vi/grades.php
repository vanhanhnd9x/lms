<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'grades', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   grades
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activities'] = 'Các hoạt động';
$string['addcategory'] = 'Thêm phụ mục';
$string['addfeedback'] = 'Thêm phản hồi';
$string['addidnumbers'] = 'Thêm số ID';
$string['additem'] = 'Thêm mục điểm';
$string['addoutcome'] = 'Thêm một điểm đầu ra';
$string['addoutcomeitem'] = 'Thêm một mục đầu ra';
$string['addscale'] = 'Thêm thước đo điểm';
$string['aggregateextracreditmean'] = 'Điểm tổng hợp(với các tín chỉ thêm vào)';
$string['aggregatemax'] = 'Điểm cao nhất';
$string['aggregatemean'] = 'Điểm trung bình';
$string['aggregatemedian'] = 'Điểm giữa';
$string['aggregatemin'] = 'Điểm thấp nhất';
$string['aggregatemode'] = 'Cách tính điểm';
$string['allstudents'] = 'Tất cả học viên';
$string['allusers'] = 'Tất cả người dùng';
$string['autosort'] = 'Sắp xếp tự động';
$string['sort'] = 'Sắp xếp';
$string['profilereport'] = 'Báo cáo hồ sơ người dùng';
$string['profilereport_help'] = 'Báo cáo lớp được sử dụng trên trang hồ sơ người dùng.';
$string['aggregationposition_help'] = 'Thiết lập này xác định hiển thị danh mục và tổng điểm được hiển thị trước hoặc sau báo cáo điểm.';
$string['average'] = 'Trung bình';
$string['gradecategorysettings'] = 'Thiết lập danh mục lớp';
$string['reportsettings'] = 'Thiết lập báo cáo';
$string['max'] = 'Cao nhất';
$string['lowest'] = 'Thấp nhất';
$string['min'] = 'Thấp nhất';
$string['calculatedgrade'] = 'Điểm được tính';
$string['calculation'] = 'Tính toán';
$string['categories'] = 'Các phụ mục';
$string['categoriesanditems'] = 'Các phụ mục và mục';
$string['categoriesedit'] = 'Chỉnh sửa các phụ mục và mục';
$string['category'] = 'Phụ mục';
$string['categoryedit'] = 'Chỉnh sửa phụ mục';
$string['categoryname'] = 'Tên phụ mục';
$string['categorytotal'] = 'Tổng số danh mục';
$string['modgradetypescale'] = 'Tỷ lệ';
$string['typescale'] = 'Tỷ lệ';
$string['modgradetype'] = 'Kiểu';
$string['modgradetypenone'] = 'Không';
$string['modgradetypescale'] = 'Tỷ lệ';
$string['modgrademaxgrade'] = 'Điểm tối đa';
$string['uncategorised'] = 'Không có danh mục';
$string['showonlyactiveenrol'] = 'Chỉ hiện cho học viên đã ghi danh';
$string['modgradetypepoint'] = 'Điểm';
$string['changedefaults'] = 'Thay đổi mặc định';
$string['choosecategory'] = 'Chọn phụ mục';
$string['coursename'] = 'Tên khóa';
$string['coursescales'] = 'Thước đo khóa';
$string['coursesettings'] = 'Thiết lập khóa';
$string['csv'] = 'CSV';
$string['default'] = 'Mặc định';
$string['defaultprev'] = 'Mặc định ({$a})';
$string['deletecategory'] = 'Xóa phụ mục';
$string['disablegradehistory'] = 'Tắt chức năng lịch sử điểm';
$string['droplow'] = 'Bỏ phần thấp nhất';
$string['dropped'] = 'Đã được bỏ';
$string['duplicatescale'] = 'Nhân đôi thước đo';
$string['edit'] = 'Chỉnh sửa';
$string['editcalculation'] = 'Chỉnh sửa tính toán';
$string['editfeedback'] = 'Chỉnh sửa phản hồi';
$string['editgrade'] = 'Chỉnh sửa điểm';
$string['editgradeletters'] = 'Chỉnh sửa điểm chữ';
$string['editoutcome'] = 'Chỉnh sửa điểm đầu ra';
$string['editoutcomes'] = 'Chỉnh sửa các điểm đầu ra';
$string['editscale'] = 'Chỉnh sửa thước đo';
$string['generalsettings'] = 'Cài đặt chung';
$string['gradeimport'] = 'Nhập điểm';
$string['gradeitem'] = 'Mục điểm';
$string['gradeitemlocked'] = 'Khóa việc cho điểm';
$string['gradeitems'] = 'Các mục điểm';
$string['gradeitemsettings'] = 'Các thiết lập mục điểm';
$string['gradeitemsinc'] = 'Các mục điểm bao gồm';
$string['gradeletter'] = 'Điểm chữ';
$string['gradeletters'] = 'Các điểm số';
$string['gradelocked'] = 'Điểm đã khóa';
$string['grademax'] = 'Điểm tối đa';
$string['grademin'] = 'Điểm tối thiểu';
$string['gradepass'] = 'Điểm để qua';
$string['gradepreferences'] = 'Những cài đặt điểm số';
$string['outcomefullname'] = 'Tên đầy đủ';
$string['outcomeshortname'] = 'Tên tắt';
$string['settings'] = 'Cài đặt';
$string['courseavg'] = 'Trung bình khoá học';
//$string['sitewide'] = 'Kiến thức rộng';
$string['numberofgrades'] = 'Số lượng điểm';
$string['grade'] = 'Điểm';
$string['range'] = 'Khoảng điểm';
$string['percentage'] = 'Tỷ lệ phần trăm';
$string['feedback'] = 'Phản hồi';
$string['selectalloroneuser'] = 'Chọn tất cả hoặc một người dùng';
$string['coursetotal'] = 'Tổng điểm khoá học';
$string['gradebook'] = 'Bảng điểm';
$string['overallaverage'] = 'Trung bình tổng';
$string['simpleview'] = 'Xem đơn giản';
$string['fullview'] = 'Xem đầy đủ';
$string['aggregation'] = 'Tổng hợp';
$string['aggregationcoefextra'] = 'Thêm tín chỉ';
$string['aggregationcoefextrasum'] = 'Thêm tín chỉ';
$string['maxgrade'] = 'Điểm tối đa';
$string['aggregateonlygraded'] = 'Tổng hợp điểm trống';
$string['aggregatesum'] = 'Tổng điểm';
$string['aggregateweightedmean'] = 'Trọng số của điểm';
$string['aggregateweightedmean2'] = 'Trọng số đơn của điểm';
$string['weight'] = 'Trọng số';
$string['controls'] = 'Điều khiển';
$string['editverbose'] = 'Chỉnh sửa {$a->category} {$a->itemmodule} {$a->itemname}';
$string['gradecategory'] = 'Danh mục điểm';
$string['gradecategoryonmodform'] = 'Danh mục điểm';
$string['aggregatesubcats'] = 'Tổng hợp bao gồm cả danh mục con';
$string['categorytotalname'] = 'Tên danh mục tổng';
$string['iteminfo'] = 'Thông tin mục';
$string['iteminfo_help'] = 'Thiết lập này cung cấp không gian để nhập thông tin về các mục. Các thông tin không được hiển thị bất cứ nơi nào khác. ';
$string['gradetype'] = 'Kiểu điểm';
$string['typenone'] = 'Thường';
$string['typescale'] = 'Tỷ lệ';
$string['typetext'] = 'Văn bản';
$string['typevalue'] = 'Giá trị';
$string['usenoscale'] = 'Không tỷ lệ';
$string['usepercent'] = 'Phần trăm';
$string['separateand'] = 'Cách riêng biệt và tự đặt';
$string['gradedisplaytype'] = 'Kiểu hiển thị điểm';
$string['letter'] = 'Kiểu chữ';
$string['letters'] = 'Kiểu chữ';
$string['letterpercentage'] = 'Kiểu chữ (phần trăm)';
$string['letterreal'] = 'Kiểu chữ (số thực)';
$string['real'] = 'Số thực';
$string['realletter'] = 'Số thực (chữ)';
$string['realpercentage'] = 'Số thực (phần trăm)';
$string['percentageletter'] = 'Phần trăm (chữ)';
$string['percentagereal'] = 'Phần trăm (số thực)';
$string['decimalpoints'] = 'Kiểu số thập phân';
$string['hidden'] = 'Ẩn';
$string['hiddenuntil'] = 'Ẩn đến khi';
$string['hiddenuntildate'] = 'Ẩn đến khi: {$a}';
$string['hidecategory'] = 'Ẩn';
$string['locked'] = 'Đã khoá';
$string['locktime'] = 'Khoá sau';
$string['locktimedate'] = 'Khoá sau: {$a}';
$string['lockverbose'] = 'Khoá {$a->category} {$a->itemmodule} {$a->itemname}';
$string['itemname'] = 'Tên mục';
$string['hideverbose'] = 'Ẩn {$a->category} {$a->itemmodule} {$a->itemname}';
$string['gradebooksetup'] = 'Thiết lập bảng điểm';
$string['weights'] = 'Trọng lượng';
$string['aggregationcoefweight_help'] = 'Mục Trọng lượng được sử dụng trong các loại tập hợp ảnh hưởng đến tầm quan trọng của từng mục so với các mục cấp khác trong cùng thể loại..';
$string['aggregationcoefweight'] = 'Mục trọng lượng';
$string['gradeadministration'] = 'Quản trị bảng điểm';
$string['graderreport'] = 'Báo cáo lớp';
$string['selectauser'] = 'Lựa chọn người dùng';
$string['simpleview'] = 'Xem đơn lẻ';
$string['weightuc'] = 'Tính theo trọng lượng';
$string['contributiontocoursetotal'] = 'Cộng vào tổng điểm';
$string['coursegradesettings'] = 'Thiết lập lớp khoá học';
$string['coursesettingsexplanation'] = 'Thiết lập trình xác định các sổ điểm xuất hiện cho tất cả các thành viên tham gia khóa học.';
$string['aggregationposition'] = 'Vị trí tập hợp';
$string['positionfirst'] = 'Trước';
$string['positionlast'] = 'Sau';
$string['minmaxtouse'] = 'Min và max lớp được sử dụng trong tính toán';
$string['gradeitemminmax'] = 'Min và max lớp như quy định tại mục thiết lập lớp';
$string['gradegrademinmax'] = 'Min ban đầu và lớp tối đa';
$string['showrank'] = 'Hiển thị thứ hạng';
$string['hidetotalifhiddenitems'] = 'Ẩn số tổng nếu có chứa các mục ẩn';
$string['hidetotalshowexhiddenitems'] = 'Hiện tổng số không bao gồm các mục ẩn';
$string['hidetotalshowinchiddenitems'] = 'Hiện tổng số bao gồm các mục ẩn';
$string['showpercentage'] = 'Hiển thị phần trăm %';
$string['showgrade'] = 'Hiện lớp';
$string['showfeedback'] = 'Hiện phản hồi';
$string['showweight'] = 'Hiển thị các trọng số';
$string['showaverage'] = 'Hiện trung bình';
$string['showlettergrade'] = 'Hiện lớp bởi chữ';
$string['showcontributiontocoursetotal'] = 'Hiện tổng cộng vào tổng khoá học';
$string['showrange'] = 'Hiển thị các dải';
$string['rangedecimals'] = 'Sau số thập phân';
$string['showhiddenitems'] = 'Hiển thị các mục ẩn';
$string['shownohidden'] = 'Không hiển thị';
$string['showhiddenuntilonly'] = 'Chỉ ẩn đến khi';
$string['showallhidden'] = 'Hiển thị ẩn';
