<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['addingrandomsamatch'] = 'Thêm ngẫu nhiên một câu hỏi trả lời ngắn';
$string['editingrandomsamatch'] = 'Chỉnh sửa câu hỏi ngẫu nhiên của dạng trả lời ngắn';
$string['nosaincategory'] = 'Không có câu hỏi trả lời ngắn trong hạng mục mà bạn đã chọn \'{$a->catname}\'. Chọn danh mục khác nhau, làm câu hỏi trong mục này.';
$string['notenoughsaincategory'] = 'Chỉ có {$a->nosaquestions} câu hỏi trả lời ngắn trong danh mục mà bạn đã chọn \'{$a->catname}\'. Chọn danh mục khác nhau, Lấy nhiều câu hỏi trong danh mục này hoặc giảm số lượng câu hỏi mà bạn đã lựa chọn.';
$string['randomsamatch'] = 'Trả lời ngắn ngẫu nhiên phù hợp';
$string['randomsamatch_help'] = 'Từ quan điểm học viên, điều này có vẻ giống như một câu hỏi phù hợp. Sự khác biệt là danh sách các tên hoặc báo cáo (câu hỏi) cho phù hợp được rút ra ngẫu nhiên từ các câu hỏi trả lời ngắn gọn trong danh mục hiện tại. Phải có đầy đủ không sử dụng các câu hỏi trả lời ngắn trong các danh mục, nếu không một thông báo lỗi sẽ được hiển thị.';
$string['randomsamatch_link'] = 'question/type/randomsamatch';
$string['randomsamatchsummary'] = 'Như một câu hỏi dạng phù hợp, nhưng tạo ra ngẫu nhiên từ các câu hỏi trả lời ngắn trong một thể loại cụ thể.';

?>
