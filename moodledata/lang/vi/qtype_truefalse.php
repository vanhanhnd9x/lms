<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

defined('MOODLE_INTERNAL') || die();

$string['addingtruefalse'] = 'Thêm câu hỏi Đúng/Sai';
$string['correctanswer'] = 'Câu trả lời đúng';
$string['correctanswerfalse'] = 'Câu trả lời đúng là \'False\'.';
$string['correctanswertrue'] = 'Câu trả lời đúng là \'True\'.';
$string['editingtruefalse'] = 'Chỉnh sửa câu hỏi Đúng/Sai';
$string['false'] = 'Sai';
$string['feedbackfalse'] = 'Thông tin phản hồi cho các đáp án \'False\'.';
$string['feedbacktrue'] = 'Thông tin phản hồi cho các đáp án \'True\'.';
$string['pleaseselectananswer'] = 'Hãy chọn một câu trả lời.';
$string['selectone'] = 'Chọn một:';
$string['true'] = 'Đúng';
$string['truefalse'] = 'Đúng/Sai';
$string['truefalse_help'] = 'Để trả lời với một câu hỏi (có thể bao gồm một hình ảnh) người trả lời chọn từ đúng hay sai.';
$string['truefalse_link'] = 'question/type/truefalse';
$string['truefalsesummary'] = 'Biểu mẫu cơ bản của nhiều câu hỏi lựa chọn chỉ với hai sự lựa chọn \'True\' và \'False\'.';

?>
