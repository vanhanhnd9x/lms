<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'question', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   question
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['cannotread'] = 'Không thể đọc file nhập (hoặc file rỗng)';
$string['categorymove'] = 'Danh mục  \'{$a->name} \' bao gồm  {$a->count}  các câu hỏi. Xin hãy  chọn danh mục khác để di chuyển chúng đi.';
$string['exportfilename'] = 'quiz';
$string['exportnameformat'] = '%Y%m%d-%H%M';
$string['noquestionsinfile'] = 'Không có câu hỏi nào trong file nhập';
$string['numquestions'] = 'Không có câu hỏi';
$string['questions'] = 'Câu hỏi';
$string['questionx'] = 'Câu hỏi {$a}';
$string['notyetanswered'] = 'Chưa được trả lời';
$string['markedoutof'] = 'Điểm trả lời đúng';
$string['markedoutofmax'] = 'Điểm trả lời đúng {$a}';
$string['editquestion'] = 'Sửa câu hỏi';
$string['responsehistory'] = 'lịch sử trả lời';
$string['step'] = 'Lần';
$string['action'] = 'Hành động';
$string['state'] = 'Trạng thái';
$string['started'] = 'Bắt đầu';
$string['marks'] = 'Điểm';
$string['answersaved'] = 'Đã được trả lời';
$string['saved'] = 'Lưu: {$a}';
$string['penaltyfactor'] = 'Hệ số phạt';
$string['permissionto'] = 'Bạn có quyền:';
$string['selectcategoryabove'] = 'Lựa chọn một danh mục ở trên';
$string['selectacategory'] = 'Chọn danh mục:';
$string['defaultinfofor'] = 'Danh mục câu hỏi được phân bổ theo \'{$a}\'.';
$string['createnewquestion'] = 'Tạo câu hỏi mới ...';
$string['moveto'] = 'Chuyển tới >>';
$string['defaultfor'] = 'Mặc định với {$a}';
$string['withselected'] = 'Với các lựa chọn';
$string['includesubcategories'] = 'Hiển thị câu hỏi từ danh mục cấp dưới';
$string['showhidden'] = 'Hiển thị câu hỏi cũ';
$string['chooseqtypetoadd'] = 'Chọn định dạng câu hỏi thêm vào';
$string['selectaqtypefordescription'] = 'Chọn 1 loại câu hỏi để xem mô tả của chúng';
$string['questionname'] = 'Tên câu hỏi';
$string['questiontext'] = 'Nội dung câu hỏi';
$string['defaultmark'] = 'Điểm mặc định';
$string['generalfeedback'] = 'Thông tin phản hồi chung';
$string['settingsformultipletries'] = 'Thiết lập cho nhiều lần trả lời';
$string['penaltyforeachincorrecttry'] = 'Hình phạt cho mỗi lần trả lời không chính xác';
$string['hintn'] = 'Gợi ý {no}';
$string['hinttext'] = 'Nội dung gợi ý';
$string['addanotherhint'] = 'Thêm gợi ý khác';
$string['answer'] = 'Câu trả lời';
$string['feedback'] = 'Phản hồi';
$string['combinedfeedback'] = 'Kết hợp thông tin phản hồi';
$string['correctfeedback'] = 'Đối với bất kỳ phản ứng chính xác';
$string['partiallycorrectfeedback'] = 'Đối với bất kỳ phản ứng một phần đúng';
$string['shownumpartscorrect'] = 'Chỉ số lượng câu trả lời đúng';
$string['incorrectfeedback'] = 'Đối với bất kỳ phản ứng không chính xác';
$string['clearwrongparts'] = 'Câu trả lời không chính xác rõ ràng';
$string['questionbank'] = 'Ngân hàng câu hỏi';
