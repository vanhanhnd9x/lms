<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['addingshortanswer'] = 'Thêm một câu trả lời ngắn';
$string['addmoreanswerblanks'] = 'Thêm Câu trả lời';
$string['answer'] = 'Câu trả lời: {$a}';
$string['answermustbegiven'] = 'Bạn phải nhập một câu trả lời nếu có chấm điểm hoặc thông tin phản hồi.';
$string['answerno'] = 'Câu trả lời {$a}';
$string['caseno'] = 'Không, trường hợp là không quan trọng';
$string['casesensitive'] = 'Phân biệt chữ hoa, thường';
$string['caseyes'] = 'Có, trường hợp phải phù hợp';
$string['correctansweris'] = 'Câu trả lời đúng là: {$a}.';
$string['correctanswers'] = 'Câu trả lời đúng';
$string['editingshortanswer'] = 'Chỉnh sửa câu hỏi trả lời ngắn';
$string['filloutoneanswer'] = 'Bạn phải cung cấp ít nhất một câu trả lời có thể. Câu trả lời để trống sẽ không được sử dụng. \'*\' có thể được sử dụng như là một ký tự đại diện để phù hợp với bất kỳ ký tự. Câu trả lời phù hợp đầu tiên sẽ được sử dụng để xác định số điểm và thông tin phản hồi.';
$string['notenoughanswers'] = 'Đây là loại câu hỏi đòi hỏi ít nhất {$a} câu trả lời';
$string['pleaseenterananswer'] = 'Xin vui lòng nhập một câu trả lời.';
$string['shortanswer'] = 'Câu trả lời ngắn';
$string['shortanswer_help'] = 'Để trả lời một câu hỏi (có thể bao gồm một hình ảnh) các loại đơn từ hoặc cụm từ ngắn. Có thể có nhiều câu trả lời đúng nhất có thể, mỗi một điểm khác nhau. Nếu "phân biệt chữ hoa" được chọn, sau đó bạn có thể có điểm khác nhau cho "từ" hoặc "từ".';
$string['shortanswer_link'] = 'question/type/shortanswer';
$string['shortanswersummary'] = 'Cho phép đáp án của một hoặc một vài từ được chấm điểm bằng cách so sánh với dạng câu trả lời khác nhau, có thể chứa ký tự đại diện.';

?>
