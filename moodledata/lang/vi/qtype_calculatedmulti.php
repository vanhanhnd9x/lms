<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

defined('MOODLE_INTERNAL') || die();


$string['addingcalculatedmulti'] = 'Thêm câu hỏi tính toán nhiều lựa chọn';
$string['calculatedmulti'] = 'Tính toán nhiều lựa chọn';
$string['calculatedmultisummary'] = 'Câu hỏi tính toán nhiều lựa chọn cũng giống như câu hỏi nhiều lựa chọn với các yếu tố lựa chọn có thể bao gồm kết quả từ công thức giá trị số được lựa chọn ngẫu nhiên từ một tập hợp các bài kiểm tra khi được thực hiện.';
$string['calculatedmulti_help'] = 'Câu hỏi tính toán nhiều lựa chọn cũng giống như câu hỏi nhiều lựa chọn mà trong các yếu tố lựa chọn của nó có thể được bao gồm kết quả công thức số sử dụng các kí hiệu trong dấu ngoặc nhọn được thay thế bằng các giá trị riêng khi các bài kiểm tra được thực hiện. ví dụ, Nếu câu hỏi "Diện tích của hình chữ nhật có chiều dài {L} và chiều rộng {W}?" một lựa chọn là {={L}*{W}} ';
$string['calculatedmulti_link'] = 'question/type/calculatedmulti';
$string['editingcalculatedmulti'] = 'Chỉnh sửa câu hỏi nhiều lựa chọn';
?>
