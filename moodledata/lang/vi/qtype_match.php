<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('MOODLE_INTERNAL') || die();

$string['match'] = 'Phù hợp';
$string['addingmatch'] = 'Thêm câu hỏi dạng phù hợp';
$string['addmoreqblanks'] = '{no} More Sets of Blanks';
$string['availablechoices'] = 'Lựa chọn có sẵn';
$string['correctansweris'] = 'Câu trả lời đúng là: {$a}.';
$string['editingmatch'] = 'Chỉnh sửa câu hỏi dạng Phù hợp';
$string['filloutthreeqsandtwoas'] = 'Bạn phải cung cấp ít nhất 2 câu hỏi và 3 câu trả lời. Bạn có thể cung cấp câu trả lời sai bằng cách đưa ra một câu trả lời với câu hỏi trống. Nếu câu hỏi và câu trả lời là trắng sẽ bị bỏ qua.';
$string['match_help'] = 'Câu hỏi phù hợp với yêu cầu người trả lời một cách chính xác, để phù hợp với một danh sách các tên hoặc báo cáo (câu hỏi) cho một danh sách các tên hoặc báo cáo (câu trả lời).';
$string['match_link'] = 'question/type/match';
$string['matchsummary'] = 'Câu trả lời cho mỗi câu hỏi phụ phải được lựa chọn từ một danh sách các khả năng.';
$string['nomatchinganswer'] = 'Bạn phải chỉ định một câu trả lời phù hợp với câu hỏi \'{$a}\'.';
$string['nomatchinganswerforq'] = 'Bạn phải chỉ định một câu trả lời cho câu hỏi này.';
$string['notenoughqsandas'] = 'Bạn phải cung cấp ít nhất {$a->q} câu hỏi và {$a->a} trả lời.';
$string['notenoughquestions'] = 'Bạn phải cung cấp ít nhất {$a} cặp câu hỏi và trả lời.';
$string['shuffle'] = 'Ngẫu nhiên';
$string['shuffle_help'] = 'Nếu được kích hoạt, thứ tự của các mệnh đề (câu trả lời) được xáo trộn ngẫu nhiên cho mỗi bài làm, với điều kiện là "Câu hỏi ngẫu nhiên" trong các thiết lập hoạt động cũng được kích hoạt.';
$string['pleaseananswerallparts'] = 'Vui lòng trả lời tất cả các phần của câu hỏi.';
?>
