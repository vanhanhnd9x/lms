<?php



defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Nhúng';
$string['modulenameplural'] = 'Nhúng';
$string['modulename_help'] = 'Sử dụng module nhúng cho... | Module nhúng cho phép...';
$string['embededfieldset'] = 'Tuỳ chỉnh các trường';
$string['embededname'] = 'Tên bài giảng';
$string['embededtext'] = 'Mã nhúng';
$string['code'] = 'Mã';
$string['optionalsettings'] = 'Tuỳ chọn thiết lập';
$string['hidenextmodule'] = 'Ẩn nút "Bài giảng tiếp theo"';
$string['hidedescriptiontext'] = 'Ẩn các văn bản mô tả khi xem nội dung dung';
$string['embededname_help'] = 'Đây là nội dung của chỉ dẫn nhỏ kết hợp với trường tên bài giảng.';
$string['embeded'] = 'nhúng';
$string['pluginadministration'] = 'quản lý nhúng';
$string['pluginname'] = 'nhúng';

?>