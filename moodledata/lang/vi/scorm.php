<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'scorm', language 'vi', branch 'MOODLE_26_STABLE'
 *
 * @package   scorm
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['autocontinue_help'] = '<p><b>Tự động tiếp tục</b></p>

<p>Nếu Tự động tiếp tục được đặt là Có, khi một SCO
gọi phương thức "kết thúc trao đổi thông tin", SCO tiếp theo sẽ được phân phối
tự động đến cho học viên.</p>

<p>Nếu nó được đặt là Không, người dùng phải nhấn
vào nút "Tiếp tục" để học tiếp.</p>';
$string['grademethod_help'] = '<p><b>Phương pháp tính điểm</b></p>

<p>Kết quả của hoạt động SCORM/AICC hiển thị trong trang Điểm số có thể được đánh
giá theo một trong các cách sau:
    <ul>
	<li><b>Trạng thái SCO</b><br />Chế độ này hiển thị số SCO đã hoàn thành/đã đỗ của hoạt động. Giá trị lớn nhất là số lượng SCO.</li>
	<li><b>Điểm cao nhất</b><br />Trang điểm số sẽ hiển thị điểm cao nhất mà học viên đạt được trong tất cả các SCO đã đỗ.</li>
	<li><b>Điểm trung bình</b><br />Nếu bạn chọn chế độ này, Moodle sẽ tính trung bình tất cả các điểm.</li>
	<li><b>Điểm tổng</b><br />Với chế độ này tất cả các điểm sẽ được cộng lại.</li>
</p>';
$string['modulename'] = 'Gói SCORM';
$string['modulenameplural'] = 'Các gói SCORM';
$string['navigation'] = 'Điều hướng';
$string['package_help'] = '<p><b>Tập tin gói</b></p>

<p>Gói là một tập tin đặc biệt có phần mở rộng là <b>zip</b> (hoặc pif) bao gồm các
   tập tin định nghĩa khóa học AICC hoặc SCORM hợp lệ.</p>

<p>Một gói <b>SCORM</b> phải chứa ở gốc của tập tin zip một tập tin tên là <b>imsmanifest.xml</b>,
   là tập tin định nghĩa cấu trúc khóa học SCORM, vị trí tài nguyên và nhiều thứ khác nữa.</p>

<p>Một gói <b>AICC</b> được định nghĩa bởi vài tập tin (từ 4 đến 7) với phần mở rộng được định nghĩa trước.
   Bạn sẽ thấy ý nghĩa của các phần mở rộng ở đây:</p>
   <ul>
	<li>CRS - Tập tin Mô tả khóa học (bắt buộc)</li>
	<li>AU  - Tập tin Đơn vị khả chuyển (bắt buộc)</li>
	<li>DES - Tập tin Ký hiệu (bắt buộc)</li>
	<li>CST - Tập tin Cấu trúc khóa học(bắt buộc)</li>
	<li>ORE - Tập tin Quan hệ mục tiêu (không bắt buộc)</li>
	<li>PRE - Tập tin Điều kiện ban đầu (không bắt buộc)</li>
	<li>CMP - Tập tin Yêu cầu về tính hoàn thành (không bắt buộc)</li>
   </ul>';
$string['pluginname'] = 'Gói SCORM';
$string['preferencespage'] = 'Tuỳ chọn chỉ cho trang này';
$string['modulename_help'] = 'Một gói SCORM là một bộ sưu tập các tập tin được đóng gói theo một tiêu chuẩn thống nhất cho các đối tượng học tập. Các mô-đun hoạt động SCORM phép SCORM hoặc AICC gói được tải lên như một tập tin zip và thêm vào một khóa học.

Nội dung thường được hiển thị trên nhiều trang, với điều hướng giữa các trang. Có những lựa chọn khác nhau để hiển thị nội dung trong một cửa sổ pop-up, với một bảng nội dung, với các nút điều hướng vv hoạt động SCORM thường bao gồm các câu hỏi, và các lớp được ghi lại trong sổ điểm.

Hoạt động SCORM có thể được sử dụng

* Để trình bày nội dung đa phương tiện và hình ảnh động
* Là một công cụ đánh giá';
$string['info'] = 'Thông tin';
$string['reports'] = 'Báo cáo';
$string['noattemptsallowed'] = 'Số lần cho phép thực hiện lại';
$string['noattemptsmade'] = 'Số lần đã thực hiện';
$string['gradeforattempt'] = 'Điểm cho lần thực hiện';
$string['gradeforattempt'] = 'Phương thức chấm điểm';
$string['highestattempt'] = 'Lấy điểm cao nhất';
$string['gradereported'] = 'Điểm đã báo cáo';
$string['scorm:deleteresponses'] = 'Xoá kết quả thực hiện';
$string['deleteallattempts'] = 'Xoá tất cả lần thực hiện';
$string['mode'] = 'Hình thức hiển thị';
$string['browse'] = 'Xem thử';
$string['normal'] = 'Bình thường';
$string['newattempt'] = 'Bắt đầu bài làm mới';
$string['enter'] = 'Bắt đầu';
$string['reportcountallattempts'] = '{$a->nbattempts} lần thực hiện cho {$a->nbusers} người dùng, hiển thị {$a->nbresults} kết quả';
$string['attempt'] = 'Thực hiện';
$string['attempts'] = 'Thực hiện';
$string['started'] = 'Bắt đầu';
$string['last'] = 'Truy cập lần cuối';
$string['score'] = 'Điểm';
$string['selectall'] = 'Chọn tất cả';
$string['selectnone'] = 'Bỏ chọn tất cả';
$string['deleteselected'] = 'Xoá kết quả thực hiện đã chọn';
$string['show'] = 'Hiển thị';
$string['optallstudents'] = 'tất cả người dùng';
$string['optattemptsonly'] = 'chỉ người dùng đã học';
$string['optnoattemptsonly'] = 'Chỉ người dùng chưa học';
$string['preferencesuser'] = 'Điều chỉnh đối với báo cáo này';
$string['pagesize'] = 'Số trang';
$string['details'] = 'Theo dõi chi tiết';
$string['completed'] = 'Đã hoàn thành';
$string['completionstatus_completed'] = 'Đã hoàn thành';
$string['packagehdr'] = 'Đóng gói tài liệu';
$string['package'] = 'Tài liệu đóng gói';
$string['updatefreq'] = 'Tần suất tự động cập nhật';
$string['everyday'] = 'Hàng ngày';
$string['everytime'] = 'Mỗi khi được sử dụng';
$string['display'] = 'Hiển thị gói';
$string['currentwindow'] = 'Trang hiện tại';
$string['popup'] = 'Trang mới';
$string['displayactivityname'] = 'Hiển thị tên hoạt động';
$string['displaycoursestructure'] = 'Hiển thị cấu trúc khoá học trên trang';
$string['displayattemptstatus'] = 'Hiển thị trạng thái học';
$string['attemptstatusall'] = 'Bảng thông tin và trang';
$string['attemptstatusmy'] = 'Chỉ có bảng thông tin';
$string['attemptstatusentry'] = 'Chỉ có trang thông tin';
$string['width'] = 'Rộng';
$string['height'] = 'Cao';
$string['options'] = 'Tuỳ chọn (trên một số trình duyệt)';
$string['scrollbars'] = 'Cho phép cửa sổ cuộn';
$string['directories'] = 'Hiển thị liên kết thư mục';
$string['location'] = 'Hiển thị thanh vị trí';
$string['menubar'] = 'Hiển thị thanh trình đơn';
$string['toolbar'] = 'Hiển thị thanh công cụ';
$string['status'] = 'Trạng thái';
$string['displayactivityname'] = 'Hiển thị tên hoạt động';
$string['skipview'] = 'Học viên bỏ qua trang cấu trúc nội dung';
$string['firstaccess'] = 'Lần đầu tiên';
$string['hidebrowse'] = 'Ẩn hình thức xem thử';
$string['displaycoursestructure'] = 'Hiển thị cấu trúc nội dung khoá học trên trang';
$string['hidetoc'] = 'Hiển thị cấu trúc khoá học trong trình phát';
$string['sided'] = 'Bên cạnh';
$string['hidden'] = 'Ẩn';
$string['popupmenu'] = 'Trong trình đơn sổ xuống';
$string['disabled'] = 'Không kích hoạt';
$string['nav'] = 'Hiển thị trình đơn bên trái';
$string['undercontent'] = 'Bên dưới nội dung';
$string['floating'] = 'Trôi nổi';
$string['fromleft'] = 'Bên trái';
$string['fromtop'] = 'Bên trên';
$string['displayattemptstatus'] = 'Hiển thị trạng thái làm việc';
$string['scormopen'] = 'Bắt đầu từ';
$string['scormclose'] = 'Tới ngày';
$string['grademethod'] = 'Phương thức chấm điểm';
$string['gradescoes'] = 'Đối tượng đang học';
$string['gradeaverage'] = 'Điểm trung bình';
$string['gradesum'] = 'Điểm tổng';
$string['attemptsmanagement'] = 'Quản lý số lần thực hiện';
$string['maximumattempts'] = 'Số lần thực hiện';
$string['nolimit'] = 'Không giới hạn';
$string['attemptsx'] = '{$a} thực hiện';
$string['attempt1'] = '1 lần thực hiện';
$string['whatgrade'] = 'Chấm điểm lần thực hiện';
$string['averageattempt'] = 'Trung bình lần thực hiện';
$string['firstattempt'] = 'Lần đầu hoàn thành';
$string['lastattempt'] = 'Lần cuối cùng hoàn thành';
$string['forcenewattempt'] = 'Buộc phải thực hiện lần mới';
$string['lastattemptlock'] = 'Khoá sau khi kết thúc làm bài';
$string['compatibilitysettings'] = 'Thiết lập ràng buộc';
$string['forcecompleted'] = 'Bắt buộc hoàn thành';
$string['autocontinue'] = 'Tự động tiếp tục';
$string['autocommit'] = 'Tự động gửi bài';