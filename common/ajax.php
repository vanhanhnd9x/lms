<?php
require("../config.php");
global $CFG, $DB;

if (!empty($_GET['schoolid'])) {
	$schoolid=$_GET['schoolid'];

	$sql="SELECT * from block_student where `block_student`.`schoolid`={$schoolid} AND `block_student`.`del`=0";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">'.get_string('block_student').'</option>';
	if (!empty($data)) {
		foreach ($data as $key => $value) {
			?>
			<option data-status="Lớp <?php echo $value->status; ?>" value="<?php echo $value->id; ?>"<?php if(!empty($_GET['blockedit'])&&$_GET['blockedit']== $value->id) echo'selected';?>><?php echo $value->name; ?></option>
			<?php 
		}
	}else{
		?>
		<script>
			$("#groupid").html('<option value=""><?php echo get_string('class'); ?></option>');
			$("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
		</script>
		<?php 
	}
}

// load lớp theo khối
if(!empty($_GET['block_student'])){
	$block_studentid=$_GET['block_student'];
	
	# code...
	$sql="SELECT `groups`.`id`,`groups`.`name` from groups
	JOIN `block_student` ON `block_student`.`id`=`groups`.`id_khoi`
	where `groups`.`id_khoi`={$block_studentid} AND `block_student`.`del`=0
	";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">'. get_string('class').'</option>';
	if($data){
		foreach ($data as $key => $value) {
			?>
			<option data-course="Nhóm <?php echo substr($value->name,6); ?>" value="<?php echo $value->id; ?>" <?php if(!empty($_GET['groupedit'])&&$_GET['groupedit']== $value->id) echo'selected';?>><?php echo $value->name; ?></option>
			<?php 
		}
	}else{
		?>
		<script>
			$("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
		</script>
		<?php 
	}
	
	
}
// load nhom lop
if(!empty($_GET['groupid'])){
	$groupid=$_GET['groupid'];
	$groupss=$DB->get_record('groups', array(
	  'id' => $groupid
	));
	$sql="SELECT `course`.`id`,`course`.`fullname` FROM `course`
	JOIN `group_course` ON `group_course`.`course_id` = `course`.`id`
	WHERE `group_course`.`group_id`={$groupid}
	";
	$data = $DB->get_records_sql($sql);

	echo'<option value="">'.get_string('classgroup').'</option>';
	if ($data) { 
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" <?php if(!empty($_GET['courseedit'])&&($_GET['courseedit']== $value->id)) echo'selected'; ?>><?php echo $value->fullname; ?></option>
			<?php 
		}
		?>
		<script>
			var idschool_year="<?php echo  $groupss->id_namhoc;?>"
			$("#school_yearid_semester").val(idschool_year).change();
		</script>
		<?php
	}else{
		?>
		<script>
			$("#studentid").html('<option value=""><?php echo get_string('student') ?></option>');
		</script>
		<?php 
	}
}
// load  hoc sinh trong nhom
if (!empty($_GET['courseid'])) {
	# code...
	$courseid=$_GET['courseid'];
	$sql="SELECT DISTINCT `user`.`id`,`user`.`firstname`,`user`.`lastname`,`role_assignments`.`roleid`
	FROM `course` JOIN `enrol` ON `enrol`.`courseid` = `course`.`id` 
	JOIN `user_enrolments` ON `user_enrolments`.`enrolid` = `enrol`.`id`
	JOIN `user` ON `user_enrolments`.`userid` = `user`.`id`
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `course`.`id`={$courseid} ";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">Chọn học sinh</option>';
	if ($data) { 
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" ><?php echo $value->firstname,'',$value->lastname; ?></option>
			<?php 
		}
	}
}

?>