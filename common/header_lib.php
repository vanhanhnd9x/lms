<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function get_root_parent_of_user($user_id){
    global $DB;
    $sql="select parent from user where id=".$user_id;
    
    $parent = $DB->get_record('user', array('id'=>$user_id));
    // print_r($parent);

    if($parent->parent==0){
        return $parent->id;
    }
    else{
        return get_root_parent_of_user($parent->parent);
    }
}
// hien thi menu theo user dang nhap
// lay cha theo roleid
function ds_chuc_nang_cha_theo_role($roleid){
    global $DB;
    $sql = "SELECT  `permissions`.* 
    FROM `permissions`
    JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid}
    AND `permissions`.`pater`=0
    ORDER BY sapxep ASC
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function ds_chuc_nang_con_theo_cha_va_role($paterid,$roleid){
    global $DB;
    $sql = "SELECT  `permissions`.* 
    FROM `permissions`
    JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid}
    AND `permissions`.`pater`={$paterid}
    AND `permissions`.`type` IN ('list','add')
    ORDER BY sapxep ASC
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}


function lay_role_id_cua_user_dang_nhap($userid){
    global $DB;
    $sql = "SELECT DISTINCT `role_assignments`.`roleid` 
    FROM `role_assignments` 
    JOIN `user` ON `role_assignments`.`userid`= `user`.`id` 
    JOIN `role` ON `role`.`id` = `role_assignments`.`roleid` 
    WHERE `role_assignments`.`userid`={$userid}";
    $data = $DB->get_field_sql($sql,NULL,$strictness=IGNORE_MISSING);
    return $data;
}
function lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle){
    global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type` IN ('edit','view')
    AND `permissions`.`module`='{$moodle}' ORDER BY sapxep ASC
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function check_chuc_nang_xoa($roleid,$moodle){
    global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type`='delete'
    AND `permissions`.`module`='{$moodle}'
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function check_chuc_nang_pop_up($roleid,$moodle){
    global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type`='popup'
    AND `permissions`.`module`='{$moodle}'
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function check_chuc_nang_them_moi($roleid,$moodle){
    global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type`='add'
    AND `permissions`.`module`='{$moodle}'
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function check_chuc_nang_gan_con($roleid,$moodle){
    global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type`='addassign_con'
    AND `permissions`.`module`='{$moodle}'
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function hien_thi_mau_button_theo_y(){

    $mau=array(
        '1'=>'btn btn-warning waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '2'=>'btn btn-dark waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '3'=>'btn btn-info btn-sm waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '4'=>'btn btn-secondary waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '5'=>'btn btn-info waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '6'=>'btn btn-primary waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '7'=>'btn btn-purple  waves-effect waves-light btn-sm tooltip-animation tooltipstered',
        '8'=>'btn btn-success  waves-effect waves-light btn-sm tooltip-animation tooltipstered',
    );
    return $mau;
}

function hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$id=null){
    global $DB,$CFG;
    $hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
    $mau=hien_thi_mau_button_theo_y();
    $key=0;
    foreach ($hanhdong as $h) {
            $key++;
        echo'<a href="'.$CFG->wwwroot.''.$h->link.''.$id.'" class="mauicon tooltip-animation" title="'.get_string( $h->name).'">
                <i class="'.$h->icon.'" aria-hidden="true"></i>
            </a>';
    }
    ?>
    <style>
        .mauicon{
            color:#106c37;
        }
        .mauicon:hover{
            color:#4bb747;
        }
    </style>
    <?php 
}
// function hien thi phan gan trong lop va nhom lop
function lay_ds_cn_gan($roleid,$module){
    global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type` ='addassign'
    AND `permissions`.`module`='{$module}' ORDER BY sapxep ASC
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function hien_thi_chuc_nang_gan_theo_tung_moodle_by_cua($moodle,$roleid,$id=null){
    global $DB,$CFG;
    $hanhdong=lay_ds_cn_gan($roleid,$moodle); 
    foreach ($hanhdong as $h) { $key++;
        echo' <li class="form-group">
            <a href="'.$CFG->wwwroot.''.$h->link.''.$id.'" class="btn btn-block btn-sm btn-light">'.get_string( $h->name).'</a>
        </li>';
    }
}
function check_chuc_nang_xoachucnangcong($roleid,$module){
     global $DB;
    $sql = "SELECT `permissions`.* 
    FROM `permissions` JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id`
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`type` ='del_con'
    AND `permissions`.`module`='{$module}' ORDER BY sapxep ASC
    ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$module,$name){
    global $DB;
    $sql="SELECT `permissions`.* FROM `permissions` 
    JOIN `permissions_role` ON `permissions_role`.`permissions_id`=`permissions`.`id` 
    WHERE `permissions_role`.`role_id`={$roleid} 
    AND `permissions`.`name`='{$name}' AND `permissions`.`module`='{$module}'
 ";
    $data = $DB->get_records_sql($sql);
    return $data;
}
function get_url_now_cre_by_trang(){
    $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') 
                === FALSE ? 'http' : 'https';
    $host     = $_SERVER['HTTP_HOST'];
    $script   = $_SERVER['SCRIPT_NAME'];
    $params   = $_SERVER['QUERY_STRING'];
     
    $currentUrl = $protocol . '://' . $host . $script . '?' . $params;
    return $currentUrl;
}
function hien_thi_menu_theo_role($role_id){
    $cha=ds_chuc_nang_cha_theo_role($role_id);
    if($cha){
        foreach ($cha as  $value) {
            # code...
            $con=ds_chuc_nang_con_theo_cha_va_role($value->id,$role_id);
            ?>
            <li class="<?php  print strpos($currentTab, $value->link) ? 'active' : '';?>">
                <a href="javascript: void(0);" class="<?php print strpos($currentTab, $value->link) ? 'active' : ''; ?>">
                    <i class="<?php echo $value->icon; ?>"></i>
                    <span> <?php echo get_string($value->name); ?> </span> <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level collapse" aria-expanded="false">
                    <?php 
                        foreach ($con as  $c) {
                        # code...
                            ?>
                            <li class="<?php print strpos($currentTab, $c->link) ? 'active' : ''; ?>">
                                <a href="<?php print new moodle_url($c->link); ?>" class="<?php print strpos($currentTab, $c->link) ? 'active' : ''; ?>"> <?php echo get_string($c->name); ?></a>
                            </li>
                            <?php 
                        }
                     ?>
                                
                </ul>
            </li>            
            <?php 
        }
        echo '';
    }
}
function hien_thi_menu_khoi_quan_ly_nha_truong_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('schools-year','schools','block-student','groups','course','student','schedule')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
             <i class="fa fa-university" aria-hidden="true"></i>
             <span>'.get_string('school_management').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
function hien_thi_menu_khoi_quan_ly_giao_vien_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('teacher','ta','rht','rhta')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
             <i class="fa fa-id-card-o" aria-hidden="true"></i>
             <span>'.get_string('teacher').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
function hien_thi_menu_khoi_quan_ly_danh_gia_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('evaluation-teacher','evaluation-ta','evaluation-student','unlock_evaluation_student')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
             <i class="fa fa-check-square-o"></i>
             <span>'.get_string('evaluate').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
function hien_thi_menu_khoi_quan_ly_ket_tai_lieu_hoc_tap_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('document','cat-question','question','quiz')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
            <i class="fa fa-file-text"></i>
             <span>'.get_string('study_document').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
function hien_thi_menu_khoi_quan_ly_ket_qua_hoc_tap_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('unittest','semester')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
             <span>'.get_string('learning_outcomes').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
function hien_thi_menu_khoi_quan_ly_baocao_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('report')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
             <span>'.get_string('report').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
function hien_thi_menu_khoi_caidathethong_theo_role_dang_nhap($roleid){
    global $DB,$CFG;
    $sql="
        SELECT * FROM `permissions`
        JOIN `permissions_role` ON  `permissions_role`.`permissions_id` = `permissions`.`id`
        WHERE `permissions`.`module` IN ('user','configuration')
        AND `permissions`.`type`='list' AND `permissions_role`.`role_id`={$roleid} 
        AND `permissions`.`pater` NOT IN (0)
        ORDER BY `permissions`.`pater`, `permissions`.`sapxep` ASC
    ";
    $data=$DB->get_records_sql($sql);
    if(!empty($data)){
        echo'<li class="">
         <a href="javascript: void(0);" class="">
            <i class="fi-cog" aria-hidden="true"></i>
             <span>'.get_string('configuration').' </span> <span class="menu-arrow"></span>
         </a>
         <ul class="nav-second-level collapse " aria-expanded="false">
            ';
            foreach ($data as $key => $value) {
                # code...
                ?>
                 <li class="">
                     <a href="<?php echo $CFG->wwwroot,$value->link; ?>" class=""> <?php echo get_string($value->name); ?></a>
                 </li>
                <?php  
            }
         echo   '                                           
         </ul>
     </li>';
    }
}
?>
