<?php


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once($CFG->dirroot . '/manage/lib.php');

//Tung added for search user invite to course
function search_child_user($creator_id, $name) {
    global $DB;

    $field = "id, firstname, lastname, email,birthday";
    $sql = "SELECT " . $field . " FROM user
        WHERE ";
//    $params['creator_id'] = $creator_id;
//    $sql .=" AND (";
    $sql .= $DB->sql_like('user.lastname', ':lastname', false);
    $params['lastname'] = '%' . $name . '%';
    $sql.=" OR ";
    $sql .= $DB->sql_like('user.firstname', ':firstname', false);
    $params['firstname'] = '%' . $name . '%';
    $sql.=" OR ";
    $sql .= $DB->sql_like('user.username', ':username', false);
    $params['username'] = '%' . $name . '%';
    $sql.=" OR ";
    $sql .= $DB->sql_like('user.email', ':email', false);
    $params['email'] = '%' . $name . '%';

//    $sql .=")";

    $result = $DB->get_records_sql($sql, $params);
    return $result;
}
//Tung added for search teams invite to course
function search_team_of_course($course_id, $creator_id, $name) {
    global $DB;
    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $creator_id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $creator_id;
    
    $field = "id, name, courseid, userid";
    $sql = "SELECT ".$field." FROM {groups} g WHERE (g.userid in (".$id_list.") OR g.courseid = :courseid) 
    AND ";
    $sql .= $DB->sql_like('g.name', ':name', false);
    $params['courseid'] = $course_id;
    $params['name'] = '%' . $name . '%';
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function get_users_from_parent($creator_id) {
    global $DB;
    $sql = 'SELECT * FROM user
        WHERE parent=:creator_id1 OR id=:creator_id2';
    $params['creator_id1'] = $creator_id;
    $params['creator_id2'] = $creator_id;
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}


function get_courses_achievments($user_id) {
    global $CFG;
    require_once($CFG->dirroot . '/lib/enrollib.php');
    $user_courses = enrol_get_users_courses($user_id);
    $user_courses_arr = array();
    foreach ($user_courses as $key => $val) {


        if (count_course_completion($val, $user_id) == 100) {
            $user_courses_arr[] = $val;
        }
    }
    return $user_courses_arr;
}

function get_courses_library() {
    global $DB;

    $sql = "select * from course where visible=1 and id in(select course_id from course_settings where course_library=1)";

    $records = $DB->get_records_sql($sql);

    return $records;
}

function is_cmc_existed($cm_id, $user_id) {
    global $DB;
    $cmc = $DB->get_record('course_modules_completion', array(
        'coursemoduleid' => $cm_id,
        'userid' => $user_id,
    ));
    return $cmc->id;
}

function insert_cmc($cm_id, $user_id) {
    global $DB;
    $course_modules_completion = new stdClass();
    $course_modules_completion->coursemoduleid = $cm_id;
    $course_modules_completion->userid = $user_id;
    $course_modules_completion->completionstate = 1;
    $course_modules_completion->timemodified = time();
    return $DB->insert_record('course_modules_completion', $course_modules_completion);
}

function mark_course_incomplete($course_id, $user_id) {
    global $DB;
    $sql = "update course_completions set timecompleted=NULL where course=$course_id and userid=$user_id";
    $DB->execute($sql);
}

function update_all_cms_as_completed($course_id, $user_id) {
    global $DB;

    $course = $DB->get_record('course', array('id' => $course_id));
    $modinfo = & get_fast_modinfo($course);
    $mods_in_course = $modinfo->sections;
    foreach ($mods_in_course as $key => $val) {
        foreach ($val as $key1 => $val1) {
            if (is_cmc_existed($val1, $user_id)) {
                update_state_course_module_completion($val1, $user_id, 1);
            } else {
                insert_cmc($val1, $user_id);
            }
        }
    }
}

function update_state_course_module_completion($cm_id, $user_id, $state) {
    global $DB;
    $sql = "update course_modules_completion set completionstate=$state,timemodified=" . time() . " where coursemoduleid=$cm_id and userid=$user_id";
    if ($state == 0) {
        $sql = "update course_modules_completion set completionstate=$state,timemodified=0 where coursemoduleid=$cm_id and userid=$user_id";
    }
    $DB->execute($sql);
    if ($state == 0) {// update state of course as incomplete also
        $cm = $DB->get_record('course_modules', array(
            'id' => $cm_id
        ));
        $course_id = $cm->course;
        mark_course_incomplete($course_id, $user_id);
    }
}

function is_cm_complete($course, $cm_id, $user_id) {
    $completion = new completion_info($course);
    if ($completion->is_course_complete($user_id)) {
        return 1;
    }

    $cm = new course_modinfo($course, $user_id);

    $cm_object = $cm->get_cm($cm_id);
    $cmc = get_completion_state_of_course_module($user_id, $cm_id);
    if ($cm_object->module == 13) {
        $pass_mark = round(get_pass_mark_quiz($cm_object->instance));
        $score = get_highest_score_quiz($user_id, $cm_object->instance);

        if ($pass_mark == $score) {
            return 1;
        } else {
            return 0;
        }
    } else {

        if ($cmc->completionstate != 0 && $cmc->completionstate != "") {
            return 1;
        } else {
            return 0;
        }
    }
}

function count_course_completion($course, $user_id) {
    $complete = 0;
    $in_complete = 0;
    $cm = new course_modinfo($course, $user_id);
    $modinfo = & get_fast_modinfo($course);
    $mods_in_course = $modinfo->sections;
    foreach ($mods_in_course as $key => $val) {
        foreach ($val as $key1 => $val1) {
            $cm_object = $cm->get_cm($val1);
            $cmc = get_completion_state_of_course_module($val1);
            if ($cm_object->module == 13) {
                $pass_mark = round(get_pass_mark_quiz($cm_object->instance));
                $score = get_highest_score_quiz($user_id, $cm_object->instance);
                if ($pass_mark == $score) {
                    //echo $cm_object->name .": complete13<br/>";
                    $complete++;
                } else {
                    //echo $cm_object->name .": in complete13<br/>";
                    $in_complete++;
                }
            } else {

                if (is_cm_complete($course, $val1, $user_id) == 1) {
                    //echo $cm_object->name .": complete<br/>";
                    $complete++;
                } else {
                    // echo $cm_object->name .": in complete<br/>";
                    $in_complete++;
                }
            }
        }
    }
    //echo "complete=".$complete."<br/>";
    //echo "incomplete=".$in_complete."<br/>";
    $completion = new completion_info($course);
    if ($completion->is_course_complete($user_id)) {
        return 100;
    }
    if ($in_complete != 0 && $complete != 0) {
        return round($complete / ($complete + $in_complete) * 100);
    } else if ($in_complete == 0 && $complete != 0) {
        return 100;
    } else if ($complete == 0) {
        return 0;
    }
}

function get_pass_mark_quiz($quiz_id) {
    global $DB;
    $quiz = $DB->get_record('quiz', array(
        'id' => $quiz_id,
    ));

    return round($quiz->sumgrades);
}

function update_quiz_attemp($attemp_id, $sumgrades, $timefinish) {
    global $DB;
    $record = new stdClass();
    $record->id = $attemp_id;

    $record->timefinish = $timefinish;
    $record->sumgrades = $sumgrades;
    $record->timemodified = time();
    return $DB->update_record('quiz_attempts', $record, false);
}

function get_highest_score_quiz_obj($user_id, $quiz_id) {
    global $DB;
    $sql = "SELECT * FROM quiz_attempts WHERE userid=$user_id AND quiz=$quiz_id AND sumgrades=(SELECT MAX(sumgrades) FROM quiz_attempts WHERE userid=$user_id AND quiz=$quiz_id);";
    //echo $sql;
    $score = $DB->get_record_sql($sql);
    return $score;
}

function get_highest_score_quiz($user_id, $quiz_id) {
    global $DB;
    $sql = "SELECT MAX(sumgrades) AS score FROM quiz_attempts WHERE userid=$user_id AND quiz=$quiz_id";

    $score = $DB->get_record_sql($sql);
    return $score->score;
}

function get_completion_state_of_course_module($user_id, $cm_id) {
    global $DB;
    $cmc = $DB->get_record('course_modules_completion', array(
        'coursemoduleid' => $cm_id,
        'userid' => $user_id,
    ));

    $cm = $DB->get_record('course_modules', array(
        'id' => $cm_id,
    ));
    $completion = new completion_info($cm->course);
    if ($completion->is_course_complete($user_id)) {
        $cmc->completionstate = 1;
    }
    return $cmc;
}

function get_user_from_id($user_id) {
    global $DB;
    $user = $DB->get_record('user', array(
        'id' => $user_id
    ));

    return $user;
}

function get_role_from_id($user_id) {
    global $DB;
    $user = $DB->get_record('role', array(
        'id' => $user_id
    ));

    return $user;
}

function get_role_assign_from_id($user_id) {
    global $DB;
    $user = $DB->get_records_sql('SELECT user.*,role_assignments.roleid  FROM user JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.userid ='.$user_id);

    return $user;
}

function update_profile_logo($user_id, $file_name) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;

    $record->profile_img = $file_name;

    return $DB->update_record('user', $record, false);
}

function time_ago($time, $prefix = 'Đăng nhập lần cuối') {
    if ($time) {


        $periods = array(get_string('second'), get_string('minute'), get_string('hour'), get_string('day'), get_string('week'), get_string('month'), get_string('year'), get_string('decade'));
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        $now = time();

        $difference = $now - $time;
        $tense = get_string('ago');

        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        //hide for vietnamese
//   if($difference != 1) {
//       $periods[$j].= "s";
//   }


        return "$prefix $difference $periods[$j] $tense";
    } else {
        return get_string('notloginyet');
    }
}

//tung added
function time_ago_second($time, $userid, $prefix = 'Đăng nhập lần cuối') {
    global $DB;
    $sql = "SELECT * FROM log WHERE userid = " . $userid;
    $result = $DB->get_records_sql($sql);

    if ($time || !empty($result)) {


        $periods = array(get_string('second'), get_string('minute'), get_string('hour'), get_string('day'), get_string('week'), get_string('month'), get_string('year'), get_string('decade'));
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        $now = time();

        $difference = $now - $time;
        $tense = get_string('ago');

        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        //hide for vietnamese
//   if($difference != 1) {
//       $periods[$j].= "s";
//   }

        return "$prefix $difference $periods[$j] $tense";
    } else {
        return get_string('notloginyet');
    }
}

function detect_mobile() {

    if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
        return true;
    }
    return false;
}

function get_document_from_id($id) {
    global $DB;
    $document = $DB->get_record('document', array(
        'id' => $id
    ));

    return $document;
}

function insert_document($name, $file_name, $course_id) {
    global $DB;

    $document = new stdClass();
    $document->course_id = $course_id;
    $document->new_name = $name;
    $document->file_name = $file_name;
    $DB->insert_record('document', $document);
}

function get_documents_from_course($course_id) {
    global $DB, $USER;
    $sql = 'SELECT * from document where course_id = ' . $course_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function delete_document($document_id) {
    global $DB;
    $DB->delete_records_select('document', " id = $document_id ");
}

function copy_course_settings($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO course_settings(course_id,course_library,module_order,new_window,                 
must_complete_by,update_complete_date,compliant_for,              
automatic_resit,certificate_template,sell_course,course_sales_date,                
active,fee,currency,sale_description,time_span_complete,         
time_span_complete_number,time_span_complete_type,    
complete_specific_date,sale_full_description,      
file_name) SELECT $new_course_id,course_library,module_order,new_window,                 
must_complete_by,update_complete_date,compliant_for,              
automatic_resit,certificate_template,sell_course,course_sales_date,                
active,fee,currency,sale_description,time_span_complete,         
time_span_complete_number,time_span_complete_type,    
complete_specific_date,sale_full_description,      
file_name   FROM course_settings WHERE course_id=$old_course_id";
    $DB->execute($sql);
}

function search_member_of_course($name, $course_id, $creator_id) {
    global $DB, $USER;
    // tung fix: load all data in account
    if (is_siteadmin()) {
        $sql = "SELECT * FROM user ";
        $rows = $DB->get_records_sql($sql);
        foreach ($rows as $row) {
            $id_list .= $row->id . ",";
        }
        $id_list = $id_list . "0";
    } else {
        $sql = "SELECT * FROM user ";
        $rows = $DB->get_records_sql($sql);
        $id_list = display_parent($rows, $USER->id);
        $id_me = $id_list;

        if ($id_me == 0) {
            $id_me = $USER->id;
        }

        if ($id_list == 0) {
            $id_list = $USER->id;
        }

        $id_list = display_children($rows, $id_list);
        $id_list .= $USER->id . "," . $id_me;
    }
    //end fix
    if ($name == "") {
        return get_users_from_parent($creator_id);
    } else {
        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent in (" . $id_list . ") OR user2.id in (" . $id_list . ")) ";
        $params['course_id'] = $course_id;
        $params['creator_id'] = $creator_id;
        $sql .=" AND (";
        $sql .= $DB->sql_like('user2.lastname', ':lastname', false);
        $params['lastname'] = '%' . $name . '%';
        $sql.=" OR ";
        $sql .= $DB->sql_like('user2.firstname', ':firstname', false);
        $params['firstname'] = '%' . $name . '%';
        $sql.=" OR ";
        $sql .= $DB->sql_like('user2.username', ':username', false);
        $params['username'] = '%' . $name . '%';

        $sql .=")";

        //echo $sql;
        $result = $DB->get_records_sql($sql, $params);
        return $result;
    }
}

//Tung added function for role teacher
function search_member_of_course_second($name, $course_id, $creator_id) {
    global $DB, $USER;
    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

    $id_list = display_children($rows, $USER->id);
    $id_list .= $USER->id;

    //end fix
    if ($name == "") {
        return get_users_from_parent($creator_id);
    } else {
        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent in (" . $id_list . ") OR user2.id in (" . $id_list . "))";
        $params['course_id'] = $course_id;
        //$params['creator_id'] = $creator_id;
        $sql .=" AND (";
        $sql .= $DB->sql_like('user2.lastname', ':lastname', false);
        $params['lastname'] = '%' . $name . '%';
        $sql.=" OR ";
        $sql .= $DB->sql_like('user2.firstname', ':firstname', false);
        $params['firstname'] = '%' . $name . '%';
        $sql.=" OR ";
        $sql .= $DB->sql_like('user2.username', ':username', false);
        $params['username'] = '%' . $name . '%';

        $sql .=")";

        //echo $sql;
        $result = $DB->get_records_sql($sql, $params);
        return $result;
    }
}

//Tung edit: search user all
function search_members_enroll_of_course($course_id, $creator_id, $q) {
    global $DB;
    $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (";
    $sql .= $DB->sql_like('user2.firstname', ':firstname', false);
    $sql.=" OR ";
    $sql.="(";
    $sql .= $DB->sql_like('user2.lastname', ':lastname', false);
    $sql.=")";
    $sql.=" OR ";
    $sql.="(";
    $sql .= $DB->sql_like('user2.email', ':email', false);
    $sql.=")";
    $sql.=")";
    $params['course_id'] = $course_id;
    $params['creator_id'] = $creator_id;
    $params['firstname'] = '%' . $q . '%';
    $params['lastname'] = '%' . $q . '%';
    $params['email'] = '%' . $q . '%';
    $result = $DB->get_records_sql($sql, $params);
    //echo $sql;
    foreach ($result as $key => $val) {
        if (is_user_enroll_in_course($val->id, $course_id) === false) {
            unset($result[$val->id]);
        }
    }
    return $result;
}

//Tung added: search teams for invite course
function search_teams_enroll_of_course($course_id, $creator_id, $q) {
    global $DB;
    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $creator_id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $creator_id;
    
    $field = "id, name, courseid, userid";
    $sql = "SELECT ".$field." FROM {groups} g WHERE (g.userid in (".$id_list.") OR g.courseid = :courseid) 
    AND ";
    $sql .= $DB->sql_like('g.name', ':name', false);
    $params['courseid'] = $course_id;
    $params['name'] = '%' . $q . '%';
    $result = $DB->get_records_sql($sql, $params);
//    var_dump($result);
    
    foreach ($result as $key => $val) {
        if (is_team_enroll_in_course($val->id, $course_id) === false) {
            unset($result[$val->id]);
        }
    }
    return $result;
}

function get_members_enroll_of_course($course_id) {
    global $DB;
    $date = time();
    $sql = "SELECT 
                user.* 
            FROM user
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                JOIN user_enrolments ON user_enrolments.userid = user.id
                JOIN enrol ON enrol.id = user_enrolments.enrolid
                JOIN course ON enrol.courseid = course.id 
            WHERE
                NOT user.id=2
                AND `role_assignments`.`roleid`=5
                AND user.del=0
                AND course.id = {$course_id}
                AND (user.time_end_int=0 OR user.time_end_int > {$date}) 
                ORDER BY user.firstname ASC";
    $result = $DB->get_records_sql($sql);

    return $result;
}

// function remove_person_from_course($person_id, $course_id) {
//     global $DB, $CFG;

//     require_once($CFG->dirroot . '/lib/enrollib.php');
//     $enrol = enrol_get_plugin('manual');
//     $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
//     $instance = reset($instances);

//     $enrol->unenrol_user($instance, $person_id);
// }

//Tung added function for role teacher
function get_members_enroll_of_course_second($course_id, $creator_id) {

    global $DB, $USER;
    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

    $id_list = display_children($rows, $USER->id);
    $id_list .= $USER->id;
    //end fix
    $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent in (" . $id_list . ") OR user2.id in (" . $id_list . "))";
    $params['course_id'] = $course_id;
    //$params['creator_id'] = $creator_id;

    $result = $DB->get_records_sql($sql, $params);
    foreach ($result as $key => $val) {

        if (is_user_enroll_in_course($val->id, $course_id) === false) {
            unset($result[$val->id]);
        }
    }

    return $result;
}

function is_user_enroll_in_course($user_id, $course_id) {
    global $DB;

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);
    if (!$ue = $DB->get_record('user_enrolments', array('enrolid' => $instance->id, 'userid' => $user_id))) {

        return false;
    }
    return true;
}
//Tung added for check team in course
function is_team_enroll_in_course($team_id, $course_id) {
    global $DB;
    $instances = $DB->get_records('group_course', array('course_id' => $course_id), 'id ASC');
    $instance = reset($instances);
    if (!$ue = $DB->get_record('groups', array('id' => $instance->id))) {
        return false;
    }
    return true;
}

function assign_user_to_course($user_id, $course_id) {


    global $CFG, $DB;
    require_once($CFG->dirroot . '/lib/enrollib.php');

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');

    $instance = reset($instances);
    $enrol->enrol_user($instance, $user_id, 5, '', '');
}


function assign_teacher_to_course($user_id, $course_id) {


    global $CFG, $DB;
    require_once($CFG->dirroot . '/lib/enrollib.php');

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');

    $instance = reset($instances);
    $enrol->enrol_user($instance, $user_id, 8, '', '');
}
function assign_teachertutor_to_course($user_id, $course_id) {


    global $CFG, $DB;
    require_once($CFG->dirroot . '/lib/enrollib.php');

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');

    $instance = reset($instances);
    $enrol->enrol_user($instance, $user_id, 10, '', '');
}

function update_teacher_to_course($teacherid,$courseid)
{
    global $CFG, $DB;
    $sql = "UPDATE user_enrolments
            JOIN `user` ON user.id = user_enrolments.userid
            JOIN role_assignments ON role_assignments.userid = user.id
            JOIN enrol ON enrol.id = user_enrolments.enrolid
            SET user_enrolments.userid ={$teacherid}
            WHERE enrol.courseid ={$courseid} AND role_assignments.roleid=8";
    return $DB->execute($sql);
}
function update_teachertutor_to_course($teacherid,$courseid)
{
    global $CFG, $DB;
    $sql = "UPDATE user_enrolments
            JOIN `user` ON user.id = user_enrolments.userid
            JOIN role_assignments ON role_assignments.userid = user.id
            JOIN enrol ON enrol.id = user_enrolments.enrolid
            SET user_enrolments.userid ={$teacherid}
            WHERE enrol.courseid ={$courseid} AND role_assignments.roleid=10";
    return $DB->execute($sql);
}

function get_all_fields_from_table($table_name) {
    global $DB;
    $sql = 'SHOW COLUMNS FROM ' . $table_name;
    $records = $DB->get_records_sql($sql);
    $return_fields = array();
    //var_dump($records);
    foreach ($records as $key => $val) {
        $return_fields[] = $val->field;
    }
    return $return_fields;
}

function get_new_coresponding_forum_id($old_forum_id, $old_course_id, $new_course_id) {
    global $DB;
    $sql = "SELECT fr2.id AS new_forum
FROM forum fr1, forum fr2

WHERE fr1.id=$old_forum_id
AND fr1.type=fr2.type
AND fr1.name=fr2.name
AND fr2.intro=fr2.intro
AND fr1.id <> fr2.id
AND fr1.course=$old_course_id
AND fr2. course=$new_course_id
";
    $record = $DB->get_record_sql($sql);
    //echo $sql."<br/>";
    return $record->new_forum;
}

function re_update_firstpost($new_fd_id, $new_first_post) {
    global $DB;

    $DB->set_field('forum_discussions', 'firstpost', $new_first_post, array('id' => $new_fd_id));
}

function get_new_coresponding_forum_post_id($old_first_post, $new_fd_id) {
    global $DB;
    $sql = "SELECT fp1.id AS new_fp
FROM forum_posts fp1,forum_posts fp2
WHERE
fp1.discussion=$new_fd_id
AND fp1.parent=fp2.parent
AND fp1.userid=fp2.userid
AND fp1.created=fp2.created
AND fp1.modified=fp2.modified
AND fp1.mailed=fp2.mailed
AND fp1.subject=fp2.subject
AND fp1.message=fp2.message
AND fp1.messageformat=fp2.messageformat
AND fp1.messagetrust=fp2.messagetrust
AND fp1.attachment=fp2.attachment
AND fp1.totalscore=fp2.totalscore
AND fp1.mailnow=fp2.mailnow
AND fp1.id<>fp2.id 
AND fp2.id=$old_first_post    
";
    $record = $DB->get_record_sql($sql);
    //echo $sql."<br/>";
    return $record->new_fp;
}

function get_new_coresponding_forum_discussion_id($old_fr_id, $old_course_id, $new_course_id) {
    global $DB;
    $sql = "SELECT fd2.id AS new_fd_id FROM
forum_discussions fd2,
forum_discussions fd1
WHERE
fd1.forum=$old_fr_id
AND fd1.id <> fd2.id
AND fd1.name=fd2.name
AND fd2.course=$new_course_id
AND fd1.course=$old_course_id;
";
    $record = $DB->get_record_sql($sql);
    //echo 'sql='.$sql."<br/>";
    return $record->new_fd_id;
}

function forum_exsit_in_forum_discussion($forum_id, $old_course_id) {
    global $DB;
    $f = $DB->get_record('forum_discussions', array('forum' => $forum_id, 'course' => $old_course_id));

    if ($f->id != '') {
        return 1;
    } else {
        return 0;
    }
}

function get_forum_discussion_from_course($course_id) {
    global $DB;
    $fds = $DB->get_records('forum_discussions', array('course' => $course_id));
    return $fds;
}

function get_forum_from_course($course_id) {
    global $DB;
    $fs = $DB->get_records('forum', array('course' => $course_id));
    return $fs;
}

function copy_forum_subscriptions($old_forum_id, $new_forum_id) {
    global $DB;
    $sql = "insert into forum_subscriptions(userid,forum) select userid,$new_forum_id from forum_subscriptions where forum = " . $old_forum_id;

    $DB->execute($sql);
}

function copy_forum_discussion($new_course_id, $old_course_id, $new_forum_id) {
    global $DB;
    $sql = "insert into forum_discussions(course,forum,name,firstpost,userid,groupid,assessed,timemodified,usermodified,timestart,timeend) select $new_course_id,$new_forum_id,name,firstpost,userid,groupid,assessed,timemodified,usermodified,timestart,timeend from forum_discussions where course=" . $old_course_id;
    $DB->execute($sql);
    return get_last_insert_forum_discussion_id();
}

function get_last_insert_forum_discussion_id() {
    global $DB;
    $record = $DB->get_record_sql("select max(id) as id from forum_discussions");
    return $record->id;
}

function copy_forum_post($new_fd_id, $old_fd_id) {
    global $DB;
    $sql = "insert into forum_posts(discussion,parent,userid,created,modified,mailed,subject,message,messageformat,messagetrust,attachment,totalscore,mailnow) select $new_fd_id,parent,userid,created,modified,mailed,subject,message,messageformat,messagetrust,attachment,totalscore,mailnow  from forum_posts where discussion = " . $old_fd_id;

    $DB->execute($sql);
}

function copy_module($all_fields, $table, $old_id, $new_course_id) {
    global $DB;
    //echo 'table='.$table;
    //echo '$old_id='.$old_id;
    $old_record = $DB->get_record($table, array('id' => $old_id));
    //var_dump($old_record);
    $new_record = new stdClass();
    $new_record->course = $new_course_id;


    foreach ($all_fields as $key => $val) {
        if ($val != 'course' && $val != 'id') {
            $new_record->$val = $old_record->$val;
        }
    }
    //echo 'new recordddddddddd<br/>';
    //var_dump($new_record);

    return $DB->insert_record($table, $new_record);
}

function copy_course_module($new_course_id, $old_cm_id, $instance, $new_section_id) {
    global $DB;
    $old_cm = $DB->get_record('course_modules', array('id' => $old_cm_id));
    $new_cm = new stdClass();

    $new_cm->course = $new_course_id;
    $new_cm->module = $old_cm->module;
    $new_cm->instance = $instance;
    $new_cm->section = $new_section_id;
    $new_cm->idnumber = $old_cm->idnumber;
    $new_cm->added = $old_cm->added;
    $new_cm->score = $old_cm->score;
    $new_cm->indent = $old_cm->indent;
    $new_cm->visible = $old_cm->visible;
    $new_cm->visibleold = $old_cm->visibleold;
    $new_cm->groupmode = $old_cm->groupmode;
    $new_cm->groupingid = $old_cm->groupingid;
    $new_cm->groupmembersonly = $old_cm->groupmembersonly;
    $new_cm->completion = $old_cm->completion;
    $new_cm->completiongradeitemnumber = $old_cm->completiongradeitemnumber;
    $new_cm->completionview = $old_cm->completionview;
    $new_cm->completionexpected = $old_cm->completionexpected;
    $new_cm->availablefrom = $old_cm->availablefrom;
    $new_cm->availableuntil = $old_cm->availableuntil;
    $new_cm->showavailability = $old_cm->showavailability;
    return $DB->insert_record('course_modules', $new_cm);
}

function copy_course_enrol($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO enrol(enrol,status,courseid,sortorder,name,             
enrolperiod,enrolstartdate,enrolenddate,     
expirynotify,expirythreshold,notifyall,        
password,cost,currency,roleid,customint1,       
customint2,customint3,customint4,customchar1,     
customchar2,customdec1,customdec2,customtext1,      
customtext2,timecreated,timemodified)

SELECT enrol,status,$new_course_id,sortorder,name,             
enrolperiod,enrolstartdate,enrolenddate,     
expirynotify,expirythreshold,notifyall,        
password,cost,currency,roleid,customint1,       
customint2,customint3,customint4,customchar1,     
customchar2,customdec1,customdec2,customtext1,      
customtext2,timecreated,timemodified from enrol WHERE courseid=$old_course_id AND enrol='manual'";

    $DB->execute($sql);
}

function update_sequence_course_section($str_sequence, $new_course_id) {
    global $DB;
    $sql = "update course_sections set sequence='$str_sequence' where course=$new_course_id";
    //echo $sql;
    $DB->execute($sql);
}

function copy_course_section($new_course_id, $old_cs_id) {
    global $DB;
    $old_cs = $DB->get_record('course_sections', array('id' => $old_cs_id));
    $new_cs = new stdClass();
    $new_cs->course = $new_course_id;
    $new_cs->section = $old_cs->section;
    $new_cs->name = $old_cs->name;
    $new_cs->summary = $old_cs->summary;
    $new_cs->summaryformat = $old_cs->summaryformat;
    $new_cs->visible = $old_cs->visible;


    return $DB->insert_record('course_sections', $new_cs);
}

function copy_group_course($old_course_id, $new_course_id) {
    global $DB;
    $sql = "insert into group_course(course_id,group_id) select $new_course_id,group_id from group_course where course_id=" . $old_course_id;
    $DB->execute($sql);
}

function copy_course_document($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO document(new_name,file_name,course_id)
        SELECT new_name,file_name,$new_course_id from document where course_id=$old_course_id";
    $DB->execute($sql);
}

function copy_course_noticeboard($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO noticeboard(course,title,content,timemodified)
        SELECT $new_course_id,title,content,timemodified  from noticeboard where course=$old_course_id";

    $DB->execute($sql);
}

function clone_course($old_course_id) {
    global $DB;
    $sql = "INSERT INTO course (category,sortorder,fullname,shortname,idnumber,summary,summaryformat,           
format,showgrades,modinfo,newsitems,startdate,numsections,marker,maxbytes,                
legacyfiles,showreports,visible,visibleold,hiddensections,groupmode,               
groupmodeforce,defaultgroupingid,lang,theme,timecreated,timemodified,            
requested,restrictmodules,enablecompletion,completionstartonenrol,completionnotify)
    (SELECT category,sortorder,fullname,shortname,idnumber,summary,summaryformat,           
format,showgrades,modinfo,newsitems,startdate,numsections,marker,maxbytes,                
legacyfiles,showreports,visible,visibleold,hiddensections,groupmode,               
groupmodeforce,defaultgroupingid,lang,theme,timecreated,timemodified,            
requested,restrictmodules,enablecompletion,completionstartonenrol,completionnotify FROM course WHERE id=$old_course_id)";

    $DB->execute($sql);
    return get_last_insert_course_id();
}

function get_last_insert_course_id() {
    global $DB;
    $record = $DB->get_record_sql("select max(id) as id from course");
    return $record->id;
}

function get_last_insert_course_section_id() {
    global $DB;
    $record = $DB->get_record_sql("select max(id) as id from course_sections");
    return $record->id;
}

function copy_course($name, $description, $code, $active_hidden, $hidden_lirary, $module_order, $enable_full_screen_hidden, $complete_by, $time_span, $time_spend_type, $date2, $apply_due_date_hidden, $compliant_for, $automatic_resit, $file_name, $sell_course, $course_sales_date, $course_currency
, $course_fee, $sale_description, $sale_full_description) {
    global $DB, $CFG, $USER;

    $course = new stdClass();

    $course->fullname = $name;
    $course->shortname = $name;
    $course->idnumber = $code;
    $course->summary = $description;
    $course->category = 1;
    $course->timecreated = time();
    $course->numsections = 0;

    $course_obj = create_course($course);

    $setting = new stdClass();

    $setting->course_id = $course_obj->id;
    $setting->course_library = $hidden_lirary;
    $setting->active = $active_hidden;
    $setting->module_order = $module_order;
    $setting->new_window = $enable_full_screen_hidden;
    $setting->update_complete_date = $apply_due_date_hidden;
    $setting->compliant_for = $compliant_for;
    $setting->automatic_resit = $automatic_resit;
    $setting->file_name = $file_name;
    $setting->sell_course = $sell_course;
    $setting->course_sales_date = $course_sales_date;
    $setting->currency = $course_currency;
    $setting->fee = $course_fee;
    $setting->sale_description = $sale_description;
    $setting->sale_full_description = $sale_full_description;
    if ($complete_by == 1) {
        if ($time_span != '') {
            $setting->time_span_complete = 1;
            $setting->time_span_complete_number = $time_span;
            $setting->time_span_complete_type = $time_spend_type;  //days, weeks
            $setting->complete_specific_date = null;
        }
    }

    if ($complete_by == 2) {
        if ($date2) {
            $setting->time_span_complete = 2;
            $setting->complete_specific_date = $date2;
            $setting->time_span_complete_number = 0;
            $setting->time_span_complete_type = 0;
        }
    }

    $DB->insert_record('course_settings', $setting);

    //enroll creator to course
    require_once ($CFG->dirroot . "/lib/enrollib.php");

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_obj->id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);
    $enrol->enrol_user($instance, $USER->id, $course_obj->id, '', '');

    return $course_obj->id;
}

function insert_course_settings($course_id, $active_hidden, $hidden_lirary, $module_order, $enable_full_screen_hidden, $complete_by, $time_span, $time_spend_type, $date2, $apply_due_date_hidden, $compliant_for, $automatic_resit, $file_name, $course_currency
, $course_fee, $sale_description, $sale_full_description) {
    global $DB;

    $setting = new stdClass();
    $setting->course_id = $course_id;
    $setting->course_library = $hidden_lirary;
    $setting->active = $active_hidden;
    $setting->module_order = $module_order;
    $setting->new_window = $enable_full_screen_hidden;
    $setting->update_complete_date = $apply_due_date_hidden;
    $setting->compliant_for = $compliant_for;
    $setting->automatic_resit = $automatic_resit;
    $setting->file_name = $file_name;
    $setting->sell_course = ($sell_course === "on" ? 1 : 0);
    if ($setting->sell_course == 1) {
        $setting->currency = $course_currency;
        $setting->fee = $course_fee;
        $setting->sale_description = $sale_description;
        $setting->sale_full_description = $sale_full_description;
        $setting->course_sales_date = date("Y-m-d");
    } else {
        $setting->currency = $course_currency;
        $setting->fee = 0;
        $setting->sale_description = "";
        $setting->sale_full_description = "";
        $setting->course_sales_date = NULL;
    }
    if ($complete_by == 1) {
        if ($time_span != '') {
            $setting->time_span_complete = 1;
            $setting->time_span_complete_number = $time_span;
            $setting->time_span_complete_type = $time_spend_type;  //days, weeks
            $setting->complete_specific_date = null;
        }
    }

    if ($complete_by == 2) {
        if ($date2) {
            $setting->time_span_complete = 2;
            $setting->complete_specific_date = $date2;
            $setting->time_span_complete_number = 0;
            $setting->time_span_complete_type = 0;
        }
    }


    $DB->insert_record('course_settings', $setting);
}

function get_course_settings($course_id) {

    global $DB;
    $course_setting = $DB->get_record('course_settings', array(
        'course_id' => $course_id
    ));

    return $course_setting;
}

function del_course($course_id) {
    global $DB;
    $course = $DB->get_record('course', array('id' => $course_id));
    delete_course($course);
    $DB->delete_records('course_settings', array('course_id' => $course_id));
}

function update_course_settings($course_id, $name,$sotiet,$gvnn,$gvtg, $description) {
    global $DB;
    $record = new stdClass();
    $record->id = $course_id;
    $record->shortname = trim($name);
    $record->fullname = trim($name);
    $record->sotiet = trim($sotiet);
    $record->summary = $description;
    // $record->idnumber = $code;
    $record->id_gv = $gvnn;
    $record->id_tg = $gvtg;
    $record->enablecompletion = 1;
    return $DB->update_record('course', $record, false);

    //course settings
    // $auto_increment_id = is_exist_course_settings($course_id);

    // if ($auto_increment_id) {
    //     $record = new stdClass();
    //     $record->course_id = $course_id;
    //     $record->active = $active_hidden;
    //     $record->course_library = $hidden_library;
    //     $record->module_order = $module_order;
    //     $record->new_window = $enable_full_screen_hidden;
    //     if ($complete_by == 1) {
    //         if ($time_span != '') {
    //             $record->time_span_complete = 1;
    //             $record->time_span_complete_number = $time_span;
    //             $record->time_span_complete_type = $time_spend_type;  //days, weeks
    //             $record->complete_specific_date = null;
    //         }
    //     }
    //     if ($complete_by == 2) {
    //         if ($date2) {
    //             $record->time_span_complete = 2;
    //             $record->complete_specific_date = $date2;
    //             $record->time_span_complete_number = 0;
    //             $record->time_span_complete_type = 0;
    //         }
    //     }
    //     $record->compliant_for = $compliant_for;
    //     $record->automatic_resit = $automatic_resit;
    //     $record->update_complete_date = $apply_due_date_hidden;
    //     // $record->file_name = $file_name;
    //     $record->sell_course = ($sell_course === "on" ? 1 : 0);
    //     if ($record->sell_course == 1) {
    //         $record->currency = $course_currency;
    //         $record->fee = $course_fee;
    //         $record->sale_description = $sale_description;
    //         $record->sale_full_description = $sale_full_description;
    //         $course_setting = get_course_settings($course_id);
    //         if ($course_setting->sell_course == 0) {
    //             $record->course_sales_date = date("Y-m-d");
    //         }
    //     } else {
    //         $record->currency = $course_currency;
    //         $record->fee = 0;
    //         $record->sale_description = "";
    //         $record->sale_full_description = "";
    //         $record->course_sales_date = NULL;
    //     }
    //     $record->id = $auto_increment_id;

    //     $DB->update_record('course_settings', $record, false);
    // } else {
    //     insert_course_settings($course_id, $active_hidden, $hidden_library, $module_order, $enable_full_screen_hidden, $complete_by, $time_span, $time_spend_type, $date2, $apply_due_date_hidden, $compliant_for, $automatic_resit, $sell_course, $course_currency, $course_fee, $sale_description, $sale_full_description);
    // }
    // if ($hidden_library == 1) { //Allow guest access               
    //     $auto_increment_enrolid = is_exist_enrol($course_id, 'guest');

    //     $enrol = new stdClass();
    //     $enrol->enrol = 'guest';
    //     $enrol->status = 0;
    //     $enrol->courseid = $course_id;
    //     $enrol->sortorder = 1;
    //     $enrol->enrolstartdate = 0;
    //     $enrol->enrolenddate = 0;
    //     $enrol->expirynotify = 0;
    //     $enrol->expirythreshold = 0;
    //     $enrol->notifyall = 0;
    //     $enrol->roleid = 0;
    //     $enrol->timecreated = time();
    //     $enrol->timemodified = time();

    //     if ($auto_increment_enrolid) {
    //         $enrol->id = $auto_increment_enrolid;
    //         $DB->update_record('enrol', $enrol, false);
    //     } else {
    //         $DB->insert_record('enrol', $enrol);
    //     }
    // }
}

function is_exist_course_settings($course_id) {
    global $DB;
    $course_setting = $DB->get_record('course_settings', array(
        'course_id' => $course_id
    ));

    return $course_setting->id;
}

function is_exist_enrol($course_id, $enrol) {
    global $DB;

    $params = array();
    $sql = 'SELECT * FROM enrol WHERE courseid = :courseid AND enrol = :enrol';
    $params['courseid'] = $course_id;
    $params['enrol'] = $enrol;
    $result = $DB->get_records_sql($sql, $params);

    return $result->id;
}

// function get_team_from_id($team_id) {
//     global $DB;
//     $team = $DB->get_record('groups', array(
//         'id' => $team_id
//     ));

//     return $team;
// }

function search_team_from_name($name) {
    global $DB, $USER;
    $params = array();
    $sql = 'SELECT * FROM groups where userid = ' . $USER->id . " AND ";
    $sql .= $DB->sql_like('name', ':name', false);
    $params['name'] = '%' . $name . '%';
    //echo $sql;
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function insert_member_to_group($user_id, $group_id) {
    global $DB;
    $groups_members = new stdClass();
    $groups_members->groupid = $group_id;
    $groups_members->userid = $user_id;
    $groups_members->timeadded = time();
    $DB->insert_record('groups_members', $groups_members);
}

function get_teams_not_belong_to_user_id($user_id) {
    global $DB, $USER;
    $sql = 'SELECT * from groups where userid=' . $USER->id . ' and id not in ( select groupid from groups_members where userid = ' . $user_id . ")";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_teams_belong_to_user_id($user_id) {
    global $DB, $USER;
    $sql = 'SELECT groupid from groups_members where userid = ' . $user_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function update_user_role($user_id, $new_role_id) {
    global $DB;
    $context = get_context_instance(CONTEXT_SYSTEM);
    $sql = 'update role_assignments set roleid=' . $new_role_id . ',contextid= ' . $context->id . ' where userid=' . $user_id;
    $DB->execute($sql);
}

function people_get_roles($uid, $ndid = null) {
    global $COURSE;
    //$context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
    $context = get_context_instance(CONTEXT_SYSTEM);


    if ($roles = get_user_roles($context, $uid)) {

        foreach ($roles as $role) {
            return $role->name;
        }
    }
    return '';
}

function remove_team_member($user_id, $team_id) {
    global $DB;
    $conditions = array(
        'userid' => $user_id,
        'groupid' => $team_id
    );

    $DB->delete_records('groups_members', $conditions);
}

//tung added for add map_user_admin
function add_map_user_admin($ownerId, $userId) {
    global $DB;
    $record = new stdClass();
    $record->owner_id = $ownerId;
    $record->user_id = $userId;
    $new_map_user_admin_id = $DB->insert_record('map_user_admin', $record);
    return $new_map_user_admin_id;
}

function add_profile($firstname,$lastname,$code,$typeoflabor,$schoolid,$email,$profile_img,$phone2,$username,$password,$area,$country,$name_rht,$name_ac,$time_start,$time_end,$description,$parentId) {
    global $DB, $USER;
    $record = new stdClass();

    $record->firstname = trim($firstname);
    $record->lastname = trim($lastname);
    // $record->birthday = trim($birthday);
    $record->email = trim($email);
    $record->profile_img = $profile_img;
    $record->code = trim($code);
    $record->typeoflabor = $typeoflabor;
    $record->schoolid = $schoolid;
    $record->phone2 = trim($phone2);

    $record->username = trim($username);
    $password = hash_internal_user_password($password);
    $record->password = trim($password);
    $record->area = $area;
    // kích hoạt hoặc khóa tài khoản
    $record->confirmed = 1;
    $record->mnethostid = 1;

    $record->name_rht = trim($name_rht);
    $record->name_ac = trim($name_ac);
    $record->country = trim($country);
    $record->time_start = $time_start;
    $record->time_end = $time_end;
    $record->description = $description;
    $record->parent = $parentId;

    $new_user_id = $DB->insert_record('user', $record);
    return $new_user_id;
}

function user_rhta($userid,$rhta)
{
    global $DB, $USER;
    $record = new stdClass();
    $record->userid = $userid;
    $record->rhta = $rhta;
    return $DB->insert_record('user_rhta', $record);
}

function update_my_profile2($user_id, $firstname, $lastname, $username, $password, $address, $city, $state, $postcode, $country, $timezone, $title, $company, $email, $phone1, $phone2, $skype, $twitter, $website) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->firstname = $firstname;
    $record->lastname = $lastname;
    $record->username = $username;
    $password = hash_internal_user_password($password);
    $record->address = $address;
    $record->city = $city;
    $record->state = $state;
    $record->postcode = $postcode;
    $record->country = $country;
    $record->timezone = $timezone;
    $record->title = $title;
    $record->company = $company;
    $record->email = $email;
    $record->phone1 = $phone1;
    $record->phone2 = $phone2;
    $record->skype = $skype;
    $record->twitter = $twitter;
    $record->website = $website;

    return $DB->update_record('user', $record, false);
}

function update_my_profile($user_id, $firstname, $lastname, $username,$birthday, $address, $city,$district,$ward, $state, $postcode, $country, $timezone, $trial, $title, $company, $email, $phone1, $phone2, $skype, $twitter, $website) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->firstname = $firstname;
    $record->state = $state;
    $record->postcode = $postcode;
    $record->website = $website;
    $record->lastname = $lastname;
    $record->username = $username;
    $record->birthday = $birthday;
//  $password = hash_internal_user_password($password);
    //$record->password = $password;
    $record->address = $address;
    $record->city = $city;
    $record->id_district = $district;
    $record->id_wards = $ward;
//  $record->zip = $zip;
    $record->country = $country;
    $record->timezone = $timezone;
    $record->title = $title;
    $record->company = $company;
    $record->email = $email;
    $record->phone1 = $phone1;
    $record->phone2 = $phone2;
    $record->skype = $skype;
    $record->twitter = $twitter;
    $record->website = $website;
    $record->trial = $trial;
    return $DB->update_record('user', $record, false);
}

function update_profile($user_id, $firstname,$lastname,$code,$schoolid,$email,$phone2,$username,$birthday,$country,$name_rht,$name_ac,$time_start,$time_end,$description) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->firstname = $firstname;
    $record->lastname = $lastname;
    $record->birthday = $birthday;
    $record->email = $email;
    $record->code = $code;
    $record->schoolid = $schoolid;
    $record->phone2 = $phone2;

    $record->username = $username;
    
    // kích hoạt hoặc khóa tài khoản
    $record->confirmed = 1;

    $record->name_rht =$name_rht;
    $record->name_ac =$name_ac;
    $record->country = $country;
    $record->time_start = $time_start;
    $record->time_end = $time_end;
    $record->description = $description;

    return $DB->update_record('user', $record, false);
}

function update_user_theme($user_id, $show_logo_on_login, $show_logo_on_trainee_toolbar, $color_title_bar, $color_title_bar_text, $color_navigation_heading, $color_heading_text, $color_login_bg, $font) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->show_logo_on_login = $show_logo_on_login;
    $record->show_logo_on_trainee_toolbar = $show_logo_on_trainee_toolbar;
    $record->title_bar_color = $color_title_bar;
    $record->title_bar_text_color = $color_title_bar_text;
    $record->navigation_heading_color = $color_navigation_heading;
    $record->heading_text_color = $color_heading_text;
    $record->login_page_background = $color_login_bg;
    $record->font = $font;
    return $DB->update_record('user', $record, false);
}

function get_user_from_user_id($user_id) {
    global $DB;
    $user = $DB->get_record('user', array(
        'id' => $user_id
    ));

    return $user;
}

function update_user_logo($user_id, $file_name) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->picture = 1;
    $record->url = $file_name;

    return $DB->update_record('user', $record, false);
}

function update_user_favico($user_id, $file_name) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->favicon = $file_name;


    return $DB->update_record('user', $record, false);
}

function update_user_ipad_icon($user_id, $file_name) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->ipad_icon = $file_name;


    return $DB->update_record('user', $record, false);
}

function displayJsAlert($ms, $redirectPath) {
    $html = '';
    if ($ms != '') {
        $html .= '<div class="alert alert-success">'.$ms.'</div>';
    }

    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}

function update_user_label_message($user_id, $hide_message_tab, $unretricted_messages, $login_message, $welcome_message, $hide_course_date) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->hide_message_tab = $hide_message_tab;
    $record->unretricted_messages = $unretricted_messages;
    $record->login_message = $login_message;
    $record->welcome_message = $welcome_message;
    $record->hide_course_date = $hide_course_date;


    return $DB->update_record('user', $record, false);
}

//Tung added update data for account ecommerce
function update_user_ecommerce($user_id, $enable_ecommerce, $payment_provider, $paypal_email, $payment_username, $payment_password, $curencies, $short_profile, $account_number) {
    global $DB;

    $record = new stdClass();
    $record->id = $user_id;
    $record->enable_ecommerce = $enable_ecommerce;
    $record->ecommerce_provider_type = $payment_provider;
    $record->paypal_id = $paypal_email;
    $record->payment_username = $payment_username;
    $record->payment_password = $payment_password;
    $record->ecommerce_currency_codes = $curencies;
    $record->organisation_profile = $short_profile;
    $record->google_analytics_id = $account_number;
    return $DB->update_record('user', $record, false);
}

//Tung added update data for account Terms
function update_user_term($user_id, $our_term, $show_confirmation_learner, $show_confirmation_ecommerce) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->our_term = $our_term;
    $record->show_confirmation_learner = $show_confirmation_learner;
    $record->show_confirmation_ecommerce = $show_confirmation_ecommerce;

    return $DB->update_record('user', $record, false);
}

//Tung added
function update_user_currency($user_id, $currency) {
    global $DB;
    $sql = "UPDATE user set ecommerce_currency_codes = " . $currency . "WHERE id =" . $user_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function parseCsv($file) {
    global $CFG;
    $row = 0;
    $userNameExist = "";
    $emailExist = "";
    $ms_error = "";
    //Username, Email,First Name, Last Name, Password [Optional], Phone [Optional], Mobile [Optional], Skype [Optional],Job Title[Optional],Company Name[Optional],WebSite[Optional],Twitter[Optional],Team1 [Optional],Team2 [Optional],Team3 [Optional],Team4 [Optional],Team5 [Optional],Course1 [Optional],Course2 [Optional],Course3 [Optional]

    if (($handle = fopen($file, "r")) !== FALSE) {
        $countInsert = 0;
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                $lastinserid = 0;
                if ($row > 0) {
                    //$arrData = explode(',', $data[$c]);
                    $arrData = $data;

                    $username = $arrData[0];

                    $email = $arrData[1];
                    $firstname = $arrData[2];

                    $lastname = $arrData[3];
                    $password = $arrData[4];
                    $phone = $arrData[5];
                    $mobile = $arrData[6];
                    $skype = $arrData[6];
                    $job_title = $arrData[7];
                    $company_name = $arrData[8];
                    $website = $arrData[9];
                    $twitter = $arrData[10];
                    $team_id1 = $arrData[11];
                    $team_id2 = $arrData[12];
                    $team_id3 = $arrData[13];
                    $team_id4 = $arrData[14];
                    $team_id5 = $arrData[15];
                    $course_id1 = $arrData[16];
                    $course_id2 = $arrData[17];
                    $course_id3 = $arrData[18];

                    $error = 0;

                    if ($firstname == "") {
                        $temp_row = $row + 1;
                        $ms_error.= "\n at row " . $temp_row . " firstname is required ";
                        $error = 1;
                    }


                    if ($lastname == "") {
                        $temp_row = $row + 1;
                        $ms_error.= "\n at row " . $temp_row . " lastname is required ";
                        $error = 1;
                    }


                    if (check_username_exist($username)) {
                        $error = 1;
                        $temp_row = $row + 1;
                        $ms_error.= "\n at row " . $temp_row . " $username is already existed ";
                    }

                    if ($email != '' && check_email_exist($email)) {
                        $error = 1;
                        $temp_row = $row + 1;
                        $ms_error.= "\n at row " . $temp_row . " $email is already existed tung";
                    }

                    if ($username != '' && $error == 0) {

                        $lastinserid = import_user($username, $email, $firstname, $lastname, $password, $phone, $mobile, $skype, $job_title, $company_name, $website, $twitter);
                    }

                    if ($lastinserid > 0) {
                        $countInsert++;
                        if ($team_id1) {
                            insert_member_to_group($lastinserid, $team_id1);
                        }
                        if ($team_id2) {
                            insert_member_to_group($lastinserid, $team_id2);
                        }
                        if ($team_id3) {
                            insert_member_to_group($lastinserid, $team_id3);
                        }
                        if ($team_id4) {
                            insert_member_to_group($lastinserid, $team_id4);
                        }
                        if ($team_id5) {
                            insert_member_to_group($lastinserid, $team_id5);
                        }
                        if ($course_id1 != '') {
                            enrol_user($lastinserid, $course_id1);
                        }
                        if ($course_id2 != '') {
                            enrol_user($lastinserid, $course_id2);
                        }
                        if ($course_id3 != '') {
                            enrol_user($lastinserid, $course_id3);
                        }
                    }
                }
            }
            $row++;
        }
        fclose($handle);
        $filenameResultImportSalesman = " users imported";

        //echo displayJsAlert($ms_error, '');
        //echo displayJsAlert($countInsert . $filenameResultImportSalesman, $CFG->wwwroot."/manage/import_people.php");
        echo displayJsAlert('', $CFG->wwwroot . "/manage/import_people.php");
    }
}

function get_never_login_users() {
    global $DB, $USER;
    $sql = 'SELECT * from user where parent = ' . $USER->id . " and id!=1 and firstaccess=0";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function check_email_exist($name) {

    global $DB;
    $user = $DB->get_record('user', array(
        'email' => $name
    ));

    return $user->id;
}

function check_username_exist($name) {

    global $DB;
    $salesman = $DB->get_record('user', array(
        'username' => $name
    ));

    return $salesman->id;
}

function enrol_user($user_id, $course_id) {
    global $DB, $CFG;
    require_once ($CFG->diroot . "/lib/enrollib.php");
    $enrol = enrol_get_plugin('manual');
    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);
    $enrol->enrol_user($instance, $user_id, 5, '', '');
}

function import_user($username, $email, $firstname, $lastname, $password = "", $phone = "", $mobile = "", $skype = "", $job_title = "", $company_name = "", $website = "", $twitter = ""
) {
    global $DB, $USER;

    $user = new stdClass();
    $user->id = -1;
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->deleted = 0;
    $user->timecreated = time();

    $user->username = $username;
    $user->mnethostid = 1;
    $user->firstname = $firstname;
    $user->lastname = $lastname;
    $user->email = $email;
    $user->parent = $USER->id;
    if ($password != '') {
        $user->password = hash_internal_user_password($password);
    }
    if ($phone != '') {
        $user->phone1 = $phone;
    }
    if ($mobile != '') {
        $user->phone2 = $mobile;
    }
    if ($skype != '') {
        $user->skype = $skype;
    }
    if ($job_title != '') {
        $user->title = $job_title;
    }
    if ($company_name != '') {
        $user->company = $company_name;
    }
    if ($website != '') {
        $user->url = $website;
    }
    if ($twitter != '') {
        $user->twitter = $twitter;
    }




    $lastinsertid = $DB->insert_record('user', $user);

    return $lastinsertid;
}

global $CFG;

include ($CFG->dirroot . '/lib/phpmailer/class.phpmailer.php');

function sendEmail($to, $from, $from_name, $subject, $body) {
    global $CFG;
    include ($CFG->dirroot . '/teams/config.php');
    global $error;

    $mail = new PHPMailer(); // create a new object

    $mail->IsSMTP(); // enable SMTP
    //$mail->IsHTML=true;
    $mail->IsHTML(true);
    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 465;
    $mail->Username = 'tung@thienhoang.com.vn'; //$mailUsername;
    $mail->Password = 'T12345678'; //$pass;
    $mail->SetFrom($from, $from_name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AddAddress($to);
    $mail->CharSet = "utf-8";
    if (!$mail->Send()) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return false;
    } else {
        $error = 'Message sent!';
        return true;
    }
}

function update_user_password($user_id) {
    global $DB;
    $passwordNoEncode = randPass(8);
    $password = hash_internal_user_password($passwordNoEncode);
    //$password = md5($passwordNoEncode);
    if ($DB->set_field('user', 'password', $password, array('id' => $user_id))) {
        return $passwordNoEncode;
    }
}

/*
  function update_user_new_password($newpassword) {
  global $DB;
  $password = hash_internal_user_password($newpassword);
  //$password = md5($newpassword);
  if ($DB->set_field('user', 'password', $password, array('id' => $user_id))) {
  return $newpassword;
  }
  }
 */

function randPass($length, $strength = 8) {
    $vowels = 'aeuy';
    $consonants = 'bdghjmnpqrstvz';
    if ($strength >= 1) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
    }
    if ($strength >= 2) {
        $vowels .= "AEUY";
    }
    if ($strength >= 4) {
        $consonants .= '23456789';
    }
    if ($strength >= 8) {
        $consonants .= '@#$%';
    }

    $password = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $password;
}

function search_complete_status_second($status, $course_id, $creator_id) {
    global $DB, $CFG, $USER;

    if ($status == "") {
        return get_members_enroll_of_course_second($course_id, $creator_id);
    } else {
        $course = $DB->get_record('course', array('id' => $course_id));
        $user_complete_arr = array();
        $user_un_complete_arr = array();

        //include($CFG->dirroot . '/lib/completionlib.php');

        $info = new completion_info($course);

        $members = get_members_enroll_of_course_second($course_id, $creator_id);
        $count_complete = 0;
        $count_in_complete = 0;
        foreach ($members as $key => $val) {
            $coursecomplete = $info->is_course_complete($val->id);
            if ($coursecomplete) {
                $user_complete_arr[] = $val->id;
                $count_complete++;
            } else {
                $user_un_complete_arr[] = $val->id;
                $count_in_complete++;
            }
        }

        if ($status == 0 && $count_in_complete == 0) {
            return null;
        }
        if ($status == 1 && $count_complete == 0) {
            return null;
        }
        //Tung added
        $sql1 = "SELECT * FROM user ";
        $rows = $DB->get_records_sql($sql1);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

        $id_list = display_children($rows, $USER->id);
        $id_list .= $USER->id;

        //end fix

        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent=:creator_id OR user2.id in (" . $id_list . "))";
        if ($status == 0) {
            $sql.=" AND user2.id in (" . implode(",", $user_un_complete_arr) . ')';
        }
        if ($status == 1) {
            $sql.=" AND user2.id in (" . implode(",", $user_complete_arr) . ')';
        }
        $params['course_id'] = $course_id;
        $params['creator_id'] = $creator_id;
        //echo 'sql='.$sql;
        $result = $DB->get_records_sql($sql, $params);

        return $result;
    }
}

function search_complete_status($status, $course_id, $creator_id) {
    global $DB;
    if ($status == "") {
        return get_members_enroll_of_course($course_id, $creator_id);
    } else {
        $course = $DB->get_record('course', array('id' => $course_id));
        $user_complete_arr = array();
        $user_un_complete_arr = array();

        //include($CFG->dirroot . '/lib/completionlib.php');

        $info = new completion_info($course);

        $members = get_members_enroll_of_course($course_id, $creator_id);
        $count_complete = 0;
        $count_in_complete = 0;
        foreach ($members as $key => $val) {
            $coursecomplete = $info->is_course_complete($val->id);
            if ($coursecomplete) {
                $user_complete_arr[] = $val->id;
                $count_complete++;
            } else {
                $user_un_complete_arr[] = $val->id;
                $count_in_complete++;
            }
        }

        if ($status == 0 && $count_in_complete == 0) {
            return null;
        }
        if ($status == 1 && $count_complete == 0) {
            return null;
        }


        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id";
        if ($status == 0) {
            $sql.=" AND user2.id in (" . implode(",", $user_un_complete_arr) . ')';
        }
        if ($status == 1) {
            $sql.=" AND user2.id in (" . implode(",", $user_complete_arr) . ')';
        }
        $params['course_id'] = $course_id;
        $params['creator_id'] = $creator_id;
        //echo 'sql='.$sql;
        $result = $DB->get_records_sql($sql, $params);

        return $result;
    }
}

//Tung added for role super admin
function select_account() {
    global $DB;
    $sql = "SELECT * FROM user WHERE parent=0";

    $result = $DB->get_records_sql($sql);
    return $result;
}

function select_account_id($id) {
    global $DB;
    $sql = "SELECT * FROM user WHERE id =" . $id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_course_modules($course_id) {

    global $DB;
    $course_modules = $DB->get_record('course_modules', array(
        'course' => $course_id
    ));

    return $course_modules;
}

//tung added

function user_all_inactive() {
    global $DB;
    $sql = "UPDATE user set confirmed = 0";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function time_select() {
    $date_han = 1351554089;
    $now = time();
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

    $now = time();

    $difference = $now - $date_han;
    //$tense = "ago";

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if ($difference != 1) {
        $periods[$j].= "s";
    }
    if ($difference == 10 & $periods[$j] == 'day') {
        user_all_inactive();
    }
    return " $difference $periods[$j] ";
}

/**
 * Tung added
 * service package 1,2,3,4,5,6,7,8
 * Infomation service package
 */
function get_service_package($sp) {
    global $DB;
    $result = $DB->get_record('service_pack', array(
        'spid' => $sp
    ));
    return $result;
}

function get_service_package_superadmin() {
    global $DB;
    $sql = "SELECT * FROM service_pack";
    $result = $DB->get_records_sql($sql);
    return $result;
}

/**
 * Tung added
 * Get information payment by user_id
 */
function get_payment_user_id($user_id) {
    global $DB;
    $sql = "SELECT * FROM payment WHERE user_id =" . $user_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

/**
 * Get infermation payment by superadmin
 * 
 */
function get_payment_superadmin() {
    global $DB;
    $sql = "SELECT * FROM payment";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_payment_user_superadmin() {
    global $DB;
    $sql = "SELECT p.*, u.firstname, u.lastname, sp.name  FROM payment p 
    JOIN user AS u ON p.user_id = u.id 
    JOIN service_pack sp ON p.spid = sp.spid
GROUP BY p.id";
    $result = $DB->get_records_sql($sql);
    return $result;
}

/**
 * Tung added
 * Insert Information payment
 */
function insert_payment($user_id, $status, $p_method, $spid, $p_start, $p_end, $p_cost) {
    global $DB;
    $payment = new stdClass();
    $payment->user_id = $user_id;
    $payment->status = $status;
    $payment->p_method = $p_method;
    $payment->spid = $spid;
    $payment->p_start = $p_start;
    $payment->p_end = $p_end;
    $payment->p_cost = $p_cost;
    return $DB->insert_record('payment', $payment);
}

/**
 * Tung added
 * Update payment
 */
function update_payment($user_id, $file_name) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->profile_img = $file_name;
    return $DB->update_record('user', $record, false);
}

/**
 * Update payment by superadmin
 */
function update_payment_by_superadmin($id, $spidname, $startdate, $enddate, $pmethod, $status) {
    global $DB;
    $record = new stdClass();
    $record->id = $id;
    $record->spid = $spidname;
    $record->p_start = $startdate;
    $record->p_end = $enddate;
    $record->p_method = $pmethod;
    $record->status = $status;
    return $DB->update_record('payment', $record, false);
}

/**
 * Tung added
 * get payment lastest
 */
function get_payment_lastest($user_id) {
    global $DB;
    $sql = "SELECT * FROM payment WHERE user_id =" . $user_id . " ORDER BY id DESC LIMIT 1";
    $result = $DB->get_records_sql($sql);

    foreach ($result as $key => $result1) {
        $result = $result1;
    }
    return $result;
}

/**
 * Tung Added
 * insert service pack 
 */
function insert_service_pack($name, $code, $cost, $method, $useractive, $email, $logo, $support) {
    global $DB;
    $servicepack = new stdClass();
    $servicepack->name = $name;
    $servicepack->spid = $code;
    $servicepack->cost = $cost;
    $servicepack->method = $method;
    $servicepack->user_active = $useractive;
    $servicepack->email = $email;
    $servicepack->logo = $logo;
    $servicepack->support = $support;
    $servicepack->create = time();
    return $DB->insert_record('service_pack', $servicepack);
}

/**
 * Tung added
 * Get Service pack by id
 */
function get_service_package_by_id($id) {
    global $DB;
    $result = $DB->get_record('service_pack', array(
        'id' => $id
    ));
    return $result;
}

/**
 * Get Service pack by spid
 */
function get_service_pack_by_spid($spid) {
    global $DB;
    $result = $DB->get_record('service_pack', array(
        'spid' => $spid,
    ));
    return $result;
}

/**
 * Tung Added
 * Update service pack
 */
function update_service_pack($id, $code, $name, $cost, $method, $useractive, $email, $logo, $support) {
    global $DB;
    $record = new stdClass();
    $record->id = $id;
    $record->spid = $code;
    $record->name = $name;
    $record->cost = $cost;
    $record->user_active = $useractive;
    $record->method = $method;
    $record->email = $email;
    $record->logo = $logo;
    $record->support = $support;
    return $DB->update_record('service_pack', $record, false);
}

/**
 * Get payment by id
 */
function get_payment_by_id($id) {
    global $DB;
    $result = $DB->get_record('payment', array(
        'id' => $id
    ));
    return $result;
}

/**
 * Delete service pack
 */
function delete_service_pack($id) {
    global $DB;
    $DB->delete_records_select('service_pack', " id = $id ");
}

/**
 * Delete payment
 */
function delete_payment($id) {
    global $DB;
    $DB->delete_records_select('payment', "id = $id");
}






/**
 * Chien Added
 * City,district
 */
function get_city(){
  global $DB;
    $city = $DB->get_records('tinhthanh');
    return $city;
}

function get_district($city){
  global $DB;
  if(!empty($city)){
    $district = $DB->get_records('quanhuyen', array('idTinhThanh' => $city));
    return $district;
  }
}

function get_commune($district=null){
  global $DB;
  if(!empty($district)){
    $commune = $DB->get_records('xaphuong', array('idQuanHuyen' => $district));
    return $commune;
  }
}


function get_info_tinhthanh($id){
    global $DB;
    if (!empty($id)) {
        # code...
        $tinhthanh = $DB->get_record('tinhthanh', array(
            'id' => $id
        ));
        return $tinhthanh;
    }
    
}

function get_info_quanhuyen($id){
    global $DB;
    if (!empty($id)) {
        $quanhuyen = $DB->get_record('quanhuyen', array(
            'id' => $id
        ));
        return $quanhuyen;
    }
    
}

function get_info_xaphuong($id){
    global $DB;
    if (!empty($id)) {
        $xaphuong = $DB->get_record('xaphuong', array(
            'id' => $id
        ));
        return $xaphuong;
    }
}

function get_info_giaovien($id){
    global $DB;
        $giaovien = $DB->get_record('user', array(
            'id' => $id
        ));
    return $giaovien;
    
}
function get_khoi($school){
  global $DB;
  if(!empty($school)){
    $khoi = $DB->get_records('block_student', array('schoolid' => $school));
    return $khoi;
  }
}
function get_info_khoi($id){
    global $DB;
        $khoi = $DB->get_record('block_student', array(
            'id' => $id
        ));
        return $khoi;
    
}
function get_status_khoi($schoolid,$status){
    global $DB;
    $sql = "SELECT block_student.* FROM block_student JOIN schools ON schools.id = block_student.schoolid
            WHERE schoolid={$schoolid} AND block_student.`status`={$status}";
    return $DB->get_record_sql($sql);
    
}
function get_info_truong($id){
    global $DB;
        $truong = $DB->get_record('schools', array(
            'id' => $id
        ));
        return $truong;
    
}


/**
 * Chien Added
 * Teacher and Teacher tutors
 */
function get_list_acrht($page,$perpage=10,$params="")
{
    global $DB;
    $sql = "SELECT user.*  FROM user INNER JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=11 AND user.del=0";
    if($params !=''){
        $sql .= $params;
    }
    if(isset($page)){
        $sql .= " ORDER BY user.id DESC LIMIT ".$page*$perpage .",".$perpage;
    }
    $rows = $DB->get_records_sql($sql);
    return $rows;
}
function get_list_taacrhta($page,$perpage=10,$params="")
{
    global $DB;
    $sql = "SELECT user.*  FROM user INNER JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=12 AND user.del=0";
    if($params !=''){
        $sql .= $params;
    }
    if(isset($page)){
        $sql .= " ORDER BY user.id DESC LIMIT ".$page*$perpage .",".$perpage;
    }
    $rows = $DB->get_records_sql($sql);
    return $rows;
}

function get_list_teacher($page,$perpage=10,$params="")
{
    global $DB;
    
    $sql = "SELECT DISTINCT user.*  FROM user JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=8 AND user.del=0";

    if(!empty($params)){
        $sql .= $params;
    }
    if(isset($page)){
        $sql .= " ORDER BY user.id DESC LIMIT ".$page*$perpage .",".$perpage;
    }

    $rows = $DB->get_records_sql($sql);
    // var_dump($rows);die;
    return $rows;
}
function get_list_teachertutors($page,$perpage=10,$params="")
{
    global $DB;
    
    $sql = "SELECT DISTINCT user.*  FROM user JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=10 AND user.del=0";
    

    if($params !=''){
        $sql .= $params;
    }
    if(isset($page)){
        $sql .= " ORDER BY user.id DESC LIMIT ".$page*$perpage .",".$perpage;
    }

    $rows = $DB->get_records_sql($sql);
    // var_dump($rows);die;
    return $rows;
}
// function get_list_teachertutors($page,$perpage=10,$params="",$nql=null)
// {
//     global $DB;
//     if (empty($nql)) {
//         $sql = "SELECT user.*  FROM user INNER JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=10 AND user.del=0";
//     }else{
        
//          $sql = "SELECT user.* FROM
//                 (SELECT * FROM user 
//                     WHERE id = {$nql}
//                 ) AS b2
//                 JOIN
//                 (SELECT user.* FROM user
//                 JOIN role_assignments 
//                 ON user.id = role_assignments.userid
//                 WHERE role_assignments.roleid=10 AND user.del=0) AS user                
//                 ON b2.id = user.name_rht";
//     }

//     if($params !=''){
//         $sql .= $params;
//     }
//     if(isset($page)){
//         $sql .= " ORDER BY user.id DESC LIMIT ".$page*$perpage .",".$perpage;
//     }

//     $rows = $DB->get_records_sql($sql);
//     return $rows;
// }

/**
 * Chien Added
 * Nhom Lop
 */

function get_idgvnn_course($courseid)
{
    global $DB;
    $sql ="SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
        FROM `user` 
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
        JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
        JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
        WHERE `course`.`id` ={$courseid} AND `role_assignments`.`roleid`= 8";
    return $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
}

function get_idgvtg_course($courseid)
{
    global $DB;
    $sql ="SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
        FROM `user` 
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
        JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
        JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
        WHERE `course`.`id` ={$courseid} AND `role_assignments`.`roleid`= 10";
    return $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
}

function get_list_class($page,$perpage=10,$params="",$userid='')
{
    global $DB;
    
    $sql = "SELECT DISTINCT
            C.*,
            groups.name,
            schools.name AS tentruong,
            GV.giao_vien, TG.tro_giang 
            FROM
            course C
            JOIN enrol ON enrol.courseid = C.id
            JOIN user_enrolments ON user_enrolments.enrolid = enrol.id
            JOIN group_course ON group_course.course_id = C.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            LEFT JOIN (SELECT CONCAT(user.firstname,' ',user.lastname) AS giao_vien,user.id as userid, `enrol`.`courseid` FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`= 8 ) AS GV ON C.id = GV.courseid
            LEFT JOIN (SELECT CONCAT(user.firstname,' ',user.lastname) AS tro_giang,user.id as userid, `enrol`.`courseid` FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`= 10) AS TG ON C.id = TG.courseid
            WHERE
            NOT C.id =1";
    if($userid !=''){
        $sql .=" AND (`user_enrolments`.`userid`={$userid})";
    }      
    
    if($params !=''){
        $sql .= $params;
    }
    $sql .= " ORDER BY C.fullname ASC ";
    if(isset($page)){
        $sql .= " LIMIT ".$page*$perpage .",".$perpage;
    }

    
    $rows = $DB->get_records_sql($sql);
    return $rows;
}

function get_info_class_of_course($course_id)
{
    global $DB;
    $sql ="SELECT course.*, groups.name, groups.id_truong 
            FROM course 
            JOIN group_course ON group_course.course_id = course.id  
            JOIN groups ON group_course.group_id=groups.id 
            WHERE course.id ={$course_id}";
    return $DB->get_record_sql($sql);
}

function insert_class_groups($name,$sotiet,$gvnn,$gvtg ,$description) {
    global $DB,$CFG,$USER;

    $course = new stdClass();
      
    // $course->coefficient_name = trim($coefficient_name);
    $course->fullname = trim($name);
    $course->sotiet = trim($sotiet);
    $course->shortname = $name;
    $course->idnumber = $code;
    $course->id_gv = $gvnn;
    $course->id_tg = $gvtg;
    $course->summary = $description;
    $course->category = 1;
    $course->timecreated=time();
    $course->numsections = 0;
    $course->enablecompletion = 1;
    $course_obj=create_course($course) ;
    
    // $setting = new stdClass();
    
    // $setting->course_id = $course_obj->id;
    // $setting->course_library = $hidden_lirary;
    // $setting->active = $active_hidden;
    // $setting->module_order = $module_order;
    // $setting->new_window = $enable_full_screen_hidden;
    // $setting->update_complete_date = $apply_due_date_hidden;
    // $setting->compliant_for = $compliant_for;
    // $setting->automatic_resit = $automatic_resit;
    // $setting->file_name = $file_name;
    // $setting->currency = $course_currency;
    // $setting->fee = $course_fee;
    // $setting->sale_description = $sale_description;
    // $setting->sale_full_description = $sale_full_description;
    // if ($complete_by == 1) {
    //     if ($time_span != '') {
    //         $setting->time_span_complete = 1;
    //         $setting->time_span_complete_number = $time_span;
    //         $setting->time_span_complete_type = $time_spend_type;  //days, weeks
    //         $setting->complete_specific_date = null;
    //     }
    // }

    // if ($complete_by == 2) {
    //     if ($date2) {
    //         $setting->time_span_complete = 2;
    //         $setting->complete_specific_date = $date2;
    //         $setting->time_span_complete_number = 0;
    //         $setting->time_span_complete_type = 0;
    //     }
    // }

    // $DB->insert_record('course_settings', $setting);
    
    //enroll creator to course
    require_once ($CFG->dirroot."/lib/enrollib.php");

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_obj->id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);
    $enrol->enrol_user($instance, 2, 3, '', '');
    
    return $course_obj;
}

function get_course_of_school($schoolid){
    global $CFG, $DB;
    $sql = "SELECT
                    course.*,
                    schools.`name` AS ten_truong,
                    groups.`name` AS ten_lop 
                FROM
                    course
                    JOIN group_course ON group_course.course_id = course.id
                    JOIN groups ON group_course.group_id = groups.id
                    JOIN schools ON schools.id = groups.id_truong
                WHERE schools.id =".$schoolid;
    return $DB->get_records_sql($sql);
}

function assign_course_to_team($course_id, $team_id) {
    global $DB;
    $group_course = new stdClass();
    $group_course->course_id = $course_id;
    $group_course->group_id = $team_id;
    $DB->insert_record('group_course', $group_course);
}


/**
 * Chien Added
 * KhoiLop
 */
function get_list_block_student($page='',$limit=''){
    global $DB;
    $sql="select * from block_student where 1";
    $data = $DB->get_records_sql($sql);
    return $data;
}

/**
 * Chien Added
 * Namhoc
 */
function insert_school_year($sy_start,$sy_end,$sy_description)
{
    global $DB,$CFG,$USER;

    $sy = new stdClass();

    $sy->sy_start = $sy_start;
    $sy->sy_end   = $sy_end;
    $sy->sy_description = $sy_description;
    return $DB->insert_record('school_year', $sy);
}

function get_namhoc()
{
    global $DB;
    $sql = "select * from school_year where 1";
    $data = $DB->get_records_sql($sql);
    return $data;
}

function get_school_year($id)
{
    global $DB;
    $school_year = $DB->get_record('school_year', array(
        'id' => $id
    ));
    return $school_year;
}

function update_school_year($id,$sy_start,$sy_end,$sy_description)
{
    global $DB;
    $record = new  stdClass();
    $record->id = $id;
    $record->sy_start = $sy_start;
    $record->sy_end = $sy_end;
    $record->sy_description = $sy_description;
    return $DB->update_record('school_year', $record, false);
}

/**
 * Tuan Added
 * Parent
 */
function get_parent($key=""){
    global $DB;
    $sql = "SELECT `user`.`id`, `user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=10";
    if($key!=""){
        $sql .= " AND (`user`.`lastname` LIKE '%$key%' OR `user`.`firstname` LIKE '%$key%' OR `user`.`birthday` LIKE '%$key%' OR `user`.`email` LIKE '%$key%' OR `user`.`phone1` LIKE '%$key%')";
    }
    $rows = $DB->get_records_sql($sql);
    return $rows;
}

function add_profile2($firstname, $state, $postcode, $hs, $website, $lastname, $username, $password, $address, $city, $zip, $country, $timezone, $title, $company, $email, $phone1, $phone2, $skype, $twitter, $parentId) {
    global $DB, $USER;
    $record = new stdClass();

    $record->firstname = $firstname;
    $record->state = $state;
    $record->postcode = $postcode;

    $record->parentof = $hs;
    $record->website = $website;
    $record->lastname = $lastname;
    $record->username = $username;
    $password = hash_internal_user_password($password);
    $record->password = $password;
    $record->address = $address;
    $record->city = $city;
    $record->zip = $zip;
    $record->country = $country;
    $record->title = $title;
    $record->timezone = $timezone;
    $record->title = $title;
    $record->company = $company;
    $record->email = $email;
    $record->phone1 = $phone1;
    $record->phone2 = $phone2;
    $record->skype = $skype;
    $record->twitter = $twitter;
    $record->parent = $parentId;
    $record->confirmed = 1;
    $record->mnethostid = 1;

    $new_user_id = $DB->insert_record('user', $record);
    return $new_user_id;
}


// Chiến ADD hs lớp

function insert_history_namecourse($courseid,$name_course,$groupid, $school_year,$sotiet)
{
    global $DB;
    $data = new stdClass();
    $data->courseid    = $courseid;
    $data->course_name    = $name_course;
    $data->groupid    = $groupid;
    $data->school_year_id = $school_year;
    $data->sotiet = $sotiet;
    $lastinsertid = $DB->insert_record('history_course_name',$data); 
    return $lastinsertid;
}

function get_members_in_class_second($courseid) {
    global $DB, $USER;
    $sql = 'SELECT user.*,`groups`.`name` FROM user JOIN `groups_members` ON `user`.`id`= `groups_members`.`userid` JOIN `groups` ON `groups_members`.`groupid`= `groups`.`id` WHERE `user`.`id` IN(SELECT `groups_members`.`userid` FROM `groups_members` WHERE `groups_members`.`groupid` IN(SELECT `group_course`.`group_id` FROM `group_course` WHERE `group_course`.`course_id`=' . $courseid .'))';
    $result = $DB->get_records_sql($sql);
    return $result;
}


// Get list student 
function get_student_diligence(){
    global $DB, $USER;
    // Lấy toàn bộ danh sách học sinh chưa được đánh giá
    $sql = "SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `user`.id  NOT IN ( Select diligence.id_hs from diligence) AND `role_assignments`.`roleid`= 5";
    $result = $DB->get_records_sql($sql);
    return $result;
}
// thêm đánh giá chuyên cần
function insert_diligence($id_hs,$diligence,$monthrating)
{
    global $DB,$CFG,$USER;

    $record = new stdClass();

    $record->id_hs = $id_hs;
    $record->diligence = $diligence;
    $record->monthrating = $monthrating;
    return $DB->insert_record('diligence', $record);

}
// danh sách đánh giá chuyên cần
function get_diligence(){
    global $DB;

    $sql = "SELECT diligence.*,groups.name FROM diligence JOIN groups_members ON diligence.id_hs=groups_members.userid JOIN groups ON groups_members.groupid = groups.id";
    $data = $DB->get_records_sql($sql);
    return $data;
}

function get_info_student($id){
    global $DB;
        $student = $DB->get_record('user', array(
            'id' => $id
        ));
        return $student;
    
}

function get_diligence_id($id) {
    global $DB;
    $diligence = $DB->get_record('diligence', array(
        'id' => $id
    ));

    return $diligence;
}


function upload_img($name, $path){
  $maxfile = @ini_get('upload_max_filesize');
  if (!empty($_FILES[$name]['name'])) {
    $type = strtolower(pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION));
    // if ($type=='jpg' || $type=='png' || $type=='gif' || $type=='jpeg') {
        if ($_FILES[$name]['size'] < get_real_size($maxfile)) {
          $path_sv = $path.$_FILES[$name]['name'];
          $kk = end(explode('/', $path_sv));
          move_uploaded_file($_FILES[$name]['tmp_name'], $path_sv);
          return $kk;
        }
    // }
  }
}





/**
 * Chien Add Groups (course)
 */

// List student in class (groups)
function get_users_from_parent_second($creator_id) {
    global $DB, $USER;

    $sql1 ="SELECT course.*, groups.name,groups.id as grid FROM course  JOIN group_course ON group_course.course_id = course.id  JOIN groups ON group_course.group_id=groups.id WHERE course.id =".$creator_id;
    $rows = $DB->get_record_sql($sql1);

    $sql = "SELECT `user`.`id`, user.firstname, user.lastname, user.code, user.birthday, groups.`name`,role_assignments.roleid, groups_members.groupid
            FROM user JOIN groups_members ON groups_members.userid = user.id JOIN groups ON groups_members.groupid= groups.id
            JOIN role_assignments ON user.id = role_assignments.userid
            WHERE groups_members.groupid = $rows->grid AND role_assignments.roleid = 5 AND user.del=0";
    $result = $DB->get_records_sql($sql);
    return $result;
}

// function get_userid_from_parent_second($creator_id) {
//     global $DB, $USER;

//     $sql1 ="SELECT course.*, groups.name,groups.id as grid FROM course  JOIN group_course ON group_course.course_id = course.id  JOIN groups ON group_course.group_id=groups.id WHERE course.id =".$creator_id;
//     $rows = $DB->get_record_sql($sql1);

//     $sql = "SELECT `user`.`id`, user.firstname, user.lastname, user.code, user.birthday, groups.`name`,role_assignments.roleid, groups_members.groupid
//             FROM user JOIN groups_members ON groups_members.userid = user.id JOIN groups ON groups_members.groupid= groups.id
//             JOIN role_assignments ON user.id = role_assignments.userid
//             WHERE groups_members.groupid = $rows->grid AND role_assignments.roleid = 5 ";
//     $result = $DB->get_record_sql($sql);
//     return $result;
// }

function get_teams_from_course($course_id){
    global $DB;
    $sql = 'SELECT group_id FROM group_course
        WHERE course_id = :course_id';
    $params['course_id'] = $course_id;
    $result = $DB->get_records_sql($sql, $params);
        return $result;
        
}

function get_members_in_groups($userid){
    global $DB;
    $sql = 'SELECT user.username,user.id,user.firstname,user.lastname,user.email, course.fullname
    FROM user_enrolments, user, course, enrol
    WHERE 
    user.id = user_enrolments.userid
        AND course.id = enrol.courseid
        AND user_enrolments.enrolid = enrol.id
    AND user_enrolments.userid =' . $userid;

    $result = $DB->get_record_sql($sql);
    return $result;
}

function get_teams_from_user_id($course_id){
    global $DB; 
    $field = "g.id, g.name, g.id_truong, g.description";
    $sql = "SELECT ".$field." FROM {groups} g";
    $params['courseid'] = $course_id;
    $result = $DB->get_records_sql($sql,$params);
    return $result;
}

function get_info_course($course_id){
    global $DB;
    $sql = "SELECT course.*, groups.id AS id_lop, groups.id_khoi, groups.id_truong FROM course
    JOIN group_course ON group_course.course_id = course.id
    JOIN groups ON group_course.group_id = groups.id   WHERE course.id=".$course_id;
    $groups = $DB->get_record_sql($sql);
    return $groups;
}

function get_info_groups1($course_id){
    global $DB;
    $sql = "SELECT course.*, groups.id AS id_lop, groups.id_truong, groups.id_khoi FROM course
            JOIN group_course ON group_course.course_id = course.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            JOIN block_student ON groups.id_khoi = block_student.id    
            WHERE course.id=".$course_id;
    $groups = $DB->get_record_sql($sql);
    return $groups;
}

function get_info_groups($course_id){
    global $DB;
    $sql = "SELECT course.*, groups.id AS id_lop, groups.id_truong, groups.id_khoi,gv.idgvnn,tg.idgvtg FROM course
            JOIN group_course ON group_course.course_id = course.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            JOIN block_student ON groups.id_khoi = block_student.id
            JOIN (SELECT user.id as idgvnn, `enrol`.`courseid` FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`=8) AS gv ON gv.courseid = course.id
            JOIN (SELECT user.id as idgvtg, `enrol`.`courseid` FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`=10) AS tg ON tg.courseid = course.id      
            WHERE course.id=".$course_id;
    $groups = $DB->get_record_sql($sql);
    return $groups;
}

function get_info_groups2($course_id){
    global $DB;
    $sql = "SELECT course.*, groups.id AS id_lop, groups.id_truong, groups.id_khoi FROM course
            JOIN group_course ON group_course.course_id = course.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            JOIN block_student ON groups.id_khoi = block_student.id     
            WHERE course.id=".$course_id;
    $groups = $DB->get_record_sql($sql);
    return $groups;
}

function get_info_class($id)
{
    global $DB;
        $class = $DB->get_record('groups', array(
            'id' => $id
        ));
        return $class;
}

function get_id_course($id){
    global $DB;
    $sql = 'SELECT * FROM group_course WHERE course_id ='. $id;
    return $DB->get_record_sql($sql);
}

function get_book_schedule($courseid)
{
    global $DB;
    $sql = "SELECT
                `schedule`.book 
            FROM
                `schedule`
                JOIN course ON course.id = `schedule`.courseid
                JOIN groups ON groups.id = `schedule`.groupid AND groups.id_namhoc = `schedule`.school_yearid
            WHERE
                `schedule`.del = 0 
                AND `schedule`.courseid =".$courseid;
    return $DB->get_records_sql($sql);
}

/**
 * Tuan add dashboard
 */
//SELECT `schedule`.* FROM SCHEDULE INNER JOIN course ON `schedule`.`courseid`=`course`.`id` WHERE `course`.`id_gv`=127 OR `course`.`id_tg`=127
function get_course_user($id){
    global $DB;
    $sql = "SELECT * FROM `course` WHERE `id_gv`=$id OR `id_tg`=$id";
    return $DB->get_records_sql($sql);
}

function get_scheduleid($course_id){
    global $DB;
    $sql = "SELECT * FROM `schedule` WHERE `courseid`=$course_id AND `del`=0";
    return $DB->get_records_sql($sql);
}

function get_schedule_info($sch_id){
    global $DB;
    $sql = "SELECT * FROM `schedule_info` WHERE `scheduleid`=$sch_id";
    return $DB->get_records_sql($sql);
}

function get_gv($id){
    global $DB;
    $sql = "SELECT * FROM `user` WHERE `id`=$id";

    return $DB->get_record('user', array('id'=>$id));
}

function get_teams_user($id){
    global $DB;
    // $sql = "(SELECT enrolid 
    //         FROM `user_enrolments` 
    //         INNER JOIN `user` 
    //         ON `user_enrolments`.userid = `user`.id
    //         WHERE `user`.id = $id)";
    
    // $sql = "SELECT courseid FROM `course` 
    //         INNER JOIN `user_enrolments` 
    //         ON `user_enrolments`.courseid = `course`.id";

    $sql = "SELECT `enrol`.courseid
            FROM `enrol`
            INNER JOIN (SELECT `user_enrolments`.enrolid AS enrol_id
                FROM `user_enrolments` 
                INNER JOIN `user` 
                ON `user_enrolments`.userid = `user`.id
                WHERE `user`.id = $id) AS `user_enrolments`
            ON `enrol`.id = enrol_id GROUP BY courseid";

    $result = $DB->get_records_sql($sql);

    return $result;
}

function get_coursename($id_course){
    global $DB;
    return $DB->get_record('course', array('id'=>$id_course));
}

function get_unittest($userid)
{
    global $DB;
    return $DB->get_records('unittest', array('userid'=>$userid));
}

function get_diligence_hs($idhs){
    global $DB;

    $sql = "SELECT diligence.*,groups.name FROM diligence JOIN groups_members ON diligence.id_hs=groups_members.userid JOIN groups ON groups_members.groupid = groups.id WHERE `diligence`.id_hs=$idhs";
    $data = $DB->get_records_sql($sql);
    return $data;
}

function chuyencan_thang($id)
{
    global $DB;

    $sql = "SELECT  DISTINCT month
        FROM
            diemdanh
        WHERE
            userid=". $id;
    $data = $DB->get_records_sql($sql);
    return $data;
}

function get_chuyencan_hs($studentid,$month)
{
    global $DB;

    $sql = "SELECT DISTINCT diemdanh.month, count(diemdanh.month) AS diemdanhhs,
            diemdanh.*,
            course.fullname 
        FROM
            diemdanh
            JOIN course ON course.id = diemdanh.course_id 
        WHERE
            diemdanh.userid = ".$studentid . " AND diemdanh.month=". $month;
    $data = $DB->get_records_sql($sql);
    return $data;
}
/**
 * Pagination
 */
function get_count_records($table, $param=null)
{
    global $DB;
    $sql = "SELECT COUNT(id) AS `count` FROM $table ";
    if (!empty($param)) {
        $sql .= "WHERE $param";
    }
    $count = $DB->get_record_sql($sql);
    return $count->count;
}

function paginate($sum_record, $page_number, $parpage, $url) {
    $num = 1; //số lượng số trang bên cạnh trang lựa chọn
    $active = '';
    $totalcount = ceil($sum_record/$parpage);
    
    if ($totalcount <=1) {
        return 1;
    }

    echo "<ul class='pagination'>";
    if ($totalcount > 6) {
        if ($page_number>0) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number-1).'">Previous</a></li>';
        }
        for($i=0; $i<$totalcount; $i++) {
            if($i==$page_number){
                $active=' active';
            }else{
                $active ='';
            }
            $link = '<li class="page-item'.$active.'"><a class="page-link" href="'.$url.'page='.$i.'">'.($i+1).'</a></li>';

            if ($i<=$num) {
                echo $link;
                if ($i==$num && $page_number==0) {
                    echo "<li class='page_item'><a class='page-link' href='#'>...</li>";
                }
            }elseif($i>=($page_number-$num) && $i<=($page_number+$num)){
                if ($i==($page_number-$num)&&$i>($num+1)) {
                    echo "<li class='page_item'><a class='page-link' href='#'>...</li>";
                }
                if ($i<($totalcount-$num)) {
                    echo $link;
                    if ($i==($page_number+$num)&&$i<($totalcount-$num-1)) {
                        echo "<li class='page_item'><a class='page-link' href='#'>...</li>";
                    }
                }
            }

            if ($i>=($totalcount-$num)) {
                echo $link;
            }

        }
        if ($page_number<$totalcount-1) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number+1).'">Next</a></li>';
        }
    }else{
        if ($page_number>0) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number-1).'">Previous</a></li>';
        }
        for($i=0; $i<$totalcount; $i++) {
            if($i==$page_number){
                $active=' active';
            }else{
                $active ='';
            }
            $link = '<li class="page-item'.$active.'"><a class="page-link" href="'.$url.'page='.$i.'">'.($i+1).'</a></li>';
            echo $link;
        }
        if ($page_number<$totalcount-1) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number+1).'">Next</a></li>';
        }
    }
    echo "</ul>";

}

/**
 * [add_logs]
 */
function add_logs($module, $action, $url, $name)
{
    global $DB, $CFG, $USER;

    if ($user) {
        $userid = $user;
    } else {
        if (session_is_loggedinas()) {  // Don't log
            return;
        }
        $userid = empty($USER->id) ? '0' : $USER->id;
    }

    if (isset($CFG->logguests) and !$CFG->logguests) {
        if (!$userid or isguestuser($userid)) {
            return;
        }
    }

    $REMOTE_ADDR = getremoteaddr();

    $timenow = time();

    if (!empty($url)) { // could break doing html_entity_decode on an empty var.
        $url = html_entity_decode($url);
    } else {
        $url = '';
    }

    $log = array('time'=>$timenow, 'userid'=>$userid, 'ip'=>$REMOTE_ADDR, 'module'=>$module,
                  'action'=>$action, 'url'=>$url, 'name'=>$name);

    try {
        $DB->insert_record_raw('log', $log, false);
    } catch (dml_write_exception $e) {
        debugging('Error: Could not insert a new entry to the Moodle log', DEBUG_ALL);
        // MDL-11893, alert $CFG->supportemail if insert into log failed
        if ($CFG->supportemail and empty($CFG->noemailever)) {
            // email_to_user is not usable because email_to_user tries to write to the logs table,
            // and this will get caught in an infinite loop, if disk is full
            $site = get_site();
            $subject = 'Insert into log failed at your moodle site '.$site->fullname;
            $message = "Insert into log table failed at ". date('l dS \of F Y h:i:s A') .".\n It is possible that your disk is full.\n\n";
            $message .= "The failed query parameters are:\n\n" . var_export($log, true);

            $lasttime = get_config('admin', 'lastloginserterrormail');
            if(empty($lasttime) || time() - $lasttime > 60*60*24) { // limit to 1 email per day
                //using email directly rather than messaging as they may not be able to log in to access a message
                mail($CFG->supportemail, $subject, $message);
                set_config('lastloginserterrormail', time(), 'admin');
            }
        }
    }
}


function view_logs($select, $params)
{
    global $CFG;

    $page = optional_param('page', '0', PARAM_INT);
    $perpage = optional_param('perpage', '20', PARAM_INT);

    $totalcount = 0;
    $logs = manage_get_logs($select, $params, $order = 'l.time DESC', $page*$perpage, $perpage, $totalcount);
    foreach ($logs as $key => $log) {
        if($log->action !='error'){
        $user_info = get_gv($log->userid);

        echo "<div class='row mb-3'>";
          echo '<div class="col-md-1">';
            echo '<span class="badge badge-primary">'.$log->module.'</span>';
          echo '</div><div class="col-md-8">';
            echo "<a href='".$CFG->wwwroot."/manage/people/profile.php?id=".$log->userid."'>".$user_info->firstname . " " . $user_info->lastname . "</a>";
            switch ($log->action) {
              case 'view':
                echo get_string('viewed');
                break;
            case 'new':
                echo get_string('addnew');
                break;
            case 'update':
                echo ' '.get_string('updated').' ';
                break;
            case 'delete':
                echo ' '.get_string('deleted').' ';
                break;
            case 'login':
                echo ' '.get_string('logined');
                break;
            case 'logout':
                echo ' '.get_string('logouted');
                break;
            }
            if ($log->action=='delete') {
                echo "<span style='color:#888; text-decoration:line-through;'>";
                echo $log->name;
                echo "</span>";
            }else{
                echo "<a href='".$CFG->wwwroot.$log->url."'>";
                echo $log->name;
                echo "</a>";
            }
            echo '</div><div class="col-md-3">';
            echo time_ago($log->time, '')."</div>";
            echo '</div>';
        }
    }
    echo "<hr>";
    paginate($totalcount, $page, $perpage, 'index.php?');
}

function get_counts_login($day)
{
    global $DB;
    $login = $DB->get_records('log', array('action'=>'login'));
    $count=0;
    foreach ($login as $key => $value) {
        if (date('d-m-Y', $value->time) == $day) {
            $count++;
        }
    }
    return $count;
}
//End LOG
//
//

function del_teacher($id){
  global $DB;
  $record = new  stdClass();
  $record->id = $id;
  $record->del = 1;
  $record->confirmed = 0;
  $DB->update_record('user', $record, false); 
}


// Mở/Khóa đánh giá học sinh


function get_unlock_evaluation_student($page,$perpage=10,$params="")
{
    global $DB;
    $sql="SELECT
        evalucation_student.*,
        course.fullname,
        groups.`name`,
        `user`.firstname,
        `user`.lastname 
    FROM
        evalucation_student
        JOIN user ON user.id =evalucation_student.userid
        JOIN groups ON groups.id = evalucation_student.groupid
        JOIN course ON course.id = evalucation_student.courseid 
    WHERE
        evalucation_student.`view` =2";
    if($params !=''){
        $sql .= $params;
    }
    if(isset($page)){
        $sql .= " LIMIT ".$page*$perpage .",".$perpage;
    }
    return $DB->get_records_sql($sql);
}

function send_evaluation_student($id){
  global $DB;
  $record = new  stdClass();
  $record->id = $id;
  $record->view = 2;
  return $DB->update_record('evalucation_student', $record, false);
}

function lock_evaluation_student($id){
  global $DB;
  $record = new  stdClass();
  $record->id = $id;
  $record->view = 1;
  return $DB->update_record('evalucation_student', $record, false);
}

function lock_evaluation_course($id){
  global $DB;
  $sql = "UPDATE evalucation_student SET view=1 where courseid=$id";
  return $DB->execute($sql);
}

function check_course_evaluation_student($id){
  global $DB;
  $sql = "SELECT COUNT(courseid) FROM evalucation_student WHERE courseid=".$id;
  return $DB->execute($sql);
}

function get_info_namhoc($id){
    global $DB;
        $namhoc = $DB->get_record('school_year', array(
            'id' => $id
        ));
    return $namhoc;
    
}

?>




