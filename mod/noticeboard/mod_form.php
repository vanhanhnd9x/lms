<?php

//  * @author : Patrick Thibaudeau @version $Id: version.php,v 2.0 2010/07/13 18:10:20 @package noticeboard
defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->libdir . '/filelib.php');

class mod_noticeboard_mod_form extends moodleform_mod {

    function definition() {
        global $CFG, $DB;

        $mform = $this->_form;

        $config = get_config('noticeboard');

        $mform->addElement('header', 'general', get_string('general', 'form'));
        $mform->addElement('text', 'title', get_string('name', 'noticeboard'), array('size' => '45'));
      
        $mform->addRule('name', null, 'required', null, 'client');

        //Have to use this option for postgresqgl to work
        $instance = $this->current->instance;
        if (empty($instance)) {
            $instance = 0;
        }

  
        $context = $this->context;

        $editoroptions = array('subdirs' => 1, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => -1, 'changeformat' => 1, 'context' => $context, 'noclean' => 1, 'trusttext' => 1);
     
        //-----------------------------for adding noticeboards---------------------------------------------------------------
        $repeatarray = array();

        $repeatarray[] = $mform->createElement('editor', 'content', get_string('noticeboardcontent', 'noticeboard'), null, $editoroptions);


        if ($this->_instance) {
            $repeatno = $DB->count_records('noticeboard', array('id' => $instance));
            $repeatno += 1;
        } else {
            $repeatno = 1;
        }

        $repeateloptions = array();
        if (!isset($repeateloptions['noticeboardcontentorder'])) {
            $repeateloptions['noticeboardcontentorder']['default'] = $i - 2;
        }
        $mform->setType('noticeboardcontentorder', PARAM_INT);
        $mform->setType('optionid', PARAM_INT);
        $repeateloptions['content']['helpbutton'] = array('content', 'noticeboard');
        $mform->setType('content', PARAM_CLEAN);



        $this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields', 1, get_string('addnoticeboard', 'noticeboard'));
        //-----------------------------------------------------------------------------------------------------------------------------------------------
        
        $features = array('groups'=>false, 'groupings'=>false, 'groupmembersonly'=>true,
                'outcomes'=>false, 'gradecat'=>false, 'idnumber'=>false);
        $this->standard_coursemodule_elements($features);
        
        // buttons
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values) {
        global $CFG, $DB;
        $default_values['content'] = $default_values['content'][0]['text'];
    }

}

?>
