<?php

require_once('../../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');

$course = required_param('id', PARAM_INT); // course id
$noticeboard = required_param('note', PARAM_INT);

if (!$DB->delete_records("noticeboard", array("id" => "$noticeboard"))) {
    $result = false;
} else {
    $result = true;
}

echo json_encode((object) array('result' => $result, 'id' => $noticeboard));

