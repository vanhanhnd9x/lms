<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : chuvantung
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once($CFG->libdir . '/resourcelib.php');
require_once($CFG->libdir . '/completionlib.php');

$courseid = optional_param('id', 0, PARAM_INT);  // noticeboard ID
//add_to_log($course->id, "noticeboard", "view", "view.php");
/// Print the page header

$PAGE->set_url('/mod/noticeboard/view.php');
$PAGE->set_title(get_string('noticeboard'));
$PAGE->set_heading(get_string('noticeboard'));
if ($PAGE->user_allowed_editing()) {
    $buttons = '<noticeboardle><tr><td><form method="get" action="' . $CFG->wwwroot . '/course/mod.php"><div>' .
            '<input type="hidden" name="update" value="' . $cm->id . '" />' .
            '<input type="submit" value="' . get_string('updatethis', 'noticeboard') . '" /></div></form></td></tr></noticeboardle>';
    $PAGE->set_button($buttons);
}


//Gather javascripts and css
$PAGE->requires->css('/mod/noticeboard/styles.css');
$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/mod/noticeboard/js/noticeboard.js');
echo $OUTPUT->header();
$results = $DB->get_records_sql('SELECT * FROM noticeboard WHERE course=' . $courseid . ' ORDER BY id DESC;');
$course = $DB->get_record('course', array('id' => $courseid));
?>
<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <?php
            $currenttab = 'noticeboard';
            include_once($CFG->dirroot . '/course/managetabs.php');
            ?>
            <table class="item-page-list">
                <tbody>
                    <?php foreach ($results as $item): ?>
                        <tr id="rows-<?php echo $item->id; ?>">
                            <td class="main-col">
                                <div class="tip"><?php echo noticeboard_ago($item->timemodified); ?></div>
                                <div class="marking-user-answer">
                                    <h3><?php echo $item->title; ?></h3>
                                    <p><?php echo $item->content; ?></p>
                                </div>
                            </td>
                            <td>
                                <a href="<?php print new moodle_url('/mod/noticeboard/manual/ajax.php', array('id' => $courseid, 'note' => $item->id)); ?>" class="btn-remove"></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>          

        </div> 
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div id="admin-fullscreen-right">

        <div class="side-panel">
            <div class="action-buttons">
                <ul>
                    <li>
                        <a href="<?php print new moodle_url('/course/modedit.php', array('add' => 'noticeboard', 'type' => '', 'course' => $courseid, 'section' => 0, 'return' => 0)); ?>" class="big-button drop">
                            <span class="left">
                                <span class="mid">
                                    <span class="right">
                                        <span class="icon_add"><?php print_r(get_string('addanote')) ?></span>
                                    </span></span></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>  <!-- End Right -->
</div>  
<?php
echo $OUTPUT->footer();
?>