<?php

// $Id: lib.php,v 2.0 2010/07/13 18:17:00 Patrick Thibaudeau Exp $
/**
 * Library of functions and constants for module noticeboard
 *
 * @author : Patrick Thibaudeau
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
defined('MOODLE_INTERNAL') || die;

/**
 * List of features supported in noticeboard display
 * @uses FEATURE_IDNUMBER
 * @uses FEATURE_GROUPS
 * @uses FEATURE_GROUPINGS
 * @uses FEATURE_GROUPMEMBERSONLY
 * @uses FEATURE_MOD_INTRO
 * @uses FEATURE_COMPLETION_TRACKS_VIEWS
 * @uses FEATURE_GRADE_HAS_GRADE
 * @uses FEATURE_GRADE_OUTCOMES
 * @param string $feature FEATURE_xx constant for requested feature
 * @return bool|null True if module supports feature, false if not, null if doesn't know
 */
function noticeboard_supports($feature) {
    switch ($feature) {
        case FEATURE_IDNUMBER: return false;
        case FEATURE_GROUPS: return false;
        case FEATURE_GROUPINGS: return false;
        case FEATURE_GROUPMEMBERSONLY: return false;
        case FEATURE_MOD_INTRO: return false;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_GRADE_HAS_GRADE: return false;
        case FEATURE_GRADE_OUTCOMES: return false;
        case FEATURE_MOD_ARCHETYPE: return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_BACKUP_MOODLE2: return true;

        default: return null;
    }
}

/**
 * Returns all other caps used in module
 * @return array
 */
function noticeboard_get_extra_capabilities() {
    return array('moodle/site:accessallgroups');
}

/**
 * This function is used by the reset_course_userdata function in moodlelib.
 * @param $data the data submitted from the reset course.
 * @return array status array
 */
function noticeboard_reset_userdata($noticeboard) {
    return array();
}

/**
 * List of view style log actions
 * @return array
 */
function noticeboard_get_view_actions() {
    return array('view', 'view all');
}

/**
 * List of update style log actions
 * @return array
 */
function noticeboard_get_post_actions() {
    return array('update', 'add');
}

/**
 * Add noticeboard display instance.
 * @param object $data
 * @param object $mform
 * @return int new page instance id
 */
function noticeboard_add_instance($noticeboard) {
    global $CFG, $DB;

    require_once("$CFG->libdir/resourcelib.php");

    $cmid = $noticeboard->coursemodule;
    $noticeboard->timemodified = time();

    //insert noticeboards and content
    if ($new_content_id = $DB->insert_record("noticeboard", $noticeboard)) {

        // we need to use context now, so we need to make sure all needed info is already in db
        $DB->set_field('course_modules', 'instance', $new_content_id, array('id' => $cmid));
        $context = get_context_instance(CONTEXT_MODULE, $cmid);

        $editoroptions = array('subdirs' => 1, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => -1, 'changeformat' => 1, 'context' => $context, 'noclean' => 1, 'trusttext' => true);

        if (isset($noticeboard->content[0]['text'])) {
            $draftitemid = $noticeboard->content[0]['itemid'];
            if ($draftitemid) {
                $tabcontentupdate = new object();
                $tabcontentupdate->id = $new_content_id;
                $tabcontentupdate->content = file_save_draft_area_files($draftitemid, $context->id, 'mod_noticeboard', 'content', $new_content_id, $editoroptions, $noticeboard->content[0]['text']);
                $DB->update_record('noticeboard', $tabcontentupdate);
            }
        }
    }
    redirect("$CFG->wwwroot/mod/noticeboard/view.php?id=" . $noticeboard->course);
}

/**
 * Given an object containing all the necessary data, 
 * (defined by the form in mod.html) this function 
 * will update an existing instance with new data.
 *
 * @param object $instance An object from the form in mod.html
 * @return boolean Success/Fail
 * */
function noticeboard_update_instance($noticeboard) {
    global $CFG, $DB;

    require_once("$CFG->libdir/resourcelib.php");

    $cmid = $noticeboard->coursemodule;

    $noticeboard->timemodified = time();
    $noticeboard->id = $noticeboard->instance;

    foreach ($noticeboard->title as $key => $value) {

        // we need to use context now, so we need to make sure all needed info is already in db
        $DB->set_field('course_modules', 'instance', $noticeboard->id, array('id' => $cmid));
        $context = get_context_instance(CONTEXT_MODULE, $cmid);
        $editoroptions = array('subdirs' => 1, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => -1, 'changeformat' => 1, 'context' => $context, 'noclean' => 1, 'trusttext' => true);


        $value = trim($value);
        $option = new object();
        $option->title = $value;
        $option->noticeboardcontentorder = $noticeboard->noticeboardcontentorder[$key];
        $option->externalurl = $noticeboard->externalurl[$key];
        //noticeboard content is now an array due to the new editor
        $draftitemid = $noticeboard->content[$key]['itemid'];

        if ($draftitemid) {
            $option->noticeboardcontent = file_save_draft_area_files($draftitemid, $context->id, 'mod_noticeboard', 'content', $noticeboard->optionid[$key], $editoroptions, $noticeboard->content[$key]['text']);
        }
        $option->contentformat = $noticeboard->content[$key]['format'];
        $option->noticeboardid = $noticeboard->id;
        $option->timemodified = time();

        if (isset($noticeboard->optionid[$key]) && !empty($noticeboard->optionid[$key])) {//existing noticeboard record
            $option->id = $noticeboard->optionid[$key];
            if (isset($value) && $value <> '') {
                $DB->update_record("noticeboard_content", $option);
            } else { //empty old option - needs to be deleted.
                $DB->delete_records("noticeboard_content", array("id" => $option->id));
            }
        } else {
            if (isset($value) && $value <> '') {
                $newnoticeboard_content_id = $DB->insert_record("noticeboard_content", $option);
                //noticeboard content is now an array due to the new editor
                //In order to enter file information from the editor
                //We must now update the record once it has been created

                if (isset($noticeboard->content[$key]['text'])) {
                    $draftitemid = $noticeboard->content[$key]['itemid'];
                    if ($draftitemid) {
                        $noticeboardcontentupdate = new object();
                        $noticeboardcontentupdate->id = $newnoticeboard_content_id;
                        $noticeboardcontentupdate->noticeboardcontent = file_save_draft_area_files($draftitemid, $context->id, 'mod_noticeboard', 'content', $newnoticeboard_content_id, $editoroptions, $noticeboard->content[$key]['text']);
                        $DB->update_record('noticeboard_content', $noticeboardcontentupdate);
                    }
                }
            }
        }
    }
    return $DB->update_record("noticeboard", $noticeboard);
}

/**
 * Given an ID of an instance of this module, 
 * this function will permanently delete the instance 
 * and any data that depends on it. 
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 * */
function noticeboard_delete_instance($id) {
    global $DB;

    if (!$noticeboard = $DB->get_record("noticeboard", array("id" => "$id"))) {
        return false;
    }

    $result = true;

    # Delete any dependent records here #

    if (!$DB->delete_records("noticeboard", array("id" => "$noticeboard->id"))) {
        $result = false;
    }


    return $result;
}

/**
 * Serves the noticeboard images or files. Implements needed access control ;-)
 *
 * @param object $course
 * @param object $cm
 * @param object $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return bool false if file not found, does not return if found - justsend the file
 */
function noticeboard_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB;

    //The following code is for security
    require_course_login($course, true, $cm);

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    $fileareas = array('mod_noticeboard', 'content');
    if (!in_array($filearea, $fileareas)) {
        return false;
    }
    //id of the content row
    $noticeboardcontentid = (int) array_shift($args);

    //Security - Check if exists
    if (!$noticeboardcontent = $DB->get_record('noticeboard_content', array('id' => $noticeboardcontentid))) {
        return false;
    }

    if (!$noticeboard = $DB->get_record('noticeboard', array('id' => $cm->instance))) {
        return false;
    }

    //Now gather file information
    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_noticeboard/$filearea/$noticeboardcontentid/$relativepath";

    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // finally send the file
    send_stored_file($file, 0, 0, true);
}

/**
 * Return a small object with summary information about what a 
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return null
 * @todo Finish documenting this function
 * */
function noticeboard_user_outline($course, $user, $mod, $noticeboard) {
    global $DB;

    if ($logs = $DB->get_records('log', array('userid' => $user->id, 'module' => 'noticeboard',
        'action' => 'view', 'info' => $noticeboard->id), 'time ASC')) {

        $numviews = count($logs);
        $lastlog = array_pop($logs);

        $result = new stdClass();
        $result->info = get_string('numviews', '', $numviews);
        $result->time = $lastlog->time;

        return $result;
    }
    return NULL;
}

/**
 * Print a detailed representation of what a user has done with 
 * a given particular instance of this module, for user activity reports.
 *
 * @return boolean
 * @todo Finish documenting this function
 * */
function noticeboard_user_complete($course, $user, $mod, $noticeboard) {
    global $CFG, $DB;

    if ($logs = $DB->get_records('log', array('userid' => $user->id, 'module' => 'noticeboard',
        'action' => 'view', 'info' => $noticeboard->id), 'time ASC')) {
        $numviews = count($logs);
        $lastlog = array_pop($logs);

        $strmostrecently = get_string('mostrecently');
        $strnumviews = get_string('numviews', '', $numviews);

        echo "$strnumviews - $strmostrecently " . userdate($lastlog->time);
    } else {
        print_string('neverseen', 'noticeboard');
    }
}

/**
 * Given a course and a time, this module should find recent activity 
 * that has occurred in noticeboard activities and print it out. 
 * Return true if there was output, or false is there was none. 
 *
 * @uses $CFG
 * @return boolean
 * @todo Finish documenting this function
 * */
function noticeboard_print_recent_activity($course, $viewfullnames, $timestart) {
    global $CFG;

    return false;  //  True if anything was printed, otherwise false 
}

/**
 * Given a course_module object, this function returns any
 * "extra" information that may be needed when printing
 * this activity in a course listing.
 *
 * See {@link get_array_of_activities()} in course/lib.php
 *
 * @param object $coursemodule
 * @return object info
 */
function noticeboard_get_coursemodule_info($coursemodule) {
    global $CFG, $DB;
    require_once("$CFG->libdir/resourcelib.php");

    if (!$noticeboard = $DB->get_record('noticeboard', array('id' => $coursemodule->instance), 'id, title')) {
        return NULL;
    }

    $info = new stdClass();
    $info->name = $noticeboard->title;


    // $fullurl = "$CFG->wwwroot/mod/noticeboard/view.php?id=$coursemodule->id";
    //$info->extra = "onclick=\"window.open('$fullurl', '', ''); return false;\"";

    return $info;
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such 
 * as sending out mail, toggling flags etc ... 
 *
 * @uses $CFG
 * @return boolean
 * @todo Finish documenting this function
 * */
function noticeboard_cron() {
    global $CFG;

    return false;
}

/**
 * Must return an array of grades for a given instance of this module, 
 * indexed by user.  It also returns a maximum allowed grade.
 * 
 * Example:
 *    $return->grades = array of grades;
 *    $return->maxgrade = maximum allowed grade;
 *
 *    return $return;
 *
 * @param int $noticeboardid ID of an instance of this module
 * @return mixed Null or object with an array of grades and with the maximum grade
 * */
function noticeboard_grades($noticeboardid) {
    return NULL;
}

/**
 * Must return an array of user records (all data) who are participants
 * for a given instance of noticeboard. Must include every user involved
 * in the instance, independient of his role (student, teacher, admin...)
 * See other modules as example.
 *
 * @param int $noticeboardid ID of an instance of this module
 * @return mixed boolean/array of students
 * */
function noticeboard_get_participants($noticeboardid) {
    return false;
}

/**
 * This function returns if a scale is being used by one noticeboard
 * it it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $noticeboardid ID of an instance of this module
 * @return mixed
 * @todo Finish documenting this function
 * */
function noticeboard_scale_used($noticeboardid, $scaleid) {
    $return = false;
    return $return;
}

function get_noticeboard_id($noticeboard) {
    $noticeboardid = $noticeboard;
    return $noticeboardid;
}

/**
 * This function extends the global navigation for the site.
 * It is important to note that you should not rely on PAGE objects within this
 * body of code as there is no guarantee that during an AJAX request they are
 * available
 *
 * @param navigation_node $navigation The page node within the global navigation
 * @param stdClass $course The course object returned from the DB
 * @param stdClass $module The module object returned from the DB
 * @param stdClass $cm The course module instance returned from the DB
 */
function noticeboard_extend_navigation($navigation, $course, $module, $cm) {
    /**
     * This is currently just a stub so that it can be easily expanded upon.
     * When expanding just remove this comment and the line below and then add
     * you content.
     */
    $navigation->nodetype = navigation_node::NODETYPE_LEAF;
}

function noticeboard_ago($time, $prefix = 'Đã đăng') {
    $periods = array(get_string('second'), get_string('minute'), get_string('hour'), get_string('day'), get_string('week'), get_string('month'), get_string('year'), get_string('decade'));
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
    $now = time();
    $difference = $now - $time;
    $tense = get_string('ago');
    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }
    $difference = round($difference);
//    Tung added
    //hide for vietnamese
//   if($difference != 1) {
//       $periods[$j].= "s";
//   }

    return "$prefix $difference $periods[$j] $tense";
}
?>
