<?php // $Id: noticeboard.php,v 1.2 2008/07/01 09:44:05 moodler Exp $ 
      // noticeboard.php - created with Moodle 1.9


$string['ajouter'] = 'Add a new noticeboard display';
$string['addnoticeboard'] = 'Add a new noticeboard';
$string['changestyle'] = 'Modify stylesheet';
$string['clicktoopen'] = 'Click to open';
$string['content'] = 'NoticeBoard content';
$string['content_help'] = 'You can embed a pdf file by following these instructions:
                            <br><ul>
                             <li>type the name you would like to use for the link.</li>
                             <li>Select the text and click on the inser/edit link button.</li>
                             <li>Click on the browse button at the end of the link url field</li>
                             <li>Upload your pdf file and then click insert</li>
                             </ul><br>
                             DO NOT ADD ANYTHING ELSE IN THE EDITOR.<br>
                             Save and display. The pdf file will be embedded.';
$string['css'] = 'Stylesheet';
$string['displaymenu'] = 'Display noticeboard menu';
$string['displaymenuagree'] = 'Check if you would like to display menu';
$string['externalurl'] = 'Embed a Web page';
$string['format'] = 'noticeboard formatting';
$string['menucss'] = 'Modify menu stylesheet';
$string['menuname'] = 'Menu name';
$string['modulename'] = 'Noticeboard display';
$string['modulenameplural'] = 'noticeboard displays';
$string['morenoticeboards'] = 'Use more noticeboards';
$string['name'] = 'Name';
$string['noformating'] = 'No formatting';
$string['order'] = 'The order you would like this noticeboard in';
$string['pluginname'] = 'noticeboard display';
$string['pluginadministration'] = 'noticeboard Administration';
$string['noticeboard'] = 'noticeboard';
$string['noticeboardadministration'] = 'noticeboard Administration';
$string['noticeboardcontent'] = 'noticeboard content';
$string['noticeboardname'] = 'noticeboard name';
$string['noticeboardorder'] = 'noticeboard display activity order within menu';
$string['noticeboard:view'] = 'View noticeboards';
$string['updatethis'] = 'Update this noticeboard display';

?>