<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for embeded
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod
 * @subpackage embeded
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'embeded';
$string['modulenameplural'] = 'embededs';
$string['modulename_help'] = 'Use the embeded module for... | The embeded module allows...';
$string['embededfieldset'] = 'Custom example fieldset';
$string['embededname'] = 'Module title';
$string['embededtext'] = 'Embeded text';
$string['code'] = 'Code';
$string['optionalsettings'] = 'Optional Settings';
$string['hidenextmodule'] = 'Hide the "Next Module" button';
$string['hidedescriptiontext'] = 'Hide the description text when viewing the content';
$string['embededname_help'] = 'This is the content of the help tooltip associated with the embededname field. Markdown syntax is supported.';
$string['embeded'] = 'embeded';
$string['pluginadministration'] = 'embeded administration';
$string['pluginname'] = 'embeded';
