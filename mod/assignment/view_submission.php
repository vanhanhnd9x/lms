<?php
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG, $USER;


require_once("lib.php");
require_once($CFG->libdir.'/plagiarismlib.php');



require_login(0, false);


$assignment_id   = optional_param('assignment_id', 0, PARAM_INT);          // Course module ID
$mode = optional_param('mode', 'all', PARAM_ALPHA);  // What mode are we in?
$id=$assignment_id;

$PAGE->set_title('View submissions');
$PAGE->set_heading('View submissions');
//echo $OUTPUT->header();
if ($id) {
    if (! $cm = get_coursemodule_from_id('assignment', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $assignment = $DB->get_record("assignment", array("id"=>$cm->instance))) {
        print_error('invalidid', 'assignment');
    }

    if (! $course = $DB->get_record("course", array("id"=>$assignment->course))) {
        print_error('coursemisconf', 'assignment');
    }
    
} else {
    if (!$assignment = $DB->get_record("assignment", array("id"=>$a))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id"=>$assignment->course))) {
        print_error('coursemisconf', 'assignment');
    }
    if (! $cm = get_coursemodule_from_instance("assignment", $assignment->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
    
}


$PAGE->set_url($url);
//require_login($course->id, false, $cm);

require_capability('mod/assignment:grade', get_context_instance(CONTEXT_MODULE, $cm->id));

$PAGE->requires->js('/mod/assignment/assignment.js');

/// Load up the required assignment code
require($CFG->dirroot.'/mod/assignment/type/'.$assignment->assignmenttype.'/assignment.class.php');


$assignmentclass = 'assignment_'.$assignment->assignmenttype;
$assignmentinstance = new $assignmentclass($cm->id, $assignment, $cm, $course);
$assignmentinstance->submissions($mode);   // Display or process the submissions
?>
<script type="text/javascript" src="<?php echo $CFG->wwwroot?>/common/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/remove_submission_html.js"></script>
<div id="page-body">        


    


</div>