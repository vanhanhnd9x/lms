<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'moodle', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   moodle
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['abouttobeinstalled'] = 'about to be installed';
$string['action'] = 'Action';
$string['actions'] = 'Actions';
$string['active'] = 'Active';
$string['activate'] = 'Activate';
$string['addperson'] = 'Add Person';
$string['activeusers'] = 'Active users';
$string['activities'] = 'Activities';
$string['activities_help'] = 'Activities, such as forums, quizzes and wikis, enable interactive content to be added to the course.';
$string['activity'] = 'Activity';
$string['activityclipboard'] = 'Moving this activity: <b>{$a}</b>';
$string['activityiscurrentlyhidden'] = 'Sorry, this activity is currently hidden';
$string['activitymodule'] = 'Activity module';
$string['activitymodules'] = 'Activity modules';
$string['activityreport'] = 'Activity report';
$string['activityreports'] = 'Activity reports';
$string['activityselect'] = 'Select this activity to be moved elsewhere';
$string['activitysince'] = 'Activity since {$a}';
$string['activityweighted'] = 'Activity per user';
$string['add'] = 'Add';
$string['addactivity'] = 'Add an activity...';
$string['addadmin'] = 'Add admin';
$string['addblock'] = 'Add a block';
$string['addcomment'] = 'Add a comment...';
$string['addcountertousername'] = 'Create user by adding number to username';
$string['addcreator'] = 'Add course creator';
$string['adddots'] = 'Add...';
$string['added'] = 'Added {$a}';
$string['addedrecip'] = 'Added {$a} new recipient';
$string['addedrecips'] = 'Added {$a} new recipients';
$string['addedtogroup'] = 'Added to group "{$a}"';
$string['addedtogroupnot'] = 'Not added to group "{$a}"';
$string['addedtogroupnotenrolled'] = 'Not added to group "{$a}", because not enrolled in course';
$string['addinganew'] = 'Adding a new {$a}';
$string['addinganewto'] = 'Adding a new {$a->what} to {$a->to}';
$string['addingdatatoexisting'] = 'Adding data to existing';
$string['addnewcategory'] = 'Add new category';
$string['addnewcourse'] = 'Add a new course';
$string['addnewuser'] = 'Add a new user';
$string['addnousersrecip'] = 'Add users who haven\'t accessed this {$a} to recipient list';
$string['addresource'] = 'Add a resource...';
$string['address'] = 'Address';
$string['addstudent'] = 'Add student';
$string['addsubcategory'] = 'Add a sub-category';
$string['addteacher'] = 'Add teacher';
$string['admin'] = 'Admin';
$string['adminhelpaddnewuser'] = 'To manually create a new user account';
$string['adminhelpassignadmins'] = 'Admins can do anything and go anywhere in the site';
$string['adminhelpassigncreators'] = 'Course creators can create new courses';
$string['adminhelpassignsiteroles'] = 'Apply defined site roles to specific users';
$string['adminhelpassignstudents'] = 'Go into a course and add students from the admin menu';
$string['adminhelpauthentication'] = 'You can use internal user accounts or external databases';
$string['adminhelpbackup'] = 'Configure automated backups and their schedule';
$string['adminhelpconfiguration'] = 'Configure how the site looks and works';
$string['adminhelpconfigvariables'] = 'Configure variables that affect general operation of the site';
$string['adminhelpcourses'] = 'Define courses and categories and assign people to them, edit pending courses';
$string['adminhelpeditorsettings'] = 'Define basic settings for HTML editor';
$string['adminhelpedituser'] = 'Browse the list of user accounts and edit any of them';
$string['adminhelpenvironment'] = 'Check how your server suits current and future installation requirements';
$string['adminhelpfailurelogs'] = 'Browse logs of failed logins';
$string['adminhelphealthcenter'] = 'Automatic detection of site problems';
$string['adminhelplanguage'] = 'For checking and editing the current language pack';
$string['adminhelplogs'] = 'Browse logs of all activity on this site';
$string['adminhelpmanageblocks'] = 'Manage installed blocks and their settings';
$string['adminhelpmanagedatabase'] = 'Access the database directly (be careful!)';
$string['adminhelpmanagefilters'] = 'Choose text filters and related settings';
$string['adminhelpmanagemodules'] = 'Manage installed modules and their settings';
$string['adminhelpmanageroles'] = 'Create and define roles that may be applied to users';
$string['adminhelpmymoodle'] = 'Configure the My Moodle blocks for users';
$string['adminhelpreports'] = 'Site level reports';
$string['adminhelpsitefiles'] = 'For publishing general files or uploading external backups';
$string['adminhelpsitesettings'] = 'Define how the front page of the site looks';
$string['adminhelpstickyblocks'] = 'Configure Moodle-wide sticky blocks';
$string['adminhelpthemes'] = 'Choose how the site looks (colours, fonts etc)';
$string['adminhelpuploadusers'] = 'Import new user accounts from a text file';
$string['adminhelpusers'] = 'Define your users and set up authentication';
$string['adminhelpxmldbeditor'] = 'Interface to edit the XMLDB files. Only for developers.';
$string['administration'] = 'Administration';
$string['administrationsite'] = 'Site administration';
$string['administrator'] = 'Administrator';
$string['administratordescription'] = 'Administrators can usually do anything on the site, in all courses.';
$string['administrators'] = 'Administrators';
$string['administratorsall'] = 'All administrators';
$string['administratorsandteachers'] = 'Administrators and teachers';
$string['advanced'] = 'Advanced';
$string['advancedfilter'] = 'Advanced search';
$string['advancedsettings'] = 'Advanced settings';
$string['again'] = 'again';
$string['aimid'] = 'AIM ID';
$string['ajaxno'] = 'No: use basic web features';
$string['ajaxuse'] = 'AJAX and Javascript';
$string['ajaxyes'] = 'Yes: use advanced web features';
$string['all'] = 'All';
$string['allactions'] = 'All actions';
$string['allactivities'] = 'All activities';
$string['alldays'] = 'All days';
$string['allfieldsrequired'] = 'All fields are required';
$string['allfiles'] = 'All files';
$string['allgroups'] = 'All groups';
$string['allchanges'] = 'All changes';
$string['alllogs'] = 'All logs';
$string['allmods'] = 'All {$a}';
$string['allow'] = 'Allow';
$string['allowinternal'] = 'Allow internal methods as well';
$string['allownone'] = 'Allow none';
$string['allownot'] = 'Do not allow';
$string['allparticipants'] = 'All participants';
$string['allteachers'] = 'All teachers';
$string['alphanumerical'] = 'Can only contain alphanumeric characters, hyphen (-) or period (.)';
$string['alreadyconfirmed'] = 'Registration has already been confirmed';
$string['always'] = 'Always';
$string['and'] = '{$a->one} and {$a->two}';
$string['answer'] = 'Answer';
$string['any'] = 'Any';
$string['approve'] = 'Approve';
$string['areyousuretorestorethis'] = 'Do you want to continue?';
$string['areyousuretorestorethisinfo'] = 'Later in this process you will have a choice of adding this backup to an existing course or creating a completely new course.';
$string['asc'] = 'Ascending';
$string['assessment'] = 'Assessment';
$string['assignadmins'] = 'Assign admins';
$string['assigncreators'] = 'Assign creators';
$string['assignsiteroles'] = 'Assign site-wide roles';
$string['authenticateduser'] = 'Authenticated user';
$string['authenticateduserdescription'] = 'All logged in users.';
$string['authentication'] = 'Authentication';
$string['authenticationplugins'] = 'Authentication plugins';
$string['autosubscribe'] = 'Forum auto-subscribe';
$string['autosubscribeno'] = 'No: don\'t automatically subscribe me to forums';
$string['autosubscribeyes'] = 'Yes: when I post, subscribe me to that forum';
$string['availability'] = 'Library';
$string['availability_help'] = 'This setting determines whether the course appears in the list of courses. Apart from teachers and administrators, users are not allowed to enter the course.';
$string['availablecourses'] = 'Available courses';
$string['back'] = 'Back';
$string['backto'] = 'Back to {$a}';
$string['backtocourselisting'] = 'Back to course listing';
$string['backtopageyouwereon'] = 'Back to the page you were on';
$string['backtoparticipants'] = 'Back to participants list';
$string['backup'] = 'Backup';
$string['backupactivehelp'] = 'Choose whether or not to do automated backups.';
$string['backupcancelled'] = 'Backup cancelled';
$string['backupcoursefileshelp'] = 'If enabled then course files will be included in automated backups';
$string['backupdate'] = 'Backup date';
$string['backupdatenew'] = '&nbsp; {$a->TAG} is now {$a->weekday}, {$a->mday} {$a->month} {$a->year}<br />';
$string['backupdateold'] = '{$a->TAG} was {$a->weekday}, {$a->mday} {$a->month} {$a->year}';
$string['backupdaterecordtype'] = '<br />{$a->recordtype} - {$a->recordname}<br />';
$string['backupdetails'] = 'Backup details';
$string['backupexecuteathelp'] = 'Choose what time automated backups should run at.';
$string['backupfailed'] = 'Some of your courses weren\'t saved!!';
$string['backupfilename'] = 'backup';
$string['backupfinished'] = 'Backup completed successfully';
$string['backupfromthissite'] = 'Backup was made on this site?';
$string['backupgradebookhistoryhelp'] = 'If enabled then gradebook history will be included in automated backups. Note that grade history must not be disabled in server settings (disablegradehistory) in order for this to work';
$string['backupincludemoduleshelp'] = 'Choose whether you want to include course modules, with or without user data, in automated backups';
$string['backupincludemoduleuserdatahelp'] = 'Choose whether you want to include module user data in automated backups.';
$string['backupkeephelp'] = 'How many recent backups for each course do you want to keep? (older ones will be deleted automatically)';
$string['backuplogdetailed'] = 'Detailed execution log';
$string['backuploglaststatus'] = 'Last execution log';
$string['backupmissinguserinfoperms'] = 'Note: This backup contains no user data. Exercise and Workshop activities will not be included in the backup, since these modules are not compatible with this type of backup.';
$string['backupnext'] = 'Next backup';
$string['backupnonisowarning'] = 'Warning: this backup is from a non-Unicode version of Moodle (pre 1.6).  If this backup contains any non-ISO-8859-1 texts then they may be CORRUPTED if you try to restore them to this Unicode version of Moodle.  See the <a href="http://docs.moodle.org/en/Backup_FAQ">Backup FAQ</a> for more information about how to recover this backup correctly.';
$string['backuporiginalname'] = 'Backup name';
$string['backuproleassignments'] = 'Backup role assignments for these roles';
$string['backupsavetohelp'] = 'Full path to the directory where you want to save the backup files<br />(leave blank to save in its course default dir)';
$string['backupsitefileshelp'] = 'If enabled then site files used in courses will be included in automated backups';
$string['backuptakealook'] = 'Please take a look at your backup logs in:
  {$a}';
$string['backupuserfileshelp'] = 'Choose whether user files (eg profile images) should be included in automated backups';
$string['backupversion'] = 'Backup version';
$string['block'] = 'Block';
$string['blockconfiga'] = 'Configuring a {$a} block';
$string['blockconfigbad'] = 'This block has not been implemented correctly and thus cannot provide a configuration interface.';
$string['blockdeleteconfirm'] = 'You are about to completely delete the block \'{$a}\'.  This will completely delete everything in the database associated with this block. Are you SURE you want to continue?';
$string['blockdeletefiles'] = 'All data associated with the block \'{$a->block}\' has been deleted from the database.  To complete the deletion (and prevent the block re-installing itself), you should now delete this directory from your server: {$a->directory}';
$string['blocks'] = 'Blocks';
$string['blocksaddedit'] = 'Add/Edit blocks';
$string['blockseditoff'] = 'Blocks editing off';
$string['blocksediton'] = 'Blocks editing on';
$string['blocksetup'] = 'Setting up block tables';
$string['blocksuccess'] = '{$a} tables have been set up correctly';
$string['brief'] = 'Brief';
$string['bycourseorder'] = 'By course order';
$string['byname'] = 'by {$a}';
$string['bypassed'] = 'Bypassed';
$string['cachecontrols'] = 'Cache controls';
$string['cancel'] = 'Cancel';
$string['cancelled'] = 'Cancelled';
$string['categories'] = 'Course categories';
$string['category'] = 'Category';
$string['category_help'] = 'This setting determines the category in which the course will appear in the list of courses.';
$string['categoryadded'] = 'The category \'{$a}\' was added';
$string['categorycontents'] = 'Subcategories and courses';
$string['categorycurrentcontents'] = 'Contents of {$a}';
$string['categorydeleted'] = 'The category \'{$a}\' was deleted';
$string['categoryduplicate'] = 'A category named \'{$a}\' already exists!';
$string['categorymodifiedcancel'] = 'Category was modified! Please cancel and try again.';
$string['categoryname'] = 'Category name';
$string['categoryupdated'] = 'The category \'{$a}\' was updated';
$string['city'] = 'City/town';
$string['clambroken'] = 'Your administrator has enabled virus checking for file uploads but has misconfigured something.<br />Your file upload was NOT successful. Your administrator has been emailed to notify them so they can fix it.<br />Maybe try uploading this file later.';
$string['clamdeletedfile'] = 'The file has been deleted';
$string['clamdeletedfilefailed'] = 'The file could not be deleted';
$string['clamemailsubject'] = '{$a} :: Clam AV notification';
$string['clamfailed'] = 'Clam AV has failed to run.  The return error message was {$a}. Here is the output from Clam:';
$string['clamlost'] = 'Moodle is configured to run clam on file upload, but the path supplied to Clam AV, {$a},  is invalid.';
$string['clamlostandactinglikevirus'] = 'In addition, Moodle is configured so that if clam fails to run, files are treated like viruses.  This essentially means that no student can upload a file successfully until you fix this.';
$string['clammovedfile'] = 'The file has been moved to your specified quarantine directory, the new location is {$a}';
$string['clammovedfilebasic'] = 'The file has been moved to a quarantine directory.';
$string['clamquarantinedirfailed'] = 'Could not move the file into your specified quarantine directory, {$a}. You need to fix this as files are being deleted if they\'re found to be infected.';
$string['clamunknownerror'] = 'There was an unknown error with clam.';
$string['cleaningtempdata'] = 'Cleaning temp data';
$string['clear'] = 'Clear';
$string['clickhelpiconformoreinfo'] = '... continues ... Click on the help icon to read the full article';
$string['clickhere'] = 'Click here ...';
$string['clicktohideshow'] = 'Click to expand or collapse';
$string['clicktochange'] = 'Click to change';
$string['closewindow'] = 'Close this window';
$string['collapseall'] = 'Collapse all';
$string['commentincontext'] = 'Find this comment in context';
$string['comments'] = 'Comments';
$string['commentsnotenabled'] = 'Comments feature is not enabled';
$string['commentsrequirelogin'] = 'You need to login to view the comments';
$string['comparelanguage'] = 'Compare and edit current language';
$string['complete'] = 'Complete';
$string['completereport'] = 'Complete report';
$string['configuration'] = 'Configuration';
$string['confirm'] = 'Confirm';
$string['confirmed'] = 'Your registration has been confirmed';
$string['confirmednot'] = 'Your registration has not yet been confirmed!';
$string['confirmcheckfull'] = 'Are you absolutely sure you want to confirm {$a} ?';
$string['content'] = 'Content';
$string['continue'] = 'Continue';
$string['continuetocourse'] = 'Click here to enter your course';
$string['convertingwikitomarkdown'] = 'Converting wiki to Markdown';
$string['cookiesenabled'] = 'Cookies must be enabled in your browser';
$string['cookiesenabled_help'] = 'Two cookies are used by this site:

The essential one is the session cookie, usually called MoodleSession. You must allow this cookie into your browser to provide continuity and maintain your login from page to page. When you log out or close the browser this cookie is destroyed (in your browser and on the server).

The other cookie is purely for convenience, usually called something like MOODLEID. It just remembers your username within the browser. This means when you return to this site the username field on the login page will be already filled out for you. It is safe to refuse this cookie - you will just have to retype your username every time you log in.';
$string['cookiesnotenabled'] = 'Unfortunately, cookies are currently not enabled in your browser';
$string['copy'] = 'copy';
$string['copyasnoun'] = 'copy';
$string['copyingcoursefiles'] = 'Copying course files';
$string['copyingsitefiles'] = 'Copying site files used in course';
$string['copyinguserfiles'] = 'Copying user files';
$string['copyingzipfile'] = 'Copying zip file';
$string['copyrightnotice'] = 'Copyright notice';
$string['coresystem'] = 'System';
$string['cost'] = 'Cost';
$string['costdefault'] = 'Default cost';
$string['counteditems'] = '{$a->count} {$a->items}';
$string['country'] = 'Country';
$string['course'] = 'Course';
$string['courseadministration'] = 'Course administration';
$string['courseapprovedemail'] = 'Your requested course, {$a->name}, has been approved and you have been made a {$a->teacher}.  To access your new course, go to {$a->url}';
$string['courseapprovedemail2'] = 'Your requested course, {$a->name}, has been approved.  To access your new course, go to {$a->url}';
$string['courseapprovedfailed'] = 'Failed to save the course as approved!';
$string['courseapprovedsubject'] = 'Your course has been approved!';
$string['courseavailable'] = 'This course is available to learners';
$string['courseavailablenot'] = 'This course is not available to learners';
$string['coursebackup'] = 'Course backup';
$string['coursecategories'] = 'Course categories';
$string['coursecategory'] = 'Course category';
$string['coursecategorydeleted'] = 'Deleted course category {$a}';
$string['coursecompletion'] = 'Course completion';
$string['coursecompletions'] = 'Course completions';
$string['coursecreators'] = 'Course creator';
$string['coursecreatorsdescription'] = 'Course creators can create new courses.';
$string['coursedeleted'] = 'Deleted course {$a}';
$string['coursefiles'] = 'Legacy course files';
$string['coursefilesedit'] = 'Edit legacy course files';
$string['coursefileswarning'] = 'Course files are deprecated';
$string['coursefileswarning_help'] = 'Course files are deprecated since Moodle 2.0, please use external repositories instead as much as possible.';
$string['courseformatdata'] = 'Course format data';
$string['courseformats'] = 'Course formats';
$string['coursegrades'] = 'Course grades';
$string['coursehelpcategory'] = 'Position the course on the course listing and may make it easier for students to find it.';
$string['coursehelpforce'] = 'Force the course group mode to every activity in the course.';
$string['coursehelpformat'] = 'The course main page will be displayed in this format.';
$string['coursehelphiddensections'] = 'How the hidden sections in the course are displayed to students.';
$string['coursehelpmaximumupload'] = 'Define the largest size of file that can be uploaded in this course, limited by the site-wide setting.';
$string['coursehelpnewsitemsnumber'] = 'Number of recent items appearing on the course home page, in a news box down the right-hand side (0 means the news box won\'t appear).';
$string['coursehelpnumberweeks'] = 'Number of weeks/topics displayed on the course main page.';
$string['coursehelpshowgrades'] = 'Enable the display of the gradebook. It does not prevent grades from being displayed within the individual activities.';
$string['coursehidden'] = 'This course is currently unavailable to students';
$string['courseinfo'] = 'Course info';
$string['coursemessage'] = 'Message course users';
$string['coursenotaccessible'] = 'This course does not allow public access';
$string['courselegacyfiles'] = 'Legacy course files';
$string['courselegacyfiles_help'] = 'The course files area provides some backward compatibility with Moodle 1.9 and earlier.  All files in this area are always accessible to all participants in the course (whether you link to them or not) and there is no way to know where any of these files are being used in Moodle.

If you use this area to store course files, you can expose yourself to a number of privacy and security issues, as well as experiencing missing files in backups, course imports and any time content is shared or re-used.  It is therefore recommended that you do not use this area unless you really know what you are doing.

The link below provides more information about all this and will show you some better ways to manage files in Moodle 2.';
$string['courselegacyfiles_link'] = 'coursefiles2';
$string['courseoverview'] = 'Course overview';
$string['courseoverviewgraph'] = 'Course overview graph';
$string['courseprofiles'] = 'Course profiles';
$string['coursereasonforrejecting'] = 'Your reasons for rejecting this request';
$string['coursereasonforrejectingemail'] = 'This will be emailed to the requester';
$string['coursereject'] = 'Reject a course request';
$string['courserejected'] = 'Course has been rejected and the requester has been notified.';
$string['courserejectemail'] = 'Sorry, but the course you requested has been rejected. Here is the reason provided:

{$a}';
$string['courserejectreason'] = 'Outline your reasons for rejecting this course<br />(this will be emailed to the requester)';
$string['courserejectsubject'] = 'Your course has been rejected';
$string['coursereport'] = 'Course report';
$string['coursereports'] = 'Course reports';
$string['courserequest'] = 'Course request';
$string['courserequestdetails'] = 'Details of the course you are requesting';
$string['courserequestfailed'] = 'For some reason, your course request could not be saved';
$string['courserequestintro'] = 'Use this form to request a course to be created for you.<br />Try and fill in as much information as you can to allow<br />the administrators to understand your reasons for wanting this course.';
$string['courserequestreason'] = 'Reasons for wanting this course';
$string['courserequestsuccess'] = 'Successfully saved your course request. Expect an email within a few days with the outcome';
$string['courserequestsupport'] = 'Supporting information to help the administrator evaluate this request';
$string['courserestore'] = 'Course restore';
$string['courses'] = 'Courses';
$string['coursesectionsummaries'] = 'Course section summaries';
$string['coursesettings'] = 'Course default settings';
$string['coursesmovedout'] = 'Courses moved out from {$a}';
$string['coursespending'] = 'Courses pending approval';
$string['coursestart'] = 'Course start';
$string['coursesummary'] = 'Description';
$string['coursesummary_help'] = 'The course summary is displayed in the list of courses. A course search searches course summary text in addition to course names.';
$string['courseupdates'] = 'Course updates';
$string['courseuploadlimit'] = 'Course upload limit';
$string['help'] = 'Help';
$string['create'] = 'Create';
$string['createaccount'] = 'Create my new account';
$string['createcategory'] = 'Create category';
$string['createfolder'] = 'Create a folder in {$a}';
$string['createuserandpass'] = 'Choose your username and password';
$string['createziparchive'] = 'Create zip archive';
$string['creatingblocks'] = 'Creating blocks';
$string['creatingblocksroles'] = 'Creating block level role assignments and overrides';
$string['creatingblogsinfo'] = 'Creating blogs info';
$string['creatingcategoriesandquestions'] = 'Creating categories and questions';
$string['creatingcoursemodules'] = 'Creating course modules';
$string['creatingcourseroles'] = 'Creating course level role assignments and overrides';
$string['creatingevents'] = 'Creating events';
$string['creatinggradebook'] = 'Creating gradebook';
$string['creatinggroupings'] = 'Creating groupings';
$string['creatinggroupingsgroups'] = 'Adding groups into groupings';
$string['creatinggroups'] = 'Creating groups';
$string['creatinglogentries'] = 'Creating log entries';
$string['creatingmessagesinfo'] = 'Creating messages info';
$string['creatingmodroles'] = 'Creating module level role assignments and overrides';
$string['creatingnewcourse'] = 'Creating new course';
$string['creatingrolesdefinitions'] = 'Creating roles definitions';
$string['creatingscales'] = 'Creating scales';
$string['creatingsections'] = 'Creating sections';
$string['creatingtemporarystructures'] = 'Creating temporary structures';
$string['creatinguserroles'] = 'Creating user level role assignments and overrides';
$string['creatingusers'] = 'Creating users';
$string['creatingxmlfile'] = 'Creating XML file';
$string['currency'] = 'Currency';
$string['currentcourseadding'] = 'Current course, adding data to it';
$string['currentcoursedeleting'] = 'Current course, deleting it first';
$string['currentlanguage'] = 'Current language';
$string['currentlocaltime'] = 'your current local time';
$string['currentlyselectedusers'] = 'Currently selected users';
$string['currentpicture'] = 'Current picture';
$string['currentrelease'] = 'Current release information';
$string['currentversion'] = 'Current version';
$string['databasechecking'] = 'Upgrading Moodle database from version {$a->oldversion} to {$a->newversion}';
$string['databaseperformance'] = 'Database performance';
$string['databasesetup'] = 'Setting up database';
$string['databasesuccess'] = 'Database was successfully upgraded';
$string['databaseupgradebackups'] = 'Backup version is now {$a}';
$string['databaseupgradeblocks'] = 'Blocks version is now {$a}';
$string['databaseupgradegroups'] = 'Groups version is now {$a}';
$string['databaseupgradelocal'] = 'Local database customisations version is now {$a}';
$string['databaseupgrades'] = 'Upgrading database';
$string['date'] = 'Date';
$string['datechanged'] = 'Date changed';
$string['datemostrecentfirst'] = 'Date - most recent first';
$string['datemostrecentlast'] = 'Date - most recent last';
$string['day'] = 'day';
$string['days'] = 'days';
$string['decodinginternallinks'] = 'Decoding internal links';
$string['default'] = 'Default';
$string['defaultcoursestudent'] = 'Student';
$string['defaultcoursestudentdescription'] = 'Students generally have fewer privileges within a course.';
$string['defaultcoursestudents'] = 'Students';
$string['defaultcoursesummary'] = 'Write a concise and interesting paragraph here that explains what this course is about';
$string['defaultcourseteacher'] = 'Teacher';
$string['defaultcourseteacherdescription'] = 'Teachers can do anything within a course, including changing the activities and grading students.';
$string['defaultcourseteachers'] = 'Teachers';
$string['delete'] = 'Delete';
$string['deleteablock'] = 'Delete a block';
$string['deleteall'] = 'Delete all';
$string['deleteallcannotundo'] = 'Delete all - cannot be undone';
$string['deleteallcomments'] = 'Delete all comments';
$string['deleteallratings'] = 'Delete all ratings';
$string['deletecategory'] = 'Delete category: {$a}';
$string['deletecategoryempty'] = 'This category is empty.';
$string['deletecategorycheck'] = 'Are you absolutely sure you want to completely delete this category <b>\'{$a}\'</b>?<br />This will move all courses into the parent category if there is one, or into Miscellaneous.';
$string['deletecategorycheck2'] = 'If you delete this category, you need to choose what to do with the courses and subcategories it contains.';
$string['deletecomment'] = 'Delete this comment';
$string['deletecompletely'] = 'Delete completely';
$string['deletecourse'] = 'Delete a course';
$string['deletecoursecheck'] = 'Are you absolutely sure you want to completely delete this course and all the data it contains?';
$string['deleted'] = 'Deleted';
$string['deletedactivity'] = 'Deleted {$a}';
$string['deletedcourse'] = '{$a} has been completely deleted';
$string['deletednot'] = 'Could not delete {$a} !';
$string['deletecheck'] = 'Delete {$a} ?';
$string['deletecheckfiles'] = 'Are you absolutely sure you want to delete these files?';
$string['deletecheckfull'] = 'Are you absolutely sure you want to completely delete {$a} ?';
$string['deletecheckwarning'] = 'You are about to delete these files';
$string['deletelogs'] = 'Delete logs';
$string['deleteselected'] = 'Delete selected';
$string['deleteselectedkey'] = 'Delete selected key';
$string['deletingcourse'] = 'Deleting {$a}';
$string['deletingexistingcoursedata'] = 'Deleting existing course data';
$string['deletingolddata'] = 'Deleting old data';
$string['department'] = 'Department';
$string['desc'] = 'Descending';
$string['description'] = 'Description';
$string['deselectall'] = 'Deselect all';
$string['detailedless'] = 'Less detailed';
$string['detailedmore'] = 'More detailed';
$string['directory'] = 'Directory';
$string['disable'] = 'Disable';
$string['disabledcomments'] = 'Comments are disabled';
$string['displayingfirst'] = 'Only the first {$a->count} {$a->things} are displayed';
$string['displayingrecords'] = 'Displaying {$a} records';
$string['displayingusers'] = 'Displaying users {$a->start} to {$a->end}';
$string['displayonpage'] = 'Display on page';
$string['documentation'] = 'Moodle documentation';
$string['down'] = 'Down';
$string['download'] = 'Download';
$string['downloadall'] = 'Download all';
$string['downloadexcel'] = 'Download in Excel format';
$string['downloadfile'] = 'Download file';
$string['downloadods'] = 'Download in ODS format';
$string['downloadtext'] = 'Download in text format';
$string['doyouagree'] = 'Have you read these conditions and understood them?';
$string['duplicate'] = 'Duplicate';
$string['duplicateconfirm'] = 'Are you sure you want to duplicate {$a->modtype} \'{$a->modname}\' ?';
$string['duplicatecontcourse'] = 'Return to the course';
$string['duplicatecontedit'] = 'Edit the new copy';
$string['duplicatesuccess'] = '{$a->modtype} \'{$a->modname}\' has been duplicated successfully';
$string['duplicatinga'] = 'Duplicating: {$a}';
$string['edhelpaspellpath'] = 'To use spell-checking within the editor, you MUST have <strong>aspell 0.50</strong> or later installed on your server, and you must specify the correct path to access the aspell binary.  On Unix/Linux systems, this path is usually <strong>/usr/bin/aspell</strong>, but it might be something else.';
$string['edhelpbgcolor'] = 'Define the edit area\'s background color.<br />Valid values are, for example: #FFFFFF or white';
$string['edhelpcleanword'] = 'This setting enables or disables Word-specific format filtering.';
$string['edhelpenablespelling'] = 'Enable or disable spell-checking. When enabled, <strong>aspell</strong> must be installed on the server. The second value is the <strong>default dictionary</strong>. This value will be used if aspell doesn\'t have dictionary for users own language.';
$string['edhelpfontfamily'] = 'The font-family property is a list of font family names and/or generic family names. Family names must be separated with comma.';
$string['edhelpfontlist'] = 'Define the fonts used on editors dropdown menu.';
$string['edhelpfontsize'] = 'The default font-size sets the size of a font. <br />Valid values are for example: medium, large, smaller, larger, 10pt, 11px.';
$string['edit'] = 'Edit';
$string['edita'] = 'Edit {$a}';
$string['editcategorysettings'] = 'Edit category settings';
$string['editcategorythis'] = 'Edit this category';
$string['editcoursesettings'] = 'Edit course settings';
$string['editfiles'] = 'Edit files';
$string['editgroupprofile'] = 'Edit group profile';
$string['editinga'] = 'Editing {$a}';
$string['editingteachershort'] = 'Editor';
$string['editlock'] = 'This value cannot be edited!';
$string['editmyprofile'] = 'Edit profile';
$string['editorbgcolor'] = 'Background-color';
$string['editorcleanonpaste'] = 'Clean Word HTML on paste';
$string['editorcommonsettings'] = 'Common settings';
$string['editordefaultfont'] = 'Default font';
$string['editorenablespelling'] = 'Enable spellchecking';
$string['editorfontlist'] = 'Fontlist';
$string['editorfontsize'] = 'Default font-size';
$string['editorresettodefaults'] = 'Reset to default values';
$string['editorsettings'] = 'Editor settings';
$string['editorshortcutkeys'] = 'Editor shortcut keys';
$string['editsettings'] = 'Edit settings';
$string['editsummary'] = 'Edit summary';
$string['editthisactivity'] = 'Edit this activity';
$string['editthiscategory'] = 'Edit this category';
$string['edituser'] = 'Edit user accounts';
$string['email'] = 'Email';
$string['emailactive'] = 'Email activated';
$string['emailagain'] = 'Email (again)';
$string['emailconfirm'] = 'Confirm your account';
$string['emailconfirmation'] = 'Hi {$a->firstname},

A new account has been requested at \'{$a->sitename}\'
using your email address.

To confirm your new account, please go to this web address:

{$a->link}

In most mail programs, this should appear as a blue link
which you can just click on.  If that doesn\'t work,
then cut and paste the address into the address
line at the top of your web browser window.

If you need help, please contact the site administrator,
{$a->admin}';
$string['emailconfirmationsubject'] = '{$a}: account confirmation';
$string['emailconfirmsent'] = '<p>An email should have been sent to your address at <b>{$a}</b></p>
   <p>It contains easy instructions to complete your registration.</p>
   <p>If you continue to have difficulty, contact the site administrator.</p>';
$string['emaildigest'] = 'Email digest type';
$string['emaildigestcomplete'] = 'Complete (daily email with full posts)';
$string['emaildigestoff'] = 'No digest (single email per forum post)';
$string['emaildigestsubjects'] = 'Subjects (daily email with subjects only)';
$string['emaildisable'] = 'This email address is disabled';
$string['emaildisableclick'] = 'Click here to disable all email from being sent to this address';
$string['emaildisplay'] = 'Email display';
$string['emaildisplaycourse'] = 'Allow only other course members to see my email address';
$string['emaildisplayno'] = 'Hide my email address from everyone';
$string['emaildisplayyes'] = 'Allow everyone to see my email address';
$string['emailenable'] = 'This email address is enabled';
$string['emailenableclick'] = 'Click here to re-enable all email being sent to this address';
$string['emailexists'] = 'This email address is already registered.';
$string['emailformat'] = 'Email format';
$string['emailcharset'] = 'Email charset';
$string['emailmustbereal'] = 'Note: your email address must be a real one';
$string['emailnotallowed'] = 'Email addresses in these domains are not allowed ({$a})';
$string['emailnotfound'] = 'The email address was not found in the database';
$string['emailonlyallowed'] = 'This email is not one of those that are allowed ({$a})';
$string['emailpasswordconfirmation'] = 'Hi {$a->firstname},

Someone (probably you) has requested a new password for your
account on \'{$a->sitename}\'.

To confirm this and have a new password sent to you via email,
go to the following web address:

{$a->link}

In most mail programs, this should appear as a blue link
which you can just click on.  If that doesn\'t work,
then cut and paste the address into the address
line at the top of your web browser window.

If you need help, please contact the site administrator,
{$a->admin}';
$string['emailpasswordconfirmationsubject'] = '{$a}: Change password confirmation';
$string['emailpasswordconfirmmaybesent'] = '<p>If you supplied a correct username or email address then an email should have been sent to you.</p>
   <p>It contains easy instructions to confirm and complete this password change.
If you continue to have difficulty, please contact the site administrator.</p>';
$string['emailpasswordconfirmsent'] = 'An email should have been sent to your address at <b>{$a}</b>.
<br />It contains easy instructions to confirm and complete this password change.
If you continue to have difficulty, contact the site administrator.';
$string['emailpasswordchangeinfo'] = 'Hi {$a->firstname},

Someone (probably you) has requested a new password for your
account on \'{$a->sitename}\'.

To change your password, please go to the following web address:

{$a->link}

In most mail programs, this should appear as a blue link
which you can just click on.  If that doesn\'t work,
then cut and paste the address into the address
line at the top of your web browser window.

If you need help, please contact the site administrator,
{$a->admin}';
$string['emailpasswordchangeinfodisabled'] = 'Hi {$a->firstname},

Someone (probably you) has requested a new password for your
account on \'{$a->sitename}\'.

Unfortunately your account on this site is disabled and can not be reset,
please contact the site administrator,
{$a->admin}';
$string['emailpasswordchangeinfofail'] = 'Hi {$a->firstname},

Someone (probably you) has requested a new password for your
account on \'{$a->sitename}\'.

Unfortunately passwords can not be reset on this site,
please contact the site administrator,
{$a->admin}';
$string['emailpasswordchangeinfosubject'] = '{$a}: Change password information';
$string['emailpasswordsent'] = 'Thank you for confirming the change of password.
An email containing your new password has been sent to your address at<br /><b>{$a->email}</b>.<br />
The new password was automatically generated - you might like to
<a href="{$a->link}">change your password</a> to something easier to remember.';
$string['enable'] = 'Enable';
$string['encryptedcode'] = 'Encrypted code';
$string['english'] = 'English';
$string['entercourse'] = 'Click to enter this course';
$string['enteremail'] = 'Enter your email address';
$string['enteremailaddress'] = 'Enter in your email address to reset your
   password and have the new password sent to you via email.';
$string['enterusername'] = 'Enter your username';
$string['entries'] = 'Entries';
$string['error'] = 'Error';
$string['errortoomanylogins'] = 'Sorry, you have exceeded the allowed number of login attempts. Restart your browser.';
$string['errorwhenconfirming'] = 'You are not confirmed yet because an error occurred.  If you clicked on a link in an email to get here, make sure that the line in your email wasn\'t broken or wrapped. You may have to use cut and paste to reconstruct the link properly.';
$string['everybody'] = 'Everybody';
$string['executeat'] = 'Execute at';
$string['existing'] = 'Existing';
$string['existingadmins'] = 'Existing admins';
$string['existingcourse'] = 'Existing course';
$string['existingcourseadding'] = 'Existing course, adding data to it';
$string['existingcoursedeleting'] = 'Existing course, deleting it first';
$string['existingcreators'] = 'Existing course creators';
$string['existingstudents'] = 'Enrolled students';
$string['existingteachers'] = 'Existing teachers';
$string['expandall'] = 'Expand all';
$string['expirynotify'] = 'Enrolment expiry notification';
$string['expirynotifyemail'] = 'The following students in this course are expiring after exactly {$a->threshold} days:

{$a->current}

The following students in this course are expiring in less than {$a->threshold} days:

{$a->past}

You may go to the following page to extend their enrolment period:
{$a->extendurl}';
$string['expirynotifystudents'] = 'Notify students';
$string['expirynotifystudents_help'] = 'If an enrolment duration has been specified, then this setting determines whether students receive email notification when they are about to be unenrolled from the course.';
$string['expirynotifystudentsemail'] = 'Dear {$a->studentstr}:

This is a notification that your enrolment in the course {$a->course} will expire in {$a->threshold} days.

Please contact {$a->teacherstr} for any further enquiries.';
$string['expirythreshold'] = 'Threshold';
$string['expirythreshold_help'] = 'If an enrolment duration has been specified, then this setting determines the number of days notice given before students are unenrolled from the course.';
$string['explanation'] = 'Explanation';
$string['extendenrol'] = 'Extend enrolment (individual)';
$string['extendperiod'] = 'Extended period';
$string['failedloginattempts'] = '{$a->attempts} failed logins since your last login';
$string['failedloginattemptsall'] = '{$a->attempts} failed logins for {$a->accounts} accounts';
$string['feedback'] = 'Feedback';
$string['fieldrequired'] = '{$a} required';
$string['file'] = 'File';
$string['filemissing'] = '{$a} is missing';
$string['files'] = 'Files';
$string['filesfolders'] = 'Files/folders';
$string['filloutallfields'] = 'Please fill out all fields in this form';
$string['filter'] = 'Filter';
$string['findmorecourses'] = 'Find more courses...';
$string['firstaccess'] = 'First access';
$string['firstname'] = 'First name';
$string['firsttime'] = 'Is this your first time here?';
$string['flashlinkmessage'] = 'Please upgrade your Flash player now:';
$string['flashupgrademessage'] = 'The Flash plugin is required to play this content, but the version you have is too old.

  You may need to log out and log in back after upgrade.';
$string['folder'] = 'Folder';
$string['folderclosed'] = 'Closed folder';
$string['folderopened'] = 'Opened folder';
$string['followingoptional'] = 'The following items are optional';
$string['followingrequired'] = 'The following items are required';
$string['force'] = 'Force';
$string['forcedmode'] = 'forced mode';
$string['forcelanguage'] = 'Force language';
$string['forceno'] = 'Do not force';
$string['forcepasswordchange'] = 'Force password change';
$string['forcepasswordchange_help'] = 'If this checkbox is ticked, the user will be prompted to change their password on their next login';
$string['forcepasswordchangecheckfull'] = 'Are you absolutely sure you want to force a password change to {$a} ?';
$string['forcepasswordchangenot'] = 'Could not force a password change to {$a}';
$string['forcepasswordchangenotice'] = 'You must change your password to proceed.';
$string['forcetheme'] = 'Force theme';
$string['forgotaccount'] = 'Lost password?';
$string['forgotten'] = 'Forgotten your username or password?';
$string['forgotten_new'] = "I've forgotten my username/password";
$string['forgottenduplicate'] = 'The email address is shared by several accounts, please enter username instead';
$string['forgotteninvalidurl'] = 'Invalid password reset URL';
$string['format'] = 'Format';
$string['format_help'] = 'The course format determines the layout of the course page.

* SCORM format - For displaying a SCORM package in the first section of the course page (as an alternative to using the SCORM/AICC module)
* Social format - A forum is displayed on the course page
* Topics format - The course page is organised into topic sections
* Weekly format - The course page is organised into weekly sections, with the first week starting on the course start date';
$string['formathtml'] = 'HTML format';
$string['formatmarkdown'] = 'Markdown format';
$string['formatplain'] = 'Plain text format';
$string['formattext'] = 'Moodle auto-format';
$string['formattexttype'] = 'Formatting';
$string['framesetinfo'] = 'This frameset document contains:';
$string['from'] = 'From';
$string['frontpagecategorycombo'] = 'Combo list';
$string['frontpagecategorynames'] = 'List of categories';
$string['frontpagecourselist'] = 'List of courses';
$string['frontpagedescription'] = 'Front page description';
$string['frontpagedescriptionhelp'] = 'This description of the site will be displayed on the front page.';
$string['frontpageformat'] = 'Front page format';
$string['frontpageformatloggedin'] = 'Front page format when logged in';
$string['frontpagenews'] = 'News items';
$string['frontpagesettings'] = 'Front page settings';
$string['frontpagetopiconly'] = 'Topic section';
$string['fulllistofcourses'] = 'All courses';
$string['fullname'] = 'Full name'; // @deprecated - use fullnamecourse or fullnameuser or some own context specific string
$string['fullnamecourse'] = 'Course Title';
$string['fullnamecourse_help'] = 'The full name of the course is displayed at the top of each page in the course and in the list of courses.';
$string['fullnamedisplay'] = '{$a->firstname} {$a->lastname}';
$string['fullnameuser'] = 'User full name';
$string['fullprofile'] = 'Full profile';
$string['fullsitename'] = 'Full site name';
$string['functiondisabled'] = 'That functionality is currently disabled';
$string['gdneed'] = 'GD must be installed to see this graph';
$string['gdnot'] = 'GD is not installed';
$string['gd1'] = 'GD 1.x is installed';
$string['gd2'] = 'GD 2.x is installed';
$string['general'] = 'General';
$string['geolocation'] = 'latitude - longitude';
$string['gettheselogs'] = 'Get these logs';
$string['go'] = 'Go';
$string['gpl'] = 'Copyright (C) 1999 onwards  Martin Dougiamas  (http://moodle.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the Moodle License information page for full details:
http://docs.moodle.org/dev/License';
$string['gpllicense'] = 'GPL license';
$string['gpl3'] = 'Copyright (C) 1999 onwards Martin Dougiamas (http://moodle.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the Moodle License information page for full details:
http://docs.moodle.org/dev/License';
$string['grade'] = 'Grade';
$string['grades'] = 'Grades';
$string['group'] = 'Group';
$string['groupadd'] = 'Add new group';
$string['groupaddusers'] = 'Add selected to group';
$string['groupextendenrol'] = 'Extend enrolment (common)';
$string['groupfor'] = 'for group';
$string['groupinfo'] = 'Info about selected group';
$string['groupinfoedit'] = 'Edit group settings';
$string['groupinfomembers'] = 'Info about selected members';
$string['groupinfopeople'] = 'Info about selected people';
$string['groupmembers'] = 'Group members';
$string['groupmemberssee'] = 'See group members';
$string['groupmembersselected'] = 'Members of selected group';
$string['groupmode'] = 'Group mode';
$string['groupmodeforce'] = 'Force group mode';
$string['groupmy'] = 'My group';
$string['groupnonmembers'] = 'People not in a group';
$string['groupnotamember'] = 'Sorry, you are not a member of that group';
$string['grouprandomassign'] = 'Randomly assign all to groups';
$string['groupremove'] = 'Remove selected group';
$string['groupremovemembers'] = 'Remove selected members';
$string['groups'] = 'Groups';
$string['groupsnone'] = 'No groups';
$string['groupsseparate'] = 'Separate groups';
$string['groupsvisible'] = 'Visible groups';
$string['guest'] = 'Guest';
$string['guestdescription'] = 'Guests have minimal privileges and usually can not enter text anywhere.';
$string['guestskey'] = 'Allow guests who have the key';
$string['guestsno'] = 'Do not allow guests in';
$string['guestsnotallowed'] = 'Sorry, \'{$a}\' does not allow guests to enter.';
$string['guestsyes'] = 'Allow guests without the key';
$string['guestuser'] = 'Guest user';
$string['guestuserinfo'] = 'This user is a special user that allows read-only access to some courses.';
$string['healthcenter'] = 'Health center';
$string['healthnoproblemsfound'] = 'There is no health problem found!';
$string['healthproblemsdetected'] = 'Health problems detected!';
$string['healthproblemsolution'] = 'Health problem solution';
$string['healthreturntomain'] = 'Continue';
$string['healthsolution'] = 'Solution';
$string['help'] = 'Help';
$string['helpprefix2'] = 'Help with {$a}';
$string['helpwiththis'] = 'Help with this';
$string['hiddenassign'] = 'Hidden assignment';
$string['hiddenfromstudents'] = 'Hidden from students';
$string['hiddensections'] = 'Hidden sections';
$string['hiddensections_help'] = 'This setting determines whether hidden sections are displayed to students in collapsed form (perhaps for a course in weekly format to indicate holidays) or are completely hidden.';
$string['hiddensectionscollapsed'] = 'Hidden sections are shown in collapsed form';
$string['hiddensectionsinvisible'] = 'Hidden sections are completely invisible';
$string['hide'] = 'Hide';
$string['hideadvancedsettings'] = 'Hide advanced settings';
$string['hidepicture'] = 'Hide picture';
$string['hidesection'] = 'Hide section {$a}';
$string['hidesettings'] = 'Hide settings';
$string['hideshowblocks'] = 'Hide or show blocks';
$string['hidetopicfromothers'] = 'Hide topic';
$string['hideweekfromothers'] = 'Hide week';
$string['hits'] = 'Hits';
$string['hitsoncourse'] = 'Hits on {$a->coursename} by {$a->username}';
$string['hitsoncoursetoday'] = 'Today\'s hits on {$a->coursename} by {$a->username}';
$string['home'] = 'Home';
$string['hour'] = 'hour';
$string['hours'] = 'hours';
$string['howtomakethemes'] = 'How to make new themes';
$string['htmleditor'] = 'Use HTML editor (some browsers only)';
$string['htmleditoravailable'] = 'The HTML editor is available';
$string['htmleditordisabled'] = 'You have disabled the HTML editor in your user profile';
$string['htmleditordisabledadmin'] = 'The administrator has disabled the HTML editor on this site';
$string['htmleditordisabledbrowser'] = 'The HTML editor is unavailable because your web browser is not compatible';
$string['htmlfilesonly'] = 'HTML files only';
$string['htmlformat'] = 'Pretty HTML format';
$string['changedpassword'] = 'Changed password';
$string['changepassword'] = 'Change password';
$string['changessaved'] = 'Changes saved';
$string['check'] = 'Check';
$string['checkall'] = 'Check all';
$string['checkingbackup'] = 'Checking backup';
$string['checkingcourse'] = 'Checking course';
$string['checkingforbbexport'] = 'Checking for BlackBoard export';
$string['checkinginstances'] = 'Checking instances';
$string['checkingsections'] = 'Checking sections';
$string['checklanguage'] = 'Check language';
$string['checknone'] = 'Check none';
$string['childcoursenotfound'] = 'Child course not found!';
$string['childcourses'] = 'Child courses';
$string['choose'] = 'Choose';
$string['choosecourse'] = 'Choose a course';
$string['choosedots'] = 'Choose...';
$string['chooselivelogs'] = 'Or watch current activity';
$string['chooselogs'] = 'Choose which logs you want to see';
$string['choosereportfilter'] = 'Choose a filter for the report';
$string['choosetheme'] = 'Choose theme';
$string['chooseuser'] = 'Choose a user';
$string['icqnumber'] = 'ICQ number';
$string['icon'] = 'Icon';
$string['idnumber'] = 'ID number';
$string['idnumbercourse'] = 'Course ID number';
$string['idnumbercourse_help'] = 'The ID number of a course is only used when matching the course against external systems and is not displayed anywhere on the site. If the course has an official code name it may be entered, otherwise the field can be left blank.';
$string['idnumbermod'] = 'ID number';
$string['idnumbermod_help'] = 'Setting an ID number provides a way of identifying the activity for grade calculation purposes. If the activity is not included in any grade calculation then the ID number field may be left blank.

The ID number can also be set in the gradebook, though it can only be edited on the activity settings page.';
$string['idnumbertaken'] = 'This ID number is already taken';
$string['imagealt'] = 'Picture description';
$string['import'] = 'Import';
$string['importdata'] = 'Import course data';
$string['importdataexported'] = 'Exported data from \'from\' course successfully.<br /> Continue to import into your \'to\' course.';
$string['importdatafinished'] = 'Import complete! Continue to your course';
$string['importdatafrom'] = 'Find a course to import data from:';
$string['inactive'] = 'Inactive';
$string['include'] = 'Include';
$string['includeallusers'] = 'Include all users';
$string['includecoursefiles'] = 'Include course files';
$string['includecourseusers'] = 'Include course users';
$string['included'] = 'Included';
$string['includelogentries'] = 'Include log entries';
$string['includemodules'] = 'Include modules';
$string['includemoduleuserdata'] = 'Include module user data';
$string['includeneededusers'] = 'Include needed users';
$string['includenoneusers'] = 'Include no users';
$string['includeroleassignments'] = 'Include role assignments';
$string['includesitefiles'] = 'Include site files used in this course';
$string['includeuserfiles'] = 'Include user files';
$string['info'] = 'Information';
$string['institution'] = 'Institution';
$string['instudentview'] = 'in student view';
$string['interests'] = 'Interests';
$string['interestslist'] = 'List of interests';
$string['interestslist_help'] = 'Enter your interests separated by commas. Your interests will be displayed on your profile page as tags.';
$string['invalidemail'] = 'Invalid email address';
$string['invalidlogin'] = 'Invalid login, please try again';
$string['invalidusername'] = 'The username can only contain alphanumeric lowercase characters, underscore (_), hyphen (-), period (.) or at symbol (@)';
$string['invalidusernameupload'] = 'Invalid username';
$string['ip_address'] = 'IP address';
$string['jump'] = 'Jump';
$string['jumpto'] = 'Jump to...';
$string['keep'] = 'Keep';
$string['keepsearching'] = 'Keep searching';
$string['langltr'] = 'Language direction left-to-right';
$string['langrtl'] = 'Language direction right-to-left';
$string['language'] = 'Language';
$string['languagegood'] = 'This language pack is up-to-date! :-)';
$string['lastaccess'] = 'Last access';
$string['lastedited'] = 'Last edited';
$string['lastlogin'] = 'Last login';
$string['lastmodified'] = 'Last modified';
$string['lastname'] = 'Last name';
$string['lastyear'] = 'Last year';
$string['latestlanguagepack'] = 'Check for latest language pack on moodle.org';
$string['layouttable'] = 'Layout table';
$string['leavetokeep'] = 'Leave blank to keep current password';
$string['legacythemeinuse'] = 'This site is being displayed to you in compatibility mode because your browser is too old.';
$string['license'] = 'Licence';
$string['licenses'] = 'Licences';
$string['liketologin'] = 'Would you like to log in now with a full user account?';
$string['list'] = 'List';
$string['listfiles'] = 'List of files in {$a}';
$string['listofallpeople'] = 'List of all people';
$string['livelogs'] = 'Live logs from the past hour';
$string['local'] = 'Local';
$string['localplugindeleteconfirm'] = 'You are about to completely delete the local plugin \'{$a}\'. This will completely delete everything in the database associated with this plugin. Are you SURE you want to continue?';
$string['localplugins'] = 'Local plugins';
$string['localpluginsmanage'] = 'Manage local plugins';
$string['location'] = 'Location';
$string['log_excel_date_format'] = 'yyyy mmmm d h:mm';
$string['loggedinas'] = 'You are logged in as {$a}';
$string['loggedinasguest'] = 'You are currently using guest access';
$string['loggedinnot'] = 'You are not logged in.';
$string['login'] = 'Login';
$string['loginalready'] = 'You are already logged in';
$string['loginas'] = 'Login as';
$string['loginaspasswordexplain'] = '<p>You must enter the special "loginas password" to use this feature.<br />If you do not know it, ask your server administrator.</p>';
$string['login_failure_logs'] = 'Login failure logs';
$string['loginguest'] = 'Login as a guest';
$string['loginsite'] = 'Login to the site';
$string['loginsteps'] = 'Hi! For full access to courses you\'ll need to take
   a minute to create a new account for yourself on this web site.
   Each of the individual courses may also have a one-time
   "enrolment key", which you won\'t need until later. Here are
   the steps:
   <ol>
   <li>Fill out the <a href="{$a}">New Account</a> form with your details.</li>
   <li>An email will be immediately sent to your email address.</li>
   <li>Read your email, and click on the web link it contains.</li>
   <li>Your account will be confirmed and you will be logged in.</li>
   <li>Now, select the course you want to participate in.</li>
   <li>If you are prompted for an "enrolment key" - use the one
   that your teacher has given you. This will "enrol" you in the
   course.</li>
   <li>You can now access the full course. From now on you will only need
   to enter your personal username and password (in the form on this page)
   to log in and access any course you have enrolled in.</li>
   </ol>';
$string['loginstepsnone'] = '<p>Hi!</p>
<p>For full access to courses you\'ll need to create yourself an account.</p>
<p>All you need to do is make up a username and password and use it in the form on this page!</p>
<p>If someone else has already chosen your username then you\'ll have to try again using a different username.</p>';
$string['loginto'] = 'Login to {$a}';
$string['loginusing'] = 'Login here using your username and password';
$string['logout'] = 'Logout';
$string['logoutconfirm'] = 'Do you really want to logout?';
$string['logs'] = 'Logs';
$string['logtoomanycourses'] = '[ <a href="{$a->url}">more</a> ]';
$string['logtoomanyusers'] = '[ <a href="{$a->url}">more</a> ]';
$string['lookback'] = 'Look back';
$string['mailto'] = 'Email to';
$string['mailadmins'] = 'Inform admins';
$string['mailstudents'] = 'Inform students';
$string['mailteachers'] = 'Inform teachers';
$string['makeafolder'] = 'Create folder';
$string['makeeditable'] = 'If you make \'{$a}\' editable by the web server process (eg apache) then you could edit this file directly from this page';
$string['makethismyhome'] = 'Make this my default home page';
$string['manageblocks'] = 'Blocks';
$string['managecourses'] = 'Manage courses';
$string['managedatabase'] = 'Database';
$string['manageeditorfiles'] = 'Manage files used by editor';
$string['managefilters'] = 'Filters';
$string['managemodules'] = 'Modules';
$string['manageroles'] = 'Roles and permissions';
$string['markedthistopic'] = 'This topic is highlighted as the current topic';
$string['markthistopic'] = 'Highlight this topic as the current topic';
$string['matchingsearchandrole'] = 'Matching \'{$a->search}\' and {$a->role}';
$string['maximumgrade'] = 'Maximum grade';
$string['maximumgradex'] = 'Maximum grade: {$a}';
$string['maximumchars'] = 'Maximum of {$a} characters';
$string['maximumshort'] = 'Max';
$string['maximumupload'] = 'Maximum upload size';
$string['maximumupload_help'] = 'This setting determines the largest size of file that can be uploaded to the course, limited by the site-wide setting set by an administrator. Activity modules also include a maximum upload size setting for further restricting the file size.';
$string['maxnumberweeks'] = 'Maximum for number of weeks/topics';
$string['maxnumberweeks_desc'] = 'This controls the maximum options that appears in the "Number of weeks/topics" setting for courses.';
$string['maxsize'] = 'Max size: {$a}';
$string['maxfilesize'] = 'Maximum size for new files: {$a}';
$string['maxnumcoursesincombo'] = 'Browse <a href="{$a->link}">{$a->numberofcourses} courses</a>.';
$string['memberincourse'] = 'People in the course';
$string['messagebody'] = 'Message body';
$string['messagedselectedusers'] = 'Selected users have been messaged and the recipient list has been reset.';
$string['messagedselectedusersfailed'] = 'Something went wrong while messaging selected users.  Some may have received the email.';
$string['messageprovider:backup'] = 'Backup notifications';
$string['messageprovider:courserequestapproved'] = 'Course creation request approval notification';
$string['messageprovider:courserequested'] = 'Course creation request notification';
$string['messageprovider:courserequestrejected'] = 'Course creation request rejection notification';
$string['messageprovider:errors'] = 'Important errors with the site';
$string['messageprovider:errors_help'] = 'These are important errors that an administrator should know about.';
$string['messageprovider:notices'] = 'Notices about minor problems';
$string['messageprovider:notices_help'] = 'These are notices that an administrator might be interested in seeing.';
$string['messageprovider:instantmessage'] = 'Personal messages between users';
$string['messageprovider:instantmessage_help'] = 'This section configures what happens to messages that are sent to you directly from other users on this site.';
$string['messageselect'] = 'Select this user as a message recipient';
$string['messageselectadd'] = 'Send a message';
$string['migratinggrades'] = 'Migrating grades';
$string['min'] = 'min';
$string['mins'] = 'mins';
$string['minutes'] = 'minutes';
$string['miscellaneous'] = 'Miscellaneous';
$string['missingcategory'] = 'You need to choose a category';
$string['missingcity'] = 'Missing city/town';
$string['missingcountry'] = 'Missing country';
$string['missingdescription'] = 'Missing description';
$string['missingemail'] = 'Missing email address';
$string['missingfirstname'] = 'Missing given name';
$string['missingfromdisk'] = 'Missing from disk';
$string['missingfullname'] = 'Missing full name';
$string['missinglastname'] = 'Missing surname';
$string['missingname'] = 'Missing name';
$string['missingnewpassword'] = 'Missing new password';
$string['missingpassword'] = 'Missing password';
$string['missingrecaptchachallengefield'] = 'Missing reCAPTCHA challenge field';
$string['missingreqreason'] = 'Missing reason';
$string['missingshortname'] = 'Missing short name';
$string['missingshortsitename'] = 'Missing short site name';
$string['missingsitedescription'] = 'Missing site description';
$string['missingsitename'] = 'Missing site name';
$string['missingstrings'] = 'Check for untranslated words or phrases';
$string['missingstudent'] = 'Must choose something';
$string['missingsummary'] = 'Missing summary';
$string['missingteacher'] = 'Must choose something';
$string['missingurl'] = 'Missing URL';
$string['missingusername'] = 'Missing username';
$string['modified'] = 'Modified';
$string['moduledeleteconfirm'] = 'You are about to completely delete the module \'{$a}\'.  This will completely delete everything in the database associated with this activity module.  Are you SURE you want to continue?';
$string['moduledeletefiles'] = 'All data associated with the module \'{$a->module}\' has been deleted from the database.  To complete the deletion (and prevent the module re-installing itself), you should now delete this directory from your server: {$a->directory}';
$string['moduleintro'] = 'Description';
$string['modulesetup'] = 'Setting up module tables';
$string['modulesuccess'] = '{$a} tables have been set up correctly';
$string['moodledocs'] = 'Moodle Docs';
$string['moodledocslink'] = 'Moodle Docs for this page';
$string['moodleversion'] = 'Moodle version';
$string['moodlerelease'] = 'Moodle release';
$string['more'] = 'more';
$string['morehelp'] = 'More help';
//Quang changed for http://220.231.97.38:83/mantis/view.php?id=414
//$string['moreinformation'] = 'More information about this error';
$string['moreinformation'] = 'oops, sorry, please go back';
$string['moreprofileinfoneeded'] = 'Please tell us more about yourself';
$string['mostrecently'] = 'most recently';
$string['move'] = 'Move';
$string['movecategorycontentto'] = 'Move into';
$string['movecategoryto'] = 'Move category to:';
$string['movecontentstoanothercategory'] = 'Move contents to another category';
$string['movecourseto'] = 'Move course to:';
$string['movedown'] = 'Move down';
$string['movefilestohere'] = 'Move files to here';
$string['movefull'] = 'Move {$a} to this location';
$string['movehere'] = 'Move to here';
$string['moveleft'] = 'Move left';
$string['moveright'] = 'Move right';
$string['movesection'] = 'Move section {$a}';
$string['moveselectedcoursesto'] = 'Move selected courses to...';
$string['movetoanotherfolder'] = 'Move to another folder';
$string['moveup'] = 'Move up';
$string['msnid'] = 'MSN ID';
$string['mustconfirm'] = 'You need to confirm your login';
$string['mustchangepassword'] = 'The new password must be different than the current one';
$string['mycourses'] = 'My courses';
$string['myfiles'] = 'My private files';
$string['myfilesmanage'] = 'Manage my private files';
$string['myhome'] = 'My home';
$string['mymoodledashboard'] = 'My Moodle dashboard';
$string['myprofile'] = 'My profile';
$string['name'] = 'Name';
$string['navigation'] = 'Navigation';
$string['needed'] = 'Needed';
$string['never'] = 'Never';
$string['neverdeletelogs'] = 'Never delete logs';
$string['new'] = 'New';
$string['newaccount'] = 'New account';
$string['newcourse'] = 'New course';
$string['newpassword'] = 'New password';
$string['newpassword_help'] = 'Enter a new password or leave blank to keep current password.';
$string['newpasswordfromlost'] = '<strong>NOTICE:</strong> Your <strong>Current password</strong> will have been sent to you in the <strong>second</strong> of the two emails sent as part of the lost password recovery process. Make sure you have received your replacement password before continuing with this screen.';
$string['newpasswordtext'] = 'Hi {$a->firstname},

Your account password at \'{$a->sitename}\' has been reset
and you have been issued with a new temporary password.

Your current login information is now:
   username: {$a->username}
   password: {$a->newpassword}

Please go to this page to change your password:
   {$a->link}

In most mail programs, this should appear as a blue link
which you can just click on.  If that doesn\'t work,
then cut and paste the address into the address
line at the top of your web browser window.

Cheers from the \'{$a->sitename}\' administrator,
{$a->signoff}';
$string['newpicture'] = 'New picture';
$string['newpicture_help'] = 'To add a new picture, browse and select an image (in JPG or PNG format) then click "Update profile". The image will be cropped to a square and resized to 100x100 pixels.';
$string['newsitem'] = 'news item';
$string['newsitems'] = 'news items';
$string['newsitemsnumber'] = 'News items to show';
$string['newsitemsnumber_help'] = 'This setting determines how many recent items appear in the latest news block on the course page. If set to "0 news items" then the latest news block will not be displayed.';
$string['newuser'] = 'New user';
$string['newusernewpasswordsubj'] = 'New user account';
$string['newusernewpasswordtext'] = 'Hi {$a->firstname},

A new account has been created for you at \'{$a->sitename}\'
and you have been issued with a new temporary password.

Your current login information is now:
   username: {$a->username}
   password: {$a->newpassword}
             (you will have to change your password
              when you login for the first time)

To start using \'{$a->sitename}\', login at
   {$a->link}

In most mail programs, this should appear as a blue link
which you can just click on.  If that doesn\'t work,
then cut and paste the address into the address
line at the top of your web browser window.

Cheers from the \'{$a->sitename}\' administrator,
{$a->signoff}';
$string['newusers'] = 'New users';
$string['newwindow'] = 'new window';
$string['next'] = 'Next';
$string['nextsection'] = 'Next section';
$string['no'] = 'No';
$string['noblockstoaddhere'] = 'There are no blocks that you can add to this page.';
$string['nobody'] = 'Nobody';
$string['nocourses'] = 'No courses';
$string['nocoursesfound'] = 'No courses were found with the words \'{$a}\'';
$string['nocoursesyet'] = 'No courses in this category';
$string['nocomments'] = 'No comments';
$string['nodstpresets'] = 'The administrator has not enabled Daylight Savings Time support.';
$string['nofilesselected'] = 'No files have been selected to restore';
$string['nofilesyet'] = 'No files have been uploaded to your course yet';
$string['nograde'] = 'No grade';
$string['nochange'] = 'No change';
$string['noimagesyet'] = 'No images have been uploaded to your course yet';
$string['nologsfound'] = 'No logs have been found';
$string['nomatchingusers'] = 'No users match \'{$a}\'';
$string['nomorecourses'] = 'No more matching courses could be found';
$string['nomoreidnumber'] = 'Not using an idnumber to avoid collisions';
$string['none'] = 'None';
$string['noneditingteacher'] = 'Non-editing teacher';
$string['noneditingteacherdescription'] = 'Non-editing teachers can teach in courses and grade students, but may not alter activities.';
$string['nonstandard'] = 'Non-standard';
$string['nopendingcourses'] = 'There are no courses pending approval';
$string['nopotentialadmins'] = 'No potential admins';
$string['nopotentialcreators'] = 'No potential course creators';
$string['nopotentialstudents'] = 'No potential students';
$string['nopotentialteachers'] = 'No potential teachers';
$string['norecentactivity'] = 'No recent activity';
$string['noreplybouncemessage'] = 'You have replied to a no-reply email address. If you were attempting to reply to a forum post, please instead reply using the {$a} forums.

Following is the content of your email:';
$string['noreplybouncesubject'] = '{$a} - bounced email.';
$string['noreplyname'] = 'Do not reply to this email';
$string['noresults'] = 'No results';
$string['normal'] = 'Normal';
$string['normalfilter'] = 'Normal search';
$string['nosite'] = 'Could not find site-level course';
$string['nostudentsfound'] = 'No {$a} found';
$string['nostudentsingroup'] = 'There are no students in this group yet';
$string['nostudentsyet'] = 'No students enrolled in this course yet';
$string['nosuchemail'] = 'No such email address';
$string['notavailable'] = 'Not available';
$string['noteachersyet'] = 'No teachers in this course yet';
$string['notenrolled'] = '{$a} is not enrolled in this course.';
$string['notenrolledprofile'] = 'This profile is not available because this user is not enrolled in this course.';
$string['noteusercannotrolldatesoncontext'] = '<strong>Note:</strong> The ability to roll dates when restoring this backup has been disabled because you lack the required permissions.';
$string['noteuserschangednonetocourse'] = '<strong>Note:</strong> Course users need to be restored when restoring user data (in activities, files or messages). This setting has been changed for you.';
$string['nothingnew'] = 'Nothing new since your last login';
$string['nothingtodisplay'] = 'Nothing to display';
$string['notice'] = 'Notice';
$string['noticenewerbackup'] = 'This backup file has been created with Moodle {$a->backuprelease} ({$a->backupversion}) and it\'s newer than your currently installed Moodle {$a->serverrelease} ({$a->serverversion}). This could cause some inconsistencies because backwards compatibility of backup files cannot be guaranteed.';
$string['notifications'] = 'Notifications';
$string['notifyloginfailuresmessage'] = '{$a->time}, IP: {$a->ip}, User: {$a->info}';
$string['notifyloginfailuresmessageend'] = 'You can view these logs at {$a}/course/report/log/index.php?id=1&amp;chooselog=1&amp;modid=site_errors.';
$string['notifyloginfailuresmessagestart'] = 'Here is a list of failed login attempts at {$a} since you were last notified';
$string['notifyloginfailuressubject'] = '{$a} :: Failed logins notification';
$string['notincluded'] = 'Not included';
$string['notingroup'] = 'Sorry, but you need to be part of a group to see this activity.';
$string['notpublic'] = 'Not public!';
$string['nousersfound'] = 'No users found';
$string['nousersmatching'] = 'No users matching \'{$a}\' were found';
$string['nousersyet'] = 'There are no users yet';
$string['novalidcourses'] = 'No valid courses to be shown';
$string['now'] = 'now';
$string['numattempts'] = '{$a} failed login attempt(s)';
$string['numberofcourses'] = 'Number of courses';
$string['numberweeks'] = 'Number of weeks/topics';
$string['numdays'] = '{$a} days';
$string['numhours'] = '{$a} hours';
$string['numletters'] = '{$a} letters';
$string['numminutes'] = '{$a} minutes';
$string['nummonths'] = '{$a} months';
$string['numseconds'] = '{$a} seconds';
$string['numviews'] = '{$a} views';
$string['numweeks'] = '{$a} weeks';
$string['numwords'] = '{$a} words';
$string['numyears'] = '{$a} years';
$string['ok'] = 'OK';
$string['oldpassword'] = 'Current password';
$string['olduserdirectory'] = 'This is the OLD users directory, and is no longer needed. You may safely delete it. The files it contains have been copied to the NEW user directory.';
$string['opentoguests'] = 'Guest access';
$string['optional'] = 'optional';
$string['order'] = 'Order';
$string['orphanedactivities'] = 'Orphaned activities';
$string['other'] = 'Other';
$string['outline'] = 'Outline';
$string['outlinereport'] = 'Outline report';
$string['page'] = 'Page';
$string['pageheaderconfigablock'] = 'Configuring a block in %fullname%';
$string['pagepath'] = 'Page path';
$string['pageshouldredirect'] = 'This page should automatically redirect. If nothing is happening please use the continue link below.';
$string['parentcategory'] = 'Parent category';
$string['parentcoursenotfound'] = 'Parent course not found!';
$string['parentfolder'] = 'Parent folder';
$string['participants'] = 'Participants';
$string['participantslist'] = 'Participants list';
$string['participationratio'] = 'Participation ratio';
$string['participationreport'] = 'Participation report';
$string['password'] = 'Password:';
$string['passwordconfirmchange'] = 'Confirm password:';
$string['passwordsame'] = 'Password and confirm password must be the same';
$string['passwordconfirmation'] = 'The password and confirmation password do not match';
$string['passwordextlink'] = 'The following link has been provided to recover your lost password. This will take you out of Moodle.';
$string['passwordforgotten'] = 'Forgotten password';
$string['passwordforgotteninstructions'] = 'Your details must first be found in the user database. Please enter <strong>either</strong> your username or your registered email address in the appropriate box. There is no need to enter both.';
$string['passwordforgotteninstructions2'] = 'To reset your password, submit your username or your email address below. If we can find you in the database, an email will be sent to your email address, with instructions how to get access again.';
$string['passwordchanged'] = 'Password has been changed';
$string['passwordnohelp'] = 'No help is available to find your lost password. Please contact your Moodle administrator.';
$string['passwordrecovery'] = 'Yes, help me log in';
$string['passwordsdiffer'] = 'These passwords do not match';
$string['passwordsent'] = 'Password has been sent';
$string['passwordsave'] = 'Save password';
$string['passwordsenttext'] = '<p>An email has been sent to your address at {$a->email}.</p>
   <p><b>Please check your email for your new password</b></p>
   <p>The new password was automatically generated, so you might like to
   <a href="{$a->link}">change it to something easier to remember</a>.</p>';
$string['path'] = 'Path';
$string['pathnotexists'] = 'Path doesn\'t exist in your server!';
$string['pathslasherror'] = 'Path can\'t end with a slash!!';
$string['paymentinstant'] = 'Use the button below to pay and be enrolled within minutes!';
$string['paymentpending'] = '(<small><b><u>{$a}</u></b> pending</small>)';
$string['paymentrequired'] = 'This course requires a payment for entry.';
$string['payments'] = 'Payments';
$string['paymentsorry'] = 'Thank you for your payment!  Unfortunately your payment has not yet been fully processed, and you are not yet registered to enter the course "{$a->fullname}".  Please try continuing to the course in a few seconds, but if you continue to have trouble then please alert the {$a->teacher} or the site administrator';
$string['paymentthanks'] = 'Thank you for your payment!  You are now enrolled in your course:<br />"{$a}"';
$string['pendingrequests'] = 'Pending requests';
$string['periodending'] = 'Period ending ({$a})';
$string['personal'] = 'Personal';
$string['personalprofile'] = 'Personal profile';
$string['phone'] = 'Phone';
$string['phone2'] = 'Mobile phone';
$string['phpinfo'] = 'PHP info';
$string['pictureof'] = 'Picture of {$a}';
$string['pictureofuser'] = 'User picture';
$string['pleaseclose'] = 'Please close this window now.';
$string['pleasesearchmore'] = 'Please search some more';
$string['pleaseusesearch'] = 'Please use the search';
$string['plugin'] = 'Plugin';
$string['plugindeletefiles'] = 'All data associated with the plugin \'{$a->name}\' has been deleted from the database. To prevent the plugin re-installing itself, you should now delete this directory from your server: {$a->directory}';
$string['plugincheck'] = 'Plugins check';
$string['pluginsetup'] = 'Setting up plugin tables';
$string['policyaccept'] = 'I understand and agree';
$string['policyagree'] = 'You must agree to this policy to continue using this site.  Do you agree?';
$string['policyagreement'] = 'Site policy agreement';
$string['policyagreementclick'] = 'Link to site policy agreement';
$string['popup'] = 'popup';
$string['popupwindow'] = 'Open file in new window';
$string['popupwindowname'] = 'Popup window';
$string['post'] = 'Post';
$string['posts'] = 'Posts';
$string['potentialadmins'] = 'Potential admins';
$string['potentialcreators'] = 'Potential course creators';
$string['potentialstudents'] = 'Potential students';
$string['potentialteachers'] = 'Potential teachers';
$string['preferences'] = 'Preferences';
$string['preferredlanguage'] = 'Preferred language';
$string['preferredtheme'] = 'Preferred theme';
$string['preprocessingbackupfile'] = 'Preprocessing backup file';
$string['preview'] = 'Preview';
$string['previewhtml'] = 'HTML format preview';
$string['previeworchoose'] = 'Preview or choose a theme';
$string['previous'] = 'Previous';
$string['previouslyselectedusers'] = 'Previously selected users not matching \'{$a}\'';
$string['previoussection'] = 'Previous section';
$string['primaryadminsetup'] = 'Setup administrator account';
$string['profile'] = 'Profile';
$string['profilenotshown'] = 'This profile description will not be shown until this person is enrolled in at least one course.';
$string['publicprofile'] = 'Public profile';
$string['publicsitefileswarning'] = 'Note: files placed here can be accessed by anyone';
$string['publicsitefileswarning2'] = 'Note: Files placed here can be accessed by anyone who knows (or can guess) the URL. For security reasons, it is recommended that any backup files are deleted immediately after restoring them.';
$string['publicsitefileswarning3'] = 'Note: Files placed here can be accessed by anyone who knows (or can guess) the URL. <br />For security reasons, backup files should be saved in the secure backupdata folder only.';
$string['publish'] = 'Publish';
$string['question'] = 'Question';
$string['questionsinthequestionbank'] = 'Questions in the question bank';
$string['readinginfofrombackup'] = 'Reading info from backup';
$string['readme'] = 'README';
$string['recentactivity'] = 'Recent activity';
$string['recentactivityreport'] = 'Full report of recent activity...';
$string['recipientslist'] = 'Recipients list';
$string['recreatedcategory'] = 'Recreated category {$a}';
$string['redirect'] = 'Redirect';
$string['refresh'] = 'Refresh';
$string['refreshingevents'] = 'Refreshing events';
$string['registration'] = 'Moodle registration';
$string['registrationcontact'] = 'Contact from the public';
$string['registrationcontactno'] = 'No, I do not want a contact form in the site listing';
$string['registrationcontactyes'] = 'Yes, provide a form for prospective Moodlers to contact me';
$string['registrationemail'] = 'Email notifications';
$string['registrationinfo'] = '<p>This page allows you to register your Moodle site with moodle.org.  Registration is free.
The main benefit of registering is that you will be added to a low-volume mailing list
for important notifications such as security alerts and new releases of Moodle.</p>
<p>By default, your information will be kept private, and will never be sold or passed on to anyone else.  The only
   reason for collecting this information is for support purposes, and to help build up a statistical
   picture of the Moodle community as a whole.</p>
<p>If you choose, you can allow your site name, country and URL to be added to the public list of Moodle Sites.</p>
<p>All new registrations are verified manually before they are added to the list, but once you are added you can update your registration (and your entry on the public list) at any time by resubmitting this form.</p>';
$string['registrationinfotitle'] = 'Registration information';
$string['registrationno'] = 'No, I do not want to receive email';
$string['registrationsend'] = 'Send registration information to moodle.org';
$string['registrationyes'] = 'Yes, please notify me about important issues';
$string['reject'] = 'Reject';
$string['rejectdots'] = 'Reject...';
$string['reload'] = 'Reload';
$string['remoteappuser'] = 'Remote {$a} User';
$string['remove'] = 'Remove';
$string['removeadmin'] = 'Remove admin';
$string['removecreator'] = 'Remove course creator';
$string['removestudent'] = 'Remove student';
$string['removeteacher'] = 'Remove teacher';
$string['rename'] = 'Rename';
$string['renamefileto'] = 'Rename <b>{$a}</b> to';
$string['report'] = 'Report';
$string['reports'] = 'Reports';
$string['repositories'] = 'Repositories';
$string['requestcourse'] = 'Request a course';
$string['requestedby'] = 'Requested by';
$string['requestedcourses'] = 'Requested courses';
$string['requestreason'] = 'Reason for course request';
$string['required'] = 'Required';
$string['requirespayment'] = 'This course requires payment for access';
$string['reset'] = 'Reset';
$string['resetcomponent'] = 'Component';
$string['resetcourse'] = 'Reset course';
$string['resetinfo'] = 'This page allows you to empty a course of user data, while retaining the activities and other settings.  Please be warned that by choosing items below and submitting this page you will delete your chosen user data from this course forever!';
$string['resetnotimplemented'] = 'Reset not implemented';
$string['resetstartdate'] = 'Reset start date';
$string['resetstatus'] = 'Status';
$string['resettask'] = 'Task';
$string['resettodefaults'] = 'Reset to defaults';
$string['resortcoursesbyname'] = 'Re-sort courses by name';
$string['resource'] = 'Resource';
$string['resourcedisplayauto'] = 'Automatic';
$string['resourcedisplaydownload'] = 'Force download';
$string['resourcedisplayembed'] = 'Embed';
$string['resourcedisplayframe'] = 'In frame';
$string['resourcedisplaynew'] = 'New window';
$string['resourcedisplayopen'] = 'Open';
$string['resourcedisplaypopup'] = 'In pop-up';
$string['resources'] = 'Resources';
$string['resources_help'] = 'Resource types enable almost any kind of web content to be inserted into the course.';
$string['restore'] = 'Restore';
$string['restorecancelled'] = 'Restore cancelled';
$string['restorecannotassignroles'] = 'Restore needs to assign roles and you do not have permission to do so';
$string['restorecannotcreateorassignroles'] = 'Restore needs to create or assign roles and you do not have permission to do so';
$string['restorecannotcreateuser'] = 'Restore needs to create user \'{$a}\' from backup file and you do not have permission to do so';
$string['restorecannotoverrideperms'] = 'Restore needs to override permissions and you do not have permission to do so';
$string['restorecoursenow'] = 'Restore this course now!';
$string['restoredaccount'] = 'Restored account';
$string['restoredaccountinfo'] = 'This account was imported from another server and the password has been lost. To set a new password by email, please click "Continue"';
$string['restorefinished'] = 'Restore completed successfully';
$string['restoreto'] = 'Restore to';
$string['restoretositeadding'] = 'Warning: You are about to restore to the site front page, adding data to it!';
$string['restoretositedeleting'] = 'Warning: You are about to restore to the site front page, deleting data from it first!';
$string['restoreuserconflict'] = 'Trying to restore user \'{$a}\' from backup file will cause conflict';
$string['restoreuserinfofailed'] = 'The restore process has stopped because you don\'t have permission to restore user data.';
$string['restoreusersprecheck'] = 'Checking user data';
$string['restoreusersprecheckerror'] = 'Some problems were detected when checking user data';
$string['restricted'] = 'Restricted';
$string['restrictmodules'] = 'Restrict activity modules?';
$string['returningtosite'] = 'Returning to this web site?';
$string['returntooriginaluser'] = 'Return to {$a}';
$string['revert'] = 'Revert';
$string['role'] = 'Role';
$string['rolemappings'] = 'Role mappings';
$string['rolerenaming'] = 'Role renaming';
$string['rolerenaming_help'] = 'This setting allows the displayed names for roles used in the course to be changed. Only the displayed name is changed - role permissions are not affected.  New role names will appear on the course participants page and elsewhere within the course. If the renamed role is one that the administrator has selected as a course manager role, then the new role name will also appear as part of the course listings.';
$string['roles'] = 'Roles';
$string['rss'] = 'RSS';
$string['rssarticles'] = 'Number of RSS recent articles';
$string['rsserror'] = 'Error reading RSS data';
$string['rsserrorauth'] = 'Your RSS link does not contain a valid authentication token.';
$string['rsserrorguest'] = 'This feed uses guest access to access the data, but guest does not have permission to read the data. Visit the original location that this feed comes from (URL) as a valid user and get a new RSS link from there.';
$string['rsstype'] = 'RSS feed for this activity';
$string['saveandnext'] = 'Save and show next';
$string['savedat'] = 'Saved at:';
$string['savechanges'] = 'Save changes';
$string['savechangesanddisplay'] = 'Save and display';
$string['savechangesandreturntocourse'] = 'Save and return to course';
//$string['savechangesandreturntocourse'] = 'Save';//Quang changed
$string['savecomment'] = 'Save comment';
$string['savepreferences'] = 'Save preferences';
$string['saveto'] = 'Save to';
$string['scale'] = 'Scale';
$string['scale_help'] = 'A scale provides a way of evaluating or grading performance in an activity. It is defined by an ordered list of values, ranging from negative to positive, separated by commas, for example "Disappointing, Not good enough, Average, Good, Very good, Excellent!"';
$string['scale_link'] = 'grade/scale';
$string['scales'] = 'Scales';
$string['scalescustom'] = 'Custom scales';
$string['scalescustomcreate'] = 'Add a new scale';
$string['scalescustomno'] = 'No custom scales have been created yet';
$string['scalesstandard'] = 'Standard scales';
$string['scalestandard'] = 'Standard scale';
$string['scalestandard_help'] = 'A standard scale is available site-wide, for all courses.';
$string['scalestandard_link'] = 'grade/scale';
$string['scalestip'] = 'To create custom scales, use the \'Scales...\' link in your course administration menu.';
$string['scalestip2'] = 'To create custom scales, click the Grades link in the course administration menu, then choose Edit, Scales.';
$string['screenreaderno'] = 'No';
$string['screenreaderuse'] = 'Screen reader';
$string['screenreaderyes'] = 'Yes';
$string['screenreaderuse_help'] = 'If set to yes, a more accessible interface is provided in various places such as chat.';
$string['screenshot'] = 'Screenshot';
$string['search'] = 'Search'; // TODO rename to searchforums and move to mod_forum
$string['search_help'] = 'For basic searching of one or more words anywhere in the text, just type them separated by spaces. All words longer than two characters are used.

For advanced searching, press the search button without typing anything in the search box to access the advanced search form.';
$string['searchagain'] = 'Search again';
$string['searchbyemail'] = 'Search by email address';
$string['searchbyusername'] = 'Search by username';
$string['searchcourses'] = 'Search courses';
$string['searchhelp'] = 'You can search for multiple words at once.<br /><br />word : find any match of this word within the text.<br />+word : only exact matching words will be found.<br />-word : don\'t include results containing this word.';
$string['searchoptions'] = 'Search options';
$string['searchresults'] = 'Search results';
$string['sec'] = 'sec';
$string['seconds'] = 'seconds';
$string['secondstotime172800'] = '2 days';
$string['secondstotime259200'] = '3 days';
$string['secondstotime345600'] = '4 days';
$string['secondstotime432000'] = '5 days';
$string['secondstotime518400'] = '6 days';
$string['secondstotime604800'] = '1 Week';
$string['secondstotime86400'] = '1 day';
$string['secretalreadyused'] = 'Change password confirmation link was already used, password was not changed.';
$string['secs'] = 'secs';
$string['section'] = 'Section';
$string['sectionname'] = 'Section name';
$string['sections'] = 'Sections';
$string['sectionusedefaultname'] = 'Use default section name';
$string['seealsostats'] = 'See also: stats';
$string['select'] = 'Select';
$string['selectacountry'] = 'Select a country';
$string['selectaregion'] = 'Select a region';
$string['selectall'] = 'Select all';
$string['selectamodule'] = 'Please select an activity module';
$string['selectdefault'] = 'Select default';
$string['selectedfile'] = 'Selected file';
$string['selectednowmove'] = '{$a} files selected for moving. Now go into the destination folder and press \'Move files to here\'';
$string['selectfiles'] = 'Select files';
$string['selectnos'] = 'Select all \'no\'';
$string['selectperiod'] = 'Select period';
$string['sendmail'] = 'Send email';
$string['senddetails'] = 'Send my details via email';
$string['separate'] = 'Separate';
$string['separateandconnected'] = 'Separate and Connected ways of knowing';
$string['separateandconnectedinfo'] = 'The scale based on the theory of separate and connected knowing. This theory describes two different ways that we can evaluate and learn about the things we see and hear.<ul><li><strong>Separate knowers</strong> remain as objective as possible without including feelings and emotions. In a discussion with other people, they like to defend their own ideas, using logic to find holes in opponent\'s ideas.</li><li><strong>Connected knowers</strong> are more sensitive to other people. They are skilled at empathy and tends to listen and ask questions until they feel they can connect and "understand things from their point of view". They learn by trying to share the experiences that led to the knowledge they find in other people.</li></ul>';
$string['serverlocaltime'] = 'Server\'s local time';
$string['setcategorytheme'] = 'Set category theme';
$string['settings'] = 'Settings';
$string['shortname'] = 'Short name'; // @deprecated - use shortnamecourse or shortnameuser or some own context specific string
$string['shortnamecollisionwarning'] = '[*] = This shortname is already in use by a course and will need to be changed upon approval';
$string['shortnamecourse'] = 'Course short name';
$string['shortnamecourse_help'] = 'The short name of the course is displayed in the navigation and is used in the subject line of course email messages.';
$string['shortnametaken'] = 'Short name is already used for another course ({$a})';
$string['shortnameuser'] = 'User short name';
$string['shortsitename'] = 'Short name for site (eg single word)';
$string['show'] = 'Show';
$string['showactions'] = 'Show actions';
$string['showadvancedsettings'] = 'Show advanced settings';
$string['showall'] = 'Show all {$a}';
$string['showallcourses'] = 'Show all courses';
$string['showalltopics'] = 'Show all topics';
$string['showallusers'] = 'Show all users';
$string['showallweeks'] = 'Show all weeks';
$string['showblockcourse'] = 'Show list of courses containing block';
$string['showcomments'] = 'Show/hide comments';
$string['showcommentsnonjs'] = 'Show comments';
$string['showgrades'] = 'Show gradebook to students';
$string['showgrades_help'] = 'Many activities in the course allow grades to be set. This setting determines whether a student can view a list of all their grades for the course via a grades link in the course administration block.';
$string['showlistofcourses'] = 'Show list of courses';
$string['showmodulecourse'] = 'Show list of courses containing activity';
$string['showonly'] = 'Show only';
$string['showonlytopic'] = 'Show only topic {$a}';
$string['showonlyweek'] = 'Show only week {$a}';
$string['showperpage'] = 'Show {$a} per page';
$string['showrecent'] = 'Show recent activity';
$string['showreports'] = 'Show activity reports';
$string['showreports_help'] = 'Activity reports are available for each participant that show their activity in the course. As well as listings of their contributions, such as forum posts or assignment submissions, these reports also include access logs. This setting determines whether a student can view their own activity reports via their profile page.';
$string['showsettings'] = 'Show settings';
$string['showtheselogs'] = 'Show these logs';
$string['showthishelpinlanguage'] = 'Show this help in language: {$a}';
$string['showtopicfromothers'] = 'Show topic';
$string['showweekfromothers'] = 'Show week';
$string['schedule'] = 'Schedule';
$string['since'] = 'Since';
$string['sincelast'] = 'since last login';
$string['site'] = 'Site';
$string['sitedefault'] = 'Site default';
$string['siteerrors'] = 'Site errors';
$string['sitefiles'] = 'Site files';
$string['sitefilesused'] = 'Site files used in this course';
$string['sitehome'] = 'Site home';
$string['sitelegacyfiles'] = 'Legacy site files';
$string['sitelogs'] = 'Site logs';
$string['sitenews'] = 'Site news';
$string['sitepages'] = 'Site pages';
$string['sitepartlist'] = 'You do not have the required permissions to view the participants list';
$string['sitepartlist0'] = 'You must be a site teacher to be allowed to see the site participants list';
$string['sitepartlist1'] = 'You must be a teacher to be allowed to see the site participants list';
$string['sites'] = 'Sites';
$string['sitesection'] = 'Include a topic section';
$string['sitesettings'] = 'Site settings';
$string['siteteachers'] = 'Site teachers';
$string['size'] = 'Size';
$string['sizeb'] = 'bytes';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'KB';
$string['sizemb'] = 'MB';
$string['skipped'] = 'Skipped';
$string['skypeid'] = 'Skype ID';
$string['twitter'] = 'Twitter ID';
$string['phonework'] = 'Work Ph';
$string['phonemobile'] = 'Mobile Ph';
$string['company'] = 'Company';
$string['title'] = 'Title';
$string['website'] = 'Website';
$string['zip'] = 'Zip';
$string['PersonAccessLevelID'] = 'Access Level';
$string['socialheadline'] = 'Social forum - latest topics';
$string['someallowguest'] = 'Some courses may allow guest access';
$string['someerrorswerefound'] = 'Some information was missing or incorrect. Look below for details.';
$string['sortby'] = 'Sort by';
$string['sortbyx'] = 'Sort by {$a} ascending';
$string['sortbyxreverse'] = 'Sort by {$a} descending';
$string['sourcerole'] = 'Source role';
$string['specifyname'] = 'You must specify a name.';
$string['standard'] = 'Standard';
$string['starpending'] = '([*] = course pending approval)';
$string['startdate'] = 'Course start date';
$string['startdate_help'] = 'This setting determines the start of the first week for a course in weekly format. It also determines the earliest date that logs of course activities are available for.';
$string['startingfrom'] = 'Starting from';
$string['startsignup'] = 'Create new account';
$string['state'] = 'State/Province';
$string['statistics'] = 'Statistics';
$string['statisticsgraph'] = 'Statistics graph';
$string['stats'] = 'Statistics';
$string['statslogins'] = 'Logins';
$string['statsmodedetailed'] = 'Detailed (user) view';
$string['statsmodegeneral'] = 'General view';
$string['statsnodata'] = 'There is no available data for that combination of course and time period.';
$string['statsnodatauser'] = 'There is no available data for that combination of course, user and time period.';
$string['statsoff'] = 'Statistics is not currently enabled';
$string['statsreads'] = 'Views';
$string['statsreportactivity'] = 'All activity (all roles)';
$string['statsreportactivitybyrole'] = 'All activity (views and posts)';
$string['statsreportforuser'] = 'for';
$string['statsreportlogins'] = 'Logins';
$string['statsreportreads'] = 'Views (all roles)';
$string['statsreporttype'] = 'Report type';
$string['statsreportwrites'] = 'Posts (all roles)';
$string['statsreport1'] = 'Logins';
$string['statsreport10'] = 'User activity';
$string['statsreport11'] = 'Most active courses';
$string['statsreport12'] = 'Most active courses (weighted)';
$string['statsreport13'] = 'Most participatory courses (enrolments)';
$string['statsreport14'] = 'Most participatory courses (views/posts)';
$string['statsreport2'] = 'Views (all roles)';
$string['statsreport3'] = 'Posts (all roles)';
$string['statsreport4'] = 'All activity (all roles)';
$string['statsreport5'] = 'All activity (views and posts)';
$string['statsreport7'] = 'User activity (views and posts)';
$string['statsreport8'] = 'All user activity';
$string['statsreport9'] = 'Logins (site course)';
$string['statsstudentactivity'] = 'Student activity';
$string['statsstudentreads'] = 'Student views';
$string['statsstudentwrites'] = 'Student posts';
$string['statsteacheractivity'] = 'Teacher activity';
$string['statsteacherreads'] = 'Teacher views';
$string['statsteacherwrites'] = 'Teacher posts';
$string['statstimeperiod'] = 'Time period - last:';
$string['statsuniquelogins'] = 'Unique logins';
$string['statsuseractivity'] = 'All activity';
$string['statsuserlogins'] = 'Logins';
$string['statsuserreads'] = 'Views';
$string['statsuserwrites'] = 'Posts';
$string['statswrites'] = 'Posts';
$string['status'] = 'Status';
$string['stringsnotset'] = 'The following strings are not defined in {$a}';
$string['studentnotallowed'] = 'Sorry, but you can not enter this course as \'{$a}\'';
$string['students'] = 'Students';
$string['studentsandteachers'] = 'Students and teachers';
$string['subcategories'] = 'Sub-categories';
$string['submit'] = 'Submit';
$string['success'] = 'Success';
$string['summary'] = 'Summary';
$string['summary_help'] = 'The idea of a summary is a short text to prepare students for the activities within the topic or week. The text is shown on the course page under the section name.';
$string['summaryof'] = 'Summary of {$a}';
$string['supplyinfo'] = 'More details';
$string['switchdevicedefault'] = 'Switch to the standard theme';
$string['switchdevicerecommended'] = 'Switch to the recommended theme for your device';
$string['switchrolereturn'] = 'Return to my normal role';
$string['switchroleto'] = 'Switch role to...';
$string['tag'] = 'Tag';
$string['tagalready'] = 'This tag already exists';
$string['tagmanagement'] = 'Add/delete tags ...';
$string['tags'] = 'Tags';
$string['targetrole'] = 'Target role';
$string['teacheronly'] = 'for the {$a} only';
$string['teacherroles'] = '{$a} roles';
$string['teachers'] = 'Teachers';
$string['textediting'] = 'When editing text';
$string['texteditor'] = 'Use standard web forms';
$string['textformat'] = 'Plain text format';
$string['thanks'] = 'Thanks';
$string['theme'] = 'Theme';
$string['themes'] = 'Themes';
$string['themesaved'] = 'New theme saved';
$string['thereareno'] = 'There are no {$a} in this course';
$string['thiscategorycontains'] = 'This category contains';
$string['time'] = 'Time';
$string['timezone'] = 'Timezone';
$string['to'] = 'To';
$string['tocreatenewaccount'] = 'Skip to create new account';
$string['today'] = 'Today';
$string['todaylogs'] = 'Today\'s logs';
$string['toeveryone'] = 'to everyone';
$string['toomanybounces'] = 'That email address has had too many bounces. You <b>must</b> change it to continue.';
$string['toomanytoshow'] = 'There are too many users to show.';
$string['toomanyusersmatchsearch'] = 'Too many users ({$a->count}) match \'{$a->search}\'';
$string['toomanyuserstoshow'] = 'Too many users ({$a}) to show';
$string['toonly'] = 'to {$a} only';
$string['top'] = 'Top';
$string['topic'] = 'Topic';
$string['topicoutline'] = 'Topic outline';
$string['topicshow'] = 'Show this topic to {$a}';
$string['topichide'] = 'Hide this topic from {$a}';
$string['total'] = 'Total';
$string['trackforums'] = 'Forum tracking';
$string['trackforumsno'] = 'No: don\'t keep track of posts I have seen';
$string['trackforumsyes'] = 'Yes: highlight new posts for me';
$string['trysearching'] = 'Try searching instead.';
$string['turneditingoff'] = 'Turn editing off';
$string['turneditingon'] = 'Turn editing on';
$string['undecided'] = 'Undecided';
$string['unfinished'] = 'Unfinished';
$string['unknowncategory'] = 'Unknown category';
$string['unlimited'] = 'Unlimited';
$string['unpacking'] = 'Unpacking {$a}';
$string['unsafepassword'] = 'Unsafe password - try something else';
$string['untilcomplete'] = 'Until complete';
$string['unusedaccounts'] = 'Accounts unused for more than {$a} days are automatically unenrolled';
$string['unzip'] = 'Unzip';
$string['unzippingbackup'] = 'Unzipping backup';
$string['up'] = 'Up';
$string['update'] = 'Update';
$string['updated'] = 'Updated {$a}';
$string['updatemymoodleoff'] = 'Stop customising this page';
$string['updatemymoodleon'] = 'Customise this page';
$string['updatemyprofile'] = 'Update profile';
$string['updatesevery'] = 'Updates every {$a} seconds';
$string['updatethis'] = 'Update this {$a}';
$string['updatethiscourse'] = 'Update this course';
$string['updatinga'] = 'Updating: {$a}';
$string['updatingain'] = 'Updating {$a->what} in {$a->in}';
$string['upload'] = 'Upload';
$string['uploadafile'] = 'Upload a file';
$string['uploadcantwrite'] = 'Failed to write file to disk';
$string['uploadedfile'] = 'File uploaded successfully';
$string['uploadedfileto'] = 'Uploaded {$a->file} to {$a->directory}';
$string['uploadedfiletoobig'] = 'Sorry, but that file is too big (limit is {$a} bytes)';
$string['uploadextension'] = 'File upload stopped by extension';
$string['uploadfailednotrecovering'] = 'Your file upload has failed because there was a problem with one of the files, {$a->name}.<br /> Here is a log of the problems:<br />{$a->problem}<br />Not recovering.';
$string['uploadfilelog'] = 'Upload log for file {$a}';
$string['uploadformlimit'] = 'Uploaded file exceeded the maximum size limit set by the form';
$string['uploadlabel'] = 'Title:';
$string['uploadnewfile'] = 'Upload new file';
$string['uploadnofilefound'] = 'No file was found - are you sure you selected one to upload?';
$string['uploadnotallowed'] = 'Uploads are not allowed';
$string['uploadnotempdir'] = 'Missing a temporary folder';
$string['uploadoldfilesdeleted'] = 'The old file(s) in your upload area have been deleted';
$string['uploadpartialfile'] = 'File was only partially uploaded';
$string['uploadproblem'] = 'An unknown problem occurred while uploading the file \'{$a}\' (perhaps it was too large?)';
$string['uploadrenamedcollision'] = 'File was renamed from {$a->oldname} to {$a->newname} because there was a filename conflict.';
$string['uploadrenamedchars'] = 'File was renamed from {$a->oldname} to {$a->newname} because of invalid characters.';
$string['uploadserverlimit'] = 'Uploaded file exceeded the maximum size limit set by the server';
$string['uploadthisfile'] = 'Upload this file';
$string['url'] = 'URL';
$string['used'] = 'Used';
$string['usedinnplaces'] = 'Used in {$a} places';
$string['usemessageform'] = 'or use the form below to send a message to the selected students';
$string['user'] = 'User';
$string['userconfirmed'] = 'Confirmed {$a}';
$string['usercurrentsettings'] = 'My profile settings';
$string['userdata'] = 'User data';
$string['userdeleted'] = 'This user account has been deleted';
$string['userdescription'] = 'Description';
$string['userdescription_help'] = 'This box enables you to enter some text about yourself which will then be displayed on your profile page for others to view.';
$string['userdetails'] = 'User details';
$string['userfiles'] = 'User files';
$string['userlist'] = 'User list';
$string['username'] = 'Username';
$string['usernameemailmatch'] = 'The username and email address do not relate to the same user';
$string['usernameexists'] = 'This username already exists';
$string['usernamelowercase'] = 'Only lowercase letters allowed';
$string['usernamenotfound'] = 'The username was not found in the database';
$string['usernameoremail'] = 'Enter either username or email address';
$string['usernotconfirmed'] = 'Could not confirm {$a}';
$string['userpic'] = 'User picture';
$string['users'] = 'Users';
$string['userselectorautoselectunique'] = 'If only one user matches the search, select them automatically';
$string['userselectorpreserveselected'] = 'Keep selected users, even if they no longer match the search';
$string['userselectorsearchanywhere'] = 'Match the search text anywhere in the user\'s name';
$string['usersnew'] = 'New users';
$string['usersnoaccesssince'] = 'Inactive for more than';
$string['userswithfiles'] = 'Users with files';
$string['useruploadtype'] = 'User upload type: {$a}';
$string['userviewingsettings'] = 'Profile settings for {$a}';
$string['userzones'] = 'User zones';
$string['usetheme'] = 'Use theme';
$string['usingexistingcourse'] = 'Using existing course';
$string['valuealreadyused'] = 'This value has already been used.';
$string['version'] = 'Version';
$string['view'] = 'View';
$string['viewallcourses'] = 'View all courses';
$string['viewallcoursescategories'] = 'View all courses and categories';
$string['viewfileinpopup'] = 'View file in a popup window';
$string['viewprofile'] = 'View profile';
$string['views'] = 'Views';
$string['viewsolution'] = 'view solution';
$string['virusfound'] = 'Attention administrator! Clam AV has found a virus in a file uploaded by {$a->user} for the course {$a->course}. Here is the output of clamscan:';
$string['virusfoundlater'] = 'A file you uploaded on {$a->date} with the filename {$a->filename} for the course {$a->course} has since been found to contain a virus.  Here is a summary of what has happened to your file:

{$a->action}

If this was submitted work, you may want to resubmit it so that your tutor can see it.';
$string['virusfoundlateradmin'] = 'Attention administrator! A file that was uploaded on {$a->date} with the filename {$a->filename} for the course {$a->course} by the user {$a->user} has since been found to contain a virus.  Here is a summary of what has happened to the file:

{$a->action}

The user has also been notified.';
$string['virusfoundlateradminnolog'] = 'Attention administrator! A file that was uploaded with the filename {$a->filename} has since been found to contain a virus. Moodle was unable to resolve this file back to the user that originally uploaded it.

Here is a summary of what has happened to the file:

{$a->action}';
$string['virusfoundsubject'] = '{$a}: Virus found!';
$string['virusfounduser'] = 'The file you have uploaded, {$a->filename}, has been scanned by a virus checker and found to be infected! Your file upload was NOT successful.';
$string['virusplaceholder'] = 'This file that has been uploaded was found to contain a virus and has been moved or deleted and the user notified.';
$string['visible'] = 'Visible';
$string['visibletostudents'] = 'Visible to {$a}';
$string['warningdeleteresource'] = 'Warning: {$a} is referred in a resource. Would you like to update the resource?';
$string['webpage'] = 'Web page';
$string['week'] = 'Week';
$string['weekhide'] = 'Hide this week from {$a}';
$string['weeklyoutline'] = 'Weekly outline';
$string['weekshow'] = 'Show this week to {$a}';
$string['welcometocourse'] = 'Welcome to {$a}';
$string['welcometocoursetext'] = 'Welcome to {$a->coursename}!

If you have not done so already, you should edit your profile page so that we can learn more about you:

  {$a->profileurl}';
$string['whattocallzip'] = 'What do you want to call the zip file?';
$string['whattodo'] = 'What to do';
$string['windowclosing'] = 'This window should close automatically. If not, please close it now.';
$string['withchosenfiles'] = 'With chosen files';
$string['withoutuserdata'] = 'without user data';
$string['withselectedusers'] = 'With selected users...';
$string['withselectedusers_help'] = '* Send message - For sending a message to one or more participants
* Add a new note - For adding a note to a selected participant
* Add a common note - For adding the same note to more than one participant
* Extend enrolment (individual) - For extending a selected student\'s access to the course, even when an enrolment period is set
* Extend enrolment (common) - For extending more than one student\'s access to the course by the same amount';
$string['withuserdata'] = 'with user data';
$string['wordforstudent'] = 'Your word for Student';
$string['wordforstudenteg'] = 'eg Student, Participant etc';
$string['wordforstudents'] = 'Your word for Students';
$string['wordforstudentseg'] = 'eg Students, Participants etc';
$string['wordforteacher'] = 'Your word for Teacher';
$string['wordforteachereg'] = 'eg Teacher, Tutor, Facilitator etc';
$string['wordforteachers'] = 'Your word for Teachers';
$string['wordforteacherseg'] = 'eg Teachers, Tutors, Facilitators etc';
$string['writingblogsinfo'] = 'Writing blogs info';
$string['writingcategoriesandquestions'] = 'Writing categories and questions';
$string['writingcoursedata'] = 'Writing course data';
$string['writingeventsinfo'] = 'Writing events info';
$string['writinggeneralinfo'] = 'Writing general info';
$string['writinggradebookinfo'] = 'Writing gradebook info';
$string['writinggroupingsgroupsinfo'] = 'Writing groupings-groups info';
$string['writinggroupingsinfo'] = 'Writing groupings info';
$string['writinggroupsinfo'] = 'Writing groups info';
$string['writingheader'] = 'Writing header';
$string['writingloginfo'] = 'Writing logs info';
$string['writingmessagesinfo'] = 'Writing messages info';
$string['writingmoduleinfo'] = 'Writing modules info';
$string['writingscalesinfo'] = 'Writing scales info';
$string['writinguserinfo'] = 'Writing users info';
$string['wrongpassword'] = 'Incorrect password for this username';
$string['xmldbeditor'] = 'XMLDB editor';
$string['yahooid'] = 'Yahoo ID';
$string['year'] = 'year';
$string['years'] = 'years';
$string['yes'] = 'Yes';
$string['youareabouttocreatezip'] = 'You are about to create a zip file containing';
$string['youaregoingtorestorefrom'] = 'You are about to start the restore process for';
$string['youneedtoenrol'] = 'To perform that action you need to enrol in this course.';
$string['yourlastlogin'] = 'Your last login was';
$string['yourself'] = 'yourself';
$string['yourteacher'] = 'your {$a}';
$string['yourwordforx'] = 'Your word for \'{$a}\'';
$string['zippingbackup'] = 'Zipping backup';
$string['dashboards'] = 'Dashboard';
$string['people'] = 'people';
$string['team'] = 'team';
$string['teams'] = 'teams';
$string['messages'] = 'messages';
$string['account'] = 'account';
$string['recentlyviewed'] = 'Recently Viewed';
$string['lastestnew'] = 'Latest News';
$string['uniquelogins'] = 'Unique Logins';
$string['searchbycourse'] = 'Search by course code or title';
$string['coursefound'] = 'course found';
$string['recentlyviewedcourses'] = 'Recently viewed courses';
$string['createanewcourse'] = 'Create a new course';
$string['searchpeople'] = 'Search by first name, last name or username';
$string['addanewperson'] = 'Add a new person';
$string['importpeople'] = 'Import people in bulk';
$string['lastloginwas'] = 'Last login was';
$string['peoplefund'] = 'people fund';
$string['searchbyteamname'] = 'Search by team name';
$string['addanewteam'] = 'Add a new team';
$string['quickreport'] = 'Quick reports';
$string['thesereport'] = 'These reports are based on <b>active</b> people and courses';
$string['activityreport'] = 'Activity report';
$string['logsreport'] = 'Logs report';
$string['selectacoursetoviewlog'] = 'Select a course to view log.';
$string['outcomesreport'] = 'Outcomes report';
$string['userreport'] = 'User report';
$string['participationreport'] = 'Participation report';
$string['searchbymessage'] = 'Search by message subject or message content';
$string['newmessage'] = 'New message';
$string['inbox'] = 'Inbox';
$string['sent'] = 'Sent';
$string['newmessagefromuser'] = 'New message from user ';
$string['to'] = 'To';
$string['subject'] = 'Subject';
$string['message'] = 'Message';
$string['send'] = 'Send';
$string['switchtolearnerview'] = 'Switch to learner view';
$string['addanewpersontothisteam'] = 'Add a new person to this team';
$string['assignpeopletothisteam'] = 'Assign people to this team';
$string['sendloginemail'] = 'Send login emails';
$string['addanewteamunder'] = 'Add a new team under this team';
$string['asasubteam'] = 'as a sub team of';
$string['teamname'] = 'Team Name:';
$string['description'] = 'Description';
$string['automaticallyassignall'] = 'Automatically assign all courses from the ';
$string['teamtothisnewteam'] = ' team to this new team';
$string['addteam'] = 'Add Team';
$string['addteamandthenadd'] = 'Add Team &amp; then add another';
$string['or'] = 'or';
$string['cancel'] = 'Cancel';
$string['enteraname'] = 'Enter a name and description for your new team.';
$string['youcanthen'] = 'You can then add people to your team, assign courses and also add sub-teams under your new team.';
$string['formoreinfor'] = 'For more information about teams';
$string['clickhere'] = 'Click here';
$string['assgincourseto'] = 'Assign courses to this team';
$string['searchforcourse'] = 'Search for courses to assign';
$string['assigntoallcourses'] = 'Assign to all courses';
$string['assigntoalluser'] = 'Assign to all users';
$string['alsoassignto'] = 'Also assign to sub teams';
$string['sendemailnoti'] = 'Send email notifications';
$string['addnewperson'] = 'Add new person';
$string['newprofile'] = 'New profile';
$string['firstname'] = 'First Name:';
$string['lastname'] = 'Last Name:';
$string['username'] = 'Username:';
$string['mostpeopleuse'] = 'Most people use an email address as their username';
$string['accesslevel'] = 'Access Level:';
$string['company'] = 'Company:';
$string['address'] = 'Address:';
$string['city'] = 'City:';
$string['state'] = 'State:';
$string['zip'] = 'Zip:';
$string['country'] = 'Country:';
$string['timezone'] = 'Time Zone:';
$string['workph'] = 'Work Ph:';
$string['mobileph'] = 'Mobile Ph:';
$string['importuser'] = 'Import users';
$string['in2easy'] = 'In 2 easy steps you can upload up to 5000 users at once';
$string['firstyouupload'] = 'First you upload your CSV file containing user information. Your file must include these fields:';
$string['usuallyanemail'] = 'usually an email address';
$string['secondyouwill'] = 'Second you will be asked to map the columns in your CSV file with the fields that we need to create a new user account.';
$string['tip'] = 'Tip: If you are not sure how to create CSV file';
$string['usethistemplate'] = 'use this template to get started';
$string['selectfile'] = 'Select a file';
$string['csvfile'] = 'CSV file:';
$string['thefirstrow'] = 'The first row of my file contains column titles';
$string['deactivate'] = 'Deactivate';
$string['sprofile'] = 'is profile';
$string['resetpass'] = 'Reset password';
$string['thispersonhasnever'] = 'This person has never logged in';
$string['sendloginemail'] = 'Send login email';
$string['contactdetail'] = 'Contact Details';
$string['emailnotification'] = 'email notifications are enabled';
$string['emaillink'] = 'Email a link';
$string['orsetanewpass'] = 'Or set a new password';
$string['deletethisteam'] = 'Delete this team';
$string['areyouteam'] = 'Are you sure you want to delete this team?';
$string['oncethisteam'] = 'Once this team has been deleted it <b>cannot</b> be recovered - please choose carefully.';
$string['achievements'] = 'Achievements';
$string['editthis'] = 'Edit this<br />result';
$string['resultsforthe'] = 'results for the';
$string['coursestatus'] = 'Course Status';
$string['notcomplete'] = 'Not Complete';
$string['ofthecourse'] = 'of the course has been completed';
$string['setstatus'] = 'Set status as Complete';
$string['achievementfor'] = 'Achievements for this course';
$string['learnerhasno'] = 'learner has no achievements for this course';
$string['moduleresult'] = 'Module results';
$string['warningchanging'] = '<b>Warning!</b> Changing the results of the modules listed below will change the course status';
$string['passmark'] = 'Passmark';
$string['score'] = 'Score';
$string['assigntoteams'] = 'Assign to teams';
$string['searchfortoteam'] = 'Search for a team to assign';
$string['alreadyassigned'] = 'Already Assigned';
$string['areyousureremove'] = 'Are you sure to remove this team from user';
$string['library'] = 'Library';
$string['createnewcourse'] = 'Create new course';
$string['createnewcourse2'] = 'Create a new course';
$string['startbygiving'] = 'Start by giving your new course a title &amp; description';
$string['coursetitle'] = 'Course Title:';
$string['active2'] = 'Active:';
$string['referencecode'] = 'Reference Code';
$string['courselibrary'] = 'Course Library:';
$string['courselibrary2'] = 'Course Library';
$string['includethiscourse'] = 'Include this course in the course library so learners can self signup';
$string['moduleorder'] = 'Module Order:';
$string['makelearner'] = 'Make learners complete all modules in the order that you choose';
$string['modulesarelaunch'] = 'Modules are launched in a new window, with no browser toolbars';
$string['newwindow'] = 'New Window:';
$string['doyouwant'] = 'Do you want students to complete this course within a specific time frame?';
$string['mustcomplete'] = 'Must complete by:';
$string['timespan'] = 'A Time Span';
$string['specificdate'] = 'A Specific Date';
$string['updatethisdate'] = 'Update this date for all people already assigned to the course';
$string['whenastudent'] = 'When a student completes this course how long will they be compliant for?';
$string['compliantfor'] = 'Compliant For:';
$string['forever'] = 'Forever';
$string['day1'] = '7 days';
$string['day2'] = '14 days';
$string['day3'] = '30 days';
$string['day4'] = '60 days';
$string['day5'] = '90 days';
$string['day6'] = '180 days';
$string['day7'] = '365 days';
$string['day8'] = 'Other';
$string['day9'] = '21 days';
$string['automaticresit'] = 'Automatic Resit:';
$string['dontresit'] = "Don't resit";
$string['weeks'] = 'Weeks';
$string['beforecompliant'] = "before compliance ends, move this course to the students 'To Do' list & send them a reminder email.";
$string['certificatetemp'] = 'Certificate Template';
$string['ifyouattach'] = 'If you attach a certificate template to this course your students will be able to download it when they complete the course.
                                                You can put special placeholder fields like [COURSE_TITLE] in your template and they will be replaced with student or course details.</p><p>For a list of placeholder values see:';
$string['howtocreate'] = 'How to create a certificate template';
$string['uploadtemp'] = 'Upload a template:';
$string['templatecanonly'] = 'Templates can only be .doc, .docx and .rtf files';
$string['iwanttosell'] = 'I want to sell this course';
$string['formore'] = 'For more ecommerce options go to the';
$string['accountmenu'] = 'Account menu and select the Ecommerce tab';
$string['coursefee'] = 'Course Fee:';
$string['briefdes'] = 'Brief Description:';
$string['255character'] = '<span>255</span> character(s) left';
$string['fulldes'] = 'Full Description:';
$string['bydefaultyou'] = 'By default you will be added to the notifications for this course. You can change this or add other administrators to the notifications list by editing the course settings later on.';
$string['survey'] = 'Survey';
$string['pageof'] = '<b>Page</b> of information';
$string['chat'] = 'Chat';
$string['assignment'] = 'Assignment';
$string['duplicateor'] = '<b>Duplicate</b> or <b>Link to</b> a module in another course';
$string['embedcontent'] = '<b>Embed</b> content from another website';
$string['linkto'] = '<b>Link</b> to another website';
$string['addmodule'] = 'Add a module';
$string['coursesummary'] = 'Course Summary';
$string['themodules'] = 'The modules in this course must be completed in the order displayed and there is no time limit set.';
$string['peopleareass'] = 'people are assigned to this course';
$string['peoplehave'] = 'people have completed this course';
$string['noticeboard'] = 'Noticeboard';
$string['documents'] = 'Documents';
$string['gradebook'] = 'Gradebook';
$string['loginlms'] = 'Login information LMS';
$string['welcometo'] = 'Welcome to';
$string['pleaseenter'] = 'Please enter your username & password to access your online learning';
$string['username'] = 'Username';
$string['rememberme'] = 'Remember me on this computer';
$string['portal'] = 'Portal Online Training';
$string['doyouwant'] = 'Do you want to remove this user from the course?';
$string['deleteall'] = 'Delete all persons';
$string['assigntoexist'] = 'Assign to existing people';
$string['importnew'] = 'Import new people';
$string['searchforpeople'] = 'Search for people to assign';
$string['importpeople'] = 'Import People';
$string['createnew'] = 'Create new user accounts in bulk by entering a comma separated list of email addresses';
$string['exemail'] = 'e.g. user@domain.com,another@lms.com';
$string['emailaddr'] = 'Email Addresses:';
$string['1000character']  = '1000 character(s) left';
$string['sendthese'] = 'Send these people a login email';
$string['person'] = 'person';
$string['searchforteam'] = 'Search for team to assign';
$string['addanote'] = 'Add a note';
$string['noticeboardcontent'] = 'noticeboard content';
$string['addadoc'] = 'Add a document';
$string['areyou'] = 'Are you sure to delete?';
$string['addareference'] = 'Add a reference document to the course';
$string['uploadadd'] = 'Upload additional documents as reference material for your course.';
$string['notealpha'] = 'Note: Alphatechnologies does not track whether or not the trainee has viewed the document.';
$string['maxfile'] = 'Max file size: 10MB';
$string['filename'] = 'Filename:';
$string['optionalchang'] = 'Optional: change display name';
$string['myachieve'] = 'My Achievements' ;
$string['youdeletecourse'] = 'Are you sure you want to delete this course?';
$string['deletecourse'] = 'Delete this course';
$string['monthago'] = 'month ago';
$string['monthago2'] = 'months ago';
$string['yearago'] = 'year ago';
$string['yearago2'] = 'years ago';
$string['dayago'] = 'day ago';
$string['dayago2'] = 'days ago';
$string['weekago'] = 'week ago';
$string['weekago2'] = 'weeks ago';
$string['hourago'] = 'hour ago';
$string['hourago2'] = 'hours ago';
$string['minuteago'] = 'minute ago';
$string['minuteago2'] = 'minutes ago';
$string['secondago'] = 'second ago';
$string['secondago2'] = 'seconds ago';
$string['second'] = 'second';
$string['minute'] = 'minute';
$string['hour'] = 'hour';
$string['day'] = 'day';
$string['week'] = 'week';
$string['month'] = 'month';
$string['year'] = 'year';
$string['decade'] = 'decade';
$string['ago'] = 'ago';
$string['notloginyet'] = 'Not login yet';
$string['activityfor'] = 'Activity for the last 30 days';
$string['uniquelog'] = 'Unique logins';
$string['coursesale']= 'Course sales';
$string['coursecomple'] = 'Course completed';
$string['coursetodo'] = 'Courses To Do';
$string['searchforcourse'] = 'Search for a course';
$string['thesearecourse'] = 'These are the courses that I have completed';
$string['achievedon'] = 'Achieved On:';
$string['recentachie'] = 'Recent Achievements';
$string['completeon'] = 'Completed on ';
$string['unread'] = 'unread';
$string['startthiscourse'] = 'Start this course';
$string['additionalrefe'] = 'Additional Reference';
$string['newmessage'] = 'New message to user ';
$string['ineedto'] = 'I need to complete the following modules to finish this training course.';
$string['nextmodule']  = 'Next Module';
$string['exit'] = 'Exit';
$string['wascreate'] = ' was created by ';
$string['wasviewed'] = ' was viewed by ';
$string['wasupdated'] = ' was updated by ';
$string['loggedin'] = 'logged in';
$string['loggedout'] = 'logged out';
$string['news'] = 'news';
$string['postedsome'] = 'posted some news';
$string['signup'] = 'Sign up';
$string['switchto'] = 'Switch to ';
$string['view'] = 'view';
$string['oncethiscourse'] = 'Once this course has been deleted it <b>cannot</b> be recovered - please choose carefully.';
$string['createyour'] = '1. Create your account';
$string['pleaseselect'] = 'Please select a country';
$string['phonenumber'] = 'Phone Number:';
$string['uploadyour'] = '2. Upload your logo - Optional';
$string['selectlogo'] = 'Select logo:';
$string['note'] = 'Note: You can upload or change your logo later';
$string['youmust'] = 'You must agree to the terms of service to continue';
$string['iagree'] = 'I agree to the';
$string['termsof'] = 'terms of service';
$string['pleaselast'] = 'Please enter your last name';
$string['pleasefirst'] = 'Please enter your first name';
$string['pleaseuser'] = 'Please enter your username';
$string['pleaseemail'] = 'Please enter a valid email address';
$string['wewillsent'] = 'We will send account login information to this address';
$string['pleaseentermail'] = 'Please enter your email';
$string['pleasecompany'] = 'Please enter your company name';
$string['included'] = "What's included?";
$string['tryafull'] = 'Try a full, unrestricted trial of the Alphatechnologies training platform for 14 days.';
$string['included2'] = '<ul class="pricingpoints">
                        <li>Full access to all features</li>
                        <li>Enhanced Security</li>                        
                        <li>Custom Branding</li>
                        <li>Ecommerce features if you want to sell courses</li>
                        <li>Mobile learning for iPad, iPhone &amp; Android</li>
                        <li>Access online, anytime, from anywhere</li>
                    </ul>
                    <h3>How does the 14 day free trial work?</h3>
                    All Alphatechnologies accounts start out as trial accounts and give you full access to the Alphatechnologies system.<p>At any time during the 14
                            days you can upgrade to a paid plan.</p>
                            <p>If you attempt login to Alphatechnologies after the trial period has expired
                            you will be prompted to upgrade to a paid plan in order to proceed.</p>';
$string['viewassigment'] = 'View assigment\'s feedback';
$string['webcounter'] = 'Web Counter';
$string['uploadprofile'] = 'Upload a profile picture';
$string['editprofile'] = 'Edit my profile';
$string['myprofilepicture'] = 'My profile picture';
$string['selectpic'] = 'Select a profile picture to upload';
$string['maximum'] = 'The maximum file Size 1Mb';
$string['noteprofile'] = '<strong>Note:</strong> This could take a few minutes to show up on your profile';
$string['subsetup'] = 'Subscription Setup:';
$string['billing'] = 'Billing';
$string['messagelab'] = 'Messages &amp; Labels';
$string['emailsetup'] = 'Email Setup';
$string['ecommerce'] = 'Ecommerce';
$string['term'] = 'Terms';
$string['history'] = 'History';
$string['no'] = 'NO';
$string['startdate'] = 'Start date';
$string['endate'] = 'End date';
$string['upgrade'] = 'Upgrade Now';
$string['accountnam'] = 'Account Name:';
$string['pleaseenter'] = 'Please enter your account name';
$string['contactper'] = 'Contact Person:';
$string['phone'] = 'Phone';
$string['deafaulttime'] = 'Default Time Zone';
$string['bydefault'] = 'By default all new users will have this time zone set.';
$string['customweb'] = 'Custom website address';
$string['setweb'] = 'Set the website address for your customized login page.';
$string['domain'] = '<b>Or</b> use your own domain name:';
$string['customdomain'] = 'How to setup a custom domain';
$string['yourlogo'] = 'Your logo';
$string['changelogo'] = 'Change this logo';
$string['accepttype'] = 'Accepted File Types:';
$string['maxfile'] = 'Maximum File Size:';
$string['resized'] = 'Your logo will be resized to fit within 300px wide, 90px tall';
$string['showlogin'] = 'Show this logo on the login screen';
$string['showtrain'] = "Show this logo in the Trainee's toolbar";
$string['theme'] = 'Theme';
$string['selectone'] = 'Select one of our pre-built themes from the options below or create your own custom theme to match your companies color scheme.';
$string['custometheme'] = 'Custom Theme';
$string['green'] = 'Green';
$string['grey'] = 'Grey';
$string['blue'] = 'Blue';
$string['red'] = 'Red';
$string['yellow'] = 'Yellow';
$string['black'] = 'Black';
$string['darkside'] = 'Dark Side';
$string['default'] = 'Default';
$string['learneradmin'] = 'Learner and Admin pages';
$string['titlebar'] = 'Title Bar';
$string['titlebartext'] = 'Title Bar Text';
$string['heading'] = 'Navigation and Heading';
$string['headingtext'] = 'Heading Text';
$string['loginpage'] = 'Login page';
$string['background'] = 'Background';
$string['addicon'] = 'Additional icons';
$string['change'] = 'change';
$string['yourfacicon'] = 'Your favicon appears in most web browsers in the address bar, tabs or bookmarks menu.';
$string['ipadicon'] = 'This image displays when you add a home screen icon on your iPhone, iPad (apple-touch-icon.png).';
$string['servicepack'] = 'Service pack';
$string['trial'] = 'Trial';
$string['to'] = 'To:';
$string['sendmessage'] = 'Send a message to ';
$string['reply'] = 'Reply';
$string['messagefound'] = 'message found';
$string['upgradeto'] = 'Upgrade to one of our subscription options';
$string['annual'] = 'Annual Commitment';
$string['monthtom'] = 'Month to month';
$string['permonth'] = 'Per Month';
$string['billingannual'] = 'Billing Annually';
$string['useractive'] = ' active users';
$string['supportemail'] = 'E-Mail Support';
$string['branding'] = 'Custom Branding';
$string['register'] = 'Register';
$string['enter_firstname'] = 'Please enter first name';
$string['enter_lastname'] = 'Please enter last name';
$string['enter_emailid'] = 'Please enter your user name is your email';
$string['enter_company'] = 'Please enter your company';
$string['remove_u_f_course'] = 'Do you want to remove users from the course?';
$string['changeresult'] = 'Change the result for';
$string['changestatus'] ='Changing the status to complete will set of all module results to complete.';
$string['changecompleted'] = 'Completed';
$string['reasonfor'] = 'Reason for change';
$string['save'] = 'Save';


// Chiến add
$string['new'] = "New";
$string['tothelist'] = "Return to the list";

$string['schools'] = "Schools";
$string['listschools'] = "List Schools";
$string['addschools'] = "Add New Schools";
$string['blocks'] = "Block Schools";
$string['addblocks'] = "Add Block Schools";
$string['listblocks'] = "List Block Schools";
$string['parent']	=	'Parent';
$string['class']	=	'Class';
$string['classname']	=	'Class name';
$string['classlist']	=	'Class list';
$string['classview']	=	'Class detail';
$string['classedit']	=	'Class edit';
$string['classnew']	=	'Add new class';
$string['classgroup'] = 'Class groups';
$string['listclassgroup'] = 'List of class groups';
$string['newclassgroup'] = 'Add new class groups';
$string['groupnumber'] = 'Group number';
$string['totalstudents'] = 'Total students';
$string['homeroomteacher'] = 'Homeroom teacher';
$string['numberofperiods'] ='Number of periods';
$string['chooseyear'] ="Choose year";
$string['notedd'] = "Only select when students leave school";
$string['attendancebook'] = "Attendance book";
$string['addclassschedule'] = "Add class schedule";
$string['assignstudents'] = 'Assign students to the classroom';
$string['toclass'] = 'Go to class';



$string['teacher']	= 'Teacher';
$string['codeteacher']	= 'Teacher Code';
$string['codemanagement']	= 'Management Code';
$string['foreignteacher']	= 'Foreign Teacher';
$string['tutorsteacher']	= 'Tutor Teacher';
$string['reasonableteam']	= 'Reasonable Team';
$string['listforeignteacher'] = 'List of foreign teachers';
$string['listtutorsteacher'] = 'List of teaching assistants';
$string['manager'] = 'Manager';
$string['coordinator'] = 'Coordinator';
$string['area'] = 'Area';
$string['nationality'] = 'Nationality';
$string['managerft'] = 'Foreign teacher management team';
$string['managert'] = 'Teachers management team';
$string['typeoflabor'] = 'Type of flabor';


// trang add
// khoi hoc sinh
$string['list_block_student']='List block student';
$string['name_block_student']='Name block student ';
$string['block_student']='Block student ';
$string['code_block_student']='Code block student ';
$string['total_class']='Total class ';
$string['total_class_group']='Total class group ';
$string['total_student']='Total student';

//
$string['choise_the_block']='Choice the block';
$string['block_1']='Block 1';
$string['block_2']='Block 2';
$string['block_3']='Block 3';
$string['block_4']='Block 4';
$string['block_5']='Block 5';
$string['block_6']='Block 6';
$string['block_7']='Block 7';
$string['block_8']='Block 8';
$string['block_9']='Block 9';

$string['add_block_student']='Add block student';
$string['edit_block_student']='Edit block student';
$string['view_block_student']='View block student';
$string['delete_block_student']='Do you want to delete this student block?';
$string['del_block_student']='Delete block student';
$string['note']='Note';
$string['errors_code_student']='Block student code already exists please enter another code  ';
//hoc sinh
$string['list_student']='List student';
$string['add_student']='Add student';
$string['edit_student']='Edit student';
$string['delete_student']='Do you want to delete this student?';
$string['del_student']='Delete student';
$string['view_student']='View student';
$string['full_name']='Full name';
$string['error_acount_student']='Account already exists';
$string['student']='Student';
$string['codestudent']='Student code';
$string['namestudent']='Name student';
$string['sex']='Sex';
$string['man']='Man';
$string['women']='Women';
$string['input_score']='Input score';
$string['time_start']='Time start';
$string['time_end']='Time end';
$string['home_address']='Home address';
$string['note_for_teacher']='Note for teacher';

// xem diem unit va diem hoc ki hoc sinh
$string['view_unitest_student']='View unitest student';
$string['list_unitest_student']='List unitest student';
$string['list_semester_student']='List semester student';
//thoi khoa bieu
$string['list_schedule']='List schedule';
$string['schedule_management']='Schedule management';
$string['add_schedule']='Add schedule';
$string['edit_schedule']='Edit schedule';
$string['view_schedule']='View schedule';
$string['delete_schedule']='Do you want to delete this schedule?';
$string['number_days_learn']='Number days learn';
$string['syllabus_name']='Syllabus name';
$string['GVNN']='Teacher';
$string['GVTG']='TA';

$string['choose_the_date']='Choose thedate';
$string['choose_month']='Choose month';
$string['choose_the_school_year']='Choose the school year';

$string['study_day']='Study day';
$string['class_room']='Class room';
$string['day']='Day';
$string['month']='Month';
$string['time']='Time';
$string['schedule']='Schedule';

$string['monday']='Monday';
$string['tuesday']='Tuesday';
$string['wednesday']='Wednesday';
$string['thursday']='Thursday';
$string['friday']='Friday';
$string['saturday']='Saturday';
$string['sunday']='Sunday';

$string['january']='January';
$string['february']='February';
$string['march']='March';
$string['april']='April';
$string['may']='May';
$string['june']='June';
$string['july']='July';
$string['august']='August';
$string['september']='September';
$string['october']='October';
$string['november']='November';
$string['december']='December';

$string['school_year']='School year';
// quan ly diem 
$string['number_of_component_points']='Number of component points';
$string['add_unitest']='Add unit test';
$string['edit_unitest']='Edit unit test';
$string['view_unitest']='View unit test';
$string['view_list_unitest']='View list unit test';
$string['del_unitest']='Do you want to delete this unittest point?';
$string['list_unitest']='Small test list';
$string['issetunit']='Students already exist points in this month, proceed to edit?
';
$string['errortunit']='Students have not been assigned to a specific class and class group. Unable to proceed to enter scores'; 
$string['unit_test']='Manage small test';

$string['test']='Test ';
$string['test_1']='Test 1';
$string['test_2']='Test 2';
$string['test_3']='Test 3';
$string['test_4']='Test 4';
$string['test_5']='Test 5';
$string['test_6']='Test 6';
$string['test_7']='Test 7';
$string['test_8']='Test 8';
$string['test_9']='Test 9';
$string['test_10']='Test 10';
$string['test_11']='Test 11';
$string['test_12']='Test 12';
$string['test_13']='Test 13';
$string['test_14']='Test 14';
$string['test_15']='Test 15';
$string['point_ladder']='Point ladder';

// diem hoc ki
// diem hoc ki
$string['semester']='Semester score management';
$string['semester1']='Semester 1'; 
$string['semester2']='Semester 2'; 
$string['addsemester']='Add semester';
$string['editsemester']='Edit semester';
$string['viewsemester']='View semester';

$string['elementary_semester_score']='Elementary semester score';
$string['high_school_semester_score']='High school semester score';
$string['delete_semester']='Do you want to delete this semester grade?';
$string['import_semester']='Import semester';
$string['list_semester']='Semester';

$string['issetsemester']='Students already exist points in this school year, proceed to edit?';

$string['readingWriting']='Reading and Writing /55';
$string['speaking']='Speaking /25';
$string['essay']='Essay /10';
$string['listening']='Listening /20';
$string['vocabularyGramar']='Vocabulary and Gramar /35';
$string['reading']='Reading /10';
$string['vocabularyGrammarReadingWriting']='Vocabulary and Grammar and Reading and Writing/55';
$string['totalMidtermMark1']='Total Midterm Mark 1 /100';
$string['totalMidtermMark2']='Total Midterm Mark 2 /100';
$string['overall']='Overall /100';

//cau hoi
$string['delete_ques']='Do you want to delete this question?';
$string['name_ques']='Question name';
$string['question_categories']='Question categories';
$string['list_ques']='List question';
$string['addquestion_categories']='Add question categories';
$string['editquestion_categories']='Edit question categories';
$string['del_question_categories']='Do you want to delete this question category? When deleting the question category, all questions in the forum will be deleted!';

//quiz
$string['quiz']='Quiz';
$string['add_quiz']='Add quiz';
$string['edit_quiz']='Edit quiz';
$string['del_quiz']='Do you want to delete this quiz?';
$string['preparethequiz']='Prepare the quiz';
$string['max_score']='Maximum score';
$string['name_quiz']='Name quiz';
$string['time_quiz']='Time to work (minutes)';
$string['time_open']='Open time';
$string['time_close']='Closed time';
$string['open']='Open';
$string['errortimequiz']='Closing time is smaller than opening time';

$string['add_question_to_quiz']='Do you want to add questions to this quiz?';
$string['del_question_form_quiz']='Do you want to delete this question from this quiz?';
$string['succesadd_question_to_quiz']='Questions have been added to the quiz!';
$string['isset_question_to_quiz']='The question already exists in the quiz!';
$string['assign_quiz_cousre']='Assign quiz to groups';
$string['list_quiz']='List quiz';
$string['list_quiz_course']='Manage the quiz of group';


// đánh giá học sinh

$string['evaluation_by']='Evaluation by';

$string['list_evaluation_student']='List evaluation student';
$string['add_evaluation_student']='Add evaluation student';
$string['edit_evaluation_student']='Edit evaluation student';
$string['view_evaluation_student']='View evaluation student';
$string['topic_month']='Topic month';
$string['attendance']='Attendance';
$string['material']='Learning material';
$string['participation']='Participation';
$string['learning_attitude']='Learning attitude';
$string['other_comment']='General comment ';
$string['isset_evaluation_student']='Students were assessed in this month and semester ';
$string['error_evaluation_student']='Students have not been assigned to a specific class and class group. Unable to proceed with the evaluation';
$string['delete_evaluation_student']='Do you want to delete this evaluation?';

$string['import_excel']='Import excel';
$string['export_excel']='Export excel';
$string['success']='Success';
$string['error']='Error';
$string['total_page']='Total page';
$string['enter_name']='Enter name';
$string['error_participate_evaluation_student']='Class group cannot participate in evaluation student';
$string['evaluation_student']='Evaluation student';


// báo cáo report class
$string['report_class']='Report class';
$string['report_number_student']='Report the number of students';
$string['report_ranking_school']='Synthetic Ranking report';
$string['report_ranking_student']='School Ranking Report';
$string['day_from']='Day from';
$string['day_to']='Day to';
$string['choose_semester']='Choose semester';
$string['all_year']='Overall';
$string['type_school']='Type of school';
$string['elementary']='Elementary';
$string['high_school']='High school';
$string['report_outstanding_students']='Report outstanding students';
$string['report_semester_1_below_50']='Report the list of students in the first semester grades below 50 points';
$string['schedule_report']='School schedule report';
$string['school_report']='School report';
$string['semester_report']='Study report the school year';
$string['student_report']='Report students'; 
$string['teacher_report']='Teacher report';
$string['ta_report']='TA report';
$string['year_end_academic_report']='Year end academic report';
$string['total_certificate_school']='Report total certificate school';
$string['certificate_eport']='Certificate eport';
$string['semester1_report']='Semester 1 report';


// Tuấn schools
$string['nameschools'] = 'Schools Name';
$string['district'] = 'District';
$string['commune'] = 'Commune';
$string['emailschools'] = 'Schools Email';
$string['principal'] = 'Principal';
$string['deputy'] = 'Deputy';
$string['birthday'] = 'Birthday';
$string['addrschools'] = 'Schools Address';
$string['noteschools'] = 'Note';
$string['searchresultsdonotexist'] = 'Search results not exist';
$string['mapschools']='School map';
$string['tkb']='schedule';
$string['warning'] = 'Warning!';
$string['truongtontaikhoi'] = 'The schools still exists block';
$string['deletedsucessfully'] = 'Deleted successfully!';
$string['selectdel'] = 'Select schools to delete';
$string['searchnoresult'] = 'No search results';
$string['searchnoresultkeyword'] = 'No search results for keyword';
$string['doyouwantdel'] = 'Do you want delete';
$string['notaccess'] = 'You do not have full access';
$string['detail'] = 'Detail';
$string['yearlink'] = 'Links year';
$string['addsuccess'] = 'Added successfully';
$string['classlink'] = 'Number of link class of the first year';
$string['notempty'] = 'Not be empty';
$string['invalidemail'] = 'Invalid email';
$string['invalidphone'] = 'Invalid phone';
$string['sizesys'] = 'Size';
$string['notimage'] = 'Incorrect image format';
$string['detailschools'] = 'Schools Details';
$string['updateschools'] = 'Update Schools';
$string['updatesuccess'] = 'Updated successfully';
$string['level'] = 'Level';
$string['level1'] = 'Level one';
$string['level2'] = 'Level two';
$string['level3'] = 'Level three';


//Đánh giá
$string['teacher_evaluation']='Teacher Evaluation';
$string['ta_evaluation']='TA Evaluation';
$string['planning']='Planning & Preparation';
$string['organization']='Classroom presence & organization';
$string['interaction']='Interaction with students';
$string['resources']='Materials & Resources';
$string['punctality']='Punctuality';
$string['activities']='Class Activities';
$string['homework']='Homework';
$string['appearance']='Appearance';
$string['youremail']='Your email';
$string['yourname']='What is your name';
$string['yourschools']='What is the name of your school?';
$string['rhta']='RHTA';
$string['teacherofyou']='What is the name of your teachers?';
$string['dobest']='What do they do best';
$string['dobetter']='What do they do better';
$string['newevaluation']='Add new evaluation';
$string['teachernn']='Teacher';
$string['teachertg']='Teacher';
$string['sumpoint']='Point';
$string['date_evaluation'] = 'Date evaluation';

// tổ quản lý GVNN
$string['RHT']='RHT';
// tổ quản lý GVTG
$string['RHTA']='RHTA';

$string['teacher_eval'] = 'List teacher evaluation';
$string['tg_eval'] = 'List teacher tutoris evaluation';

//Tài liệu
$string['listdoc']='List document';
$string['assigned'] = 'Assigned';
$string['docname'] = 'Document name';
$string['describe'] = 'Describe document';
$string['assigntoclass'] = 'Assign to class group';
$string['assigndoc']='Assign document to class group';
$string['namegroups'] = 'Class groups name';
$string['assign'] = 'Assign';
$string['selectfile'] = 'Please select file';
$string['lecture'] = 'Lecture schedule';
$string['action7day'] = 'Operating in 7 days ago';
$string['viewed'] = ' viewed ';
$string['addnew'] = ' add new ';
$string['logined'] = ' login ';
$string['logouted'] = ' logout ';

$string['document_manage'] = 'Manage document';
// user
$string['delete_user'] = 'Do you want delete user?';
$string['de_activeusers'] = 'Lock user';
$string['role_decentralization'] = 'Role decentralization';

$string['absent'] = 'Absent';

// mo kho danh gia

$string['unlock'] = 'Unlock ';
$string['unlock_evaluation_student'] = 'Unlock student evaluation';
$string['unlock_evaluation_student_groups'] = 'Lock student evaluation';
// chinh sua them phan them moi tai khoan

$string['username_new'] = 'User name ';
$string['email_new'] = 'Email ';
// them cho menu 
$string['evaluate'] = 'Evaluate ';
$string['school_management'] = 'School management ';
$string['study_document'] = 'Study document ';
$string['learning_outcomes'] = 'Learning outcomes ';
//
$string['history_student'] = 'History student';
$string['dowload_file_sample'] = 'Download the sample file here';

// quản lý năm học
$string['list_school_year'] = 'List school year';
$string['addnew_school_year'] = 'Add new school year';
$string['edit_school_year'] = 'Edit school year';
$string['start_year'] = 'Start year';
$string['end_year'] = 'End year';
$string['isset_school_year'] = 'The school year already exists';
$string['delete_school_year'] = 'Do you want to delete this school year?';
$string['transfer_group'] = 'Transfer class group';
$string['no_records'] = 'No records';
$string['define_group_name'] = 'Group name = coefficient of the class name + group name';
$string['export_file'] = 'Export file';
