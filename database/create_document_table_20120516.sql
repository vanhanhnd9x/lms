CREATE TABLE `document` (                                            
            `id` INT(11) NOT NULL AUTO_INCREMENT,                              
            `new_name` VARCHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT '0',   
            `file_name` VARCHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT '0',  
            `course_id` INT(11) NOT NULL DEFAULT '0',                          
            PRIMARY KEY (`id`)                                                 
          ) ENGINE=INNODB DEFAULT CHARSET=utf8        