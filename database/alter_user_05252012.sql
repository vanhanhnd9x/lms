USE lms;
ALTER TABLE `user` ADD COLUMN `font` TINYINT(1) DEFAULT '1' NULL;
ALTER TABLE `user` ADD COLUMN `title_bar_color` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `title_bar_text_color` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `navigation_heading_color` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `heading_text_color` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `login_page_background` VARCHAR(255);
ALTER TABLE `user` ADD COLUMN `favicon` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `ipad_icon` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `hide_message_tab` TINYINT(1) DEFAULT '0' NULL;
ALTER TABLE `user` ADD COLUMN `unretricted_messages` TINYINT(1) DEFAULT '0' NULL;
ALTER TABLE `user` ADD COLUMN `login_message` VARCHAR(255) NULL;
ALTER TABLE `user` ADD COLUMN `welcome_message` VARCHAR(255) DEFAULT '0';
ALTER TABLE `user` ADD COLUMN `hide_course_date` TINYINT(1) DEFAULT '0';