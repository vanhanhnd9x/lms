CREATE TABLE `course_settings` (                                                     
                   `course_id` INT(11) NOT NULL DEFAULT '0',                                          
                   `course_library` TINYINT(1) NOT NULL DEFAULT '0',                                  
                   `module_order` TINYINT(1) NOT NULL DEFAULT '0',                                    
                   `new_window` TINYINT(1) NOT NULL DEFAULT '0',                                      
                   `must_complete_by` INT(1) NOT NULL DEFAULT '0',                                    
                   `update_complete_date` TINYINT(1) NOT NULL DEFAULT '0',                            
                   `compliant_for` INT(1) NOT NULL DEFAULT '0',                                       
                   `automatic_resit` INT(1) NOT NULL DEFAULT '0',                                     
                   `certificate_template` VARCHAR(255) NOT NULL DEFAULT '0',                          
                   `sell_course` TINYINT(1) NOT NULL DEFAULT '0',                                     
                   `active` TINYINT(1) NOT NULL DEFAULT '1',                                          
                   `fee` FLOAT NOT NULL DEFAULT '0',                                                  
                   `currency` VARCHAR(3) NOT NULL DEFAULT 'USD',                                      
                   `sale_description` VARCHAR(255) DEFAULT NULL,                                      
                   `time_span_complete` TINYINT(1) NOT NULL DEFAULT '0',                              
                   `time_span_complete_number` INT(255) NOT NULL DEFAULT '0',                         
                   `time_span_complete_type` VARCHAR(255) NOT NULL DEFAULT '0' COMMENT 'days/weeks',  
                   `complete_specific_date` DATE DEFAULT NULL,                                        
                   `sale_full_description` TEXT,                                                      
                   `id` INT(11) NOT NULL AUTO_INCREMENT,                                              
                   `file_name` VARCHAR(255) NOT NULL DEFAULT '0',                                     
                   PRIMARY KEY (`id`)                                                                 
                 ) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1  