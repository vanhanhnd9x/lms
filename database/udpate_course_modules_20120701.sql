UPDATE course_modules SET completionview = 1;
UPDATE course SET enablecompletion = 1 WHERE enablecompletion = 0;
UPDATE course_modules SET completion = 1 WHERE completion = 0 AND module NOT IN (13,20,21,23,27,47);
UPDATE course_modules SET completion = 2 WHERE completion = 0 AND module IN (13,20,21,23,27,47);