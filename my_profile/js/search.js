$(document).ready(function(){
    $('.mid').click(function() {
        
        $('#upPhoto').show();
    });
    $('#cancelUpload').click(function() {
        
        $('#upPhoto').hide();
    });
    
    $("#username").keyup(function() {
        var userName = $(this).val();
        searchWhenKeyUpUserName(userName);
    });
    $("#username").focusout(function() {
        var userName = $(this).val();
        searchWhenKeyUpUserName(userName);
    });
    $("#email").keyup(function() {
        var email = $(this).val();
        searchWhenKeyUpEmail(email);
    });
    $("#email").focusout(function() {
        var email = $(this).val();
        searchWhenKeyUpEmail(email);
    });
   
});


function validate(){
    var error=0;
    
    if(document.getElementById('firstname')&&document.getElementById('firstname').value==''){
        $("#erFirst").show();
        error=1;
    }else{
        $("#erFirst").hide();
    }
    if(document.getElementById('lastname')&&document.getElementById('lastname').value==''){
        $("#erLast").show();
        error=1;
    }else{
        $("#erLast").hide();
    }
    
    if(document.getElementById('company')&&document.getElementById('company').value==''){
        $("#erCompany").show();
        error=1;
    }else{
        $("#erCompany").hide();
    }
    
    if(document.getElementById('password')&&document.getElementById('confirm_password')){
        password=document.getElementById('password').value;
        confirm_password=document.getElementById('confirm_password').value;
        if(password!=confirm_password){
        $("#erPass").show();
        $("#erPassConfirm").show();
        error=1;    
        }
        
    }else{
        $("#erPass").hide();
        $("#erPassConfirm").hide();
    }
		
    
    if(document.getElementById("dupUsername")&&(document.getElementById("dupUsername").value==1||document.getElementById("dupUsername").value=='1')){
        document.getElementById("erUsername").innerHTML='Username is already existed';
        $("#erUsername").show();
        error=1;	
    }
    else if(document.getElementById('username')&&document.getElementById('username').value==''){
        $("#erUsername").show();
        error=1;
    }
    else{
        $("#erUsername").hide();
    }
    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    
    if(document.getElementById("dupEmail")&&(document.getElementById("dupEmail").value==1||document.getElementById("dupEmail").value=='1')){
        error=1;
        document.getElementById("erEmail").innerHTML='Email is already existed';
        $("#erEmail").show();
    }
    else if(document.getElementById('email')&&reg.test(document.getElementById('email').value) == false) {
        document.getElementById("erEmail").innerHTML='Email is not valid';
        $("#erEmail").show();
        error=1;
      document.getElementById("errEmail").value==1;
    }
    
    else{
        $("#erEmail").hide();
    
    }
   
    if(error==1){
        return false;
    }
    return true;
    
}
function searchWhenKeyUpUserName(username)
{
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            if(xmlhttp.responseText=='1'||xmlhttp.responseText==1){
                document.getElementById("erUserName").innerHTML='Username is already existed';
                $("#erUserName").show();
                document.getElementById("dupUsername").value=1;
								
            }
            else{
                $("#erUserName").hide();
                document.getElementById("dupUsername").value=0;
            }
						
            

        }
    }
    
    xmlhttp.open("GET","ajaxcheckusername.php?q="+username,true);
    xmlhttp.send();
}
function searchWhenKeyUpEmail(email){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            
            if(xmlhttp.responseText=='1'||xmlhttp.responseText==1){

                document.getElementById("erEmail").innerHTML='Email is already existed';
                $("#erEmail").show();
                document.getElementById("dupEmail").value=1;
                
            }
            else if(document.getElementById("errEmail")&&document.getElementById("errEmail").value==0){
                $("#erEmail").hide();
                document.getElementById("dupEmail").value=0;
            }
            

        }
    }
    
    xmlhttp.open("GET","ajaxcheckemail.php?q="+email,true);
    xmlhttp.send();
}



