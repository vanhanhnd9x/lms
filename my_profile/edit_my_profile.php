<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');

global $CFG;
require_once($CFG->dirroot . '/common/lib.php');

require_login(0, false);

$PAGE->set_title(get_string('myprofile'));
$PAGE->set_heading(get_string('myprofile'));
global $USER;
//$user_id = optional_param('user_id', '', PARAM_TEXT);    
$user_id = $USER->id;
$action = optional_param('action', '', PARAM_TEXT);
echo $OUTPUT->header();
$user = get_user_from_id($user_id);
//var_dump($user);
//die();
?>
<!-- <script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/common/js/jquery-1.7.1.js"></script> -->
<script type="text/javascript" src="js/search.js"></script> 

<div class="row">
  <div class="col-md-10">
      <div class="card-box ribbon-box">
          <div class="card-content">
            <div class="ribbon ribbon-success">
              <h4 class="m-t-0 header-title">Chỉnh sửa hồ sơ</h4>
            </div>
            <p></p>
              
              <form id="profile" action="<?php echo $CFG->wwwroot ?>/my_profile/index.php?action=update_profile" onsubmit="return validate();" method="post" class="form-row">

                      <div class="col-5 form-group">
                        <label>Công ty</label>
                        <div id="erCompany" class="alert alert-danger" style="display: none;">
                              <?php print_string('pleasecompany') ?></div>
                        <input type="text" id="company" name="company" class="form-control" value="<?php echo $user->company ?>">
                      </div>
                      <div class="col-3 form-group">
                        <label class="control-label">Username</label>
                        <input type="text" id="username" name="username" class="form-control" value="<?php echo $user->username ?>">
                      </div>
                      <div class="col-4 form-group">
                        <label class="control-label">Email</label>
                        <div id="erEmail" class="alert alert-danger" style="display: none;">
                                    <?php print_string('pleaseentermail') ?></div> 
                        <input type="email" id="email" name="email" class="form-control" value="<?php echo $user->email ?>" required>
                      </div>

                      <div class="col-6">
                          <div class="form-group label-floating">
                              <label class="control-label">Mật Khẩu</label>
                              <div id="erPass" class="alert" style="display: none;">
                                    <?php print_string('passwordsame') ?></div> 
                              <input id="password" name="password" type="password" class="form-control">
                          </div>
                      </div>
                      <div class="col-6">
                          <div class="form-group label-floating">
                            <div id="erPassConfirm" class="alert" style="display: none;">
                                    <?php print_string('passwordsame') ?></div>
                              <label class="control-label">Xác nhận thay đổi mật khẩu</label>
                              <input id="confirm_password" name="confirm_password" type="password" class="form-control">
                          </div>
                      </div>

                      <div class="col-6">
                          <div class="form-group label-floating">
                              <label class="control-label">Tên</label>
                              <div id="erFirst" class="alert alert-danger" style="display: none;">
                                    <?php print_string('pleasefirst') ?></div>
                              <input type="text" id="firstname" name="firstname" class="form-control" value="<?php echo $user->firstname ?>">
                          </div>
                      </div>
                      <div class="col-6">
                          <div class="form-group label-floating">
                              <label class="control-label">Họ</label>
                              <div id="erLast" class="alert alert-danger" style="display: none;">
                                    <?php print_string('pleaselast') ?>  
                              </div>
                              <input type="text" id="lastname" name="lastname" class="form-control" value="<?php echo $user->lastname ?>">
                          </div>
                      </div>

                      <div class="col-4">
                          <div class="form-group label-floating">
                              <label class="control-label">Địa chỉ</label>
                              <input type="text" name="address" class="form-control" value="<?php echo $user->address ?>">
                          </div>
                      </div>

                      <div class="col-4">
                          <div class="form-group label-floating">
                              <label class="control-label">Thành phố</label>
                              <input type="text" name="city" class="form-control" value="<?php echo $user->city ?>">
                          </div>
                      </div>
                      <div class="col-4">
                          <div class="form-group label-floating">
                              <label class="control-label">Quốc gia</label>
                              <select name="country" id="country" class="form-control">
                                    <option value="AF" <?php echo $user->country == 'AF' ? 'selected="selected"' : ''; ?>>Afghanistan</option>
                                    <option value="AL" <?php echo $user->country == 'AL' ? 'selected="selected"' : ''; ?>>Albania</option>
                                    <option value="DZ" <?php echo $user->country == 'DZ' ? 'selected="selected"' : ''; ?>>Algeria</option>
                                    <option value="AS" <?php echo $user->country == 'AS' ? 'selected="selected"' : ''; ?>>American Samoa</option>
                                    <option value="AD" <?php echo $user->country == 'AD' ? 'selected="selected"' : ''; ?>>Andorra</option>
                                    <option value="AO" <?php echo $user->country == 'AO' ? 'selected="selected"' : ''; ?>>Angola</option>
                                    <option value="AI" <?php echo $user->country == 'AI' ? 'selected="selected"' : ''; ?>>Anguilla</option>
                                    <option value="AQ" <?php echo $user->country == 'AQ' ? 'selected="selected"' : ''; ?>>Antarctica</option>
                                    <option value="AG" <?php echo $user->country == 'AG' ? 'selected="selected"' : ''; ?>>Antigua and Barbuda</option>
                                    <option value="AR" <?php echo $user->country == 'AR' ? 'selected="selected"' : ''; ?>>Argentina</option>
                                    <option value="AM" <?php echo $user->country == 'AM' ? 'selected="selected"' : ''; ?>>Armenia</option>
                                    <option value="AW" <?php echo $user->country == 'AW' ? 'selected="selected"' : ''; ?>>Aruba</option>
                                    <option value="AU" <?php echo $user->country == 'AU' ? 'selected="selected"' : ''; ?>>Australia</option>
                                    <option value="AT" <?php echo $user->country == 'AT' ? 'selected="selected"' : ''; ?>>Austria</option>
                                    <option value="AZ" <?php echo $user->country == 'AZ' ? 'selected="selected"' : ''; ?>>Azerbaijan</option>
                                    <option value="BS" <?php echo $user->country == 'BS' ? 'selected="selected"' : ''; ?>>Bahamas</option>
                                    <option value="BH" <?php echo $user->country == 'BH' ? 'selected="selected"' : ''; ?>>Bahrain</option>
                                    <option value="BD" <?php echo $user->country == 'BD' ? 'selected="selected"' : ''; ?>>Bangladesh</option>
                                    <option value="BB" <?php echo $user->country == 'BB' ? 'selected="selected"' : ''; ?>>Barbados</option>
                                    <option value="BY" <?php echo $user->country == 'BY' ? 'selected="selected"' : ''; ?>>Belarus</option>
                                    <option value="BE" <?php echo $user->country == 'BE' ? 'selected="selected"' : ''; ?>>Belgium</option>
                                    <option value="BZ" <?php echo $user->country == 'BZ' ? 'selected="selected"' : ''; ?>>Belize</option>
                                    <option value="BJ" <?php echo $user->country == 'BJ' ? 'selected="selected"' : ''; ?>>Benin</option>
                                    <option value="BM" <?php echo $user->country == 'BM' ? 'selected="selected"' : ''; ?>>Bermuda</option>
                                    <option value="BT" <?php echo $user->country == 'BT' ? 'selected="selected"' : ''; ?>>Bhutan</option>
                                    <option value="BO" <?php echo $user->country == 'BO' ? 'selected="selected"' : ''; ?>>Bolivia</option>
                                    <option value="BA" <?php echo $user->country == 'BA' ? 'selected="selected"' : ''; ?>>Bosnia and Herzegovina</option>
                                    <option value="BW" <?php echo $user->country == 'BW' ? 'selected="selected"' : ''; ?>>Botswana</option>
                                    <option value="BV" <?php echo $user->country == 'BV' ? 'selected="selected"' : ''; ?>>Bouvet Island</option>
                                    <option value="BR" <?php echo $user->country == 'BR' ? 'selected="selected"' : ''; ?>>Brazil</option>
                                    <option value="BN" <?php echo $user->country == 'BN' ? 'selected="selected"' : ''; ?>>Brunei Darussalam</option>
                                    <option value="BG" <?php echo $user->country == 'BG' ? 'selected="selected"' : ''; ?>>Bulgaria</option>
                                    <option value="BF" <?php echo $user->country == 'BF' ? 'selected="selected"' : ''; ?>>Burkina Faso</option>
                                    <option value="BI" <?php echo $user->country == 'BI' ? 'selected="selected"' : ''; ?>>Burundi</option>
                                    <option value="KH" <?php echo $user->country == 'KH' ? 'selected="selected"' : ''; ?>>Cambodia</option>
                                    <option value="CM" <?php echo $user->country == 'CM' ? 'selected="selected"' : ''; ?>>Cameroon</option>
                                    <option value="CA" <?php echo $user->country == 'CA' ? 'selected="selected"' : ''; ?>>Canada</option>
                                    <option value="CV" <?php echo $user->country == 'CV' ? 'selected="selected"' : ''; ?>>Cape Verde</option>
                                    <option value="KY" <?php echo $user->country == 'KY' ? 'selected="selected"' : ''; ?>>Cayman Islands</option>
                                    <option value="CF" <?php echo $user->country == 'CF' ? 'selected="selected"' : ''; ?>>Central African Republic</option>
                                    <option value="TD" <?php echo $user->country == 'TD' ? 'selected="selected"' : ''; ?>>Chad</option>
                                    <option value="CL" <?php echo $user->country == 'CL' ? 'selected="selected"' : ''; ?>>Chile</option>
                                    <option value="CN" <?php echo $user->country == 'CN' ? 'selected="selected"' : ''; ?>>China</option>
                                    <option value="CX" <?php echo $user->country == 'CX' ? 'selected="selected"' : ''; ?>>Christmas Island</option>
                                    <option value="CC" <?php echo $user->country == 'CC' ? 'selected="selected"' : ''; ?>>Cocos Islands</option>
                                    <option value="CO" <?php echo $user->country == 'CO' ? 'selected="selected"' : ''; ?>>Colombia</option>
                                    <option value="KM" <?php echo $user->country == 'KM' ? 'selected="selected"' : ''; ?>>Comoros</option>
                                    <option value="CD" <?php echo $user->country == 'CD' ? 'selected="selected"' : ''; ?>>Congo</option>
                                    <option value="CG" <?php echo $user->country == 'CG' ? 'selected="selected"' : ''; ?>>Congo</option>
                                    <option value="CK" <?php echo $user->country == 'CK' ? 'selected="selected"' : ''; ?>>Cook Islands</option>
                                    <option value="CR" <?php echo $user->country == 'CR' ? 'selected="selected"' : ''; ?>>Costa Rica</option>
                                    <option value="CI" <?php echo $user->country == 'CI' ? 'selected="selected"' : ''; ?>>Cote D'Ivoire</option>
                                    <option value="HR" <?php echo $user->country == 'HR' ? 'selected="selected"' : ''; ?>>Croatia</option>
                                    <option value="CU" <?php echo $user->country == 'CU' ? 'selected="selected"' : ''; ?>>Cuba</option>
                                    <option value="CY" <?php echo $user->country == 'CY' ? 'selected="selected"' : ''; ?>>Cyprus</option>
                                    <option value="CZ" <?php echo $user->country == 'CZ' ? 'selected="selected"' : ''; ?>>Czech Republic</option>
                                    <option value="DK" <?php echo $user->country == 'DK' ? 'selected="selected"' : ''; ?>>Denmark</option>
                                    <option value="DJ" <?php echo $user->country == 'DJ' ? 'selected="selected"' : ''; ?>>Djibouti</option>
                                    <option value="DM" <?php echo $user->country == 'DM' ? 'selected="selected"' : ''; ?>>Dominica</option>
                                    <option value="DO" <?php echo $user->country == 'DO' ? 'selected="selected"' : ''; ?>>Dominican Republic</option>
                                    <option value="EC" <?php echo $user->country == 'EC' ? 'selected="selected"' : ''; ?>>Ecuador</option>
                                    <option value="EG" <?php echo $user->country == 'EG' ? 'selected="selected"' : ''; ?>>Egypt</option>
                                    <option value="SV" <?php echo $user->country == 'SV' ? 'selected="selected"' : ''; ?>>El Salvador</option>
                                    <option value="GQ" <?php echo $user->country == 'GQ' ? 'selected="selected"' : ''; ?>>Equatorial Guinea</option>
                                    <option value="ER" <?php echo $user->country == 'ER' ? 'selected="selected"' : ''; ?>>Eritrea</option>
                                    <option value="EE" <?php echo $user->country == 'EE' ? 'selected="selected"' : ''; ?>>Estonia</option>
                                    <option value="ET" <?php echo $user->country == 'ET' ? 'selected="selected"' : ''; ?>>Ethiopia</option>
                                    <option value="FK" <?php echo $user->country == 'FK' ? 'selected="selected"' : ''; ?>>Falkland Islands</option>
                                    <option value="FO" <?php echo $user->country == 'FO' ? 'selected="selected"' : ''; ?>>Faroe Islands</option>
                                    <option value="FJ" <?php echo $user->country == 'FJ' ? 'selected="selected"' : ''; ?>>Fiji</option>
                                    <option value="FI" <?php echo $user->country == 'FI' ? 'selected="selected"' : ''; ?>>Finland</option>
                                    <option value="FR" <?php echo $user->country == 'FR' ? 'selected="selected"' : ''; ?>>France</option>
                                    <option value="GF" <?php echo $user->country == 'GF' ? 'selected="selected"' : ''; ?>>French Guiana</option>
                                    <option value="PF" <?php echo $user->country == 'PF' ? 'selected="selected"' : ''; ?>>French Polynesia</option>
                                    <option value="GA" <?php echo $user->country == 'GA' ? 'selected="selected"' : ''; ?>>Gabon</option>
                                    <option value="GM" <?php echo $user->country == 'GM' ? 'selected="selected"' : ''; ?>>Gambia</option>
                                    <option value="GE" <?php echo $user->country == 'GE' ? 'selected="selected"' : ''; ?>>Georgia</option>
                                    <option value="DE" <?php echo $user->country == 'DE' ? 'selected="selected"' : ''; ?>>Germany</option>
                                    <option value="GH" <?php echo $user->country == 'GH' ? 'selected="selected"' : ''; ?>>Ghana</option>
                                    <option value="GI" <?php echo $user->country == 'GI' ? 'selected="selected"' : ''; ?>>Gibraltar</option>
                                    <option value="GR" <?php echo $user->country == 'GR' ? 'selected="selected"' : ''; ?>>Greece</option>
                                    <option value="GL" <?php echo $user->country == 'GL' ? 'selected="selected"' : ''; ?>>Greenland</option>
                                    <option value="GD" <?php echo $user->country == 'GD' ? 'selected="selected"' : ''; ?>>Grenada</option>
                                    <option value="GP" <?php echo $user->country == 'GP' ? 'selected="selected"' : ''; ?>>Guadeloupe</option>
                                    <option value="GU" <?php echo $user->country == 'GU' ? 'selected="selected"' : ''; ?>>Guam</option>
                                    <option value="GT" <?php echo $user->country == 'GT' ? 'selected="selected"' : ''; ?>>Guatemala</option>
                                    <option value="GN" <?php echo $user->country == 'GN' ? 'selected="selected"' : ''; ?>>Guinea</option>
                                    <option value="GW" <?php echo $user->country == 'GW' ? 'selected="selected"' : ''; ?>>Guinea-Bissau</option>
                                    <option value="GY" <?php echo $user->country == 'GY' ? 'selected="selected"' : ''; ?>>Guyana</option>
                                    <option value="HT" <?php echo $user->country == 'HT' ? 'selected="selected"' : ''; ?>>Haiti</option>
                                    <option value="HN" <?php echo $user->country == 'HN' ? 'selected="selected"' : ''; ?>>Honduras</option>
                                    <option value="HK" <?php echo $user->country == 'HK' ? 'selected="selected"' : ''; ?>>Hong Kong</option>
                                    <option value="HU" <?php echo $user->country == 'HU' ? 'selected="selected"' : ''; ?>>Hungary</option>
                                    <option value="IS" <?php echo $user->country == 'IS' ? 'selected="selected"' : ''; ?>>Iceland</option>
                                    <option value="IN" <?php echo $user->country == 'IN' ? 'selected="selected"' : ''; ?>>India</option>
                                    <option value="ID" <?php echo $user->country == 'ID' ? 'selected="selected"' : ''; ?>>Indonesia</option>
                                    <option value="IR" <?php echo $user->country == 'IR' ? 'selected="selected"' : ''; ?>>Iran</option>
                                    <option value="IQ" <?php echo $user->country == 'IQ' ? 'selected="selected"' : ''; ?>>Iraq</option>
                                    <option value="IE" <?php echo $user->country == 'IE' ? 'selected="selected"' : ''; ?>>Ireland</option>
                                    <option value="IL" <?php echo $user->country == 'IL' ? 'selected="selected"' : ''; ?>>Israel</option>
                                    <option value="IT" <?php echo $user->country == 'IT' ? 'selected="selected"' : ''; ?>>Italy</option>
                                    <option value="JM" <?php echo $user->country == 'JM' ? 'selected="selected"' : ''; ?>>Jamaica</option>
                                    <option value="JP" <?php echo $user->country == 'JP' ? 'selected="selected"' : ''; ?>>Japan</option>
                                    <option value="JO" <?php echo $user->country == 'JO' ? 'selected="selected"' : ''; ?>>Jordan</option>
                                    <option value="KZ" <?php echo $user->country == 'KZ' ? 'selected="selected"' : ''; ?>>Kazakhstan</option>
                                    <option value="KE" <?php echo $user->country == 'KE' ? 'selected="selected"' : ''; ?>>Kenya</option>
                                    <option value="KI" <?php echo $user->country == 'KI' ? 'selected="selected"' : ''; ?>>Kiribati</option>
                                    <option value="KR" <?php echo $user->country == 'KR' ? 'selected="selected"' : ''; ?>>Korea, North</option>
                                    <option value="KP <?php echo $user->country == 'KP' ? 'selected="selected"' : ''; ?>">Korea, South</option>
                                    <option value="KW" <?php echo $user->country == 'KW' ? 'selected="selected"' : ''; ?>>Kuwait</option>
                                    <option value="KG" <?php echo $user->country == 'KG' ? 'selected="selected"' : ''; ?>>Kyrgyzstan</option>
                                    <option value="LA" <?php echo $user->country == 'LA' ? 'selected="selected"' : ''; ?>>Lao</option>
                                    <option value="LV" <?php echo $user->country == 'LV' ? 'selected="selected"' : ''; ?>>Latvia</option>
                                    <option value="LB" <?php echo $user->country == 'LB' ? 'selected="selected"' : ''; ?>>Lebanon</option>
                                    <option value="LS" <?php echo $user->country == 'LS' ? 'selected="selected"' : ''; ?>>Lesotho</option>
                                    <option value="LR" <?php echo $user->country == 'LR' ? 'selected="selected"' : ''; ?>>Liberia</option>
                                    <option value="LY" <?php echo $user->country == 'LY' ? 'selected="selected"' : ''; ?>>Libyan Arab Jamahiriya</option>
                                    <option value="LI" <?php echo $user->country == 'LI' ? 'selected="selected"' : ''; ?>>Liechtenstein</option>
                                    <option value="LT" <?php echo $user->country == 'LT' ? 'selected="selected"' : ''; ?>>Lithuania</option>
                                    <option value="LU" <?php echo $user->country == 'LU' ? 'selected="selected"' : ''; ?>>Luxembourg</option>
                                    <option value="MO" <?php echo $user->country == 'MO' ? 'selected="selected"' : ''; ?>>Macao</option>
                                    <option value="MK" <?php echo $user->country == 'MK' ? 'selected="selected"' : ''; ?>>Macedonia</option>
                                    <option value="MG" <?php echo $user->country == 'MG' ? 'selected="selected"' : ''; ?>>Madagascar</option>
                                    <option value="MW" <?php echo $user->country == 'MW' ? 'selected="selected"' : ''; ?>>Malawi</option>
                                    <option value="MY" <?php echo $user->country == 'MY' ? 'selected="selected"' : ''; ?>>Malaysia</option>
                                    <option value="MV" <?php echo $user->country == 'MV' ? 'selected="selected"' : ''; ?>>Maldives</option>
                                    <option value="ML" <?php echo $user->country == 'ML' ? 'selected="selected"' : ''; ?>>Mali</option>
                                    <option value="MT" <?php echo $user->country == 'MT' ? 'selected="selected"' : ''; ?>>Malta</option>
                                    <option value="MH" <?php echo $user->country == 'MH' ? 'selected="selected"' : ''; ?>>Marshall Islands</option>
                                    <option value="MQ" <?php echo $user->country == 'MQ' ? 'selected="selected"' : ''; ?>>Martinique</option>
                                    <option value="MR" <?php echo $user->country == 'MR' ? 'selected="selected"' : ''; ?>>Mauritania</option>
                                    <option value="MU" <?php echo $user->country == 'MU' ? 'selected="selected"' : ''; ?>>Mauritius</option>
                                    <option value="YT" <?php echo $user->country == 'YT' ? 'selected="selected"' : ''; ?>>Mayotte</option>
                                    <option value="MX" <?php echo $user->country == 'MX' ? 'selected="selected"' : ''; ?>>Mexico</option>
                                    <option value="FM" <?php echo $user->country == 'FM' ? 'selected="selected"' : ''; ?>>Micronesia</option>
                                    <option value="MD" <?php echo $user->country == 'MD' ? 'selected="selected"' : ''; ?>>Moldova</option>
                                    <option value="MC" <?php echo $user->country == 'MC' ? 'selected="selected"' : ''; ?>>Monaco</option>
                                    <option value="MN" <?php echo $user->country == 'MN' ? 'selected="selected"' : ''; ?>>Mongolia</option>
                                    <option value="MS" <?php echo $user->country == 'MS' ? 'selected="selected"' : ''; ?>>Montserrat</option>
                                    <option value="MA" <?php echo $user->country == 'MA' ? 'selected="selected"' : ''; ?>>Morocco</option>
                                    <option value="MZ" <?php echo $user->country == 'MZ' ? 'selected="selected"' : ''; ?>>Mozambique</option>
                                    <option value="MM" <?php echo $user->country == 'MM' ? 'selected="selected"' : ''; ?>>Myanmar</option>
                                    <option value="NA" <?php echo $user->country == 'NA' ? 'selected="selected"' : ''; ?>>Namibia</option>
                                    <option value="NR" <?php echo $user->country == 'NR' ? 'selected="selected"' : ''; ?>>Nauru</option>
                                    <option value="NP" <?php echo $user->country == 'NP' ? 'selected="selected"' : ''; ?>>Nepal</option>
                                    <option value="NL" <?php echo $user->country == 'NL' ? 'selected="selected"' : ''; ?>>Netherlands</option>
                                    <option value="AN" <?php echo $user->country == 'AN' ? 'selected="selected"' : ''; ?>>Netherlands Antilles</option>
                                    <option value="NC" <?php echo $user->country == 'NC' ? 'selected="selected"' : ''; ?>>New Caledonia</option>
                                    <option value="NZ" <?php echo $user->country == 'NZ' ? 'selected="selected"' : ''; ?>>New Zealand</option>
                                    <option value="NI" <?php echo $user->country == 'NI' ? 'selected="selected"' : ''; ?>>Nicaragua</option>
                                    <option value="NE" <?php echo $user->country == 'NE' ? 'selected="selected"' : ''; ?>>Niger</option>
                                    <option value="NG" <?php echo $user->country == 'NG' ? 'selected="selected"' : ''; ?>>Nigeria</option>
                                    <option value="NU" <?php echo $user->country == 'NU' ? 'selected="selected"' : ''; ?>>Niue</option>
                                    <option value="NF" <?php echo $user->country == 'NF' ? 'selected="selected"' : ''; ?>>Norfolk Island</option>
                                    <option value="MP" <?php echo $user->country == 'MP' ? 'selected="selected"' : ''; ?>>Northern Mariana Islands</option>
                                    <option value="NO" <?php echo $user->country == 'NO' ? 'selected="selected"' : ''; ?>>Norway</option>
                                    <option value="OM" <?php echo $user->country == 'OM' ? 'selected="selected"' : ''; ?>>Oman</option>
                                    <option value="PK" <?php echo $user->country == 'PK' ? 'selected="selected"' : ''; ?>>Pakistan</option>
                                    <option value="PW" <?php echo $user->country == 'PW' ? 'selected="selected"' : ''; ?>>Palau</option>
                                    <option value="PS" <?php echo $user->country == 'PS' ? 'selected="selected"' : ''; ?>>Palestinian</option>
                                    <option value="PA" <?php echo $user->country == 'PA' ? 'selected="selected"' : ''; ?>>Panama</option>
                                    <option value="PG" <?php echo $user->country == 'PG' ? 'selected="selected"' : ''; ?>>Papua New Guinea</option>
                                    <option value="PY" <?php echo $user->country == 'PY' ? 'selected="selected"' : ''; ?>>Paraguay</option>
                                    <option value="PE" <?php echo $user->country == 'PE' ? 'selected="selected"' : ''; ?>>Peru</option>
                                    <option value="PH" <?php echo $user->country == 'PH' ? 'selected="selected"' : ''; ?>>Philippines</option>
                                    <option value="PN" <?php echo $user->country == 'PN' ? 'selected="selected"' : ''; ?>>Pitcairn</option>
                                    <option value="PL" <?php echo $user->country == 'PL' ? 'selected="selected"' : ''; ?>>Poland</option>
                                    <option value="PT" <?php echo $user->country == 'PT' ? 'selected="selected"' : ''; ?>>Portugal</option>
                                    <option value="PR" <?php echo $user->country == 'PR' ? 'selected="selected"' : ''; ?>>Puerto Rico</option>
                                    <option value="QA" <?php echo $user->country == 'QA' ? 'selected="selected"' : ''; ?>>Qatar</option>
                                    <option value="RE" <?php echo $user->country == 'RE' ? 'selected="selected"' : ''; ?>>Reunion</option>
                                    <option value="RO" <?php echo $user->country == 'RO' ? 'selected="selected"' : ''; ?>>Romania</option>
                                    <option value="RU" <?php echo $user->country == 'RU' ? 'selected="selected"' : ''; ?>>Russian Federation</option>
                                    <option value="RW" <?php echo $user->country == 'RW' ? 'selected="selected"' : ''; ?>>Rwanda</option>
                                    <option value="WS" <?php echo $user->country == 'WS' ? 'selected="selected"' : ''; ?>>Samoa</option>
                                    <option value="SM" <?php echo $user->country == 'SM' ? 'selected="selected"' : ''; ?>>San Marino</option>
                                    <option value="ST" <?php echo $user->country == 'ST' ? 'selected="selected"' : ''; ?>>Sao Tome and Principe</option>
                                    <option value="SA" <?php echo $user->country == 'SA' ? 'selected="selected"' : ''; ?>>Saudi Arabia</option>
                                    <option value="SN" <?php echo $user->country == 'SN' ? 'selected="selected"' : ''; ?>>Senegal</option>
                                    <option value="CS" <?php echo $user->country == 'CS' ? 'selected="selected"' : ''; ?>>Serbia and Montenegro</option>
                                    <option value="SC" <?php echo $user->country == 'SC' ? 'selected="selected"' : ''; ?>>Seychelles</option>
                                    <option value="SL" <?php echo $user->country == 'SL' ? 'selected="selected"' : ''; ?>>Sierra Leone</option>
                                    <option value="SG" <?php echo $user->country == 'SG' ? 'selected="selected"' : ''; ?>>Singapore</option>
                                    <option value="SK" <?php echo $user->country == 'SK' ? 'selected="selected"' : ''; ?>>Slovakia</option>
                                    <option value="SI" <?php echo $user->country == 'SI' ? 'selected="selected"' : ''; ?>>Slovenia</option>
                                    <option value="SB" <?php echo $user->country == 'SB' ? 'selected="selected"' : ''; ?>>Solomon Islands</option>
                                    <option value="SO" <?php echo $user->country == 'SO' ? 'selected="selected"' : ''; ?>>Somalia</option>
                                    <option value="ZA" <?php echo $user->country == 'ZA' ? 'selected="selected"' : ''; ?>>South Africa</option>
                                    <option value="GS" <?php echo $user->country == 'GS' ? 'selected="selected"' : ''; ?>>South Georgia</option>
                                    <option value="ES" <?php echo $user->country == 'ES' ? 'selected="selected"' : ''; ?>>Spain</option>
                                    <option value="LK" <?php echo $user->country == 'LK' ? 'selected="selected"' : ''; ?>>Sri Lanka</option>
                                    <option value="SH" <?php echo $user->country == 'SH' ? 'selected="selected"' : ''; ?>>St. Helena</option>
                                    <option value="KN" <?php echo $user->country == 'KN' ? 'selected="selected"' : ''; ?>>St. Kitts and Nevis</option>
                                    <option value="LC" <?php echo $user->country == 'LC' ? 'selected="selected"' : ''; ?>>St. Lucia</option>
                                    <option value="PM" <?php echo $user->country == 'PM' ? 'selected="selected"' : ''; ?>>St. Pierre and Miquelon</option>
                                    <option value="VC" <?php echo $user->country == 'VC' ? 'selected="selected"' : ''; ?>>St. Vincent</option>
                                    <option value="SD" <?php echo $user->country == 'SD' ? 'selected="selected"' : ''; ?>>Sudan</option>
                                    <option value="SR" <?php echo $user->country == 'SR' ? 'selected="selected"' : ''; ?>>Suriname</option>
                                    <option value="SJ" <?php echo $user->country == 'SJ' ? 'selected="selected"' : ''; ?>>Svalbard and Jan Mayen</option>
                                    <option value="SZ" <?php echo $user->country == 'SZ' ? 'selected="selected"' : ''; ?>>Swaziland</option>
                                    <option value="SE" <?php echo $user->country == 'SE' ? 'selected="selected"' : ''; ?>>Sweden</option>
                                    <option value="CH" <?php echo $user->country == 'CH' ? 'selected="selected"' : ''; ?>>Switzerland</option>
                                    <option value="SY" <?php echo $user->country == 'SY' ? 'selected="selected"' : ''; ?>>Syrian Arab Republic</option>
                                    <option value="TW" <?php echo $user->country == 'TW' ? 'selected="selected"' : ''; ?>>Taiwan</option>
                                    <option value="TJ" <?php echo $user->country == 'TJ' ? 'selected="selected"' : ''; ?>>Tajikistan</option>
                                    <option value="TZ" <?php echo $user->country == 'TZ' ? 'selected="selected"' : ''; ?>>Tanzania</option>
                                    <option value="TH" <?php echo $user->country == 'TH' ? 'selected="selected"' : ''; ?>>Thailand</option>
                                    <option value="TL" <?php echo $user->country == 'TL' ? 'selected="selected"' : ''; ?>>Timor-Leste</option>
                                    <option value="TG" <?php echo $user->country == 'TG' ? 'selected="selected"' : ''; ?>>Togo</option>
                                    <option value="TK" <?php echo $user->country == 'TK' ? 'selected="selected"' : ''; ?>>Tokelau</option>
                                    <option value="TO" <?php echo $user->country == 'TO' ? 'selected="selected"' : ''; ?>>Tonga</option>
                                    <option value="TT" <?php echo $user->country == 'TT' ? 'selected="selected"' : ''; ?>>Trinidad and Tobago</option>
                                    <option value="TN" <?php echo $user->country == 'TN' ? 'selected="selected"' : ''; ?>>Tunisia</option>
                                    <option value="TR" <?php echo $user->country == 'TR' ? 'selected="selected"' : ''; ?>>Turkey</option>
                                    <option value="TM" <?php echo $user->country == 'TM' ? 'selected="selected"' : ''; ?>>Turkmenistan</option>
                                    <option value="TC" <?php echo $user->country == 'TC' ? 'selected="selected"' : ''; ?>>Turks and Caicos Islands</option>
                                    <option value="TV" <?php echo $user->country == 'TV' ? 'selected="selected"' : ''; ?>>Tuvalu</option>
                                    <option value="UG" <?php echo $user->country == 'UG' ? 'selected="selected"' : ''; ?>>Uganda</option>
                                    <option value="UA" <?php echo $user->country == 'UA' ? 'selected="selected"' : ''; ?>>Ukraine</option>
                                    <option value="AE" <?php echo $user->country == 'AE' ? 'selected="selected"' : ''; ?>>United Arab Emirates</option>
                                    <option value="UK" <?php echo $user->country == 'UK' ? 'selected="selected"' : ''; ?>>United Kingdom</option>
                                    <option value="US" <?php echo $user->country == 'US' ? 'selected="selected"' : ''; ?>>United States</option>
                                    <option value="UY" <?php echo $user->country == 'UY' ? 'selected="selected"' : ''; ?>>Uruguay</option>
                                    <option value="UZ" <?php echo $user->country == 'UZ' ? 'selected="selected"' : ''; ?>>Uzbekistan</option>
                                    <option value="VU" <?php echo $user->country == 'VU' ? 'selected="selected"' : ''; ?>>Vanuatu</option>
                                    <option value="VE" <?php echo $user->country == 'VE' ? 'selected="selected"' : ''; ?>>Venezuela</option>
                                    <option value="VN" <?php echo $user->country == 'VN' ? 'selected="selected"' : ''; ?>>Viet Nam</option>
                                    <option value="VG" <?php echo $user->country == 'VG' ? 'selected="selected"' : ''; ?>>Virgin Islands, British</option>
                                    <option value="VI" <?php echo $user->country == 'VI' ? 'selected="selected"' : ''; ?>>Virgin Islands, U.s.</option>
                                    <option value="WF" <?php echo $user->country == 'WF' ? 'selected="selected"' : ''; ?>>Wallis and Futuna</option>
                                    <option value="EH" <?php echo $user->country == 'EH' ? 'selected="selected"' : ''; ?>>Western Sahara</option>
                                    <option value="YE" <?php echo $user->country == 'YE' ? 'selected="selected"' : ''; ?>>Yemen</option>
                                    <option value="ZM" <?php echo $user->country == 'ZM' ? 'selected="selected"' : ''; ?>>Zambia</option>
                                    <option value="ZW" <?php echo $user->country == 'ZW' ? 'selected="selected"' : ''; ?>>Zimbabwe</option>
                              </select>
                          </div>
                      </div>
                      <div class="col-4">
                          <div class="form-group label-floating">
                              <label class="control-label">Mã vùng</label>
                              <input type="text" class="form-control" value="<?php echo $user->postcode ?>" name="postcode">
                          </div>
                      </div>
                      
                  
                      <div class="col-4">
                          <div class="form-group label-floating">
                              <label class="control-label">Số điện thoại cố định</label>
                              <input type="text" name="phone1" number="true" class="form-control" value="<?php echo $user->phone1 ?>">
                          </div>
                      </div>
                      <div class="col-4">
                          <div class="form-group label-floating">
                              <label class="control-label">Số điện thoại di động</label>
                              <input type="text" name="phone2" number="true" class="form-control" value="<?php echo $user->phone2 ?>">
                          </div>
                      </div>

                      <div class="col-6">
                          <div class="form-group label-floating">
                              <label class="control-label">Skype</label>
                              <input type="text" name="skype" class="form-control" value="<?php echo $user->skype ?>">
                          </div>
                      </div>
                      <div class="col-6">
                          <div class="form-group label-floating">
                              <label class="control-label">Website</label>
                              <input type="text" url="true" name="website" class="form-control" value="<?php echo $user->website ?>">
                          </div>
                      </div>
                      <div class="col-auto">
                        <button type="submit" class="btn btn-success pull-right">Cập Nhật</button>
                      </div>
              </form>
          </div>
      </div>
  </div>
  <div class="col-md-2">
      <div class="card-box">
          <div class="menu-right">
            <h4>Chức năng liên quan</h4>
          </div>
          <div class="card-content">
                <style type="text/css" media="screen">
                  .card-content ul>li>a{color:#4ba64f!important;}
                  /*.card-content ul>li>a:hover{background: #3C4858; border-radius: 3px}*/
                </style>
                <ul class="nav">
                  <li class="active">
                      <a href="./dashboard.html">
                          <p> Bảng điều khiển 1 </p>
                      </a>
                  </li>
                  <li class="active">
                      <a href="./dashboard.html">
                          <p> Bảng điều khiển 2 </p>
                      </a>
                  </li>
                  <li class="active">
                      <a href="./dashboard.html">
                          <p> Bảng điều khiển 3 </p>
                      </a>
                  </li>
                  <li>
                      <a data-toggle="collapse" href="#menu1">
                          <p> Pages
                              <b class="caret"></b>
                          </p>
                      </a>
                      <div class="collapse" id="menu1">
                          <ul class="nav">
                              <li>
                                  <a href="./pages/pricing.html">
                                      <span class="sidebar-mini"> P </span>
                                      <span class="sidebar-normal"> Pricing </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="./pages/rtl.html">
                                      <span class="sidebar-mini"> RS </span>
                                      <span class="sidebar-normal"> RTL Support </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="./pages/timeline.html">
                                      <span class="sidebar-mini"> T </span>
                                      <span class="sidebar-normal"> Timeline </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="./pages/login.html">
                                      <span class="sidebar-mini"> LP </span>
                                      <span class="sidebar-normal"> Login Page </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="./pages/register.html">
                                      <span class="sidebar-mini"> RP </span>
                                      <span class="sidebar-normal"> Register Page </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="./pages/lock.html">
                                      <span class="sidebar-mini"> LSP </span>
                                      <span class="sidebar-normal"> Lock Screen Page </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="./pages/user.html">
                                      <span class="sidebar-mini"> UP </span>
                                      <span class="sidebar-normal"> User Profile </span>
                                  </a>
                              </li>
                          </ul>
                      </div>
                  </li>
                  
                </ul>
          </div>
      </div>
  </div>
</div>

<?php
echo $OUTPUT->footer();
?>
