<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');

global $CFG;
require_once($CFG->dirroot . '/common/lib.php');

require_login(0, false);

$PAGE->set_title(get_string('myprofile'));
$PAGE->set_heading(get_string('myprofile'));
global $USER;
//$user_id = optional_param('user_id', '', PARAM_TEXT);    
$user_id = $USER->id;
$action = optional_param('action', '', PARAM_TEXT);
echo $OUTPUT->header();

if ($action == 'upload') {
  if ($_FILES["file"]["name"] != '') {
    if ($_FILES["file"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file"]["name"]));
      if (strtolower($file_extension) == "jpg" || strtolower($file_extension) == "jpeg"
          || strtolower($file_extension) == "gif"
          || strtolower($file_extension) == "png") {
        move_uploaded_file($_FILES["file"]["tmp_name"], $CFG->dirroot . "/my_profile/logo/" . $USER->id . "_" . $_FILES["file"]["name"]);
        update_profile_logo($USER->id, $CFG->wwwroot . "/my_profile/logo/" . $USER->id . "_" . $_FILES["file"]["name"]);
      }
      else {

        //displayJsAlert("File type for logo image must be : .jpg, .jpeg, .gif, .png", '');
      }
    }
  }
  echo displayJsAlert("", $CFG->wwwroot . "/my_profile/index.php?user_id=" . $user_id);
}
if ($action == 'update_profile') {
  $action = optional_param('action', '', PARAM_TEXT);
  $firstname = optional_param('firstname', '', PARAM_TEXT);
  $state = optional_param('state', '', PARAM_TEXT);
  $postcode = optional_param('postcode', '', PARAM_TEXT);
  $website = optional_param('website', '', PARAM_TEXT);
  $lastname = optional_param('lastname', '', PARAM_TEXT);
  $username = optional_param('username', '', PARAM_TEXT);
  $password = optional_param('password', '', PARAM_TEXT);
  $address = optional_param('address', '', PARAM_TEXT);
  $city = optional_param('city', '', PARAM_TEXT);
  $zip = optional_param('zip', '0', PARAM_TEXT);
  $country = optional_param('country', '', PARAM_TEXT);
  $timezone = optional_param('timezone', '', PARAM_TEXT);
  $title = optional_param('title', '', PARAM_TEXT);
  $company = optional_param('company', '', PARAM_TEXT);
  $email = optional_param('email', '', PARAM_TEXT);
  $phone1 = optional_param('phone1', '', PARAM_TEXT);
  $phone2 = optional_param('phone2', '', PARAM_TEXT);
  $skype = optional_param('skype', '', PARAM_TEXT);
  $twitter = optional_param('twitter', '', PARAM_TEXT);
  $website = optional_param('website', '', PARAM_TEXT);
//    var_dump($timezone);
//    die();
  update_my_profile2($user_id, $firstname, $lastname, $username, $password, $address, $city, $state, $postcode, $country, $timezone, $title, $company, $email, $phone1, $phone2, $skype, $twitter, $website);
}
$user = get_user_from_id($user_id);
?>
<script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/common/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<div class="row">
    <div class="col-md-8">
        <div class="card-box">
          <h4 class="m-t-0 header-title"><?php echo $user->firstname ?> <?php echo $user->lastname ?></h4>
          <p class="text-muted m-b-30 font-14"><?php echo time_ago($user->lastlogin); ?></p>
            <div class="card-content">
                <form style="line-height: 4.5em">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- <div class="form-group label-floating"> -->
                                <label><?php print_r(get_string('firstname')) ?></label>
                                <label><?php echo $user->firstname ?></label>
                            <!-- </div> -->
                        </div>
                        <div class="col-md-6">
                            <label><?php print_r(get_string('company')) ?></label>
                            <label><?php echo $user->company ?></label>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><?php print_r(get_string('lastname')) ?></label>
                            <label><?php echo $user->lastname ?></label>                        
                        </div>
                        
                        <div class="col-md-6">
                            <label><?php print_r(get_string('email')) ?>:</label>
                            <label><?php echo $user->email ?></label>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><?php print_r(get_string('city')) ?></label>
                            <label><?php echo $user->city ?></label>  
                        </div>
                        <div class="col-md-6">
                            <label><?php print_r(get_string('workph')) ?></label>
                            <label><?php echo $user->phone1 ?></label>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><?php print_r(get_string('username')) ?>:</label>
                            <label><?php echo $user->username ?></label>
                        </div>
                        <div class="col-md-6">
                            <label><?php print_r(get_string('mobileph')) ?></label>
                            <label><?php echo $user->phone2 ?></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>SKYPE</label>
                            <label><?php echo $user->skype ?> </label>
                        </div>
                        <div class="col-md-6">
                            <label>WEBSITE: </label>
                            <label><?php echo $user->website ?></label>
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-success pull-right"><a href="<?php echo $CFG->wwwroot ?>/my_profile/edit_my_profile.php" style="color: #fff">Chỉnh sửa hồ sơ</a></button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-box text-center">
            <?php if ($user->profile_img != '') { ?>
              <div class="card-avatar">
                <a href="#pablo">
                    <img class="img-fluid" src="<?php echo $user->profile_img; ?>" style="height: 120px" />
                </a>
              </div>
            <?php } ?>
            
            <div class="card-content">
                <h4 class="header-title"><?php echo $user->firstname ?> <?php echo $user->lastname ?></h4>
                <!-- <h4 class="card-title">Alec Thompson</h4> -->
                <p class="description">
                    <?php echo $user->title ?>
                </p>
                <a href="#pablo" class="mid btn btn-success btn-round">Đổi avatar</a>

                <div id="upPhoto" class="side-panel" style="display:none;">
                  <h3><?php print_string('myprofilepicture') ?></h3>
                  <form action="<?php echo $CFG->wwwroot ?>/my_profile/index.php?action=upload" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="user_id" value="<?php echo $user_id ?>"/>
                    <p><?php print_string('selectpic') ?></p>
                    <div class="padded">
                      <input type="file" class="form-control" id="file" name="file">
                      <div class="tip italic padded">
                        <?php print_string('maximum') ?>
                      </div>  
                      <div class="tip padded"><?php print_string('noteprofile') ?></div>  
                    </div>
                    <div class="form-buttons">
                      <input type="submit" value="<?php print_string('upload') ?>" class="btn btn-success btn-round">
                      &nbsp;<?php print_string('or') ?>&nbsp;<a id="cancelUpload" href="#" class="btn btn-round btn-danger"><?php print_string('cancel') ?></a>
                    </div>
                  </form>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
echo $OUTPUT->footer();
?>
