<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);

$enable_ecommerce = optional_param('enable_ecommerce', 0, PARAM_TEXT);


$payment_provider = optional_param('ecommerce_provider_type', '', PARAM_TEXT);
$paypal_email = optional_param('paypal_id', '', PARAM_TEXT);
$payment_username = optional_param('PaymentUsername', '', PARAM_TEXT);
$payment_password = optional_param('payment_password', '', PARAM_TEXT);
$curencies = optional_param('curencies', '', PARAM_TEXT);
$short_profile = optional_param('organisation_profile', '', PARAM_TEXT);
$account_number = optional_param('google_analytics_id', '', PARAM_TEXT);
$user_id = optional_param('user_id', '', PARAM_TEXT);
$account_id = optional_param('id', 2, PARAM_INT);
global $CFG;
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/ecommerce.js"></script>
<script type="text/javascript" src="colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        
  <?php
  if (is_siteadmin()) {
    if ($action == 'update_ecommerce') {
      for ($i = 0; $i <= count($_POST['EcommerceCurrencyCodes']); $i++) {
        if ($i > 0) {
          $curency.= " ";
        }
        $curency .= $_POST['EcommerceCurrencyCodes'][$i];
      }
      update_user_ecommerce($user_id, $enable_ecommerce, $payment_provider, $paypal_email, $payment_username, $payment_password, $curency, $short_profile, $account_number);
      //update_user_currency($user_id, $curencies);
      $SELECT = select_account_id($user_id);
    }
    else {
      $SELECT = select_account_id($account_id);
    }
    foreach ($SELECT as $USE) {
      ?>      
      <table class="admin-fullscreen">
        <tbody><tr>
            <td class="admin-fullscreen-left">
              <div class="admin-fullscreen-content">

                <div class="focus-panel">
                  <div class="panel-head">
                    <div style="float: right">
                      <label> <?php echo $USE->username ?></label>
                    </div>
                    <h3>
                      <?php print_string('account') ?>
                    </h3>
                  </div>
                  <div class="body">
                    <div class="grid-tabs">
                      <ul>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class=""><?php print_string('billing') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('ecommerce') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>
                      </ul>
                      <div class="clear">
                      </div>
                    </div>

                    <form method="POST" action="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USE->id ?>&action=update_ecommerce">


                      <input type="hidden" value="<?php echo $USE->id ?>" name="user_id" id="user_id">
                      <input type="hidden" value="<?php echo $curency ?>" name="curencies">
                      <p style="margin-left: 10px">
                        By enabling the ecommerce features in Alphatechnologies you will see an "I want to sell
                        this course" section appear in the "Course Settings" area for each of your
                        courses.
                      </p>
                      <p style="margin-left: 10px">
                        You can set set a price for each course that you want to sell and accept hands-free
                        signup &amp; payment for your courses.</p>
                      <div style="margin-left: 10px">
                        <input type="checkbox" name="enable_ecommerce" value="1" id="enable_ecommerce" <?php echo $USE->enable_ecommerce != 0 ? "checked='checked'" : "" ?>>
                        <b>Enable ecommerce features</b></div>

                      <div class="form-sub-heading" style="margin-left: 10px">
                        Payment Provider: <select name="ecommerce_provider_type" id="ecommerce_provider_type" class="select-hide">
                          <option value="0" <?php echo $USE->ecommerce_provider_type == "0" ? 'selected="selected"' : '' ?>>Select a Provider</option>
                          <option value="1" <?php echo $USE->ecommerce_provider_type == "1" ? 'selected="selected"' : '' ?>>Paypal</option>
                          <option value="2" <?php echo $USE->ecommerce_provider_type == "2" ? 'selected="selected"' : '' ?>>Payment Express</option>
                        </select>
                      </div>
                      <div class="div-option option-1" style="display: block;">
                        <p style="margin-left: 10px">
                          If you want to accept payment for your courses you will need a PayPal business account.
                          If you don't have a PayPal business account you can <a href="https://www.paypal.com/nz/mrb/pal=S8RNBCYYZQ5CG" target="_blank">
                            sign up for one here</a></p>
                        <table class="simple-form">
                          <tbody><tr>
                              <th>
                                PayPal Email:
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->paypal_id ?>" size="50" name="paypal_id" maxlength="100" id="paypal_id">
                              </td>
                            </tr>    

                          </tbody></table>
                      </div>
                      <div class="div-option option-2" style="display: none;">
                        <p style="margin-left: 10px">
                          If you want to accept payment for your courses then you will need to create an account with Payment Express.
                          If you don't have a merchant account and Payment Express username/ password you can <a href="https://sec.paymentexpress.com/pxmi/apply" target="_blank">
                            apply here</a></p>
                        <table class="simple-form">
                          <tbody><tr>
                              <th>
                                <?php print_r(get_string('username')) ?>
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->payment_username ?>" size="50" name="PaymentUsername" maxlength="100" id="payment_username">
                              </td>
                            </tr>    
                            <tr>
                              <th>
                                Password:
                              </th>
                              <td><input type="password" size="50" name="payment_password" maxlength="100" id="payment_password">
                              </td>
                            </tr>                                                            
                          </tbody></table>
                      </div>
                      <div class="div-option option-1 option-2" style="display: block;margin-left: 10px">
                        <p>When selecting currencies to sell your courses please ensure that your merchant account is setup to allow these currencies.</p>
                        <table class="simple-form">
                          <tbody><tr>
                              <th style="text-align:left;width: 4%;">
                                Currencies:
                              </th>
                              <td style="text-align:left;width: 21%;">
                                <div class="checkbox-list">
                                  <?php
                                  $chk = $USE->ecommerce_currency_codes;
                                  $chkbox = explode(' ', $chk);
                                  ?>
                                  <input type="checkbox" <?php
                              for ($i = 0; $i <= count($chkbox); $i++) {
                                echo $chkbox[$i] == '1' ? 'checked="checked"' : '';
                              }
                              ?> value="1" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>USD</span><br>
                                  <input type="checkbox" <?php
                              for ($i = 0; $i <= count($chkbox); $i++) {
                                echo $chkbox[$i] == '2' ? 'checked="checked"' : '';
                              }
                                  ?> value="2" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>AUD</span><br>
                                  <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '3' ? 'checked="checked"' : '';
                                     }
                                  ?> value="3" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>CAD</span><br>
                                  <input type="checkbox" <?php
                              for ($i = 0; $i <= count($chkbox); $i++) {
                                echo $chkbox[$i] == '4' ? 'checked="checked"' : '';
                              }
                              ?> value="4" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>EUR</span><br>
                                  <input type="checkbox" <?php
                              for ($i = 0; $i <= count($chkbox); $i++) {
                                echo $chkbox[$i] == '5' ? 'checked="checked"' : '';
                              }
                                  ?> value="5" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>GBP</span><br>
                                  <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '6' ? 'checked="checked"' : '';
                                     }
                                     ?> value="6" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>NZD</span><br>
                                  <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '7' ? 'checked="checked"' : '';
                                     }
                                     ?> value="7" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>ZAR</span><br>
                                  <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '8' ? 'checked="checked"' : '';
                                     }
                                     ?> value="8" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>SEK</span><br>


                                </div>
                              </td>
                            </tr>    

                          </tbody></table>
                      </div>
                      <div class="form-sub-heading">
                        Organisation Profile
                      </div>
                      <p style="margin-left: 10px">Provide a short description of your organisation so that people looking your available list of online courses have some idea of who you are.</p>
                      <div style="margin-left:10px">       
                        <table class="simple-form">
                          <tbody><tr>
                              <th style="text-align: left;width: 20%">
                                Short Profile:
                              </th>
                              <td>
                                <textarea style="width:95%;" rows="5" name="organisation_profile" id="organisation_profile" cols="20" class="profile" ><?php echo $USE->organisation_profile ?></textarea>
                                <div class="tip" id="OrganisationProfile_counter"><span>500</span> character(s) left</div>
                              </td>
                            </tr>   
                            <tr>
                              <th>

                              </th>
                              <td>&nbsp;
                              </td>
                            </tr>                                                             
                          </tbody></table>   
                      </div>
                      <div class="form-sub-heading">
                        Google Analytics
                      </div>
                      <p style="margin-left: 10px">Track page views for your courses using your own <a target="_blank" href="http://www.google.com/analytics/">Google Analytics account</a>.</p>
                      <div style="margin-left:10px">  
                        <table class="simple-form">
                          <tbody><tr>
                              <th style="text-align: left;width: 20%">
                                Account Number:
                          <div class="tip">e.g. UA-xxxxxx-x</div>
                          </th>
                          <td>
                            <input type="text" value="<?php echo $USE->google_analytics_id ?>" name="google_analytics_id" id="google_analytics_id">
                          </td>
                          </tr>    
                          <tr>
                            <th>

                            </th>
                            <td>&nbsp;
                            </td>
                          </tr>                                                            
                          </tbody></table>
                      </div>
                      <div class="form-buttons">
                        <input type="submit" name="Save" class="btnlarge" value="Save">
                      </div>                            

                    </form>

                  </div>
                </div>
              </div>
            </td>
            <td class="admin-fullscreen-right">
              <div class="admin-fullscreen-content">

                <div class="side-panel">
                  <h3>Course shopping cart</h3>
                  <div class="padded">
                    <p>
                      The courses that you setup for selling will be visible to the public here:</p>
                  </div>
                  <div class="padded">
                    <p>
                      <a  target="_blank" href="../course/Sale_course/sale_course.php?userid=<?php echo $USE->id ?>">http://<?php echo $USE->custom_website_address ?>/online-course</a>
                    </p>
                  </div>
                </div>
                <div class="side-panel">
                  <h3>Course listings widget</h3>
                  <div id="embedCode">
                    <div class="tip padded">Copy and paste the HTML code below to embed this course list in another blog or website</div>
                    <textarea style="width:100%;" rows="5" class="embed"></textarea>                
                  </div> 
                </div>

              </div>
            </td>
          </tr>
        </tbody></table>

      <?php
    }
  }
  else {
    if ($action == 'update_ecommerce') {
      for ($i = 0; $i <= count($_POST['EcommerceCurrencyCodes']); $i++) {
        if ($i > 0) {
          $curency.= " ";
        }
        $curency .= $_POST['EcommerceCurrencyCodes'][$i];
      }
      update_user_ecommerce($user_id, $enable_ecommerce, $payment_provider, $paypal_email, $payment_username, $payment_password, $curency, $short_profile, $account_number);
      $USER = get_user_from_user_id($user_id);
    }
    else {
      global $USER;
    }
    ?>
    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">
              <div class="focus-panel">
                <div class="panel-head">
                  <h3>
  <?php print_string('account') ?>
                  </h3>
                </div>
                <div class="body">
                  <div class="grid-tabs">
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $USER->id; ?>"><?php print_string('profile') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('theme') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('billing') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USER->id; ?>" class="selected"><?php print_string('ecommerce') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('term') ?></a></li> 
                    </ul>
                    <div class="clear">
                    </div>
                  </div>
                  <form method="post" action="<?php echo$CFG->wwwroot ?>/account/ecommerce.php?action=update_ecommerce">
                    <input type="hidden" value="<?php echo $USER->id ?>" name="user_id" id="user_id">
                    <p style="margin-left: 10px">
                      By enabling the ecommerce features in Alphatechnologies you will see an "I want to sell
                      this course" section appear in the "Course Settings" area for each of your
                      courses.
                    </p>
                    <p style="margin-left: 10px">
                      You can set set a price for each course that you want to sell and accept hands-free
                      signup &amp; payment for your courses.</p>
                    <div style="margin-left: 10px">
                      <input type="checkbox" value="1" name="enable_ecommerce" id="enable_ecommerce" <?php echo $USER->enable_ecommerce != 0 ? 'checked="checked"' : ''; ?>>

                      <b>Enable ecommerce features</b></div>

                    <div class="form-sub-heading" style="margin-left: 10px">
                      Payment Provider: <select name="ecommerce_provider_type" id="ecommerce_provider_type" class="select-hide">
                        <option value="0" <?php echo $USER->ecommerce_provider_type == '0' ? 'selected="selected"' : ''; ?>>Select a Provider</option>
                        <option value="1" <?php echo $USER->ecommerce_provider_type == '1' ? 'selected="selected"' : ''; ?>>Paypal</option>
                        <option value="2" <?php echo $USER->ecommerce_provider_type == '2' ? 'selected="selected"' : ''; ?>>Payment Express</option>
                      </select>
                    </div>
                    <div class="div-option option-1" style="display: block;">
                      <p style="margin-left: 10px">
                        If you want to accept payment for your courses you will need a PayPal business account.
                        If you don't have a PayPal business account you can <a href="https://www.paypal.com/nz/mrb/pal=S8RNBCYYZQ5CG" target="_blank">
                          sign up for one here</a></p>
                      <table class="simple-form">
                        <tbody><tr>
                            <th>
                              PayPal Email:
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->paypal_id ?>" size="50" name="paypal_id" maxlength="100" id="paypal_id">
                            </td>
                          </tr>    

                        </tbody></table>
                    </div>
                    <div class="div-option option-2" style="display: none;">
                      <p style="margin-left: 10px">
                        If you want to accept payment for your courses then you will need to create an account with Payment Express.
                        If you don't have a merchant account and Payment Express username/ password you can <a href="https://sec.paymentexpress.com/pxmi/apply" target="_blank">
                          apply here</a></p>
                      <table class="simple-form">
                        <tbody><tr>
                            <th>
  <?php print_r(get_string('username')) ?>
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->payment_username ?>" size="50" name="PaymentUsername" maxlength="100" id="payment_username">
                            </td>
                          </tr>    
                          <tr>
                            <th>
                              Password:
                            </th>
                            <td><input type="password" size="50" name="payment_password" maxlength="100" id="payment_password">
                            </td>
                          </tr>                                                            
                        </tbody></table>
                    </div>
                    <div class="div-option option-1 option-2" style="display: block;margin-left: 10px">
                      <p>When selecting currencies to sell your courses please ensure that your merchant account is setup to allow these currencies.</p>
                      <table class="simple-form">
                        <tbody><tr>
                            <th style="text-align:left;width: 4%;">
                              Currencies:
                            </th>
                            <td style="text-align:left;width: 21%;">
                              <div class="checkbox-list">
                                <?php
                                $chk = $USER->ecommerce_currency_codes;
                                $chkbox = explode(' ', $chk);
                                ?>
                                <input type="checkbox" <?php
                              for ($i = 0; $i <= count($chkbox); $i++) {
                                echo $chkbox[$i] == '1' ? 'checked="checked"' : '';
                              }
                                ?> value="1" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>USD</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '2' ? 'checked="checked"' : '';
                                     }
                                     ?> value="2" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>AUD</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '3' ? 'checked="checked"' : '';
                                     }
                                     ?> value="3" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>CAD</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '4' ? 'checked="checked"' : '';
                                     }
                                     ?> value="4" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>EUR</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '5' ? 'checked="checked"' : '';
                                     }
                                     ?> value="5" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>GBP</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '6' ? 'checked="checked"' : '';
                                     }
                                     ?> value="6" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>NZD</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '7' ? 'checked="checked"' : '';
                                     }
                                     ?> value="7" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>ZAR</span><br>
                                <input type="checkbox" <?php
                                     for ($i = 0; $i <= count($chkbox); $i++) {
                                       echo $chkbox[$i] == '8' ? 'checked="checked"' : '';
                                     }
                                     ?> value="8" id ="ecommerce_currency_codes" name="EcommerceCurrencyCodes[]"><span>SEK</span><br>
                              </div>
                            </td>
                          </tr>    

                        </tbody></table>
                    </div>
                    <div class="form-sub-heading">
                      Organisation Profile
                    </div>
                    <p style="margin-left: 10px">Provide a short description of your organisation so that people looking your available list of online courses have some idea of who you are.</p>
                    <div style="margin-left:10px">       
                      <table class="simple-form">
                        <tbody><tr>
                            <th style="text-align: left;width: 20%">
                              Short Profile:
                            </th>
                            <td>
                              <textarea style="width:95%;" rows="5" name="organisation_profile" id="organisation_profile" cols="20" class="profile" ><?php echo $USER->organisation_profile ?></textarea>
                              <div class="tip" id="OrganisationProfile_counter"><span>500</span> character(s) left</div>
                            </td>
                          </tr>   
                          <tr>
                            <th>

                            </th>
                            <td>&nbsp;
                            </td>
                          </tr>                                                             
                        </tbody></table>  
                    </div>
                    <div class="form-sub-heading">
                      Google Analytics
                    </div>
                    <p style="margin-left: 10px">Track page views for your courses using your own <a target="_blank" href="http://www.google.com/analytics/">Google Analytics account</a>.</p>
                    <div style="margin-left:10px">  
                      <table class="simple-form">
                        <tbody><tr>
                            <th style="text-align: left;width: 20%">
                              Account Number:
                        <div class="tip">e.g. UA-xxxxxx-x</div>
                        </th>
                        <td>
                          <input type="text" value="<?php echo $USER->google_analytics_id ?>" name="google_analytics_id" id="google_analytics_id">
                        </td>
                        </tr>    
                        <tr>
                          <th>

                          </th>
                          <td>&nbsp;
                          </td>
                        </tr>                                                            
                        </tbody></table>
                    </div>
                    <div class="form-buttons">
                      <input type="submit" class="btnlarge" value="Save">
                    </div>                            
                  </form>
                </div>
              </div>
            </div>
          </td>
          <td class="admin-fullscreen-right">
            <div class="admin-fullscreen-content">
              <div class="side-panel">
                <h3>Course shopping cart</h3>
                <div class="padded">
                  <p>
                    The courses that you setup for selling will be visible to the public here:</p>
                </div>
                <div class="padded">
                  <p>
                    <a  target="_blank" href="../course/Sale_course/sale_course.php?userid=3">http://<?php echo $USER->custom_website_address ?>/online-course</a>
                  </p>
                </div>
              </div>
              <div class="side-panel">
                <h3>Course listings widget</h3>
                <div id="embedCode">
                  <div class="tip padded">Copy and paste the HTML code below to embed this course list in another blog or website</div>
                  <textarea style="width:100%;" rows="5" class="embed"></textarea>                
                </div> 
              </div>

            </div>
          </td>
        </tr>
      </tbody></table>
  <?php
}
?>     
</div>