<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$our_term = optional_param('Terms', '', PARAM_TEXT);
$show_confirmation_learner = optional_param('ShowOnFirstLogin', '', PARAM_TEXT);
$show_confirmation_ecommerce = optional_param('ShowOnEcommerceSignUp', '', PARAM_TEXT);
$user_id = optional_param('user_id', '', PARAM_TEXT);
$account_id = optional_param('id', 2, PARAM_INT);

global $CFG;
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/term.js"></script>
<script type="text/javascript" src="colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        

  <?php
  if (is_siteadmin()) {
    if ($action == 'update_term') {
      update_user_term($user_id, $our_term, $show_confirmation_learner, $show_confirmation_ecommerce);
      $SELECT = select_account_id($user_id);
    }
    else {
      $SELECT = select_account_id($account_id);
    }

    foreach ($SELECT as $USE) {
      ?>
      <table class="admin-fullscreen">
        <tbody><tr>
            <td class="admin-fullscreen-left">
              <div class="admin-fullscreen-content">

                <div class="focus-panel">
                  <div class="panel-head">
                    <div style="float: right">
                      <label> <?php echo $USE->username ?></label>
                    </div>
                    <h3>
                      Account
                    </h3>
                  </div>
                  <div class="body">

                    <div class="grid-tabs">
                      <ul>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class=""><?php print_string('billing') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('term') ?></a></li>    
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>
                      </ul>
                      <div class="clear">
                      </div>
                    </div>

                    <form method="post" action="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USE->id ?>&action=update_term">
                      <input type="hidden" name="user_id" value="<?php echo $USE->id ?>">

                      <p style="margin-left: 10px">
                        Set the Terms &amp; Conditions learners must agree to before they can purchase or access your online courses. 
                      </p>
                      <div class="form-sub-heading">
                        Our Terms &amp; Conditions
                      </div>
                      <table class="simple-form">
                        <tbody><tr>
                            <td>
                              <textarea style="width:95%;" rows="20" name="Terms" id="Terms" cols="20" class="profile"><?php echo $USE->our_term ?></textarea>
                            </td>
                          </tr>
                        </tbody></table>
                      <div class="form-sub-heading">
                        Display Settings
                      </div>
                      <p style="margin-left: 10px">Terms &amp; conditions will only be displayed if at least one of the following options are checked:</p>
                      <table class="simple-form">
                        <tbody><tr>
                            <td><input type="checkbox" value="1" <?php echo $USE->show_confirmation_learner != 0 ? 'checked="checked"' : '' ?>name="ShowOnFirstLogin" id="ShowOnFirstLogin">
                              <span class="tip">Show a confirmation checkbox on the learners first login</span></td>
                          </tr>                                 
                          <tr>
                            <td><input type="checkbox" value="1" <?php echo $USE->show_confirmation_ecommerce != 0 ? 'checked="checked"' : '' ?>name="ShowOnEcommerceSignUp" id="ShowOnEcommerceSignUp">
                              <span class="tip">Show a confirmation checkbox on the ecommerce sign up form</span></td>
                          </tr>                               
                        </tbody></table>                                                          
                      <div class="form-buttons">
                        <input type="submit" class="btnlarge" value="Save">
                      </div>                            

                    </form>
                  </div>
                </div> 


              </div>
            </td>
            <td class="admin-fullscreen-right">
              <div class="admin-fullscreen-content">


              </div>
            </td>
          </tr>
        </tbody></table>

      <?php
    }
  }
  else {

    if ($action == "update_term") {
      update_user_term($user_id, $our_term, $show_confirmation_learner, $show_confirmation_ecommerce);
      $USER = get_user_from_user_id($user_id);
    }
    else {
      global $USER;
    }
    ?>
    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">

              <div class="focus-panel">
                <div class="panel-head">
                  <h3>
                    <?php print_string('account') ?>
                  </h3>
                </div>
                <div class="body">

                  <div class="grid-tabs">
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $USER->id; ?>"><?php print_string('profile') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('theme') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('billing') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USER->id; ?>" class="selected"><?php print_string('term') ?></a></li>    
                    </ul>
                    <div class="clear">
                    </div>
                  </div>

                  <form method="post" action="<?php echo $CFG->wwwroot ?>/account/term.php?action=update_term">
                    <input type="hidden" name="user_id" value="<?php echo $USER->id ?>">
                    <p style="margin-left: 10px">
                      Set the Terms &amp; Conditions learners must agree to before they can purchase or access your online courses. 
                    </p>
                    <div class="form-sub-heading">
                      Our Terms &amp; Conditions
                    </div>
                    <table class="simple-form">
                      <tbody><tr>
                          <td>
                            <textarea style="width:95%;" rows="20" name="Terms" id="Terms" cols="20" class="profile"><?php echo $USER->our_term ?></textarea>
                          </td>
                        </tr>
                      </tbody></table>
                    <div class="form-sub-heading">
                      Display Settings
                    </div>
                    <p style="margin-left: 10px">Terms &amp; conditions will only be displayed if at least one of the following options are checked:</p>
                    <table class="simple-form">
                      <tbody><tr>
                          <td><input type="checkbox" value="1" <?php echo $USER->show_confirmation_learner != 0 ? 'checked="checked"' : '' ?> name="ShowOnFirstLogin" id="ShowOnFirstLogin">
                            <span class="tip">Show a confirmation checkbox on the learners first login</span></td>
                        </tr>                                 
                        <tr>
                          <td><input type="checkbox" value="1" <?php echo $USER->show_confirmation_ecommerce != 0 ? 'checked="checked"' : '' ?> name="ShowOnEcommerceSignUp" id="ShowOnEcommerceSignUp">
                            <span class="tip">Show a confirmation checkbox on the ecommerce sign up form</span></td>
                        </tr>                               
                      </tbody></table>                                                          
                    <div class="form-buttons">
                      <input type="submit" class="btnlarge" value="Save">
                    </div>                            
                  </form>
                </div>
              </div> 
            </div>
          </td>
          <td class="admin-fullscreen-right">
            <div class="admin-fullscreen-content">
            </div>
          </td>
        </tr>
      </tbody></table>
    <?php
  }
  ?>
</div>