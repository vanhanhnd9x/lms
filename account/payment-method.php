<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}
global $CFG, $USER, $OUTPUT, $PAGE, $DB;
require('../config.php');
require_login(0, false);
require($CFG->dirroot . '/common/lib.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>
      Phương thức thanh toán
    </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/price.css" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
      <script type="text/javascript" src="js/jquery.counter.min.js"></script>
      <script type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
      <script type="text/javascript" src="js/jquery.bgiframe.min.js"></script>
      <script type="text/javascript" src="js/jquery.raty.min.js"></script>  
      <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css">
          <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
            <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css">
              <link type="text/css" rel="stylesheet" href="css/fullcalendar.css">
<!--                <script type="text/javascript">
                  function payment_method(){
                    var  myDiv = document.getElementById('mydiv');
                    myDiv.style.display = (this.selectedIndex == 1) ? "block" : "none";
                    myDiv.style.display = (this.selectedIndex == 2) ? "block" : "none";
                  }
                                                
                </script>-->
                </head>
                <body>

                  <div id="page-body">
                    <?php
                    $service_pack = array();
                    $SubscriptionId = optional_param('SubscriptionId', 0, PARAM_INT);
                    //get package form DB
                    $service_pack = get_service_package($SubscriptionId);
                    if ($SubscriptionId <= '4') {
                      $cost = $service_pack->cost * 12;
                    }
                    else {
                      $cost = $service_pack->cost;
                    }
                    $Transaction_SubscriptionId = optional_param('Transaction_SubscriptionId', 0, PARAM_INT);
                    $payment_method = optional_param('paymentmethod', '', PARAM_TEXT);
                    $transaction_cost = optional_param('transaction_cost', '', PARAM_TEXT);
                    $transaction_spid = optional_param('transaction_spid', 0, PARAM_INT);
                    if (!empty($Transaction_SubscriptionId)) {
                      //insert payment
                      $t = optional_param('Transaction.AgreedToTerms', '', PARAM_TEXT);
                      global $USER;
                      $user_id = $USER->id;
                      $status = "Đăng ký";
                      $p_method = $payment_method;
                      $spid = $transaction_spid;
                      $payment_lastest = get_payment_lastest($user_id);
                      if (!empty($payment_lastest)) {
                        $p_start = $payment_lastest->p_end;
                        if ($Transaction_SubscriptionId <= '4') {
                          $p_end = strtotime("+1 years", $p_start);
                        }
                        else {
                          $p_end = strtotime("+1 months", $p_start);
                        }
                      }
                      else {
                        $p_start = time();
                        if ($Transaction_SubscriptionId <= '4') {
                          $p_end = strtotime("+1 years");
                        }
                        else {
                          $p_end = strtotime("+1 months");
                        }
                      }

                      $p_cost = $transaction_cost;
                      try {
                        $insert = insert_payment($user_id, $status, $p_method, $spid, $p_start, $p_end, $p_cost);
                        header("Location: billing.php?id=".$user_id);
                      } catch (Exception $exc) {
                        echo "Đã xảy ra lỗi. Đăng ký của Bạn chưa thành công. Bạn hãy thực hiện đăng ký 1 lần nữa. Rất tiếc!";
                        header("Location: billing.php");
                      }
                    }
//                   $SubscriptionId = optional_param(SubscriptionId, 0, PARAM_INT);
                    ?> 

                    <form name="payment" action="payment-method.php" method="post">
                      <input name="__RequestVerificationToken" type="hidden" value="">
                        <div class="container">
                          <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6 panel">
                              <div class="panel-body">

                                <h3>
                                  Đăng ký gói: <?php echo $service_pack->name ?></h3>
                                <div>
                                  <p>
                                    Chi phí gói đăng ký sử dụng dịch vụ trong 1 năm <b>VND <?php echo number_format($cost) ?> </b>. Tài khoản đăng ký được sử dụng với 
                                    <?php echo $t->user_active ?> người dùng hoạt động. Khi bạn muốn tăng thêm số lượng người dùng hoạt động thì sẽ phải trả chi phí 200.000vnd cho mỗi người dùng hoạt động trên tháng.</p>
                                </div>
                                <input id="Transaction_TransactionType" name="Transaction_TransactionType" type="hidden" value="Subscribe">
                                  <input id="Transaction_AuthType" name="Transaction_AuthType" type="hidden" value="Purchase">
                                    <input id="transaction_cost" name="transaction_cost" type="hidden" value="<?php echo $cost ?>">
                                      <input id="transaction_spid" name="transaction_spid" type="hidden" value="<?php echo $service_pack->spid ?>">
                                        <input id="Transaction_SubscriptionId" name="Transaction_SubscriptionId" type="hidden" value="<?php echo $SubscriptionId ?>">

                                          <div class="form-group">
                                            <h3>
                                              VND <?php echo number_format($cost) ?></h3>
                                          </div>

                                          <div class="form-group">
                                            <div class="col-sm-12">
                                              <label for="PaymentMethod">
                                                Phương thức thanh toán</label>
                                              <select class="form-control" name="paymentmethod">
                                                <option value="tienmat.chuyenkhoan" selected=""> Thanh toán tiền mặt/chuyển khoản</option>
                                                <option value="paypal"> Thanh toán Paypal</option>
                                              </select>

                                            </div>
                                          </div>
                                          <div id="mydiv">
                                            <br>
                                              <div class="text_address"><span class="style_01"><a href="#">© 2011 Công ty cổ phần Giải pháp Thiên Hoàng </a></span> 
                                                <br><br>
                                                    <span class="style_02">Trụ sở chính:&nbsp;Tầng 2 Toà nhà Licogi 13 - 164 Khuất Duy Tiến, Phường Nhân Chính, Quận Thanh Xuân, Thành phố Hà Nội</span>&nbsp;<br>
                                                      <span class="style_02">Tel: </span>+84 35589666<br>
                                                        <span class="style_02">Fax: </span>+84 35571666<br>
                                                          <span class="style_02">E-mail: </span><a href="#">sales@thienhoang.com.vn </a>         <br>
                                                            <span class="style_02">Website: </span><a href="#">www.thienhoang.com.vn </a>- <a href="#">www.thienhoang.net </a></div>
                                                            <div></div>
                                                            <br><br>
                                                                <div class="code_account"><span class="style_02">Mã số thuế: 0102298554 </span>         <br>
                                                                    <span class="style_02">Số TK: 0491001607222 tại VIETCOMBANK Chi nhánh Thăng Long</span></div>
                                                                <br><br>
                                                                    </div>   

                                                                    <div class="form-group">
                                                                      <div class="col-sm-12">
                                                                        <input id="Transaction_AgreedToTerms" name="Transaction.AgreedToTerms" type="checkbox" >
<!--                                                                          <input name="Transaction.AgreedToTerms" type="hidden" value="false">-->
                                                                          Tôi chấp nhận các <a target="_blank" href="http://thienhoang.com.vn">điều khoản & dịch vụ</a>
                                                                      </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                      <div class="col-sm-12">
                                                                        <input type="button" onclick="submitForm(this);" value=" Gửi đi " class="btn btn-primary"></div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </form>

                                                                    <?php
                                                                    $userfullname = fullname($USER);
                                                                    $userfirstname = $USER->firstname;
                                                                    $userlastname = $USER->lastname;
                                                                    $useraddress = $USER->address;
                                                                    $usercity = $USER->city;
                                                                    $paypalemail = $USER->paypal_id;
                                                                    $curency = $USER->ecommerce_currency_codes;
                                                                    
                                                                    //get service pack
//                                                                    $servicepack = get_service_package($sp);
                                                                    
                                                                    switch ($curency) {
                                                                      case 1:
                                                                        $curencies = "USD";
                                                                        break;
                                                                      case 2:
                                                                        $curencies = "AUD";
                                                                        break;
                                                                      case 3:
                                                                        $curencies = "CAD";
                                                                        break;
                                                                      case 4:
                                                                        $curencies = "EUR";
                                                                        break;
                                                                      case 5:
                                                                        $curencies = "GBP";
                                                                        break;
                                                                      case 6:
                                                                        $curencies = "NZD";
                                                                        break;
                                                                      case 7:
                                                                        $curencies = "ZAR";
                                                                        break;
                                                                      case 8:
                                                                        $curencies = "SEK";
                                                                        break;
                                                                      default:
                                                                        break;
                                                                    }
                                                                    $paypalurl = !empty($CFG->usepaypalsandbox) ? 'https://www.paypal.com/cgi-bin/webscr' : 'https://www.sandbox.paypal.com/cgi-bin/webscr';
                                                                    ?>

                                                                    <form name="paymentpaypal" action="<?php echo $paypalurl ?>" method="post">
                                                                      <input type="hidden" name="cmd" value="_xclick" />
                                                                      <input type="hidden" name="charset" value="utf-8" />
                                                                      <input type="hidden" name="business" value="<?php echo $paypalemail ?>" />
                                                                      <input type="hidden" name="item_name" value="<?php echo $service_pack->name ?>" />
                                                                      <input type="hidden" name="item_number" value="<?php echo $service_pack->spid ?>" />
                                                                      <input type="hidden" name="quantity" value="1" />
                                                                      <input type="hidden" name="on0" value="<?php print_string("user") ?>" />
                                                                      <input type="hidden" name="os0" value="<?php p($userfullname) ?>" />
                                                                      <input type="hidden" name="custom" value="<?php echo "{$USER->id}" ?>" />

                                                                      <input type="hidden" name="currency_code" value="<?php echo $curencies ?>" />
                                                                      <input type="hidden" name="amount" value="<?php echo $service_pack->cost_usd ?>" />

                                                                      <input type="hidden" name="for_auction" value="false" />
                                                                      <input type="hidden" name="no_note" value="1" />
                                                                      <input type="hidden" name="no_shipping" value="1" />
                                                                      <input type="hidden" name="notify_url" value="<?php echo "$CFG->wwwroot/enrol/paypal/ipn.php" ?>" />
                                                                      <input type="hidden" name="return" value="<?php echo "$CFG->wwwroot/enrol/paypal/return.php?id=$course->id" ?>" />
                                                                      <input type="hidden" name="cancel_return" value="<?php echo $CFG->wwwroot ?>" />
                                                                      <input type="hidden" name="rm" value="2" />
                                                                      <input type="hidden" name="cbt" value="" />

                                                                      <input type="hidden" name="first_name" value="<?php p($userfirstname) ?>" />
                                                                      <input type="hidden" name="last_name" value="<?php p($userlastname) ?>" />
                                                                      <input type="hidden" name="address" value="<?php p($useraddress) ?>" />
                                                                      <input type="hidden" name="city" value="<?php p($usercity) ?>" />
                                                                      <input type="hidden" name="email" value="<?php p($USER->email) ?>" />
                                                                      <input type="hidden" name="country" value="<?php p($USER->country) ?>" />

                                                                    </form> 

                                                                    </div>
                                                                    <script type="text/javascript">
                                                                      function submitForm(v) {
                                                                        $(v).val("Đang gửi").attr("disabled", "true");
                                                                        var x = document.forms["payment"]["paymentmethod"].value;
                                                                        if (x == "paypal"){
                                                                          document.forms["paymentpaypal"].submit();
                                                                          
                                                                          return false;
                                                                        }
                                                                        document.forms["payment"].submit();
                                                                      }
                                                                    </script>


                                                                    </body></html>
