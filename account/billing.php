<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}
global $CFG;
require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
//require($CFG->dirroot . '/lib/accesslib.php');

$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$message = optional_param('message', '', PARAM_TEXT);
$account_id = optional_param('id', 2, PARAM_INT);
$pid = optional_param('pid', 0, PARAM_INT);
$pmethod = optional_param('pmethod', '', PARAM_TEXT);
$status = optional_param('status', '', PARAM_TEXT);
$idupdate = optional_param('idupdate', 0, PARAM_INT);
$startdate = optional_param('startdate', '', PARAM_TEXT);
$enddate = optional_param('enddate', '', PARAM_TEXT);
$spidname = optional_param('name', '', PARAM_TEXT);
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<link type="text/css" rel="stylesheet" href="css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css">
<script type="text/javascript">
  /* Custom filtering function which will search data in column four between two values */
  $.fn.dataTable.ext.search.push(
  function( settings, data, dataIndex ) {
    var min = parseInt( $('#min').val(), 10 );
    var max = parseInt( $('#max').val(), 10 );
    var cost = parseFloat( data[2] ) || 0; // use data for the age column
 
    if ( ( isNaN( min ) && isNaN( max ) ) ||
      ( isNaN( min ) && cost <= max ) ||
      ( min <= cost   && isNaN( max ) ) ||
      ( min <= cost   && cost <= max ) )
    {
      return true;
    }
    return false;
  }
);
 
  $(document).ready(function() {
    var table = $('#payment-superadmin').DataTable();
    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').keyup( function() {
      table.draw();
    } );
    
    $('#payment-superadmin tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
      }
      else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
      }
    } );
    $('#cancelpayment').click(function() {
    });
        
    $('#assignCancel').click(function() {
      $('#facebox').hide();
      $('#overlay').fadeOut('fast');
    });
    $(document).keyup(function (e) {
      if (e.keyCode == 27) {

        $('#facebox').hide();
        $('#overlay').fadeOut('fast');  
      }
    });
    $(function() {
      $('#addpayment').click(function(){
            
        $('#overlay').fadeIn('fast',function(){
          $('#facebox').show();
        });
      });
    });
    
    $('#startdate').datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange:"-5:+5"
    });
    $('#enddate').datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange:"-5:+5"
    });
    
  } );
</script>
<script type="text/javascript">
  function update_pack(v){
    $("#updatepack").val(v);
    alert("Bạn đã thực hiện gia hạn thành công.");
    //    var url = "billing.php?message=yes";  
    //    window.location("billing.php?message=yes");
  }
  
  
</script>

<div id="page-body">        
  <?php
  switch ($action) {
    case "insert":
      try {
        $insert = insert_service_pack($name, $code, $cost, $method, $useractive, $email, $logo, $support);
        header("Location:servicepack.php?id=" . $account_id);
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
      break;
    case "update":
      try {
        $startdatetime = strtotime($startdate);
        $enddatetime = strtotime($enddate);
        $update = update_payment_by_superadmin($idupdate, $spidname, $startdatetime, $enddatetime, $pmethod, $status);
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
      break;
    case "delete":
      try {
        $delete = delete_payment($pid);
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
      break;
    default:
      break;
  }
  global $USER;
  $user_id = $USER->id;
  $payment = get_payment_user_id($USER->id);
  ?>
  <table class="admin-fullscreen">
    <tbody><tr>
        <td class="admin-fullscreen-left" style="width: 82%">
          <div class="admin-fullscreen-content">

            <div class="focus-panel">
              <div class="panel-head" style="background-color: rgb(112, 163, 60);">
                <?php  
                $USE = get_user_from_id($account_id);
                  if (is_siteadmin()) {
                ?>
                <div style="float: right">
                  <label> <?php echo $USE->username ?></label>
                </div>
                <?php  
                  }
                ?>
                <h3>
                  <?php print_string('account') ?>
                </h3>
              </div>
              <div class="body">

                <div class="grid-tabs">
                  <?php
                  if (is_siteadmin()) {
                    ?>
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('billing') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/email_setup.php?id=<?php echo $account_id; ?>" class=""><?php print_string('emailsetup') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>    
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>  
                    </ul>
                    <?php
                  }
                  else {
                    ?>
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $USER->id; ?>"><?php print_string('profile') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('theme') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $USER->id; ?>" class="selected"><?php print_string('billing') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('messagelab') ?></a></li>                  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/email_setup.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('emailsetup') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('ecommerce') ?></a></li>    
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('term') ?></a></li>  
                    </ul>
                    <?php
                  }
                  ?>
                  <div class="clear"></div>
                </div>
                <input id="updatepack" name="updatepack" value="" type="hidden">
                <div class="sys-note">
                  <?php
                  if (!is_siteadmin()) {
                    if (empty($payment)) {
                      $timecreated = $USER->timecreated;
                      $trial = strtotime("+14 day", $timecreated);
                      ?>
                      <span> Cảm ơn đã dùng thử LMS. Tài khoản được sử dụng đến ngày <?php echo date('d-m-y', $trial) ?></span><br>
                      <?php
                    }
                    else {
                      $now = time();
                      foreach ($payment as $key1 => $pay1) {
                        $startdate = $pay1->p_start;
                        $enddate = $pay1->p_end;
                        if ($startdate <= $now && $now <= $enddate) {
                          $service_pack1 = get_service_package($pay1->spid);
                            $current_pack = $service_pack1->name;
                            $current_spid = $service_pack1->spid;
                            $current_cost = $service_pack1->cost;
                        }
                      }
                      ?>
                      <span> Bạn đang sử dụng gói dịch vụ <?php echo "<strong>" . $current_pack . "</strong>" ?> Tiếp tục đăng ký gói dịch vụ <a href="billing.php?message=yes" onclick="update_pack(1);"> tại đây.</a></span><br>
                      <span> Hãy nâng cấp để sử dụng gói dịch vụ cao hơn.</span><p>
                      <?php } ?>
                      <?php
                      $updatepack = optional_param('updatepack', 0, PARAM_INT);

                      if (!empty($message)) {

                        $user_id = $USER->id;
                        $status = "Đang chờ";
                        $p_method = "Gia hạn";
                        $spid = $current_spid;

                        $payment_lastest = get_payment_lastest($user_id);
                        if (!empty($payment_lastest)) {
                          $p_start = $payment_lastest->p_end;
                          if ($current_spid <= '4') {
                            $p_end = strtotime("+1 years", $p_start);
                          }
                          else {
                            $p_end = strtotime("+1 months", $p_start);
                          }
                          //insert cost
                          if ($current_spid <= '4') {
                            $cost = $current_cost * 12;
                          }
                          else {
                            $cost = $current_cost;
                          }
                          $p_cost = $cost;
                          try {
                            $insert = insert_payment($user_id, $status, $p_method, $spid, $p_start, $p_end, $p_cost);
                            header("Location: billing.php");
                          } catch (Exception $exc) {
                            echo "Đã xảy ra lỗi. Gia hạn của Bạn chưa thành công. Bạn hãy thực hiện gia hạn 1 lần nữa. Rất tiếc!";
                            header("Location: billing.php");
                          }
                        }
                        ?>
                        <span><strong>Bạn đã gia hạn thành công gói dịch vụ đã đăng ký.</strong> </span>
  <?php } ?>
                    <p>
                    <div class="history"><h5><?php print_string('history') ?></h5></div>

                    <table id="tb-history">
                      <tr>
                        <th class="th-no"><?php print_string('no') ?></th>
                        <th class="th-action"><?php print_string('action') ?></th>
                        <th class="th-start"><?php print_string('startdate') ?></th>		
                        <th class="th-end"><?php print_string('endate') ?></th>
                        <th class="th-end"><?php print_string('status') ?></th>
                      </tr>
                      <?php
                      foreach ($payment as $key3 => $pay3) {
                        $service_pack2 = get_service_package($pay3->spid);
                          $pack_name = $service_pack2->name;
                          $pack_user = $service_pack2->user_active;
                        $p_start = date("d/m/y", $pay3->p_start);
                        $p_end = date("d/m/y", $pay3->p_end);
                        ?>
                        <tr>
                          <td><?php echo $pay3->id ?></td>
                          <td><?php echo "Đăng ký gói dịch vụ: " . $pack_name . "[" . $pack_user . " người dùng]" ?></td>		
                          <td><?php echo $p_start ?></td>
                          <td><?php echo $p_end ?></td>
                          <td><?php echo $pay3->status ?></td>
                        </tr>
                    <?php } ?>
                    </table>
                    <?php
                  }
                  else {
                    ?>
                    <div id="demo"></div>
                    <table border="0" cellspacing="5" cellpadding="5">
                      <tbody><tr>
                          <td>Chi phí nhỏ nhất:</td>
                          <td><input type="text" id="min" name="min"></td>
                        </tr>
                        <tr>
                          <td>Chi phí lớn nhất:</td>
                          <td><input type="text" id="max" name="max"></td>
                        </tr>
                      </tbody></table>
                    <!--                    <button id="cancelpayment" >Thoát payment</button>-->
                    <form name="edit-payment" action=" <?php echo $CFG->wwwroot ?>/account/billing.php?id= <?php echo $USER->id ?>" method="post" > 
                      <table id="payment-superadmin" class="display" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Tên đơn vị</th>
                            <th>Gói đăng ký</th>
                            <th>Chi phí</th>
                            <th>Từ ngày</th>
                            <th>Đến ngày</th>
                            <th>PT Thanh toán</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                          </tr>
                        </thead>

                        <tfoot>
                          <tr>
                            <th>Tên đơn vị</th>
                            <th>Gói đăng ký</th>
                            <th>Chi phí</th>
                            <th>Từ ngày</th>
                            <th>Đến ngày</th>
                            <th>PT Thanh toán</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                          </tr>
                        </tfoot>
                        <tbody>
                          <?php
                          $payment_super = get_payment_user_superadmin();
                          foreach ($payment_super as $key => $p_super) {
                            $selected1 = $p_super->status == "Đăng ký" ? "selected='selected'" : "";
                            $selected2 = $p_super->status == "Đang chờ" ? "selected='selected'" : "";
                            $selected3 = $p_super->status == "Đã thanh toán" ? "selected='selected'" : "";
                            $selected4 = $p_super->status == "Đang hoạt động" ? "selected='selected'" : "";
                            $selected5 = $p_super->status == "Huỷ bỏ" ? "selected='selected'" : "";
                            $selected6 = $p_super->status == "Đóng" ? "selected='selected'" : "";
                            $selected7 = $p_super->p_method == "tienmat.chuyenkhoan" ? "selected='selected'" : "";
                            $selected8 = $p_super->p_metho == "paypal" ? "selected='selected'" : "";
                            $sp_startdate = date("d/m/y", $p_super->p_start);
                            $sp_enddate = date("d/m/y", $p_super->p_end);
                            ?>
                            <tr>
                          <input type="hidden" name="idupdate" value="<?php echo $pid ?>">
                          <input type="hidden" name="action" value="update">
                          <td><a href="<?php echo $CFG->wwwroot ?>/manage/people/profile.php?id=<?php echo $p_super->user_id ?>"><?php echo $p_super->firstname . ' ' . $p_super->lastname ?></a></td>
                          <td><a href="#">
                              <?php
                              $arr_packs = get_service_package_superadmin();
                              if ($p_super->id == $pid) {
                                ?>
                                <select name="name">
                                  <?php
                                  foreach ($arr_packs as $key => $arr_pack) {
                                    echo "<option value='" . $arr_pack->spid . "'>" . $arr_pack->name . "</option>";
                                  }
                                  ?>
                                </select>
                                <?php
                              }
                              else {
                                echo $p_super->name;
                              }
                              ?>
                            </a></td>		
                          <td><?php echo number_format($p_super->p_cost) ?></td>
                          <td><?php echo $p_super->id == $pid ? "<input type='text' name='startdate' id='startdate'>" : $sp_startdate ?></td>
                          <td><?php echo $p_super->id == $pid ? "<input type='text' name='enddate' id='enddate'>" : $sp_enddate ?></td>
                          <td><?php echo $p_super->id == $pid ? "<select name='pmethod'>
                          <option value='tienmat.chuyenkhoan'" . $selected7 . ">Tiền mặt - Chuyển khoản</option>
                          <option value='paypal'" . $selected8 . ">Paypal</option>
                          </select>" : $p_super->p_method; ?></td>
                          <td><?php echo $p_super->id == $pid ? "<select name='status'>
                          <option value='Đăng ký'" . $selected1 . ">Đăng ký</option>
                          <option value='Đang chờ'" . $selected2 . ">Đang chờ</option>
                          <option value='Đã thanh toán'" . $selected3 . ">Đã thanh toán</option>
                          <option value='Đang hoạt động'" . $selected4 . ">Đang hoạt động</option>
                          <option value='Huỷ bỏ'" . $selected5 . ">Huỷ bỏ</option>
                          <option value='Đóng'" . $selected6 . ">Đóng</option>
                          </select>" : $p_super->status; ?></td>
                          <td><?php
                              echo $p_super->id == $pid ? "<input type='submit' name='submit' value='Lưu'><a href=" . $CFG->wwwroot . "/account/billing.php?id=" . $account_id . "> Thoát </a>" :
                                  "<a href=" . $CFG->wwwroot . "/account/billing.php?id=" . $account_id . "&pid=" . $p_super->id . ">Chỉnh sửa</a> 
                          <a href=" . $CFG->wwwroot . "/account/billing.php?id=" . $account_id . "&pid=" . $p_super->id . "&action=delete>Xoá</a>"
                              ?></td>
                          </tr>

                          <?php
                        }
                        ?>
                        </tbody>
                      </table>
                    </form>
                    <?php
                  }
                  ?>
                  <!--payment details-->
                  <div class="overlay" id="overlay" style="display:none;"></div>
                  <div id="facebox" style="display: none;" >      
                    <div class="popup"><div class="content">
<?php ?>
                        <div id="assignItemBox">
                          <div class="assign-header">
                            <span style="color: #fff; font-weight: bolder;">Thêm mới thanh toán</span>
                          </div>
                          <form action="<?php echo $CFG->wwwroot ?>/account/billing.php?action=insert" method="post">
                            <div id="assignResults">
                              <table cellspacing="0" class="item-page-list">
                                <?php
                                $servicepacks = get_service_package_superadmin();
                                ?>
                                <tbody>
                                <fieldset>
                                  <tr>
                                    <td><label for="name">Tên đơn vị</label></td>
                                    <td><input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"></td>
                                  </tr>
                                  <tr>
                                    <td><label for="pack">Gói đăng ký</label></td>
                                    <td>
                                      <select name="pack">
                                        <?php
                                        foreach ($servicepacks as $key => $servicepack) {
                                          ?>
                                          <option value="<?php echo $servicepack->spid ?>"><?php echo $servicepack->name . '-' . $servicepack->method ?></option>
                                          <?php
                                        }
                                        ?>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label for="cost">Chi phí</label></td>
                                    <td>
                                      <input type="text" name="cost" value="">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label for="startdate">Từ ngày</label></td>
                                    <td><input type='text' name='startdate' id='startdate'></td>
                                  </tr>
                                  <tr>
                                    <td><label for="enddate">Từ ngày</label></td>
                                    <td><input type='text' name='enddate' id='enddate'></td>
                                  </tr>
                                  <tr>
                                    <td><label for="method">Phương thức thanh toán</label></td>
                                    <td>
                                      <select name="pmethod">
                                        <option class="selected" value="tienmat.chuyenkhoan">Tiền mặt - Chuyển khoản</option>
                                        <option value="paypal">Paypal</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label for="status">Trạng thái</label></td>
                                    <td>
                                      <select name="status">
                                        <option value="Đăng ký">Đăng ký</option>
                                        <option class="selected" value="Đang chờ">Đang chờ</option>
                                        <option value="Đã thanh toán">Đã thanh toán</option>
                                        <option value="Đang hoạt động">Đang hoạt động</option>
                                        <option value="Đóng">Đóng</option>
                                      </select>
                                    </td>
                                  </tr>
                                  <!-- Allow form submission with keyboard without duplicating the dialog button -->
                                  <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                                </fieldset>
                                </tbody>
                              </table>

                            </div>
                            <div class="assign-footer-opt clearfix hidden">
                              <span id="assignEmails"><input type="checkbox" name="SendEmail" id="SendEmail" checked> <label for="SendEmail"><?php print_r(get_string('sendemailnoti')) ?></label></span>
                            </div>
                            <div class="assign-footer clearfix">
                              <div class="float-left">
                                <input type="submit" class="big-button drop" value="<?php print_string('save', 'admin') ?>" />

<?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                            </div>
                          </form>
                        </div></div> </div></div>
                  <!--#end payment details-->
                </div> 
              </div>
            </div>
          </div>
        </td>
        <td class="admin-fullscreen-right">
          <div class="admin-fullscreen-content">
            <div class="side-panel">
              <?php
              if (!is_siteadmin()) {
                ?>
                <div class="action-buttons">
                  <ul>
                    <li><a href="pricingplan.php" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span class="icon_go"><?php print_string('upgrade') ?></span></span></span></span></a></li>
                  </ul>
                </div>
                <?php
              }
              else {
                ?>
                <div class="action-buttons">
                  <ul>
                    <li><a href="#" class="big-button drop" onclick="" id="addpayment"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print('Thêm') ?></span></span></span></span></a></li>
                  </ul>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </td>
      </tr>
    </tbody></table>
</div>


