<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$user_id = optional_param('user_id', '', PARAM_TEXT);
$show_logo_on_login = optional_param('show_logo_on_login', 0, PARAM_TEXT);
$show_logo_on_trainee_toolbar = optional_param('show_logo_on_trainee_toolbar', 0, PARAM_TEXT);
$color_title_bar = optional_param('color_title_bar', 0, PARAM_TEXT);
$color_title_bar_text = optional_param('color_title_bar_text', 0, PARAM_TEXT);
$color_navigation_heading = optional_param('color_navigation_heading', 0, PARAM_TEXT);
$color_heading_text = optional_param('color_heading_text', 0, PARAM_TEXT);
$color_login_bg = optional_param('color_login_bg', 0, PARAM_TEXT);
$font = optional_param('change_font', 0, PARAM_TEXT);
$account_id = optional_param('id', 2, PARAM_INT);
global $CFG;
//$rows = select_account(); 
//$selects = select_account_id($account_id);
if (is_siteadmin()) {
  if ($_FILES["file"]["name"] != '') {
    if ($_FILES["file"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file"]["name"]));
      if (strtolower($file_extension) == "jpg" || strtolower($file_extension) == "jpeg"
          || strtolower($file_extension) == "gif"
          || strtolower($file_extension) == "png") {
        move_uploaded_file($_FILES["file"]["tmp_name"], $CFG->dirroot . "/account/logo/" . $account_id . "_" . $_FILES["file"]["name"]);
        update_user_logo($account_id, $CFG->wwwroot . "/account/logo/" . $account_id . "_" . $_FILES["file"]["name"]);
      }
      else {
        //displayJsAlert("File type for logo image must be : .jpg, .jpeg, .gif, .png", '');
      }
    }
  }
//file_favicon 
  if ($_FILES["file_favicon"]["name"] != '') {
    if ($_FILES["file_favicon"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file_favicon"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file_favicon"]["name"]));
      if (strtolower($file_extension) == "ico") {
        move_uploaded_file($_FILES["file_favicon"]["tmp_name"], $CFG->dirroot . "/account/logo/" . $account_id . "_file_favicon_" . $_FILES["file_favicon"]["name"]);
        update_user_favico($account_id, $CFG->wwwroot . "/account/logo/" . $account_id . "_file_favicon_" . $_FILES["file_favicon"]["name"]);
      }
      else {
        //displayJsAlert("File type for favico must be : .ico", '');
      }
    }
  }
//file_ipad
  if ($_FILES["file_ipad"]["name"] != '') {
    if ($_FILES["file_ipad"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file_ipad"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file_ipad"]["name"]));
      if (strtolower($file_extension) == "png") {
        move_uploaded_file($_FILES["file_ipad"]["tmp_name"], $CFG->dirroot . "/account/logo/" . $account_id . "_file_ipad_" . $_FILES["file_ipad"]["name"]);
        update_user_ipad_icon($account_id, $CFG->wwwroot . "/account/logo/" . $account_id . "_file_ipad_" . $_FILES["file_ipad"]["name"]);
      }
      else {
        //displayJsAlert("File type for favico must be : .png", '');
      }
    }
  }
  if ($action == 'update_theme') {
    update_user_theme($account_id, $show_logo_on_login, $show_logo_on_trainee_toolbar, $color_title_bar, $color_title_bar_text, $color_navigation_heading, $color_heading_text, $color_login_bg, $font);
    //$USER=get_user_from_user_id($account_id); 
    $SELECT = select_account_id($account_id);
  }
  else {
    $SELECT = select_account_id($account_id);
  }
  ?>
  <script type="text/javascript" src="js/jquery-1.7.1.js"></script>
  <script type="text/javascript" src="js/theme.js"></script>
  <script type="text/javascript" src="colorpicker/js/colorpicker.js"></script>
  <script type="text/javascript" src="js/search.js"></script>
  <script type="text/javascript" src="js/jquery.autocomplete.js"></script>
  <link rel="stylesheet" media="screen" type="text/css" href="colorpicker/css/colorpicker.css" />

  <div id="page-body">        
    <?php
    foreach ($SELECT as $USE) {
      ?>
      <table class="admin-fullscreen">
        <tbody><tr>
            <td class="admin-fullscreen-left">
              <div class="admin-fullscreen-content">

                <div class="focus-panel">
                  <div class="panel-head" style="background-color: rgb(112, 163, 60);">
                    <div style="float: right">
                      <label> <?php echo $USE->username ?></label>
                    </div>
                    <h3 id="prev-heading" style="background-color: rgb(112, 163, 60); color: rgb(255, 255, 255);">
                      <?php print_string('account') ?>
                    </h3>
                  </div>
                  <div class="body">

                    <div class="grid-tabs">
                      <ul>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('theme') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class=""><?php print_string('billing') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>   
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>
                      </ul>
                      <div class="clear">
                      </div>
                    </div>
                    <div style="margin-left:10px">


                      <form method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>&action=update_theme">
                        <p>
                          <b><?php print_string('yourlogo') ?></b>
                        </p>
                        <hr>
                        <div>
                          <input type="hidden" value="" name="RemoveLogo" id="RemoveLogo">
                          <input type="hidden" value="<?php $account_id ?>" name="user_id" id="user_id"/>

                          <?php if ($USE->url != '') { ?>
                            <div style="text-align: center;">
                              <div class="padded img-background">
                                <img height="90" width="134" alt="<?php echo $USE->username ?>" src="<?php echo $USE->url ?>"></div>
                            </div>
                          <?php } ?>
                          <div style="text-align: center;"> (<a onclick="$('.logo-upload').slideDown()" href="javascript:void(0);"><?php print_string('changelogo') ?></a>)</div>
                        </div>
                        <div>
                          <div style="padding: 15px; background: none repeat scroll 0% 0% rgb(238, 238, 238); display: none;" class="logo-upload"> 

                            <input type="file" name="file" id="file"/>
                            <ul style="list-style-type:none;" class="tip">
                              <li><?php print_string('accepttype') ?><span class="label"> .jpg, .jpeg, .gif, .png</span></li>
                              <li><?php print_string('maxfile') ?><span class="label"> 200kb</span></li>
                              <li><?php print_string('resized') ?></li>
                            </ul>
                          </div>
                        </div>
                        <hr>
                        <div class="padded">
                          <div>
                            <input type="checkbox" value="1" name="show_logo_on_login" id="show_logo_on_login" <?php echo $USE->show_logo_on_login != 0 ? 'checked="checked"' : ''; ?>>
                            <span class="tip"><?php print_string('showlogin') ?></span>
                          </div>
                          <div>
                            <input type="checkbox" value="1" name="show_logo_on_trainee_toolbar" id="show_logo_on_trainee_toolbar" <?php echo $USE->show_logo_on_trainee_toolbar != 0 ? 'checked="checked"' : ''; ?>>

                            <span class="tip"><?php print_string('showtrain') ?></span>
                          </div>
                        </div>
                        <div class="form-sub-heading"><?php print_string('theme') ?></div>
                        <p class="tip padded"><?php print_string('selectone') ?></p>
                        <select style="width:200px;"  name="theme_color" id="theme_color">
                          <option value="3"><?php print_string('green') ?></option>
                          <option value="4"><?php print_string('grey') ?></option>
                          <option value="5"><?php print_string('blue') ?></option>
                          <option value="6"><?php print_string('red') ?></option>
                          <option value="7"><?php print_string('yellow') ?></option>
                          <option value="8"><?php print_string('black') ?></option>
                          <option value="70"><?php print_string('darkside') ?></option>
                          <option value="189"><?php print_string('default') ?></option>
                          <option value="0" selected="selected"><?php print_string('custometheme') ?></option>
                        </select> 
                        <table cellspacing="0" cellpadding="0" style="width:100%;" class="themeTable">
                          <tbody><tr>
                              <td class="tip" style="border-bottom:1px solid #ccc;padding-top:20px;" colspan="5"><?php print_string('learneradmin') ?></td>
                            </tr>
                            <tr>
                              <td align="center">
                                <div style="margin-top:35px;">
                                  <select style="width:100px;" onchange="" name="change_font" id="change_font">
                                    <option value="1" <?php echo $USE->font == 1 ? 'selected="selected"' : ''; ?>>Arial</option>
                                    <option value="2" <?php echo $USE->font == 2 ? 'selected="selected"' : ''; ?>>Tahoma</option>
                                    <option value="3" <?php echo $USE->font == 3 ? 'selected="selected"' : ''; ?>>Verdana</option>
                                    <option value="4" <?php echo $USE->font == 4 ? 'selected="selected"' : ''; ?>>Helvetica</option>
                                  </select> 
                                  <div class="themename">Font</div> 
                                </div>
                              </td>
                              <td align="center">
                                <div onclick="" id="ToolbarBgColor" class="themeitem">
                                  <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(79, 82, 76);"></div>
                                  <input type="hidden" value="<?php echo $USE->title_bar_color != '' ? $USE->title_bar_color : ''; ?>" name="color_title_bar" id="color_title_bar"/>
                                </div>
                                <div class="themename"><?php print_string('titlebar') ?></div> 
                                <?php if ($USE->title_bar_color != '') { ?>
                                  <script type="text/javascript">
                                    $('#ToolbarBgColor div').css('backgroundColor', '#' + '<?php echo $USE->title_bar_color; ?>');    
                                  </script>
                                <?php } ?>
                              </td>
                              <td align="center">
                                <div onclick="" id="ToolbarTextColor" class="themeitem">
                                  <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(255, 255, 255);"></div>
                                  <input type="hidden" value="<?php echo $USE->title_bar_text_color != '' ? $USE->title_bar_text_color : ''; ?>" name="color_title_bar_text" id="color_title_bar_text"/>
                                </div>
                                <div class="themename"><?php print_string('titlebartext') ?></div>
                                <?php if ($USE->title_bar_text_color != '') { ?>
                                  <script type="text/javascript">
                                    $('#ToolbarTextColor div').css('backgroundColor', '#' + '<?php echo $USE->title_bar_text_color; ?>');    
                                  </script>
                                <?php } ?>
                              </td>
                              <td align="center">
                                <div onclick="" id="TabBgColor" class="themeitem">
                                  <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(112, 163, 60);"></div>
                                  <input type="hidden" value="<?php echo $USE->navigation_heading_color != '' ? $USE->navigation_heading_color : ''; ?>" name="color_navigation_heading" id="color_navigation_heading"/>
                                </div>

                                <div class="themename"><?php print_string('heading') ?></div>
                                <?php if ($USE->navigation_heading_color != '') { ?>
                                  <script type="text/javascript">
                                    $('#TabBgColor div').css('backgroundColor', '#' + '<?php echo $USE->navigation_heading_color; ?>');    
                                  </script>
                                <?php } ?>
                              </td>
                              <td align="center">
                                <div onclick="" id="TabTextColor" class="themeitem">
                                  <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(255, 255, 255);"></div>

                                  <input type="hidden" value="<?php echo $USE->heading_text_color != '' ? $USE->heading_text_color : ''; ?>" name="color_heading_text" id="color_heading_text"/>
                                </div>
                                <div class="themename"><?php print_string('headingtext') ?></div>
                                <?php if ($USE->heading_text_color != '') { ?>
                                  <script type="text/javascript">
                                    $('#TabTextColor div').css('backgroundColor', '#' + '<?php echo $USE->heading_text_color; ?>');    
                                  </script>
                                <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td class="tip" style="border-bottom:1px solid #ccc;padding-top:20px;" colspan="5"><?php print_string('loginpage') ?></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="center">
                                <div onclick="" id="LoginBgColor" class="themeitem">
                                  <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(255, 255, 255);"></div>

                                  <input type="hidden" value="<?php echo $USE->login_page_background != '' ? $USE->login_page_background : ''; ?>" name="color_login_bg" id="color_login_bg"/>
                                </div>
                                <div class="themename"><?php print_string('background') ?></div>
                                <?php if ($USE->login_page_background != '') { ?>
                                  <script type="text/javascript">
                                    $('#LoginBgColor div').css('backgroundColor', '#' + '<?php echo $USE->login_page_background; ?>');    
                                  </script>
                                <?php } ?>
                              </td>
                            </tr>   
                          </tbody></table>
                        <div class="tip" style="border-bottom:1px solid #ccc;padding-top:20px;"><?php print_string('addicon') ?></div>
                        <table cellspacing="5" cellpadding="5">
                          <tbody><tr>
                              <td valign="middle" align="center">

                                <?php if ($USE->favicon != '') { ?>
                                  <img height="16" width="16" alt="" src="<?php echo $USE->favicon ?>">
                                <?php } ?>
                              </td>
                              <td valign="middle" align="center">
                                <?php if ($USE->ipad_icon != '') { ?>
                                  <img height="57" width="57" alt="" src="<?php echo $USE->ipad_icon ?>">
                                <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td align="center">
                                <div>
                                  <div class="padded tip"><b>Favicon</b> (<a onclick="$('.favicon-upload').slideDown()" href="javascript:void(0);"><?php print_string('change') ?></a>)</div>
                                  <div style="width:300px; text-align:left">
                                    <div class="tip"><?php print_string('yourfacicon') ?></div>
                                    <div style="display:none;padding:15px; background:#eee" class="favicon-upload"> 
                                      <input type="file" name="file_favicon" id="file_favicon">
                                      <ul style="list-style-type:none;" class="tip">
                                        <li><?php print_string('accepttype') ?><span class="label">.ico</span></li>
                                        <li><?php print_string('maxfile') ?><span class="label"> 200kb</span></li>
                                      </ul>
                                    </div>
                                  </div> 
                                </div> 
                              </td>
                              <td align="center">
                                <div> 
                                  <div class="padded tip"><b>iPad/ iPhone icon</b> (<a onclick="$('.iDevice-upload').slideDown()" href="javascript:void(0);"><?php print_string('change') ?></a>)</div>
                                  <div style="width:300px; text-align:left">
                                    <div class="tip"><?php print_string('ipadicon') ?></div>
                                    <div style="display:none;padding:15px; background:#eee" class="iDevice-upload"> 
                                      <input type="file" name="file_ipad" id="file_ipad">
                                      <ul style="list-style-type:none;" class="tip">
                                        <li><?php print_string('accepttype') ?><span class="label">.png</span></li>
                                        <li><?php print_string('maxfile') ?><span class="label"> 200kb</span></li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody></table> 
                        <div style="width:200px; height:200px; position:absolute;display:none;" class="colorpicker-container">
                          <div id="colorpickerHolder"><div class="colorpicker" id="collorpicker_359" style="display: block; position: relative;"><div class="colorpicker_color" style="background-color: rgb(255, 0, 255);"><div><div style="left: 0px; top: 150px;"></div></div></div><div class="colorpicker_hue"><div style="top: 25px;"></div></div><div class="colorpicker_new_color" style="background-color: rgb(0, 0, 0);"></div><div class="colorpicker_current_color" style="background-color: rgb(0, 0, 0);"></div><div class="colorpicker_hex"><span>#</span><input type="text" size="6" maxlength="6"></div><div class="colorpicker_rgb_r colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_rgb_g colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_rgb_b colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_hsb_h colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_hsb_s colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_hsb_b colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_submit"></div><div class="colopicker_done"><input type="button" value="Clear" onclick="" class="cp_clear"><input type="button" value="Done" onclick=""></div></div></div>
                        </div>
                        <div class="form-buttons">
                          <input type="submit" class="btnlarge" value="<?php print_string('save','admin') ?>">
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

              </div>
            </td>
            <td class="admin-fullscreen-right">
              <div class="admin-fullscreen-content">
              </div>
            </td>
          </tr>
        </tbody></table>
    </div>
    <?php
  }
}else {
  if ($_FILES["file"]["name"] != '') {
    if ($_FILES["file"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file"]["name"]));
      if (strtolower($file_extension) == "jpg" || strtolower($file_extension) == "jpeg"
          || strtolower($file_extension) == "gif"
          || strtolower($file_extension) == "png") {
        move_uploaded_file($_FILES["file"]["tmp_name"], $CFG->dirroot . "/account/logo/" . $USER->id . "_" . $_FILES["file"]["name"]);
        update_user_logo($USER->id, $CFG->wwwroot . "/account/logo/" . $USER->id . "_" . $_FILES["file"]["name"]);
      }
      else {
        //displayJsAlert("File type for logo image must be : .jpg, .jpeg, .gif, .png", '');
      }
    }
  }
//file_favicon 
  if ($_FILES["file_favicon"]["name"] != '') {
    if ($_FILES["file_favicon"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file_favicon"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file_favicon"]["name"]));


      if (strtolower($file_extension) == "ico") {
        move_uploaded_file($_FILES["file_favicon"]["tmp_name"], $CFG->dirroot . "/account/logo/" . $USER->id . "_file_favicon_" . $_FILES["file_favicon"]["name"]);
        update_user_favico($USER->id, $CFG->wwwroot . "/account/logo/" . $USER->id . "_file_favicon_" . $_FILES["file_favicon"]["name"]);
      }
      else {
        //displayJsAlert("File type for favico must be : .ico", '');
      }
    }
  }
//file_ipad
  if ($_FILES["file_ipad"]["name"] != '') {
    if ($_FILES["file_ipad"]["error"] > 0) {
      echo "Return Code: " . $_FILES["file_ipad"]["error"] . "<br />";
    }
    else {
      $file_extension = end(explode('.', $_FILES["file_ipad"]["name"]));


      if (strtolower($file_extension) == "png") {
        move_uploaded_file($_FILES["file_ipad"]["tmp_name"], $CFG->dirroot . "/account/logo/" . $USER->id . "_file_ipad_" . $_FILES["file_ipad"]["name"]);
        update_user_ipad_icon($USER->id, $CFG->wwwroot . "/account/logo/" . $USER->id . "_file_ipad_" . $_FILES["file_ipad"]["name"]);
      }
      else {
        //displayJsAlert("File type for favico must be : .png", '');
      }
    }
  }
  if ($action == 'update_theme') {
    update_user_theme($user_id, $show_logo_on_login, $show_logo_on_trainee_toolbar, $color_title_bar, $color_title_bar_text, $color_navigation_heading, $color_heading_text, $color_login_bg, $font);
    //$USER=get_user_from_user_id($account_id); 
    $USER = get_user_from_user_id($user_id);
  }
  else {
    global $USER;
  }
  ?>

  <script type="text/javascript" src="js/jquery-1.7.1.js"></script>
  <script type="text/javascript" src="js/theme.js"></script>
  <script type="text/javascript" src="colorpicker/js/colorpicker.js"></script>
  <script type="text/javascript" src="js/search.js"></script>
  <script type="text/javascript" src="js/jquery.autocomplete.js"></script>
  <link rel="stylesheet" media="screen" type="text/css" href="colorpicker/css/colorpicker.css" />

  <div id="page-body">        


    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">

              <div class="focus-panel">
                <div class="panel-head" style="background-color: rgb(112, 163, 60);">
                  <h3 id="prev-heading" style="background-color: rgb(112, 163, 60); color: rgb(255, 255, 255);">
                    <?php print_string('account') ?>
                  </h3>
                </div>
                <div class="body">
                  <div class="grid-tabs">
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $USER->id; ?>"><?php print_string('profile') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $USER->id; ?>" class="selected"><?php print_string('theme') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('billing') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('term') ?></a></li>   
                    </ul>
                    <div class="clear">
                    </div>
                  </div>
                  <div style="margin-left:10px">
                    <form method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/account/theme.php?action=update_theme">
                      <p>
                        <b><?php print_string('yourlogo') ?></b>
                      </p>
                      <hr>
                      <div>
                        <input type="hidden" value="" name="RemoveLogo" id="RemoveLogo">
                        <input type="hidden" value="<?php echo $USER->id ?>" name="user_id" id="user_id"/>

                        <?php if ($USER->url != '') { ?>
                          <div style="text-align: center;">
                            <div class="padded img-background">

                              <img height="90" width="134" alt="<?php echo $USER->username ?>" src="<?php echo $USER->url ?>"></div>

                          </div>
                        <?php } ?>
                        <div style="text-align: center;"> (<a onclick="$('.logo-upload').slideDown()" href="javascript:void(0);"><?php print_string('changelogo') ?></a>)</div>
                      </div>
                      <div>

                        <div style="padding: 15px; background: none repeat scroll 0% 0% rgb(238, 238, 238); display: none;" class="logo-upload"> 

                          <input type="file" name="file" id="file"/>
                          <ul style="list-style-type:none;" class="tip">
                            <li><?php print_string('accepttype') ?><span class="label"> .jpg, .jpeg, .gif, .png</span></li>
                            <li><?php print_string('maxfile') ?><span class="label"> 200kb</span></li>
                            <li><?php print_string('resized') ?></li>
                          </ul>
                        </div>

                      </div>
                      <hr>
                      <div class="padded">
                        <div>
                          <input type="checkbox" value="1" name="show_logo_on_login" id="show_logo_on_login" <?php echo $USER->show_logo_on_login != 0 ? 'checked="checked"' : ''; ?>>
                          <span class="tip"><?php print_string('showlogin') ?></span>
                        </div>
                        <div>
                          <input type="checkbox" value="1" name="show_logo_on_trainee_toolbar" id="show_logo_on_trainee_toolbar" <?php echo $USER->show_logo_on_trainee_toolbar != 0 ? 'checked="checked"' : ''; ?>>
                          <span class="tip"><?php print_string('showtrain') ?></span>
                        </div>
                      </div>
                      <div class="form-sub-heading"><?php print_string('theme') ?></div>
                      <p class="tip padded"><?php print_string('selectone') ?></p>
                      <select style="width:200px;"  name="theme_color" id="theme_color">
                        <option value="3"><?php print_string('green') ?></option>
                        <option value="4"><?php print_string('grey') ?></option>
                        <option value="5"><?php print_string('blue') ?></option>
                        <option value="6"><?php print_string('red') ?></option>
                        <option value="7"><?php print_string('yellow') ?></option>
                        <option value="8"><?php print_string('black') ?></option>
                        <option value="70"><?php print_string('darkside') ?></option>
                        <option value="189"><?php print_string('default') ?></option>
                        <option value="0" selected="selected"><?php print_string('custometheme') ?></option>
                      </select> 
                      <table cellspacing="0" cellpadding="0" style="width:100%;" class="themeTable">
                        <tbody><tr>
                            <td class="tip" style="border-bottom:1px solid #ccc;padding-top:20px;" colspan="5"><?php print_string('learneradmin') ?></td>
                          </tr>
                          <tr>
                            <td align="center">
                              <div style="margin-top:35px;">
                                <select style="width:100px;" onchange="" name="change_font" id="change_font">
                                  <option value="1" <?php echo $USER->font == 1 ? 'selected="selected"' : ''; ?>>Arial</option>
                                  <option value="2" <?php echo $USER->font == 2 ? 'selected="selected"' : ''; ?>>Tahoma</option>
                                  <option value="3" <?php echo $USER->font == 3 ? 'selected="selected"' : ''; ?>>Verdana</option>
                                  <option value="4" <?php echo $USER->font == 4 ? 'selected="selected"' : ''; ?>>Helvetica</option>
                                </select> 
                                <div class="themename">Font</div> 
                              </div>
                            </td>
                            <td align="center">
                              <div onclick="" id="ToolbarBgColor" class="themeitem">
                                <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(79, 82, 76);"></div>
                                <input type="hidden" value="<?php echo $USER->title_bar_color != '' ? $USER->title_bar_color : ''; ?>" name="color_title_bar" id="color_title_bar"/>
                              </div>
                              <div class="themename"><?php print_string('titlebar') ?></div> 
                              <?php if ($USER->title_bar_color != '') { ?>
                                <script type="text/javascript">
                                  $('#ToolbarBgColor div').css('backgroundColor', '#' + '<?php echo $USER->title_bar_color; ?>');    
                                </script>
                              <?php } ?>
                            </td>
                            <td align="center">
                              <div onclick="" id="ToolbarTextColor" class="themeitem">
                                <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(255, 255, 255);"></div>
                                <input type="hidden" value="<?php echo $USER->title_bar_text_color != '' ? $USER->title_bar_text_color : ''; ?>" name="color_title_bar_text" id="color_title_bar_text"/>
                              </div>
                              <div class="themename"><?php print_string('titlebartext') ?></div>
                              <?php if ($USER->title_bar_text_color != '') { ?>
                                <script type="text/javascript">
                                  $('#ToolbarTextColor div').css('backgroundColor', '#' + '<?php echo $USER->title_bar_text_color; ?>');    
                                </script>
                              <?php } ?>
                            </td>
                            <td align="center">
                              <div onclick="" id="TabBgColor" class="themeitem">
                                <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(112, 163, 60);"></div>
                                <input type="hidden" value="<?php echo $USER->navigation_heading_color != '' ? $USER->navigation_heading_color : ''; ?>" name="color_navigation_heading" id="color_navigation_heading"/>
                              </div>
                              <div class="themename"><?php print_string('heading') ?></div>
                              <?php if ($USER->navigation_heading_color != '') { ?>
                                <script type="text/javascript">
                                  $('#TabBgColor div').css('backgroundColor', '#' + '<?php echo $USER->navigation_heading_color; ?>');    
                                </script>
                              <?php } ?>
                            </td>
                            <td align="center">
                              <div onclick="" id="TabTextColor" class="themeitem">
                                <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(255, 255, 255);"></div>
                                <input type="hidden" value="<?php echo $USER->heading_text_color != '' ? $USER->heading_text_color : ''; ?>" name="color_heading_text" id="color_heading_text"/>
                              </div>
                              <div class="themename"><?php print_string('headingtext') ?></div>
                              <?php if ($USER->heading_text_color != '') { ?>
                                <script type="text/javascript">
                                  $('#TabTextColor div').css('backgroundColor', '#' + '<?php echo $USER->heading_text_color; ?>');    
                                </script>
                              <?php } ?>
                            </td>
                          </tr>
                          <tr>
                            <td class="tip" style="border-bottom:1px solid #ccc;padding-top:20px;" colspan="5"><?php print_string('loginpage') ?></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td align="center">
                              <div onclick="" id="LoginBgColor" class="themeitem">
                                <div class="colorBlob" style="background: none repeat scroll 0% 0% rgb(255, 255, 255);"></div>
                                <input type="hidden" value="<?php echo $USER->login_page_background != '' ? $USER->login_page_background : ''; ?>" name="color_login_bg" id="color_login_bg"/>
                              </div>
                              <div class="themename"><?php print_string('background') ?></div>
                              <?php if ($USER->login_page_background != '') { ?>
                                <script type="text/javascript">
                                  $('#LoginBgColor div').css('backgroundColor', '#' + '<?php echo $USER->login_page_background; ?>');    
                                </script>
                              <?php } ?>
                            </td>
                          </tr>   
                        </tbody></table>
                      <div class="tip" style="border-bottom:1px solid #ccc;padding-top:20px;"><?php print_string('addicon') ?></div>
                      <table cellspacing="5" cellpadding="5">
                        <tbody><tr>
                            <td valign="middle" align="center">
                              <?php if ($USER->favicon != '') { ?>
                                <img height="16" width="16" alt="" src="<?php echo $USER->favicon ?>">
                              <?php } ?>
                            </td>
                            <td valign="middle" align="center">
                              <?php if ($USER->ipad_icon != '') { ?>
                                <img height="57" width="57" alt="" src="<?php echo $USER->ipad_icon ?>">
                              <?php } ?>
                            </td>
                          </tr>
                          <tr>
                            <td align="center">
                              <div>
                                <div class="padded tip"><b>Favicon</b> (<a onclick="$('.favicon-upload').slideDown()" href="javascript:void(0);"><?php print_string('change') ?></a>)</div>
                                <div style="width:300px; text-align:left">
                                  <div class="tip"><?php print_string('yourfacicon') ?></div>
                                  <div style="display:none;padding:15px; background:#eee" class="favicon-upload"> 
                                    <input type="file" name="file_favicon" id="file_favicon">
                                    <ul style="list-style-type:none;" class="tip">
                                      <li><?php print_string('accepttype') ?><span class="label">.ico</span></li>
                                      <li><?php print_string('maxfile') ?><span class="label"> 200kb</span></li>
                                    </ul>
                                  </div>
                                </div> 
                              </div> 
                            </td>
                            <td align="center">
                              <div> 
                                <div class="padded tip"><b>iPad/ iPhone icon</b> (<a onclick="$('.iDevice-upload').slideDown()" href="javascript:void(0);"><?php print_string('change') ?></a>)</div>
                                <div style="width:300px; text-align:left">
                                  <div class="tip"><?php print_string('ipadicon') ?></div>
                                  <div style="display:none;padding:15px; background:#eee" class="iDevice-upload"> 
                                    <input type="file" name="file_ipad" id="file_ipad">
                                    <ul style="list-style-type:none;" class="tip">
                                      <li><?php print_string('accepttype') ?><span class="label">.png</span></li>
                                      <li><?php print_string('maxfile') ?><span class="label"> 200kb</span></li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </tbody></table> 
                      <div style="width:200px; height:200px; position:absolute;display:none;" class="colorpicker-container">
                        <div id="colorpickerHolder"><div class="colorpicker" id="collorpicker_359" style="display: block; position: relative;"><div class="colorpicker_color" style="background-color: rgb(255, 0, 255);"><div><div style="left: 0px; top: 150px;"></div></div></div><div class="colorpicker_hue"><div style="top: 25px;"></div></div><div class="colorpicker_new_color" style="background-color: rgb(0, 0, 0);"></div><div class="colorpicker_current_color" style="background-color: rgb(0, 0, 0);"></div><div class="colorpicker_hex"><span>#</span><input type="text" size="6" maxlength="6"></div><div class="colorpicker_rgb_r colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_rgb_g colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_rgb_b colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_hsb_h colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_hsb_s colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_hsb_b colorpicker_field"><input type="text" size="3" maxlength="3"><span></span></div><div class="colorpicker_submit"></div><div class="colopicker_done"><input type="button" value="Clear" onclick="" class="cp_clear"><input type="button" value="Done" onclick=""></div></div></div>
                      </div>
                      <div class="form-buttons">
                        <input type="submit" class="btnlarge" value="<?php print_string('save', 'admin') ?>">
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </td>
          <td class="admin-fullscreen-right">
            <div class="admin-fullscreen-content">
            </div>
          </td>
        </tr>
      </tbody></table>
  </div>
<?php } ?>
