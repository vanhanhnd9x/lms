<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');
require_login(0, false);
require('lib.php');
$PAGE->set_title("Account");
$PAGE->set_heading("Account");

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$account_id = optional_param('id', 0, PARAM_INT);
global $CFG;

?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        
        
        
        <table class="admin-fullscreen">
            <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        

     <div class="focus-panel">
        <div class="panel-head">
            <h3>
                Account
            </h3>
        </div>
         <div class="body">
             
<div class="grid-tabs">
    <ul>
        <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>">Profile</a></li>
        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class="">Theme</a></li>
        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class="">Billing</a></li>
        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class="">Messages &amp; Labels</a></li>                 
        <li><a href="<?php echo $CFG->wwwroot ?>/account/email_setup.php?id=<?php echo $account_id; ?>" class="selected">Email Setup</a></li> 
        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class="">Ecommerce</a></li>  
        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class="">Terms</a></li>  
        <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>
    </ul>
    <div class="clear">
    </div>
</div>

<form method="post" action="/settings/emails">
             <p>All emails sent by Alphatechnologies are sent from <b>system@Alphatechnologies.com</b> but you can specify your own <b>reply-to</b> address so that if your learners reply to one of the emails you will know about it.</p>
                            <table class="simple-form">
                                <tbody><tr>
                                    <th>
                                        Reply-To Address:
                                    </th>
                                    <td>
                                            <input type="text" value="quang.nguyencong@gmail.com" size="40" name="ReplyToAddress" id="ReplyToAddress">
                                            <span class="tip">

                 
                 eg. hr@mycompany.com</span>
                                    </td>
                                </tr>
                                </tbody></table>   
                                <div class="form-buttons">
                                <input type="submit" class="btnlarge" value="Save">
                                </div>                                          
              </form>
             
             
             <div class="form-sub-heading">
                 Create a custom email template
             </div>
             <table class="item-page-list">
                 
                 <tbody><tr>
                     <td>
                         <div class="title">
                             <a href="/settings/email/1">Password reset</a></div>
                         <div class="tip">
                             Sent to a person when they request a password reset</div>
                     </td>
                 </tr>
                 
                 <tr>
                     <td>
                         <div class="title">
                             <a href="/settings/email/14">New user welcome message &amp; login link</a></div>
                         <div class="tip">
                             Sent to a person when they're first setup</div>
                     </td>
                 </tr>
                 
                 <tr>
                     <td>
                         <div class="title">
                             <a href="/settings/email/15">New course assigned</a></div>
                         <div class="tip">
                             Sent to a person when they're assigned to a course</div>
                     </td>
                 </tr>
                 
                 <tr>
                     <td>
                         <div class="title">
                             <a href="/settings/email/17">People assigned to a Team</a></div>
                         <div class="tip">
                             Sent to a person when they're assigned to a team</div>
                     </td>
                 </tr>
                 
             </tbody></table>
         </div>
     </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        
                        
                    </div>
                </td>
            </tr>
        </tbody></table>
        
        
    </div>