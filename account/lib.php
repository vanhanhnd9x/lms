<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function update_profile($user_id,$name,$state,$postcode,                                                      
          $website,$contact_person,$contact_person_email,                                          
          $contact_person_phone,                                          
          $contact_person_fax,                                            
          $custom_website_address,$own_domain_name){
	global $DB;
	$record = new stdClass();
	$record->id = $user_id;
        $record->username = $name;
	$record->state = $state;
	$record->postcode = $postcode;
	$record->website = $website;
	$record->contact_person = $contact_person;
	$record->contact_person_email = $contact_person_email;
	$record->contact_person_phone = $contact_person_phone;
	$record->contact_person_fax = $contact_person_fax;
	$record->custom_website_address = $custom_website_address;
	$record->own_domain_name = $own_domain_name;

	return $DB->update_record('user', $record, false);
}
function update_user_theme($user_id,$show_logo_on_login,$show_logo_on_trainee_toolbar,
	$color_title_bar,
	$color_title_bar_text,$color_navigation_heading,$color_heading_text,$color_login_bg,$font){
	global $DB;
	$record = new stdClass();
	$record->id = $user_id;
	$record->show_logo_on_login = $show_logo_on_login;
	$record->show_logo_on_trainee_toolbar = $show_logo_on_trainee_toolbar;
	$record->title_bar_color = $color_title_bar;
	$record->title_bar_text_color = $color_title_bar_text;
	$record->navigation_heading_color = $color_navigation_heading;
	$record->heading_text_color = $color_heading_text;
	$record->login_page_background = $color_login_bg;
	$record->font = $font;
	return $DB->update_record('user', $record, false);
}

function get_user_from_user_id($user_id){
	 global $DB;
	$user = $DB->get_record('user', array(
		'id' => $user_id
		));

	return $user;
}
function update_user_logo($user_id,$file_name){
    global $DB;
    $record = new stdClass();
	$record->id = $user_id;
	$record->picture = 1;
	$record->url = $file_name;

	return $DB->update_record('user', $record, false);
}
function update_user_favico($user_id,$file_name){
    global $DB;
    $record = new stdClass();
	$record->id = $user_id;
	$record->favicon = $file_name;
	

	return $DB->update_record('user', $record, false);
}
function update_user_ipad_icon($user_id,$file_name){
    global $DB;
    $record = new stdClass();
	$record->id = $user_id;
	$record->ipad_icon = $file_name;
	

	return $DB->update_record('user', $record, false);
}
function displayJsAlert($ms, $redirectPath) {
    $html = '';
    if($ms!=''){
        $html .= '<script language="javascript">alert("' . $ms . '")</script>';
    }
    
    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}
function update_user_label_message($user_id,$hide_message_tab,$unretricted_messages,
	$login_message,$welcome_message,$hide_course_date){
    global $DB;
    $record = new stdClass();
	$record->id = $user_id;
	$record->hide_message_tab = $hide_message_tab;
	$record->unretricted_messages = $unretricted_messages;
	$record->login_message =$login_message;
	$record->welcome_message =$welcome_message;
	$record->hide_course_date =$hide_course_date;
	
	
	return $DB->update_record('user', $record, false);
}
?>
