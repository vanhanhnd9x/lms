<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}
global $CFG;
require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
//require($CFG->dirroot . '/lib/accesslib.php');

$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$message = optional_param('message', '', PARAM_TEXT);
$account_id = optional_param('id', 0, PARAM_INT);
$pid = optional_param('pid', 0, PARAM_INT);
?>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<!--<script type="text/javascript" src="js/jquery.autocomplete.js"></script>-->
<script type="text/javascript" src="js/dataTables.tableTools.js"></script>
<script type="text/javascript" src="js/dataTables.editor.js"></script>


<link type="text/css" rel="stylesheet" href="css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="css/dataTables.tableTools.css">
<link type="text/css" rel="stylesheet" href="css/dataTables.editor.min.css">

<script type="text/javascript" class="init">
  /* Custom filtering function which will search data in column four between two values */
  $.fn.dataTable.ext.search.push(
  function( settings, data, dataIndex ) {
    var min = parseInt( $('#min').val(), 10 );
    var max = parseInt( $('#max').val(), 10 );
    var cost = parseFloat( data[2] ) || 0; // use data for the age column
 
    if ( ( isNaN( min ) && isNaN( max ) ) ||
      ( isNaN( min ) && cost <= max ) ||
      ( min <= cost   && isNaN( max ) ) ||
      ( min <= cost   && cost <= max ) )
    {
      return true;
    }
    return false;
  }
);
  var editor; // use a global for the submit and return data rendering in the examples

  $(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
      ajax: "staff.php",
      table: "#example",
      fields: [ {
          label: "Tên:",
          name: "firstname"
        }, {
          label: "Họ:",
          name: "lastname"
        }, {
          label: "Gói đăng ký:",
          name: "spid"
        }, {
          label: "Chi phí:",
          name: "p_cost"
        }, {
          label: "Từ ngày:",
          name: "p_start"
        }, {
          label: "Đến ngày:",
          name: "p_end",
          type: "date"
        }, {
          label: "PT Thanh toán:",
          name: "p_method"
        },{
          label: "Trạng thái:",
          name: "status"
        }
      ]
    } );

    $('#example').DataTable( {
      dom: "Tfrtip",
      ajax: "staff.php",
      columns: [
        { data: null, render: function ( data, type, row ) {
            // Combine the first and last names into a single table field
            return data.firstname+' '+data.lastname;
          } },
        { data: "spid" },
        { data: "p_cost", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) },
        { data: "p_start" },
        { data: "p_end" },
        { data: "p_method" },
        { data: "status" }
      ],
      tableTools: {
        sRowSelect: "os",
        aButtons: [
          { sExtends: "editor_create", editor: editor },
          { sExtends: "editor_edit",   editor: editor },
          { sExtends: "editor_remove", editor: editor }
        ]
      }
    } );
  } );
</script>
<script type="text/javascript">
  function update_pack(v){
    $("#updatepack").val(v);
    alert("Bạn đã thực hiện gia hạn thành công.");
    //    var url = "billing.php?message=yes";  
    //    window.location("billing.php?message=yes");
  }
  
</script>

<div id="page-body">        
  <?php
  global $USER;
  $user_id = $USER->id;
  $payment = get_payment_user_id($account_id);

$payment_super = get_payment_user_superadmin();
//var_dump($payment_super);
$payment_super = array_values($payment_super);
$t1->data = $payment_super;
header("Content-type: application/json");
echo json_encode($t1);


  ?>

  <table class="admin-fullscreen">
    <tbody><tr>
        <td class="admin-fullscreen-left">
          <div class="admin-fullscreen-content">

            <div class="focus-panel">
              <div class="panel-head">
                <h3>
                  <?php print_string('account') ?>
                </h3>
              </div>
              <div class="body">

                <div class="grid-tabs">
                  <ul>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('billing') ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                  
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/email_setup.php?id=<?php echo $account_id; ?>" class=""><?php print_string('emailsetup') ?></a></li>  
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>    
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>  
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>  
                  </ul>
                  <div class="clear"></div>
                </div>
                <input id="updatepack" name="updatepack" value="" type="hidden">
                <div class="sys-note">
                  <?php
                  if (!is_siteadmin($account_id)) {
                    if (empty($payment)) {
                      ?>
                      <span> Cảm ơn đã dùng thử LMS. Bạn còn 5 ngày sử dụng.</span><br>
                      <?php
                    }
                    else {
                      $now = time();
                      foreach ($payment as $key1 => $pay1) {
                        $startdate = $pay1->p_start;
                        $enddate = $pay1->p_end;
                        if ($startdate <= $now && $now <= $enddate) {
                          $service_pack1 = get_service_package($pay1->spid);
                          foreach ($service_pack1 as $key2 => $sp1) {
                            $current_pack = $sp1->name;
                            $current_spid = $sp1->spid;
                            $current_cost = $sp1->cost;
                          }
                        }
                      }
                      ?>
                      <span> Bạn đang sử dụng gói dịch vụ <?php echo "<strong>" . $current_pack . "</strong>" ?> Tiếp tục đăng ký gói dịch vụ <a href="billing.php?message=yes" onclick="update_pack(1);"> tại đây.</a></span><br>
                      <span> Hãy nâng cấp để sử dụng gói dịch vụ cao hơn.</span><p>
                      <?php } ?>
                      <?php
                      $updatepack = optional_param('updatepack', 0, PARAM_INT);

                      if (!empty($message)) {

                        $user_id = $USER->id;
                        $status = "Đang chờ";
                        $p_method = "Gia hạn";
                        $spid = $current_spid;

                        $payment_lastest = get_payment_lastest($user_id);
                        if (!empty($payment_lastest)) {
                          $p_start = $payment_lastest->p_end;
                          if ($current_spid <= '4') {
                            $p_end = strtotime("+1 years", $p_start);
                          }
                          else {
                            $p_end = strtotime("+1 months", $p_start);
                          }
                          //insert cost
                          if ($current_spid <= '4') {
                            $cost = $current_cost * 12;
                          }
                          else {
                            $cost = $current_cost;
                          }
                          $p_cost = $cost;
                          try {
                            $insert = insert_payment($user_id, $status, $p_method, $spid, $p_start, $p_end, $p_cost);
                            header("Location: billing.php");
                          } catch (Exception $exc) {
                            echo "Đã xảy ra lỗi. Gia hạn của Bạn chưa thành công. Bạn hãy thực hiện gia hạn 1 lần nữa. Rất tiếc!";
                            header("Location: billing.php");
                          }
                        }
                        ?>
                        <span><strong>Bạn đã gia hạn thành công gói dịch vụ đã đăng ký.</strong> </span>
                      <?php } ?>
                    <p>
                    <div class="history"><h5><?php print_string('history') ?></h5></div>

                    <table id="tb-history">
                      <tr>
                        <th class="th-no"><?php print_string('no') ?></th>
                        <th class="th-action"><?php print_string('action') ?></th>
                        <th class="th-start"><?php print_string('startdate') ?></th>		
                        <th class="th-end"><?php print_string('endate') ?></th>
                        <th class="th-end"><?php print_string('status') ?></th>
                      </tr>
                      <?php
                      foreach ($payment as $key3 => $pay3) {
                        $service_pack2 = get_service_package($pay3->spid);
                        foreach ($service_pack2 as $key4 => $sp4) {
                          $pack_name = $sp4->name;
                          $pack_user = $sp4->user_active;
                        }
                        $p_start = date("d/m/y", $pay3->p_start);
                        $p_end = date("d/m/y", $pay3->p_end);
                        ?>
                        <tr>
                          <td><?php echo $pay3->id ?></td>
                          <td><?php echo "Đăng ký gói dịch vụ: " . $pack_name . "[" . $pack_user . " người dùng]" ?></td>		
                          <td><?php echo $p_start ?></td>
                          <td><?php echo $p_end ?></td>
                          <td><?php echo $pay3->status ?></td>
                        </tr>
                      <?php } ?>
                    </table>
                    <?php
                  }
                  else {
                    ?>
                    <div id="demo"></div>
                    <table border="0" cellspacing="5" cellpadding="5">
                      <tbody><tr>
                          <td>Chi phí nhỏ nhất:</td>
                          <td><input type="text" id="min" name="min"></td>
                        </tr>
                        <tr>
                          <td>Chi phí lớn nhất:</td>
                          <td><input type="text" id="max" name="max"></td>
                        </tr>
                      </tbody></table>

                    <!--                    Table payment-->

                    <table id="example" class="display" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Tên đơn vị</th>
                          <th>Gói đăng ký</th>
                          <th>Chi phí</th>
                          <th>Từ ngày</th>
                          <th>Đến ngày</th>
                          <th>PT Thanh toán</th>
                          <th>Trạng thái</th>
                        </tr>
                      </thead>

                      <tfoot>
                        <tr>
                          <th>Tên đơn vị</th>
                          <th>Gói đăng ký</th>
                          <th>Chi phí</th>
                          <th>Từ ngày</th>
                          <th>Đến ngày</th>
                          <th>PT Thanh toán</th>
                          <th>Trạng thái</th>
                        </tr>
                      </tfoot>
                    </table>
                    <!--#end table payment-->

                    <?php
                  }
                  ?>
                  <!--payment details-->
                  <?php
                  if (isset($pid)) {
                    $paymentedit = get_payment_by_id($pid);
                    ?>
                    <div class="overlay" id="overlay" style="display:none;"></div>
                    <div id="facebox" style="display: none;" >      
                      <div class="popup"><div class="content">
                          <?php ?>
                          <div id="assignItemBox">
                            <div class="assign-header">
                              <span style="color: #fff; font-weight: bolder;">Chỉnh sửa thanh toán <?php echo $paymentedit->id ?></span>
                            </div>
                            <form action="<?php echo $CFG->wwwroot ?>/account/billing.php?action=editpayment" method="post">
                              <div id="assignResults">
                                <table cellspacing="0" class="item-page-list">
                                  <?php ?>
                                  <tbody>
                                  <fieldset>
                                    <tr>
                                      <td><label for="name">Gói đăng ký</label></td>
                                      <td><input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"></td>
                                    </tr>
                                    <tr>
                                      <td><label for="cost">Chi phí</label></td>
                                      <td><input type="text" name="cost" id="cost" value="" class="text ui-widget-content ui-corner-all"></td>
                                    </tr>
                                    <tr>
                                      <td><label for="startdate">Ngày bắt đầu</label></td>
                                      <td>
                                        <select name="method">
                                          <option class="selected" value="Hằng năm">Hằng năm</option>
                                          <option value="Hằng tháng">Hằng tháng</option>
                                        </select>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td><label for="method">Phương thức thanh toán</label></td>
                                      <td><input type="text" name="useractive" id="useractive" value="" class="text ui-widget-content ui-corner-all"></td>
                                    </tr>
                                    <tr>
                                      <td><label for="status">Trạng thái</label></td>
                                      <td>
                                        <select name="email">
                                          <option class="selected" value="Không">Không</option>
                                          <option value="Có">Có</option>
                                        </select>
                                      </td>
                                    </tr>
                                    <!-- Allow form submission with keyboard without duplicating the dialog button -->
                                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                                  </fieldset>
                                  </tbody>
                                </table>

                              </div>
                              <div class="assign-footer-opt clearfix hidden">
                                <span id="assignEmails"><input type="checkbox" name="SendEmail" id="SendEmail" checked> <label for="SendEmail"><?php print_r(get_string('sendemailnoti')) ?></label></span>
                              </div>
                              <div class="assign-footer clearfix">
                                <div class="float-left">
                                  <input type="submit" class="big-button drop" value="<?php print_string('save', 'admin') ?>" />

                                  <?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                              </div>
                            </form>


                          </div></div> </div></div>
                    <?php
                  }
                  ?>

                  <!--#end payment details-->


                </div> 
              </div>
            </div>
          </div>
        </td>
        <td class="admin-fullscreen-right">
          <div class="admin-fullscreen-content">

            <div class="side-panel">
              <?php
              if (!is_siteadmin()) {
                ?>
                <div class="action-buttons">
                  <ul>
                    <li><a href="pricingplan.php" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span class="icon_go"><?php print_string('upgrade') ?></span></span></span></span></a></li>
                  </ul>
                </div>
                <?php
              }
              ?>
            </div>
          </div>


        </td>
      </tr>
    </tbody></table>


</div>


