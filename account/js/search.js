$(document).ready(function(){
 

 
$('#enable_custom_domain').click(function() {
        if(this.checked) {
           //alert('checked');
					 
					 $("#own_domain_name").removeAttr("disabled"); 
					 $("#custom_website_address").attr("disabled", "disabled");
					 document.getElementById("custom_website_address").value='';
            
        }
        else { 
					$("#custom_website_address").removeAttr("disabled"); 
          $("#own_domain_name").attr("disabled", "disabled"); 
					document.getElementById("own_domain_name").value='';
        }
  
    });
		
	$('#show_logo_on_login').click(function() {
        if(this.checked) {
           
					 document.getElementById("show_logo_on_login").value=1;
        }
        else { 
					document.getElementById("show_logo_on_login").value=0;
        }
    });
		
$('#show_logo_on_trainee_toolbar').click(function() {
        if(this.checked) {
           
					 document.getElementById("show_logo_on_trainee_toolbar").value=1;
        }
        else { 
					document.getElementById("show_logo_on_trainee_toolbar").value=0;
        }
    });

		
	$("#change_font").change(function () {
		
		if($(this).val()==1){//arial
						$("body").css("font-family","Arial") ;
						
		}
		if($(this).val()==2){//tahoma
						$("body").css("font-family","Tahoma") ;
						
		}
		if($(this).val()==3){//Verdana
						$("body").css("font-family","Verdana") ;
						
		}
		if($(this).val()==4){//Helvetica
						$("body").css("font-family","Helvetica") ;
						
		}
		
	});
	
	$("#theme_color").change(function () {
					
					if($(this).val()==3){//green
						$('.super-nav-bg').css('backgroundColor', '#90BD63');
						$('#prev-heading').css('backgroundColor', '#90BD63');
						
						$('#TabBgColor div').css('backgroundColor', '#90BD63');
						$('#ToolbarBgColor div').css('backgroundColor', '#4D4D4D');
						
						
					}
					if($(this).val()==4){//grey
						$('.super-nav-bg').css('backgroundColor', '#525252');
						$('#prev-heading').css('backgroundColor', '#525252');
						
						$('#ToolbarBgColor div').css('backgroundColor', '#525252');
						$('#ToolbarTextColor div').css('backgroundColor', '#EDEDED');
						$('#TabBgColor div').css('backgroundColor', '#858585');
					}
					if($(this).val()==5){//blue
						
						$('.super-nav-bg').css('backgroundColor', '#274873');
						$('#prev-heading').css('backgroundColor', '#274873');
						
						$('#ToolbarBgColor div').css('backgroundColor', '#274873');
						$('#ToolbarTextColor div').css('backgroundColor', '#E0DCE0');
						$('#TabTextColor div').css('backgroundColor', '#E1E6FA');
						
						
					}
					if($(this).val()==6){//red
												
						$('.super-nav-bg').css('backgroundColor', '#274873');
						$('#prev-heading').css('backgroundColor', '#274873');
						
						$('#ToolbarBgColor div').css('backgroundColor', '#D1D1D1');
						$('#ToolbarTextColor div').css('backgroundColor', '#941221');
						
						$('#TabBgColor div').css('backgroundColor', '#B31023');
						$('#TabTextColor div').css('backgroundColor', '#E6E8EB');
					}
					if($(this).val()==7){//yellow
						$('.super-nav-bg').css('backgroundColor', '#E0D42D');
						$('#prev-heading').css('backgroundColor', '#E0D42D');
						
						$('#ToolbarBgColor div').css('backgroundColor', '#707070');
						$('#ToolbarTextColor div').css('backgroundColor', '#F5F5F5');
						
						$('#TabBgColor div').css('backgroundColor', '#E0D42D');
						$('#TabTextColor div').css('backgroundColor', '#383838');
					}
					if($(this).val()==8){//black
						$('.super-nav-bg').css('backgroundColor', '#000000');
						$('#prev-heading').css('backgroundColor', '#000000');
						
						$('#ToolbarBgColor div').css('backgroundColor', '#000000');
						$('#ToolbarTextColor div').css('backgroundColor', '#FFFFFF');
						
						$('#TabBgColor div').css('backgroundColor', '#000000');
						$('#TabTextColor div').css('backgroundColor', '#FFFFFF');
					}
					if($(this).val()==70){//Dark side
						$('.super-nav-bg').css('backgroundColor', '#393945');
						$('#prev-heading').css('backgroundColor', '#393945');
						
						$('#ToolbarBgColor div').css('backgroundColor', '#000000');
						$('#ToolbarTextColor div').css('backgroundColor', '#FFFFFF');
						
						$('#TabBgColor div').css('backgroundColor', '#393945');
						$('#TabTextColor div').css('backgroundColor', '#FFFFFF');
					}
					if($(this).val()==189){//Default
						$('.super-nav-bg').css('backgroundColor', '#90BD63');
						$('#prev-heading').css('backgroundColor', '#90BD63');
						
						$('#TabBgColor div').css('backgroundColor', '#90BD63');
						$('#ToolbarBgColor div').css('backgroundColor', '#4D4D4D');
					}
					if($(this).val()==782){//Custom theme
						$('.super-nav-bg').css('backgroundColor', '#90BD63');
						$('#prev-heading').css('backgroundColor', '#90BD63');
						
						$('#TabBgColor div').css('backgroundColor', '#90BD63');
						$('#ToolbarBgColor div').css('backgroundColor', '#4D4D4D');
					}
					
					
					
        });

    
    $('#ToolbarBgColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
            
                $('#ToolbarBgColor div').css('backgroundColor', '#' + hex);
                document.getElementById('color_title_bar').value=hex;
            
		
	}
});


$('#ToolbarTextColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#ToolbarTextColor div').css('backgroundColor', '#' + hex);
                document.getElementById('color_title_bar_text').value=hex;
	}
});

$('#TabBgColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#TabBgColor div').css('backgroundColor', '#' + hex);
                document.getElementById('color_navigation_heading').value=hex;
	}
});

$('#TabTextColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#TabTextColor div').css('backgroundColor', '#' + hex);
                document.getElementById('color_heading_text').value=hex;
	}
});
$('#LoginBgColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#LoginBgColor div').css('backgroundColor', '#' + hex);
                document.getElementById('color_login_bg').value=hex;
	}
});

  
});


function validate(){
    var error=0;
    
    
    if(document.getElementById("name")&&document.getElementById("name").value==''){
        
        $("#erUsername").show();
        error=1;	
    }
    
   
    if(error==1){
        return false;
    }
    return true;
    
}

function displayConfirmBox(redirectPath,id,ms){
    var where_to= confirm(ms);
    if (where_to== true)
    {
        window.location=redirectPath;
    }
    else
    {
	
        return false;
    }
}

function select_account(userid,url){
    window.location = url+"/account/index.php?id="+userid;
}

