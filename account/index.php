<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$user_id = optional_param('user_id', '', PARAM_TEXT);
$account_name = optional_param('account_name', '', PARAM_TEXT);
$address = optional_param('address', '', PARAM_TEXT);
$city = optional_param('city', '', PARAM_TEXT);
$state = optional_param('state', '', PARAM_TEXT);
$country = optional_param('country', '', PARAM_TEXT);
$postcode = optional_param('postcode', '', PARAM_TEXT);
$website = optional_param('website', '', PARAM_TEXT);
$contact_person = optional_param('contact_person', '', PARAM_TEXT);
$contact_person_email = optional_param('contact_person_email', '', PARAM_TEXT);
$contact_person_phone = optional_param('contact_person_phone', '', PARAM_TEXT);
$contact_person_fax = optional_param('contact_person_fax', '', PARAM_TEXT);
$timezone = optional_param('Organisation.TimeZone', '', PARAM_TEXT);
$custom_website_address = optional_param('custom_website_address', '', PARAM_TEXT);
$own_domain_name = optional_param('own_domain_name', '', PARAM_TEXT);
$account_id = optional_param('id', 2, PARAM_INT);
$rows = select_account();
global $CFG;
?>
<script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/common/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        
  <?php
  if (is_siteadmin()) {
    if ($action == 'update_profile') {
      update_profile($user_id, $account_name, $address, $city, $state, $country, $postcode, $website, $contact_person, $contact_person_email, $contact_person_phone, $contact_person_fax, $timezone, $custom_website_address, $own_domain_name);
//$SELECT=get_user_from_user_id($account_id);
      $SELECT = select_account_id($account_id);
    }
    else {
//	global $USER;
      //$USER=get_user_from_user_id($account_id);
      //global  $USE;
      $SELECT = select_account_id($account_id);
    }
    ?>
    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">

              <div class="focus-panel">
                <div class="panel-head">
                  <div style="float: right">
                    <select name="sl_account" onchange="select_account(this.value,'<?php echo $CFG->wwwroot ?>');">
                      <!--                                                <option value="" selected="selected">----select----</option>-->
                      <?php
                      foreach ($rows as $row) {
                        ?>
                        <option value="<?php echo $row->id; ?>" <?php echo $row->id == $account_id ? 'selected="selected"' : ''; ?>><?php echo $row->username; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <h3>
                    <?php print_string('account') ?>
                  </h3>
                </div>
                <div class="body">

                  <div class="grid-tabs">
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('profile') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class=""><?php print_string('billing') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>
                    </ul>
                    <div class="clear">
                    </div>
                  </div>
                  <?php
                  foreach ($SELECT as $USE) {
                    ?>
                    <form method="post" action="<?php echo $CFG->wwwroot ?>/account/index.php?action=update_profile&id=<?php echo $account_id; ?>" onsubmit="return validate();">
                      <input type="hidden" value="<?php echo $USE->id ?>" name="user_id" id="user_id">
                      <div style="margin-left: 6px">               
                        <table class="simple-form">
                          <tbody><tr>
                              <th style="text-align:left;">
                                <?php print_string('accountnam') ?>
                              </th>
                              <td>
                                <div id="erUsername" class="alert" style="display: none;">
                                  <?php print_string('pleaseenter') ?></div>    
                                <input type="text" value="<?php echo $USE->company ?>" style="width:300px;" name="account_name" id="name">

                              </td>
                            </tr>

                            <tr>
                              <th style="text-align:left;">
                                <?php print_r(get_string('address')) ?>
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->address ?>" name="address" id="address">
                              </td>
                            </tr>

                            <tr>
                              <th style="text-align:left;">
                                <?php print_r(get_string('city')) ?>
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->city ?>" name="city" id="city">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                <?php print_r(get_string('state')) ?>
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->state ?>" name="state" id="state">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                <?php print_r(get_string('country')) ?>
                              </th>
                              <td>
                                <select name="country" id="country">
                                  <option value="AF" <?php echo $USE->country == 'AF' ? 'selected="selected"' : ''; ?>>Afghanistan</option>
                                  <option value="AL" <?php echo $USE->country == 'AL' ? 'selected="selected"' : ''; ?>>Albania</option>
                                  <option value="DZ" <?php echo $USE->country == 'DZ' ? 'selected="selected"' : ''; ?>>Algeria</option>
                                  <option value="AS" <?php echo $USE->country == 'AS' ? 'selected="selected"' : ''; ?>>American Samoa</option>
                                  <option value="AD" <?php echo $USE->country == 'AD' ? 'selected="selected"' : ''; ?>>Andorra</option>
                                  <option value="AO" <?php echo $USE->country == 'AO' ? 'selected="selected"' : ''; ?>>Angola</option>
                                  <option value="AI" <?php echo $USE->country == 'AI' ? 'selected="selected"' : ''; ?>>Anguilla</option>
                                  <option value="AQ" <?php echo $USE->country == 'AQ' ? 'selected="selected"' : ''; ?>>Antarctica</option>
                                  <option value="AG" <?php echo $USE->country == 'AG' ? 'selected="selected"' : ''; ?>>Antigua and Barbuda</option>
                                  <option value="AR" <?php echo $USE->country == 'AR' ? 'selected="selected"' : ''; ?>>Argentina</option>
                                  <option value="AM" <?php echo $USE->country == 'AM' ? 'selected="selected"' : ''; ?>>Armenia</option>
                                  <option value="AW" <?php echo $USE->country == 'AW' ? 'selected="selected"' : ''; ?>>Aruba</option>
                                  <option value="AU" <?php echo $USE->country == 'AU' ? 'selected="selected"' : ''; ?>>Australia</option>
                                  <option value="AT" <?php echo $USE->country == 'AT' ? 'selected="selected"' : ''; ?>>Austria</option>
                                  <option value="AZ" <?php echo $USE->country == 'AZ' ? 'selected="selected"' : ''; ?>>Azerbaijan</option>
                                  <option value="BS" <?php echo $USE->country == 'BS' ? 'selected="selected"' : ''; ?>>Bahamas</option>
                                  <option value="BH" <?php echo $USE->country == 'BH' ? 'selected="selected"' : ''; ?>>Bahrain</option>
                                  <option value="BD" <?php echo $USE->country == 'BD' ? 'selected="selected"' : ''; ?>>Bangladesh</option>
                                  <option value="BB" <?php echo $USE->country == 'BB' ? 'selected="selected"' : ''; ?>>Barbados</option>
                                  <option value="BY" <?php echo $USE->country == 'BY' ? 'selected="selected"' : ''; ?>>Belarus</option>
                                  <option value="BE" <?php echo $USE->country == 'BE' ? 'selected="selected"' : ''; ?>>Belgium</option>
                                  <option value="BZ" <?php echo $USE->country == 'BZ' ? 'selected="selected"' : ''; ?>>Belize</option>
                                  <option value="BJ" <?php echo $USE->country == 'BJ' ? 'selected="selected"' : ''; ?>>Benin</option>
                                  <option value="BM" <?php echo $USE->country == 'BM' ? 'selected="selected"' : ''; ?>>Bermuda</option>
                                  <option value="BT" <?php echo $USE->country == 'BT' ? 'selected="selected"' : ''; ?>>Bhutan</option>
                                  <option value="BO" <?php echo $USE->country == 'BO' ? 'selected="selected"' : ''; ?>>Bolivia</option>
                                  <option value="BA" <?php echo $USE->country == 'BA' ? 'selected="selected"' : ''; ?>>Bosnia and Herzegovina</option>
                                  <option value="BW" <?php echo $USE->country == 'BW' ? 'selected="selected"' : ''; ?>>Botswana</option>
                                  <option value="BV" <?php echo $USE->country == 'BV' ? 'selected="selected"' : ''; ?>>Bouvet Island</option>
                                  <option value="BR" <?php echo $USE->country == 'BR' ? 'selected="selected"' : ''; ?>>Brazil</option>
                                  <option value="BN" <?php echo $USE->country == 'BN' ? 'selected="selected"' : ''; ?>>Brunei Darussalam</option>
                                  <option value="BG" <?php echo $USE->country == 'BG' ? 'selected="selected"' : ''; ?>>Bulgaria</option>
                                  <option value="BF" <?php echo $USE->country == 'BF' ? 'selected="selected"' : ''; ?>>Burkina Faso</option>
                                  <option value="BI" <?php echo $USE->country == 'BI' ? 'selected="selected"' : ''; ?>>Burundi</option>
                                  <option value="KH" <?php echo $USE->country == 'KH' ? 'selected="selected"' : ''; ?>>Cambodia</option>
                                  <option value="CM" <?php echo $USE->country == 'CM' ? 'selected="selected"' : ''; ?>>Cameroon</option>
                                  <option value="CA" <?php echo $USE->country == 'CA' ? 'selected="selected"' : ''; ?>>Canada</option>
                                  <option value="CV" <?php echo $USE->country == 'CV' ? 'selected="selected"' : ''; ?>>Cape Verde</option>
                                  <option value="KY" <?php echo $USE->country == 'KY' ? 'selected="selected"' : ''; ?>>Cayman Islands</option>
                                  <option value="CF" <?php echo $USE->country == 'CF' ? 'selected="selected"' : ''; ?>>Central African Republic</option>
                                  <option value="TD" <?php echo $USE->country == 'TD' ? 'selected="selected"' : ''; ?>>Chad</option>
                                  <option value="CL" <?php echo $USE->country == 'CL' ? 'selected="selected"' : ''; ?>>Chile</option>
                                  <option value="CN" <?php echo $USE->country == 'CN' ? 'selected="selected"' : ''; ?>>China</option>
                                  <option value="CX" <?php echo $USE->country == 'CX' ? 'selected="selected"' : ''; ?>>Christmas Island</option>
                                  <option value="CC" <?php echo $USE->country == 'CC' ? 'selected="selected"' : ''; ?>>Cocos Islands</option>
                                  <option value="CO" <?php echo $USE->country == 'CO' ? 'selected="selected"' : ''; ?>>Colombia</option>
                                  <option value="KM" <?php echo $USE->country == 'KM' ? 'selected="selected"' : ''; ?>>Comoros</option>
                                  <option value="CD" <?php echo $USE->country == 'CD' ? 'selected="selected"' : ''; ?>>Congo</option>
                                  <option value="CG" <?php echo $USE->country == 'CG' ? 'selected="selected"' : ''; ?>>Congo</option>
                                  <option value="CK" <?php echo $USE->country == 'CK' ? 'selected="selected"' : ''; ?>>Cook Islands</option>
                                  <option value="CR" <?php echo $USE->country == 'CR' ? 'selected="selected"' : ''; ?>>Costa Rica</option>
                                  <option value="CI" <?php echo $USE->country == 'CI' ? 'selected="selected"' : ''; ?>>Cote D'Ivoire</option>
                                  <option value="HR" <?php echo $USE->country == 'HR' ? 'selected="selected"' : ''; ?>>Croatia</option>
                                  <option value="CU" <?php echo $USE->country == 'CU' ? 'selected="selected"' : ''; ?>>Cuba</option>
                                  <option value="CY" <?php echo $USE->country == 'CY' ? 'selected="selected"' : ''; ?>>Cyprus</option>
                                  <option value="CZ" <?php echo $USE->country == 'CZ' ? 'selected="selected"' : ''; ?>>Czech Republic</option>
                                  <option value="DK" <?php echo $USE->country == 'DK' ? 'selected="selected"' : ''; ?>>Denmark</option>
                                  <option value="DJ" <?php echo $USE->country == 'DJ' ? 'selected="selected"' : ''; ?>>Djibouti</option>
                                  <option value="DM" <?php echo $USE->country == 'DM' ? 'selected="selected"' : ''; ?>>Dominica</option>
                                  <option value="DO" <?php echo $USE->country == 'DO' ? 'selected="selected"' : ''; ?>>Dominican Republic</option>
                                  <option value="EC" <?php echo $USE->country == 'EC' ? 'selected="selected"' : ''; ?>>Ecuador</option>
                                  <option value="EG" <?php echo $USE->country == 'EG' ? 'selected="selected"' : ''; ?>>Egypt</option>
                                  <option value="SV" <?php echo $USE->country == 'SV' ? 'selected="selected"' : ''; ?>>El Salvador</option>
                                  <option value="GQ" <?php echo $USE->country == 'GQ' ? 'selected="selected"' : ''; ?>>Equatorial Guinea</option>
                                  <option value="ER" <?php echo $USE->country == 'ER' ? 'selected="selected"' : ''; ?>>Eritrea</option>
                                  <option value="EE" <?php echo $USE->country == 'EE' ? 'selected="selected"' : ''; ?>>Estonia</option>
                                  <option value="ET" <?php echo $USE->country == 'ET' ? 'selected="selected"' : ''; ?>>Ethiopia</option>
                                  <option value="FK" <?php echo $USE->country == 'FK' ? 'selected="selected"' : ''; ?>>Falkland Islands</option>
                                  <option value="FO" <?php echo $USE->country == 'FO' ? 'selected="selected"' : ''; ?>>Faroe Islands</option>
                                  <option value="FJ" <?php echo $USE->country == 'FJ' ? 'selected="selected"' : ''; ?>>Fiji</option>
                                  <option value="FI" <?php echo $USE->country == 'FI' ? 'selected="selected"' : ''; ?>>Finland</option>
                                  <option value="FR" <?php echo $USE->country == 'FR' ? 'selected="selected"' : ''; ?>>France</option>
                                  <option value="GF" <?php echo $USE->country == 'GF' ? 'selected="selected"' : ''; ?>>French Guiana</option>
                                  <option value="PF" <?php echo $USE->country == 'PF' ? 'selected="selected"' : ''; ?>>French Polynesia</option>
                                  <option value="GA" <?php echo $USE->country == 'GA' ? 'selected="selected"' : ''; ?>>Gabon</option>
                                  <option value="GM" <?php echo $USE->country == 'GM' ? 'selected="selected"' : ''; ?>>Gambia</option>
                                  <option value="GE" <?php echo $USE->country == 'GE' ? 'selected="selected"' : ''; ?>>Georgia</option>
                                  <option value="DE" <?php echo $USE->country == 'DE' ? 'selected="selected"' : ''; ?>>Germany</option>
                                  <option value="GH" <?php echo $USE->country == 'GH' ? 'selected="selected"' : ''; ?>>Ghana</option>
                                  <option value="GI" <?php echo $USE->country == 'GI' ? 'selected="selected"' : ''; ?>>Gibraltar</option>
                                  <option value="GR" <?php echo $USE->country == 'GR' ? 'selected="selected"' : ''; ?>>Greece</option>
                                  <option value="GL" <?php echo $USE->country == 'GL' ? 'selected="selected"' : ''; ?>>Greenland</option>
                                  <option value="GD" <?php echo $USE->country == 'GD' ? 'selected="selected"' : ''; ?>>Grenada</option>
                                  <option value="GP" <?php echo $USE->country == 'GP' ? 'selected="selected"' : ''; ?>>Guadeloupe</option>
                                  <option value="GU" <?php echo $USE->country == 'GU' ? 'selected="selected"' : ''; ?>>Guam</option>
                                  <option value="GT" <?php echo $USE->country == 'GT' ? 'selected="selected"' : ''; ?>>Guatemala</option>
                                  <option value="GN" <?php echo $USE->country == 'GN' ? 'selected="selected"' : ''; ?>>Guinea</option>
                                  <option value="GW" <?php echo $USE->country == 'GW' ? 'selected="selected"' : ''; ?>>Guinea-Bissau</option>
                                  <option value="GY" <?php echo $USE->country == 'GY' ? 'selected="selected"' : ''; ?>>Guyana</option>
                                  <option value="HT" <?php echo $USE->country == 'HT' ? 'selected="selected"' : ''; ?>>Haiti</option>
                                  <option value="HN" <?php echo $USE->country == 'HN' ? 'selected="selected"' : ''; ?>>Honduras</option>
                                  <option value="HK" <?php echo $USE->country == 'HK' ? 'selected="selected"' : ''; ?>>Hong Kong</option>
                                  <option value="HU" <?php echo $USE->country == 'HU' ? 'selected="selected"' : ''; ?>>Hungary</option>
                                  <option value="IS" <?php echo $USE->country == 'IS' ? 'selected="selected"' : ''; ?>>Iceland</option>
                                  <option value="IN" <?php echo $USE->country == 'IN' ? 'selected="selected"' : ''; ?>>India</option>
                                  <option value="ID" <?php echo $USE->country == 'ID' ? 'selected="selected"' : ''; ?>>Indonesia</option>
                                  <option value="IR" <?php echo $USE->country == 'IR' ? 'selected="selected"' : ''; ?>>Iran</option>
                                  <option value="IQ" <?php echo $USE->country == 'IQ' ? 'selected="selected"' : ''; ?>>Iraq</option>
                                  <option value="IE" <?php echo $USE->country == 'IE' ? 'selected="selected"' : ''; ?>>Ireland</option>
                                  <option value="IL" <?php echo $USE->country == 'IL' ? 'selected="selected"' : ''; ?>>Israel</option>
                                  <option value="IT" <?php echo $USE->country == 'IT' ? 'selected="selected"' : ''; ?>>Italy</option>
                                  <option value="JM" <?php echo $USE->country == 'JM' ? 'selected="selected"' : ''; ?>>Jamaica</option>
                                  <option value="JP" <?php echo $USE->country == 'JP' ? 'selected="selected"' : ''; ?>>Japan</option>
                                  <option value="JO" <?php echo $USE->country == 'JO' ? 'selected="selected"' : ''; ?>>Jordan</option>
                                  <option value="KZ" <?php echo $USE->country == 'KZ' ? 'selected="selected"' : ''; ?>>Kazakhstan</option>
                                  <option value="KE" <?php echo $USE->country == 'KE' ? 'selected="selected"' : ''; ?>>Kenya</option>
                                  <option value="KI" <?php echo $USE->country == 'KI' ? 'selected="selected"' : ''; ?>>Kiribati</option>
                                  <option value="KR" <?php echo $USE->country == 'KR' ? 'selected="selected"' : ''; ?>>Korea, North</option>
                                  <option value="KP  <?php echo $USE->country == 'KP' ? 'selected="selected"' : ''; ?>">Korea, South</option>
                                  <option value="KW" <?php echo $USE->country == 'KW' ? 'selected="selected"' : ''; ?>>Kuwait</option>
                                  <option value="KG" <?php echo $USE->country == 'KG' ? 'selected="selected"' : ''; ?>>Kyrgyzstan</option>
                                  <option value="LA" <?php echo $USE->country == 'LA' ? 'selected="selected"' : ''; ?>>Lao</option>
                                  <option value="LV" <?php echo $USE->country == 'LV' ? 'selected="selected"' : ''; ?>>Latvia</option>
                                  <option value="LB" <?php echo $USE->country == 'LB' ? 'selected="selected"' : ''; ?>>Lebanon</option>
                                  <option value="LS" <?php echo $USE->country == 'LS' ? 'selected="selected"' : ''; ?>>Lesotho</option>
                                  <option value="LR" <?php echo $USE->country == 'LR' ? 'selected="selected"' : ''; ?>>Liberia</option>
                                  <option value="LY" <?php echo $USE->country == 'LY' ? 'selected="selected"' : ''; ?>>Libyan Arab Jamahiriya</option>
                                  <option value="LI" <?php echo $USE->country == 'LI' ? 'selected="selected"' : ''; ?>>Liechtenstein</option>
                                  <option value="LT" <?php echo $USE->country == 'LT' ? 'selected="selected"' : ''; ?>>Lithuania</option>
                                  <option value="LU" <?php echo $USE->country == 'LU' ? 'selected="selected"' : ''; ?>>Luxembourg</option>
                                  <option value="MO" <?php echo $USE->country == 'MO' ? 'selected="selected"' : ''; ?>>Macao</option>
                                  <option value="MK" <?php echo $USE->country == 'MK' ? 'selected="selected"' : ''; ?>>Macedonia</option>
                                  <option value="MG" <?php echo $USE->country == 'MG' ? 'selected="selected"' : ''; ?>>Madagascar</option>
                                  <option value="MW" <?php echo $USE->country == 'MW' ? 'selected="selected"' : ''; ?>>Malawi</option>
                                  <option value="MY" <?php echo $USE->country == 'MY' ? 'selected="selected"' : ''; ?>>Malaysia</option>
                                  <option value="MV" <?php echo $USE->country == 'MV' ? 'selected="selected"' : ''; ?>>Maldives</option>
                                  <option value="ML" <?php echo $USE->country == 'ML' ? 'selected="selected"' : ''; ?>>Mali</option>
                                  <option value="MT" <?php echo $USE->country == 'MT' ? 'selected="selected"' : ''; ?>>Malta</option>
                                  <option value="MH" <?php echo $USE->country == 'MH' ? 'selected="selected"' : ''; ?>>Marshall Islands</option>
                                  <option value="MQ" <?php echo $USE->country == 'MQ' ? 'selected="selected"' : ''; ?>>Martinique</option>
                                  <option value="MR" <?php echo $USE->country == 'MR' ? 'selected="selected"' : ''; ?>>Mauritania</option>
                                  <option value="MU" <?php echo $USE->country == 'MU' ? 'selected="selected"' : ''; ?>>Mauritius</option>
                                  <option value="YT" <?php echo $USE->country == 'YT' ? 'selected="selected"' : ''; ?>>Mayotte</option>
                                  <option value="MX" <?php echo $USE->country == 'MX' ? 'selected="selected"' : ''; ?>>Mexico</option>
                                  <option value="FM" <?php echo $USE->country == 'FM' ? 'selected="selected"' : ''; ?>>Micronesia</option>
                                  <option value="MD" <?php echo $USE->country == 'MD' ? 'selected="selected"' : ''; ?>>Moldova</option>
                                  <option value="MC" <?php echo $USE->country == 'MC' ? 'selected="selected"' : ''; ?>>Monaco</option>
                                  <option value="MN" <?php echo $USE->country == 'MN' ? 'selected="selected"' : ''; ?>>Mongolia</option>
                                  <option value="MS" <?php echo $USE->country == 'MS' ? 'selected="selected"' : ''; ?>>Montserrat</option>
                                  <option value="MA" <?php echo $USE->country == 'MA' ? 'selected="selected"' : ''; ?>>Morocco</option>
                                  <option value="MZ" <?php echo $USE->country == 'MZ' ? 'selected="selected"' : ''; ?>>Mozambique</option>
                                  <option value="MM" <?php echo $USE->country == 'MM' ? 'selected="selected"' : ''; ?>>Myanmar</option>
                                  <option value="NA" <?php echo $USE->country == 'NA' ? 'selected="selected"' : ''; ?>>Namibia</option>
                                  <option value="NR" <?php echo $USE->country == 'NR' ? 'selected="selected"' : ''; ?>>Nauru</option>
                                  <option value="NP" <?php echo $USE->country == 'NP' ? 'selected="selected"' : ''; ?>>Nepal</option>
                                  <option value="NL" <?php echo $USE->country == 'NL' ? 'selected="selected"' : ''; ?>>Netherlands</option>
                                  <option value="AN" <?php echo $USE->country == 'AN' ? 'selected="selected"' : ''; ?>>Netherlands Antilles</option>
                                  <option value="NC" <?php echo $USE->country == 'NC' ? 'selected="selected"' : ''; ?>>New Caledonia</option>
                                  <option value="NZ" <?php echo $USE->country == 'NZ' ? 'selected="selected"' : ''; ?>>New Zealand</option>
                                  <option value="NI" <?php echo $USE->country == 'NI' ? 'selected="selected"' : ''; ?>>Nicaragua</option>
                                  <option value="NE" <?php echo $USE->country == 'NE' ? 'selected="selected"' : ''; ?>>Niger</option>
                                  <option value="NG" <?php echo $USE->country == 'NG' ? 'selected="selected"' : ''; ?>>Nigeria</option>
                                  <option value="NU" <?php echo $USE->country == 'NU' ? 'selected="selected"' : ''; ?>>Niue</option>
                                  <option value="NF" <?php echo $USE->country == 'NF' ? 'selected="selected"' : ''; ?>>Norfolk Island</option>
                                  <option value="MP" <?php echo $USE->country == 'MP' ? 'selected="selected"' : ''; ?>>Northern Mariana Islands</option>
                                  <option value="NO" <?php echo $USE->country == 'NO' ? 'selected="selected"' : ''; ?>>Norway</option>
                                  <option value="OM" <?php echo $USE->country == 'OM' ? 'selected="selected"' : ''; ?>>Oman</option>
                                  <option value="PK" <?php echo $USE->country == 'PK' ? 'selected="selected"' : ''; ?>>Pakistan</option>
                                  <option value="PW" <?php echo $USE->country == 'PW' ? 'selected="selected"' : ''; ?>>Palau</option>
                                  <option value="PS" <?php echo $USE->country == 'PS' ? 'selected="selected"' : ''; ?>>Palestinian</option>
                                  <option value="PA" <?php echo $USE->country == 'PA' ? 'selected="selected"' : ''; ?>>Panama</option>
                                  <option value="PG" <?php echo $USE->country == 'PG' ? 'selected="selected"' : ''; ?>>Papua New Guinea</option>
                                  <option value="PY" <?php echo $USE->country == 'PY' ? 'selected="selected"' : ''; ?>>Paraguay</option>
                                  <option value="PE" <?php echo $USE->country == 'PE' ? 'selected="selected"' : ''; ?>>Peru</option>
                                  <option value="PH" <?php echo $USE->country == 'PH' ? 'selected="selected"' : ''; ?>>Philippines</option>
                                  <option value="PN" <?php echo $USE->country == 'PN' ? 'selected="selected"' : ''; ?>>Pitcairn</option>
                                  <option value="PL" <?php echo $USE->country == 'PL' ? 'selected="selected"' : ''; ?>>Poland</option>
                                  <option value="PT" <?php echo $USE->country == 'PT' ? 'selected="selected"' : ''; ?>>Portugal</option>
                                  <option value="PR" <?php echo $USE->country == 'PR' ? 'selected="selected"' : ''; ?>>Puerto Rico</option>
                                  <option value="QA" <?php echo $USE->country == 'QA' ? 'selected="selected"' : ''; ?>>Qatar</option>
                                  <option value="RE" <?php echo $USE->country == 'RE' ? 'selected="selected"' : ''; ?>>Reunion</option>
                                  <option value="RO" <?php echo $USE->country == 'RO' ? 'selected="selected"' : ''; ?>>Romania</option>
                                  <option value="RU" <?php echo $USE->country == 'RU' ? 'selected="selected"' : ''; ?>>Russian Federation</option>
                                  <option value="RW" <?php echo $USE->country == 'RW' ? 'selected="selected"' : ''; ?>>Rwanda</option>
                                  <option value="WS" <?php echo $USE->country == 'WS' ? 'selected="selected"' : ''; ?>>Samoa</option>
                                  <option value="SM" <?php echo $USE->country == 'SM' ? 'selected="selected"' : ''; ?>>San Marino</option>
                                  <option value="ST" <?php echo $USE->country == 'ST' ? 'selected="selected"' : ''; ?>>Sao Tome and Principe</option>
                                  <option value="SA" <?php echo $USE->country == 'SA' ? 'selected="selected"' : ''; ?>>Saudi Arabia</option>
                                  <option value="SN" <?php echo $USE->country == 'SN' ? 'selected="selected"' : ''; ?>>Senegal</option>
                                  <option value="CS" <?php echo $USE->country == 'CS' ? 'selected="selected"' : ''; ?>>Serbia and Montenegro</option>
                                  <option value="SC" <?php echo $USE->country == 'SC' ? 'selected="selected"' : ''; ?>>Seychelles</option>
                                  <option value="SL" <?php echo $USE->country == 'SL' ? 'selected="selected"' : ''; ?>>Sierra Leone</option>
                                  <option value="SG" <?php echo $USE->country == 'SG' ? 'selected="selected"' : ''; ?>>Singapore</option>
                                  <option value="SK" <?php echo $USE->country == 'SK' ? 'selected="selected"' : ''; ?>>Slovakia</option>
                                  <option value="SI" <?php echo $USE->country == 'SI' ? 'selected="selected"' : ''; ?>>Slovenia</option>
                                  <option value="SB" <?php echo $USE->country == 'SB' ? 'selected="selected"' : ''; ?>>Solomon Islands</option>
                                  <option value="SO" <?php echo $USE->country == 'SO' ? 'selected="selected"' : ''; ?>>Somalia</option>
                                  <option value="ZA" <?php echo $USE->country == 'ZA' ? 'selected="selected"' : ''; ?>>South Africa</option>
                                  <option value="GS" <?php echo $USE->country == 'GS' ? 'selected="selected"' : ''; ?>>South Georgia</option>
                                  <option value="ES" <?php echo $USE->country == 'ES' ? 'selected="selected"' : ''; ?>>Spain</option>
                                  <option value="LK" <?php echo $USE->country == 'LK' ? 'selected="selected"' : ''; ?>>Sri Lanka</option>
                                  <option value="SH" <?php echo $USE->country == 'SH' ? 'selected="selected"' : ''; ?>>St. Helena</option>
                                  <option value="KN" <?php echo $USE->country == 'KN' ? 'selected="selected"' : ''; ?>>St. Kitts and Nevis</option>
                                  <option value="LC" <?php echo $USE->country == 'LC' ? 'selected="selected"' : ''; ?>>St. Lucia</option>
                                  <option value="PM" <?php echo $USE->country == 'PM' ? 'selected="selected"' : ''; ?>>St. Pierre and Miquelon</option>
                                  <option value="VC" <?php echo $USE->country == 'VC' ? 'selected="selected"' : ''; ?>>St. Vincent</option>
                                  <option value="SD" <?php echo $USE->country == 'SD' ? 'selected="selected"' : ''; ?>>Sudan</option>
                                  <option value="SR" <?php echo $USE->country == 'SR' ? 'selected="selected"' : ''; ?>>Suriname</option>
                                  <option value="SJ" <?php echo $USE->country == 'SJ' ? 'selected="selected"' : ''; ?>>Svalbard and Jan Mayen</option>
                                  <option value="SZ" <?php echo $USE->country == 'SZ' ? 'selected="selected"' : ''; ?>>Swaziland</option>
                                  <option value="SE" <?php echo $USE->country == 'SE' ? 'selected="selected"' : ''; ?>>Sweden</option>
                                  <option value="CH" <?php echo $USE->country == 'CH' ? 'selected="selected"' : ''; ?>>Switzerland</option>
                                  <option value="SY" <?php echo $USE->country == 'SY' ? 'selected="selected"' : ''; ?>>Syrian Arab Republic</option>
                                  <option value="TW" <?php echo $USE->country == 'TW' ? 'selected="selected"' : ''; ?>>Taiwan</option>
                                  <option value="TJ" <?php echo $USE->country == 'TJ' ? 'selected="selected"' : ''; ?>>Tajikistan</option>
                                  <option value="TZ" <?php echo $USE->country == 'TZ' ? 'selected="selected"' : ''; ?>>Tanzania</option>
                                  <option value="TH" <?php echo $USE->country == 'TH' ? 'selected="selected"' : ''; ?>>Thailand</option>
                                  <option value="TL" <?php echo $USE->country == 'TL' ? 'selected="selected"' : ''; ?>>Timor-Leste</option>
                                  <option value="TG" <?php echo $USE->country == 'TG' ? 'selected="selected"' : ''; ?>>Togo</option>
                                  <option value="TK" <?php echo $USE->country == 'TK' ? 'selected="selected"' : ''; ?>>Tokelau</option>
                                  <option value="TO" <?php echo $USE->country == 'TO' ? 'selected="selected"' : ''; ?>>Tonga</option>
                                  <option value="TT" <?php echo $USE->country == 'TT' ? 'selected="selected"' : ''; ?>>Trinidad and Tobago</option>
                                  <option value="TN" <?php echo $USE->country == 'TN' ? 'selected="selected"' : ''; ?>>Tunisia</option>
                                  <option value="TR" <?php echo $USE->country == 'TR' ? 'selected="selected"' : ''; ?>>Turkey</option>
                                  <option value="TM" <?php echo $USE->country == 'TM' ? 'selected="selected"' : ''; ?>>Turkmenistan</option>
                                  <option value="TC" <?php echo $USE->country == 'TC' ? 'selected="selected"' : ''; ?>>Turks and Caicos Islands</option>
                                  <option value="TV" <?php echo $USE->country == 'TV' ? 'selected="selected"' : ''; ?>>Tuvalu</option>
                                  <option value="UG" <?php echo $USE->country == 'UG' ? 'selected="selected"' : ''; ?>>Uganda</option>
                                  <option value="UA" <?php echo $USE->country == 'UA' ? 'selected="selected"' : ''; ?>>Ukraine</option>
                                  <option value="AE" <?php echo $USE->country == 'AE' ? 'selected="selected"' : ''; ?>>United Arab Emirates</option>
                                  <option value="UK" <?php echo $USE->country == 'UK' ? 'selected="selected"' : ''; ?>>United Kingdom</option>
                                  <option value="US" <?php echo $USE->country == 'US' ? 'selected="selected"' : ''; ?>>United States</option>
                                  <option value="UY" <?php echo $USE->country == 'UY' ? 'selected="selected"' : ''; ?>>Uruguay</option>
                                  <option value="UZ" <?php echo $USE->country == 'UZ' ? 'selected="selected"' : ''; ?>>Uzbekistan</option>
                                  <option value="VU" <?php echo $USE->country == 'VU' ? 'selected="selected"' : ''; ?>>Vanuatu</option>
                                  <option value="VE" <?php echo $USE->country == 'VE' ? 'selected="selected"' : ''; ?>>Venezuela</option>
                                  <option value="VN" <?php echo $USE->country == 'VN' ? 'selected="selected"' : ''; ?>>Viet Nam</option>
                                  <option value="VG" <?php echo $USE->country == 'VG' ? 'selected="selected"' : ''; ?>>Virgin Islands, British</option>
                                  <option value="VI" <?php echo $USE->country == 'VI' ? 'selected="selected"' : ''; ?>>Virgin Islands, U.s.</option>
                                  <option value="WF" <?php echo $USE->country == 'WF' ? 'selected="selected"' : ''; ?>>Wallis and Futuna</option>
                                  <option value="EH" <?php echo $USE->country == 'EH' ? 'selected="selected"' : ''; ?>>Western Sahara</option>
                                  <option value="YE" <?php echo $USE->country == 'YE' ? 'selected="selected"' : ''; ?>>Yemen</option>
                                  <option value="ZM" <?php echo $USE->country == 'ZM' ? 'selected="selected"' : ''; ?>>Zambia</option>
                                  <option value="ZW" <?php echo $USE > country == 'ZW' ? 'selected="selected"' : ''; ?>>Zimbabwe</option>
                                </select>      
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                Post Code:
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->postcode ?>" style="width:150px;" name="postcode" id="postcode">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                Website:
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->website ?>" style="width:300px;" name="website" id="website">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                <?php print_string('contactper') ?>
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->contact_person ?>" name="contact_person" id="contact_person">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                Email:
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->contact_person_email ?>" style="width:300px;" name="contact_person_email" id="contact_person_email">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                <?php print_string('phone') ?>
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->contact_person_phone ?>" name="contact_person_phone" id="contact_person_phone">
                              </td>
                            </tr>
                            <tr>
                              <th style="text-align:left;">
                                Fax:
                              </th>
                              <td>
                                <input type="text" value="<?php echo $USE->contact_person_fax ?>" name="contact_person_fax" id="contact_person_fax">
                              </td>
                            </tr>
                          </tbody></table>
                      </div>

                      <div class="form-sub-heading"><?php print_string('deafaulttime') ?></div>
                      <p class="tip" style="margin-left: 10px"><?php print_string('bydefault') ?></p>
                      <table class="simple-form">  
                        <tbody><tr>
                            <th>
                              <?php print_string('deafaulttime') ?>
                            </th>
                            <td>
                              <select style="width:350px;" name="Organisation.TimeZone" id="Organisation_TimeZone"><option value="Dateline Standard Time">(UTC-12:00) International Date Line West</option>
                                <option value="UTC-11">(UTC-11:00) Coordinated Universal Time-11</option>
                                <option value="Samoa Standard Time">(UTC-11:00) Samoa</option>
                                <option value="Hawaiian Standard Time">(UTC-10:00) Hawaii</option>
                                <option value="Alaskan Standard Time">(UTC-09:00) Alaska</option>
                                <option value="Pacific Standard Time (Mexico)">(UTC-08:00) Baja California</option>
                                <option value="Pacific Standard Time">(UTC-08:00) Pacific Time (US &amp; Canada)</option>
                                <option value="US Mountain Standard Time">(UTC-07:00) Arizona</option>
                                <option value="Mountain Standard Time (Mexico)">(UTC-07:00) Chihuahua, La Paz, Mazatlan</option>
                                <option value="Mountain Standard Time">(UTC-07:00) Mountain Time (US &amp; Canada)</option>
                                <option value="Central America Standard Time">(UTC-06:00) Central America</option>
                                <option value="Central Standard Time">(UTC-06:00) Central Time (US &amp; Canada)</option>
                                <option value="Central Standard Time (Mexico)">(UTC-06:00) Guadalajara, Mexico City, Monterrey</option>
                                <option value="Canada Central Standard Time">(UTC-06:00) Saskatchewan</option>
                                <option value="SA Pacific Standard Time">(UTC-05:00) Bogota, Lima, Quito</option>
                                <option value="Eastern Standard Time">(UTC-05:00) Eastern Time (US &amp; Canada)</option>
                                <option value="US Eastern Standard Time">(UTC-05:00) Indiana (East)</option>
                                <option value="Venezuela Standard Time">(UTC-04:30) Caracas</option>
                                <option value="Paraguay Standard Time">(UTC-04:00) Asuncion</option>
                                <option value="Atlantic Standard Time">(UTC-04:00) Atlantic Time (Canada)</option>
                                <option value="Central Brazilian Standard Time">(UTC-04:00) Cuiaba</option>
                                <option value="SA Western Standard Time">(UTC-04:00) Georgetown, La Paz, Manaus, San Juan</option>
                                <option value="Pacific SA Standard Time">(UTC-04:00) Santiago</option>
                                <option value="Newfoundland Standard Time">(UTC-03:30) Newfoundland</option>
                                <option value="E. South America Standard Time">(UTC-03:00) Brasilia</option>
                                <option value="Argentina Standard Time">(UTC-03:00) Buenos Aires</option>
                                <option value="SA Eastern Standard Time">(UTC-03:00) Cayenne, Fortaleza</option>
                                <option value="Greenland Standard Time">(UTC-03:00) Greenland</option>
                                <option value="Montevideo Standard Time">(UTC-03:00) Montevideo</option>
                                <option value="UTC-02">(UTC-02:00) Coordinated Universal Time-02</option>
                                <option value="Mid-Atlantic Standard Time">(UTC-02:00) Mid-Atlantic</option>
                                <option value="Azores Standard Time">(UTC-01:00) Azores</option>
                                <option value="Cape Verde Standard Time">(UTC-01:00) Cape Verde Is.</option>
                                <option value="Morocco Standard Time">(UTC) Casablanca</option>
                                <option value="UTC" selected="selected">(UTC) Coordinated Universal Time</option>
                                <option value="GMT Standard Time">(UTC) Dublin, Edinburgh, Lisbon, London</option>
                                <option value="Greenwich Standard Time">(UTC) Monrovia, Reykjavik</option>
                                <option value="W. Europe Standard Time">(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                <option value="Central Europe Standard Time">(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                <option value="Romance Standard Time">(UTC+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                <option value="Central European Standard Time">(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                <option value="W. Central Africa Standard Time">(UTC+01:00) West Central Africa</option>
                                <option value="Namibia Standard Time">(UTC+01:00) Windhoek</option>
                                <option value="Jordan Standard Time">(UTC+02:00) Amman</option>
                                <option value="GTB Standard Time">(UTC+02:00) Athens, Bucharest</option>
                                <option value="Middle East Standard Time">(UTC+02:00) Beirut</option>
                                <option value="Egypt Standard Time">(UTC+02:00) Cairo</option>
                                <option value="Syria Standard Time">(UTC+02:00) Damascus</option>
                                <option value="South Africa Standard Time">(UTC+02:00) Harare, Pretoria</option>
                                <option value="FLE Standard Time">(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                <option value="Turkey Standard Time">(UTC+02:00) Istanbul</option>
                                <option value="Israel Standard Time">(UTC+02:00) Jerusalem</option>
                                <option value="E. Europe Standard Time">(UTC+02:00) Minsk</option>
                                <option value="Arabic Standard Time">(UTC+03:00) Baghdad</option>
                                <option value="Kaliningrad Standard Time">(UTC+03:00) Kaliningrad</option>
                                <option value="Arab Standard Time">(UTC+03:00) Kuwait, Riyadh</option>
                                <option value="E. Africa Standard Time">(UTC+03:00) Nairobi</option>
                                <option value="Iran Standard Time">(UTC+03:30) Tehran</option>
                                <option value="Arabian Standard Time">(UTC+04:00) Abu Dhabi, Muscat</option>
                                <option value="Azerbaijan Standard Time">(UTC+04:00) Baku</option>
                                <option value="Russian Standard Time">(UTC+04:00) Moscow, St. Petersburg, Volgograd</option>
                                <option value="Mauritius Standard Time">(UTC+04:00) Port Louis</option>
                                <option value="Georgian Standard Time">(UTC+04:00) Tbilisi</option>
                                <option value="Caucasus Standard Time">(UTC+04:00) Yerevan</option>
                                <option value="Afghanistan Standard Time">(UTC+04:30) Kabul</option>
                                <option value="Pakistan Standard Time">(UTC+05:00) Islamabad, Karachi</option>
                                <option value="West Asia Standard Time">(UTC+05:00) Tashkent</option>
                                <option value="India Standard Time">(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                <option value="Sri Lanka Standard Time">(UTC+05:30) Sri Jayawardenepura</option>
                                <option value="Nepal Standard Time">(UTC+05:45) Kathmandu</option>
                                <option value="Central Asia Standard Time">(UTC+06:00) Astana</option>
                                <option value="Bangladesh Standard Time">(UTC+06:00) Dhaka</option>
                                <option value="Ekaterinburg Standard Time">(UTC+06:00) Ekaterinburg</option>
                                <option value="Myanmar Standard Time">(UTC+06:30) Yangon (Rangoon)</option>
                                <option value="SE Asia Standard Time">(UTC+07:00) Bangkok, Hanoi, Jakarta</option>
                                <option value="N. Central Asia Standard Time">(UTC+07:00) Novosibirsk</option>
                                <option value="China Standard Time">(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                <option value="North Asia Standard Time">(UTC+08:00) Krasnoyarsk</option>
                                <option value="Singapore Standard Time">(UTC+08:00) Kuala Lumpur, Singapore</option>
                                <option value="W. Australia Standard Time">(UTC+08:00) Perth</option>
                                <option value="Taipei Standard Time">(UTC+08:00) Taipei</option>
                                <option value="Ulaanbaatar Standard Time">(UTC+08:00) Ulaanbaatar</option>
                                <option value="North Asia East Standard Time">(UTC+09:00) Irkutsk</option>
                                <option value="Tokyo Standard Time">(UTC+09:00) Osaka, Sapporo, Tokyo</option>
                                <option value="Korea Standard Time">(UTC+09:00) Seoul</option>
                                <option value="Cen. Australia Standard Time">(UTC+09:30) Adelaide</option>
                                <option value="AUS Central Standard Time">(UTC+09:30) Darwin</option>
                                <option value="E. Australia Standard Time">(UTC+10:00) Brisbane</option>
                                <option value="AUS Eastern Standard Time">(UTC+10:00) Canberra, Melbourne, Sydney</option>
                                <option value="West Pacific Standard Time">(UTC+10:00) Guam, Port Moresby</option>
                                <option value="Tasmania Standard Time">(UTC+10:00) Hobart</option>
                                <option value="Yakutsk Standard Time">(UTC+10:00) Yakutsk</option>
                                <option value="Central Pacific Standard Time">(UTC+11:00) Solomon Is., New Caledonia</option>
                                <option value="Vladivostok Standard Time">(UTC+11:00) Vladivostok</option>
                                <option value="New Zealand Standard Time">(UTC+12:00) Auckland, Wellington</option>
                                <option value="UTC+12">(UTC+12:00) Coordinated Universal Time+12</option>
                                <option value="Fiji Standard Time">(UTC+12:00) Fiji</option>
                                <option value="Magadan Standard Time">(UTC+12:00) Magadan</option>
                                <option value="Kamchatka Standard Time">(UTC+12:00) Petropavlovsk-Kamchatsky - Old</option>
                                <option value="Tonga Standard Time">(UTC+13:00) Nuku'alofa</option>
                              </select>          
                            </td>
                          </tr>
                          <tr>
                            <th>

                            </th>
                            <td>
                              &nbsp;
                            </td>
                          </tr>
                        </tbody></table>
                      <div class="form-sub-heading"><?php print_string('customweb') ?></div>
                      <p class="tip" style="margin-left: 10px"><?php print_string('setweb') ?></p>
                      <table class="simple-form">                                            
                        <tbody><tr>
                            <td>
                              &nbsp; https://<input type="text" <?php echo $USE->own_domain_name != '' ? 'disabled=""' : ''; ?> value="<?php echo $USE->custom_website_address ?>" style="width:100px;" name="custom_website_address" id="custom_website_address" class="sub-domain">.thienhoang.com.vn&nbsp;
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;
                              <input type="checkbox" <?php echo $USE->own_domain_name != '' ? 'checked="checked"' : ''; ?> value="true" name="enable_custom_domain" id="enable_custom_domain" class="enable-custom-domain">
                              <span class="tip"> 
                                <?php print_string('domain') ?> http://</span> 
                              <input type="text" value="<?php echo $USE->own_domain_name ?>" style="width:250px;" name="own_domain_name" id="own_domain_name" class="custom-domain" <?php echo $USE->own_domain_name == '' ? 'disabled=""' : ''; ?>> 
                              <a target="_blank" href="http://thienhoang.com.vn"><?php print_string('customdomain') ?></a>
                            </td>
                          </tr>
                        </tbody></table>
                      <div class="form-buttons" >
                        <input type="submit" class="btnlarge" value="<?php print_string('save','admin') ?>">
                      </div> 
                    </form>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        </tbody></table>
      <?php
    }
  }
  else {
    if ($action == 'update_profile') {
      update_profile($user_id, $account_name, $address, $city, $state, $country, $postcode, $website, $contact_person, $contact_person_email, $contact_person_phone, $contact_person_fax, $timezone, $custom_website_address, $own_domain_name);
//$SELECT=get_user_from_user_id($account_id);
      $USER = get_user_from_user_id($USER->id);
    }
    else {
      global $USER;
    }
    ?>
    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">

              <div class="focus-panel">
                <div class="panel-head">
                  <h3>
                    <?php print_string('account') ?>
                  </h3>
                </div>
                <div class="body">

                  <div class="grid-tabs">
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $USER->id; ?>" class="selected"><?php print_string('profile') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('theme') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('billing') ?></a></li>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('messagelab') ?></a></li>                 
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('term') ?></a></li> 
                    </ul>
                    <div class="clear">
                    </div>
                  </div>

                  <form method="post" action="<?php echo $CFG->wwwroot ?>/account/index.php?action=update_profile" onsubmit="return validate();">
                    <input type="hidden" value="<?php echo $USER->id ?>" name="user_id" id="user_id">
                    <div style="margin-left: 6px">               
                      <table class="simple-form">
                        <tbody><tr>
                            <th style="text-align:left;">
                              <?php print_string('accountnam') ?>
                            </th>
                            <td>
                              <div id="erUsername" class="alert" style="display: none;">
                                <?php print_string('pleaseenter') ?></div>    
                              <input type="text" value="<?php echo $USER->company ?>" style="width:300px;" name="account_name" id="name">

                            </td>
                          </tr>

                          <tr>
                            <th style="text-align:left;">
                              <?php print_r(get_string('address')) ?>
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->address ?>" name="address" id="address">
                            </td>
                          </tr>

                          <tr>
                            <th style="text-align:left;">
                              <?php print_r(get_string('city')) ?>
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->city ?>" name="city" id="city">
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              <?php print_r(get_string('state')) ?>
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->state ?>" name="state" id="state">
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              <?php print_r(get_string('country')) ?>
                            </th>
                            <td>
                              <select name="country" id="country">
                                <option value="AF" <?php echo $USER->country == 'AF' ? 'selected="selected"' : ''; ?>>Afghanistan</option>
                                <option value="AL" <?php echo $USER->country == 'AL' ? 'selected="selected"' : ''; ?>>Albania</option>
                                <option value="DZ" <?php echo $USER->country == 'DZ' ? 'selected="selected"' : ''; ?>>Algeria</option>
                                <option value="AS" <?php echo $USER->country == 'AS' ? 'selected="selected"' : ''; ?>>American Samoa</option>
                                <option value="AD" <?php echo $USER->country == 'AD' ? 'selected="selected"' : ''; ?>>Andorra</option>
                                <option value="AO" <?php echo $USER->country == 'AO' ? 'selected="selected"' : ''; ?>>Angola</option>
                                <option value="AI" <?php echo $USER->country == 'AI' ? 'selected="selected"' : ''; ?>>Anguilla</option>
                                <option value="AQ" <?php echo $USER->country == 'AQ' ? 'selected="selected"' : ''; ?>>Antarctica</option>
                                <option value="AG" <?php echo $USER->country == 'AG' ? 'selected="selected"' : ''; ?>>Antigua and Barbuda</option>
                                <option value="AR" <?php echo $USER->country == 'AR' ? 'selected="selected"' : ''; ?>>Argentina</option>
                                <option value="AM" <?php echo $USER->country == 'AM' ? 'selected="selected"' : ''; ?>>Armenia</option>
                                <option value="AW" <?php echo $USER->country == 'AW' ? 'selected="selected"' : ''; ?>>Aruba</option>
                                <option value="AU" <?php echo $USER->country == 'AU' ? 'selected="selected"' : ''; ?>>Australia</option>
                                <option value="AT" <?php echo $USER->country == 'AT' ? 'selected="selected"' : ''; ?>>Austria</option>
                                <option value="AZ" <?php echo $USER->country == 'AZ' ? 'selected="selected"' : ''; ?>>Azerbaijan</option>
                                <option value="BS" <?php echo $USER->country == 'BS' ? 'selected="selected"' : ''; ?>>Bahamas</option>
                                <option value="BH" <?php echo $USER->country == 'BH' ? 'selected="selected"' : ''; ?>>Bahrain</option>
                                <option value="BD" <?php echo $USER->country == 'BD' ? 'selected="selected"' : ''; ?>>Bangladesh</option>
                                <option value="BB" <?php echo $USER->country == 'BB' ? 'selected="selected"' : ''; ?>>Barbados</option>
                                <option value="BY" <?php echo $USER->country == 'BY' ? 'selected="selected"' : ''; ?>>Belarus</option>
                                <option value="BE" <?php echo $USER->country == 'BE' ? 'selected="selected"' : ''; ?>>Belgium</option>
                                <option value="BZ" <?php echo $USER->country == 'BZ' ? 'selected="selected"' : ''; ?>>Belize</option>
                                <option value="BJ" <?php echo $USER->country == 'BJ' ? 'selected="selected"' : ''; ?>>Benin</option>
                                <option value="BM" <?php echo $USER->country == 'BM' ? 'selected="selected"' : ''; ?>>Bermuda</option>
                                <option value="BT" <?php echo $USER->country == 'BT' ? 'selected="selected"' : ''; ?>>Bhutan</option>
                                <option value="BO" <?php echo $USER->country == 'BO' ? 'selected="selected"' : ''; ?>>Bolivia</option>
                                <option value="BA" <?php echo $USER->country == 'BA' ? 'selected="selected"' : ''; ?>>Bosnia and Herzegovina</option>
                                <option value="BW" <?php echo $USER->country == 'BW' ? 'selected="selected"' : ''; ?>>Botswana</option>
                                <option value="BV" <?php echo $USER->country == 'BV' ? 'selected="selected"' : ''; ?>>Bouvet Island</option>
                                <option value="BR" <?php echo $USER->country == 'BR' ? 'selected="selected"' : ''; ?>>Brazil</option>
                                <option value="BN" <?php echo $USER->country == 'BN' ? 'selected="selected"' : ''; ?>>Brunei Darussalam</option>
                                <option value="BG" <?php echo $USER->country == 'BG' ? 'selected="selected"' : ''; ?>>Bulgaria</option>
                                <option value="BF" <?php echo $USER->country == 'BF' ? 'selected="selected"' : ''; ?>>Burkina Faso</option>
                                <option value="BI" <?php echo $USER->country == 'BI' ? 'selected="selected"' : ''; ?>>Burundi</option>
                                <option value="KH" <?php echo $USER->country == 'KH' ? 'selected="selected"' : ''; ?>>Cambodia</option>
                                <option value="CM" <?php echo $USER->country == 'CM' ? 'selected="selected"' : ''; ?>>Cameroon</option>
                                <option value="CA" <?php echo $USER->country == 'CA' ? 'selected="selected"' : ''; ?>>Canada</option>
                                <option value="CV" <?php echo $USER->country == 'CV' ? 'selected="selected"' : ''; ?>>Cape Verde</option>
                                <option value="KY" <?php echo $USER->country == 'KY' ? 'selected="selected"' : ''; ?>>Cayman Islands</option>
                                <option value="CF" <?php echo $USER->country == 'CF' ? 'selected="selected"' : ''; ?>>Central African Republic</option>
                                <option value="TD" <?php echo $USER->country == 'TD' ? 'selected="selected"' : ''; ?>>Chad</option>
                                <option value="CL" <?php echo $USER->country == 'CL' ? 'selected="selected"' : ''; ?>>Chile</option>
                                <option value="CN" <?php echo $USER->country == 'CN' ? 'selected="selected"' : ''; ?>>China</option>
                                <option value="CX" <?php echo $USER->country == 'CX' ? 'selected="selected"' : ''; ?>>Christmas Island</option>
                                <option value="CC" <?php echo $USER->country == 'CC' ? 'selected="selected"' : ''; ?>>Cocos Islands</option>
                                <option value="CO" <?php echo $USER->country == 'CO' ? 'selected="selected"' : ''; ?>>Colombia</option>
                                <option value="KM" <?php echo $USER->country == 'KM' ? 'selected="selected"' : ''; ?>>Comoros</option>
                                <option value="CD" <?php echo $USER->country == 'CD' ? 'selected="selected"' : ''; ?>>Congo</option>
                                <option value="CG" <?php echo $USER->country == 'CG' ? 'selected="selected"' : ''; ?>>Congo</option>
                                <option value="CK" <?php echo $USER->country == 'CK' ? 'selected="selected"' : ''; ?>>Cook Islands</option>
                                <option value="CR" <?php echo $USER->country == 'CR' ? 'selected="selected"' : ''; ?>>Costa Rica</option>
                                <option value="CI" <?php echo $USER->country == 'CI' ? 'selected="selected"' : ''; ?>>Cote D'Ivoire</option>
                                <option value="HR" <?php echo $USER->country == 'HR' ? 'selected="selected"' : ''; ?>>Croatia</option>
                                <option value="CU" <?php echo $USER->country == 'CU' ? 'selected="selected"' : ''; ?>>Cuba</option>
                                <option value="CY" <?php echo $USER->country == 'CY' ? 'selected="selected"' : ''; ?>>Cyprus</option>
                                <option value="CZ" <?php echo $USER->country == 'CZ' ? 'selected="selected"' : ''; ?>>Czech Republic</option>
                                <option value="DK" <?php echo $USER->country == 'DK' ? 'selected="selected"' : ''; ?>>Denmark</option>
                                <option value="DJ" <?php echo $USER->country == 'DJ' ? 'selected="selected"' : ''; ?>>Djibouti</option>
                                <option value="DM" <?php echo $USER->country == 'DM' ? 'selected="selected"' : ''; ?>>Dominica</option>
                                <option value="DO" <?php echo $USER->country == 'DO' ? 'selected="selected"' : ''; ?>>Dominican Republic</option>
                                <option value="EC" <?php echo $USER->country == 'EC' ? 'selected="selected"' : ''; ?>>Ecuador</option>
                                <option value="EG" <?php echo $USER->country == 'EG' ? 'selected="selected"' : ''; ?>>Egypt</option>
                                <option value="SV" <?php echo $USER->country == 'SV' ? 'selected="selected"' : ''; ?>>El Salvador</option>
                                <option value="GQ" <?php echo $USER->country == 'GQ' ? 'selected="selected"' : ''; ?>>Equatorial Guinea</option>
                                <option value="ER" <?php echo $USER->country == 'ER' ? 'selected="selected"' : ''; ?>>Eritrea</option>
                                <option value="EE" <?php echo $USER->country == 'EE' ? 'selected="selected"' : ''; ?>>Estonia</option>
                                <option value="ET" <?php echo $USER->country == 'ET' ? 'selected="selected"' : ''; ?>>Ethiopia</option>
                                <option value="FK" <?php echo $USER->country == 'FK' ? 'selected="selected"' : ''; ?>>Falkland Islands</option>
                                <option value="FO" <?php echo $USER->country == 'FO' ? 'selected="selected"' : ''; ?>>Faroe Islands</option>
                                <option value="FJ" <?php echo $USER->country == 'FJ' ? 'selected="selected"' : ''; ?>>Fiji</option>
                                <option value="FI" <?php echo $USER->country == 'FI' ? 'selected="selected"' : ''; ?>>Finland</option>
                                <option value="FR" <?php echo $USER->country == 'FR' ? 'selected="selected"' : ''; ?>>France</option>
                                <option value="GF" <?php echo $USER->country == 'GF' ? 'selected="selected"' : ''; ?>>French Guiana</option>
                                <option value="PF" <?php echo $USER->country == 'PF' ? 'selected="selected"' : ''; ?>>French Polynesia</option>
                                <option value="GA" <?php echo $USER->country == 'GA' ? 'selected="selected"' : ''; ?>>Gabon</option>
                                <option value="GM" <?php echo $USER->country == 'GM' ? 'selected="selected"' : ''; ?>>Gambia</option>
                                <option value="GE" <?php echo $USER->country == 'GE' ? 'selected="selected"' : ''; ?>>Georgia</option>
                                <option value="DE" <?php echo $USER->country == 'DE' ? 'selected="selected"' : ''; ?>>Germany</option>
                                <option value="GH" <?php echo $USER->country == 'GH' ? 'selected="selected"' : ''; ?>>Ghana</option>
                                <option value="GI" <?php echo $USER->country == 'GI' ? 'selected="selected"' : ''; ?>>Gibraltar</option>
                                <option value="GR" <?php echo $USER->country == 'GR' ? 'selected="selected"' : ''; ?>>Greece</option>
                                <option value="GL" <?php echo $USER->country == 'GL' ? 'selected="selected"' : ''; ?>>Greenland</option>
                                <option value="GD" <?php echo $USER->country == 'GD' ? 'selected="selected"' : ''; ?>>Grenada</option>
                                <option value="GP" <?php echo $USER->country == 'GP' ? 'selected="selected"' : ''; ?>>Guadeloupe</option>
                                <option value="GU" <?php echo $USER->country == 'GU' ? 'selected="selected"' : ''; ?>>Guam</option>
                                <option value="GT" <?php echo $USER->country == 'GT' ? 'selected="selected"' : ''; ?>>Guatemala</option>
                                <option value="GN" <?php echo $USER->country == 'GN' ? 'selected="selected"' : ''; ?>>Guinea</option>
                                <option value="GW" <?php echo $USER->country == 'GW' ? 'selected="selected"' : ''; ?>>Guinea-Bissau</option>
                                <option value="GY" <?php echo $USER->country == 'GY' ? 'selected="selected"' : ''; ?>>Guyana</option>
                                <option value="HT" <?php echo $USER->country == 'HT' ? 'selected="selected"' : ''; ?>>Haiti</option>
                                <option value="HN" <?php echo $USER->country == 'HN' ? 'selected="selected"' : ''; ?>>Honduras</option>
                                <option value="HK" <?php echo $USER->country == 'HK' ? 'selected="selected"' : ''; ?>>Hong Kong</option>
                                <option value="HU" <?php echo $USER->country == 'HU' ? 'selected="selected"' : ''; ?>>Hungary</option>
                                <option value="IS" <?php echo $USER->country == 'IS' ? 'selected="selected"' : ''; ?>>Iceland</option>
                                <option value="IN" <?php echo $USER->country == 'IN' ? 'selected="selected"' : ''; ?>>India</option>
                                <option value="ID" <?php echo $USER->country == 'ID' ? 'selected="selected"' : ''; ?>>Indonesia</option>
                                <option value="IR" <?php echo $USER->country == 'IR' ? 'selected="selected"' : ''; ?>>Iran</option>
                                <option value="IQ" <?php echo $USER->country == 'IQ' ? 'selected="selected"' : ''; ?>>Iraq</option>
                                <option value="IE" <?php echo $USER->country == 'IE' ? 'selected="selected"' : ''; ?>>Ireland</option>
                                <option value="IL" <?php echo $USER->country == 'IL' ? 'selected="selected"' : ''; ?>>Israel</option>
                                <option value="IT" <?php echo $USER->country == 'IT' ? 'selected="selected"' : ''; ?>>Italy</option>
                                <option value="JM" <?php echo $USER->country == 'JM' ? 'selected="selected"' : ''; ?>>Jamaica</option>
                                <option value="JP" <?php echo $USER->country == 'JP' ? 'selected="selected"' : ''; ?>>Japan</option>
                                <option value="JO" <?php echo $USER->country == 'JO' ? 'selected="selected"' : ''; ?>>Jordan</option>
                                <option value="KZ" <?php echo $USER->country == 'KZ' ? 'selected="selected"' : ''; ?>>Kazakhstan</option>
                                <option value="KE" <?php echo $USER->country == 'KE' ? 'selected="selected"' : ''; ?>>Kenya</option>
                                <option value="KI" <?php echo $USER->country == 'KI' ? 'selected="selected"' : ''; ?>>Kiribati</option>
                                <option value="KR" <?php echo $USER->country == 'KR' ? 'selected="selected"' : ''; ?>>Korea, North</option>
                                <option value="KP <?php echo $USER->country == 'KP' ? 'selected="selected"' : ''; ?>">Korea, South</option>
                                <option value="KW" <?php echo $USER->country == 'KW' ? 'selected="selected"' : ''; ?>>Kuwait</option>
                                <option value="KG" <?php echo $USER->country == 'KG' ? 'selected="selected"' : ''; ?>>Kyrgyzstan</option>
                                <option value="LA" <?php echo $USER->country == 'LA' ? 'selected="selected"' : ''; ?>>Lao</option>
                                <option value="LV" <?php echo $USER->country == 'LV' ? 'selected="selected"' : ''; ?>>Latvia</option>
                                <option value="LB" <?php echo $USER->country == 'LB' ? 'selected="selected"' : ''; ?>>Lebanon</option>
                                <option value="LS" <?php echo $USER->country == 'LS' ? 'selected="selected"' : ''; ?>>Lesotho</option>
                                <option value="LR" <?php echo $USER->country == 'LR' ? 'selected="selected"' : ''; ?>>Liberia</option>
                                <option value="LY" <?php echo $USER->country == 'LY' ? 'selected="selected"' : ''; ?>>Libyan Arab Jamahiriya</option>
                                <option value="LI" <?php echo $USER->country == 'LI' ? 'selected="selected"' : ''; ?>>Liechtenstein</option>
                                <option value="LT" <?php echo $USER->country == 'LT' ? 'selected="selected"' : ''; ?>>Lithuania</option>
                                <option value="LU" <?php echo $USER->country == 'LU' ? 'selected="selected"' : ''; ?>>Luxembourg</option>
                                <option value="MO" <?php echo $USER->country == 'MO' ? 'selected="selected"' : ''; ?>>Macao</option>
                                <option value="MK" <?php echo $USER->country == 'MK' ? 'selected="selected"' : ''; ?>>Macedonia</option>
                                <option value="MG" <?php echo $USER->country == 'MG' ? 'selected="selected"' : ''; ?>>Madagascar</option>
                                <option value="MW" <?php echo $USER->country == 'MW' ? 'selected="selected"' : ''; ?>>Malawi</option>
                                <option value="MY" <?php echo $USER->country == 'MY' ? 'selected="selected"' : ''; ?>>Malaysia</option>
                                <option value="MV" <?php echo $USER->country == 'MV' ? 'selected="selected"' : ''; ?>>Maldives</option>
                                <option value="ML" <?php echo $USER->country == 'ML' ? 'selected="selected"' : ''; ?>>Mali</option>
                                <option value="MT" <?php echo $USER->country == 'MT' ? 'selected="selected"' : ''; ?>>Malta</option>
                                <option value="MH" <?php echo $USER->country == 'MH' ? 'selected="selected"' : ''; ?>>Marshall Islands</option>
                                <option value="MQ" <?php echo $USER->country == 'MQ' ? 'selected="selected"' : ''; ?>>Martinique</option>
                                <option value="MR" <?php echo $USER->country == 'MR' ? 'selected="selected"' : ''; ?>>Mauritania</option>
                                <option value="MU" <?php echo $USER->country == 'MU' ? 'selected="selected"' : ''; ?>>Mauritius</option>
                                <option value="YT" <?php echo $USER->country == 'YT' ? 'selected="selected"' : ''; ?>>Mayotte</option>
                                <option value="MX" <?php echo $USER->country == 'MX' ? 'selected="selected"' : ''; ?>>Mexico</option>
                                <option value="FM" <?php echo $USER->country == 'FM' ? 'selected="selected"' : ''; ?>>Micronesia</option>
                                <option value="MD" <?php echo $USER->country == 'MD' ? 'selected="selected"' : ''; ?>>Moldova</option>
                                <option value="MC" <?php echo $USER->country == 'MC' ? 'selected="selected"' : ''; ?>>Monaco</option>
                                <option value="MN" <?php echo $USER->country == 'MN' ? 'selected="selected"' : ''; ?>>Mongolia</option>
                                <option value="MS" <?php echo $USER->country == 'MS' ? 'selected="selected"' : ''; ?>>Montserrat</option>
                                <option value="MA" <?php echo $USER->country == 'MA' ? 'selected="selected"' : ''; ?>>Morocco</option>
                                <option value="MZ" <?php echo $USER->country == 'MZ' ? 'selected="selected"' : ''; ?>>Mozambique</option>
                                <option value="MM" <?php echo $USER->country == 'MM' ? 'selected="selected"' : ''; ?>>Myanmar</option>
                                <option value="NA" <?php echo $USER->country == 'NA' ? 'selected="selected"' : ''; ?>>Namibia</option>
                                <option value="NR" <?php echo $USER->country == 'NR' ? 'selected="selected"' : ''; ?>>Nauru</option>
                                <option value="NP" <?php echo $USER->country == 'NP' ? 'selected="selected"' : ''; ?>>Nepal</option>
                                <option value="NL" <?php echo $USER->country == 'NL' ? 'selected="selected"' : ''; ?>>Netherlands</option>
                                <option value="AN" <?php echo $USER->country == 'AN' ? 'selected="selected"' : ''; ?>>Netherlands Antilles</option>
                                <option value="NC" <?php echo $USER->country == 'NC' ? 'selected="selected"' : ''; ?>>New Caledonia</option>
                                <option value="NZ" <?php echo $USER->country == 'NZ' ? 'selected="selected"' : ''; ?>>New Zealand</option>
                                <option value="NI" <?php echo $USER->country == 'NI' ? 'selected="selected"' : ''; ?>>Nicaragua</option>
                                <option value="NE" <?php echo $USER->country == 'NE' ? 'selected="selected"' : ''; ?>>Niger</option>
                                <option value="NG" <?php echo $USER->country == 'NG' ? 'selected="selected"' : ''; ?>>Nigeria</option>
                                <option value="NU" <?php echo $USER->country == 'NU' ? 'selected="selected"' : ''; ?>>Niue</option>
                                <option value="NF" <?php echo $USER->country == 'NF' ? 'selected="selected"' : ''; ?>>Norfolk Island</option>
                                <option value="MP" <?php echo $USER->country == 'MP' ? 'selected="selected"' : ''; ?>>Northern Mariana Islands</option>
                                <option value="NO" <?php echo $USER->country == 'NO' ? 'selected="selected"' : ''; ?>>Norway</option>
                                <option value="OM" <?php echo $USER->country == 'OM' ? 'selected="selected"' : ''; ?>>Oman</option>
                                <option value="PK" <?php echo $USER->country == 'PK' ? 'selected="selected"' : ''; ?>>Pakistan</option>
                                <option value="PW" <?php echo $USER->country == 'PW' ? 'selected="selected"' : ''; ?>>Palau</option>
                                <option value="PS" <?php echo $USER->country == 'PS' ? 'selected="selected"' : ''; ?>>Palestinian</option>
                                <option value="PA" <?php echo $USER->country == 'PA' ? 'selected="selected"' : ''; ?>>Panama</option>
                                <option value="PG" <?php echo $USER->country == 'PG' ? 'selected="selected"' : ''; ?>>Papua New Guinea</option>
                                <option value="PY" <?php echo $USER->country == 'PY' ? 'selected="selected"' : ''; ?>>Paraguay</option>
                                <option value="PE" <?php echo $USER->country == 'PE' ? 'selected="selected"' : ''; ?>>Peru</option>
                                <option value="PH" <?php echo $USER->country == 'PH' ? 'selected="selected"' : ''; ?>>Philippines</option>
                                <option value="PN" <?php echo $USER->country == 'PN' ? 'selected="selected"' : ''; ?>>Pitcairn</option>
                                <option value="PL" <?php echo $USER->country == 'PL' ? 'selected="selected"' : ''; ?>>Poland</option>
                                <option value="PT" <?php echo $USER->country == 'PT' ? 'selected="selected"' : ''; ?>>Portugal</option>
                                <option value="PR" <?php echo $USER->country == 'PR' ? 'selected="selected"' : ''; ?>>Puerto Rico</option>
                                <option value="QA" <?php echo $USER->country == 'QA' ? 'selected="selected"' : ''; ?>>Qatar</option>
                                <option value="RE" <?php echo $USER->country == 'RE' ? 'selected="selected"' : ''; ?>>Reunion</option>
                                <option value="RO" <?php echo $USER->country == 'RO' ? 'selected="selected"' : ''; ?>>Romania</option>
                                <option value="RU" <?php echo $USER->country == 'RU' ? 'selected="selected"' : ''; ?>>Russian Federation</option>
                                <option value="RW" <?php echo $USER->country == 'RW' ? 'selected="selected"' : ''; ?>>Rwanda</option>
                                <option value="WS" <?php echo $USER->country == 'WS' ? 'selected="selected"' : ''; ?>>Samoa</option>
                                <option value="SM" <?php echo $USER->country == 'SM' ? 'selected="selected"' : ''; ?>>San Marino</option>
                                <option value="ST" <?php echo $USER->country == 'ST' ? 'selected="selected"' : ''; ?>>Sao Tome and Principe</option>
                                <option value="SA" <?php echo $USER->country == 'SA' ? 'selected="selected"' : ''; ?>>Saudi Arabia</option>
                                <option value="SN" <?php echo $USER->country == 'SN' ? 'selected="selected"' : ''; ?>>Senegal</option>
                                <option value="CS" <?php echo $USER->country == 'CS' ? 'selected="selected"' : ''; ?>>Serbia and Montenegro</option>
                                <option value="SC" <?php echo $USER->country == 'SC' ? 'selected="selected"' : ''; ?>>Seychelles</option>
                                <option value="SL" <?php echo $USER->country == 'SL' ? 'selected="selected"' : ''; ?>>Sierra Leone</option>
                                <option value="SG" <?php echo $USER->country == 'SG' ? 'selected="selected"' : ''; ?>>Singapore</option>
                                <option value="SK" <?php echo $USER->country == 'SK' ? 'selected="selected"' : ''; ?>>Slovakia</option>
                                <option value="SI" <?php echo $USER->country == 'SI' ? 'selected="selected"' : ''; ?>>Slovenia</option>
                                <option value="SB" <?php echo $USER->country == 'SB' ? 'selected="selected"' : ''; ?>>Solomon Islands</option>
                                <option value="SO" <?php echo $USER->country == 'SO' ? 'selected="selected"' : ''; ?>>Somalia</option>
                                <option value="ZA" <?php echo $USER->country == 'ZA' ? 'selected="selected"' : ''; ?>>South Africa</option>
                                <option value="GS" <?php echo $USER->country == 'GS' ? 'selected="selected"' : ''; ?>>South Georgia</option>
                                <option value="ES" <?php echo $USER->country == 'ES' ? 'selected="selected"' : ''; ?>>Spain</option>
                                <option value="LK" <?php echo $USER->country == 'LK' ? 'selected="selected"' : ''; ?>>Sri Lanka</option>
                                <option value="SH" <?php echo $USER->country == 'SH' ? 'selected="selected"' : ''; ?>>St. Helena</option>
                                <option value="KN" <?php echo $USER->country == 'KN' ? 'selected="selected"' : ''; ?>>St. Kitts and Nevis</option>
                                <option value="LC" <?php echo $USER->country == 'LC' ? 'selected="selected"' : ''; ?>>St. Lucia</option>
                                <option value="PM" <?php echo $USER->country == 'PM' ? 'selected="selected"' : ''; ?>>St. Pierre and Miquelon</option>
                                <option value="VC" <?php echo $USER->country == 'VC' ? 'selected="selected"' : ''; ?>>St. Vincent</option>
                                <option value="SD" <?php echo $USER->country == 'SD' ? 'selected="selected"' : ''; ?>>Sudan</option>
                                <option value="SR" <?php echo $USER->country == 'SR' ? 'selected="selected"' : ''; ?>>Suriname</option>
                                <option value="SJ" <?php echo $USER->country == 'SJ' ? 'selected="selected"' : ''; ?>>Svalbard and Jan Mayen</option>
                                <option value="SZ" <?php echo $USER->country == 'SZ' ? 'selected="selected"' : ''; ?>>Swaziland</option>
                                <option value="SE" <?php echo $USER->country == 'SE' ? 'selected="selected"' : ''; ?>>Sweden</option>
                                <option value="CH" <?php echo $USER->country == 'CH' ? 'selected="selected"' : ''; ?>>Switzerland</option>
                                <option value="SY" <?php echo $USER->country == 'SY' ? 'selected="selected"' : ''; ?>>Syrian Arab Republic</option>
                                <option value="TW" <?php echo $USER->country == 'TW' ? 'selected="selected"' : ''; ?>>Taiwan</option>
                                <option value="TJ" <?php echo $USER->country == 'TJ' ? 'selected="selected"' : ''; ?>>Tajikistan</option>
                                <option value="TZ" <?php echo $USER->country == 'TZ' ? 'selected="selected"' : ''; ?>>Tanzania</option>
                                <option value="TH" <?php echo $USER->country == 'TH' ? 'selected="selected"' : ''; ?>>Thailand</option>
                                <option value="TL" <?php echo $USER->country == 'TL' ? 'selected="selected"' : ''; ?>>Timor-Leste</option>
                                <option value="TG" <?php echo $USER->country == 'TG' ? 'selected="selected"' : ''; ?>>Togo</option>
                                <option value="TK" <?php echo $USER->country == 'TK' ? 'selected="selected"' : ''; ?>>Tokelau</option>
                                <option value="TO" <?php echo $USER->country == 'TO' ? 'selected="selected"' : ''; ?>>Tonga</option>
                                <option value="TT" <?php echo $USER->country == 'TT' ? 'selected="selected"' : ''; ?>>Trinidad and Tobago</option>
                                <option value="TN" <?php echo $USER->country == 'TN' ? 'selected="selected"' : ''; ?>>Tunisia</option>
                                <option value="TR" <?php echo $USER->country == 'TR' ? 'selected="selected"' : ''; ?>>Turkey</option>
                                <option value="TM" <?php echo $USER->country == 'TM' ? 'selected="selected"' : ''; ?>>Turkmenistan</option>
                                <option value="TC" <?php echo $USER->country == 'TC' ? 'selected="selected"' : ''; ?>>Turks and Caicos Islands</option>
                                <option value="TV" <?php echo $USER->country == 'TV' ? 'selected="selected"' : ''; ?>>Tuvalu</option>
                                <option value="UG" <?php echo $USER->country == 'UG' ? 'selected="selected"' : ''; ?>>Uganda</option>
                                <option value="UA" <?php echo $USER->country == 'UA' ? 'selected="selected"' : ''; ?>>Ukraine</option>
                                <option value="AE" <?php echo $USER->country == 'AE' ? 'selected="selected"' : ''; ?>>United Arab Emirates</option>
                                <option value="UK" <?php echo $USER->country == 'UK' ? 'selected="selected"' : ''; ?>>United Kingdom</option>
                                <option value="US" <?php echo $USER->country == 'US' ? 'selected="selected"' : ''; ?>>United States</option>
                                <option value="UY" <?php echo $USER->country == 'UY' ? 'selected="selected"' : ''; ?>>Uruguay</option>
                                <option value="UZ" <?php echo $USER->country == 'UZ' ? 'selected="selected"' : ''; ?>>Uzbekistan</option>
                                <option value="VU" <?php echo $USER->country == 'VU' ? 'selected="selected"' : ''; ?>>Vanuatu</option>
                                <option value="VE" <?php echo $USER->country == 'VE' ? 'selected="selected"' : ''; ?>>Venezuela</option>
                                <option value="VN" <?php echo $USER->country == 'VN' ? 'selected="selected"' : ''; ?>>Viet Nam</option>
                                <option value="VG" <?php echo $USER->country == 'VG' ? 'selected="selected"' : ''; ?>>Virgin Islands, British</option>
                                <option value="VI" <?php echo $USER->country == 'VI' ? 'selected="selected"' : ''; ?>>Virgin Islands, U.s.</option>
                                <option value="WF" <?php echo $USER->country == 'WF' ? 'selected="selected"' : ''; ?>>Wallis and Futuna</option>
                                <option value="EH" <?php echo $USER->country == 'EH' ? 'selected="selected"' : ''; ?>>Western Sahara</option>
                                <option value="YE" <?php echo $USER->country == 'YE' ? 'selected="selected"' : ''; ?>>Yemen</option>
                                <option value="ZM" <?php echo $USER->country == 'ZM' ? 'selected="selected"' : ''; ?>>Zambia</option>
                                <option value="ZW" <?php echo $USER->country == 'ZW' ? 'selected="selected"' : ''; ?>>Zimbabwe</option>
                              </select>      
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              Post Code:
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->postcode ?>" style="width:150px;" name="postcode" id="postcode">
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              Website:
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->website ?>" style="width:300px;" name="website" id="website">
                            </td>
                          </tr>

                          <tr>
                            <th style="text-align:left;">
                              <?php print_string('contactper') ?>
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->contact_person ?>" name="contact_person" id="contact_person">
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              Email:
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->contact_person_email ?>" style="width:300px;" name="contact_person_email" id="contact_person_email">
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              <?php print_string('phone') ?>
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->contact_person_phone ?>" name="contact_person_phone" id="contact_person_phone">
                            </td>
                          </tr>
                          <tr>
                            <th style="text-align:left;">
                              Fax:
                            </th>
                            <td>
                              <input type="text" value="<?php echo $USER->contact_person_fax ?>" name="contact_person_fax" id="contact_person_fax">
                            </td>
                          </tr>

                        </tbody></table>
                    </div>

                    <div class="form-sub-heading"><?php print_string('deafaulttime') ?></div>
                    <p class="tip" style="margin-left: 10px"><?php print_string('deafaulttime') ?></p>
                    <table class="simple-form">  
                      <tbody><tr>
                          <th>
                            <?php print_string('deafaulttime') ?>
                          </th>
                          <td>
                            <select style="width:350px;" name="Organisation.TimeZone" id="Organisation_TimeZone"><option value="Dateline Standard Time">(UTC-12:00) International Date Line West</option>
                              <option value="UTC-11">(UTC-11:00) Coordinated Universal Time-11</option>
                              <option value="Samoa Standard Time">(UTC-11:00) Samoa</option>
                              <option value="Hawaiian Standard Time">(UTC-10:00) Hawaii</option>
                              <option value="Alaskan Standard Time">(UTC-09:00) Alaska</option>
                              <option value="Pacific Standard Time (Mexico)">(UTC-08:00) Baja California</option>
                              <option value="Pacific Standard Time">(UTC-08:00) Pacific Time (US &amp; Canada)</option>
                              <option value="US Mountain Standard Time">(UTC-07:00) Arizona</option>
                              <option value="Mountain Standard Time (Mexico)">(UTC-07:00) Chihuahua, La Paz, Mazatlan</option>
                              <option value="Mountain Standard Time">(UTC-07:00) Mountain Time (US &amp; Canada)</option>
                              <option value="Central America Standard Time">(UTC-06:00) Central America</option>
                              <option value="Central Standard Time">(UTC-06:00) Central Time (US &amp; Canada)</option>
                              <option value="Central Standard Time (Mexico)">(UTC-06:00) Guadalajara, Mexico City, Monterrey</option>
                              <option value="Canada Central Standard Time">(UTC-06:00) Saskatchewan</option>
                              <option value="SA Pacific Standard Time">(UTC-05:00) Bogota, Lima, Quito</option>
                              <option value="Eastern Standard Time">(UTC-05:00) Eastern Time (US &amp; Canada)</option>
                              <option value="US Eastern Standard Time">(UTC-05:00) Indiana (East)</option>
                              <option value="Venezuela Standard Time">(UTC-04:30) Caracas</option>
                              <option value="Paraguay Standard Time">(UTC-04:00) Asuncion</option>
                              <option value="Atlantic Standard Time">(UTC-04:00) Atlantic Time (Canada)</option>
                              <option value="Central Brazilian Standard Time">(UTC-04:00) Cuiaba</option>
                              <option value="SA Western Standard Time">(UTC-04:00) Georgetown, La Paz, Manaus, San Juan</option>
                              <option value="Pacific SA Standard Time">(UTC-04:00) Santiago</option>
                              <option value="Newfoundland Standard Time">(UTC-03:30) Newfoundland</option>
                              <option value="E. South America Standard Time">(UTC-03:00) Brasilia</option>
                              <option value="Argentina Standard Time">(UTC-03:00) Buenos Aires</option>
                              <option value="SA Eastern Standard Time">(UTC-03:00) Cayenne, Fortaleza</option>
                              <option value="Greenland Standard Time">(UTC-03:00) Greenland</option>
                              <option value="Montevideo Standard Time">(UTC-03:00) Montevideo</option>
                              <option value="UTC-02">(UTC-02:00) Coordinated Universal Time-02</option>
                              <option value="Mid-Atlantic Standard Time">(UTC-02:00) Mid-Atlantic</option>
                              <option value="Azores Standard Time">(UTC-01:00) Azores</option>
                              <option value="Cape Verde Standard Time">(UTC-01:00) Cape Verde Is.</option>
                              <option value="Morocco Standard Time">(UTC) Casablanca</option>
                              <option value="UTC" selected="selected">(UTC) Coordinated Universal Time</option>
                              <option value="GMT Standard Time">(UTC) Dublin, Edinburgh, Lisbon, London</option>
                              <option value="Greenwich Standard Time">(UTC) Monrovia, Reykjavik</option>
                              <option value="W. Europe Standard Time">(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                              <option value="Central Europe Standard Time">(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                              <option value="Romance Standard Time">(UTC+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                              <option value="Central European Standard Time">(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                              <option value="W. Central Africa Standard Time">(UTC+01:00) West Central Africa</option>
                              <option value="Namibia Standard Time">(UTC+01:00) Windhoek</option>
                              <option value="Jordan Standard Time">(UTC+02:00) Amman</option>
                              <option value="GTB Standard Time">(UTC+02:00) Athens, Bucharest</option>
                              <option value="Middle East Standard Time">(UTC+02:00) Beirut</option>
                              <option value="Egypt Standard Time">(UTC+02:00) Cairo</option>
                              <option value="Syria Standard Time">(UTC+02:00) Damascus</option>
                              <option value="South Africa Standard Time">(UTC+02:00) Harare, Pretoria</option>
                              <option value="FLE Standard Time">(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                              <option value="Turkey Standard Time">(UTC+02:00) Istanbul</option>
                              <option value="Israel Standard Time">(UTC+02:00) Jerusalem</option>
                              <option value="E. Europe Standard Time">(UTC+02:00) Minsk</option>
                              <option value="Arabic Standard Time">(UTC+03:00) Baghdad</option>
                              <option value="Kaliningrad Standard Time">(UTC+03:00) Kaliningrad</option>
                              <option value="Arab Standard Time">(UTC+03:00) Kuwait, Riyadh</option>
                              <option value="E. Africa Standard Time">(UTC+03:00) Nairobi</option>
                              <option value="Iran Standard Time">(UTC+03:30) Tehran</option>
                              <option value="Arabian Standard Time">(UTC+04:00) Abu Dhabi, Muscat</option>
                              <option value="Azerbaijan Standard Time">(UTC+04:00) Baku</option>
                              <option value="Russian Standard Time">(UTC+04:00) Moscow, St. Petersburg, Volgograd</option>
                              <option value="Mauritius Standard Time">(UTC+04:00) Port Louis</option>
                              <option value="Georgian Standard Time">(UTC+04:00) Tbilisi</option>
                              <option value="Caucasus Standard Time">(UTC+04:00) Yerevan</option>
                              <option value="Afghanistan Standard Time">(UTC+04:30) Kabul</option>
                              <option value="Pakistan Standard Time">(UTC+05:00) Islamabad, Karachi</option>
                              <option value="West Asia Standard Time">(UTC+05:00) Tashkent</option>
                              <option value="India Standard Time">(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                              <option value="Sri Lanka Standard Time">(UTC+05:30) Sri Jayawardenepura</option>
                              <option value="Nepal Standard Time">(UTC+05:45) Kathmandu</option>
                              <option value="Central Asia Standard Time">(UTC+06:00) Astana</option>
                              <option value="Bangladesh Standard Time">(UTC+06:00) Dhaka</option>
                              <option value="Ekaterinburg Standard Time">(UTC+06:00) Ekaterinburg</option>
                              <option value="Myanmar Standard Time">(UTC+06:30) Yangon (Rangoon)</option>
                              <option value="SE Asia Standard Time">(UTC+07:00) Bangkok, Hanoi, Jakarta</option>
                              <option value="N. Central Asia Standard Time">(UTC+07:00) Novosibirsk</option>
                              <option value="China Standard Time">(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                              <option value="North Asia Standard Time">(UTC+08:00) Krasnoyarsk</option>
                              <option value="Singapore Standard Time">(UTC+08:00) Kuala Lumpur, Singapore</option>
                              <option value="W. Australia Standard Time">(UTC+08:00) Perth</option>
                              <option value="Taipei Standard Time">(UTC+08:00) Taipei</option>
                              <option value="Ulaanbaatar Standard Time">(UTC+08:00) Ulaanbaatar</option>
                              <option value="North Asia East Standard Time">(UTC+09:00) Irkutsk</option>
                              <option value="Tokyo Standard Time">(UTC+09:00) Osaka, Sapporo, Tokyo</option>
                              <option value="Korea Standard Time">(UTC+09:00) Seoul</option>
                              <option value="Cen. Australia Standard Time">(UTC+09:30) Adelaide</option>
                              <option value="AUS Central Standard Time">(UTC+09:30) Darwin</option>
                              <option value="E. Australia Standard Time">(UTC+10:00) Brisbane</option>
                              <option value="AUS Eastern Standard Time">(UTC+10:00) Canberra, Melbourne, Sydney</option>
                              <option value="West Pacific Standard Time">(UTC+10:00) Guam, Port Moresby</option>
                              <option value="Tasmania Standard Time">(UTC+10:00) Hobart</option>
                              <option value="Yakutsk Standard Time">(UTC+10:00) Yakutsk</option>
                              <option value="Central Pacific Standard Time">(UTC+11:00) Solomon Is., New Caledonia</option>
                              <option value="Vladivostok Standard Time">(UTC+11:00) Vladivostok</option>
                              <option value="New Zealand Standard Time">(UTC+12:00) Auckland, Wellington</option>
                              <option value="UTC+12">(UTC+12:00) Coordinated Universal Time+12</option>
                              <option value="Fiji Standard Time">(UTC+12:00) Fiji</option>
                              <option value="Magadan Standard Time">(UTC+12:00) Magadan</option>
                              <option value="Kamchatka Standard Time">(UTC+12:00) Petropavlovsk-Kamchatsky - Old</option>
                              <option value="Tonga Standard Time">(UTC+13:00) Nuku'alofa</option>
                            </select>          
                          </td>
                        </tr>
                        <tr>
                          <th>
                          </th>
                          <td>
                            &nbsp;
                          </td>
                        </tr>
                      </tbody></table>
                    <div class="form-sub-heading"><?php print_string('customweb') ?></div>
                    <p class="tip" style="margin-left: 10px"><?php print_string('setweb') ?></p>
                    <table class="simple-form">                                            
                      <tbody><tr>
                          <td>
                            &nbsp; https://<input type="text" <?php echo $USER->own_domain_name != '' ? 'disabled=""' : ''; ?> value="<?php echo $USER->custom_website_address ?>" style="width:100px;" name="custom_website_address" id="custom_website_address" class="sub-domain">.thienhoang.com.vn&nbsp;

                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;
                            <input type="checkbox" <?php echo $USER->own_domain_name != '' ? 'checked="checked"' : ''; ?> value="true" name="enable_custom_domain" id="enable_custom_domain" class="enable-custom-domain">
                            <span class="tip"> 
                              <?php print_string('domain') ?> http://</span> 
                            <input type="text" value="<?php echo $USER->own_domain_name ?>" style="width:250px;" name="own_domain_name" id="own_domain_name" class="custom-domain" <?php echo $USER->own_domain_name == '' ? 'disabled=""' : ''; ?>> 
                            <a target="_blank" href="http://thienhoang.com.vn"><?php print_string('customdomain') ?></a>
                          </td>
                        </tr>
                      </tbody></table>
                    <div class="form-buttons" >
                      <input type="submit" class="btnlarge" value="<?php print_string('save','admin') ?>">
                    </div> 
                  </form>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </tbody></table>
    <?php
  }
  ?>


</div>