<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$pid = optional_param('pid', 0, PARAM_INT);
$idupdate = optional_param('idupdate', 0, PARAM_INT);
$name = optional_param('name', '', PARAM_TEXT);
$code = optional_param('code', 0, PARAM_INT);
$cost = optional_param('cost', '', PARAM_TEXT);
$useractive = optional_param('useractive', '', PARAM_TEXT);
$method = optional_param('method', '', PARAM_TEXT);
$email = optional_param('email', '', PARAM_TEXT);
$logo = optional_param('logo', '', PARAM_TEXT);
$support = optional_param('support', '', PARAM_TEXT);
$account_id = optional_param('id', 0, PARAM_INT);
global $CFG;
?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link type="text/css" rel="stylesheet" href="css/jquery.dataTables.css">
<script type="text/javascript">
  /* Custom filtering function which will search data in column four between two values */
  $.fn.dataTable.ext.search.push(
  function( settings, data, dataIndex ) {
    var min = parseInt( $('#min').val(), 10 );
    var max = parseInt( $('#max').val(), 10 );
    var age = parseFloat( data[4] ) || 0; // use data for the age column
 
    if ( ( isNaN( min ) && isNaN( max ) ) ||
      ( isNaN( min ) && age <= max ) ||
      ( min <= age   && isNaN( max ) ) ||
      ( min <= age   && age <= max ) )
    {
      return true;
    }
    return false;
  }
);
 
  $(document).ready(function() {
    var table = $('#servicepack').DataTable();
     
    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').keyup( function() {
      table.draw();
    } );
    $(function() {
      $('#addTeams').click(function(){
            
        $('#overlay').fadeIn('fast',function(){
          $('#facebox').show();
        });
      });
    });
    $(document).keyup(function (e) {
      if (e.keyCode == 27) {

        $('#facebox').hide();
        $('#overlay').fadeOut('fast');  
      }
    });
    $('#assignCancel').click(function() {
      $('#facebox').hide();
      $('#overlay').fadeOut('fast');
  
    });
    
    $('#servicepack tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
      }
      else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
      }
    } );
 
    $('#button').click( function () {
      table.row('.selected').remove().draw( false );
    } );
    
  } );
</script>
<div id="page-body">        
  <?php
  switch ($action) {
    case "insert":
      try {
        $insert = insert_service_pack($name, $code, $cost, $method, $useractive, $email, $logo, $support);
        header("Location:servicepack.php?id=" . $account_id);
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
      break;
    case "update":
      try {
        $update = update_service_pack($idupdate, $code, $name, $cost, $method, $useractive, $email, $logo, $support);
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
      break;
    case "delete":
      try {
        $delete = delete_service_pack($pid);
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
      break;
    default:
      break;
  }


  global $USER;
  $user_id = $USER->id;
  $payment = get_payment_user_id($user_id);
  ?>

  <table class="admin-fullscreen">
    <tbody><tr>
        <td class="admin-fullscreen-left" style="width: 80%">
          <div class="admin-fullscreen-content">

            <div class="focus-panel">
              <div class="panel-head" style="background-color: rgb(112, 163, 60);">
                <?php  
                $USE = get_user_from_id($account_id);
                  if (is_siteadmin()) {
                ?>
                <div style="float: right">
                  <label> <?php echo $USE->username ?></label>
                </div>
                <?php  
                  }
                ?>
                <h3>
                  <?php print_string('account') ?>
                </h3>
              </div>
              <div class="body">

                <div class="grid-tabs">
                  <ul>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class=""><?php print_string('billing') ?></a></li>
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class=""><?php print_string('messagelab') ?></a></li>                  
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/email_setup.php?id=<?php echo $account_id; ?>" class=""><?php print_string('emailsetup') ?></a></li>  
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>    
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>  
                    <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('servicepack') ?></a></li>  
                  </ul>
                  <div class="clear"></div>
                </div>
                <div class="sys-note">
                  <table border="0" cellspacing="5" cellpadding="5">
                    <tbody><tr>
                        <td>Số lượng người dùng nhỏ nhất:</td>
                        <td><input type="text" id="min" name="min"></td>
                      </tr>
                      <tr>
                        <td>Số lượng người dùng lớn nhất:</td>
                        <td><input type="text" id="max" name="max"></td>
                      </tr>
                    </tbody></table>
                  <!--                  <button id="bt-submit"type="submit">Chỉnh sửa thanh toán</button>-->
<!--                  <button id="button">Xoá thanh toán</button><span></span>-->
                  <form name="edit-pack" action=" <?php echo $CFG->wwwroot ?>/account/servicepack.php?id= <?php echo $account_id ?>" method="post" >
                    <table id="servicepack" class="display" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Mã</th>
                          <th>Tên</th>
                          <th>Chi phí/tháng</th>
                          <th>Thanh toán</th>
                          <th>Người dùng</th>
                          <th>Hỗ trợ Email</th>
                          <th>Logo</th>
                          <th>Hỗ trợ</th>
                          <th>Hành động</th>
                        </tr>
                      </thead>

                      <tfoot>
                        <tr>
                          <th>Mã</th>
                          <th>Tên</th>
                          <th>Người dùng</th>
                          <th>Chi phí/tháng</th>
                          <th>Thanh toán</th>
                          <th>Hỗ trợ Email</th>
                          <th>Logo</th>
                          <th>Hỗ trợ</th>
                          <th>Hành động</th>
                        </tr>
                      </tfoot>

                      <tbody>
                        <?php
                        $service_pack = get_service_package_superadmin();
                        foreach ($service_pack as $key => $s_pack) {
                          $selected1 = $s_pack->method == "Hằng năm" ? "selected='selected'" : "";
                          $selected2 = $s_pack->method == "Hằng tháng" ? "selected='selected'" : "";
                          $selected3 = $s_pack->email == "Có" ? "selected='selected'" : "";
                          $selected4 = $s_pack->email == "Không" ? "selected='selected'" : "";
                          $selected5 = $s_pack->logo == "Có" ? "selected='selected'" : "";
                          $selected6 = $s_pack->logo == "Không" ? "selected='selected'" : "";
                          $selected7 = $s_pack->support == "Có" ? "selected='selected'" : "";
                          $selected8 = $s_pack->support == "Không" ? "selected='selected'" : "";
                          
                          ?>
                          <tr>
                        <input type="hidden" name="idupdate" value="<?php echo $pid ?>">
                        <input type="hidden" name="action" value="update">
                        <td><?php echo $s_pack->id == $pid ? "<input type='text' name='code' value='" . $s_pack->spid . "'>" : $s_pack->spid; ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<input type='text' name='name' value='" . $s_pack->name . "'>" : "<a id='editpack' href='#'>" . $s_pack->name . "</a>"; ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<input type='text' name='cost' value='" . $s_pack->cost . "'>" : number_format($s_pack->cost); ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<select name='method'>
                          <option value='Hằng năm'" . $selected1 . ">Hằng năm</option>
                          <option value='Hằng tháng'" . $selected2 . ">Hằng tháng</option>
                          </select>" : $s_pack->method; ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<input type='text' name='useractive' value='" . $s_pack->user_active . "'>" : $s_pack->user_active; ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<select name='email'>
                          <option value='Có'" . $selected3 . ">Có</option>
                          <option value='Không'" . $selected4 . ">Không</option>
                          </select>" : $s_pack->email; ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<select name='logo'>
                          <option value='Có'" . $selected5 . ">Có</option>
                          <option value='Không'" . $selected6 . ">Không</option>
                          </select>" : $s_pack->logo; ?></td>
                        <td><?php echo $s_pack->id == $pid ? "<select name='support'>
                          <option value='Có'" . $selected7 . ">Có</option>
                          <option value='Không'" . $selected8 . ">Không</option>
                          </select>" : $s_pack->support; ?></td>
                        <td><?php
                        echo $s_pack->id == $pid ? "<input type='submit' name='submit' value='Lưu'><a href=" . $CFG->wwwroot . "/account/servicepack.php?id=" . $account_id . "> Thoát </a>" :
                            "<a href=" . $CFG->wwwroot . "/account/servicepack.php?id=" . $account_id . "&pid=" . $s_pack->id . ">Chỉnh sửa</a> 
                          <a href=" . $CFG->wwwroot . "/account/servicepack.php?id=" . $account_id . "&pid=" . $s_pack->id . "&action=delete>Xoá</a>"
                            ?></td>

                        </tr>
                    </form>
                    <?php
                  }
                  ?>
                  </tbody>
                  </table>
                  </form>
                </div> 
              </div>
            </div>
          </div>
        </td>
        <td class="admin-fullscreen-right">
          <div class="admin-fullscreen-content">
            <div class="side-panel">
              <div class="action-buttons">
                <ul>
                  <li><a href="#" class="big-button drop" onclick="" id="addTeams"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print('Tạo gói dịch vụ') ?></span></span></span></span></a></li>
                </ul>
              </div>
            </div>

          </div>
          <div class="overlay" id="overlay" style="display:none;"></div>
          <div id="facebox" style="display: none;">      
            <div class="popup">         <div class="content">

                <div id="assignItemBox">
                  <div class="assign-header">
                    <span style="color: #fff; font-weight: bolder;">Tạo mới gói dịch vụ</span>
                  </div>
                  <form action="<?php echo $CFG->wwwroot ?>/account/servicepack.php?action=insert" method="post">
                    <div id="assignResults">
                      <table cellspacing="0" class="item-page-list">
                        <tbody>
                        <fieldset>
                          <tr>
                            <td><label for="name">Tên(*)</label></td>
                            <td><input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"></td>
                          </tr>
                          <tr>
                            <td><label for="code">Mã(*)</label></td>
                            <td><input type="text" name="code" id="code" value="" class="text ui-widget-content ui-corner-all"> Nhập vào kiểu số 1-100</td>
                          </tr>
                          <tr>
                            <td><label for="cost">Chi phí/Tháng(*)</label></td>
                            <td><input type="text" name="cost" id="cost" value="" class="text ui-widget-content ui-corner-all"></td>
                          </tr>
                          <tr>
                            <td><label for="method">Thanh toán(*)</label></td>
                            <td>
                              <select name="method">
                                <option class="selected" value="Hằng năm">Hằng năm</option>
                                <option value="Hằng tháng">Hằng tháng</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><label for="useractive">Người dùng(*)</label></td>
                            <td><input type="text" name="useractive" id="useractive" value="" class="text ui-widget-content ui-corner-all"></td>
                          </tr>
                          <tr>
                            <td><label for="email">Email(*)</label></td>
                            <td>
                              <select name="email">
                                <option class="selected" value="Không">Không</option>
                                <option value="Có">Có</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><label for="logo">Logo(*)</label></td>
                            <td>
                              <select name="logo">
                                <option class="selected" value="Không">Không</option>
                                <option value="Có">Có</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><label for="support">Hỗ trợ(*)</label></td>
                            <td>
                              <select name="support">
                                <option class="selected" value="Không">Không</option>
                                <option value="Có">Có</option>
                              </select>
                            </td>
                          </tr>

                          <!-- Allow form submission with keyboard without duplicating the dialog button -->
                          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                        </fieldset>
                        </tbody>
                      </table>

                    </div>
                    <div class="assign-footer-opt clearfix hidden">
                      <span id="assignEmails">(*)  Trường thông tin bắt buộc</span>
                    </div>
                    <div class="assign-footer clearfix">
                      <div class="float-left">
                        <input type="submit" class="big-button drop" value="<?php print_string('save', 'admin') ?>" />

                        <?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                    </div>
                  </form>


                </div></div> </div></div>
          <!-- Edit service pack         -->
          <div id="<?php print "facebox-edit" ?>"style="display: none;">      
            <div class="popup">         <div class="content">

                <div id="assignItemBox">
                  <div class="assign-header">
                    <span style="color: #fff; font-weight: bolder;">Chỉnh sửa gói dịch vụ</span>
                  </div>
                  <form action="<?php echo $CFG->wwwroot ?>/account/servicepack.php?action=insertpack" method="post">
                    <div id="assignResults">
                      <table cellspacing="0" class="item-page-list">
                        <tbody>
                        <fieldset>
                          <tr>
                            <td><label for="name">Tên(*)</label></td>
                            <td><input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"></td>
                          </tr>
                          <tr>
                            <td><label for="code">Mã(*)</label></td>
                            <td><input type="text" name="code" id="code" value="" class="text ui-widget-content ui-corner-all"> Nhập vào kiểu số 1-100</td>
                          </tr>
                          <tr>
                            <td><label for="cost">Chi phí/Tháng(*)</label></td>
                            <td><input type="text" name="cost" id="cost" value="" class="text ui-widget-content ui-corner-all"></td>
                          </tr>
                          <tr>
                            <td><label for="method">Thanh toán(*)</label></td>
                            <td>
                              <select name="method">
                                <option class="selected" value="Hằng năm">Hằng năm</option>
                                <option value="Hằng tháng">Hằng tháng</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><label for="useractive">Người dùng(*)</label></td>
                            <td><input type="text" name="useractive" id="useractive" value="" class="text ui-widget-content ui-corner-all"></td>
                          </tr>
                          <tr>
                            <td><label for="email">Email(*)</label></td>
                            <td>
                              <select name="email">
                                <option class="selected" value="Không">Không</option>
                                <option value="Có">Có</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><label for="logo">Logo(*)</label></td>
                            <td>
                              <select name="logo">
                                <option class="selected" value="Không">Không</option>
                                <option value="Có">Có</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td><label for="support">Hỗ trợ(*)</label></td>
                            <td>
                              <select name="support">
                                <option class="selected" value="Không">Không</option>
                                <option value="Có">Có</option>
                              </select>
                            </td>
                          </tr>

                          <!-- Allow form submission with keyboard without duplicating the dialog button -->
<!--                          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">-->
                        </fieldset>
                        </tbody>
                      </table>

                    </div>
                    <div class="assign-footer-opt clearfix hidden">
                      <span id="assignEmails">(*)  Trường thông tin bắt buộc</span>
                    </div>
                    <div class="assign-footer clearfix">
                      <div class="float-left">
                        <input type="submit" class="big-button drop" value="<?php print_string('save', 'admin') ?>" />
                        <?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                    </div>
                  </form>
                </div></div> </div></div>
        </td>
      </tr>
    </tbody></table>
</div>


