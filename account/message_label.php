<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');
require_login(0, false);
//require('lib.php');
require($CFG->dirroot . '/common/lib.php');
$PAGE->set_title(get_string('account'));
$PAGE->set_heading(get_string('account'));

echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$hide_message_tab = optional_param('hide_message_tab', '', PARAM_TEXT);
$unretricted_messages = optional_param('unretricted_messages', '', PARAM_TEXT);
$login_message = optional_param('login_message', '', PARAM_TEXT);
$welcome_message = optional_param('welcome_message', '', PARAM_TEXT);
$hide_course_date = optional_param('hide_course_date', '', PARAM_TEXT);
$account_id = optional_param('id', 2, PARAM_INT);

$user_id = optional_param('user_id', '', PARAM_TEXT);

global $CFG;
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/message_label.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        

<?php
if (is_siteadmin()) {
  if ($action == 'update_message') {
    update_user_label_message($user_id, $hide_message_tab, $unretricted_messages, $login_message, $welcome_message, $hide_course_date);
    $SELECT = select_account_id($account_id);
  }
  else {
    $SELECT = select_account_id($account_id);
  }
  foreach ($SELECT as $USE) {
    ?> 

      <table class="admin-fullscreen">
        <tbody><tr>
            <td class="admin-fullscreen-left">
              <div class="admin-fullscreen-content">

                <div class="focus-panel">
                  <div class="panel-head">
                    <div style="float: right">
                      <label> <?php echo $USE->username ?></label>
                    </div>
                    <h3>
                      <?php print_string('account') ?>
                    </h3>
                  </div>
                  <div class="body">

                    <div class="grid-tabs">
                      <ul>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $account_id; ?>"><?php print_string('profile') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $account_id; ?>" class=""><?php print_string('theme') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $account_id; ?>" class=""><?php print_string('billing') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $account_id; ?>" class="selected"><?php print_string('messagelab') ?></a></li>                 
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $account_id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $account_id; ?>" class=""><?php print_string('term') ?></a></li>   
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/servicepack.php?id=<?php echo $account_id; ?>" class=""><?php print_string('servicepack') ?></a></li>
                      </ul>
                      <div class="clear">
                      </div>
                    </div>

                    <form method="post" action="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USE->id ?>&action=update_message">
                      <input type="hidden" value="<?php echo $USE->id ?>" name="user_id" id="user_id">
                      <p>Use these settings to customize the emails sent from Alphatechnologies and the messages that your learners see during their learning journey.</p>
                      <div class="form-sub-heading">
                        Message Sending Options

                      </div> 
                      <div style="margin-left:10px">

                        <input type="checkbox" value="<?php echo $USE->hide_message_tab ?>" name="hide_message_tab" id="hide_message_tab" <?php echo $USE->hide_message_tab != 0 ? 'checked="checked"' : ''; ?>>
                        <b>Hide Messages tab for Learners</b></div>  
                      <div style="margin-left:10px">
                        <input type="checkbox" value="<?php echo $USE->unretricted_messages ?>" name="unretricted_messages" id="unretricted_messages" <?php echo $USE->unretricted_messages != 0 ? 'checked="checked"' : ''; ?>>
                        <b>Allow learners to send messages to any other user.</b> <span class="tip">If unchecked they can still send to people in their teams.</span></div> 


                      <div class="form-sub-heading">
                        Login Screen
                      </div>                                                               
                      <p style="margin-left: 10px">Use this message to welcome your learners on the login screen.</p>
                      <textarea style="width:95%;margin-left: 10px" rows="10" name="login_message" id="login_message" cols="20">
    <?php echo $USE->login_message ?>	
                      </textarea>


                      <div class="form-sub-heading">
                        Trainee Home Page
                      </div>                                 
                      <p style="margin-left: 10px">This is first message your learners see once they have logged in.</p>
                      <textarea style="width:95%;margin-left: 10px" rows="10" name="welcome_message" id="welcome_message" cols="20">
    <?php echo $USE->welcome_message ?>
                      </textarea>

                      <p style="margin-left: 10px">
                        <input type="checkbox" value="<?php echo $CFG->hide_course_date ?>" name="hide_course_date" id="hide_course_date" <?php echo $USE->hide_course_date != 0 ? 'checked="checked"' : ''; ?>>

                        <b>Hide Course Assigned &amp; Completed Dates</b>
                      </p>
                      <div style="display: none">
                        <div class="form-sub-heading">
                          Custom User Data/ Fields
                        </div> 
                        <p>Enter custom field labels to add additional fields on a person's profile. You can make all new people enter these fields on their first login by checking the option "make field compulsory".</p>

                        <table class="simple-form flexible-form">
                          <tbody><tr>
                              <th>
                                Custom Field Label 1:
                              </th>
                              <td>
                                <input type="text" value="" size="40" name="custom_field_1" id="custom_field_1">
                                <input type="checkbox" value="" name="custom_field_1_require" id="custom_field_1_require">
                                <span class="tip">Make field compulsory</span>  
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Custom Field Label 2:
                              </th>
                              <td>
                                <input type="text" value="" size="40" name="CustomField2" id="CustomField2"> <input type="checkbox" value="true" name="CustomField2Required" id="CustomField2Required"><input type="hidden" value="false" name="CustomField2Required"> <span class="tip">Make field compulsory</span>  
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Custom Field Label 3:
                              </th>
                              <td>
                                <input type="text" value="" size="40" name="CustomField3" id="CustomField3"> <input type="checkbox" value="true" name="CustomField3Required" id="CustomField3Required"><input type="hidden" value="false" name="CustomField3Required"> <span class="tip">Make field compulsory</span>  
                              </td>
                            </tr>
                          </tbody></table> 


                        <div class="form-sub-heading">
                          Custom Labels
                        </div> 
                        <table class="simple-form flexible-form">
                          <tbody><tr>
                              <th>
                                Course/Module Complete:
                              </th>
                              <td>
                                <input type="text" value="Complete" size="40" name="ResultComplete" id="ResultComplete">
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Course/Module Not Complete:
                              </th>
                              <td>
                                <input type="text" value="Not Complete" size="40" name="ResultNotComplete" id="ResultNotComplete">
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Module Requires Marking:
                              </th>
                              <td>
                                <input type="text" value="Marking Required" size="40" name="ResultMarkingRequired" id="ResultMarkingRequired">
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Module In Progress:
                              </th>
                              <td>
                                <input type="text" value="In Progress" size="40" name="ResultInProgress" id="ResultInProgress">
                              </td>
                            </tr>
                          </tbody></table> 
                      </div>
                      <div class="form-buttons">
                        <input type="submit" class="btnlarge" value="Save">
                      </div>
                    </form>
                  </div>                            
                </div>



              </div>
            </td>
            <td class="admin-fullscreen-right">
              <div class="admin-fullscreen-content">


              </div>
            </td>
          </tr>
        </tbody></table>

    <?php
  }
}
else {
  if ($action == 'update_message') {
    update_user_label_message($user_id, $hide_message_tab, $unretricted_messages, $login_message, $welcome_message, $hide_course_date);
    $USER = get_user_from_user_id($user_id);
  }
  else {
    global $USER;
  }
  ?>
    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">

              <div class="focus-panel">
                <div class="panel-head">
                  <h3>
                    <?php print_string('account') ?>
                  </h3>
                </div>
                <div class="body">

                  <div class="grid-tabs">
                    <ul>
                      <li><a href="<?php echo $CFG->wwwroot ?>/account/index.php?id=<?php echo $USER->id; ?>"><?php print_string('profile') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/theme.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('theme') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/billing.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('billing') ?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/message_label.php?id=<?php echo $USER->id; ?>" class="selected"><?php print_string('messagelab') ?></a></li>                 
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/ecommerce.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('ecommerce') ?></a></li>  
                        <li><a href="<?php echo $CFG->wwwroot ?>/account/term.php?id=<?php echo $USER->id; ?>" class=""><?php print_string('term') ?></a></li>   
                    </ul>
                    <div class="clear">
                    </div>
                  </div>
                  <form method="post" action="<?php echo $CFG->wwwroot ?>/account/message_label.php?action=update_message">
                    <input type="hidden" value="<?php echo $USER->id ?>" name="user_id" id="user_id">
                    <p>Use these settings to customize the emails sent from Alphatechnologies and the messages that your learners see during their learning journey.</p>
                    <div class="form-sub-heading">
                      Message Sending Options
                    </div> 
                    <div style="margin-left:10px">
                      <input type="checkbox" value="1" name="hide_message_tab" id="hide_message_tab" <?php echo $USER->hide_message_tab != 0 ? 'checked="checked"' : ''; ?>>
                      <b>Hide Messages tab for Learners</b></div>  
                    <div style="margin-left:10px">
                      <input type="checkbox" value="1" name="unretricted_messages" id="unretricted_messages" <?php echo $USER->unretricted_messages != 0 ? 'checked="checked"' : ''; ?>>
                      <b>Allow learners to send messages to any other user.</b> <span class="tip">If unchecked they can still send to people in their teams.</span></div> 
                    <div class="form-sub-heading">
                      Login Screen
                    </div>                                                               
                    <p style="margin-left: 10px">Use this message to welcome your learners on the login screen.</p>
                    <textarea style="width:95%;margin-left: 10px" rows="10" name="login_message" id="login_message" cols="20">
  <?php echo $USER->login_message ?>	
                    </textarea>
                    <div class="form-sub-heading">
                      Trainee Home Page
                    </div>                                 
                    <p style="margin-left: 10px">This is first message your learners see once they have logged in.</p>
                    <textarea style="width:95%;margin-left: 10px" rows="10" name="welcome_message" id="welcome_message" cols="20">
  <?php echo $USER->welcome_message ?>
                    </textarea>
                    <p style="margin-left: 10px">
                      <input type="checkbox" value="<?php echo $CFG->hide_course_date ?>" name="hide_course_date" id="hide_course_date" <?php echo $USER->hide_course_date != 0 ? 'checked="checked"' : ''; ?>>

                      <b>Hide Course Assigned &amp; Completed Dates</b>
                    </p>
                    <div style="display: none">
                      <div class="form-sub-heading">
                        Custom User Data/ Fields
                      </div> 
                      <p>Enter custom field labels to add additional fields on a person's profile. You can make all new people enter these fields on their first login by checking the option "make field compulsory".</p>

                      <table class="simple-form flexible-form">
                        <tbody><tr>
                            <th>
                              Custom Field Label 1:
                            </th>
                            <td>
                              <input type="text" value="" size="40" name="custom_field_1" id="custom_field_1">
                              <input type="checkbox" value="" name="custom_field_1_require" id="custom_field_1_require">
                              <span class="tip">Make field compulsory</span>  
                            </td>
                          </tr>
                          <tr>
                            <th>
                              Custom Field Label 2:
                            </th>
                            <td>
                              <input type="text" value="" size="40" name="CustomField2" id="CustomField2"> <input type="checkbox" value="true" name="CustomField2Required" id="CustomField2Required"><input type="hidden" value="false" name="CustomField2Required"> <span class="tip">Make field compulsory</span>  
                            </td>
                          </tr>
                          <tr>
                            <th>
                              Custom Field Label 3:
                            </th>
                            <td>
                              <input type="text" value="" size="40" name="CustomField3" id="CustomField3"> <input type="checkbox" value="true" name="CustomField3Required" id="CustomField3Required"><input type="hidden" value="false" name="CustomField3Required"> <span class="tip">Make field compulsory</span>  
                            </td>
                          </tr>
                        </tbody></table> 
                      <div class="form-sub-heading">
                        Custom Labels
                      </div> 
                      <table class="simple-form flexible-form">
                        <tbody><tr>
                            <th>
                              Course/Module Complete:
                            </th>
                            <td>
                              <input type="text" value="Complete" size="40" name="ResultComplete" id="ResultComplete">
                            </td>
                          </tr>
                          <tr>
                            <th>
                              Course/Module Not Complete:
                            </th>
                            <td>
                              <input type="text" value="Not Complete" size="40" name="ResultNotComplete" id="ResultNotComplete">
                            </td>
                          </tr>
                          <tr>
                            <th>
                              Module Requires Marking:
                            </th>
                            <td>
                              <input type="text" value="Marking Required" size="40" name="ResultMarkingRequired" id="ResultMarkingRequired">
                            </td>
                          </tr>
                          <tr>
                            <th>
                              Module In Progress:
                            </th>
                            <td>
                              <input type="text" value="In Progress" size="40" name="ResultInProgress" id="ResultInProgress">
                            </td>
                          </tr>
                        </tbody></table> 
                    </div>
                    <div class="form-buttons">
                      <input type="submit" class="btnlarge" value="Save">
                    </div>
                  </form>
                </div>                            
              </div>
            </div>
          </td>
          <td class="admin-fullscreen-right">
            <div class="admin-fullscreen-content">
            </div>
          </td>
        </tr>
      </tbody></table>
  <?php
}
?>


</div>