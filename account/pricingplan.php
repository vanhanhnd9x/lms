
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}
global $CFG;
require('../config.php');
require_login(0, false);
require($CFG->dirroot . '/common/lib.php');
//$PAGE->set_title("Đăng ký gói sử dụng");
//$PAGE->set_heading("Đăng ký gói sử dụng");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <title> Đăng ký gói sử dụng</title>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
      <script type="text/javascript" src="js/jquery.counter.min.js"></script>
      <script type="text/javascript" src="js/jquery.scrollTo-min.js"></script>
      <script type="text/javascript" src="js/jquery.bgiframe.min.js"></script>
      <script type="text/javascript" src="js/jquery.raty.min.js"></script>
      <script type="text/javascript" src="js/bootstrap.js"></script>
      <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css">
          <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
            <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css">
              <link type="text/css" rel="stylesheet" href="css/fullcalendar.css">
                <link type="text/css" rel="stylesheet" href="css/price.css">
                  </head>
                  <?php
                  $sp1 = '1';
                  $sp2 = '2';
                  $sp3 = '3';
                  $sp4 = '4';
                  $sp5 = '5';
                  $sp6 = '6';
                  $sp7 = '7';
                  $sp8 = '8';
                  ?>
                  <body>
                    <div id="page-body">
                      <form method="post" action="payment-method.php">
                        <input type="hidden" value="" name="__RequestVerificationToken">
                          <input type="hidden" value="" name="SubscriptionId" id="SubscriptionId">
                            <div class="container">
                              <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-10">
                                  <h2><?php print_string('upgradeto') ?></h2>
                                  <div>
                                    <div class="btn-group btn-group radio-grp">
                                      <button value="1" class="btn btn-primary btn-input" type="button">
                                        <?php print_string('annual') ?></button>
                                      <button value="0" class="btn btn-default btn-input" type="button">
                                        <?php print_string('monthtom') ?></button>
                                      <input type="hidden" value="0" name="is_private">
                                    </div>
                                  </div>
                                  <br>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-10 panel">
                                  <ul class="pricing-table">

                                    <li data-rel="1" class="price-block">
                                      <div class="pricing-header">
                                        <?php
                                        $servicepack1 = get_service_package($sp1);
                                        ?>
                                        <h3> <?php echo $servicepack1->name ?></h3>

                                        <h4>
                                          <?php echo number_format($servicepack1->cost) ?>vnd
                                          <span><?php print_string('permonth') ?></span></h4>

                                        <h4>
                                          <span style="font-weight: bold;"><?php print_string('billingannual') ?></span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack1->user_active ?></b> <?php print_string('useractive') ?></li>
                                        <?php
                                        if ($servicepack1->email == "Có") {
                                          ?>
                                          <li><?php print_string('supportemail') ?></li>
                                          <?php
                                        }
                                        if ($servicepack1->logo == "Có") {
                                          ?>
                                          <li><?php print_string('branding') ?></li>
                                          <?php
                                        }
                                        ?>
                                        <li></li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value="<?php print_string('register') ?>" onclick="submitForm('1')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="1" class="price-block">
                                      <div class="pricing-header">
<?php
$servicepack2 = get_service_package($sp2);
?>
                                        <h3><?php echo $servicepack2->name ?></h3>
                                        <h4>
                                        <?php echo number_format($servicepack2->cost) ?>vnd
                                          <span><?php print_string('permonth') ?></span></h4>

                                        <h4>
                                          <span style="font-weight: bold;"><?php print_string('billingannual') ?></span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack2->user_active ?></b> <?php print_string('useractive') ?></li>

<?php
if ($servicepack2->email == "Có") {
  ?>
                                          <li><?php print_string('supportemail') ?></li>
                                          <?php
                                        }
                                        if ($servicepack2->logo == "Có") {
                                          ?>
                                          <li><?php print_string('branding') ?></li>
                                          <?php
                                        }
                                        ?>
                                        <li></li>

                                        <li>Instructor Led Training</li>
                                        <li>Phát triển API</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value="<?php print_string('register') ?>" onclick="submitForm('2')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="1" class="price-block most-popular">
                                      <div class="pricing-header">
<?php
$servicepack3 = get_service_package($sp3);
?>
                                        <h3><?php echo $servicepack3->name ?></h3>
                                        <h4>
                                        <?php echo number_format($servicepack3->cost) ?>vnd
                                          <span><?php print_string('permonth') ?></span></h4>

                                        <h4>
                                          <span style="font-weight: bold;"><?php print_string('billingannual') ?></span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack3->user_active ?></b><?php print_string('useractive') ?></li>

<?php
if ($servicepack3->email == "Có") {
  ?>
                                          <li><?php print_string('supportemail') ?></li>
                                          <?php
                                        }
                                        if ($servicepack3->logo == "Có") {
                                          ?>
                                          <li><?php print_string('branding') ?></li>
                                          <?php
                                        }
                                        ?>
                                        <li>Shopify Integration</li>

                                        <li>Instructor Led Training</li>
                                        <li>Phát triển API</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value="<?php print_string('register') ?>" onclick="submitForm('3')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="1" class="price-block">
                                      <div class="pricing-header">
<?php
$servicepack4 = get_service_package($sp4);
?>
                                        <h3><?php echo $servicepack4->name ?></h3>
                                        <h4>
<?php echo number_format($servicepack4->cost) ?>vnd
                                          <span><?php print_string('permonth') ?></span></h4>

                                        <h4>
                                          <span style="font-weight: bold;"><?php print_string('billingannual') ?></span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack4->user_active ?></b><?php print_string('useractive') ?></li>

<?php
if ($servicepack4->email == "Có") {
  ?>
                                          <li><?php print_string('supportemail') ?></li>
  <?php
}
if ($servicepack4->logo == "Có") {
  ?>
                                          <li><?php print_string('branding') ?></li>
                                          <?php
                                        }
                                        ?>

                                        <li>Instructor Led Training</li>
                                        <li>Developer API</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value="<?php print_string('register') ?>" onclick="submitForm('4')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="0" class="price-block" style="display: none;">
                                      <div class="pricing-header">
<?php
$servicepack5 = get_service_package($sp5);
?>
                                        <h3><?php echo $servicepack5->name ?></h3>
                                        <h4>
<?php echo number_format($servicepack5->cost) ?>vnd
                                          <span>Mỗi tháng</span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack5->user_active ?></b> người dùng</li>

                                        <li>
                                          Hỗ trợ E-mail</li>

                                        <li>Tuỳ chỉnh thương hiệu</li>
                                        <li>Shopify Integration</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value=" Đăng ký" onclick="submitForm('5')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="0" class="price-block" style="display: none;">
                                      <div class="pricing-header">
<?php
$servicepack6 = get_service_package($sp6);
?>
                                        <h3><?php echo $servicepack6->name ?></h3>
                                        <h4>
<?php echo number_format($servicepack6->cost) ?>vnd
                                          <span>Mỗi tháng</span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack6->user_active ?></b> người dùng</li>

                                        <li>
                                          Hỗ trợ E-mail</li>

                                        <li>Tuỳ chỉnh thương hiệu</li>
                                        <li>Shopify Integration</li>

                                        <li>Instructor Led Training</li>
                                        <li>Phát triển API</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value=" Đăng ký" onclick="submitForm('6')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="0" class="price-block most-popular" style="display: none;">
                                      <div class="pricing-header">
<?php
$servicepack7 = get_service_package($sp7);
?>
                                        <h3><?php echo $servicepack7->name ?></h3>
                                        <h4>
<?php echo number_format($servicepack7->cost) ?>vnd
                                          <span>Mỗi tháng</span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack7->user_active ?></b> người dùng</li>

                                        <li>
                                          Hỗ trợ E-Mail</li>

                                        <li>Tuỳ chỉnh thương hiệu</li>
                                        <li>Shopify Integration</li>

                                        <li>Instructor Led Training</li>
                                        <li>Phát triển API</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value=" Đăng ký" onclick="submitForm('7')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <li data-rel="0" class="price-block" style="display: none;">
                                      <div class="pricing-header">
<?php
$servicepack8 = get_service_package($sp8);
?>
                                        <h3><?php echo $servicepack8->name ?></h3>
                                        <h4>
<?php echo number_format($servicepack8->cost) ?>vnd
                                          <span>Mỗi tháng</span></h4>

                                      </div>
                                      <ul class="features">
                                        <li>
                                          <b><?php echo $servicepack8->user_active ?></b> người dùng</li>

                                        <li>
                                          Hỗ trợ E-mail</li>

                                        <li>Tuỳ chỉnh thương hiệu</li>
                                        <li>Shopify Integration</li>

                                        <li>Instructor Led Training</li>
                                        <li>Phát triển API</li>

                                      </ul>

                                      <div class="pricing-footer">
                                        <input type="button" alt="Đăng ký" value=" Đăng ký" onclick="submitForm('8')" class="btn btn-warning">
                                      </div>

                                    </li>

                                    <!-- Platinum -->
                                    <li class="price-block enterprise">
                                      <div class="pricing-header">
                                        <h3>Doanh nghiệp</h3>
                                        <h4>
                                          Liên hệ chúng tôi<span>về thông tin thanh toán nếu bạn muốn tăng tuỳ chọn</span>
                                        </h4>
                                      </div>
                                      <ul class="features">
                                        <li>Số lượng người dùng</li>
                                        <li>Tuỳ chỉnh điều khoản/kế hoạch</li>
                                        <li></li>
                                        <li>Nhiều tài khoản</li>
                                        <li>Khả năng SSO</li>
                                        <li>Gói nội dung</li>
                                        <li>Giảm giá cho nhiều năm</li>
                                        <li>Mở rộng giới hạn API</li>
                                      </ul>
                                      <div class="pricing-footer">
                                        <a href="mailto:tung@thienhoang.comvn?subject=Đăng ký gói tuỳ chọn LMS">Liên hệ chi tiết</a>
                                      </div>
                                    </li>
                                    <!-- Enterprise -->
                                  </ul>
                                  <div>
                                    <div class="plan-annual">
                                      <ul class="big-list">
                                        <li>Bạn đăng ký gói sử dụng này. Bắt đầu từ hôm nay 29/09/2014</li>
                                        <li>Bạn có thể đăng ký sử dụng các gói đăng ký hàng năm lớn hơn</li>
                                        <li>Bạn không thể xuống gói đăng ký thấp hơn hoặc gói hàng tháng</li>
                                      </ul>
                                    </div>
                                    <div class="plan-mtm" style="display: none">
                                      <ul class="big-list">
                                        <li>Bạn sẽ phải thanh toán hàng tháng</li>
                                        <li>Bạn có thể nâng cấp hoặc đăng ký gói sử dụng thấp hơn bất kỳ lúc nào</li>
                                        <li>Bạn có thể chuyển sang đăng ký gói sử dụng hàng năm bất kỳ nếu giá trị thanh toán lớn hơn hiện tại</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-10">
                                  <div class="col-lg-6">
                                    <!-- /input-group -->
                                  </div>
                                  <div class="clear">
                                  </div>
                                  <!-- /.col-lg-6 -->
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-10">
                                  <!-- /.row -->
                                  <div>
                                    <div class="pricing-questions">
                                      <h4>
                                        Người dùng đăng ký là gì?</h4>
                                      An "active user" is any trainer or trainee who signs in during the billing
                                      period. Once a user has become "active" for the billing period, there
                                      is no limit to the number of times he or she can sign in.<p>
                                        Users who do NOT sign in during the billing period are considered inactive for that
                                        period, and will not count towards your monthly active users limit.</p>
                                      <h4>
                                        Điều gì sẽ xảy ra nếu số lượng người dùng của tôi quá với giới hạn gói đăng ký sử dụng?</h4>
                                      We won't restrict you from adding new users when you need to. If you exceed the
                                      active users included in your plan you will be charged at your selected plans extra
                                      user rate at the start of the next billing period.
                                    </div>
                                  </div>
                                  <div>
                                    <div class="pricing-questions">
                                      <h4>
                                        Có hợp đồng về đăng ký không?</h4>
                                      <p>
                                        Litmos is a pay-as-you-go service. You do not commit to any long term contracts.
                                        If you decide to cancel you'll be billed for the current billing period and that's
                                        it.</p>
                                      <h4>
                                        Làm thế nào nó hoạt động nếu tôi đăng ký gói thấp hơn gói của tôi đang sử dụng?</h4>
                                      <p>
                                        If you downgrade you will be charged at the new rate from your next billing date
                                        onwards. We do not issue pro-rated refunds for part months.</p>
                                      <h4>
                                        Hình thức thanh toán nào được chấp nhận?</h4>
                                      We currently accept Visa &amp; Mastercard.
                                    </div>
                                  </div>
                                </div>
                              </div>


                            </div></form>

<!--                          <script src="https://static1.litmos.com/static/release/utils.js?v=2014.09.03" type="text/javascript"></script>-->
                            <script type="text/javascript">
                              function submitForm(v) {
                                $("#SubscriptionId").val(v);
                                $("form").submit();
                              }

                              $(".price-block[data-rel='0']").hide();

                              $(".radio-grp button").on('click', function () {
                                var btn = $(this),
                                container = btn.parent(),
                                hidden = container.find('input'),
                                value = btn.attr('value');
                                hidden.val(value);
                                if (value == '0') {
                                  $(".price-block[data-rel='1']").hide();
                                  $(".price-block[data-rel='0']").show();
                                  $(".plan-annual").hide();
                                  $(".plan-mtm").show();
                                } else {
                                  $(".price-block[data-rel='0']").hide();
                                  $(".price-block[data-rel='1']").show();


                                  $(".plan-annual").show();
                                  $(".plan-mtm").hide();
                                }

                                $(".btn-input").removeClass('btn-primary');
                                $(".btn-input").addClass('btn-default');
                                btn.addClass('btn-primary');
                              });
                            </script>
                            </div></body></html>

