<?php
define('AJAX_SCRIPT', true);
require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');

$s = required_param('s', PARAM_TEXT); // course id
$action = required_param('action', PARAM_ACTION);
//echo $action;

switch ($action) {
    case 'searchCourse':
        $courses = search_enrol_get_my_courses($s);
        //var_dump($courses);
        ?>     
        <ul class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-transition="none" data-dividertheme="a" data-theme="c" data-inset="true" data-role="listview">
            <li class="list-heading ui-li ui-li-divider ui-bar-a ui-corner-top" data-role="list-divider" role="heading"><?php print_r(get_string('courselibrary')) ?></li>
        <?php
        foreach ($courses as $key => $course) {
            //var_dump($course);
            $course_setting = get_course_settings($course->id);
            //var_dump($course_setting);
//            $completion = new completion_info($course);
//            $coursecomplete = $completion->is_course_complete($USER->id);
            $score = count_course_completion($course, $USER->id);
            
            ?>
                <li class="arrow ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c" data-transition="none" data-id="21593" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="c">
                    <div class="ui-btn-inner ui-li">
                        <a class="ui-link-inherit" data-transition="none" href="<?php echo new moodle_url('view.php', array('id' => $course->id)); ?>"> 
                            <div class="ui-btn-text">

            <?php echo $course->fullname . '   '; ?>     

                                <div class="tip">
                                <?php echo $course->idnumber; ?>
                                </div>
                                <div>
                                    <span class="box-mobile-right">
                                        <span class="box-mobile box-blue"><?php echo $score . "%" . " complete"; ?></span>
                                    </span>
                                </div>
                            </div>
                            <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"> </span>
                        </a>
                    </div>
                </li>
            <?php
        }//}}
        ?>
        </ul>
<?php
        break;
        
    }
    ?>

