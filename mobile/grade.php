<?php
require_once('../config.php');
require("../grade/lib.php");
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/lib/completionlib.php');
require_once($CFG->dirroot . '/lib/completion/completion_completion.php');
require_once($CFG->dirroot . '/manage/news/lib.php'); // Require for right block news
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_login();

$PAGE->set_title($course->fullname);
$PAGE->set_heading($course->fullname);
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();
$page = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage = optional_param('perpage', '10', PARAM_INT); // how many per page
$id = optional_param('id', 0, PARAM_INT);
$course = $DB->get_record('course', array('id' => $id));
//add code
$courses = enrol_get_my_courses();
$info = new completion_info($course);
// Load criteria to display
$completions = $info->get_completions($USER->id);							
// Is course complete?
$coursecomplete = $info->is_course_complete($USER->id);

// Has this user completed any criteria?
$criteriacomplete = $info->count_course_user_data($USER->id);


 
  $modinfo = get_fast_modinfo($course);
  
  
  $sections = $modinfo->sections[0];
  
  $cmid_first = current($sections);
  $modinfo = get_fast_modinfo($course);
 
	/// extra fast view mode
	        
    if (!empty($modinfo->sections[0])) {
    
    foreach ($modinfo->sections[0] as $cmid) {
        if($cmid_first == $cmid){
            $cm = $modinfo->cms[$cmid];
            if (!$cm->uservisible) {
                continue;
            }

            if (($url = $cm->get_url())) {
                $url_start_course = $url;
            }
        }
        
    }
}

?>



<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">


    <div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
        <div style="float:left;padding:10px;"><?php echo $USER->company; ?></div>
        <div style="float:right;padding:10px;">

<!--            <img src="" alt="AVATA" class="mini-pic" border="0"> -->
            <span class="header-text"><?php echo $USER->username; ?></span>&nbsp;
        </div>
    </div>



    <div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                    <td class="menu" valign="top">



                        <ul class="super-nav2">

<!--                            <li><a href="index.php" link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>-->
                            <li class="active"><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>


                            <li><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
                            <li><a href="messages.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
                            <li><a href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
                        </ul> 


                    </td>



                    <td class="content" valign="top">
                        <div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <div class="grid-tabs">
                <ul>
                    <li><a href="<?php print new moodle_url('view.php', array('id' => $course->id)); ?>" class="">Content</a></li>
                    <li><a href="<?php print new moodle_url('grade.php', array('id' => $course->id)); ?>" class="selected">Grade</a></li>
                </ul>
                <div class="clear">
                </div>
            </div> 
            <div class="panel-head">
                <h3><?php echo $course->fullname; ?></h3>
                <div class="subtitle"></div>
            </div>
            
            <div class="heading-title">I need to complete the following modules to finish this training course.</div>
            <?php
            require_once $CFG->libdir . '/gradelib.php';
            require_once $CFG->dirroot . '/grade/lib.php';
            require_once $CFG->dirroot . '/grade/report/user/lib.php';

            
            grade_report_user_course_view($course, $USER);
                   
            
            
            ?>

        </div> 
    </div><!-- Left Content -->

    <!-- Right Block -->
    <div id="admin-fullscreen-right">
        <div class="side-panel">
            <div class="action-buttons">
                <ul>
                    <?php if(!$criteriacomplete): ?>
                    <li><a href="<?php echo $url_start_course ;?>" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span>Start this course</span></span></span></span></a></li>
                    <?php endif; ?>
                        
                </ul>
            </div>
        </div>
        <div class="side-panel">
            <h3>My Achievements</h3>
            <ul>
                
                <li>   
                    
                    <div class="padded tip">                
                        Completed on Feb 8, 2012
                            
                    </div>
                    <div class="clear"></div>
                </li>
                    
            </ul>
        </div>
    </div>  <!-- End Right -->

</div>
                    </td>




                </tr>
                
            </tbody></table>
    </div> 
</div> 

<?php
echo $OUTPUT->footer();
?>