<?php
require_once('../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/message/lib.php');
require_login();

$PAGE->set_title("Messages");
$PAGE->set_heading("Messages");
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();

$coursename = optional_param('coursename', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
$message = optional_param('Message', '', PARAM_TEXT);
$user_from = optional_param('user_from', 0, PARAM_INT);
$hidden_to = optional_param('hidden_to', 0, PARAM_TEXT);
$to_id = parseUserIdParam_second($hidden_to);




global $USER, $CFG;
$redirectPath = $CFG->wwwroot . "/mobile/message.php";

if ($action == 'reply') {

  $userfrom = $USER;
  $userto = getUserFromId_second($to_id);
  $result = message_post_message_second($userfrom, $userto, $message, FORMAT_MOODLE);
  if ($result > 0) {
    //echo displayJsAlert('Reply message successfully', $redirectPath);
    echo displayJsAlert_second('', $redirectPath);
  }
}
?>



<div class="ui-page ui-body-c ui-page-active" data-type="messages" data-role="page" data-url="/messages/thread/424304" data-external-page="true" tabindex="0" style="min-height: 312px;">
  <div class="ui-header ui-bar-a" data-backbtn="true" data-role="header" role="banner">
    <h1 class="ui-title text_ipad_header" role="heading" aria-level="1"><?php print_r(get_string(newmessage)) ?> </h1>
  </div>
  <div class="ui-content" data-role="content" role="main">
    <form method="post" action="message_progress.php?action=reply">
      <input type="hidden" value="qPX8ScygToWuvtZ7BVLjJfWVmjlj2R/s1WT837VrmtqYyF2FQXJINIxiDDF3hnpz1C+7w0uj1ZEmz0Q2awiOhKkA9m3ZxMMHUq9d5vR/JFr60mGikm+zeOKGufXZDQ6K6ZkWz2Ajk3lYc+sADElq7P/+T1nkXqKWr269TbMK/a0bSca/g0LGfAG4159KpFAF" name="__RequestVerificationToken">
      <div class="ui-field-contain ui-body ui-br" data-role="fieldcontain">

<?php
if ($action == 'read_message') {
  global $DB;
  $user = $DB->get_record('user', array('id' => $user_from));
  message_mark_messages_read($USER->id, $user->id);

  global $USER, $CFG;
  $hidden_to = "";

  $histories = message_get_history_second($USER, $user, $limitnum = 0, true);

  foreach ($histories as $key) {

    $user_two = getUserFromId_second($key->useridfrom);
    ?>
            <div class="float-right tip"><?php echo time_ago_second($key->timecreated) ?></div>
            <div class="title"><?php echo $user_two->firstname . " " . $user_two->lastname; ?></div>
            <div class="tip">To: <?php
        if ($key->useridto != $USER->id) {

          $hidden_to = $key->useridto;
        }
        $user_three = getUserFromId_second($key->useridto);
        echo $user_three->firstname . " " . $user_three->lastname;
    ?>
            </div>
            <div class="body-dent">
              Content: <?php echo $key->smallmessage; ?>
              <br>
              <br>
              User: <?php echo $USER->firstname . " " . $USER->lastname . " (" . $USER->email . ")"; ?> 
            </div>
  <?php }
} ?>
      </div>
      <div>Reply :</div>
      <div class="ui-field-contain ui-body ui-br" data-role="fieldcontain">
        <textarea id="Message" class="full messageBody ui-input-text ui-body-c ui-corner-all ui-shadow-inset" rows="5" name="Message" cols="20"></textarea>
      </div>
      <div class="ui-field-contain ui-body ui-br" data-inline="true" data-role="fieldcontain">
        <div class="ui-btn ui-btn-up-b ui-btn-inline ui-shadow ui-btn-corner-all ui-fullsize" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="" data-iconpos="" data-theme="b" data-inline="true" data-mini="false" aria-disabled="false">
          <span class="ui-btn-inner ui-btn-corner-all">
            <span class="ui-btn-text text_ipad_header">Reply</span>
          </span>
          <input class="ui-btn-hidden" type="submit" data-theme="b" value="Reply All" data-inline="true" aria-disabled="false">
          <input type="hidden"  name="hidden_to" value="<?php echo $hidden_to ?>"/>
        </div>
        <a class="ui-btn ui-btn-up-c ui-btn-inline ui-shadow ui-btn-corner-all" data-role="button" href="message.php" data-inline="true" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="c">
          <span class="ui-btn-inner ui-btn-corner-all">
            <span class="ui-btn-text">Cancel</span>
          </span>
        </a>
      </div>
    </form>
  </div>
</div> 

<?php
echo $OUTPUT->footer();

function message_get_history_second($user1, $user2, $limitnum = 0, $viewingnewmessages = false) {
  global $DB, $CFG;

  $messages = array();

  //we want messages sorted oldest to newest but if getting a subset of messages we need to sort
  //desc to get the last $limitnum messages then flip the order in php
  $sort = 'asc';
  if ($limitnum > 0) {
    $sort = 'desc';
  }

  $notificationswhere = null;
  //we have just moved new messages to read. If theyre here to see new messages dont hide notifications
  if (!$viewingnewmessages && $CFG->messaginghidereadnotifications) {
    $notificationswhere = 'AND notification=0';
  }

  //prevent notifications of your own actions appearing in your own message history
  $ownnotificationwhere = ' AND NOT (useridfrom=? AND notification=1)';

  if ($messages_read = $DB->get_records_select('message_read', "((useridto = ? AND useridfrom = ?) OR
                                                    (useridto = ? AND useridfrom = ?)) $notificationswhere $ownnotificationwhere", array($user1->id, $user2->id, $user2->id, $user1->id, $user1->id), "timecreated $sort", '*', 0, $limitnum)) {
    foreach ($messages_read as $message) {
      $messages[$message->timecreated] = $message;
    }
  }
  if ($messages_new = $DB->get_records_select('message', "((useridto = ? AND useridfrom = ?) OR
                                                    (useridto = ? AND useridfrom = ?)) $ownnotificationwhere", array($user1->id, $user2->id, $user2->id, $user1->id, $user1->id), "timecreated $sort", '*', 0, $limitnum)) {
    foreach ($messages_new as $message) {
      $messages[$message->timecreated] = $message;
    }
  }

  //if we only want the last $limitnum messages
  ksort($messages);
  $messagecount = count($messages);
  if ($limitnum > 0 && $messagecount > $limitnum) {
    $messages = array_slice($messages, $messagecount - $limitnum, $limitnum, true);
  }

  return $messages;
}

function getUserFromId_second($userId) {
  global $DB;
  $user = $DB->get_record('user', array(
    'id' => $userId
      ));

  return $user;
}

function message_mark_messages_read_second($touserid, $fromuserid) {
  global $DB;

  $sql = 'SELECT m.* FROM {message} m WHERE m.useridto=:useridto AND m.useridfrom=:useridfrom';
  $messages = $DB->get_recordset_sql($sql, array('useridto' => $touserid, 'useridfrom' => $fromuserid));

  foreach ($messages as $message) {
    message_mark_message_read($message, time());
  }

  $messages->close();
}

function parseUserIdParam_second($userIdStr) {
  //$supplierIdStr='val,key';
  if (strripos($userIdStr, ",")) {
    $lastPosOfComma = strripos($userIdStr, ",");
    $userId = substr($userIdStr, $lastPosOfComma + 1, strlen($userIdStr));
    return $userId;
  }
  else {
    return $userIdStr;
  }
}

function message_post_message_second($userfrom, $userto, $message, $format) {
  global $SITE, $CFG, $USER;

  $eventdata = new stdClass();
  $eventdata->component = 'moodle';
  $eventdata->name = 'instantmessage';
  $eventdata->userfrom = $userfrom;
  $eventdata->userto = $userto;

  //using string manager directly so that strings in the message will be in the message recipients language rather than the senders
  $eventdata->subject = get_string_manager()->get_string('unreadnewmessage', 'message', fullname($userfrom), $userto->lang);

  if ($format == FORMAT_HTML) {
    $eventdata->fullmessage = '';
    $eventdata->fullmessagehtml = $message;
  }
  else {
    $eventdata->fullmessage = $message;
    $eventdata->fullmessagehtml = '';
  }

  $eventdata->fullmessageformat = $format;
  $eventdata->smallmessage = $message; //store the message unfiltered. Clean up on output.

  $s = new stdClass();
  $s->sitename = format_string($SITE->shortname, true, array('context' => get_context_instance(CONTEXT_COURSE, SITEID)));
  $s->url = $CFG->wwwroot . '/message/index.php?user=' . $userto->id . '&id=' . $userfrom->id;

  $emailtagline = get_string_manager()->get_string('emailtagline', 'message', $s, $userto->lang);
  if (!empty($eventdata->fullmessage)) {
    $eventdata->fullmessage .= "\n\n---------------------------------------------------------------------\n" . $emailtagline;
  }
  if (!empty($eventdata->fullmessagehtml)) {
    $eventdata->fullmessagehtml .= "<br /><br />---------------------------------------------------------------------<br />" . $emailtagline;
  }

  $eventdata->timecreated = time();
  return message_send($eventdata);
}

function displayJsAlert_second($ms, $redirectPath) {
  $html = '';
  if ($ms != '') {
    $html .= '<script language="javascript">alert("' . $ms . '")</script>';
  }

  if ($redirectPath != '') {
    $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
  }
  return $html;
}

function time_ago_second($datefrom, $dateto = -1) {
// Defaults and assume if 0 is passed in that
// its an error rather than the epoch

  if ($datefrom <= 0) {
    return "A long time ago";
  }
  if ($dateto == -1) {
    $dateto = time();
  }

// Calculate the difference in seconds betweeen
// the two timestamps

  $difference = $dateto - $datefrom;

// If difference is less than 60 seconds,
// seconds is a good interval of choice

  if ($difference < 60) {
    $interval = "s";
  }

// If difference is between 60 seconds and
// 60 minutes, minutes is a good interval
  elseif ($difference >= 60 && $difference < 60 * 60) {
    $interval = "n";
  }

// If difference is between 1 hour and 24 hours
// hours is a good interval
  elseif ($difference >= 60 * 60 && $difference < 60 * 60 * 24) {
    $interval = "h";
  }

// If difference is between 1 day and 7 days
// days is a good interval
  elseif ($difference >= 60 * 60 * 24 && $difference < 60 * 60 * 24 * 7) {
    $interval = "d";
  }

// If difference is between 1 week and 30 days
// weeks is a good interval
  elseif ($difference >= 60 * 60 * 24 * 7 && $difference < 60 * 60 * 24 * 30) {
    $interval = "ww";
  }

// If difference is between 30 days and 365 days
// months is a good interval, again, the same thing
// applies, if the 29th February happens to exist
// between your 2 dates, the function will return
// the 'incorrect' value for a day
  elseif ($difference > 60 * 60 * 24 * 30 && $difference < 60 * 60 * 24 * 365) {
    $interval = "m";
  }
  elseif ($difference > 60 * 60 * 24 * 365) {
    $interval = "y";
  }

// Based on the interval, determine the
// number of units between the two dates
// From this point on, you would be hard
// pushed telling the difference between
// this function and DateDiff. If the $datediff
// returned is 1, be sure to return the singular
// of the unit, e.g. 'day' rather 'days'

  switch ($interval) {
    case "m":
      $months_difference = floor($difference / 60 / 60 / 24 /
          29);
      while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
        $months_difference++;
      }
      $datediff = $months_difference;

// We need this in here because it is possible
// to have an 'm' interval and a months
// difference of 12 because we are using 29 days
// in a month

      if ($datediff == 12) {
        $datediff--;
      }

      $res = ($datediff == 1) ? "$datediff month ago" : "$datediff
months ago";
      break;

    case "y":
      $datediff = floor($difference / 60 / 60 / 24 / 365);
      $res = ($datediff == 1) ? "$datediff year ago" : "$datediff
years ago";
      break;

    case "d":
      $datediff = floor($difference / 60 / 60 / 24);
      $res = ($datediff == 1) ? "$datediff " . get_string('dayago') : "$datediff " . get_string('dayago');
      break;

    case "ww":
      $datediff = floor($difference / 60 / 60 / 24 / 7);
      $res = ($datediff == 1) ? "$datediff week ago" : "$datediff
weeks ago";
      break;

    case "h":
      $datediff = floor($difference / 60 / 60);
      $res = ($datediff == 1) ? "$datediff hour ago" : "$datediff
hours ago";
      break;

    case "n":
      $datediff = floor($difference / 60);
      $res = ($datediff == 1) ? "$datediff minute ago" :
          "$datediff minutes ago";
      break;

    case "s":
      $datediff = $difference;
      $res = ($datediff == 1) ? "$datediff second ago" :
          "$datediff seconds ago";
      break;
  }
  return $res;
}
?>