<?php
require_once('../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . "/common/header_lib.php");
require_login();

$PAGE->requires->js('/manage/manage.js');
$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/mobile/searchcourse.js');

$PAGE->set_title("Courses");
$PAGE->set_heading("Courses");
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();
$page = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage = optional_param('perpage', '10', PARAM_INT); // how many per page
//add code
global $USER;
$courses = get_courses_library();
?>



<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">


    <div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
        <div style="float:left;padding:10px;"> 
            <?php
            $root_user_id = get_root_parent_of_user($USER->id);
            global $DB;
            $root_user = $DB->get_record('user', array(
                'id' => $root_user_id
                    ));
            if ($root_user->company != '') {
                echo $root_user->company;
            }
            ?></div>
        <div style="float:right;padding:10px;">

            <img src="<?php
            if ($USER->profile_img == "") {
                echo "../account/logo/avata.jpg";
            } else {
                echo $USER->profile_img;
            }
            ?>" alt="AVATA" class="mini-pic" border="0"> 
            <span class="header-text text_ipad_header"><?php echo $USER->username; ?></span>&nbsp;
        </div>
    </div>



    <div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                    <td class="menu" valign="top">



                        <ul class="super-nav2">

<!--                            <li ><a href="index.php" link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>-->
                            <li><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>


                            <li class="active"><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
                            <li><a href="message.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
                            <li><a href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
                        </ul> 


                    </td>
                    <td class="content" valign="top">

                        <div class="course-item">

                            <div class="search-container">
                                <div class="ui-input-search ui-shadow-inset ui-btn-corner-all ui-btn-shadow ui-icon-searchfield ui-body-c">
                                    <input class="search-input ui-input-text ui-body-c" type="text" data-type="search" name="searchCourse" placeholder="Search for a Course" autocorrect="off" autocomplete="off" autocapitialize="off">
                                    <a class="ui-input-clear ui-btn ui-btn-up-c ui-shadow ui-btn-corner-all ui-fullsize ui-btn-icon-notext ui-input-clear-hidden" title="clear text" href="#" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="delete" data-iconpos="notext" data-theme="c" data-mini="false">
                                        <span class="ui-btn-inner ui-btn-corner-all">
                                            <span class="ui-btn-text">clear text</span>
                                            <span class="ui-icon ui-icon-delete ui-icon-shadow"> </span>
                                        </span>
                                    </a>
                                </div>
                            </div>


                            <div id="librarysearch" >
                                <ul class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-transition="none" data-dividertheme="a" data-theme="c" data-inset="true" data-role="listview">
                                  <li class="list-heading ui-li ui-li-divider ui-bar-a ui-corner-top" data-role="list-divider" role="heading"><?php print_r(get_string('courselibrary')) ?></li>
<?php
foreach ($courses as $key => $course) {
    $course_setting = get_course_settings($course->id);

    $completion = new completion_info($course);
    $coursecomplete = $completion->is_course_complete($USER->id);
    $score = count_course_completion($course, $USER->id);
    ?>
                                        <li class="arrow ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c" data-transition="none" data-id="21593" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="c">
                                            <div class="ui-btn-inner ui-li">
                                                <a class="ui-link-inherit" data-transition="none" href="<?php echo new moodle_url('view.php', array('id' => $course->id)); ?>"> 
                                                    <div class="ui-btn-text">

    <?php echo $course->fullname . '   '; ?>     

                                                        <div class="tip">
                                                        <?php echo $course->idnumber; ?>
                                                        </div>
                                                        <div>
                                                            <span class="box-mobile-right">
                                                                <span class="box-mobile box-blue"><?php echo $score . "%" . " complete"; ?></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"> </span>
                                                </a>
                                            </div>
                                        </li>
    <?php
}//}}
?>
                                </ul>
                            </div>
                        </div>
                    </td>


                    </td>
                </tr>
            </tbody></table>
    </div> 
</div> 

<?php
echo $OUTPUT->footer();
?>