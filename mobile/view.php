<?php
require_once('../config.php');
require("../grade/lib.php");
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/lib/completionlib.php');
require_once($CFG->dirroot . '/lib/completion/completion_completion.php');
require_once($CFG->dirroot . '/manage/news/lib.php'); // Require for right block news
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . "/common/header_lib.php");
require_login();
//$PAGE->set_title("Home");
//$PAGE->set_heading("Home");
$PAGE->set_pagelayout('mobile');
//echo $OUTPUT->header();
//add code

$id = optional_param('id', 0, PARAM_INT);
$name = optional_param('name', '', PARAM_RAW);
$edit = optional_param('edit', -1, PARAM_BOOL);
$hide = optional_param('hide', 0, PARAM_INT);
$show = optional_param('show', 0, PARAM_INT);
$idnumber = optional_param('idnumber', '', PARAM_RAW);
$section = optional_param('section', 0, PARAM_INT);
$move = optional_param('move', 0, PARAM_INT);
$marker = optional_param('marker', -1, PARAM_INT);
$switchrole = optional_param('switchrole', -1, PARAM_INT);
$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);

//fomat
$streditsummary = get_string('editsummary');
$stradd = get_string('add');
$stractivities = get_string('activities');
$strshowalltopics = get_string('showalltopics');
$strtopic = get_string('topic');
$strgroups = get_string('groups');
$strgroupmy = get_string('groupmy');
$editing = $PAGE->user_is_editing();

if (empty($id) && empty($name) && empty($idnumber)) {
    print_error('unspecifycourseid', 'error');
}

if (!empty($name)) {
    if (!($course = $DB->get_record('course', array('shortname' => $name)))) {
        print_error('invalidcoursenameshort', 'error');
    }
} else if (!empty($idnumber)) {
    if (!($course = $DB->get_record('course', array('idnumber' => $idnumber)))) {
        print_error('invalidcourseid', 'error');
    }
} else {
    if (!($course = $DB->get_record('course', array('id' => $id)))) {
        print_error('invalidcourseid', 'error');
    }
}


$PAGE->set_url('/course/view.php', array('id' => $course->id)); // Defined here to avoid notices on errors etc

preload_course_contexts($course->id);
if (!$context = get_context_instance(CONTEXT_COURSE, $course->id)) {
    print_error('nocontext');
}

// Remove any switched roles before checking login
if ($switchrole == 0 && confirm_sesskey()) {
    role_switch($switchrole, $context);
}

require_login($course);
$PAGE->requires->js('/course/course_view.js');
// Switchrole - sanity check in cost-order...
$reset_user_allowed_editing = false;
if ($switchrole > 0 && confirm_sesskey() &&
        has_capability('moodle/role:switchroles', $context)) {
    // is this role assignable in this context?
    // inquiring minds want to know...
    $aroles = get_switchable_roles($context);
    if (is_array($aroles) && isset($aroles[$switchrole])) {
        role_switch($switchrole, $context);
        // Double check that this role is allowed here
        require_login($course->id);
    }
    // reset course page state - this prevents some weird problems ;-)
    $USER->activitycopy = false;
    $USER->activitycopycourse = NULL;
    unset($USER->activitycopyname);
    unset($SESSION->modform);
    $USER->editing = 0;
    $reset_user_allowed_editing = true;
}

//If course is hosted on an external server, redirect to corresponding
//url with appropriate authentication attached as parameter
if (file_exists($CFG->dirroot . '/course/externservercourse.php')) {
    include $CFG->dirroot . '/course/externservercourse.php';
    if (function_exists('extern_server_course')) {
        if ($extern_url = extern_server_course($course)) {
            redirect($extern_url);
        }
    }
}


require_once($CFG->dirroot . '/calendar/lib.php');    /// This is after login because it needs $USER

add_to_log($course->id, 'course', 'view', "view.php?id=$course->id", "$course->id");

$course->format = clean_param($course->format, PARAM_ALPHA);
if (!file_exists($CFG->dirroot . '/course/format/' . $course->format . '/format.php')) {
    $course->format = 'weeks';  // Default format is weeks
}

//$PAGE->set_pagelayout('course');
$PAGE->set_pagetype('course-view-' . $course->format);
$PAGE->set_other_editing_capability('moodle/course:manageactivities');

if ($reset_user_allowed_editing) {
    // ugly hack
    unset($PAGE->_user_allowed_editing);
}

// LMS: Default Enable edit course
//    if (!isset($USER->editing)) {
$USER->editing = 1;
//    }
if ($PAGE->user_allowed_editing()) {
    if (($edit == 1) and confirm_sesskey()) {
        $USER->editing = 1;
        redirect($PAGE->url);
    } else if (($edit == 0) and confirm_sesskey()) {
        $USER->editing = 0;
        if (!empty($USER->activitycopy) && $USER->activitycopycourse == $course->id) {
            $USER->activitycopy = false;
            $USER->activitycopycourse = NULL;
        }
        redirect($PAGE->url);
    }

    if ($hide && confirm_sesskey()) {
        set_section_visible($course->id, $hide, '0');
    }

    if ($show && confirm_sesskey()) {
        set_section_visible($course->id, $show, '1');
    }

    if (!empty($section)) {
        if (!empty($move) and confirm_sesskey()) {
            if (!move_section($course, $section, $move)) {
                echo $OUTPUT->notification('An error occurred while moving a section');
            }
            // Clear the navigation cache at this point so that the affects
            // are seen immediately on the navigation.
            $PAGE->navigation->clear_cache();
        }
    }
} else {
    $USER->editing = 0;
}

$SESSION->fromdiscussion = $CFG->wwwroot . '/course/view.php?id=' . $course->id;


if ($course->id == SITEID) {
    // This course is not a real course.
    redirect($CFG->wwwroot . '/');
}

// AJAX-capable course format?
$useajax = false;
$formatajax = course_format_ajax_support($course->format);

if (!empty($CFG->enablecourseajax)
        and $formatajax->capable
        and !empty($USER->editing)
        and ajaxenabled($formatajax->testedbrowsers)
        and $PAGE->theme->enablecourseajax
        and has_capability('moodle/course:manageactivities', $context)) {
    $PAGE->requires->yui2_lib('dragdrop');
    $PAGE->requires->yui2_lib('connection');
    $PAGE->requires->yui2_lib('selector');
    $PAGE->requires->js('/lib/ajax/block_classes.js', true);
    $PAGE->requires->js('/lib/ajax/section_classes.js', true);

    // Okay, global variable alert. VERY UGLY. We need to create
    // this object here before the <blockname>_print_block()
    // function is called, since that function needs to set some
    // stuff in the javascriptportal object.
    $COURSE->javascriptportal = new jsportal();
    $useajax = true;
}

$CFG->blocksdrag = $useajax;   // this will add a new class to the header so we can style differently

$completion = new completion_info($course);
// Add link continue course
// Is course complete?
// Load criteria to display
$completions = $completion->get_completions($USER->id);


// Has this user completed any criteria?
$criteriacomplete = $completion->count_course_user_data($USER->id);

$modinfo = get_fast_modinfo($course);

$sections = $modinfo->sections[0];

$cmid_first = current($sections);
$modinfo = get_fast_modinfo($course);

/// extra fast view mode
$url_start_course = "";

if (!empty($modinfo->sections[0])) {

    foreach ($modinfo->sections[0] as $cmid) {
        if ($cmid_first == $cmid) {
            $cm = $modinfo->cms[$cmid];
            if (!$cm->uservisible) {
                continue;
            }

            if (($url = $cm->get_url())) {
                $url_start_course = $url;
            }
        }
    }
}




if ($completion->is_enabled() && ajaxenabled()) {
    $PAGE->requires->string_for_js('completion-title-manual-y', 'completion');
    $PAGE->requires->string_for_js('completion-title-manual-n', 'completion');
    $PAGE->requires->string_for_js('completion-alt-manual-y', 'completion');
    $PAGE->requires->string_for_js('completion-alt-manual-n', 'completion');

    $PAGE->requires->js_init_call('M.core_completion.init');
}

// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/course/view.php', array('id' => $course->id)));
    $PAGE->set_button($buttons);
}
$PAGE->requires->js('/blocks/course_addmodule/addmodules.js');
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
?>



<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">


    <div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
         <div style="float:left;padding:10px;"> 
            <?php 
            $root_user_id=get_root_parent_of_user($USER->id);
            global $DB;
            $root_user = $DB->get_record('user', array(
            'id' => $root_user_id
            ));
            if($root_user->company!=''){            											
                echo $root_user->company;
            }
            ?></div>
        <div style="float:right;padding:10px;">

            <img src="<?php 
            if($USER->profile_img==""){
                echo "../account/logo/avata.jpg";
            }
            else {
                echo $USER->profile_img ;
            }
            ?>" alt="AVATA" class="mini-pic" border="0"> 
            <span class="header-text text_ipad_header"><?php echo $USER->username; ?></span>&nbsp;
        </div>
    </div>



    <div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                    <td class="menu" valign="top">



                        <ul class="super-nav2">

<!--                            <li><a href="index.php" link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>-->
                            <li><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>


                            <li class="active"><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
                            <li><a href="message.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
                            <li><a href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
                        </ul> 


                    </td>



                    <td class="content" valign="top">

                        <div class="tab-container">
                            <div class="tabs">
                                <ul>
                                    <li class="selected">
                                        <a class="ui-link" onclick="LM.courseTabSwitch(0)">To Do</a>
                                    </li>
                                </ul>
                            </div>
                            <?php
                            if ($completion->is_enabled() && ajaxenabled()) {
                                // This value tracks whether there has been a dynamic change to the page.
                                // It is used so that if a user does this - (a) set some tickmarks, (b)
                                // go to another page, (c) clicks Back button - the page will
                                // automatically reload. Otherwise it would start with the wrong tick
                                // values.
                                echo html_writer::start_tag('form', array('action' => '.', 'method' => 'get'));
                                echo html_writer::start_tag('div');
                                echo html_writer::empty_tag('input', array('type' => 'hidden', 'id' => 'completion_dynamic_change', 'name' => 'completion_dynamic_change', 'value' => '0'));
                                echo html_writer::end_tag('div');
                                echo html_writer::end_tag('form');
                            }

                            $modinfo = & get_fast_modinfo($COURSE);
                            get_all_mods($course->id, $mods, $modnames, $modnamesplural, $modnamesused);
                            foreach ($mods as $modid => $unused) {
                                if (!isset($modinfo->cms[$modid])) {
                                    rebuild_course_cache($course->id);
                                    $modinfo = & get_fast_modinfo($COURSE);
                                    debugging('Rebuilding course cache', DEBUG_DEVELOPER);
                                    break;
                                }
                            }

                            if (!$sections = get_all_sections($course->id)) {   // No sections found
                                // Double-check to be extra sure
                                if (!$section = $DB->get_record('course_sections', array('course' => $course->id, 'section' => 0))) {
                                    $section->course = $course->id;   // Create a default section.
                                    $section->section = 0;
                                    $section->visible = 1;
                                    $section->summaryformat = FORMAT_HTML;
                                    $section->id = $DB->insert_record('course_sections', $section);
                                }
                                if (!$sections = get_all_sections($course->id)) {      // Try again
                                    print_error('cannotcreateorfindstructs', 'error');
                                }
                            }
                            // code fomat topics


                            $topic = optional_param('topic', -1, PARAM_INT);

                            if ($topic != -1) {
                                $displaysection = course_set_display($course->id, $topic);
                            } else {
                                $displaysection = course_get_display($course->id);
                            }

                            $context = get_context_instance(CONTEXT_COURSE, $course->id);

                            if (($marker >= 0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
                                $course->marker = $marker;
                                $DB->set_field("course", "marker", $marker, array("id" => $course->id));
                            }

                            $section = 0;
//var_dump($sections);
                            $thissection = $sections[$section];
                            //var_dump($thissection);

                            unset($sections[0]);

                            if ($thissection->summary or $thissection->sequence or $PAGE->user_is_editing()) {

                                $current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
                                $can_reorder = 0;
                                if ((is_teacher() || is_admin() || is_siteadmin()) && $current_switchrole != 5) {
                                    $can_reorder = 1;
                                } else {
                                    $can_reorder = 0;
                                }

                                $id_sort = "sort";
                                if ($can_reorder == 0) {
                                    $id_sort = "disable_sort";
                                }
                                $sectionmods = explode(",", $thissection->sequence);
                                //var_dump($sectionmods);
                                ?>



                                <div style="clear:both;"></div>
                            </div>
                            <div class="course-item">
                                <div style="padding-left:10px;">
                                    <h3 style="margin-bottom:5px;"><?php echo $course->fullname; ?></h3>
                                    <div class="tip" style="font-size:13px;padding-left:10px;">I need to complete the following modules to finish this training course.</div>
                                </div>
                                <?php
                                if ($can_reorder == 0) {

                                    foreach ($sectionmods as $modnumber) {
                                        if (empty($mods[$modnumber])) {
                                            continue;
                                        }
                                        $mod = $mods[$modnumber];
                                        //var_dump($mod);

                                        if ($ismoving and $mod->id == $USER->activitycopy) {
                                            // do not display moving mod
                                            continue;
                                        }

                                        if (isset($modinfo->cms[$modnumber])) {
                                            // We can continue (because it will not be displayed at all)
                                            // if:
                                            // 1) The activity is not visible to users
                                            // and
                                            // 2a) The 'showavailability' option is not set (if that is set,
                                            //     we need to display the activity so we can show
                                            //     availability info)
                                            // or
                                            // 2b) The 'availableinfo' is empty, i.e. the activity was
                                            //     hidden in a way that leaves no info, such as using the
                                            //     eye icon.
                                            if (!$modinfo->cms[$modnumber]->uservisible &&
                                                    (empty($modinfo->cms[$modnumber]->showavailability) ||
                                                    empty($modinfo->cms[$modnumber]->availableinfo))) {
                                                // visibility shortcut
                                                continue;
                                            }
                                        } else {
                                            if (!file_exists("$CFG->dirroot/mod/$mod->modname/lib.php")) {
                                                // module not installed
                                                continue;
                                            }
                                            if (!coursemodule_visible_for_user($mod) &&
                                                    empty($mod->showavailability)) {
                                                // full visibility check
                                                continue;
                                            }
                                        }
                                        if (!isset($modulenames[$mod->modname])) {
                                            $modulenames[$mod->modname] = get_string('modulename', $mod->modname);
                                        }

                                        $modulename = $modulenames[$mod->modname];

                                        // In some cases the activity is visible to user, but it is
                                        // dimmed. This is done if viewhiddenactivities is true and if:
                                        // 1. the activity is not visible, or
                                        // 2. the activity has dates set which do not include current, or
                                        // 3. the activity has any other conditions set (regardless of whether
                                        //    current user meets them)
                                        $canviewhidden = has_capability(
                                                'moodle/course:viewhiddenactivities', get_context_instance(CONTEXT_MODULE, $mod->id));

                                        $accessiblebutdim = false;
                                        if ($canviewhidden) {
                                            $accessiblebutdim = !$mod->visible;
                                            if (!empty($CFG->enableavailability)) {
                                                $accessiblebutdim = $accessiblebutdim ||
                                                        $mod->availablefrom > time() ||
                                                        ($mod->availableuntil && $mod->availableuntil < time()) ||
                                                        count($mod->conditionsgrade) > 0 ||
                                                        count($mod->conditionscompletion) > 0;
                                            }
                                        }
                                        $liclasses = array();
                                        $liclasses[] = 'activity';
                                        $liclasses[] = $mod->modname;
                                        $liclasses[] = 'modtype_' . $mod->modname;
                                        $extraclasses = $mod->get_extra_classes();
                                        if ($extraclasses) {
                                            $liclasses = array_merge($liclasses, explode(' ', $extraclasses));
                                        }

                                        $classes = array('mod-indent');
                                        if (!empty($mod->indent)) {
                                            $classes[] = 'mod-indent-' . $mod->indent;
                                            if ($mod->indent > 15) {
                                                $classes[] = 'mod-indent-huge';
                                            }
                                        }

                                        // Get data about this course-module
                                        list($content, $instancename) =
                                                get_print_section_cm_text($modinfo->cms[$modnumber], $course);

                                        //Accessibility: for files get description via icon, this is very ugly hack!
                                        $altname = '';
                                        $altname = $mod->modfullname;
                                        if (!empty($customicon)) {
                                            $archetype = plugin_supports('mod', $mod->modname, FEATURE_MOD_ARCHETYPE, MOD_ARCHETYPE_OTHER);
                                            if ($archetype == MOD_ARCHETYPE_RESOURCE) {
                                                $mimetype = mimeinfo_from_icon('type', $customicon);
                                                $altname = get_mimetype_description($mimetype);
                                            }
                                        }

                                        // Avoid unnecessary duplication: if e.g. a forum name already
                                        // includes the word forum (or Forum, etc) then it is unhelpful
                                        // to include that in the accessible description that is added.
                                        $tl = textlib_get_instance();
                                        if (false !== strpos($tl->strtolower($instancename), $tl->strtolower($altname))) {
                                            $altname = '';
                                        }
                                        // File type after name, for alphabetic lists (screen reader).
                                        if ($altname) {
                                            $altname = get_accesshide(' ' . $altname);
                                        }

//                                        // We may be displaying this just in order to show information
//                                        // about visibility, without the actual link
//                                        $contentpart = '';
//                                        if ($mod->uservisible) {
//                                            // Nope - in this case the link is fully working for user
//                                            $linkclasses = '';
//                                            $textclasses = '';
//                                            if ($accessiblebutdim) {
//                                                $linkclasses .= ' dimmed';
//                                                $textclasses .= ' dimmed_text';
//                                                $accesstext = '<span class="accesshide">' .
//                                                        get_string('hiddenfromstudents') . ': </span>';
//                                            } else {
//                                                $accesstext = '';
//                                            }
//                                            if ($linkclasses) {
//                                                $linkcss = 'class="' . trim($linkclasses) . '" ';
//                                            } else {
//                                                $linkcss = '';
//                                            }
//                                            if ($textclasses) {
//                                                $textcss = 'class="' . trim($textclasses) . '" ';
//                                            } else {
//                                                $textcss = '';
//                                            }

                                            // Get on-click attribute value if specified
                                            $onclick = $mod->get_on_click();
                                            if ($onclick) {
                                                $onclick = ' onclick="' . $onclick . '"';
                                            }
                                            if ($url = $mod->get_url()) {                                                
                                            ?>
                                            <div data-role="refresh-list">
                                                <ul class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-dividertheme="b" data-theme="c" data-inset="true" data-role="listview">
                                                    <li class="arrow ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-corner-top ui-btn-up-c" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="c">
                                                        <div class="ui-btn-inner ui-li ui-corner-top">
                                                            <a class="ui-link-inherit" data-transition="none" onclick="LM.checkSupport(true,false,'/course/21616/module/96999/open?moduletype=3&assigned=True&httpsOnly=True',false,this)" href="<?php echo $url . '&current_switchrole=5'; ?>">
                                                            <div class="ui-btn-text">
                                                                 <?php echo $instancename.$altname;?> 
<!--                                                                <span class="box-mobile-right">
                                                                    <span class="box-mobile box-green"> Complete</span>
                                                                </span>-->
                                                            </div>
                                                            <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"> </span>
                                                            </a>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="course-item" style="display:none;">
                                            <ul class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-inset="true" data-role="listview"> </ul>
                                        </div>
                                        <div class="course-item" style="display:none;"> </div>

                                        <?php
                                    }}
                                }
                            }
                            ?>
                        </td>




                    </tr>

                </tbody></table>
        </div> 
    </div> 

    <?php
    echo $OUTPUT->footer();
    ?>