<?php
require_once('../config.php');
global $CFG;
require_once($CFG->dirroot . "/common/header_lib.php");
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/message/send_form.php');
require_login();

$PAGE->set_title("Message");
$PAGE->set_heading("Message");
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();


//add code

$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
$id = optional_param('id', -1, PARAM_INT); //course id
if ($current_switchrole != -1 && $id != -1) {
    $context = get_context_instance(CONTEXT_COURSE, $id);
    role_switch($current_switchrole, $context);
    require_login($id);
} else {
    require_login(0, false);
}
?>



<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">


    <div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
        <div style="float:left;padding:10px;">
            <?php 
            $root_user_id=get_root_parent_of_user($USER->id);
            global $DB;
            $root_user = $DB->get_record('user', array(
            'id' => $root_user_id
            ));
            if($root_user->company!=''){            											
                echo $root_user->company;
            }
            ?></div>
        <div style="float:right;padding:10px;">

            <img src="<?php 
            if($USER->profile_img==""){
                echo "../account/logo/avata.jpg";
            }
            else {
                echo $USER->profile_img ;
            }
            ?>" alt="AVATA" class="mini-pic" border="0"> 
            <span class="header-text text_ipad_header"><?php echo $USER->username; ?></span>&nbsp;
        </div>
    </div>



    <div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                    <td class="menu" valign="top">



                        <ul class="super-nav2">

<!--                            <li ><a href="index.php" link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>-->
                            <li><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>


                            <li><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
                            <li class="active"><a href="message.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
                            <li><a href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
                        </ul> 


                    </td>
                    <td class="content" valign="top">

                        <ul class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-dividertheme="a" data-inset="true" data-role="listview">
                          <li class="list-heading ui-li ui-li-divider ui-bar-a ui-corner-top" data-role="list-divider" role="heading"><?php print_r(get_string('inbox')) ?></li>
                           

                                <?php
                            $count_read = 0;
                            global $DB;
                            $params = array('userid' => $USER->id);
                            $sql = "SELECT * FROM message WHERE useridto =" . $USER->id;

                            //echo $sql;
                            $mss = $DB->get_records_sql($sql);
                            //var_dump($mss);
                            //cho $sql;
                            //$mss = get_read_messages($USER->id);

                            foreach ($mss as $key) {
                                //var_dump($key);
                                //$from_name_obj = getUserFromId($key->useridfrom);
                                global $DB;
                                $t = $key->useridfrom;
                                
                                $sql2 = "SELECT * FROM user WHERE id=".$t;
                                //echo $sql2;
                                $from_name_obj = $DB->get_records_sql($sql2);
                                //var_dump($from_name_obj);
                                foreach ($from_name_obj as $from_name_name){
                                $from_name = $from_name_name->firstname . " " . $from_name_name->lastname;
                                
                                ?>
                                <li class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="c">
                                    <div class="ui-btn-inner ui-li">
                                        <a class="ui-link-inherit" href="<?php echo $CFG->wwwroot ?>/mobile/message_progress.php?action=read_message&user_from=<?php echo $key->useridfrom ?>">
                                        <div class="ui-btn-text">

                                            
                                                <h3 class="ui-li-heading"><?php echo $from_name; ?> </h3>
                                                <span class="box-tag box-tag-lightblue " title="">
                                                                    <span>unread</span>
                                                                </span>

                                                <p class="tip ui-li-desc"><?php echo $key->subject; ?></p>
                                            

                                            <span class="box-mobile-right">
                                                <span class="box-mobile box-blue"><?php echo time_ago_second($key->timecreated); ?></span>
                                            </span>

                                        </div>
                                        <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"> </span>
                                        </a>
                                    </div>
                                </li>
                            <?php }} ?>










                            <?php
                            $count_read = 0;
                            global $DB;
                            $params = array('userid' => $USER->id);
                            $sql = "SELECT * FROM message_read WHERE useridto =" . $USER->id;

                            //echo $sql;
                            $mss = $DB->get_records_sql($sql);
                            //var_dump($mss);
                            //cho $sql;
                            //$mss = get_read_messages($USER->id);

                            foreach ($mss as $key) {
                                //var_dump($key);
                                //$from_name_obj = getUserFromId($key->useridfrom);
                                global $DB;
                                $t = $key->useridfrom;
                                
                                $sql2 = "SELECT * FROM user WHERE id=".$t;
                                //echo $sql2;
                                $from_name_obj = $DB->get_records_sql($sql2);
                                //var_dump($from_name_obj);
                                foreach ($from_name_obj as $from_name_name){
                                $from_name = $from_name_name->firstname . " " . $from_name_name->lastname;
                                
                                ?>
                                <li class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="c">
                                    <div class="ui-btn-inner ui-li">
                                        <a class="ui-link-inherit" href="<?php echo $CFG->wwwroot ?>/mobile/message_progress.php?action=read_message&user_from=<?php echo $key->useridfrom ?>">
                                        <div class="ui-btn-text">

                                            
                                                <h3 class="ui-li-heading"><?php echo $from_name; // echo $USER->firstname." ".$USER->lastname;  ?> </h3>


                                                <p class="tip ui-li-desc"><?php echo $key->subject; ?></p>
                                            

                                            <span class="box-mobile-right">
                                                <span class="box-mobile box-blue"><?php echo time_ago_second($key->timecreated); ?></span>
                                            </span>

                                        </div>
                                        <span class="ui-icon ui-icon-arrow-r ui-icon-shadow"> </span>
                                        </a>
                                    </div>
                                </li>
                            <?php }} ?>
                        </ul> 

                    </td>


                    </td>
                </tr>
            </tbody></table>
    </div> 
</div> 

<?php
echo $OUTPUT->footer();
function time_ago_second($datefrom, $dateto = -1) {
// Defaults and assume if 0 is passed in that
// its an error rather than the epoch

    if ($datefrom <= 0) {
        return "A long time ago";
    }
    if ($dateto == -1) {
        $dateto = time();
    }

// Calculate the difference in seconds betweeen
// the two timestamps

    $difference = $dateto - $datefrom;

// If difference is less than 60 seconds,
// seconds is a good interval of choice

    if ($difference < 60) {
        $interval = "s";
    }

// If difference is between 60 seconds and
// 60 minutes, minutes is a good interval
    elseif ($difference >= 60 && $difference < 60 * 60) {
        $interval = "n";
    }

// If difference is between 1 hour and 24 hours
// hours is a good interval
    elseif ($difference >= 60 * 60 && $difference < 60 * 60 * 24) {
        $interval = "h";
    }

// If difference is between 1 day and 7 days
// days is a good interval
    elseif ($difference >= 60 * 60 * 24 && $difference < 60 * 60 * 24 * 7) {
        $interval = "d";
    }

// If difference is between 1 week and 30 days
// weeks is a good interval
    elseif ($difference >= 60 * 60 * 24 * 7 && $difference < 60 * 60 * 24 * 30) {
        $interval = "ww";
    }

// If difference is between 30 days and 365 days
// months is a good interval, again, the same thing
// applies, if the 29th February happens to exist
// between your 2 dates, the function will return
// the 'incorrect' value for a day
    elseif ($difference > 60 * 60 * 24 * 30 && $difference < 60 * 60 * 24 * 365) {
        $interval = "m";
    } elseif ($difference > 60 * 60 * 24 * 365) {
        $interval = "y";
    }

// Based on the interval, determine the
// number of units between the two dates
// From this point on, you would be hard
// pushed telling the difference between
// this function and DateDiff. If the $datediff
// returned is 1, be sure to return the singular
// of the unit, e.g. 'day' rather 'days'

    switch ($interval) {
        case "m":
            $months_difference = floor($difference / 60 / 60 / 24 /
                    29);
            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
                $months_difference++;
            }
            $datediff = $months_difference;

// We need this in here because it is possible
// to have an 'm' interval and a months
// difference of 12 because we are using 29 days
// in a month

            if ($datediff == 12) {
                $datediff--;
            }

            $res = ($datediff == 1) ? "$datediff".get_string('monthago') : "$datediff".get_string('monthago2');
            break;

        case "y":
            $datediff = floor($difference / 60 / 60 / 24 / 365);
            $res = ($datediff == 1) ? "$datediff year ago" : "$datediff
years ago";
            break;

        case "d":
            $datediff = floor($difference / 60 / 60 / 24);
            $res = ($datediff == 1) ? "$datediff ".  get_string('dayago') : "$datediff ".  get_string('dayago');
            break;

        case "ww":
            $datediff = floor($difference / 60 / 60 / 24 / 7);
            $res = ($datediff == 1) ? "$datediff week ago" : "$datediff
weeks ago";
            break;

        case "h":
            $datediff = floor($difference / 60 / 60);
            $res = ($datediff == 1) ? "$datediff hour ago" : "$datediff
hours ago";
            break;

        case "n":
            $datediff = floor($difference / 60);
            $res = ($datediff == 1) ? "$datediff minute ago" :
                    "$datediff minutes ago";
            break;

        case "s":
            $datediff = $difference;
            $res = ($datediff == 1) ? "$datediff second ago" :
                    "$datediff seconds ago";
            break;
    }
    return $res;
}
?>