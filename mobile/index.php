<?php 
require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_login();

$PAGE->set_title("Dashboard");
$PAGE->set_heading("Dashboard");
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '10', PARAM_INT); // how many per page

?>

    
    
<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">
    
 
<div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
    <div style="float:left;padding:10px;"><?php echo $USER->company ?></div>
	<div style="float:right;padding:10px;">
        
        <img src="/admin/people/142807/avatar" alt="AVATA" class="mini-pic" border="0"> 
        <span class="header-text"><?php echo $USER->email ?></span>&nbsp;
    </div>
</div>
 
         
   
	<div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                <td class="menu" valign="top">
                    

      
    <ul class="super-nav2">
        
            <li class="active"><a link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>
            <li><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>
        
        
            <li><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
            <li><a href="messages.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
            <li><a  href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
    </ul> 
         
   
                </td>
                <td class="content" valign="top">
                    <div id="chart" class="banner-chart">
                       
                    </div>
                     <div class="grid-tabs">
                    <ul>
                        <li><a href="<?php print new moodle_url('/manage'); ?>" class="selected"><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                        

                    </ul>
                    <div class="clearfix"></div>


                </div>
                    <div class="item-page-list">
                    <div id ='dashboard'>
                        <?php
                            echo manage_print_log($order="l.time ASC", $page, $perpage,
                                "dashboard.php?", $modname="", $modid=0, $modaction="", $groupid=0);
                            
                        ?>
                    </div>

                </div>
                </td>
            </tr>
        </tbody></table>
    </div> 
</div> 
   
<?php 
echo $OUTPUT->footer();
?>