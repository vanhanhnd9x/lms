<?php
require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . "/common/header_lib.php");
require_login();
$PAGE->set_title("Setting");
$PAGE->set_heading("Setting");
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();
$page = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage = optional_param('perpage', '10', PARAM_INT); // how many per page
?>



<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">


    <div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
        <div style="float:left;padding:10px;">
            <?php 
            $root_user_id=get_root_parent_of_user($USER->id);
            global $DB;
            $root_user = $DB->get_record('user', array(
            'id' => $root_user_id
            ));
            if($root_user->company!=''){            											
                echo $root_user->company;
            }
            ?></div>
        <div style="float:right;padding:10px;">

            <img src="<?php 
            if($USER->profile_img==""){
                echo "../account/logo/avata.jpg";
            }
            else {
                echo $USER->profile_img ;
            }
            ?>" alt="AVATA" class="mini-pic" border="0"> 
            <span class="header-text text_ipad_header"><?php echo $USER->username ?></span>&nbsp;
        </div>
    </div>



    <div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                    <td class="menu" valign="top">



                        <ul class="super-nav2">

<!--                            <li><a href="index.php" link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>-->
                            <li><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>


                            <li><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
                            <li><a href="message.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
                            <li class="active"><a href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
                        </ul> 


                    </td>
                    <td class="content" valign="top">


                        <div class="browserInfo" style="display:none"></div>
                        <div class="ui-field-contain ui-body ui-br" data-role="fieldcontain">
                            <fieldset class="ui-corner-all ui-controlgroup ui-controlgroup-vertical" data-role="controlgroup">
                                <a class="ui-btn ui-btn-inline ui-corner-top ui-corner-bottom ui-controlgroup-last ui-btn-up-c" href="<?php echo $CFG->httpswwwroot.'/login/logout.php?sesskey='.$USER->sesskey; ?>" data-inline="true" data-ajax="false" data-role="button" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="c">
                                    <span class="ui-btn-inner ui-corner-top ui-corner-bottom ui-controlgroup-last">
                                        <span class="ui-btn-text">sign out</span>
                                    </span>
                                </a>
                            </fieldset>
                        </div>



                    </td>
                </tr>
            </tbody></table>
    </div> 
</div> 

<?php
echo $OUTPUT->footer();
?>