<?php
require_once('../config.php');
require("../grade/lib.php");
require_once($CFG->dirroot . "/common/header_lib.php");
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/lib/completionlib.php');
require_once($CFG->dirroot . '/lib/completion/completion_completion.php');
require_once($CFG->dirroot . '/manage/news/lib.php'); // Require for right block news
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_login();
$PAGE->requires->js('/manage/js/jquery.min.js');
$PAGE->requires->js('/manage/recent_activity.js');
$PAGE->requires->js('/manage/js/jquery.jqplot.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.highlighter.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.cursor.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.dateAxisRenderer.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.enhancedLegendRenderer.js');
$PAGE->requires->css('/manage/css/jquery.jqplot.min.css');

if ($unique_login_values != "") {
    $PAGE->requires->js('/manage/js/js.php?unique_login_values="' . $unique_login_values . '"');
}
$PAGE->set_title("Home");
$PAGE->set_heading("Home");
$PAGE->set_pagelayout('mobile');
echo $OUTPUT->header();
$page = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage = optional_param('perpage', '10', PARAM_INT); // how many per page
//add code
$courses = enrol_get_my_courses();
?>



<div class="ui-page ui-body-c ui-page-active" data-url="" data-role="page" data-type="dashboard" o-url="/admin/dashboard">


    <div role="banner" data-role="header" data-backbtn="false" class="mobile-header ui-bar-a ui-header">
        <div  style="float:left;padding:10px;">
            <span>
            <?php 
            $root_user_id=get_root_parent_of_user($USER->id);
            global $DB;
            $root_user = $DB->get_record('user', array(
            'id' => $root_user_id
            ));
            if($root_user->company!=''){            											
                echo $root_user->company;
            }
            ?></span></div>
        <div style="float:right;padding:10px;">

            <img src="<?php 
            if($USER->profile_img==""){
                echo "../account/logo/avata.jpg";
            }
            else {
                echo $USER->profile_img ;
            }
            ?>" alt="AVATA" class="mini-pic" border="0"> 
            <span class="header-text text_ipad_header"><?php echo $USER->username; ?></span>&nbsp;
        </div>
    </div>



    <div role="main" class="ui-content" data-role="content"> 
        <table class="table-content">
            <tbody><tr>
                    <td class="menu" valign="top">



                        <ul class="super-nav2">

<!--                            <li><a href="index.php" link="javascript:void(0);" data-transition="none" onclick="LM.rootHome();" class="toolbar-a ui-link"><span class="toolbar-icon dashboardicon"></span><span class="toolbar-text">Dashboard</span></a></li>-->
                            <li class="active"><a href="home.php" link="/home" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon homeicon"></span><span class="toolbar-text">Home</span></a></li>


                            <li><a  href="course.php" link="/courses/library" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon courseicon"></span><span class="toolbar-text">Courses</span></a></li>
                            <li><a href="message.php" link="/messages/inbox" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon inboxicon"></span><span class="toolbar-text">Messages</span></a></li>
                            <li><a href="setting.php" link="/account" data-transition="none" class="toolbar-a ui-link"><span class="toolbar-icon settingsicon"></span><span class="toolbar-text">Settings</span></a></li>
                        </ul> 


                    </td>



                    <td class="content" valign="top">
                        <div style="padding-left: 10px;">
                            <h3 style="margin-bottom: 5px;"> Hello <?php echo $USER->firstname ?>,</h3>
                            <div class="tip" style="font-size: 13px; padding-left: 10px;">
                                This is the trainee view that your learners see when they log in. They can customize their account by adding a profile pic and more contact information.
                                <br>
                                <br>
                                To add your organization’s logo, change the color scheme or customize the welcome text on this page, switch back to the Admin View and select the ‘Account’ tab
                                <br>
                            </div>
                        </div>
                        <div data-role="refresh-list">
                            <ul class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-transition="none" data-dividertheme="a" data-theme="c" data-inset="true" data-role="listview">
                                <li class="list-heading ui-li ui-li-divider ui-bar-a ui-corner-top" data-role="list-divider" role="heading">Courses To Do</li>


<?php
foreach ($courses as $course) {
    $course_setting = get_course_settings($course->id);
    if ($course_setting->active) {
        $completion = new completion_info($course);
        $coursecomplete = $completion->is_course_complete($USER->id);
        $score = count_course_completion($course, $USER->id);
        if ($score < 100) {
            ?>
                                            <li class="arrow ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c" data-transition="none" data-id="21405" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="c">
                                                <div class="ui-btn-inner ui-li">
                                                    <a class="ui-link-inherit" data-transition="none" href="view.php?id=<?php echo $course->id; ?>">
                                                        <div class="ui-btn-text">


                                                            <?php echo $course->fullname . '   ';
                                                            ?> 
            <!--                                                <p class="line-tip ui-li-desc">Assigned On: Aug 18, 2012 </p>-->

                                                            <span class="box-mobile-right">
                                                                <span class="box-mobile box-blue"><?php echo $score . "%" . " complete"; ?></span>
                                                            </span>
                                                        </div>
                                                        <span class="ui-icon ui-icon-arrow-r ui-icon-shadow">&nbsp;</span>
                                                    </a>
                                                </div>
                                            </li>
                                        <?php }
                                    }
                                } ?> 
                            </ul>
                        </div>
                    </td>




                </tr>

            </tbody></table>
    </div> 
</div> 

<?php
echo $OUTPUT->footer();
?>