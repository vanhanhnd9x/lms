<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/course/team/lib.php');
require_login(0, false);

$PAGE->set_title(get_string('teams'));
$PAGE->set_heading(get_string('teams'));

$team_id = optional_param('team_id', 0, PARAM_TEXT);

echo $OUTPUT->header();
global $CFG;
// $team = get_team_from_id($team_id);
$team = get_list_groups_class($team_id);
$courses_of_team = get_courses_from_team($team_id);
$course_of_team_arr = array();
$courses = enrol_get_my_courses_second();
?>

<script type="text/javascript" src="js/search.js"></script>

<div id="page-body">        
    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        <div id="usersPanel" class="focus-panel">
                            <div class="panel-head clearfix">
                                <div></div>   
                                <h3><?php echo $team->name ?></h3>
                                <div class="subtitle"><?php echo $team->description ?></div>
                            </div>
                            <div class="body">
                                <div class="grid-tabs">
                                    <ul>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $team_id ?>" class=""><?php print_r(get_string('people')) ?></a></li>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/teams/team_course.php?team_id=<?php echo $team_id ?>" class="selected"><?php print_r(get_string('courses')) ?></a></li> 
                                        <li><a href="<?php echo $CFG->wwwroot ?>/teams/team_settings.php?team_id=<?php echo $team_id ?>" class=""><?php print_string('settings') ?></a></li>  
                                    </ul>
                                    <div class="clear">
                                    </div>
                                </div>  
                                <table class="item-page-list">
                                    <tbody>
                                        <?php
                                        foreach ($courses_of_team as $key => $val) {
                                            $course = get_course_from_id($key);
                                            $course_of_team_arr[] = $key;
                                            ?>
                                            <tr id="c15539">
                                                <td class="main-col">                       
                                                    <div class="title">
                                                        <a href="<?php echo $CFG->wwwroot ?>/course/view.php?id=<?php echo $course->id ?>"><?php echo $course->fullname ?></a>
                                                    </div>
                                                    <div class="tip"></div>
                                                </td>
                                                <td>                           
                                                    <a class="remove-msg btn-remove-ms float-right" title='Delete' onclick='javascript:displayConfirmBox("<?php echo $CFG->wwwroot ?>/teams/team.php?action=delete_course_team&team_id=<?php echo $team_id ?>&course_id=<?php echo $course->id ?>&confirm=yes",<?php echo $course->id ?>, "Bạn chắc chắn muốn xóa khóa học này?")' href='#'></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody></table>
                                <div class="list-pager">
                                    <ul>
                                    </ul>
                                    <div class="clear"></div>    
                                </div>
                                <div class="tip center-padded"><?php echo count($courses_of_team) ?> <?php print_r(get_string('coursefound')) ?></div>
                            </div>
                        </div>       
                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        <div class="side-panel team-actions">
                            <div class="action-buttons">
                                <ul>
                                    <li>
                                        <a alt="<?php print_string('assgincourseto') ?>" onclick="" href="#" class="big-button drop" id="assignCourses"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print_string('assgincourseto') ?></span></span></span></span></a>
                                    </li>
                                    <?php
                                    if (is_parent_team($sub_teams)) {
                                        ?>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/teams/add_sub_team.php?team_id=<?php echo $team_id; ?>">+ <?php print_r(get_string('addanewteamunder')) ?></a> </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>          

                    </div>
                </td>
            </tr>
        </tbody></table>
    <div class="overlay" id="overlay" style="display:none;"></div>
    <div id="facebox" style="display: none;">      
        <div class="popup">  
            <div class="content">
                <div id="assignItemBox">
                    <div class="assign-header">
                        <div class="assign-search">
                            <form action="" method="get">
                                <div class="field-help-box">
                                    <label id="searchPrompt" for="assignSearch"><?php print_r(get_string('searchforcourse')) ?></label><input type="text" autocapitialize="off" autocorrect="off" autocomplete="off" name="s" id="assignSearchCourse">
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="<?php echo $CFG->wwwroot ?>/teams/team.php?action=assign_course_to_team&team_id=<?php echo $team_id ?>" method="post">
                        <div id="assignResults">
                            <table cellspacing="0" class="item-page-list">
                                <tbody>
                                    <tr class="header-row">
                                        <td><input type="checkbox" value="true" name="AssignAll" id="AssignAll"></td>
                                        <td colspan="2"><label class="italic" for="AssignAll"><?php print_string('assigntoalluser') ?></label></td>
                                    </tr>
                                    <?php
                                    foreach ($courses as $key => $val) {
                                        ?>
                                        <tr class="c1">
                                            <td>
                                                <?php if (in_array($val->id, $course_of_team_arr)) { ?>
                                                    &nbsp;
                                                <?php } else { ?>
                                                    <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                                                <?php } ?>
                                            </td>
                                            <td class="main-col">
                                                <div class="title">
                                                    <label for="u15539"><?php echo $val->fullname ?></label></div>                                    
                                            </td>
                                            <td class="nowrap">&nbsp;
                                                <?php if (in_array($val->id, $course_of_team_arr)) { ?>
                                                    <span title="" class="box-tag box-tag-green "><span>
                                                            <?php print_r(get_string('alreadyassigned')) ?>
                                                        </span></span>                       
                                                <?php } else { ?>
                                                    <span title="" class=""><span>

                                                        </span></span>                       
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table></div>
                        <div class="assign-footer-opt clearfix hidden">
                            <span id="assignTeams"><input type="checkbox" name="SubTeams" id="SubTeams" value="true"> <label for="SubTeams"><?php print_string('alsoassignto') ?>&nbsp;&nbsp;</label></span>
                            <input type="hidden" name="assign_course_to_subteam" id="assign_course_to_subteam" value=""/>
                            <span id="assignEmails"><input type="checkbox" name="SendEmail" id="SendEmail" value="true"> <label for="SendEmail"><?php print_r(get_string('sendemailnoti')) ?></label></span>
                        </div>
                        <div class="assign-footer clearfix">
                            <div class="float-left">
                                <input type="submit" class="big-button drop" value="<?php print_r(get_string('assign', 'cohort')) ?>" />
                                <?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                        </div>
                    </form>
                </div>
            </div>       
        </div>     
    </div>
</div>
