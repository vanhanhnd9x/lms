<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php');

require_login(0, false);

$PAGE->set_title("Teams");
$PAGE->set_heading("Teams");

//now the page contents
// $PAGE->set_pagelayout('teams');
$team_id = optional_param('team_id', 0, PARAM_TEXT);
$class = get_info_team($team_id);
echo $OUTPUT->header();

global $CFG,$DB;
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='groups';
$check_xoacon=check_chuc_nang_xoachucnangcong($roleid,$moodle);
$name1='import_excel';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}


function update_code_student($number){
 global $DB;
 $record = new stdClass();
 $record->id = 1;
 $record->number = $number;
 $record->time_update =time();
 return $DB->update_record('code_student', $record, false);
}
function get_number_code_student(){
  global $CFG, $DB;
  $number_code=$DB->get_record('code_student', array(
    'id' => '1'
  ));
  return $number_code->number;
}
$lay_code_student=$DB->get_record('code_student', array(
  'id' => '1'
));
$numer=get_number_code_student();
function sinh_code($number){
  $key=strlen($number);
  switch ($key) {
    case 1:  $code_number='000000'.$number; return $code_number; break;
    case 2:  $code_number='00000'.$number; return $code_number; break;
    case 3:  $code_number='0000'.$number; return $code_number; break;
    case 4:  $code_number='000'.$number; return $code_number; break;
    case 5:  $code_number='00'.$number; return $code_number; break;
    case 6:  $code_number='0'.$number; return $code_number; break;
    case 7:  $code_number=$number; return $code_number; break;
  }
}

$action = optional_param('action', '', PARAM_TEXT);

$url=$CFG->dirroot . '/teams/excel_for_student/';

if(!file_exists($url)){
    mkdir($url, 0777, true);
}
if ($action=='import_student'){
    
    $schoolid = optional_param('schoolid', 0, PARAM_TEXT);
    $block_student = optional_param('block_student', 0, PARAM_TEXT);
    $groupid = optional_param('groupid', 0, PARAM_TEXT);
    $school_year = get_info_class($groupid)->id_namhoc;

    if ($_FILES['excel']['error'] > 0){
        echo "Upload lỗi rồi!";
    }
    else {
        
        if($_FILES['excel']['type']=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            //Đường dẫn file
            $newparth= $url.$_FILES["excel"]["name"];
            // move_uploaded_file($_FILES["excel"]["tmp_name"], $newparth); //Move the file from the temporary position till a new one.
            copy($_FILES["excel"]["tmp_name"], $newparth);
            $file =  $newparth;
            $objFile = PHPExcel_IOFactory::identify($file);
            $objData = PHPExcel_IOFactory::createReader($objFile);

            $objData->setReadDataOnly(true);
            $objPHPExcel = $objData->load($file);
            $sheet = $objPHPExcel->setActiveSheetIndex(0);
            $Totalrow = $sheet->getHighestRow();
            $LastColumn = $sheet->getHighestColumn();
            $TotalCol = PHPExcel_Cell::columnIndexFromString($LastColumn);
            $data = [];
                for ($i = 2; $i <= $Totalrow; $i++) {
                    $code_student=sinh_code($numer+1);
                    $code=date('Y').''.$code_student;

                    $firstname  = $sheet->getCellByColumnAndRow(0, $i)->getValue();
                    
                    $lastname   = $sheet->getCellByColumnAndRow(1, $i)->getValue();
                    $username   = $code;
                    $password   = $sheet->getCellByColumnAndRow(8, $i)->getValue();
                    if(empty($password)) {
                        $password = '0123456789';
                    }
                    $ns         = $sheet->getCellByColumnAndRow(2, $i)->getValue();
                    $birthday   = PHPExcel_Style_NumberFormat::toFormattedString($ns,'dd/mm/yyyy');
                    $sex        = $sheet->getCellByColumnAndRow(3, $i)->getValue();
                    $input_score = $sheet->getCellByColumnAndRow(4, $i)->getValue();
                    $address    = $sheet->getCellByColumnAndRow(5, $i)->getValue();
                    $name_parent= $sheet->getCellByColumnAndRow(6, $i)->getValue();
                    $email      = $sheet->getCellByColumnAndRow(7, $i)->getValue();
                    $phone1     = $sheet->getCellByColumnAndRow(8, $i)->getValue();
                    $role_id    = 5;

                    $new_user_id = add_student($firstname,$lastname,$username,$password,$birthday,$sex,$input_score,$code,$address,$name_parent,$email,$phone1);
                    $context = get_context_instance(CONTEXT_SYSTEM);
                    role_assign($role_id, $new_user_id,$context->id);
                    update_code_student($numer+1);
                    insert_member_to_group($new_user_id,$groupid);
                    insert_history_student($new_user_id,$schoolid,$block_student,$groupid,'',$school_year);
                    $numer++;
                    
                }
                echo displayJsAlert('', $CFG->wwwroot . "/teams/view_team.php?team_id=".$team_id);
                
            unlink($file);
        }
        // unlink($file);
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="card-content">
                <form id="profile" action="" onsubmit="return validate();" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <input type="text" name="action" value="import_student" hidden="">
                        <div class="form-group col-md-6">
                            <label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                            <select name="schoolid" id="schoolid" class="form-control" required>
                                <option value="<?php echo $class->id_truong ?>"><?php echo $class->tentruong ?></option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
                            <select name="block_student" id="block_student" class="form-control" required>
                                <option value="<?php echo $class->id_khoi ?>"><?php echo $class->tenkhoi; ?></option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
                            <select name="groupid" id="groupid" class="form-control" required>
                                <option value="<?php echo $team_id ?>"><?php echo $class->name; ?></option>
                            </select>
                        </div>
                        
                        
                        <div class="col-md-6" id="return_number">
                            <label class="control-label">Chọn file excel<span style="color:red">*</span></label>
                            <input type="file" value="Chọn file" required="" class="form-control" name="excel">
                            <div class="alert-link"><a href="<?php echo $CFG->wwwroot ?>/teams/excel_for_student/import_student.xlsx" download><?php echo get_string('dowload_file_sample'); ?></a></div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-buttons" style="border-top: none !important">
                                <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
                                <?php print_r(get_string('or')) ?>
                                <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
echo $OUTPUT->footer();

?>