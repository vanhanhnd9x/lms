<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');

global $DB;

require_login(0, false);

$PAGE->set_title(get_string('classlist'));
$PAGE->set_heading(get_string('classlist'));

$page         = optional_param('page', 0, PARAM_INT);
$action         = optional_param('action', '', PARAM_TEXT);
$classname         = trim(optional_param('classname', '', PARAM_TEXT));
$homeroomteacher         = trim(optional_param('homeroomteacher', '', PARAM_TEXT));
$school_year         = trim(optional_param('school_year', '', PARAM_TEXT));
$phone         = trim(optional_param('phone', '', PARAM_TEXT));
$email         = trim(optional_param('email', '', PARAM_TEXT));
$schoolid         = trim(optional_param('schoolid', '', PARAM_TEXT));
// $perpage      = optional_param('perpage', 20, PARAM_INT);
// $search       = trim(optional_param('search', '', PARAM_TEXT));
// $params = " WHERE name LIKE '%".$search."%' 
//             OR email LIKE '%".$search."%' 
//             OR gvcn LIKE '%".$search."%'
//             ";

// $list_class = get_list_groups($page,$perpage,$params);
// $totalcount = count(get_list_groups(null,$perpage,$params));

$schoolsyear = $DB->get_records_sql('SELECT * FROM school_year ORDER BY sy_start DESC');
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='groups';
$name1='classlist';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
$idnamhoc=get_id_school_year_now();

$page=isset($_GET['page'])?$_GET['page']:1;

$url= $CFG->wwwroot.'/teams/index.php?page=';
if($action=='search'){
    $list_class=get_list_groups2($page,20,$classname,$homeroomteacher,$schoolid,$school_year,$phone,$email);
    $url= $CFG->wwwroot.'/teams/index.php?action=search&classname='.$classname.'&homeroomteacher='.$homeroomteacher.'&schoolid='.$schoolid.'&school_year='.$school_year.'&phone='.$phone.'&email='.$email.'&page=';
}else{
    if(empty($school_year)){
        $school_year=$idnamhoc;
    }
    $list_class=get_list_groups2($page,20,$classname,$homeroomteacher,$schoolid,$school_year,$phone,$email);
}

// $checkten=kiem_tra_ten_lop_theo_khoi_nam_hoc(1,17,'Lớp 1');
// var_dump($checkten);
?>


<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mb-4">
                    <?php 
                    // if(!is_teacher()){
                    ?>
                    <?php  
                    if(!empty($checkthemmoi)){
                        ?>
                        <div class="col-md-2">
                            <a href="<?php print new moodle_url('/teams/new_team.php'); ?>" class="btn btn-success"><?php print_r(get_string('new')) ?></a>
                        </div>
                        <?php 
                    } 
                    ?>
                    
                    <div class="col-md-10">
                        <form action="" method="get">
                            <div class="row">
                                <input type="text" hidden="" name="action" value="search">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="classname"><?php print_r(get_string('classname')) ?></label>
                                        <input type="text" id="classname" class="saj form-control" placeholder="<?php print_r(get_string('classname')) ?>..." name="classname" value="<?php echo $classname; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="homeroomteacher"><?php print_r(get_string('homeroomteacher')) ?></label>
                                    <input type="text" id="homeroomteacher" class="saj form-control" placeholder="<?php print_r(get_string('homeroomteacher')) ?>..." name="homeroomteacher" value="<?php echo $homeroomteacher; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nameschools"><?php print_r(get_string('nameschools')) ?></label>
                                        <select type="text" id="nameschools" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true" name="schoolid">
                                            <option value="">--None--</option>
                                            <?php 
                                                $truonghoc = $DB->get_records('schools', array('del'=>0));
                                               foreach ($truonghoc as $vlu) {
                                                  ?>
                                                 <option value="<?php echo $vlu->id; ?>"  <?php if (!empty($schoolid)&&$schoolid==$vlu->id) { echo'selected'; } ?> ><?php echo $vlu->name; ?></option> 
                                                   <?php
                                               }
                                            ?>
                                          </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                     <div class="form-group">
                                          <label for="school_year"><?php print_r(get_string('school_year')) ?></label>
                                            <select type="text" id="school_year" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true" name="school_year">
                                                <option value="">--None--</option>
                                                <?php 
                                                  foreach ($schoolsyear as $vlu) { ?>
                                                    <option value="<?= $vlu->id ?>" <?= $vlu->id==$school_year ? "selected" : " " ?>>
                                                        <?= $vlu->sy_start .'-'.$vlu->sy_end ?>
                                                    </option>';
                                                  
                                               <?php }?>
                                            </select>
                                     </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <div class="form-group">
                                        <label for="phone"><?php print_r(get_string('phone')) ?></label>
                                        <input type="text" id="phone" class="saj form-control" placeholder="<?php print_r(get_string('phone')) ?>..." name="phone" value="<?php echo $phone; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email"><?php print_r(get_string('email')) ?></label>
                                    <input type="text" id="email" class="saj form-control" placeholder="<?php print_r(get_string('email')) ?>..." name="email" value="<?php echo $email; ?>">
                                    </div>
                                </div>
                                <div class="col-4 form-group">
                                    <button type="submit" class="btn btn-success">
                                        <?php echo get_string('search'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php //} ?>

                </div>
                
                <?php if(!empty($list_class['data'])){ ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo'<th class="text-center">'.get_string('action').'</th>';
                                    }
                                 ?>
                                <th><?php print_r(get_string('classname')) ?></th>
                                <th><?php print_r(get_string('nameschools')) ?></th>
                                <th><?php print_r(get_string('groupnumber')) ?></th>
                                <th><?php print_r(get_string('totalstudents')) ?></th>
                                <th><?php print_r(get_string('school_year')) ?></th>
                                <th><?php print_r(get_string('homeroomteacher')) ?></th>
                                <th><?php print_r(get_string('phone')) ?></th>
                                <th><?php print_r(get_string('email')) ?></th>

                                
                            </tr>
                        </thead>

                        <!-- <tbody id="page"> -->
                        <!-- <tbody id=""> -->
                            <?php 
                            if($page==1){
                                $i=0;
                            }else{
                                $i=($page-1)*20;
                            }  
                            foreach ($list_class['data'] as $key => $val) {
                              $i++;     
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $i;  ?></td>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo' <td class="text-center">';
                                        $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$val->id);
                                        if(!empty($checkdelete)){
                                           ?>
                                           <a onclick="return Confirm('Xóa lớp học','Bạn có muốn xóa <?php echo $val->name ?> khỏi hệ thống?','Yes','Cancel','<?php echo $CFG->wwwroot?>/teams/team.php?action=delete_team&team_id=<?php echo $val->id ?>')" class="mauicon" title="Xóa lớp học">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                           <?php 
                                        }
                                        echo'</td>';
                                    }
                                 ?>
                                <td><?php echo $val->name ?></td>
                                <?php 
                                    
                                    // $teams_in_course = get_list_groups_class($val->id);
                                    // $members = get_members_in_class($val->id); 
                                    switch ($val->status) {
                                        case 0:
                                            $hs=dem_hs_cua_lop_khi_chua_tien_hanh_chuyen_lop($val->id);
                                            $nhom=dem_nhom_cua_lop_khi_chua_tien_hanh_chuyen_lop($val->id);
                                        break;
                                        case 1:
                                            $hs=tong_hoc_sinh_cua_lop_da_tien_hanh_len_lop($val->id,$val->id_namhoc);
                                            $nhom=dem_so_nhom_lop_cua_lop_da_tien_hanh_len_lop($val->id,$val->id_namhoc);
                                        break;
                                    }
                                ?>
                                <td><?php echo $val->schoolname; ?></td>
                                <td><?php echo $nhom; ?></td>
                                <td><?php echo $hs; ?></td>
                                <td><?php echo $val->sy_start. ' - '.$val->sy_end;?></td>
                                <td><?php echo $val->gvcn ?></td>
                                <td><?php echo $val->phone ?></td>
                                <td><?php echo $val->email ?></td>

                                
                                  
                            </tr>
                            <?php } ?>
                            
                    </table>
                    <?php 
                    if ($list_class['total_page']) {
                      if ($page > 2) { 
                        $startPage = $page - 2; 
                      } else { 
                        $startPage = 1; 
                      } 
                      if ($list_class['total_page'] > $page + 2) { 
                        $endPage = $page + 2; 
                      } else { 
                        $endPage =$list_class['total_page']; 
                      }
                    }
                     ?>
                    <p>
                        <?php echo get_string('total_page'); ?>:
                        <?php echo $list_class['total_page'] ?>
                    </p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                            for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                            # code...
                              ?>
                            <li class="page-item <?php if($page==$i) echo'active1'; ?>"><a class="page-link" href="<?php echo $url,$i;?>">
                                    <?php echo $i; ?></a></li>
                            <?php 
                            }
                            ?>
                            <li class="page-item <?php if(($page==$list_class['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <?php }else{ ?>
                    <h4 class="text-danger">Không có bản ghi nào!</h4>
                <?php } ?>
            </div>
          <!--   <span id="maincontent"></span>       -->           
        </div>
    </div>
</div>
<style>
        .active1 a{
            background: #4bb747;
            border: 1px solid #4bb747;
            color:white !important;
        }
        .active1 a:hover{
            background: #218838 !important;
            border: 1px solid #218838;
            color:white;
        }
    </style>
<?php
    echo $OUTPUT->footer();
?>

<script>
    // $('.saj').keyup(function() {
    //     $.get("ajax.php?classname="+$("#classname").val()+"&nameschools="+$("#nameschools").val()+"&groupnumber="+$("#groupnumber").val()+"&totalstudents="+$("#totalstudents").val()+"&school_year="+$("#school_year").val()+"&homeroomteacher="+$("#homeroomteacher").val()+"&phone="+$("#phone").val()+"&email="+$("#email").val(), function(data){
    //         $("#page").html(data);
    //     });
    // });

    // $("select").change(function(event) {
    //     $.get("ajax.php?classname="+$("#classname").val()+"&nameschools="+$("#nameschools").val()+"&groupnumber="+$("#groupnumber").val()+"&totalstudents="+$("#totalstudents").val()+"&school_year="+$("#school_year").val()+"&homeroomteacher="+$("#homeroomteacher").val()+"&phone="+$("#phone").val()+"&email="+$("#email").val(), function(data){
    //         $("#page").html(data);
    //     });
    // });

    // $(document).ready(function(){
    //      search_ajax();
    // });

    // function search_ajax() {
    //     $.get("ajax.php?classname="+$("#classname").val()+"&nameschools="+$("#nameschools").val()+"&groupnumber="+$("#groupnumber").val()+"&totalstudents="+$("#totalstudents").val()+"&school_year="+$("#school_year").val()+"&homeroomteacher="+$("#homeroomteacher").val()+"&phone="+$("#phone").val()+"&email="+$("#email").val(), function(data){
    //         $("#page").html(data);
    //     });
    // }
</script>