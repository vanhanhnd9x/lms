<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');


require_login(0, false);

$PAGE->set_title("Teams");
$PAGE->set_heading("Teams");

//now the page contents
// $PAGE->set_pagelayout('teams');
$team_id= optional_param('team_id', 0, PARAM_TEXT);
echo $OUTPUT->header();
global $CFG;
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div class="row">
    <div class="card-box">
        <div class="col-md-12">        
        
        <table class="admin-fullscreen">
            <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        

        <div id="courseList" class="focus-panel">
            <div class="panel-head">
              <h3><?php print_r(get_string('addanewperson')) ?></h3>
                <div class="subtitle">Enter a first name, last name &amp; username to setup a new learner</div>
            </div>
            <div class="body">
              
            <form method="post" id="userForm" action="<?php echo $CFG->wwwroot ?>/teams/team.php" onsubmit='return validate_form_add_member_to_team();'><input type="hidden" value="0" name="Person.Id" id="Person_Id">
                <input type="hidden" value="add_member_to_team" name="action"/>
                
                <table class="simple-form">
                
                <tbody><tr>
                    <th>
                        <?php print_r(get_string('team'))?>:
                    </th>
                    <td>
                        <?php 
                        $team=get_team_from_id($team_id);
                        ?>
                        <select name="team_id">
                            <option value="<?php echo $team->id ?>"><?php echo $team->name ?></option>
                        </select>
                    </td>                    
                </tr>
                
                <tr>
                    <th>
                        <?php print_r(get_string('firstname')) ?>
                    </th>
                    <td>
                        <input type="text" value="" name="first_name" id="Person_FirstName" class="first-name">
                        <div id="Person_FirstName_validationMessage" class="field-validation-error"></div>
                    </td>
                    
                </tr> 
                <tr>
                    <th>
                        <?php print_r(get_string('lastname')) ?>
                    </th>
                    <td>
                        <input type="text" value="" name="last_name" id="Person_LastName" class="input-validation-valid">
                        <div id="Person_LastName_validationMessage" class="field-validation-valid"></div>
                    </td>
                </tr> 
                    <tr>
                      <th><?php print_r(get_string('username')) ?></th>
                        <td>
                        <div class="v-field clearfix"><input type="text" value="" size="30" name="username" id="Person_UserName" class="user-name"></div>                        
                        
                        <div id="Person_UserName_validationMessage" class="field-validation-error"></div>
                        <div class="tip italic clearfix">Most people use an email address for the username</div>                        
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <input type="checkbox" value="true" name="LoginEmail" id="loginemail" checked="checked"><input type="hidden" value="false" name="LoginEmail">
                        </th>
                        <td>                            
                    <label for="loginemail">Send a welcome email to this person inviting them to login</label>
                        </td>
                    </tr>                                                                         
              </tbody></table> 
              <div class="advanced-options">
              
                <div class="form-sub-heading"><?php print_r(get_string('optionalsettings','assignment')) ?></div>
              <table class="split-layout">
                <tbody><tr>
                    <td class="left">
            <table class="simple-form"> 
                    <tbody><tr>
                        <th><?php print_r(get_string('accesslevel')) ?></th>
                        <?php 
                        if(is_teacher()&&!is_admin())
                        {
                        ?>
                        <td>
                        <select name='role_id' id='role_combo_box'>
                            <option value="">---</option>
                            <option value="5">Learner</option>
                        </select>
                        </td>
                        <?php 
                        }  else {
                            
                        
                        ?>
                        <td>
                            <?php echo draw_role_combo_box(); ?>                             
                        </td>
                        <?php }?>
                    </tr>
                    <tr>
                        <th>
                            Password:
                        </th>
                        <td>
                                <input type="text" value="" name="Person.Password" id="Person_Password" class="input-validation-valid">
                                <span id="Person_Password_validationMessage" class="field-validation-valid"></span>
                        </td>
                    </tr>     
                    <tr>
                        <td colspan="2"><hr></td>
                    </tr>                                      
                    <tr>
                        <th>
                            <?php print_r(get_string('address')) ?>
                        </th>
                        <td>
                                <input type="text" value="" size="40" name="Person.Street1" id="Person_Street1">
                        </td>
                    </tr> 
                    <tr>
                        <th>
                            
                        </th>
                        <td>
                                <input type="text" value="" size="40" name="Person.Street2" id="Person_Street2">
                        </td>
                    </tr> 
                    <tr>
                        <th>
                            <?php print_r(get_string('city')) ?>
                        </th>
                        <td>
                                <input type="text" value="" size="40" name="Person.City" id="Person_City">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php print_r(get_string('state')) ?>
                        </th>
                        <td>
                                <input type="text" value="" name="Person.State" id="Person_State">
                        </td>
                    </tr> 
                    <tr>
                        <th>
                            <?php print_r(get_string('zip')) ?>
                        </th>
                        <td>
                                <input type="text" value="" size="10" name="Person.PostalCode" id="Person_PostalCode">
                        </td>
                    </tr> 
                    <tr>
                        <th>
                            <?php print_r(get_string('country')) ?>
                        </th>
                        <td>
                                <input type="text" value="" name="Person.Country" id="Person_Country">
                        </td>
                    </tr>                                                                             
                    <tr>
                        <th>
                            <?php print_r(get_string('timezone')) ?>
                        </th>
                        <td>
                                <select style="width:300px;" name="Person.TimeZone" id="Person_TimeZone"><option value="Dateline Standard Time">(UTC-12:00) International Date Line West</option>
<option value="UTC-11">(UTC-11:00) Coordinated Universal Time-11</option>
<option value="Samoa Standard Time">(UTC-11:00) Samoa</option>
<option value="Hawaiian Standard Time">(UTC-10:00) Hawaii</option>
<option value="Alaskan Standard Time">(UTC-09:00) Alaska</option>
<option value="Pacific Standard Time (Mexico)">(UTC-08:00) Baja California</option>
<option value="Pacific Standard Time">(UTC-08:00) Pacific Time (US &amp; Canada)</option>
<option value="US Mountain Standard Time">(UTC-07:00) Arizona</option>
<option value="Mountain Standard Time (Mexico)">(UTC-07:00) Chihuahua, La Paz, Mazatlan</option>
<option value="Mountain Standard Time">(UTC-07:00) Mountain Time (US &amp; Canada)</option>
<option value="Central America Standard Time">(UTC-06:00) Central America</option>
<option value="Central Standard Time">(UTC-06:00) Central Time (US &amp; Canada)</option>
<option value="Central Standard Time (Mexico)">(UTC-06:00) Guadalajara, Mexico City, Monterrey</option>
<option value="Canada Central Standard Time">(UTC-06:00) Saskatchewan</option>
<option value="SA Pacific Standard Time">(UTC-05:00) Bogota, Lima, Quito</option>
<option value="Eastern Standard Time">(UTC-05:00) Eastern Time (US &amp; Canada)</option>
<option value="US Eastern Standard Time">(UTC-05:00) Indiana (East)</option>
<option value="Venezuela Standard Time">(UTC-04:30) Caracas</option>
<option value="Paraguay Standard Time">(UTC-04:00) Asuncion</option>
<option value="Atlantic Standard Time">(UTC-04:00) Atlantic Time (Canada)</option>
<option value="Central Brazilian Standard Time">(UTC-04:00) Cuiaba</option>
<option value="SA Western Standard Time">(UTC-04:00) Georgetown, La Paz, Manaus, San Juan</option>
<option value="Pacific SA Standard Time">(UTC-04:00) Santiago</option>
<option value="Newfoundland Standard Time">(UTC-03:30) Newfoundland</option>
<option value="E. South America Standard Time">(UTC-03:00) Brasilia</option>
<option value="Argentina Standard Time">(UTC-03:00) Buenos Aires</option>
<option value="SA Eastern Standard Time">(UTC-03:00) Cayenne, Fortaleza</option>
<option value="Greenland Standard Time">(UTC-03:00) Greenland</option>
<option value="Montevideo Standard Time">(UTC-03:00) Montevideo</option>
<option value="UTC-02">(UTC-02:00) Coordinated Universal Time-02</option>
<option value="Mid-Atlantic Standard Time">(UTC-02:00) Mid-Atlantic</option>
<option value="Azores Standard Time">(UTC-01:00) Azores</option>
<option value="Cape Verde Standard Time">(UTC-01:00) Cape Verde Is.</option>
<option value="Morocco Standard Time">(UTC) Casablanca</option>
<option value="UTC" selected="selected">(UTC) Coordinated Universal Time</option>
<option value="GMT Standard Time">(UTC) Dublin, Edinburgh, Lisbon, London</option>
<option value="Greenwich Standard Time">(UTC) Monrovia, Reykjavik</option>
<option value="W. Europe Standard Time">(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
<option value="Central Europe Standard Time">(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
<option value="Romance Standard Time">(UTC+01:00) Brussels, Copenhagen, Madrid, Paris</option>
<option value="Central European Standard Time">(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
<option value="W. Central Africa Standard Time">(UTC+01:00) West Central Africa</option>
<option value="Namibia Standard Time">(UTC+01:00) Windhoek</option>
<option value="Jordan Standard Time">(UTC+02:00) Amman</option>
<option value="GTB Standard Time">(UTC+02:00) Athens, Bucharest</option>
<option value="Middle East Standard Time">(UTC+02:00) Beirut</option>
<option value="Egypt Standard Time">(UTC+02:00) Cairo</option>
<option value="Syria Standard Time">(UTC+02:00) Damascus</option>
<option value="South Africa Standard Time">(UTC+02:00) Harare, Pretoria</option>
<option value="FLE Standard Time">(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
<option value="Turkey Standard Time">(UTC+02:00) Istanbul</option>
<option value="Israel Standard Time">(UTC+02:00) Jerusalem</option>
<option value="E. Europe Standard Time">(UTC+02:00) Minsk</option>
<option value="Arabic Standard Time">(UTC+03:00) Baghdad</option>
<option value="Kaliningrad Standard Time">(UTC+03:00) Kaliningrad</option>
<option value="Arab Standard Time">(UTC+03:00) Kuwait, Riyadh</option>
<option value="E. Africa Standard Time">(UTC+03:00) Nairobi</option>
<option value="Iran Standard Time">(UTC+03:30) Tehran</option>
<option value="Arabian Standard Time">(UTC+04:00) Abu Dhabi, Muscat</option>
<option value="Azerbaijan Standard Time">(UTC+04:00) Baku</option>
<option value="Russian Standard Time">(UTC+04:00) Moscow, St. Petersburg, Volgograd</option>
<option value="Mauritius Standard Time">(UTC+04:00) Port Louis</option>
<option value="Georgian Standard Time">(UTC+04:00) Tbilisi</option>
<option value="Caucasus Standard Time">(UTC+04:00) Yerevan</option>
<option value="Afghanistan Standard Time">(UTC+04:30) Kabul</option>
<option value="Pakistan Standard Time">(UTC+05:00) Islamabad, Karachi</option>
<option value="West Asia Standard Time">(UTC+05:00) Tashkent</option>
<option value="India Standard Time">(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
<option value="Sri Lanka Standard Time">(UTC+05:30) Sri Jayawardenepura</option>
<option value="Nepal Standard Time">(UTC+05:45) Kathmandu</option>
<option value="Central Asia Standard Time">(UTC+06:00) Astana</option>
<option value="Bangladesh Standard Time">(UTC+06:00) Dhaka</option>
<option value="Ekaterinburg Standard Time">(UTC+06:00) Ekaterinburg</option>
<option value="Myanmar Standard Time">(UTC+06:30) Yangon (Rangoon)</option>
<option value="SE Asia Standard Time">(UTC+07:00) Bangkok, Hanoi, Jakarta</option>
<option value="N. Central Asia Standard Time">(UTC+07:00) Novosibirsk</option>
<option value="China Standard Time">(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
<option value="North Asia Standard Time">(UTC+08:00) Krasnoyarsk</option>
<option value="Singapore Standard Time">(UTC+08:00) Kuala Lumpur, Singapore</option>
<option value="W. Australia Standard Time">(UTC+08:00) Perth</option>
<option value="Taipei Standard Time">(UTC+08:00) Taipei</option>
<option value="Ulaanbaatar Standard Time">(UTC+08:00) Ulaanbaatar</option>
<option value="North Asia East Standard Time">(UTC+09:00) Irkutsk</option>
<option value="Tokyo Standard Time">(UTC+09:00) Osaka, Sapporo, Tokyo</option>
<option value="Korea Standard Time">(UTC+09:00) Seoul</option>
<option value="Cen. Australia Standard Time">(UTC+09:30) Adelaide</option>
<option value="AUS Central Standard Time">(UTC+09:30) Darwin</option>
<option value="E. Australia Standard Time">(UTC+10:00) Brisbane</option>
<option value="AUS Eastern Standard Time">(UTC+10:00) Canberra, Melbourne, Sydney</option>
<option value="West Pacific Standard Time">(UTC+10:00) Guam, Port Moresby</option>
<option value="Tasmania Standard Time">(UTC+10:00) Hobart</option>
<option value="Yakutsk Standard Time">(UTC+10:00) Yakutsk</option>
<option value="Central Pacific Standard Time">(UTC+11:00) Solomon Is., New Caledonia</option>
<option value="Vladivostok Standard Time">(UTC+11:00) Vladivostok</option>
<option value="New Zealand Standard Time">(UTC+12:00) Auckland, Wellington</option>
<option value="UTC+12">(UTC+12:00) Coordinated Universal Time+12</option>
<option value="Fiji Standard Time">(UTC+12:00) Fiji</option>
<option value="Magadan Standard Time">(UTC+12:00) Magadan</option>
<option value="Kamchatka Standard Time">(UTC+12:00) Petropavlovsk-Kamchatsky - Old</option>
<option value="Tonga Standard Time">(UTC+13:00) Nuku'alofa</option>
</select>
                        </td>
                    </tr>                                                                                                                                
            </tbody></table>                       
                    </td>
                    <td>
            <table class="simple-form">
                    <tbody><tr>
                        <th>Email:</th>
                        <td><input type="text" value="" size="30" name="email" id="Person_Email" class="v-email email-field input-validation-valid">
                        <span id="Person_Email_validationMessage" class="field-validation-valid"></span>
                        </td>
                    </tr>                       
                    <tr>
                        <th>
                            <input type="checkbox" value="true" name="Person.DisableMessages" id="notifications"><input type="hidden" value="false" name="Person.DisableMessages">
                        </th>
                        <td>                            
                    <label for="notifications">Disable all email notifications</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr></td>
                    </tr> 
                    <tr>
                        <th>
                            Title:
                        </th>
                        <td>
                                <input type="text" value="" name="Person.Title" id="Person_Title">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php print_r(get_string('company')) ?>
                        </th>
                        <td>
                                <input type="text" value="" size="30" name="Person.CompanyName" id="Person_CompanyName">
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2"><hr></td>
                    </tr>                                                                                                                  
                    <tr>
                        <th>
                            <?php print_r(get_string('workph')) ?>
                        </th>
                        <td>
                                <input type="text" value="" name="Person.PhoneWork" id="Person_PhoneWork">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php print_r(get_string('mobileph')) ?>
                        </th>
                        <td>
                                <input type="text" value="" name="Person.PhoneMobile" id="Person_PhoneMobile">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Skype:
                        </th>
                        <td>
                                <input type="text" value="" name="Person.Skype" id="Person_Skype">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Twitter:
                        </th>
                        <td>
                                <input type="text" value="" name="Person.Twitter" id="Person_Twitter">
                        </td>
                    </tr>
                    

                    <tr>
                        <td colspan="2"><hr></td>
                    </tr>                             
                    <tr>
                        <th>
                            Website:
                        </th>
                        <td>
                                <input type="text" value="" size="30" name="Person.Website" id="Person_Website">
                        </td>
                    </tr>                                                                                                                                                       
            </tbody></table>                       
                    </td>
                </tr>
              </tbody></table>

        </div>

            <div class="form-buttons">
                <input type="submit" id="saveUser" class="btnlarge" value=" Add Person ">
                or
                <input type="hidden" value="" name="AddAnother" id="AddAnother">
                <input type="submit" class="btnlarge" id="AddMore" value="Add Person &amp; then add another">                 
                or
                <a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $team_id ?>">cancel</a>
            </div>
            </form><script type="text/javascript">
//&lt;![CDATA[
if (!window.mvcClientValidationMetadata) { window.mvcClientValidationMetadata = []; }
window.mvcClientValidationMetadata.push({"Fields":[{"FieldName":"Person.FirstName","ReplaceValidationMessageContents":true,"ValidationMessageId":"Person_FirstName_validationMessage","ValidationRules":[{"ErrorMessage":"First name required","ValidationParameters":{},"ValidationType":"required"}]},{"FieldName":"Person.LastName","ReplaceValidationMessageContents":true,"ValidationMessageId":"Person_LastName_validationMessage","ValidationRules":[]},{"FieldName":"Person.UserName","ReplaceValidationMessageContents":true,"ValidationMessageId":"Person_UserName_validationMessage","ValidationRules":[{"ErrorMessage":"Username required","ValidationParameters":{},"ValidationType":"required"}]},{"FieldName":"Person.Password","ReplaceValidationMessageContents":true,"ValidationMessageId":"Person_Password_validationMessage","ValidationRules":[]},{"FieldName":"Person.Email","ReplaceValidationMessageContents":true,"ValidationMessageId":"Person_Email_validationMessage","ValidationRules":[{"ErrorMessage":"Invalid email address","ValidationParameters":{"pattern":"^\\s*([a-zA-Z0-9_\\-\\.\\\u0027]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\\s*$"},"ValidationType":"regularExpression"}]}],"FormId":"userForm","ReplaceValidationSummary":false});
//]]&gt;
</script>                
                
            </div>
            
        </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        

<div class="side-panel">

</div>


                    </div>
                </td>
            </tr>
        </tbody></table>
        
        </div>
    </div>
</div>