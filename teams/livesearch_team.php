<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once ("../config.php");
require_once ("lib.php");
// Require Login.
require_login(0, FALSE);
global $USER, $CFG;
//$q = required_param('q', PARAM_TEXT);
$s = required_param('s', PARAM_TEXT);
//echo $s;
//$teams = search_team_from_name($s, $USER->id);

?>
<div class="body">
    <div id="searchResults">

        <table class="item-page-list">

            <tbody>
<?php
if(is_teacher()){
$teams = get_team_list_second($s);

foreach ($teams as $key => $val) {
    ?>
                    <tr>
                      
                        <td>                       
                            
                            <div class="title"><a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $val->id ?>" title="<?php echo $val->name ?>"><?php echo $val->name ?></a></div>                         
                        </td>
                    </tr>
<?php } }
 else{
 $teams = get_team_list($s);
 foreach ($teams as $key => $val) {
    ?>
                    <tr>
                      
                        <td>                       
                            
                            <div class="title"><a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $val->id ?>" title="<?php echo $val->name ?>"><?php echo $val->name ?></a></div>                         
                        </td>
                    </tr>
                    <?php
}}
?>
            </tbody>

        </table>

        <div class="list-pager">
            <ul>

            </ul>
            <div class="clear"></div>    
        </div>


      <div class="tip center-padded"><?php echo count($teams)." ".  get_string('team') ?> </div>

    </div>
</div>
