<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');

require_login(0, false);

$PAGE->set_title(get_string('teams'));
$PAGE->set_heading(get_string('teams'));

$team_id = optional_param('team_id', 0, PARAM_TEXT);
$team = get_team_from_id($team_id);
echo $OUTPUT->header();
global $CFG;
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        
        
        
        <table class="admin-fullscreen">
            <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        
    <div class="focus-panel">
        
<div class="panel-head clearfix">
    <div></div>   
    <h3><?php echo $team->name ?></h3>
    <div class="subtitle"><?php echo $team->description ?></div>
</div>
           

        <div class="body">
          <p class="warning"><b><?php print_string('areyouteam') ?></b></p>
            <p><?php print_string('oncethisteam') ?></p>
            <div class="form-buttons">
                <form method="post" action="<?php echo $CFG->wwwroot?>/teams/team.php?action=delete_team&team_id=<?php echo $team_id ?>">
                  <input type="submit" class="btnlarge" value="<?php print_string('delete') ?>"> <?php print_string('or') ?> <a href="<?php echo $CFG->wwwroot?>/teams/team_settings.php?team_id=<?php echo $team_id ?>" class=""><?php print_string('cancel') ?></a>   
                </form>
            </div>
        </div>
    </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        
                        
                    </div>
                </td>
            </tr>
        </tbody></table>
        
        
    </div>
