<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');

require_login(0, false);

$PAGE->set_title(get_string('teams'));
$PAGE->set_heading(get_string('teams'));

$team_id = optional_param('team_id', 0, PARAM_TEXT);
$team = get_team_from_id($team_id);
echo $OUTPUT->header();
global $CFG;
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        
    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">

                        <div id="teamList" class="focus-panel">
                            <div class="panel-head">
                              <h3><?php print_r(get_string('addanewteam')) ?></h3>

                              <div class="subtitle"><?php print_r(get_string('asasubteam')) ?> <a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $team_id ?>"><?php echo $team->name ?></a></div>

                            </div>
                            <div class="body">
                                <form method="post" id="createTeamForm" action="<?php echo $CFG->wwwroot ?>/teams/team.php?action=add_sub_team&team_id=<?php echo $team_id ?>">
                                    
                                    <table class="simple-form">
                                        <tbody><tr>
                                                <th>
                                                    <?php print_r(get_string('teamname')) ?>
                                                </th>
                                                <td>
                                                    <input type="text" value="" name="name" id="createTeamName" class="full first-focus">

                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('description')) ?>
                                                </th>
                                                <td>
                                                    <textarea rows="10" name="description" id="Description" cols="20" class="full"></textarea>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><input type="checkbox" value="true" name="InheritCourses" id="InheritCourses"></th>
                                                <td><label for="InheritCourses"><?php print_r(get_string('automaticallyassignall')) ?> <b><?php echo $team->name ?></b> <?php print_r(get_string('teamtothisnewteam')) ?></label></td>
                                            <input type="hidden" name="inherit_course" id="inherit_course" value=""/>
                                            </tr>

                                        </tbody></table>
                                    <div class="form-buttons">
                                      <input type="submit" class="btnlarge" value="<?php print_r(get_string('addteam')); ?>">
                                        <?php print_r(get_string('or')) ?>
                                        <input type="hidden" value="" name="AddAnother" id="AddAnother">
                                        <input type="submit" class="btnlarge" id="AddMore" value="<?php print_r(get_string('addteamandthenadd')); ?>">
                                        <?php print_r(get_string('or')) ?> 
                                        <a href="<?php echo $CFG->wwwroot ?>/teams/team_course.php?team_id=<?php echo $team->id ?>"><?php print_r(get_string('cancel')) ?></a>

                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">

                        <div class="side-panel">
                          <h3><?php print_r(get_string('help')) ?></h3>
                            <div class="help">
                              <p><?php print_r(get_string('enteraname')) ?></p>
                              <p><?php print_r(get_string('youcanthen')) ?></p>
                                
                            </div>
                        </div>

                        <?php
                        $sub_teams = get_sub_teams($team_id);
                        if (count($sub_teams) > 0) {
                            ?>

                            <div class="side-panel team-teams">
                                <h3>
                                    Teams under the team1 edit team</h3>
                                <ul>
                                    <?php foreach ($sub_teams as $key => $val) { ?>
                                        <li>
                                            <a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $team_id ?>"><?php echo $val->name ?></a></li>
                                    <?php } ?>
                                </ul>     
                            </div>
                        <?php } ?>
                    </div>
                </td>
            </tr>
        </tbody></table>


</div>