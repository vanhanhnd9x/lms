<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$daysToSendReminder=30;
$mailUsername="tung@thienhoang.com.vn";
$pass="T12345678";
$NordpeisEmail='tung@thienhoang.com.vn';


$emailTitleNo= get_string('loginlms');
$emailContentNo="Chào mừng tới hệ thống tuyển dụng LMS<br/><br/>
Thông tin đăng nhập của bạn như sau:<br/><br/>
Tài khoản: {USER_NAME}<br/><br/>
Mật khẩu: {PASSWORD}<br/><br/>
Để đăng nhập và hoàn thành bài kiểm tra, bạn sử dụng đường link sau:{LINK_TO_WEB}.";


$team_update_title="Team update LMS";
$team_update_content="Hello {USER_NAME},
<br/><br/>
You have been assigned to the team <b>{TEAM_NAME}</b>.<br/><br/>
<br/><br/>
{TEAM_NAME}
<br/><br/>
To login and complete your training courses please click on the following link:
{LINK_TO_WEB}
.";


$team_invitation_title="LMS-online course invitation";
$team_invitation_content="Hello {USER_NAME},
<br/><br/>
You have been invited to do an online course.<br/><br/>
---------------------------------------------------<br/><br/>
{COURSE_TITLE}<br/><br/>
{COURSE_DESCRIPTION}<br/><br/>
---------------------------------------------------<br/><br/>
<br/><br/>
{TEAM_NAME}
<br/><br/>
To login and complete your training courses please click on the following link:
{LINK_TO_WEB}
.";

$welcome_title="E learning Online: account confirmation";
$welcome_content="Hi,<br/><br/>

A new account has been requested at 'E learning Online' <br/><br/>
using your email address.

To confirm your new account, please go to this web address:<br/><br/>

{CONFIRM_LINK_EMAIL}<br/><br/>
Thank you

";

?>
