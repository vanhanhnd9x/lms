<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ("../config.php");
require_once($CFG->dirroot . '/teams/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
// Require Login.
require_login(0, FALSE);

$q = $_GET["q"];
$team_id = $_GET["team_id"];
$courses_of_team = get_courses_from_team($team_id);
$course_of_team_arr = array();
foreach ($courses_of_team as $key => $val) {
    $course_of_team_arr[] = $key;
}
$courses = search_enrol_get_my_courses($q);
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>

<table cellspacing="0" class="item-page-list">
    <tbody>

        <?php
        foreach ($courses as $key => $val) {
            ?>
            <tr class="c1">
                <td>
                    <?php if (in_array($val->id, $course_of_team_arr)) { ?>
                        &nbsp;
                    <?php } else { ?>
                        <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                    <?php } ?>
                </td>
                <td class="main-col">
                    <div class="title">
                        <label for="u15539"><?php echo $val->fullname ?></label></div>                                    
                </td>
                <td class="nowrap">&nbsp;
                    <?php if (in_array($val->id, $course_of_team_arr)) { ?>
                        <span title="" class="box-tag box-tag-green "><span>
                                <?php print_r(get_string('alreadyassigned')) ?>
                            </span></span>                       
                    <?php } else { ?>
                        <span title="" class=""><span>

                            </span></span>                       
                    <?php } ?>

                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
