<?php
if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');
// require_once($CFG->dirroot . '/common/lib.php');


require_login(0, false);
$team_id= optional_param('team_id', 0, PARAM_TEXT);
$action= optional_param('action', '', PARAM_TEXT);
if(empty($action)){
  $PAGE->set_title(get_string('class'));
  $PAGE->set_heading(get_string('class'));
  $name1='classedit';
}
if($action=='view'){
  $PAGE->set_title(get_string('classview'));
  $PAGE->set_heading(get_string('classview'));
  $name1='classview';
}

echo $OUTPUT->header();
global $CFG;
$team=  get_team_from_id($team_id);
$teacher = get_list_teacher();
$teachertutors = get_list_teachertutors();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='groups';

$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

?>
<script type="text/javascript" src="js/search.js"></script>

<div class="row">
  <div class="col-md-10">
    <div class="card-box">
        <form method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/teams/team.php?action=edit_team&team_id=<?php echo $team_id ?>" onsubmit="return validate();" class="form-horizontal">
            <div class="row">

              <div class="col-md-6">
              <div class="form-group">
                <label><?php print_r(get_string('schools'))?> <span style="color:red">*</span></label>
                <select name="schoolid" id="schoolid" class="form-control" disabled="">
                 <option value="">-- None --</option>
                  <?php 
                   $schools = $DB->get_records_sql('SELECT * FROM schools WHERE del=0');;
                   foreach ($schools as $key => $value) {
                          # code...
                    ?>
                    <option value="<?php echo $value->id; ?>" <?php echo $team->id_truong == $value->id ? 'selected="selected"' : ''; ?>><?php echo $value->name; ?></option>
                    <?php 
                  }
                  ?>
                </select>
                <span id="er_schools" class="text-danger" style="display: none;">Vui lòng chọn trường học</span>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label><?php print_r(get_string('block_student'))?> <span style="color:red">*</span></label>
                <select name="block_student" id="block_student" class="form-control" disabled="">
                 <option value="">-- None --</option>
                 <?php
                    $khoi = get_khoi($team->id_truong);
                    foreach ($khoi as $val) {
                      if ($val->id == $team->id_khoi) {
                        echo '<option selected value="'.$val->id.'">'.$val->name.'</option>';
                      }else
                        echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                    }
                  ?>
                </select>
                <span id="er_khoi" class="text-danger" style="display: none;">Vui lòng chọn khối học</span>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label><?php print_r(get_string('classname')) ?> <span class="text-danger">*</span></label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <input type="text" id="class_name_block" name="class_name_block" readonly="" value="<?php echo substr($team->name,0,7) ?>">
                      </span>
                    </div>
                    <input type="text" value="<?php echo substr($team->name,7) ?>" name="name" id="class_name" class="form-control" required>
                    <span id="er_class_name" class="text-danger" style="display: none;">Vui lòng nhập tên lớp</span>
                  </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
            

            <div class="col-md-6">
              <div class="form-group">
                <label><?php print_r(get_string('homeroomteacher'))?> </label>
                <input type="text" value="<?php echo $team->gvcn ?>" name="gvcn" id="gvcn" class="form-control">
                <!-- <span id="er_gvcn" class="text-danger" style="display: none;">Vui lòng nhập họ tên giáo viên chủ nhiệm</span> -->
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label><?php print_r(get_string('email'))?> </label>
                <input type="text" value="<?php echo $team->email ?>" name="email" id="email" class="form-control">
                <span id="er_email" class="text-danger"></span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label><?php print_r(get_string('phone'))?> </label>
                <input type="text" value="<?php echo $team->phone ?>" name="phone" id="phone" class="form-control">
                <span id="er_phone" class="text-danger"></span>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label><?php print_r(get_string('school_year')) ?><span style="color:red">*</span></label>
                <select name="namhoc" id="namhoc" class="form-control" data-style="select-with-transition" title="" data-size="7" required>
                  <option value="" >-- None --</option>
                  <?php $sy = $DB->get_records('school_year'); foreach ($sy as $items) { ?>
                    <option value="<?php echo $items->id  ?>" <?php echo $items->id==$team->id_namhoc ? 'selected' : '' ?> ><?php echo $items->sy_start .' - '.$items->sy_end  ?></option>
                  <?php } ?>
                </select>
                <span id="er_namhoc" class="text-danger" style="display: none;">Vui lòng chọn năm học</span>
              </div>
            </div>


           <!--  -->

            <div class="col-md-12">
              <div class="form-group">
                <label><?php print_r(get_string('description')) ?></label>
                <label class="control-label"></label>
                <textarea name="description" id="description" cols="20" class="form-control"><?php echo $team->description ?></textarea>
              </div>
            </div>
            <?php 
              if(empty($action)){
                ?>
                <div class="col-md-12">
                  <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="idsave">
                  <?php print_r(get_string('or')) ?> 
                  <a href="<?php echo $CFG->wwwroot ?>/teams/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a> 
                </div> 
                <?php 
              }
              if($action=='view'){
                ?>
                <div class="col-md-12">
                 <a href="<?php echo $CFG->wwwroot ?>/teams/index.php" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
               </div>
                <?php 
              }
             ?>
                                
          </div>
        </form>
    </div>
  </div>
</div>
<?php 
echo $OUTPUT->footer();
if($team->status==1){
  ?>
  <script>
      $('#idsave').attr('disabled', true);
    </script>
  <?php
}
?>
<script type="text/javascript">
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
      });
    }
  });
</script>
