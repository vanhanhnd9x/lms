function validate() {
    var error = 0;

    if (document.getElementById('class_name') && document.getElementById('class_name').value == '') {
        $("#er_class_name").show();
        error = 1;
    } else {
        $("#er_class_name").hide();
    }

    if (document.getElementById('schoolid') && document.getElementById('schoolid').value == '') {
        $("#er_schools").show();
        error = 1;
    } else {
        $("#er_schools").hide();
    }
    if (document.getElementById('block_student') && document.getElementById('block_student').value == '') {
        $("#er_khoi").show();
        error = 1;
    } else {
        $("#er_khoi").hide();
    }
    if (document.getElementById('gvcn') && document.getElementById('gvcn').value == '') {
        $("#er_gvcn").show();
        error = 1;
    } else {
        $("#er_gvcn").hide();
    }

    if (document.getElementById('namhoc') && document.getElementById('namhoc').value == '') {
        $("#er_namhoc").show();
        error = 1;
    } else {
        $("#er_namhoc").hide();
    }

// if (document.getElementById("email") && (document.getElementById("email").value == '')) {
//         document.getElementById("er_email").innerHTML = 'Vui lòng nhập email';
//         // $("#er_email").show();
//         error = 0;
//     } else

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (document.getElementById('email').value != '' && reg.test(document.getElementById('email').value) == false) {
        document.getElementById("er_email").innerHTML = 'Email nhập sai định dạng';
        $("#er_email").show();
        error = 1;
    } else {
        $("#er_email").hide();
    }

    var reg =/^[0-9]{9,11}$/;
    if(document.getElementById('phone').value !='' && reg.test(document.getElementById('phone').value) == false) {
        document.getElementById("er_phone").innerHTML = 'Số điện thoại không hợp lệ';
        $("#er_phone").show();
        error = 1;
    }else {
        $("#er_phone").hide();
    }

    if (error == 1) {
        return false;
    }
    return true;

}
