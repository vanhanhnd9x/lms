<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ("../config.php");
require_once ("lib.php");
// Require Login.
require_login(0, FALSE);

$q = $_GET["q"];
$team_id = $_GET["team_id"];
$users = search_members_in_team($team_id, $q);
$un_members = search_un_members_in_team($team_id, $q);
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div>
    <table cellspacing="0" class="item-page-list">
        <tbody>
            <?php
            foreach ($users as $key => $val) {
                ?>
                <tr>
                    <td>
                        &nbsp;

                    </td>
                    <td class="main-col">
                        <label for="u134670">
                            <span class="title">
                                <?php echo $val->firstname ?> <?php echo $val->lastname ?></span>                                  
                            <span class="tip">
                                <?php echo $val->username ?></span>   
                        </label>
                    </td>
                    <td class="nowrap">&nbsp;<span class="box-tag box-tag-green " title=""><span><?php print_r(get_string('alreadyassigned')) ?></span></span>                

                    </td>
                </tr>
            <?php } ?>

            <?php
            foreach ($un_members as $key => $val) {
                ?>
                <tr>
                    <td>
                        <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">

                    </td>
                    <td class="main-col">
                        <label for="u134670">
                            <span class="title">
                                <?php echo $val->firstname ?> <?php echo $val->lastname ?></span>                                  
                            <span class="tip">
                                <?php echo $val->username ?></span>   
                        </label>
                    </td>
                    <td class="nowrap">&nbsp;<span title=""><span></span></span>                

                    </td>

                </tr>
            <?php } ?>  

        </tbody></table>
</div>
