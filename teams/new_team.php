<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');

require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');


require_login(0, false);

$PAGE->set_title(get_string('class'));
$PAGE->set_heading(get_string('class'));

//now the page contents
// $PAGE->set_pagelayout('teams');
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='groups';
$name1='classnew';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$sql_namhoc="SELECT * FROM `school_year` ORDER BY sy_end DESC";
$sy = $DB->get_records_sql($sql_namhoc);
?>

<script type="text/javascript" src="js/search.js"></script>


<div class="row">
  <div class="col-md-10">
    <div class="card-box">
        <form method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot  ?>/teams/team.php" onsubmit="return validate();" class="form-horizontal">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('schools')) ?> <span class="text-danger">*</span></label>
                  <select name="schoolid" id="schoolid" class="form-control" required>
                   <option value="">-- None --</option>
                    <?php 
                     $schools = $DB->get_records_sql('SELECT * FROM schools WHERE del=0');
                     foreach ($schools as $key => $value) {
                            # code...
                      ?>
                      <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                      <?php 
                    }
                    ?>
                  </select>
                  <span id="er_schools" class="text-danger" style="display: none;">Vui lòng chọn trường học</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('blocks')) ?> <span class="text-danger">*</span></label>
                  <select name="block_student" id="block_student" class="form-control" required>
                   <option value="">-- None --</option>
                  </select>
                  <span id="er_khoi" class="text-danger" style="display: none;">Vui lòng chọn khối học</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('classname')) ?> <span class="text-danger">*</span></label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <input type="text" id="class_name_block" name="class_name_block" readonly="" placeholder="<?php echo get_string('class'); ?>...">
                      </span>
                    </div>
                    <input type="text" value="" name="name" id="class_name" class="form-control" required>
                    <span id="er_class_name" class="text-danger" style="display: none;">Vui lòng nhập tên lớp</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <!-- <div class="form-group">
                  <label>Mã lớp</label>
                  <input type="text" class="form-control" value="<?php echo $course->idnumber ?>" name="code" id="code">
                </div> -->
              </div>
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('homeroomteacher')) ?> </label>
                  <input type="text" value="<?php echo $course->gvcn ?>" name="gvcn" id="gvcn" class="form-control ">
                  <!-- <span id="er_gvcn" class="text-danger" style="display: none;">Vui lòng nhập họ tên giáo viên chủ nhiệm</span> -->
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('email')) ?> </label>
                  <input type="text" value="<?php echo $course->email ?>" name="email" id="email" class="form-control ">
                  <span id="er_email" class="text-danger"></span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('phone')) ?> </label>
                  <input type="text" value="<?php echo $course->phone ?>" name="phone" id="phone" class="form-control ">
                  <span id="er_phone" class="text-danger"></span>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label><?php print_r(get_string('school_year')) ?> <span class="text-danger">*</span></label>
                  <select name="namhoc" id="namhoc" class="form-control" data-style="select-with-transition" title="" data-size="7" required>
                    <option value="" >-- None --</option>
                    <?php //$sy = $DB->get_records('school_year');
                     foreach ($sy as $items) { ?>
                      <option value="<?php echo $items->id  ?>" ><?php echo $items->sy_start .' - '.$items->sy_end  ?></option>
                    <?php } ?>
                  </select>
                  <span id="er_namhoc" class="text-danger" style="display: none;">Vui lòng chọn năm học</span>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label><?php print_r(get_string('description')) ?></label>
                  <label class="control-label"></label>
                  <textarea name="description" id="description" cols="20" class="form-control"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
                <input type="hidden" value="add_new_team" name="action">
                  <?php print_r(get_string('or')) ?>
                <a href="<?php echo $CFG->wwwroot ?>/teams/" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
              </div>
            </div>
        </form>
    </div>
  </div>
</div>
<?php 
echo $OUTPUT->footer();
?>
<script type="text/javascript">
  $('#schoolid').on('change', function() {
    var schoolid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/common/ajax.php?schoolid="+schoolid,function(data){
        $("#block_student").html(data);
      });
    }
  });
  $(document).on('change', '#block_student', function() {
    var text = $(this).find('option:selected').attr('data-status');
    console.log(text);
    $('#class_name_block').val(text);
  });

</script>
