<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/team/lib.php');

function displayJsAlert($ms, $redirectPath) {
    $html = '';
    if ($ms != '') {
        $html .= '<script language="javascript">alert("' . $ms . '")</script>';
    }

    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}

function add_new_team_class_up($name,$school,$khoi) {
    global $DB, $USER;
    $data = new stdClass();
    $data->name = trim($name);
    $data->id_truong = $school;
    $data->id_khoi = $khoi;
    // $data->id_namhoc = $namhoc;
    $data->timecreated = time();
    $data->timemodified = $data->timecreated;
    $data->userid = $USER->id;
    $data->courseid = 0;
    $lastinsertid = $DB->insert_record('groups', $data);
    return $lastinsertid;
}


function add_new_team($name, $gvcn,$email,$phone,$school,$khoi,$namhoc, $description) {
    global $DB, $USER;
    $data = new stdClass();
    $data->name = trim($name);
    $data->gvcn = trim($gvcn);
    $data->email = trim($email);
    $data->phone = trim($phone);
    $data->id_truong = $school;
    $data->id_khoi = $khoi;
    $data->id_namhoc = $namhoc;
    $data->description = $description;
    $data->timecreated = time();
    $data->timemodified = $data->timecreated;
    $data->userid = $USER->id;
    $data->courseid = 0;
    //var_dump($data);
    //die();
    $lastinsertid = $DB->insert_record('groups', $data);
    return $lastinsertid;
}
function add_new_team_year($groupid, $schoolyearid) {
    global $DB, $USER;
    $data = new stdClass();
    $data->groupid = $groupid;
    $data->schoolyearid = $schoolyearid;

    $lastinsertid = $DB->insert_record('groups_year', $data);
    return $lastinsertid;
}

function add_new_sub_team($name, $description, $parent) {
    global $DB, $USER;
    $data = new stdClass();
    $data->name = $name;
    $data->description = $description;
    $data->timecreated = time();
    $data->timemodified = $data->timecreated;
    $data->userid = $USER->id;
    $data->courseid = 0;
    $data->parent = $parent;
    //var_dump($data);
    //die();
    $lastinsertid = $DB->insert_record('groups', $data);
    return $lastinsertid;
}

function get_team_list($search = "", $creator_id) {
    global $DB;
//    Tung added for view groups
    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $creator_id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $creator_id;
//    Get Course of User
    $courseArr = enrol_get_my_courses_for_people();
    foreach ($courseArr as $course) {
        $idCourse = $course->id;
        $teamArrIds = get_teams_from_course($idCourse);
//        var_dump($teamArrIds);
        foreach ($teamArrIds as $teamArrId) {
            $teamId .= $teamArrId->group_id . ",";
        }
        $teamId .= $teamId;
    }
    $teamId .= "0";
//    var_dump($teamId);
    $sql = 'SELECT * FROM groups WHERE userid IN (' . $id_list . ') OR id IN ('.$teamId.')';
    $params = null;
    if ($search != "") {
        $sql.=" and " . $DB->sql_like('name', ':name', false);
        $params['name'] = '%' . $search . '%';
    }
    //echo($sql);
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

//Tung added function for role teacher
function get_team_list_second($search = "") {
    global $DB, $USER;
    // tung fix: load all data in account
    $sql_user = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql_user);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

    $id_list = display_children($rows, $id_list);
    $id_list .= $USER->id;
    //end fix
    $sql = 'SELECT * from groups where userid in (' . $id_list . ')';
    $params = null;
    if ($search != "") {
        $sql.=" and " . $DB->sql_like('name', ':name', false);
        $params['name'] = '%' . $search . '%';
    }
    //echo($sql);
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function get_un_members_in_team($team_id, $parent_id) {
    global $DB;
    $sql = 'SELECT * from user where parent=' . $parent_id . " and id not in (select userid from groups_members where groupid=$team_id)";

    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_all_courses() {
    global $DB;
    $courses = $DB->get_records('course', null);

    return $courses;
}

function search_un_members_in_team($team_id, $name) {
    global $DB, $USER;
    $params = array();
    $sql = 'SELECT * from user where parent=' . $USER->id . " and id not in (select userid from groups_members where groupid=$team_id)";

    if ($name != "") {
        $sql.=" and (" . $DB->sql_like('user.username', ':name', false);
        $params['name'] = '%' . $name . '%';

        $sql.=" or " . $DB->sql_like('user.firstname', ':name1', false);
        $params['name1'] = '%' . $name . '%';

        $sql.=" or " . $DB->sql_like('user.lastname', ':name2', false);
        $params['name2'] = '%' . $name . '%';

        $sql.=")";
        $result = $DB->get_records_sql($sql, $params);
    } else {
        $result = $DB->get_records_sql($sql);
    }
    //echo($sql);

    return $result;
}

function search_members_in_team($team_id, $name) {
    global $DB, $USER;
    $params = array();
    $sql = 'SELECT user.username,user.id,user.firstname,user.lastname
    FROM groups_members,user
    WHERE 
    user.id=groups_members.userid  
    AND user.parent=' . $USER->id . ' and groups_members.groupid=' . $team_id;
    if ($name != "") {
        $sql.=" and (" . $DB->sql_like('user.username', ':name', false);
        $params['name'] = '%' . $name . '%';

        $sql.=" or " . $DB->sql_like('user.firstname', ':name1', false);
        $params['name1'] = '%' . $name . '%';

        $sql.=" or " . $DB->sql_like('user.lastname', ':name2', false);
        $params['name2'] = '%' . $name . '%';

        $sql.=")";
        $result = $DB->get_records_sql($sql, $params);
    } else {
        $result = $DB->get_records_sql($sql);
    }
    //echo($sql);

    return $result;
}


// Danh sách học sinh đã được gán vào lớp học
function get_members_in_team($team_id) {
    global $DB;
    $sql = 'SELECT user.username,user.id,user.firstname,user.lastname,user.email,groups.name 
    FROM groups_members, user, groups
    WHERE 
    user.id = groups_members.userid
    AND groups_members.groupid = groups.id
    AND groups_members.userid =' . $team_id;

    $result = $DB->get_record_sql($sql);
    return $result;
}

function get_users_class($courseid) {
    global $DB, $USER;
//    $sql1 = "SELECT * FROM user ";
//    $rows = $DB->get_records_sql($sql1);
//
//    $id_list = display_parent($rows, $USER->id);
//    $id_me = $id_list;
//
//    if ($id_me == 0) {
//        $id_me = $USER->id;
//    }
//    if ($id_list == 0) {
//        $id_list = $USER->id;
//    }

    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $creator_id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $USER->id;

//    $id_list = display_children($rows, $id_list);
//    $id_list .= $USER->id . "," . $id_me;
    $field = "id, firstname, lastname, email, company";
    $sql = 'SELECT ' . $field . ' FROM {user} u
        WHERE u.id in (' . $id_list . ')';
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_members_in_team_second($course_id) {
    global $DB;
    $sql = "SELECT 
                user.id, user.birthday,
                user.`code`,
                user.name_parent,
                user.email,
                user.sex,
                user.firstname,
                user.lastname
                
            FROM user 
                JOIN user_enrolments ON user_enrolments.userid = user.id
                JOIN enrol ON enrol.id = user_enrolments.enrolid
                JOIN course ON enrol.courseid = course.id 
                JOIN role_assignments ON user.id = role_assignments.userid
            WHERE
                NOT user.id=2
                AND user.del=0
                AND role_assignments.roleid =5
                AND course.id = {$course_id} ORDER BY user.firstname ASC";

    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_info_groups($course_id){
    global $DB;
    $sql = "SELECT course.*, groups.id AS id_lop, groups.id_truong, groups.id_khoi FROM course
            JOIN group_course ON group_course.course_id = course.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            JOIN block_student ON groups.id_khoi = block_student.id
            WHERE course.id=".$course_id;
    $groups = $DB->get_record_sql($sql);
    return $groups;
}

function get_team_from_id($team_id) {
    global $DB;
    $team = "SELECT
                groups.*,
                groups_year.schoolyearid,
                schools.name AS tentruong,
                school_year.sy_start,school_year.sy_end
            FROM
                groups
                JOIN groups_year ON groups.id = groups_year.groupid
                JOIN schools ON schools.id=groups.id_truong
                JOIN school_year ON school_year.id = groups.id_namhoc
            WHERE
                groups.id = {$team_id}";
    return $DB->get_record_sql($team);
}

function get_khoi($school){
  global $DB;
  if(!empty($school)){
    $khoi = $DB->get_records('block_student', array('schoolid' => $school,'del'=>0));
    return $khoi;
  }
}

function randPass($length, $strength = 8) {
    $vowels = 'aeuy';
    $consonants = 'bdghjmnpqrstvz';
    if ($strength >= 1) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
    }
    if ($strength >= 2) {
        $vowels .= "AEUY";
    }
    if ($strength >= 4) {
        $consonants .= '23456789';
    }
    if ($strength >= 8) {
        $consonants .= '@#$%';
    }

    $password = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $password;
}

function get_all_rolles() {
    global $DB;
    $roles = $DB->get_records('role', null);

    return $roles;
}

function draw_role_combo_box($role_id = "") {
    $html = "";
    $html .= "<select name='role_id' id='role_combo_box'>";

    $roles = get_all_rolles();
    $strSelect = "";
    $html .= '<option value="">---</option>';
    foreach ($roles as $key => $val) {
        $role = $roles[$key];
        if ($role_id == $role->id) {
            $strSelect = "selected";
        } else {
            $strSelect = "";
        }

        $html .= '<option value="' . $role->id . '"  ' . $strSelect . '> ' . $role->name . ' </option>';
    }
    $html .= "</select>";
    return $html;
}

function search_team_from_name($name, $creator_id) {
    global $DB;
    $params = array();
    $sql = 'SELECT * FROM groups where userid=' . $creator_id . ' and ';
    $sql .= $DB->sql_like('name', ':name', false);
    $params['name'] = '%' . $name . '%';
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function search_user_from_name($username) {
    global $DB;
    $params = array();
    $sql = 'SELECT * FROM user where 1=1 and ';
    $sql .= $DB->sql_like('username', ':username', false);
    $params['username'] = '%' . $username . '%';
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function update_team($team_id, $name,$gvcn,$email,$phone,$namhoc, $description) {
    global $DB;
    $DB->set_field('groups', 'name', trim($name), array('id' => $team_id));
    $DB->set_field('groups', 'gvcn', trim($gvcn), array('id' => $team_id));
    $DB->set_field('groups', 'email', trim($email), array('id' => $team_id));
    $DB->set_field('groups', 'phone', trim($phone), array('id' => $team_id));
    // $DB->set_field('groups', 'id_truong', $school, array('id' => $team_id));
    // $DB->set_field('groups', 'id_khoi', $khoi, array('id' => $team_id));
    $DB->set_field('groups', 'id_namhoc', $namhoc, array('id' => $team_id));
    $DB->set_field('groups', 'description', $description, array('id' => $team_id));
    // $DB->set_field('groups', 'timemodified', time(), array('id' => $team_id));
}

function update_team_year($team_id,$schoolyearid){
    global $DB;
    $sql ="UPDATE groups_year SET schoolyearid=".$schoolyearid ." WHERE groupid=".$team_id;
    return $DB->execute($sql);
}

function get_unassigned_courses($team_id) {
    global $DB;
    $sql = 'SELECT id FROM course
        WHERE id not in (select course_id from group_course where group_id=:team_id) ';
    $params['team_id'] = $team_id;
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function get_courses_from_team($team_id) {
    global $DB;

    $sql = 'SELECT course_id FROM group_course
        WHERE id = :team_id';
    $params['team_id'] = $team_id;

    $result = $DB->get_records_sql($sql, $params);

    return $result;
}

function get_user_from_username($user_name) {
    global $DB;
    $user = $DB->get_record('user', array(
        'username' => $user_name
    ));

    return $user;
}



function is_parent_team($team_id) {
    global $DB;
    $group = $DB->get_record('groups', array(
        'id' => $team_id
    ));

    if ($group->parent == 0) {
        return 1;
    }
    return 0;
}

function get_sub_teams($team_id) {
    global $DB;
    $sql = 'SELECT * from groups where parent = ' . $team_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function remove_person_from_course($person_id, $course_id) {
    global $DB, $CFG;

    require_once($CFG->dirroot . '/lib/enrollib.php');
    $enrol = enrol_get_plugin('manual');
    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);

    $enrol->unenrol_user($instance, $person_id);
}

function get_user_enroll_of_course($user_id) {
    global $DB;
    $sql = "SELECT user2.id,
        user2.sex,user2.code,user2.name_parent,
        user2.firstname AS Firstname,
        user2.lastname AS Lastname,
        user2.email AS Email,
        user2.city AS City,
        user2.birthday AS Birthday,
        course.id as id_gr,
        course.fullname AS Course
        ,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
        ,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
        FROM course AS course 
        JOIN enrol AS en ON en.courseid = course.id
        JOIN user_enrolments AS ue ON ue.enrolid = en.id
        JOIN user AS user2 ON ue.userid = user2.id WHERE user2.id=:user_id";
        $params['user_id'] = $user_id;
    //$params['creator_id'] = $creator_id;

    $result = $DB->get_record_sql($sql, $params);


    return $result;
}
function remove_team_member($user_id, $team_id,$course_id="") {
    global $DB;
    $conditions = array(
        'userid' => $user_id,
        'groupid' => $team_id
    );

    $DB->delete_records('groups_members', $conditions);
    if($course_id != ""){
        remove_person_from_course($user_id,$course_id);
    }
}
function remove_student_from_class_cre_by_trang($userid,$groupid,$schoolyear,$block_student,$school){
    global $DB;
    $DB->delete_records('history_student', array(
        'iduser'=>$userid,
        'idschool'=>$school,
        'idblock'=>$block_student,
        'idclass'=>$groupid,
        'idschoolyear'=>$schoolyear,
        )
    ); 
}



function remove_course_team($course_id, $team_id) {
    global $DB;
    $conditions = array(
        'course_id' => $course_id,
        'group_id' => $team_id
    );

    $DB->delete_records('group_course', $conditions);
}

function assign_course_to_team($course_id, $team_id) {
    global $DB;
    $group_course = new stdClass();
    $group_course->course_id = $course_id;
    $group_course->group_id = $team_id;
    $DB->insert_record('group_course', $group_course);
}

function get_course_from_id($course_id) {
    global $DB;
    $course = $DB->get_record('course', array(
        'id' => $course_id
    ));

    return $course;
}

function get_user_from_id($user_id) {
    global $DB;
    $user = $DB->get_record('user', array(
        'id' => $user_id
    ));

    return $user;
}

function insert_member_to_group($user_id, $group_id) {
    global $DB;
    $groups_members = new stdClass();
    $groups_members->groupid = $group_id;
    $groups_members->userid = $user_id;
    $groups_members->timeadded = time();
    $DB->insert_record('groups_members', $groups_members);
}

function assign_member_with_role($user_id, $role_id, $context_id, $modifier_id) {
    global $DB;
    $role_assignments = new stdClass();
    $role_assignments->roleid = $role_id;
    $role_assignments->contextid = $context_id;
    $role_assignments->userid = $user_id;
    $role_assignments->timemodified = time();
    $role_assignments->modifierid = $modifier_id;

    $DB->insert_record('role_assignments', $role_assignments);
}

function insert_new_user($name, $firstName, $lastName, $email, $team_id, $role_id = "") {
    global $DB, $USER;

    $user = new stdClass();
    //$user->id = -1;
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->deleted = 0;
    $user->timecreated = time();
    $password = randPass(8);
    $user->password = hash_internal_user_password($password);
    $user->username = $name;
    $user->mnethostid = 1;
    $user->firstname = $firstName;
    $user->lastname = $lastName;
    $user->email = $email;
    $user->parent = $USER->id;
    $lastinsertid = $DB->insert_record('user', $user);
    $context = get_context_instance(CONTEXT_USER, $lastinsertid);
    //var_dump($context);
    $context_id = $context->id;
    //insert into groups_members
    insert_member_to_group($lastinsertid, $team_id);

    assign_member_with_role($lastinsertid, $role_id, $context_id, $USER->id);
    return $lastinsertid;
}

include ('../lib/phpmailer/class.phpmailer.php');

function sendEmail($to, $from, $from_name, $subject, $body) {

    global $CFG;
    include($CFG->dirroot . '/teams/config.php');

    global $error;

    $mail = new PHPMailer(); // create a new object

    $mail->IsSMTP(); // enable SMTP
    //$mail->IsHTML=true;
    $mail->IsHTML(true);
    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 465;
    $mail->Username = 'tung@thienhoang.com.vn';
    $mail->Password = 'T12345678';
    $mail->SetFrom($from, $from_name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AddAddress($to);
    $mail->CharSet = "utf-8";

    if (!$mail->Send()) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return false;
    } else {
        $error = 'Message sent!';
        return true;
    }
}

function update_user_password($user_id) {
    global $DB;
    $passwordNoEncode = randPass(8);
    $password = hash_internal_user_password($passwordNoEncode);
    if ($DB->set_field('user', 'password', $password, array('id' => $user_id))) {
        return $passwordNoEncode;
    }
}


// Danh sách học sinh chưa được gán vào lớp học
function get_users_from_parent_second($creator_id) {
    global $DB, $USER;

    $sql = "SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid`
    WHERE `role_assignments`.`roleid`= 5 AND user.del=0";
    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_info_giaovien($id){
    global $DB;
        $giaovien = $DB->get_record('user', array(
            'id' => $id
        ));
        return $giaovien;
    
}

function get_list_teacher()
{
    global $DB;
    $sql = "SELECT user.*  FROM user INNER JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=8";
    $rows = $DB->get_records_sql($sql);
    return $rows;
}
function get_list_teachertutors()
{
    global $DB;
    $sql = "SELECT user.*  FROM user INNER JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=10";
    $rows = $DB->get_records_sql($sql);
    return $rows;
}

function get_day_group_class($team_id){
    global $DB;
    $sql='SELECT schedule_info.`day` FROM `course` JOIN `schedule` ON `course`.`id`=`schedule`.`courseid` JOIN schedule_info ON `schedule`.id = `schedule_info`.`scheduleid` WHERE `course`.`id`='.$team_id;
    $data = $DB->get_records_sql($sql);
    return $data;
}

function get_info_truong($id){
    global $DB;
        $truong = $DB->get_record('schools', array(
            'id' => $id
        ));
        return $truong;
    
}
function get_info_namhoc($id){
    global $DB;
        $namhoc = $DB->get_record('school_year', array(
            'id' => $id
        ));
        return $namhoc;
    
}


// Load danh sách nhóm lớp theo lớp học
function get_list_groups_class($group_id,$page,$perpage=10,$params=""){
    global $DB;
    $sql = 'SELECT course.*, groups.name FROM course JOIN group_course ON course.id = group_course.course_id JOIN groups ON groups.id=group_course.group_id WHERE group_course.group_id ='. $group_id;
    if($params !=''){
        $sql .= " AND fullname LIKE '%".$params."%'";
    }
    if(isset($page)){
        $sql .= " LIMIT ".$page*$perpage .",".$perpage;
    }
    return $DB->get_records_sql($sql);
}

// Load học sinh theo lớp học
function get_members_in_class($team_id){
    global $DB, $USER;
    $date = time();
    $sql = "SELECT user.*,concat_ws(' ',`user`.`lastname`,`user`.`firstname`) as fullname
            FROM 
                groups_members
                JOIN user ON user.id = groups_members.userid
            WHERE 
                groups_members.groupid={$team_id}
                AND (user.time_end_int=0 OR user.time_end_int > {$date})
                AND user.del=0 ORDER BY user.firstname ASC";


    $result = $DB->get_records_sql($sql);
    return $result;
}

function get_info_course_of_group($team_id){
    global $DB;
    $sql = 'SELECT groups.id as id_lop,groups.name,groups.id_khoi,groups.id_truong,groups.id_namhoc FROM groups JOIN group_course ON groups.id=group_course.group_id WHERE group_course.group_id ='. $team_id;
    return $DB->get_record_sql($sql);
}

function get_info_khoi($id){
    global $DB;
        $khoi = $DB->get_record('block_student', array(
            'id' => $id
        ));
        return $khoi;
    
}
function get_status_khoi($schoolid,$status){
    global $DB;
    $sql = "SELECT block_student.* FROM block_student JOIN schools ON schools.id = block_student.schoolid
            WHERE schoolid={$schoolid} AND block_student.`status`={$status}";
    return $DB->get_record_sql($sql);
    
}
// Lớp học


function get_list_groups($page,$perpage=10,$params="")
{
    global $DB;
    $sql = "SELECT groups.*, groups_year.schoolyearid FROM groups JOIN groups_year ON groups.id=groups_year.groupid";
    
    if($params !=''){
        $sql .= $params;
    }
    $sql.=" ORDER BY groups.name ASC ";
    if(isset($page)){
        $sql .= "  LIMIT ".$page*$perpage .",".$perpage;
    }
    
    $rows = $DB->get_records_sql($sql);
    return $rows;
}
function get_list_groups2($page,$perpage,$classname=null,$homeroomteacher=null,$schoolid=null,$school_year=null,$phone=null,$email=null){
    global $DB;
    if ($page>1) {
      $start=(($page-1)*$perpage);
    }else{
      $start=0;
    }
    $sql = "SELECT `groups`.*,`school_year`.`sy_start`,`school_year`.`sy_end`, `schools`.`name` as schoolname 
    FROM `groups` 
    JOIN `school_year` ON `groups`.`id_namhoc`= `school_year`.`id`
    JOIN `schools` ON `schools`.`id`=`groups`.`id_truong`
    WHERE `schools`.`del`=0
    "; 
    $sql_count = " SELECT COUNT(DISTINCT `groups`.`id` )  
    FROM `groups` 
    JOIN `school_year` ON `groups`.`id_namhoc`= `school_year`.`id` 
    JOIN `schools` ON `schools`.`id`=`groups`.`id_truong`
    WHERE `schools`.`del`=0
    ";
    
    if(!empty($classname)){
        $sql.= " AND `groups`.`name` LIKE '%{$classname}%'" ;
        $sql_count.= " AND `groups`.`name` LIKE '%{$classname}%'" ;
    }
    if(!empty($homeroomteacher)){
        $sql.= " AND `groups`.`gvcn` LIKE '%{$homeroomteacher}%'" ;
        $sql_count.= " AND `groups`.`gvcn` LIKE '%{$homeroomteacher}%'" ;
    }
    if(!empty($schoolid)){
        $sql.= " AND `groups`.`id_truong` ={$schoolid}" ;
        $sql_count.= " AND `groups`.`id_truong` ={$schoolid}" ;
    }
    if(!empty($school_year)){
        $sql.= " AND `groups`.`id_namhoc` ={$school_year}" ;
        $sql_count.= " AND `groups`.`id_namhoc` ={$school_year}" ;
    }
    if(!empty($phone)){
        $sql.= " AND `groups`.`phone` LIKE '%{$phone}%'" ;
        $sql_count.= " AND `groups`.`phone` LIKE '%{$phone}%'" ;
    }
    if(!empty($email)){
        $sql.= " AND `groups`.`email` LIKE '%{$email}%'" ;
        $sql_count.= " AND `groups`.`email` LIKE '%{$email}%'" ;
    }
    $sql.=" ORDER BY `groups`.`name` ASC  LIMIT {$start} , {$perpage} ";

    $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);

    $total_page=ceil((int)$total_row / (int)$perpage);
    $data = $DB->get_records_sql($sql);
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
    
}
function get_teams_from_class($course_id){
    global $DB;
    $sql = 'SELECT group_id FROM group_course
        WHERE course_id = :course_id';
    $params['course_id'] = $course_id;
    $result = $DB->get_records_sql($sql, $params);
        return $result;
        
}
function get_class_from_user_id($user_id, $course_id){
    global $DB;
    $sql2 = "SELECT user_id, owner_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id OR mua.user_id = :user_id";
    $params2['owner_id'] = $user_id;
    $params2['user_id'] = $user_id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $user_id;
    
    //   Get teams from course id
    $teams_in_courses = get_teams_from_class($course_id);
    $teamId = "";
    foreach ($teams_in_courses as $teams_in_course) {
        $teamId .= $teams_in_course->group_id . ",";
    }
    $teamId .= "0";
    
    $field = "g.id, g.name, g.id_truong, g.description";
    $sql = "SELECT ".$field." FROM {groups} g WHERE (g.userid in (".$id_list.") OR g.courseid = :courseid) OR g.id in (".$teamId.")";
    $params['courseid'] = $course_id;
    $result = $DB->get_records_sql($sql,$params);
    return $result;
}

function get_members_gr($course_id) {
    global $DB;
    $sql = "SELECT user2.id,
    user2.sex,user2.code,user2.name_parent,
    user2.firstname AS Firstname,
    user2.lastname AS Lastname,
    user2.email AS Email,
    user2.city AS City,
    user2.birthday AS Birthday,
    course.fullname AS Course
    ,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
    ,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
     
    FROM course AS course 
    JOIN enrol AS en ON en.courseid = course.id
    JOIN user_enrolments AS ue ON ue.enrolid = en.id
    JOIN user AS user2 ON ue.userid = user2.id
    JOIN role_assignments ON user2.id = role_assignments.userid 
    WHERE NOT user2.id=2 AND course.id=:course_id AND role_assignments.roleid=5";
    $params['course_id'] = $course_id;

    $result = $DB->get_records_sql($sql, $params);

    return $result;
}


// import student
function get_info_team($teamid)
{
    global $DB;
    $sql = "SELECT
        groups.*,
        schools.`name` AS tentruong,
        block_student.`name` AS tenkhoi 
    FROM
        groups
        JOIN schools ON schools.id = groups.id_truong
        JOIN block_student ON block_student.id = groups.id_khoi 
    WHERE
        groups.id = ".$teamid;
    return $DB->get_record_sql($sql);
}

function add_student($firstname,$lastname,$username,$password,$birthday,$sex,$input_score,$code,$address,$name_parent,$email,$phone1) {
  global $DB, $USER;
  $record = new stdClass();
 
  $record->firstname    =  trim($firstname) ;
  $record->lastname     =  trim($lastname);
  $record->birthday     =  trim($birthday) ;
  $record->email        =  trim($email);
  $record->phone1       =  trim($phone1);

  $record->username = trim($username);
  $password = hash_internal_user_password($password);
  $record->password = $password;

  $record->confirmed = 1;
  $record->mnethostid = 1;

  $record->address = trim($address);
  $record->parent = 0;
  $record->input_score = $input_score;
  $record->sex = $sex;
  $record->name_parent = trim($name_parent);

  $record->code = trim($code);
  $record->del = 0;

  $new_user_id = $DB->insert_record('user', $record);
  return $new_user_id;
}

/**
 * Groups UP
 */
function get_user_to_groups($idUser)
{
    global $DB;
    $sql ="SELECT 
                course.fullname,
                course.id
                
            FROM user
                JOIN user_enrolments ON user_enrolments.userid = user.id
                JOIN enrol ON enrol.id = user_enrolments.enrolid
                JOIN course ON enrol.courseid = course.id
                JOIN role_assignments ON role_assignments.userid = user.id 
            WHERE
                user.del=0
                AND role_assignments.roleid = 5
                AND user.id=".$idUser;
    return $DB->get_record_sql($sql);
}

function insert_history_student($idUser,$idSchool,$idBlock,$idClass,$idGroups="",$idYear)
{
    global $DB;
    $data = new stdClass();

    $data->iduser   = $idUser;
    $data->idschool = $idSchool;
    $data->idblock  = $idBlock;
    $data->idclass  = $idClass;
    $data->idgroups = $idGroups;
    $data->idschoolyear = $idYear;

    $lastinsertid = $DB->insert_record('history_student', $data);
    return $lastinsertid;

}

function update_student_classup($userid,$groupid)
{
    global $DB;
    $sql ="UPDATE groups_members SET groupid=".$groupid." WHERE userid=".$userid;
    return $DB->execute($sql);
}

function update_course_classup($groupid_old,$groupid_new)
{
    global $DB;
    $sql ="UPDATE group_course SET group_id=".$groupid_new." WHERE group_id=".$groupid_old;
    return $DB->execute($sql);
}
function insert_schools_year($start,$end)
{
    global $DB;
    $data = new stdClass();
    $data->sy_start = $start;
    $data->sy_end = $end;

    $lastinsertid = $DB->insert_record('school_year', $data);
    return $lastinsertid;

}

function upadte_class_year($idclass,$idyear)
{
    global $DB;
    $sql = "UPDATE groups SET id_namhoc=".$idyear." WHERE id=".$idclass;
    return $DB->execute($sql);
}

function get_list_groups_class_classup($group_id){
    global $DB;
    $sql = 'SELECT course.*, groups.name FROM course JOIN group_course ON course.id = group_course.course_id JOIN groups ON groups.id=group_course.group_id WHERE group_course.group_id ='. $group_id;
    return $DB->get_records_sql($sql);
}

function update_name_course_classup($fullname,$groupid,$courseid)
{
    global $DB;
    $sql = "UPDATE course
            JOIN group_course ON course.id = group_course.course_id 
            JOIN groups ON groups.id=group_course.group_id
            SET fullname = '{$fullname}',shortname= '{$fullname}'
            WHERE group_course.group_id ={$groupid} AND course.id ={$courseid}";       
    return $DB->execute($sql);
}
function insert_history_namecourse($courseid,$name_course,$groupid, $school_year,$sotiet)
{
    global $DB;
    $data = new stdClass();
    $data->courseid    = $courseid;
    $data->course_name    = $name_course;
    $data->groupid    = $groupid;
    $data->school_year_id = $school_year;
    $data->sotiet = $sotiet;
    $lastinsertid = $DB->insert_record('history_course_name',$data); 
    return $lastinsertid;
}


/**
 * Paginantion
 */
function paginate($sum_record, $page_number, $parpage, $url) {
    $num = 1; //số lượng số trang bên cạnh trang lựa chọn
    $active = '';
    $totalcount = ceil($sum_record/$parpage);
    
    if ($totalcount <=1) {
        return 1;
    }

    echo "<ul class='pagination'>";
    if ($totalcount > 6) {
        if ($page_number>0) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number-1).'">Previous</a></li>';
        }
        for($i=0; $i<$totalcount; $i++) {
            if($i==$page_number){
                $active=' active';
            }else{
                $active ='';
            }
            $link = '<li class="page-item'.$active.'"><a class="page-link" href="'.$url.'page='.$i.'">'.($i+1).'</a></li>';

            if ($i<=$num) {
                echo $link;
                if ($i==$num && $page_number==0) {
                    echo "<li class='page_item'><a class='page-link' href='#'>...</li>";
                }
            }elseif($i>=($page_number-$num) && $i<=($page_number+$num)){
                if ($i==($page_number-$num)&&$i>($num+1)) {
                    echo "<li class='page_item'><a class='page-link' href='#'>...</li>";
                }
                if ($i<($totalcount-$num)) {
                    echo $link;
                    if ($i==($page_number+$num)&&$i<($totalcount-$num-1)) {
                        echo "<li class='page_item'><a class='page-link' href='#'>...</li>";
                    }
                }
            }

            if ($i>=($totalcount-$num)) {
                echo $link;
            }

        }
        if ($page_number<$totalcount-1) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number+1).'">Next</a></li>';
        }
    }else{
        if ($page_number>0) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number-1).'">Previous</a></li>';
        }
        for($i=0; $i<$totalcount; $i++) {
            if($i==$page_number){
                $active=' active';
            }else{
                $active ='';
            }
            $link = '<li class="page-item'.$active.'"><a class="page-link" href="'.$url.'page='.$i.'">'.($i+1).'</a></li>';
            echo $link;
        }
        if ($page_number<$totalcount-1) {
            echo '<li class="page-item"><a class="page-link" href="'.$url.'page='.($page_number+1).'">Next</a></li>';
        }
    }

    echo "</ul>";

}

//LOG action
function add_logs($module, $action, $url, $name)
{
    global $DB, $CFG, $USER;

    if ($user) {
        $userid = $user;
    } else {
        if (session_is_loggedinas()) {  // Don't log
            return;
        }
        $userid = empty($USER->id) ? '0' : $USER->id;
    }

    if (isset($CFG->logguests) and !$CFG->logguests) {
        if (!$userid or isguestuser($userid)) {
            return;
        }
    }

    $REMOTE_ADDR = getremoteaddr();

    $timenow = time();

    if (!empty($url)) { // could break doing html_entity_decode on an empty var.
        $url = html_entity_decode($url);
    } else {
        $url = '';
    }

    $log = array('time'=>$timenow, 'userid'=>$userid, 'ip'=>$REMOTE_ADDR, 'module'=>$module,
                  'action'=>$action, 'url'=>$url, 'name'=>$name);

    try {
        $DB->insert_record_raw('log', $log, false);
    } catch (dml_write_exception $e) {
        debugging('Error: Could not insert a new entry to the Moodle log', DEBUG_ALL);
        // MDL-11893, alert $CFG->supportemail if insert into log failed
        if ($CFG->supportemail and empty($CFG->noemailever)) {
            // email_to_user is not usable because email_to_user tries to write to the logs table,
            // and this will get caught in an infinite loop, if disk is full
            $site = get_site();
            $subject = 'Insert into log failed at your moodle site '.$site->fullname;
            $message = "Insert into log table failed at ". date('l dS \of F Y h:i:s A') .".\n It is possible that your disk is full.\n\n";
            $message .= "The failed query parameters are:\n\n" . var_export($log, true);

            $lasttime = get_config('admin', 'lastloginserterrormail');
            if(empty($lasttime) || time() - $lasttime > 60*60*24) { // limit to 1 email per day
                //using email directly rather than messaging as they may not be able to log in to access a message
                mail($CFG->supportemail, $subject, $message);
                set_config('lastloginserterrormail', time(), 'admin');
            }
        }
    }
}


function view_logs($select, $params)
{
    global $CFG;

    $page = optional_param('page', '0', PARAM_INT);
    $perpage = optional_param('perpage', '20', PARAM_INT);

    $totalcount = 0;
    $logs = manage_get_logs($select, $params, $order = 'l.time DESC', $page*$perpage, $perpage, $totalcount);
    foreach ($logs as $key => $log) {
        $user_info = get_gv($log->userid);

        echo "<div class='row mb-3'>";
          echo '<div class="col-md-1">';
            echo '<span class="badge badge-primary">'.$log->module.'</span>';
          echo '</div><div class="col-md-8">';
            echo "<a href='".$CFG->wwwroot."/manage/people/profile.php?id=".$log->userid."'>".$user_info->firstname . " " . $user_info->lastname . "</a>";
            switch ($log->action) {
              case 'view':
                echo ' đã xem ';
                break;
              case 'new':
                echo ' thêm mới ';
                break;
              case 'update':
                echo ' cập nhật  ';
                break;
              case 'delete':
                echo ' xóa ';
                break;
              case 'login':
                echo ' đăng nhập ';
                break;
              case 'logout':
                echo ' đăng xuất ';
                break;
            }
            if ($log->action=='delete') {
                echo "<span style='color:#888; text-decoration:line-through;'>";
                echo $log->name;
                echo "</span>";
            }else{
                echo "<a href='".$CFG->wwwroot.$log->url."'>";
                echo $log->name;
                echo "</a>";
            }
          echo '</div><div class="col-md-3"><span class="float-right">';
            echo time_ago($log->time, '')."</span></div>";
        echo '</div>';
    }
    echo "<hr>";
    paginate($totalcount, $page, $perpage, 'index.php?');
}
//End LOG

// xoa lop
function delete_team($team_id) {

    global $DB;
    // xóa học sinh khỏi lớp
    $list_student_del=get_members_in_class($team_id);
    if($list_student_del){
        foreach ($list_student_del as $del) {
            # code...
            remove_student_from_class_cre_by_trang($del->id);
        }
    }

    $DB->delete_records_select('groups', " id = $team_id ");
    $DB->delete_records_select('group_course', " group_id = $team_id ");
    $DB->delete_records_select('groups_members', " groupid = $team_id ");
    $DB->delete_records_select('groups', " parent = $team_id ");
    $DB->delete_records_select('groups_year', " groupid = $team_id ");
}
// check trung tên lop trong 1 khối , 1 năm học
function kiem_tra_ten_lop_theo_khoi_nam_hoc($block_student,$schoolyear,$name){
    global $DB;
    $id=$DB->get_record('groups', array(
      'id_khoi' => $block_student,
      'id_namhoc' => $schoolyear,
      'name' => $name,
    )); 
    if(!empty($id)){
        return 1;
    }else{
        return 2;
    }
}
function insert_history_student_in_team($idUser,$idSchool,$idBlock,$idClass,$idGroups="",$idYear="")
{
    global $DB;
    $data = new stdClass();

    $data->iduser   = $idUser;
    $data->idschool = $idSchool;
    $data->idblock  = $idBlock;
    $data->idclass  = $idClass;
    $data->idgroups = $idGroups;
    $data->idschoolyear = $idYear;

    $lastinsertid = $DB->insert_record('history_student', $data);
    return $lastinsertid;

}
// trang edit
// cac function phuc vu cho viec hien thi lop, nhom lop cua lop da tien hanh len lop
function danh_sach_hs_cu_cua_lop_da_tien_hanh_len_lop($groupid,$schoolyear){
    global $DB;
    $date = time();
    $sql=" SELECT DISTINCT `user`.`id`,concat_ws(' ',`user`.`lastname`,`user`.`firstname`) as fullname ,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex`
        FROM `user`
        JOIN `history_student` ON `user`.`id` = `history_student`.`iduser`
        WHERE `history_student`.`idclass` ={$groupid} 
        AND `history_student`.`idschoolyear`={$schoolyear}
        AND `user`.`del`=0
        AND (`user`.`time_end_int`=0 OR `user`.`time_end_int` > {$date})
    ";
    $data=$DB->get_records_sql($sql);
    return $data;
}
function tong_hoc_sinh_cua_lop_da_tien_hanh_len_lop($groupid,$schoolyear){
    global $DB;
    $date = time();
    $sql=" SELECT COUNT(DISTINCT `user`.`id`)
            FROM `user`
            JOIN `history_student` ON `user`.`id` = `history_student`.`iduser`
            WHERE `history_student`.`idclass` ={$groupid} 
            AND `history_student`.`idschoolyear`={$schoolyear}
            AND `user`.`del`=0
            AND (`user`.`time_end_int`=0 OR `user`.`time_end_int` > {$date})
    ";
    $data = $DB->get_field_sql($sql, null,$strictness=IGNORE_MISSING);
    return $data;
}
function dem_so_nhom_lop_cua_lop_da_tien_hanh_len_lop($groupid,$schoolyear){
    global $DB;
    $sql=" SELECT COUNT(DISTINCT `course`.`id`)
            FROM `course`
            JOIN `history_course_name` ON `history_course_name`.`courseid`=`course`.`id`
            WHERE `history_course_name`.`groupid`= {$groupid} 
            AND `history_course_name`.`school_year_id`={$schoolyear}
    ";
    $data = $DB->get_field_sql($sql, null,$strictness=IGNORE_MISSING);
    return $data;
}
function dem_hs_cua_lop_khi_chua_tien_hanh_chuyen_lop($groupid){
    global $DB, $USER;
    $date = time();
    $sql = "SELECT COUNT(DISTINCT `user`.`id`)
            FROM 
                groups_members
                JOIN user ON user.id = groups_members.userid
            WHERE 
                groups_members.groupid={$groupid}
                AND (user.time_end_int=0 OR user.time_end_int > {$date})
                AND user.del=0 ";


    $data = $DB->get_field_sql($sql, null,$strictness=IGNORE_MISSING);
    return $data;
}
function dem_nhom_cua_lop_khi_chua_tien_hanh_chuyen_lop($groupid){
    global $DB;
    $sql = 'SELECT COUNT(DISTINCT `course`.`id`) FROM course JOIN group_course ON course.id = group_course.course_id JOIN groups ON groups.id=group_course.group_id WHERE group_course.group_id ='. $groupid;
    $data = $DB->get_field_sql($sql, null,$strictness=IGNORE_MISSING);
    return $data;
}
function lay_ds_nhom_khi_chua_tien_hanh_len_lop($groupid,$schoolyear){
    global $DB;
    $sql=" SELECT DISTINCT `course`.`id`,`history_course_name`.`course_name` as fullname, `course`.`id`
            FROM `course`
            JOIN `history_course_name` ON `history_course_name`.`courseid`=`course`.`id`
            WHERE `history_course_name`.`groupid`= {$groupid} 
            AND `history_course_name`.`school_year_id`={$schoolyear}
    ";
    $data=$DB->get_records_sql($sql);
    return $data;
}

function get_info_class($id)
{
    global $DB;
    $class = $DB->get_record('groups', array(
        'id' => $id
    ));
    return $class;
}
?>
