<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');
global $DB;
require_login(0, false);
$team_id = optional_param('team_id', 0, PARAM_TEXT);
$team = get_team_from_id($team_id);
// trang edit
$trangthailop=$DB->get_record('groups', array(
        'id' =>$team_id
    ),
    $fields='id,status', $strictness=IGNORE_MISSING
);
$PAGE->set_title(get_string('list_student').': '. $team->name.'('. $team->sy_start.'-'.$team->sy_end.')'. ' - '. $team->tentruong);
$PAGE->set_heading(get_string('list_student').': '. $team->name.'('. $team->sy_start.'-'.$team->sy_end.')'. ' - '. $team->tentruong);

//trang edit
switch ($trangthailop->status) {
    case 0:
       $list_members = get_members_in_class($team_id);
    break;
    case 1:
       $list_members=danh_sach_hs_cu_cua_lop_da_tien_hanh_len_lop($team_id,$team->id_namhoc);
    break;
   
}
// $list_members = get_members_in_class($team_id);

$teams_of_courses_arr = array();
foreach ($teams_in_course as $key => $val) {
    $teams_of_courses_arr[] = $key;
}

add_logs('class','view','/teams/view_team.php?team_id='.$team_id,$team->name);

echo $OUTPUT->header();
global $CFG,$DB;
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='groups';
$check_xoacon=check_chuc_nang_xoachucnangcong($roleid,$moodle);
$name1='list_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>

<div class="row">        
    <div class="col-md-9">
        <div class="card-box">
            <div class="table-rep-plugin">

                <div class="row">
                    <div class="form-group col-md-12">
                        <input id="foo-search" type="text" placeholder="<?php print_r(get_string('search')) ?>..." class="form-control" autocomplete="on">
                    </div>
                </div>

                <div class="table-responsive">
                    <table id="table-foo" class="table table-striped table-bordered" data-page-size="20">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th><?php print_r(get_string('codestudent')) ?></th>
                                <th><?php print_r(get_string('name')) ?></th>
                                <th><?php print_r(get_string('sex')) ?></th>
                                <th><?php print_r(get_string('birthday')) ?></th>
                              
                                <th><?php print_r(get_string('parent')) ?></th>
                                <th><?php print_r(get_string('phone')) ?></th>
                                <th><?php print_r(get_string('time_start')) ?></th>
                                <th><?php print_r(get_string('time_end')) ?></th>
                                <?php
                                switch ($trangthailop->status) {
                                    case 0:
                                        if(!empty($check_xoacon)){
                                            ?>
                                            <th class="text-right"><?php print_r(get_string('action')) ?></th>
                                            <?php 
                                        } 
                                    break;
                                } 
                                    
                                 ?>
                                
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i=1;
                            foreach ($list_members as $key => $val) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $i; $i++ ?></td>
                                <td><?php echo $val->code ?></td>
                                <td>
                                    <div class="title">
                                        <a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $val->id ?>">
                                            <?php echo $val->fullname; ?> 
                                        </a>
                                        <a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $val->id ?>" class="pop-profile"></a>
                                    </div>                                  
                                    <div class="tip">
                                        <?php echo $val->code ?>
                                    </div>
                                </td>
                                <td class="text-center"><?php if($val->sex==1){
                                        echo "Nữ";
                                    }
                                    if($val->sex==2){
                                        echo "Nam";
                                    }?>
                                </td>
                                <td><?php echo $val->birthday ?></td>
                                <td><?php echo $val->name_parent ?></td>
                                <td><?php echo $val->phone1 ?></td>
                                <td><?php echo $val->time_start ?></td>
                                <td><?php echo $val->time_end ?></td>
                                
                                 <?php 
                                     switch ($trangthailop->status) {
                                        case 0:
                                            if(!empty($check_xoacon)){
                                                ?>
                                               <td class="text-right">
                                                    <?php   
                                                        $get_us=get_user_enroll_of_course($val->id);
                                                        if(!empty($get_us)){
                                                    ?>
                                                    <a class="btn btn-danger btn-sm tooltip-animation" title='Xóa học sinh' onclick="return Confirm('Xóa học sinh','Bạn có muốn xóa học sinh khỏi lớp học','Yes','Cancel','<?php echo $CFG->wwwroot ?>/teams/team.php?action=delete_team_member&team_id=<?php echo $team_id ?>&user_id=<?php echo $val->id?>&course_id=<?php echo $get_us->id_gr ?>&confirm=yes')" href='#'>
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    </a>
                                                <?php }else{?>
                                                    <a class="btn btn-danger btn-sm tooltip-animation" title='Xóa học sinh' onclick="return Confirm('Xóa học sinh','Bạn có muốn xóa học sinh khỏi lớp học','Yes','Cancel','<?php echo $CFG->wwwroot ?>/teams/team.php?action=delete_team_member&team_id=<?php echo $team_id ?>&user_id=<?php echo $val->id?>&confirm=yes')" href='#'>
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    </a>
                                                <?php } ?>
                                                </td>
                                                <?php 
                                            }
                                        break;
                                    } 
                                 ?>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>

                        <tfoot>
                            <tr class="active">
                                <td colspan="10">
                                    <div class="float-right">
                                        <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10 m-b-0"></ul>
                                    </div>
                                    <div class="float-left">
                                        <a href="<?php echo $CFG->wwwroot ?>/teams/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                                    </div>
                                </td>
                            </tr>
                            
                            

                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- <div class="tip center-padded"><?php echo $count." ". get_string('peoplefund') ?></div> -->
        </div>
    </div>
    <?php 
        switch ($trangthailop->status) {
            case 0:
            ?>
            <div class="col-md-3">
                <div class="card-box">
                    <div class="side-panel team-actions">
                        <div class="action-buttons">
                            <ul class=" metismenu list-unstyled">
                                <?php 
                                $checkpopup = check_chuc_nang_pop_up($roleid,$moodle);
                                if(!empty($checkpopup)){
                                    echo'<li class="form-group">
                                    <a href="#" data-toggle="modal" data-target="#overlay" class="btn btn-success btn-block btn-sm">
                                        <i class="fa fa-plus-square"></i>'.get_string('assignstudents').'
                                    </a>
                                </li>';
                                }
                                ?>
                                
                                <!-- <li class="form-group">
                                    <a href="<?php echo $CFG->wwwroot ?>/teams/add_member_to_team.php?team_id=<?php echo $team_id; ?>" class="btn btn-block btn-sm btn-light">Thêm học sinh vào lớp</a>
                                </li> -->
                                <?php hien_thi_chuc_nang_gan_theo_tung_moodle_by_cua($moodle,$roleid,$team_id); ?>
                                <!-- <li class="form-group">
                                    <a href="<?php echo $CFG->wwwroot ?>/course/classup/index.php?team_id=<?php echo $team_id ?>" class="btn btn-block btn-sm btn-light"><?php print_r(get_string('toclass')) ?></a>
                                </li>
                                <li class="form-group">
                                    <a href="<?php echo $CFG->wwwroot ?>/teams/import_member_to_team.php?team_id=<?php echo $team_id; ?>" class="btn btn-block btn-sm btn-light">Import exel</a>
                                </li> -->
                                
                                <!-- <?php 
                                if (is_parent_team($team_id)) {
                                ?>
                                <li><a href="<?php echo $CFG->wwwroot ?>/teams/add_sub_team.php?team_id=<?php echo $team_id; ?>">+ <?php print_r(get_string('addanewteamunder')) ?></a> </li>
                                <?php } ?> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            break;
        }
    ?>
    
</div>





<!-- Modal DS học sinh -->
<div id="overlay" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="assign-search">                                                           
            <form action="<?php echo $CFG->wwwroot?>/teams/team.php" method="post">
                <input type="hidden" name="action" value="assign_role"/>
                <input type="hidden" name="team_id" id="team_id" value="<?php echo $team_id ?>"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Danh sách học sinh</h4>
                    <input type="hidden" value="<?php echo $id ?>" name="course_id" id="course_id" />
                </div>
                <div class="modal-body">
                    <div id="assignResults">
                        <table id="datatable2" class="table table-bordered">
                            <thead>
                                <tr>
                                    <!-- <th><input type="checkbox" name="" id="checkAll"></th> -->
                                    <!-- <th data-priority="1">
                                        <input type="checkbox" value="true" name="AssignAll" id="AssignAll">
                                        <label class="italic" for="AssignAll"><?php print_string('assigntoalluser')?></label>
                                    </th> -->
                                    <th></th>
                                    <th data-priority="1"><?php echo get_string('full_name'); ?></th>
                                    <th data-priority="1"><?php echo get_string('emial_new'); ?></th>
                                    <th data-priority="1"><?php echo get_string('brithday'); ?></th>
                                    <th class="disabled-sorting text-right">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <!-- all học sinh -->
                                <?php 
                                    $list_un_members=get_users_from_parent_second($team_id);
                                    foreach($list_un_members as $key=>$val){
                                ?>
                                <tr>
                                    <?php if(!empty(get_members_in_team($val->id))) { ?>
                                        <td>&nbsp;</td>
                                    <?php }else{ ?>
                                        <td>
                                            <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                                        </td>
                                    <?php } ?>
                                    
                                    <td>
                                        <?php echo $val->firstname ?> <?php echo $val->lastname ?></span>
                                    </td>
                                    <td><?php echo $val->email ?></td>
                                    <td><?php echo $val->birthday ?></td>
                                    <td class="text-right">&nbsp;
                                        <?php if(!empty(get_members_in_team($val->id))) { ?>
                                            <span title="" class="badge badge-success"><span>
                                                    <?php print_r(get_string('alreadyassigned')) .print_r(' cho '). print_r(get_members_in_team($val->id)->name) ?>
                                                </span></span>                       
                                        <?php } else {
                                            ?>
                                            <span title="" class=""><span>
                                                </span></span>                       
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><?php print_r(get_string('assign', 'cohort')) ?></button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<style>
        .pagination > .active > a{
            background: #4bb747 !important;
            border: 1px solid #4bb747 !important;
            color:white !important;
        }
        .pagination > .active >  a:hover{
            background: #218838 !important;
            border: 1px solid #218838 !important;
            color:white !important;
        }
    </style>
<?php
    echo $OUTPUT->footer(); 
?>