<?php

// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

require_once('../config.php');
require_once('../user/lib.php');
require_once('lib.php');
require_once ("config.php");
require_login(0, false);
global $CFG;
if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    print_error('disabled', 'message');
}

$action = optional_param('action', 0, PARAM_TEXT);
$class_name_block = optional_param('class_name_block', 0, PARAM_TEXT);
$name_class = optional_param('name', 0, PARAM_TEXT);
$name = $class_name_block . $name_class;
$gvcn = optional_param('gvcn', '', PARAM_TEXT);
$email = optional_param('email', '', PARAM_TEXT);
$phone = optional_param('phone', '', PARAM_TEXT);
$school = optional_param('schoolid', '', PARAM_TEXT);
$khoi = optional_param('block_student', '', PARAM_TEXT);
$namhoc = optional_param('namhoc', '', PARAM_TEXT);
$description = optional_param('description', 0, PARAM_TEXT);
$team_id = optional_param('team_id', 0, PARAM_TEXT);
$course_id = optional_param('course_id', 0, PARAM_TEXT);
$first_name = optional_param('first_name', 0, PARAM_TEXT);
$last_name = optional_param('last_name', 0, PARAM_TEXT);
$username = optional_param('username', 0, PARAM_TEXT);
$email = optional_param('email', 0, PARAM_TEXT);
$role_id = optional_param('role_id', 0, PARAM_TEXT);
$user_id = optional_param('user_id', 0, PARAM_TEXT);
$confirm = optional_param("confirm", '', PARAM_TEXT);
$add_another = optional_param('AddAnother', 0, PARAM_TEXT);
$to_assign = optional_param('to_assign', 0, PARAM_TEXT);
$inherit_course = optional_param('inherit_course', 0, PARAM_TEXT);
$assign_course_to_subteam = optional_param('assign_course_to_subteam', 0, PARAM_TEXT);
$send_email_notification= optional_param('send_email_notification', 0, PARAM_TEXT);



$redirectPath = $CFG->wwwroot . "/teams/";
if ($action == 'add_new_team') {

    if ($add_another == 1 || $add_another == "1") {
        $redirectPath = $CFG->wwwroot . "/teams/new_team.php";
    }
    $checkten=kiem_tra_ten_lop_theo_khoi_nam_hoc($khoi,$namhoc,$name);
    if($checkten==1){
        ?>
        <script>
          alert('Đã tồn tại tên lớp trong khối thuộc năm học lựa chọn'); 
        </script>
        <?php 
        echo displayJsAlert('', $CFG->wwwroot . "/teams/new_team.php");
    }elseif($checkten==2){
        $id = add_new_team($name, $gvcn,$email,$phone,$school,$khoi,$namhoc, $description);
        if ($id > 0) {
            add_new_team_year($id,$namhoc);
            //echo displayJsAlert('Added new team succesfully', $redirectPath);
            echo displayJsAlert('', $redirectPath);
        }
    }
    // $id = add_new_team($name, $gvcn,$email,$phone,$school,$khoi,$namhoc, $description);
    // if ($id > 0) {
    //     add_new_team_year($id,$namhoc);
    //     //echo displayJsAlert('Added new team succesfully', $redirectPath);
    //     echo displayJsAlert('', $redirectPath);
    // }
}



if ($action == 'add_member_to_team') {
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    $last_insert_id = insert_new_user($username, $first_name, $last_name, $email, $team_id, $role_id);

    if ($last_insert_id > 0) {
        if ($add_another == 1 || $add_another == "1") {
            $redirectPath = $CFG->wwwroot . "/teams/add_member_to_team.php?team_id=$team_id";
        }
        //echo displayJsAlert('Added new member succesfully', $redirectPath);
        echo displayJsAlert('', $redirectPath);
    }
}

if ($action == 'delete_team_member' && $confirm == '') {
    
}
if ($action == 'delete_team_member' && $confirm == 'yes') {
    global $DB;
    $thongtinlopcheck=$DB->get_record('groups', array(
        'id' => $team_id
        ),
        $fields='id,name,id_namhoc,id_khoi,id_truong', $strictness=IGNORE_MISSING
    );
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    remove_team_member($user_id, $team_id,$course_id);
    remove_student_from_class_cre_by_trang($user_id,$team_id,$thongtinlopcheck->id_namhoc,$thongtinlopcheck->id_khoi,$thongtinlopcheck->id_truong);
    //echo displayJsAlert('remove member of team succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
if ($action == 'assign_role') {
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    if (empty($to_assign)) {
        echo displayJsAlert('Không có học sinh nào được chọn để gán.', $redirectPath);
    }else{
    foreach ($to_assign as $key => $val) {
        insert_member_to_group($val, $team_id);
        $team=get_team_from_id($team_id);
        if($send_email_notification==1){
            $member=  get_user_from_id($val);
            $mailto = $member->email;
            $username = $member->username;
            
            $invitationContenNoEmail = $team_update_content;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{TEAM_NAME}", $team->name, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            sendEmail($mailto, $mailUsername, 'LMS', $team_update_title, $invitationContenNoEmail);
            $invitationContenNoEmail = $team_update_content;
				
    
        }
    }
}
    //echo displayJsAlert('assign user to team succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
if ($action == 'edit_team') {
    global $DB;
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    $thongtinlop=$DB->get_record('groups', array(
        'id' => $team_id
        ),
        $fields='id,name,id_namhoc,id_khoi', $strictness=IGNORE_MISSING
    );
    if($thongtinlop->name!=$name){
        $checktenupdate=kiem_tra_ten_lop_theo_khoi_nam_hoc($thongtinlop->id_khoi,$namhoc,$name);
        if($checktenupdate==1){
            ?>
            <script>
              alert('Đã tồn tại tên lớp trong khối thuộc năm học lựa chọn'); 
            </script>
            <?php 
            echo displayJsAlert('', $redirectPath);
        }elseif($checktenupdate==2){
            update_team($team_id, $name,$gvcn,$email,$phone,$namhoc, $description);
            add_new_team_year($team_id,$namhoc);
            // update_team_year($team_id,$namhoc);
            //echo displayJsAlert('update team succesfully', $redirectPath);
            echo displayJsAlert('', $redirectPath);
        }

    }else{
        update_team($team_id, $name,$gvcn,$email,$phone,$namhoc, $description);
        add_new_team_year($team_id,$namhoc);
        // update_team_year($team_id,$namhoc);
        //echo displayJsAlert('update team succesfully', $redirectPath);
        echo displayJsAlert('', $redirectPath);
    }
    // var_dump($thongtinlop);die;
    // update_team($team_id, $name,$gvcn,$email,$phone,$namhoc, $description);
    // add_new_team_year($team_id,$namhoc);
    // // update_team_year($team_id,$namhoc);
    // //echo displayJsAlert('update team succesfully', $redirectPath);
    // echo displayJsAlert('', $redirectPath);
}
if ($action == 'assign_course_to_team') {
    // $redirectPath = $CFG->wwwroot . "/teams/team_course.php?team_id=$team_id";
    $redirectPath = $CFG->wwwroot . "/course/team/groups.php?team_id=$team_id";

    foreach ($to_assign as $key => $val) {

        assign_course_to_team($val, $team_id);
    }
    if ($assign_course_to_subteam == 1||$assign_course_to_subteam == "1") {//assign courses also to sub team
        $sub_teams = get_sub_teams($team_id);
        foreach ($to_assign as $key => $val) {//$val=course_id
             foreach ($sub_teams as $key1 => $val1) {
                 assign_course_to_team($val, $val1->id);//$val1->id=$team_id
             }

            
        }
    }

    //echo displayJsAlert('assign courses to team succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}

if ($action == 'delete_course_team') {
    // $redirectPath = $CFG->wwwroot . "/teams/team_course.php?team_id=$team_id";
    $redirectPath = $CFG->wwwroot . "/course/team/groups.php?team_id=$team_id";
    remove_course_team($course_id, $team_id);
    //echo displayJsAlert('remove courses of team succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
if ($action == 'add_sub_team') {

    if ($add_another == 1 || $add_another == "1") {
        $redirectPath = $CFG->wwwroot . "/teams/add_sub_team.php?team_id=" . $team_id;
    }
    $id = add_new_sub_team($name, $description, $team_id);

    if ($inherit_course == 1) {
        $courses_of_parent_team = get_courses_from_team($team_id);
        foreach ($courses_of_parent_team as $key => $val) {
            assign_course_to_team($val->course_id, $id);
        }
    }
    if ($id > 0) {
        $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=" . $id;
        //echo displayJsAlert('Added new sub team succesfully', $redirectPath);
        echo displayJsAlert('', $redirectPath);
    }
}
if ($action == 'delete_team') {
    delete_team($team_id);
    $redirectPath = $CFG->wwwroot . "/teams";
    echo displayJsAlert('', $redirectPath);
}
if($action == 'send_login_email'){
    
    $members=  get_members_in_team($team_id);
    foreach($members as $key=>$val){
            $mailto = $val->email;
            $username = $val->username;
            $newPass = update_user_password($val->id);
            $invitationContenNoEmail = $emailContentNo;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{PASSWORD}", $newPass, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
            $invitationContenNoEmail = $emailContentNo;
				
    }
		//echo displayJsAlert('Email sent succesfully', $redirectPath);
                echo displayJsAlert('', $redirectPath);
}
function thay_doi_trang_thai_lop_cu_khi_len_lop($idgroups){
    global $DB;
    $record = new stdClass();
    $record->id = $idgroups;
    $record->status = 1;
    return $DB->update_record('groups', $record, false);
}
if ($action == 'groups_up') {
    global $DB;
    // Check và tạo mới năm học
    $sql    = "SELECT * FROM school_year";
    $row    = $DB->get_records_sql($sql);

    $all_year_start = array();
    foreach ($row as $key => $value) {
        $all_year_start[$value->id] = $value->sy_start;
    }
    $current_year = date('Y');
    if ($getID = array_search($current_year, $all_year_start)) {
        $idyar  = $getID;
    }
    else{
        $idyar = insert_schools_year(date('Y'),date('Y')+1);
    }
    // $team_id = optional_param('team_id', 0, PARAM_TEXT);
    $school_year_id = optional_param('school_year_id', '', PARAM_TEXT);
    $schoolid = optional_param('schoolid', '', PARAM_TEXT);
    $block_student_old = optional_param('block_student_old', '', PARAM_TEXT);
    $class_old = optional_param('class_old', '', PARAM_TEXT);
    // $groups_old = optional_param('groups_old', '', PARAM_TEXT);

    $liststudent = optional_param('studentid', '', PARAM_TEXT);
    
    $block_student_new = optional_param('block_student_new', '', PARAM_TEXT);
    $name_group_new = optional_param('group_new', '', PARAM_TEXT); //lớp học mới.

    $groupid_new = add_new_team_class_up($name_group_new,$schoolid,$block_student_new);

    // load list course theo group, auto edit name course
    $list_course_group = get_list_groups_class_classup($class_old);
    foreach ($list_course_group as $val) {
        $course_name = $val->fullname;
        $course_name_new = substr($val->fullname,0,6).(substr($val->fullname,6,1)+1).(substr($val->fullname,7));
        // insert_history_namecourse($val->id,$course_name,$class_old,$school_year_id,$val->sotiet);
        insert_history_namecourse($val->id,$course_name_new,$groupid_new,$idyar,'');
        update_name_course_classup($course_name_new,$class_old,$val->id);
    }

    

    upadte_class_year($groupid_new,$idyar);

    add_new_team_year($groupid_new,$idyar);

    foreach ($liststudent as $student) {
        $groups_old = get_user_to_groups($student)->id; //nhóm lớp cũ
        // insert_history_student($student,$schoolid,$block_student_old,$class_old,$groups_old,$school_year_id);
        insert_history_student($student,$schoolid,$block_student_new,$groupid_new,$groups_old,$idyar);
        update_student_classup($student,$groupid_new);// update hs sang lớp học mới
        update_course_classup($class_old,$groupid_new); // update nhóm sang lớp mới 
    }

    // trang edit
    // sau khi len lop thanh cong chuyen doi trang thai lop cu status=1
    // lop cu se k duoc them hoc sinh them nhom lop k co cac thao tac lien quan den lop cu
    thay_doi_trang_thai_lop_cu_khi_len_lop($class_old);

    $redirectPath = $CFG->wwwroot . "/teams";
    echo displayJsAlert('Lên lớp thành công!', $redirectPath);   
}
?>