<?php 
require_once('../config.php');
require_once($CFG->dirroot . '/teams/lib.php');

$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 20, PARAM_INT);

$classname = optional_param('classname', '', PARAM_TEXT);
$nameschools = optional_param('nameschools', '', PARAM_TEXT);
$school_year = optional_param('school_year', '', PARAM_TEXT);
$homeroomteacher = optional_param('homeroomteacher', '', PARAM_TEXT);
$phone = optional_param('phone', '', PARAM_TEXT);
$email = optional_param('email', '', PARAM_TEXT);

$params = " WHERE 1";

if(!empty($classname)){
  $params .= " AND groups.name LIKE '%".$classname."%'";
}
if(!empty($school_year)){
  $params .= " AND groups.id_namhoc =".$school_year;
}
if(!empty($homeroomteacher)){
  $params .= " AND groups.gvcn LIKE '%".$homeroomteacher."%'";
}
if(!empty($phone)){
  $params .= " AND groups.phone LIKE '%".$phone."%'";
}
if(!empty($email)){
  $params .= " AND groups.email LIKE '%".$email."%'";
}

if (!empty($nameschools)){
	$params .= " AND id_truong=".$nameschools;
}

$list_class = get_list_groups($page,$perpage,$params);
$totalcount = count(get_list_groups(null,$perpage,$params));
 ?>


<?php if (!empty($list_class)): ?>
 <?php $i=$page*$perpage+1; foreach ($list_class as $key => $val) {?>
<tr>
    <td><?php echo $i; $i++; ?></td>
    <td><?php echo $val->name ?></td>
    <?php 
        $sch = get_info_truong($val->id_truong);
        $teams_in_course = get_list_groups_class($val->id);
        $members = get_members_in_class($val->id); 
    ?>
    <td><?php echo $sch->name ?></td>
    <td><?php echo count($teams_in_course) ?></td>
    <td><?php echo count($members) ?></td>
    <td><?php echo get_info_namhoc($val->id_namhoc)->sy_start . ' - '.get_info_namhoc($val->id_namhoc)->sy_end ?></td>
    <td><?php echo $val->gvcn ?></td>
    <td><?php echo $val->phone ?></td>
    <td><?php echo $val->email ?></td>

    
    <td class="text-right">
        <a href="<?php echo $CFG->wwwroot ?>/teams/team_settings.php?team_id=<?php echo $val->id ?>" class="btn btn-info btn-sm tooltip-animation" title="Chỉnh sửa lớp">
            <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $val->id ?>" class="btn btn-warning btn-sm tooltip-animation" title="Thông tin lớp">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
        <a href="<?php echo $CFG->wwwroot ?>/course/team/groups.php?team_id=<?php echo $val->id ?>" class="btn btn-purple waves-effect waves-light btn-sm tooltip-animation" title="Nhóm lớp">
            <i class="fa fa-users" aria-hidden="true"></i>
        </a>
        <a onclick="return Confirm('Xóa lớp học','Bạn có muốn xóa <?php echo $val->name ?> khỏi hệ thống?','Yes','Cancel','<?php echo $CFG->wwwroot?>/teams/team.php?action=delete_team&team_id=<?php echo $val->id ?>')" class="btn waves-effect waves-light btn-danger btn-sm tooltip-animation" title="Xóa lớp học">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
    </td>
</tr>
<?php } ?>
<tr class="active">
  <td colspan="10">
    <div class="float-left">
      <?php
        paginate($totalcount,$page,$perpage,"#"); 
      ?>
    </div>
  </td>
</tr>
<?php else: ?>
<tr class="active">
  <td colspan="10">
    <div class="float-left">
			<span class="text-danger"><h5><?php echo get_string('searchnoresult') ?></h5></span>
    </div>
  </td>
</tr>
<?php endif ?>