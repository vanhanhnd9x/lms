$(document).ready(function(){
 
if ($("#to").length) {
   $("#to").autocomplete("livesearch.php", {
	        width: 260,
	        matchContains: true,
	        selectFirst: false
         });
     $('#to').result(function (event, data, formatted) {
        item_autocomplete_select(data);
});
}

$('#searchInBox').focus(function() {
  $('#searchInBoxLabel').html('');
});

$('#searchInBox').keyup(function() {
	if($(this).val().length>0){
		search_inbox_when_key_up($(this).val());
	}
        else{
            
            $('#searchInBoxLabel').html('Search by message subject or message content');
        }
  ti
  
});

$("#searchInBox").focusout(function() {
   $('#searchInBoxLabel').html('Search by message subject or message content');
   document.getElementById('searchInBox').value='';
});


$('#searchSent').focus(function() {
  $('#searchSentLabel').html('');
});

$('#searchSent').keyup(function() {
  
	if($(this).val().length>0){
		search_sent_when_key_up($(this).val());
	}
});


});


function search_sent_when_key_up(search_val){
if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("searchResults").innerHTML=xmlhttp.responseText;
        }
    }
    
    xmlhttp.open("GET","livesearch_sent_ms.php?q="+search_val,true);
    xmlhttp.send();
}

function search_inbox_when_key_up(search_val){
if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("searchResults").innerHTML=xmlhttp.responseText;
        }
    }
    
    xmlhttp.open("GET","livesearch_inbox_ms.php?q="+search_val,true);
    xmlhttp.send();
}


function item_autocomplete_select(data){
  
   if(document.getElementById('hidden_to')){
       receive=document.getElementById('hidden_to').value=data;
   }
   
   
}
function displayConfirmBox(redirectPath,id,ms){
    var where_to= confirm(ms);
    if (where_to== true)
    {
        window.location=redirectPath;
    }
    else
    {
	
        return false;
    }
}


