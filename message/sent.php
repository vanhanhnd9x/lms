<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../config.php');

require_once('../config.php');
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/message/message_lib.php');
require_once($CFG->dirroot . '/message/send_form.php');


require_login(0, false);

$PAGE->set_title("Message");
$PAGE->set_heading("Message");

//now the page contents
// $PAGE->set_pagelayout('teams');
echo $OUTPUT->header();
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>

<div class="row">
    <div class="col-md-9">
        <div class="card-box">
            <div id="page-body">    
                <div class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        <div class="focus-panel" id="courseList">

                          <!-- <div class="panel-head">
                            <form action="" method="get">
                              <div class="field-help">
                                <label for="searchSent" id="searchSentLabel"><?php print_r(get_string('searchbymessage')) ?></label>
                                <input id="searchSent" name="s" value="" type="text">
                              </div>
                            </form>
                          </div> -->
                                <div id="searchResults">
                                    <?php
                                        $count_unread = 0;
                                        $mss = get_unread_sent_messages($USER->id);
                                        foreach ($mss as $key) {
                                            $from_name_obj = getUserFromId($key->useridto);
                                            $from_name = $from_name_obj->firstname . " " . $from_name_obj->lastname;
                                    ?>
                                        <!-- <div>
                                            <input id="m325389" name="ThreadId" class="row-select msg-chk" value="325389" type="checkbox">
                                        </div> -->
                                    <div class="row">
                                        <div class="col-md-10 float-left mb-2">
                                            <div class="title">
                                                <a href="<?php echo $CFG->wwwroot ?>/message/message.php?action=read_message&user_from=<?php echo $key->useridto ?>">
                                                    <?php echo $from_name; ?>
                                                </a>
                                            </div>
                                            <div class="tip"><?php echo $key->subject; ?></div>
                                        </div>
                                        <div class="col-md-2 float-right mb-2">
                                            <span class="tip"><?php echo time_ago($key->timecreated); ?></span>
                                            <span>
                                                <a class="remove-msg btn-remove-ms float-right" title='Delete' onclick='javascript:displayConfirmBox("<?php echo $CFG->wwwroot ?>/message/message_sent.php?action=delete_sent&message_id=<?php echo $key->id ?>&confirm=yes",<?php echo $key->id ?>,"Are you sure to delete")' href='#'> <i class="fa fa-times" aria-hidden="true" style="color:#f1556c;"></i></a>
                                            </span> 
                                        </div>  
                                    </div>
                                    <hr>           
                                      
                                    <?php
                                        $count_unread++;
                                        }
                                    ?>


                                    <?php
                                        $count_read = 0;
                                        $mss = get_read_sent_messages($USER->id);
                                        foreach ($mss as $key) {
                                            $from_name_obj = getUserFromId($key->useridto);
                                            $from_name = $from_name_obj->firstname . " " . $from_name_obj->lastname;
                                    ?>
                                        <!-- <div>
                                            <input id="m325389" name="ThreadId" class="row-select msg-chk" value="325389" type="checkbox">
                                        </div> -->
                                    <div class="row">
                                        <div class="col-md-10 float-left mb-2">
                                            <div class="title">
                                                <a href="<?php echo $CFG->wwwroot ?>/message/message.php?action=read_message&user_from=<?php echo $key->useridto ?>">
                                                    <?php echo $from_name; ?>
                                                </a>
                                            </div>
                                            <div class="tip"><?php echo $key->subject; ?></div>
                                        </div>           
                                        <div class="col-md-2 float-right mb-2">
                                            <span class="tip"><?php echo time_ago($key->timecreated); ?></span>
                                            <span>
                                                <a class="remove-msg btn-remove-ms float-right" title='Delete' onclick='javascript:displayConfirmBox("<?php echo $CFG->wwwroot ?>/message/message_sent.php?action=delete_sent&message_id=<?php echo $key->id ?>&confirm=yes",<?php echo $key->id ?>,"Are you sure to delete")' href='#'><i class="fa fa-times" aria-hidden="true" style="color:#f1556c;"></i></a>
                                            </span> 
                                        </div>                 
                                    </div>
                                    <hr> 
                                    <?php
                                        $count_read++;
                                        }
                                    ?>
  
                                    <div class="tip center-padded" id="count-ms">
                                        <?php echo $count_read + $count_unread .' ';
                                            print_string('messagefound') ?> 
                                    </div>
                                </div>            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card-box">
            <div class="admin-fullscreen-content">
                <div class="side-panel">
                  <div class="action-buttons">
                    <ul class="list-unstyled">
                      <li class="form-group">
                        <a class="btn btn-success" href="<?php echo $CFG->wwwroot ?>/message/message.php?action=draw_send_message_form"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print_r(get_string('newmessage')) ?></span></span></span></span></a>
                      </li>
                      <li class="form-group">
                        <a href="<?php echo $CFG->wwwroot ?>/message" class="sidebar-link-on">
                          <span><font color='red'><?php print_r(get_string('inbox')) ?></font>(<?php echo count_unread_message($USER->id) ?>)</span>
                        </a>
                      </li>
                      <li class="form-group"><a href="<?php echo $CFG->wwwroot ?>/message/sent.php" class="sidebar-link"><span><?php print_r(get_string('sent','moodle')) ?></span></a></li>
                      <li>&nbsp;</li>
                    </ul>
                  </div>
                </div>
            </div>          
        </div>
    </div>
</div>   

<?php
echo $OUTPUT->footer();
?>
