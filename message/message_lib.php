<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function draw_form_send_ms() {
  ?>
  <div class="row">
    <div class="col-md-6">
      <div class="card-box">
        <form action='message.php' method='post'>
          <div class="form-group row">
            <label class="col-2 col-form-label">
              <?php print_r(get_string('to')) ?>
            </label>
            <div class="col-10"> 
              <input id="to" type='text' value='' name='to' class="form-control" />
              <input id="hidden_to" type='hidden' value='' name='hidden_to'/>
              <input type='hidden' value='send_message' name='action'/>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-2 col-form-label">
              <?php print_r(get_string('subject')) ?>
            </label>
            <div class="col-10"> 
              <input type='text' value='' name='subject' class="form-control" />
            </div>
          </div>
          <div class="form-group row">
            <label class="col-2 col-form-label">
              <?php print_r(get_string('message')) ?>
            </label>
            <div class="col-10"> 
              <textarea name='message' row="30" col="30" class="form-control"></textarea>
            </div>
          </div>
          <div class="push-md-2">
            <input type='submit' class="btn btn-success" value=<?php print_r(get_string('send')); ?>>
          </div> 
        </form>
      </div>
    </div>
  </div>
  <?php
}

function searchUserFromName($name) {
  global $DB;
  $params = array();
  $sql = 'SELECT * FROM user where ';
  $sql .= $DB->sql_like('username', ':username', false);
  $params['username'] = '%' . $name . '%';
  $result = $DB->get_records_sql($sql, $params);
  return $result;
}

function parseUserIdParam($userIdStr) {
  //$supplierIdStr='val,key';
  if (strripos($userIdStr, ",")) {
    $lastPosOfComma = strripos($userIdStr, ",");
    $userId = substr($userIdStr, $lastPosOfComma + 1, strlen($userIdStr));
    return $userId;
  }
  else {
    return $userIdStr;
  }
}

function count_unread_message($user_id) {
  global $DB;
  return $DB->count_records_select('message', "useridto = ?", array($user_id), "COUNT('id')");
}

function get_read_sent_messages($user_id) {
  global $DB;
  $sql = "SELECT *
            FROM message_read
            WHERE useridfrom = :userid";
  $params = array('userid' => $user_id);

  return $DB->get_records_sql($sql, $params);
}

function get_unread_sent_messages($user_id) {
  global $DB;
  $sql = "SELECT *
            FROM message
            WHERE useridfrom = :userid";
  $params = array('userid' => $user_id);

  return $DB->get_records_sql($sql, $params);
}

function get_read_messages($user_id) {
  global $DB;
  $sql = "SELECT *
            FROM message_read
            WHERE useridto = :userid";
  $params = array('userid' => $user_id);

  return $DB->get_records_sql($sql, $params);
}

function get_unread_messages($user_id) {
  global $DB;
  $sql = "SELECT *
            FROM message
            WHERE useridto = :userid";
  $params = array('userid' => $user_id);

  return $DB->get_records_sql($sql, $params);
}

function displayJsAlert($ms, $redirectPath) {
  $html = '';
  if ($ms != '') {
    $html .= '<script language="javascript">alert("' . $ms . '")</script>';
  }

  if ($redirectPath != '') {
    $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
  }
  return $html;
}

function getUserFromId($userId) {
  global $DB;
  $user = $DB->get_record('user', array(
    'id' => $userId
      ));

  return $user;
}

function del_sent_ms($message_id) {
  global $DB;
  return $DB->delete_records_select('message', " id = $message_id ");
}

function del_sent_ms_read($message_id) {
  global $DB;
  return $DB->delete_records_select('message_read', " id = $message_id ");
}

function del_message_read($useridfrom) {
  global $DB;
  return $DB->delete_records_select('message_read', " useridfrom = $useridfrom ");
}

function del_message($useridfrom) {
  global $DB;
  return $DB->delete_records_select('message', " useridfrom = $useridfrom ");
}

function del_message_read_from_ms_id($message_id) {
  global $DB;
  return $DB->delete_records_select('message_read', " id = $message_id ");
}

function del_message_from_ms_id($message_id) {
  global $DB;
  return $DB->delete_records_select('message', " id = $message_id ");
}

function get_read_message_from_id($message_id) {
  global $DB;
  $user = $DB->get_record('message_read', array(
    'id' => $message_id
      ));

  return $user;
}

function get_unread_message_from_id($message_id) {
  global $DB;
  $ms = $DB->get_record('message', array(
    'id' => $message_id
      ));

  return $ms;
}

function time_ago($datefrom, $dateto = -1) {
// Defaults and assume if 0 is passed in that
// its an error rather than the epoch

  if ($datefrom <= 0) {
    return "A long time ago";
  }
  if ($dateto == -1) {
    $dateto = time();
  }

// Calculate the difference in seconds betweeen
// the two timestamps

  $difference = $dateto - $datefrom;

// If difference is less than 60 seconds,
// seconds is a good interval of choice

  if ($difference < 60) {
    $interval = "s";
  }

// If difference is between 60 seconds and
// 60 minutes, minutes is a good interval
  elseif ($difference >= 60 && $difference < 60 * 60) {
    $interval = "n";
  }

// If difference is between 1 hour and 24 hours
// hours is a good interval
  elseif ($difference >= 60 * 60 && $difference < 60 * 60 * 24) {
    $interval = "h";
  }

// If difference is between 1 day and 7 days
// days is a good interval
  elseif ($difference >= 60 * 60 * 24 && $difference < 60 * 60 * 24 * 7) {
    $interval = "d";
  }

// If difference is between 1 week and 30 days
// weeks is a good interval
  elseif ($difference >= 60 * 60 * 24 * 7 && $difference < 60 * 60 * 24 * 30) {
    $interval = "ww";
  }

// If difference is between 30 days and 365 days
// months is a good interval, again, the same thing
// applies, if the 29th February happens to exist
// between your 2 dates, the function will return
// the 'incorrect' value for a day
  elseif ($difference > 60 * 60 * 24 * 30 && $difference < 60 * 60 * 24 * 365) {
    $interval = "m";
  }
  elseif ($difference > 60 * 60 * 24 * 365) {
    $interval = "y";
  }

// Based on the interval, determine the
// number of units between the two dates
// From this point on, you would be hard
// pushed telling the difference between
// this function and DateDiff. If the $datediff
// returned is 1, be sure to return the singular
// of the unit, e.g. 'day' rather 'days'

  switch ($interval) {
    case "m":
      $months_difference = floor($difference / 60 / 60 / 24 /
          29);
      while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom) + ($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
        $months_difference++;
      }
      $datediff = $months_difference;

// We need this in here because it is possible
// to have an 'm' interval and a months
// difference of 12 because we are using 29 days
// in a month

      if ($datediff == 12) {
        $datediff--;
      }

      $res = ($datediff == 1) ? "$datediff " . get_string('monthago') : "$datediff " . get_string('monthago2');
      break;

    case "y":
      $datediff = floor($difference / 60 / 60 / 24 / 365);
      $res = ($datediff == 1) ? "$datediff " . get_string('yearago') : "$datediff " . get_string('yearago2');
      break;

    case "d":
      $datediff = floor($difference / 60 / 60 / 24);
      $res = ($datediff == 1) ? "$datediff " . get_string('dayago') : "$datediff " . get_string('dayago2');
      break;

    case "ww":
      $datediff = floor($difference / 60 / 60 / 24 / 7);
      $res = ($datediff == 1) ? "$datediff " . get_string('weekago') : "$datediff " . get_string('weekago2');
      break;

    case "h":
      $datediff = floor($difference / 60 / 60);
      $res = ($datediff == 1) ? "$datediff " . get_string('hourago') : "$datediff " . get_string('hourago2');
      break;

    case "n":
      $datediff = floor($difference / 60);
      $res = ($datediff == 1) ? "$datediff " . get_string('minuteago') :
          "$datediff " . get_string('minuteago2');
      break;

    case "s":
      $datediff = $difference;
      $res = ($datediff == 1) ? "$datediff " . get_string('secondago') :
          "$datediff " . get_string('secondago2');
      break;
  }
  return $res;
}

function get_oldest_ms_inbox($user1, $user2) {
  $mss = message_get_history($user1, $user2, 1, true);

  foreach ($mss as $key) {
    if ($user2->id == $key->useridfrom) {
      return $key;
    }
  }
}

function get_oldest_ms_sent($user1, $user2) {
  $mss = message_get_history($user1, $user2, 1, true);

  foreach ($mss as $key) {
    if ($user2->id == $key->useridto) {
      return $key;
    }
  }
}

function get_list_sent($user_id) {
  global $DB;
  $sql = "SELECT message.useridto     
FROM message
            WHERE useridfrom = :userid1
UNION
SELECT message_read.useridto     
FROM message_read
            WHERE useridfrom = :userid2";
  $params = array('userid1' => $user_id, 'userid2' => $user_id);

  return $DB->get_records_sql($sql, $params);
}

function search_sent($user_id, $search_val) {
  global $DB;

  $sql = 'SELECT DISTINCT message_read.useridto FROM message_read,user
WHERE (' . $DB->sql_like('message_read.subject', ':subject', false) . ' OR ' . $DB->sql_like('message_read.fullmessage', ':fullmessage', false) . '
OR ' . $DB->sql_like('user.username', ':username', false) . ' OR ' . $DB->sql_like('user.firstname', ':firstname', false) . ' OR ' . $DB->sql_like('user.lastname', ':lastname', false) . ')
AND (user.id=message_read.useridfrom OR user.id=message_read.useridto)
AND message_read.useridfrom=' . $user_id . '
UNION

SELECT DISTINCT message.useridto FROM message,user
WHERE (' . $DB->sql_like('message.subject', ':subject1', false) . ' OR ' . $DB->sql_like('message.fullmessage', ':fullmessage1', false) . '
OR ' . $DB->sql_like('user.username', ':username1', false) . ' OR ' . $DB->sql_like('user.firstname', ':firstname1', false) . ' OR ' . $DB->sql_like('user.lastname', ':lastname1', false) . ')
AND (user.id=message.useridfrom OR user.id=message.useridto)
AND message.useridfrom=' . $user_id . '
;';
  $params['subject'] = '%' . $search_val . '%';
  $params['fullmessage'] = '%' . $search_val . '%';
  $params['username'] = '%' . $search_val . '%';
  $params['firstname'] = '%' . $search_val . '%';
  $params['lastname'] = '%' . $search_val . '%';

  $params['subject1'] = '%' . $search_val . '%';
  $params['fullmessage1'] = '%' . $search_val . '%';
  $params['username1'] = '%' . $search_val . '%';
  $params['firstname1'] = '%' . $search_val . '%';
  $params['lastname1'] = '%' . $search_val . '%';
  //echo($sql);
  $result = $DB->get_records_sql($sql, $params);

  //print_r($result);
  return $result;
}

function search_inbox($user_id, $search_val) {
  global $DB;

  $sql = 'SELECT DISTINCT message_read.useridfrom FROM message_read,user
WHERE (' . $DB->sql_like('message_read.subject', ':subject', false) . ' OR ' . $DB->sql_like('message_read.fullmessage', ':fullmessage', false) . '
OR ' . $DB->sql_like('user.username', ':username', false) . ' OR ' . $DB->sql_like('user.firstname', ':firstname', false) . ' OR ' . $DB->sql_like('user.lastname', ':lastname', false) . ')
AND (user.id=message_read.useridfrom OR user.id=message_read.useridto)
AND message_read.useridto=' . $user_id . '
UNION

SELECT DISTINCT message.useridfrom FROM message,user
WHERE (' . $DB->sql_like('message.subject', ':subject1', false) . ' OR ' . $DB->sql_like('message.fullmessage', ':fullmessage1', false) . '
OR ' . $DB->sql_like('user.username', ':username1', false) . ' OR ' . $DB->sql_like('user.firstname', ':firstname1', false) . ' OR ' . $DB->sql_like('user.lastname', ':lastname1', false) . ')
AND (user.id=message.useridfrom OR user.id=message.useridto)
AND message.useridto=' . $user_id . '
;';
  $params['subject'] = '%' . $search_val . '%';
  $params['fullmessage'] = '%' . $search_val . '%';
  $params['username'] = '%' . $search_val . '%';
  $params['firstname'] = '%' . $search_val . '%';
  $params['lastname'] = '%' . $search_val . '%';

  $params['subject1'] = '%' . $search_val . '%';
  $params['fullmessage1'] = '%' . $search_val . '%';
  $params['username1'] = '%' . $search_val . '%';
  $params['firstname1'] = '%' . $search_val . '%';
  $params['lastname1'] = '%' . $search_val . '%';

  $result = $DB->get_records_sql($sql, $params);

  //print_r($result);
  return $result;
}

function get_list_from_user_inbox($user_id) {
  global $DB;
  $sql = "SELECT message.useridfrom     
FROM message
            WHERE useridto = :userid1
UNION
SELECT message_read.useridfrom     
FROM message_read
            WHERE useridto = :userid2";
  $params = array('userid1' => $user_id, 'userid2' => $user_id);

  return $DB->get_records_sql($sql, $params);
}

function is_message_unread($message_id) {
  $ms = get_unread_message_from_id($message_id);
  if ($ms->id > 0) {
    return 1;
  }
  else {
    return 0;
  }
}

function display_sent_message_and_reply_form($user_to_obj) {
  global $USER, $CFG;
  ?>
  <table class="admin-fullscreen">
    <tbody><tr>
        <td class="admin-fullscreen-left">
          <div class="admin-fullscreen-content">
            <div id="courseList" class="focus-panel">
              <div class="panel-head">
                <h3>
                  <?php
                  print_r(get_string('newmessage'));
                  echo fullname($user_to_obj)
                  ?>
                </h3>
              </div>
              <div class="body">

                <table class="item-page-list">
                  <tbody>
                    <?php
                    //$user2=  getUserFromId($message->useridfrom);
                    $hidden_to = "";
                    $histories = message_get_history($USER, $user_to_obj, $limitnum = 0, true);
                    foreach ($histories as $key) {
                      ?>
                      <tr>
                        <td class="align-top"><a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $key->useridto ?>"></a></td>                                                                       
                        <td class="main-col align-top">                                     
                          <div class="float-right tip"><?php echo time_ago($key->timecreated) ?></div>
                          <div class="title">
                            <a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $key->useridto ?>" class="">
                              <?php
                              $user = getUserFromId($key->useridto);
                              echo $user->firstname . " " . $user->lastname;
                              ?>
                            </a></div>
                          <div class="tip"><?php print_string('to'); ?>  

                            <?php
                            if ($key->useridfrom == $USER->id) {
                              //echo 	'$key->useridto='.$key->useridto;
                              $hidden_to = $key->useridfrom;
                              //echo 	'$hidden_to='.$hidden_to;
                            }
                            $user = getUserFromId($key->useridfrom);
                            echo $user->firstname . " " . $user->lastname;
                            ?>
                          </div>
                          <div class="body-dent">                    
                            <span style="font-size: 10pt; font-family: Verdana">
    <?php echo $key->smallmessage; ?>
                            </span>
                          </div>
                        </td>              
                      </tr>
  <?php } ?>
                    <tr>
                      <td colspan="2">
                        <a class="msg-reply" href="#m341202" style="display: none;">reply</a>
                        <div class="inline-reply hidden padded" id="m341202" style="display: block;">
                          <div class="tip padded"><?php print_string('sendmessage', 'moodle');
  echo " " . fullname($user_from_obj) ?></div>
                          <form method="post" action="<?php echo $CFG->wwwroot ?>/message/message_sent.php?action=reply"><textarea rows="5" name="message" id="message" cols="50" class="full"></textarea>

                            <div class="padded">
                              <input type="hidden"  name="hidden_to" value="<?php echo $hidden_to ?>"/>
                              <input type="submit" value="<?php print_string('reply', 'moodle') ?>" class="btnlarge"> <?php print_string('or') ?><a class="msg-reply-cancel" href="<?php echo $CFG->wwwroot ?>/message"><?php print_string('cancel') ?></a>
                            </div>
                          </form>
                        </div>
                      </td>
                    </tr>       
                  </tbody> 
                </table>   
              </div>
            </div>
          </div>
        </td>
        <td class="admin-fullscreen-right">
          <div class="admin-fullscreen-content">
          </div>
        </td>
      </tr>
    </tbody></table>


  <?php
}
?>
<?php

function display_message_and_reply_form($user_from_obj) {
  global $USER, $CFG;
  ?>
  <div style="margin-left:15px">
    <table class="admin-fullscreen">
      <tbody><tr>
          <td class="admin-fullscreen-left">
            <div class="admin-fullscreen-content">
              <div id="courseList" class="focus-panel">
                <div class="panel-head">
                  <h3>
                    <?php
                    print_r(get_string('newmessagefromuser', 'moodle'));
                    echo " " . fullname($user_from_obj)
                    ?>
                  </h3>
                </div>
                <div class="body">

                  <table class="item-page-list">
                    <tbody>
                      <?php
                      //$user2=  getUserFromId($message->useridfrom);
                      $hidden_to = "";
                      $histories = message_get_history($USER, $user_from_obj, $limitnum = 0, true);
                      foreach ($histories as $key) {
                        ?>

                        <tr>
                          <td class="align-top"><a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $key->useridfrom ?>"></a></td>                                                                       
                          <td class="main-col align-top">                                     
                            <div class="float-right tip"><?php echo time_ago($key->timecreated) ?></div>
                            <div class="title">
                              <a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $key->useridfrom ?>" class="">
                                <?php
                                $user = getUserFromId($key->useridfrom);
                                echo $user->firstname . " " . $user->lastname;
                                ?>
                              </a>
                            </div>
                            <div class="tip"><?php print_string('to') ?>  

                              <?php
                              if ($key->useridto != $USER->id) {
                                //echo 	'$key->useridto='.$key->useridto;
                                $hidden_to = $key->useridto;
                                //echo 	'$hidden_to='.$hidden_to;
                              }
                              $user = getUserFromId($key->useridto);
                              echo $user->firstname . " " . $user->lastname;
                              ?>
                            </div>
                            <div class="body-dent">                    
                              <span style="font-size: 10pt; font-family: Verdana">
    <?php echo $key->smallmessage; ?>
                              </span>
                            </div>

                          </td>              
                        </tr>
  <?php } ?>
                      <tr>
                        <td colspan="2">
                          <a class="msg-reply" href="#m341202" style="display: none;">reply</a>
                          <div class="inline-reply hidden padded" id="m341202" style="display: block;">
                            <div class="tip padded"><?php print_string('sendmessage', 'moodle');
  echo " " . fullname($user_from_obj) ?></div>
                            <form method="post" action="<?php echo $CFG->wwwroot ?>/message/message.php?action=reply"><textarea rows="5" name="message" id="message" cols="50" class="full"></textarea>
                              <div class="padded">
                                <input type="hidden"  name="hidden_to" value="<?php echo $hidden_to ?>"/>
                                <input type="submit" value="<?php print_string('reply', 'moodle') ?>" class="btnlarge"> <?php print_string('or') ?> <a class="msg-reply-cancel" href="<?php echo $CFG->wwwroot ?>/message"><?php print_string('cancel') ?></a>
                              </div>
                            </form>
                          </div>
                        </td>
                      </tr>       
                    </tbody> 
                  </table>   
                </div>
              </div>
            </div>
          </td>
          <td class="admin-fullscreen-right">
            <div class="admin-fullscreen-content">
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <?php
}
?>
