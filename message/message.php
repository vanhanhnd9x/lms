<?php
require_once('../config.php');
require_once('lib.php');
require_once('send_form.php');
require_once('message_lib.php');

require_login(0, false);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    print_error('disabled', 'message');
}
//if they have numerous contacts or are viewing course participants we might need to page through them
$page = optional_param('page', 0, PARAM_INT);
$action = optional_param('action', 0, PARAM_TEXT);
$PAGE->set_context(get_context_instance(CONTEXT_USER, $USER->id));
$PAGE->navigation->extend_for_user($USER);
$PAGE->set_pagelayout('course');
$confirm = optional_param("confirm", '', PARAM_TEXT);
$context = get_context_instance(CONTEXT_SYSTEM);
$message_id = optional_param('message_id', 0, PARAM_TEXT);
$user_from = optional_param('user_from', 0, PARAM_TEXT);

$message = optional_param('message', 0, PARAM_TEXT);
$hidden_to = optional_param('hidden_to', 0, PARAM_TEXT);
$to_id = parseUserIdParam($hidden_to);

//$is_unread==1 --> unread
//$is_unread==0 --> read
//now the page contents
$PAGE->set_title(get_string('sent'));
$PAGE->set_heading(get_string('sent'));
echo $OUTPUT->header();
//echo $OUTPUT->box_start('message');
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<?php
global $USER, $CFG;
$redirectPath = $CFG->wwwroot . "/message/";
if ($action == 'draw_send_message_form') {
    draw_form_send_ms();
		
}

if ($action == 'send_message') {
	  $userfrom = $USER;
    $userto = getUserFromId($to_id);
    $result = message_post_message($userfrom, $userto, $message, FORMAT_MOODLE);

    if ($result > 0) {
        //echo displayJsAlert('Send message successfully', $redirectPath);
        echo displayJsAlert('', $redirectPath);
    }
}

if ($action == 'reply') {
	
    $userfrom = $USER;
    $userto = getUserFromId($to_id);
	  $result = message_post_message($userfrom, $userto, $message, FORMAT_MOODLE);
	  if ($result > 0) {
        //echo displayJsAlert('Reply message successfully', $redirectPath);
              echo displayJsAlert('', $redirectPath);
    }
}


if ($action == 'read_message') {
    $user_from_obj=getUserFromId($user_from);
    display_message_and_reply_form($user_from_obj);
    message_mark_messages_read($USER->id, $user_from_obj->id);
    
}
if ($action == 'delete' && $confirm == '') {
    
}
if ($action == 'delete' && $confirm == 'yes') {
	$unread=is_message_unread($message_id);
	
        if($unread){
					del_message_from_ms_id($message_id);
				}
				else{
					del_message_read_from_ms_id($message_id);
				}
            //echo displayJsAlert('delete message succesfully', $redirectPath);
           echo displayJsAlert('', $redirectPath);
    
}

echo $OUTPUT->footer();
?>
