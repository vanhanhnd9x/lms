<?php
require_once('../config.php');
require_once('lib.php');
require_once('send_form.php');
require_once('message_lib.php');

require_login(0, false);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    print_error('disabled', 'message');
}



//if they have numerous contacts or are viewing course participants we might need to page through them
$page = optional_param('page', 0, PARAM_INT);
$action = optional_param('action', 0, PARAM_TEXT);


$PAGE->set_context(get_context_instance(CONTEXT_USER, $USER->id));
$PAGE->navigation->extend_for_user($USER);
$PAGE->set_pagelayout('course');
$confirm = optional_param("confirm", '', PARAM_TEXT);
$context = get_context_instance(CONTEXT_SYSTEM);
$message_id = optional_param('message_id', 0, PARAM_TEXT);
$user_to = optional_param('user_to', 0, PARAM_TEXT);

echo $OUTPUT->header();
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<?php
echo $OUTPUT->box_start('message');
global $USER, $CFG;
$redirectPath = $CFG->wwwroot . "/message/sent.php";


if ($action == 'reply') {

    $message = optional_param('message', 0, PARAM_TEXT);
    $hidden_to = optional_param('hidden_to', 0, PARAM_TEXT);
    $to_id = parseUserIdParam($hidden_to);
    $userfrom = $USER;
    $userto = getUserFromId($to_id);
		//var_dump($message);
		//var_dump($to_id);
    $result = message_post_message($userfrom, $userto, $message, FORMAT_MOODLE);

    if ($result > 0) {
        //echo displayJsAlert('Reply message successfully', $redirectPath);
        echo displayJsAlert('', $redirectPath);
    }
}


if ($action == 'read_message') {
    $user_to_obj=getUserFromId($user_to);
    display_sent_message_and_reply_form($user_to_obj);
    
    
}
if ($action == 'delete_sent' && $confirm == '') {
    
}
if ($action == 'delete_sent' && $confirm == 'yes') {
	$unread=is_message_unread($message_id);
	
        if($unread){
					del_sent_ms($message_id);
				}
				else{
					del_sent_ms_read($message_id);
				}
            //echo displayJsAlert('delete message succesfully', $redirectPath);
           echo displayJsAlert('', $redirectPath);
    
}
?>
