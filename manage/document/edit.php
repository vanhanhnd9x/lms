<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/document/lib.php');
global $CFG, $USER;
require_login(0, false);

require_login(0, false);
$PAGE->set_title(get_string('update'));
$PAGE->set_heading(get_string('update'));
echo $OUTPUT->header();

$id = optional_param('id', '', PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$tencu = optional_param('tencu', '', PARAM_TEXT);
$file = optional_param('file', '', PARAM_TEXT);

$var = get_all_document($id);
if ($action == 'upload') {
  $description = trim(optional_param('description', '', PARAM_TEXT));
  $document_name = trim(optional_param('document_name', '', PARAM_TEXT));
  $ulf=upload_gg("file",  $CFG->dirroot . "/course/document/upload/");
  if ($ulf=="") {
    $ulf = $tencu;
  }
  $new_doc = update_doc($id, $document_name, $ulf, $description);
  add_logs('document', 'update', '/manage/document/index.php', $document_name);
  echo displayJsAlert(get_string('addsuccess'), $CFG->wwwroot . "/manage/document/index.php");

}else{
  $document_name=$var->new_name;
  $description = $var->description;
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='document';
$name1='edit';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<?php //if (is_admin() || is_teacher() || is_teacher_tutors()): ?>

<div class="row">
  <div class="col-md-12">

    <div class="card-box">
        <div class="card-content">
          <div class="panel-head">
              <h3><?php print_r(get_string('addareference')) ?></h3>          
          </div>
          <br>
          <div class="material-datatables" style="clear: both;">
              <form method="post" id="form0"  onsubmit="return check()" action="<?php echo $CFG->wwwroot ?>/manage/document/edit.php?action=upload" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <input type="hidden" name="tencu" value="<?php echo $var->file_name ?>">
                  <div class="tip">
                      <p><?php print_r(get_string('uploadadd')) ?></p>
                      <ul>
                          <li><?php print_r(get_string('notealpha')) ?></li>
                          <li><?php echo get_string('format') ?>: .xml, .gif, .png, .jpg, .rar, .zip</li>
                          <li><?php print_r(get_string('maxfile')) ?> <?php echo ini_get('upload_max_filesize') ?></li>
                      </ul> 
                  </div>    
                  <br><br>
                  <div class="form-buttons">
                      <div class="form-group">
                        <label for="file"><?php print_r(get_string('filename')) ?></label>
                        <input type="text" class="form-control" value="<?php echo $var->file_name ?>" disabled>
                        <input type="file" name="file" class="form-control" /> 
                        <div id="loi"></div>
                        
                      </div>
                      <div class="form-group">
                        <label for="document_name"><?php print_r(get_string('optionalchang')) ?></label>
                        <input type="text" id="document_name" name="document_name" class="form-control" value="<?php 
                          if(!empty($document_name)){echo $document_name;}
                         ?>" />
                        <div id="errnull"></div>
                      </div>
                      <div class="form-group">
                        <label for="description"><?php echo get_string('describe') ?></label>
                        <textarea class="form-control" id="description" name="description"><?php echo $description ?></textarea>
                      </div>
                      <hr>
                      <input type="submit" class="btnlarge btn btn-primary" value="<?php print_r(get_string('save', 'admin')) ?> " id="submitUpload">
                      <?php print_r(get_string('or')) ?> <a class="btn btn-danger" href="<?php echo $CFG->wwwroot ?>/manage/document/index.php?id=<?php echo $id ?>"><?php print_r(get_string('cancel')) ?></a>  
                  </div>

              </form>
          </div>
            
        <!-- end content-->
    </div>
      <!--  end card  -->
  </div>
    <!-- end col-md-12 -->
</div>
  
<?php //else: ?>
<?php //echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot); ?>
<?php //endif ?>
<?php echo $OUTPUT->footer(); ?>
<script>
  function check() {
    var er=0;

    if($('#file').get(0).files.length==0){
      $("#loi").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('selectfile') ?></div>');
      er=1;
    }
    if ($('#document_name').val()=="") {
      $("#errnull").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('notempty') ?></div>');
      er=1;
    }
    if (window.File && window.FileReader && window.FileList && window.Blob){
      // lay dung luong va kieu file tu the input file

        if($('#file').get(0).files.length==0){
          $("#loi").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('selectfile') ?></div>');
        }else{
          var fname = $('#file')[0].files[0].name;
          var fsize = $('#file')[0].files[0].size;
          var ftype = $('#file')[0].files[0].type;

          // if (ftype=='image/png' || ftype=='image/gif' || ftype=='image/jpeg' || ftype=='image/pjpeg') {
            if (fsize > <?php echo get_real_size(ini_get('upload_max_filesize')) ?>) {
              er=1;
              $("#loi").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('size') ?> < <?php echo ini_get('upload_max_filesize') ?></div>');
            }else{
              $("#loi").html('');
            }
        }
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    }


    if (er==0) {
      return true;
    }else{
      return false;
    }
  }

//---------------------------------------------------------------------------------------------------
  $('#document_name').change(function() {
      if ($('#document_name').val()!="") {
        $('#errnull').addClass('hide');
      }else{
        $('#errnull').removeClass('hide');
        $("#errnull").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('notempty') ?></div>');
      }
  });
// --------------------------------------------------------------------------------------------------
  $('#file').change(function(event) {
    if (window.File && window.FileReader && window.FileList && window.Blob){
      // lay dung luong va kieu file tu the input file

        if($('#file').get(0).files.length==0){
          $("#loi").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('notempty') ?></div>');
        }else{
          var fname = $('#file')[0].files[0].name;
          var fsize = $('#file')[0].files[0].size;
          var ftype = $('#file')[0].files[0].type;

          // if (ftype=='image/png' || ftype=='image/gif' || ftype=='image/jpeg' || ftype=='image/pjpeg') {
            if (fsize > <?php echo get_real_size(ini_get('upload_max_filesize')) ?>) {
              $("#loi").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo get_string('sizesys') ?> < <?php echo ini_get('upload_max_filesize') ?></div>');
            }else{
              $("#loi").html('');
            }
        }
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    }
  });
</script>
