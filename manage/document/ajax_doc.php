<?php 
require("../../config.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/document/lib.php');

if (empty($_GET['page'])) {
	$page = 0;
}else{
$page = $_GET['page'];
}
$perpage = 15;
$param = $_GET['key'];
$rows = get_list_class($page, $perpage, $param);

// echo '<pre>';
// var_dump($arr_course);
?>
<?php foreach ($rows as $value): ?>
	<?php $result_course = get_doc_add_course($_GET['doc_id'], $value->id); ?>
		<?php if (!$result_course): ?>
			<tr>
		    <td><input type="checkbox" class="list" name="classgroup[]" value="<?php echo $value->id ?>"></td>
		    <td><?php echo $value->fullname ?></td>
		    <td><?php echo get_nameSchool($value->id)->tentruong ?></td>
		    <td></td>
		  </tr>
		<?php else: ?>
		  <tr>
		    <td></td>
		    <td><?php echo $value->fullname ?></td>
		    <td><?php echo get_nameSchool($value->id)->tentruong ?></td>
		    <td><span class="alert alert-success btn-sm"><?php echo get_string('assigned') ?></span></td>
		  </tr>
		<?php endif ?>
<?php endforeach ?>
<tr>
	<td colspan="4">
		<?php paginate(count(get_list_class(null, $perpage, $param)),$page,$perpage,'#') ?>
	</td>
</tr>

<script>
	$('a').on('click', function(event) {
		var x=$(this).attr('href');
		var page=x.replace('#page=', '');
		$.get('ajax_doc.php?doc_id='+$(this).val()+'&key='+$('#kk').val()+'&page='+page,function(data) {
      $('#pagek').html(data);
    });
	});
</script>
<script>
    $(':checkbox').click(function() {
      if ($('.list').is(':checked')) {
      	$('#selclass').attr('type', 'submit');
      }else {
      	$('#selclass').attr('type', 'button');
      }
    });
</script>