<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/document/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('listdoc'));
echo $OUTPUT->header();

$page = optional_param('page', '0', PARAM_INT);
$perpage  = optional_param('perpage', '20', PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);
 $roleid1 = optional_param('roleid1', '', PARAM_TEXT);
$totalcount=0;
// if($search!='') 
//   $param = "(description LIKE '%$search%' OR new_name LIKE '%$search%' )";
// else
//   $param = 1;

// if (is_student()) {
//   $doc = get_doc_user($page, $perpage, $param, $totalcount, $USER->id);
// }else
//   $doc = get_all_document(null, $page, $perpage, $param, $totalcount);
$rows = get_list_class();
if ($action=="delete_document") {
  $id = optional_param('document_id', '', PARAM_TEXT);
  $name_doc = get_all_document($id)->new_name;
  del_document($id);
  add_logs('document', 'delete', '/manage/document/index.php', $name_doc);
  echo displayJsAlert(get_string('deletedsucessfully'), $CFG->wwwroot . "/manage/document/index.php");
}

if ($action=="addgroup") {
  $listgroup = optional_param('classgroup', '', PARAM_TEXT);
  $id = optional_param('doc_id', '', PARAM_TEXT);
  foreach ($listgroup as $course_id) {
    add_to_course($id, $course_id);
  }
  echo displayJsAlert(get_string('assigned'), $CFG->wwwroot . "/manage/document/index.php");
}
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$cua=seach_documet($name,$page,$number);
if(in_array($roleid,array(5,8,10))){
  $cua=danh_sach_tai_lieu_theo_hoc_sinh_va_gv($USER->id,$roleid,$name,$page,$number);
  
}
global $DB;

$url= $CFG->wwwroot.'/manage/document/index.php?page=';
if($action=='search'){
 
  
  $url= $CFG->wwwroot.'/manage/document/index.php?action=search&roleid1='.$roleid.'&name='.$name.'&page=';
}
$moodle='document';
$name1='listdoc';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
$checkgan=lay_ds_cn_gan($roleid,$moodle);
$checkpopup=check_chuc_nang_pop_up($roleid,$moodle);

?>
<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mr_bottom15 hidden-print">
                        <div class="col-md-2">
                          <?php 
                           if (!empty($checkthemmoi)):
                           // if (!is_student()):
                            ?>                          
                            <a href="<?php echo new moodle_url('/manage/document/add_document.php'); ?>" class="btn btn-success"><?php echo get_string('add') ?></a>
                            <?php endif ?>
                        </div>
                        <div class="col-md-10">
                            <form action="" class="row form-group" method="get">
                                <input type="text" name="action" hidden="" value="search">
                                <input type="text" name="roleid1" hidden="" value="<?php echo $roleid; ?>">
                                <div class="col-md-6">
                                    <input type="text" name="name" class="form-control" placeholder="<?php echo get_string('search'); ?>" value="<?php echo $search ?>">
                                </div>
                                
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-success"><?php echo get_string('search') ?></button>
                                </div>
                            </form>                            
                        </div>
                    </div>
                     <?php 
                    if(!empty($cua['data'])){
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">STT</th>
                                        <?php 
                                          if(!empty($checkdelete)||!empty($hanhdong)||!empty($checkgan)||!empty($checkpopup)){
                                            ?>
                                            <th class="disabled-sorting text-center hidden-print"><?php echo get_string('action') ?></th>
                                            <?php 
                                          }
                                         ?>
                                        <th><?php echo get_string('docname') ?></th>
                                        <th><?php echo get_string('describe') ?></th>
                                        
                                    </tr>
                                </thead>
                                <tbody id="page">
                                <?php 
                                  $i=0;
                                  foreach ($cua['data'] as $value): 
                                  $i++;
                                ?>
                                <?php // if ($value->del=='0' && $value->course_id=='0'): ?>
                                <tr>
                                  <td class="text-center"><?php echo $i; ?></td>
                                  <?php 
                                    if(!empty($checkdelete)||!empty($hanhdong)||!empty($checkgan)||!empty($checkpopup)){
                                      echo'<td class="text-center hidden-print">';
                                      if($checkgan){
                                        ?>
                                         <button type="button" class="mauicon btnid tooltip-animation" title='<?php echo get_string('assigntoclass') ?>' data-toggle="modal" data-target="#myModal" value="<?php echo $value->id ?>">
                                          <i class="fa fa-plus-circle"></i>
                                        </button>
                                        
                                        <?php 
                                      }
                                      if($checkpopup){
                                        ?>
                                        <a href="<?php echo $CFG->wwwroot.'/course/document/upload/'.$value->file_name ?>" class="mauicon tooltip-animation" title="<?php echo get_string('download') ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>
                                        <?php 
                                      }
                                      $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$value->id);
                                      if(!empty($checkdelete)){
                                        ?>
                                        <a href="#" class="mauicon tooltip-animation" title='<?php echo get_string('delete') ?>' onclick="javascript:displayConfirmBox('<?php echo $CFG->wwwroot ?>/manage/document/index.php?action=delete_document&document_id=<?php echo $value->id ?>', '<?php echo get_string('doyouwantdel') ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <?php 
                                      }
                                      echo'</td>';
                                    }
                                   ?>
                                  <td><?php echo $value->new_name ?></td>
                                  <td><?php echo $value->description ?></td>
                                  
                                  <!-- <td class="text-right hidden-print">
                                    <?php if (!is_student()): ?>
                                      
                                    
                                      <button type="button" class="btn btn-success btn-sm btnid tooltip-animation" title='<?php echo get_string('assigntoclass') ?>' data-toggle="modal" data-target="#myModal" value="<?php echo $value->id ?>">
                                        <i class="fa fa-plus-circle"></i>
                                      </button>
                                    <?php endif ?>
                                      <a href="<?php echo $CFG->wwwroot.'/course/document/upload/'.$value->file_name ?>" class="btn btn-warning btn-sm tooltip-animation" title="<?php echo get_string('download') ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>
                                      <?php if (!is_student()): ?>
                                        <a href="<?php echo $CFG->wwwroot.'/manage/document/edit.php?id='.$value->id ?>" class="btn btn-info btn-sm tooltip-animation" title='<?php echo get_string('update') ?>'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                      
                                      
                                      <a href="#" class="btn btn-danger btn-sm tooltip-animation" title='<?php echo get_string('delete') ?>' onclick="javascript:displayConfirmBox('<?php echo $CFG->wwwroot ?>/manage/document/index.php?action=delete_document&document_id=<?php echo $value->id ?>', '<?php echo get_string('doyouwantdel') ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                      <?php endif ?>
                                  </td>
                                </tr> -->
                                <?php //endif ?>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                          </div>
                        <?php 
                            if ($cua['total_page']) {
                              if ($page > 2) { 
                                $startPage = $page - 2; 
                              } else { 
                                $startPage = 1; 
                              } 
                              if ($cua['total_page'] > $page + 2) { 
                                $endPage = $page + 2; 
                              } else { 
                                $endPage =$cua['total_page']; 
                              }
                            }
                             ?>
                            <p>
                                <?php echo get_string('total_page'); ?>:
                                <?php echo $cua['total_page'] ?>
                            </p>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
                                        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <?php 
                                    for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                                    # code...
                                      ?>
                                    <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>">
                                            <?php echo $i; ?></a></li>
                                    <?php 
                                    }
                                    ?>
                                    <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
                                        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                    <?php 
                    }
                 ?>
                   
                </div>
            </div>         
    </div>
    <!-- end col-md-12 -->
    
    <div class="col-md-3">
      
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo get_string('assigndoc') ?></h4>
            </div>
            <div class="modal-body row" style="padding-bottom: 0px;">
              <div class="col-md-12">
                <input type="text" id="kk" class="form-control small" placeholder="<?php echo get_string('search') ?>...">
              </div>
            </div>
            <form action="#">
              <div class="modal-body">
                <table id="tbk" class="table table-hover">
                  <thead>
                      <tr>
                        <th></th>
                        <th><?php echo get_string('namegroups') ?></th>
                        <th><?php echo get_string('nameschools') ?></th>
                        <th><?php echo get_string('status') ?></th>
                      </tr>
                  </thead>
                  <tbody id="pagek">
                  </tbody>
                </table>

              </div>
              <div class="modal-footer">
                <input type="text" name="action" value="addgroup" hidden="">
                <input type="text" name="doc_id" id="doc_id" value="" hidden="">
                <input type="button" class="btn btn-success" id="selclass" value="<?php echo get_string('assign') ?>">
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<style>
    .mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }
    .btnid{
      border: none;
      background: none !important;
    }

</style>
<?php
    echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script type="text/javascript">
function displayConfirmBox(redirectPath,ms){
    var where_to= confirm(ms);
    if (where_to== true)
    {
        window.location=redirectPath;
    }
    else
    {
        return false;
    }
}
</script>

<script>
$(document).ready(function(){
  $("#kk").on("keyup", function() {
    $('#doc_id').val($(this).val());
    $.get('ajax_doc.php?doc_id='+$(this).val()+'&key='+$('#kk').val(),function(data) {
      $('#pagek').html(data);
    });
  });
});
$('#selclass').click(function(event) {
  if (!$('.list').is(':checked')) {
    alert("chưa chọn nhóm lớp");
  }
});
</script>

<script>
  $('.btnid').click(function() {
    $('#doc_id').val($(this).val());
    $.get('ajax_doc.php?doc_id='+$(this).val()+'&key='+$('#kk').val(),function(data) {
      $('#pagek').html(data);
    });
  });
</script>