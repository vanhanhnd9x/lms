<?php 
require_once '../../config.php';

function get_all_document($id=null, $page=0, $perpage=1000, $param=1, &$totalcount)
{
  global $DB;
  if (!empty($id)) {
    return $DB->get_record('document', array('id'=>$id));
  }
  $totalcount = $DB->get_record_sql("SELECT count(id) AS 'count' FROM `document` WHERE del=0 AND course_id=0 AND $param");
  return $DB->get_records_sql("SELECT * FROM `document` WHERE del=0 AND course_id=0 AND $param LIMIT $page, $perpage");
}

function get_doc_user($page=0, $perpage=20, $param=1, &$totalcount, $user){
  global $DB;
  $sql = "SELECT DISTINCT count(*) AS 'count' FROM document 
          JOIN doc_add_course ON document.id = doc_add_course.doc_id
          JOIN course ON doc_add_course.course_id = course.id
          JOIN enrol ON course.id=enrol.courseid
          JOIN user_enrolments ON enrol.id = user_enrolments.enrolid
          WHERE (user_enrolments.userid=$user OR course.id_tg=$user OR course.id_gv=$user) AND document.del=0 AND $param";
  $totalcount = $DB->get_record_sql($sql);
  $sql = "SELECT DISTINCT document.* FROM document 
          JOIN doc_add_course ON document.id = doc_add_course.doc_id
          JOIN course ON doc_add_course.course_id = course.id
          JOIN enrol ON course.id=enrol.courseid
          JOIN user_enrolments ON enrol.id = user_enrolments.enrolid
          WHERE (user_enrolments.userid=$user OR course.id_tg=$user OR course.id_gv=$user) AND document.del=0 AND $param LIMIT $page, $perpage";
  return $DB->get_records_sql($sql);
}

function get_all_course()
{
  global $DB;
  return $DB->get_records('course');
}

function new_document($name, $file_name, $description, $course_id=null) {
    global $DB;

    $document = new stdClass();
    $document->course_id = $course_id;
    $document->new_name = $name;
    $document->file_name = $file_name;
    $document->description = $description;
    $document->del = 0;
    return $DB->insert_record('document', $document);
}


function del_document($id){
  global $DB, $CFG;
  $doc = get_all_document($id);
  $url_file = $CFG->dirroot. "/course/document/upload/" . $doc->file_name;
  if (file_exists($url_file));
  {
    unlink($url_file);
  }
  $DB->delete_records_select('document', " id = $id ");
  $DB->delete_records_select('doc_add_course', " doc_id = $id ");
}


function upload_gg($name, $path){
  $maxfile = @ini_get('upload_max_filesize');
  if (!empty($_FILES[$name]['name'])) {
    if ($_FILES[$name]['size'] < get_real_size($maxfile)) {
      $path_sv = $path.time().$_FILES[$name]['name'];
      $kk = end(explode('/', $path_sv));
      move_uploaded_file($_FILES[$name]['tmp_name'], $path_sv);
      return $kk;
    }
  }
}

function loii($id){
  if($id==3){
    return "Không được bỏ trống";
  }
  if ($id==1) {
    return "Vượt quá dung lượng";
  }
  if ($id==2) {
    return "Không đúng định dạng";
  }
  return 0;
}

function add_to_course($doc_id, $course_id)
{
  global $DB;
  $result = new stdClass();
  $result->course_id = $course_id;
  $result->doc_id = $doc_id;
  return $DB->insert_record('doc_add_course', $result);
}

function get_doc_add_course($doc_id, $course_id)
{
  global $DB;
  $gg = $DB->get_records('doc_add_course', array('doc_id'=>$doc_id, 'course_id'=>$course_id));
  if (empty($gg)) {
    return false;
  }
  return true;
}

function get_nameSchool($id)
{
  global $DB;
  $sql = "SELECT course.fullname ,schools.`name` AS tentruong 
          FROM group_course 
          JOIN course ON course.id = group_course.course_id 
          JOIN groups ON groups.id = group_course.group_id
          JOIN schools ON schools.id = groups.id_truong
          WHERE group_course.course_id=".$id;
  return $DB->get_record_sql($sql);
}


function update_doc($id, $name, $file_name, $description) {
    global $DB;

    $document = new stdClass();
    $document->id = $id;
    $document->course_id = $course_id;
    $document->new_name = $name;
    $document->file_name = $file_name;
    $document->description = $description;
    $document->course_id = 0;
    $document->del = 0;
    return $DB->update_record('document', $document, false);
}
function danh_sach_tai_lieu_theo_hoc_sinh_va_gv($userid=null,$roleid=null,$name=null,$page,$number){
  global $DB;
   if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql=" SELECT `document`.*  FROM document  
  JOIN `doc_add_course` ON `document`.`id` = `doc_add_course`.`doc_id`
  JOIN `course` ON `doc_add_course`.`course_id` = `course`.`id`
  WHERE `document`.`del`=0 
  ";
  $sql_count=" SELECT COUNT(`document`.`id`)   FROM document  
  JOIN `doc_add_course` ON `document`.`id` = `doc_add_course`.`doc_id`
  JOIN `course` ON `doc_add_course`.`course_id` = `course`.`id`
  WHERE `document`.`del`=0 
  ";
  if(!empty($userid)){
     $sql.=" AND `course`.`id` IN (SELECT `course`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id`  WHERE `user`.`id`={$userid}  )";
    $sql_count.=" AND `course`.`id` IN (SELECT `course`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id`  WHERE `user`.`id`={$userid}  )";
  }
   // if($roleid==5){
   //  $sql.=" AND `course`.`id` IN (SELECT `course`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$userid}  AND `role_assignments`.`roleid`=5)";
   //  $sql_count.=" AND `course`.`id` IN (SELECT `course`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$userid}  AND `role_assignments`.`roleid`=5)";
   //  }
   //  if($roleid==8){
   //    $sql.=" AND `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$userid} AND `role_assignments`.`roleid`=8)";
   //    $sql_count.=" AND `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$userid} AND `role_assignments`.`roleid`=8)";
   //  }
   //  if($roleid==10){
   //    $sql.=" AND `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$userid} AND `role_assignments`.`roleid`=10)";
   //    $sql_count.=" AND `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$userid} AND `role_assignments`.`roleid`=10)";
   //  }
    if(!empty($name)){
     $sql .= " AND (`document`.`new_name` LIKE '%{$name}%' OR `document`.`description` LIKE '%{$name}%')";
     $sql_count .= " AND (`document`.`new_name` LIKE '%{$name}%' OR `document`.`description` LIKE '%{$name}%')";
    }
  $sql .= " ORDER BY `document`.`id` DESC LIMIT {$start} , {$number} ";
  $data=$DB->get_records_sql($sql);
  $total_row=$DB->get_records_sql($sql_count);
  foreach ($total_row as $key => $value) {
    $total_page=ceil((int)$key / (int)$number);
    break;
  }
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page 
  );
  return $kq;
}
function seach_documet($name=null,$page,$number){
  global $DB;
   if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql=" SELECT `document`.*  FROM document  
  -- JOIN `doc_add_course` ON `document`.`id` = `doc_add_course`.`doc_id`
  -- JOIN `course` ON `doc_add_course`.`course_id` = `course`.`id`
  WHERE `document`.`del`=0 
  ";
  $sql_count=" SELECT COUNT(`document`.`id`)   FROM document  
  -- JOIN `doc_add_course` ON `document`.`id` = `doc_add_course`.`doc_id`
  -- JOIN `course` ON `doc_add_course`.`course_id` = `course`.`id`
  WHERE `document`.`del`=0 
  ";
  
    if(!empty($name)){
     $sql .= " AND (`document`.`new_name` LIKE '%{$name}%' OR `document`.`description` LIKE '%{$name}%')";
     $sql_count .= " AND (`document`.`new_name` LIKE '%{$name}%' OR `document`.`description` LIKE '%{$name}%')";
    }
  $sql .= " ORDER BY `document`.`id` DESC LIMIT {$start} , {$number} ";
  $data=$DB->get_records_sql($sql);
  $total_row=$DB->get_records_sql($sql_count);
  foreach ($total_row as $key => $value) {
    $total_page=ceil((int)$key / (int)$number);
    break;
  }
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page 
  );
  return $kq;
}
?>