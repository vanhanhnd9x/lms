<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file processes AJAX enrolment actions and returns JSON for the manual enrolments plugin
 *
 * The general idea behind this file is that any errors should throw exceptions
 * which will be returned and acted upon by the calling AJAX script.
 *
 * @package    manage
 * @subpackage manual
 * @copyright  2012 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
//require_once($CFG->dirroot.'/manage/common.php');
define('AJAX_SCRIPT', true);
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/lib/datalib.php');
$s = required_param('s', PARAM_TEXT); // course id
$action = required_param('action', PARAM_ACTION);


switch ($action) {
    case 'searchClass':
        $class = search_get_class($s);
        // var_dump($class);
        // die;
        $content  = "<table id=\"tech-companies-1\" class=\"table table-striped table-hover\">";
        $content .= "<thead>
                        <tr>
                            <th data-priority=\"6\">Mã Lớp</th>
                            <th data-priority=\"6\">Tên Lớp</th>
                            <th data-priority=\"3\">Giáo viên chủ nhiệm</th>
                            <th data-priority=\"6\">Khối học sinh</th>
                            <th data-priority=\"6\">Mô tả</th>
                            <th class=\"text-right\">Hành động</th>
                        </tr>
                    </thead>";
        $content .= "<tbody>";
        $finded = 0;
        foreach ($class as $cl) {
            $finded++;
            $content .="<tr>";
            $content .="<td>" . $cl->malop ."</td>";
            $content .="<td>" . $cl->tenlop ."</td>";
            $content .="<td>" . $cl->id_gv ."</td>";
            $content .="<td>" . $cl->id_khoi ."</td>";
            $content .="<td>" . $cl->mota ."</td>";
            

            $content .='<td class="text-right">';

            $edit = new moodle_url('/manage/class/edit_class.php',array('class_id'=>$cl->id));
            $view =  new moodle_url('/manage/class/index.php');
            $remove =  new moodle_url('?action=delete',array('id'=>$cl->id));

            $content .= "<a href=\"$edit\" class=\"btn btn-info\"><i class=\"fa fa-pencil\"></i></a>";
            $content .= '<a href="' . $view . '" class="btn btn-warning"><i class="fa fa-eye"></i></a>';
            $content .= '<a href="' . $remove . '" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $content .='</td>';
        }
        $content .="</tbody>";
        $content .="</table>";
        
        $content .="</div>"; // End div rearchReasult
        break;

    case 'searchTeacher':
        $teacher = search_get_teacher($s);
//end fix
        $content  = "<table id=\"tech-companies-1\" class=\"table table-striped table-hover\">";
        $content .= "<thead>
                        <tr>
                            <th data-priority=\"6\">Họ tên</th>
                            <th data-priority=\"6\">Email</th>
                            <th data-priority=\"3\">Số điện thoại</th>
                            <th data-priority=\"6\">Công ty</th>
                            <th data-priority=\"6\">Địa chỉ</th>
                            <th data-priority=\"3\">Trạng thái</th>
                            <th class=\"text-right\">Hành động</th>
                        </tr>
                    </thead>";
        $content .= "<tbody>";
        $finded = 0;
        foreach ($teacher as $user) {
            $finded++;
            $content .="<tr>";
            $content .="<td>" . $user->firstname . ' ' . $user->lastname ."</td>";
            $content .="<td>" . $user->email ."</td>";
            $content .="<td>" . $user->phone2 ."</td>";
            $content .="<td>" . $user->company ."</td>";

            $xaphuong = get_info_xaphuong($user->id_wards);
            $quanhuyen = get_info_quanhuyen($user->id_district);
            $tinhthanh = get_info_tinhthanh($user->city);
            $content .="<td>" . $user->address .' - '. $xaphuong->name.' - '. $quanhuyen->name.' - '. $tinhthanh->name ."</td>";
            
            $active = $user->confirmed ? get_string('active') : get_string('inactive');
            $activetitle = $user->confirmed ? 'badge-success' : 'badge-danger';

            // $content .= '<span class="box-tag box-tag-yellow" title="Vai trò">' . people_get_roles($user->id) . '</span>';
            $content .='<td><span class="badge label-table ' . $activetitle . '" title="Trạng thái">' . $active . '</span></td>';
            $content .='<td class="text-right">';

            $edit = new moodle_url('/manage/people/edit.php',array('user_id'=>$user->id));
            $view =  new moodle_url('/manage/people/profile.php', array('id' => $user->id));
            $remove =  new moodle_url('?action=delete',array('id'=>$user->id));

            $content .= "<a href=\"$edit\" class=\"btn btn-info\"><i class=\"fa fa-pencil\"></i></a>";
            $content .= '<a href="' . $view . '" class="btn btn-warning"><i class="fa fa-eye"></i></a>';
            $content .= '<a href="' . $remove . '" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $content .='</td>';
        }
        $content .="</tbody>";
        $content .="</table>";
        
        $content .="</div>"; // End div rearchReasult
        $content .="<p class='text-info'>" . $finded . " " . get_string('peoplefund') . "</p>";
        break;

    case 'searchCourse':
        $courses = search_enrol_get_my_courses($s);
        $content .="<ul>";
        $finded = 0;
        foreach ($courses as $course) {
            $finded++;
            $course_setting = get_course_settings($course->id);
            $content .="<li>";
            $content .="<div class=\"main-col\">";
            $content . "<div class=\"title\">";
            $link = new moodle_url('/course/view.php', array('id' => $course->id));
            $content .= "<a href=\"$link\"><span class=\"nav-reports\">$course->fullname</span></a>";
            $content .= "<div class=\"tip\">";
            $content .= $course->idnumber;
            $content .= "</div></div>"; // End main col
            $content .="<div class='right-col'>";
            $edit = new moodle_url('/course/view.php', array('id' => $course->id));
            $active = $course->visible ? 'Active' : 'Inactive';
            if ($course_setting->course_library == 1) {
                $content .= "<a href=\"$edit\"><span class=\"box-tag box-tag-purply\">Library</span></a>";
            }
            if ($course_setting->sell_course == 1) {
                $content .= "<a href=\"$edit\"><span class=\"box-tag box-tag-yellow\">" . $course_setting->currency . " " . $course_setting->fee . "</span></a>";
            }
            $content .= "<a href=\"$edit\"><span class=\"box-tag box-tag-green\">$active</span></a>";
            $content .="</div>"; // End right col
            $content .="</li>";
        }
        $content .="</ul>";
        $content .="<div class=\"courses-found\">$finded " . get_string('coursefound', 'moodle') . "  </div>";
        break;
    case 'searchPeople':
        //Tung added search people
        if (!is_teacher()) {
            $sql = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
            $params['ownerid'] = $USER->id;
            $rows = $DB->get_records_sql($sql, $params);
            foreach ($rows as $row) {
                $id_list .= $row->user_id . ",";
            }
            $id_list = $id_list . "0";
            $sql2 = "SELECT user_id FROM map_user_admin WHERE owner_id in (" . $id_list . ")";
            $rows2 = $DB->get_records_sql($sql2);
            foreach ($rows2 as $row2) {
                $id_list .= $row2->user_id . ",";
            }
            $id_list = $id_list . $USER->id;
            $users = get_users(true, $s, false, null, 'firstname ASC', '', '', '', '', '*', " id in (" . $id_list . ")", array(''));
        } else {
            $sql = "SELECT owner_id FROM map_user_admin where user_id = :userid";
            $params['userid'] = $USER->id;
            $rows = $DB->get_records_sql($sql, $params);
            foreach ($rows as $row) {
                $owner_id = $row->owner_id;
            }
            $sql2 = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
            $params['ownerid'] = $owner_id;
            $rows2 = $DB->get_records_sql($sql2, $params);
            foreach ($rows2 as $row2) {
                $id_list .= $row2->user_id . ",";
            }
            $id_list = $id_list . $owner_id;
//List People
            $users = get_users(true, $s, false, null, 'firstname ASC', '', '', '', '', '*', " id in (" . $id_list . ")", array(''));
        }
//end fix
        $content  = "<table id=\"tech-companies-1\" class=\"table table-striped table-hover\">";
        $content .= "<thead>
                        <tr>
                            <th data-priority=\"6\">Họ tên</th>
                            <th data-priority=\"6\">Email</th>
                            <th data-priority=\"3\">Số điện thoại</th>
                            <th data-priority=\"6\">Công ty</th>
                            <th data-priority=\"6\">Địa chỉ</th>
                            <th data-priority=\"3\">Trạng thái</th>
                            <th class=\"text-right\">Hành động</th>
                        </tr>
                    </thead>";
        $content .= "<tbody>";
        $finded = 0;
        foreach ($users as $user) {
            $finded++;
            $content .="<tr>";
            $content .="<td>" . $user->firstname . ' ' . $user->lastname ."</td>";
            $content .="<td>" . $user->email ."</td>";
            $content .="<td>" . $user->phone2 ."</td>";
            $content .="<td>" . $user->company ."</td>";

            $xaphuong = get_info_xaphuong($user->id_wards);
            $quanhuyen = get_info_quanhuyen($user->id_district);
            $tinhthanh = get_info_tinhthanh($user->city);
            $content .="<td>" . $user->address .' - '. $xaphuong->name.' - '. $quanhuyen->name.' - '. $tinhthanh->name ."</td>";
            
            $active = $user->confirmed ? get_string('active') : get_string('inactive');
            $activetitle = $user->confirmed ? 'badge-success' : 'badge-danger';

            // $content .= '<span class="box-tag box-tag-yellow" title="Vai trò">' . people_get_roles($user->id) . '</span>';
            $content .='<td><span class="badge label-table ' . $activetitle . '" title="Trạng thái">' . $active . '</span></td>';
            $content .='<td class="text-right">';

            $edit = new moodle_url('/manage/people/edit.php',array('user_id'=>$user->id));
            $view =  new moodle_url('/manage/people/profile.php', array('id' => $user->id));
            $remove =  new moodle_url('?action=delete',array('id'=>$user->id));

            $content .= "<a href=\"$edit\" class=\"btn btn-info\"><i class=\"fa fa-pencil\"></i></a>";
            $content .= '<a href="' . $view . '" class="btn btn-warning"><i class="fa fa-eye"></i></a>';
            $content .= '<a href="' . $remove . '" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $content .='</td>';
        }
        $content .="</tbody>";
        $content .="</table>";
        
        $content .="</div>"; // End div rearchReasult
        $content .="<p class='text-info'>" . $finded . " " . get_string('peoplefund') . "</p>";
        break;
    case 'searchTeam':
        $teams = manage_get_team_list($s);

        $content = "<ul>";
        $finded = 0;
        foreach ($teams as $team) {
            $finded ++;
            $content .="<li>";

            $content .="<div class=\"main-col\">";

            $content . "<div class=\"title\">";
            $link = new moodle_url('/manage/teams.php', array('id' => $team->id));
            $content .= "<a href=\"$link\"><span class=\"nav-reports\">" . $team->name . "</span></a>";
            $content .= "</div>"; // End main col
            $content .="</li>";
        }
        $content .="</ul>";
        $content .="<div class=\"courses-found\">$finded groups found</div>";

        break;

        // Tìm kiếm phụ huynh học sinh
    case 'searchParent':
        $parent = get_parent($s);
        $content  = "<table id=\"tech-companies-1\" class=\"table table-striped table-hover\">";
        $content .= "<thead>
                        <tr>
                            <th data-priority=\"6\">Họ tên</th>
                            <th data-priority=\"6\">Email</th>
                            <th data-priority=\"3\">Số điện thoại</th>
                            <th data-priority=\"6\">Công ty</th>
                            <th data-priority=\"6\">Địa chỉ</th>
                            <th data-priority=\"3\">Trạng thái</th>
                            <th class=\"text-right\">Hành động</th>
                        </tr>
                    </thead>";
        $content .= "<tbody>";
        $finded = 0;
        foreach ($parent as $user) {
            $finded++;
            $content .="<tr>";
            $content .="<td>" . $user->firstname . ' ' . $user->lastname ."</td>";
            $content .="<td>" . $user->email ."</td>";
            $content .="<td>" . $user->phone2 ."</td>";
            $content .="<td>" . $user->company ."</td>";
            $content .="<td>" . $user->address ."</td>";
            
            $active = $user->confirmed ? get_string('active') : get_string('inactive');
            $activetitle = $user->confirmed ? 'badge-success' : 'badge-danger';

            // $content .= '<span class="box-tag box-tag-yellow" title="Vai trò">' . people_get_roles($user->id) . '</span>';
            $content .='<td><span class="badge label-table ' . $activetitle . '" title="Trạng thái">' . $active . '</span></td>';
            $content .='<td class="text-right">';

            $edit = new moodle_url('/manage/people/edit.php',array('user_id'=>$user->id));
            $view =  new moodle_url('/manage/people/profile.php', array('id' => $user->id));
            $remove =  new moodle_url('?action=delete',array('id'=>$user->id));

            $content .= "<a href=\"$edit\" class=\"btn btn-info\"><i class=\"fa fa-pencil\"></i></a>";
            $content .= '<a href="' . $view . '" class="btn btn-warning"><i class="fa fa-eye"></i></a>';
            $content .= '<a href="' . $remove . '" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            $content .='</td>';
        }
        $content .="</tbody>";
        $content .="</table>";
        
        $content .="</div>"; // End div rearchReasult
        $content .="<p class='text-info'>" . $finded . " " . get_string('peoplefund') . "</p>";
        break;
}

echo ($content);
?>