<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php'); 

$PAGE->set_title(get_string('add_unitest'));
$PAGE->set_heading(get_string('add_unitest'));
$PAGE->set_pagelayout(get_string('add_unitest'));
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
if ($action=='add_unittest') {
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	$groupid = optional_param('groupid', '', PARAM_TEXT);
	$studentid = optional_param('studentid', '', PARAM_TEXT);
	$number = optional_param('number', '', PARAM_TEXT);
	$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
	$month = optional_param('month', '', PARAM_TEXT);
	$note = optional_param('note', '', PARAM_TEXT);
	$score = optional_param('score', '', PARAM_TEXT);
	$check=check_unit_test_user($studentid,$school_yearid,$month);
	if(!empty($check)){
		echo displayJsAlert(get_string('issetunit'), $CFG->wwwroot . "/manage/manage_score/editunit.php?idedit=".$check);
		?>
		<!-- <script>
			var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/editunit.php?idedit=<?php echo $check;?>";
			var e=confirm("<?php echo get_string('issetunit'); ?>");
			if(e==true){
				window.location=urlweb;
			}
		</script> -->
		<?php 
	}else{
		$newid=save_unittest($studentid,$courseid,$groupid,$school_yearid,$month,$number);
		if(!empty($newid)){
			foreach ($score as $key => $value) {
				save_note_score($newid,$key+1,$note[$key],$score[$key]);
			}
			echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/index.php");
		}else{
			echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/manage_score/add_unittest.php");
		}
	}
	// $newid=save_unittest($studentid,$courseid,$groupid,$school_yearid,$month);
	

}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<!-- <div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Tìm kiếm học sinh</label>
								<input type="text" id="key" name="key" class="form-control" value="">
							</div>
							<div class="" id="return_student">
								
							</div>
						</div> -->
						<input type="text" name="action" value="add_unittest" hidden="">
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control" required>
								<option value=""><?php echo get_string('schools'); ?></option>
								<?php 
								$schools = get_all_shool();
								foreach ($schools as $key => $value) {
                        # code...
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control" required>
								<option value=""><?php echo get_string('block_student'); ?></option>
							</select>
						</div>

						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value=""><?php echo get_string('class'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value=""><?php echo get_string('classgroup'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
							<select name="studentid" id="studentid" class="form-control" required>
								<option value=""><?php echo get_string('student'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('number_of_component_points'); ?><span style="color:red">*</span></label>(Tối đa 12 điểm)
								<input type="number" id="number" name="number" class="form-control" value="" min="1" max="12" required="">
							</div>
						</div>
						
						<div class="col-md-12" id="return_number">
							
						</div>
						<div class="col-md-12">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var groupid = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var courseid = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+courseid,function(data){
				$("#studentid").html(data);
			});
		}
	}); 
	$('#number').on('change keyup', function() {
		var sanitized = $(this).val();
		if(sanitized<13){
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?number="+sanitized,function(data){
				$("#return_number").html(data);
			});
		}
	});
</script>