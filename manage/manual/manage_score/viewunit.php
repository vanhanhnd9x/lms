<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');

$PAGE->set_title(get_string('view_unitest'));
$PAGE->set_heading(get_string('view_unitest'));
//now the page contents
$PAGE->set_pagelayout(get_string('view_unitest'));
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idview = optional_param('idview', '', PARAM_TEXT);


$unittest=$DB->get_record('unittest', array(
	'id' => $idview
));
$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
$school_year=$DB->get_record('school_year', array(
	'id' => $unittest->school_yearid
));
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="edit_unit" hidden="">
						<?php $info=show_info_student_in_unit($unittest->userid,$unittest->courseid,$unittest->groupid); ?>

						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?></label>
								<input type="text" class="form-control" value="<?php echo $school_year->sy_start,' - ',$school_year->sy_end; ?>" disabled="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('month'); ?></label>
								<input type="text" value="<?php echo show_month_in_unit( $unittest->month); ?>" class="form-control" disabled>
							</div>
						</div>
						<?php 
						if (!empty($unittest->number)) {
								# code...
							switch ($unittest->number) {
								case 1:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								break;
								case 2:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								break;
								case 3:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								break;
								case 4:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								break;
								case 5:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								break;
								case 6:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								break;
								case 7:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								$ghichu[6]=$unittest->note_7;
								$diem[6]=$unittest->score_7;
								break;
								case 8:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								$ghichu[6]=$unittest->note_7;
								$diem[6]=$unittest->score_7;
								$ghichu[7]=$unittest->note_8;
								$diem[7]=$unittest->score_8;
								break;
								case 9:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								$ghichu[6]=$unittest->note_7;
								$diem[6]=$unittest->score_7;
								$ghichu[7]=$unittest->note_8;
								$diem[7]=$unittest->score_8;
								$ghichu[8]=$unittest->note_9;
								$diem[8]=$unittest->score_9;
								break;
								case 10:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								$ghichu[6]=$unittest->note_7;
								$diem[6]=$unittest->score_7;
								$ghichu[7]=$unittest->note_8;
								$diem[7]=$unittest->score_8;
								$ghichu[9]=$unittest->note_10;
								$diem[9]=$unittest->score_10;
								break;
								case 11:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								$ghichu[6]=$unittest->note_7;
								$diem[6]=$unittest->score_7;
								$ghichu[7]=$unittest->note_8;
								$diem[7]=$unittest->score_8;
								$ghichu[9]=$unittest->note_10;
								$diem[9]=$unittest->score_10;
								$ghichu[10]=$unittest->note_11;
								$diem[10]=$unittest->score_11;
								break;
								case 12:
								$ghichu[0]=$unittest->note_1;
								$diem[0]=$unittest->score_1;
								$ghichu[1]=$unittest->note_2;
								$diem[1]=$unittest->score_2;
								$ghichu[2]=$unittest->note_3;
								$diem[2]=$unittest->score_3;
								$ghichu[3]=$unittest->note_4;
								$diem[3]=$unittest->score_4;
								$ghichu[4]=$unittest->note_5;
								$diem[4]=$unittest->score_5;
								$ghichu[5]=$unittest->note_6;
								$diem[5]=$unittest->score_6;
								$ghichu[6]=$unittest->note_7;
								$diem[6]=$unittest->score_7;
								$ghichu[7]=$unittest->note_8;
								$diem[7]=$unittest->score_8;
								$ghichu[9]=$unittest->note_10;
								$diem[9]=$unittest->score_10;
								$ghichu[10]=$unittest->note_11;
								$diem[10]=$unittest->score_11;
								$ghichu[11]=$unittest->note_12;
								$diem[11]=$unittest->score_12;
								break;
							}
							for ($i=0; $i < $unittest->number; $i++) {

								?>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('score') ?> <?php echo $i +1;?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label"><?php echo get_string('note') ?></label>
										<input type="text" id="note" name="note[]" class="form-control" value="<?php echo $ghichu[$i];  ?>" disabled>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label"><?php echo get_string('score') ?></label>
										<input type="text" id="score" name="score[]" class="form-control" value="<?php echo $diem[$i];  ?>" disabled>
									</div>
								</div>
								<?php 
							}
							
						}
						?>
						<div class="col-md-12">
							<div class="form-group label-floating">
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
							</div>
						</div>


					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
