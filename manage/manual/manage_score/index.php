<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('list_unitest'));
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$number=20;
$page=isset($_GET['page'])?$_GET['page']:1;
if($action=='search'){
  $schoolid = trim(optional_param('schoolid', '', PARAM_TEXT));
  $block_student = trim(optional_param('block_student', '', PARAM_TEXT));
  $groupid = trim(optional_param('groupid', '', PARAM_TEXT));
  $name = trim(optional_param('name', '', PARAM_TEXT));
  $month = trim(optional_param('month', '', PARAM_TEXT));
  $school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
  $data=search_unittest($schoolid,$block_student,$groupid,$name,$month,$school_yearid,$page,$number);
  $url=$CFG->wwwroot.'/manage/manage_score/index.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&groupid='.$groupid.'&name='.$name.'&month='.$month.'&school_yearid='.$school_yearid.'&page=';
}else{
  $data=get_all_unittest_admin($page,$number);
  $url=$CFG->wwwroot.'/manage/manage_score/index.php?page=';
}




?>


<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/manage_score/add_unittest.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a> 
          </div>
          <div class="col-md-10">
           <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
             <div class="col-4">
              <label for=""><?php echo get_string('schools'); ?></label>
              <select name="schoolid" id="schoolid" class="form-control">
               <option value=""><?php echo get_string('schools'); ?></option>
               <?php 
               $schools = get_all_shool();
               foreach ($schools as $key => $value) {
                ?>
                <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                <?php 
              }
              ?>
            </select>
          </div>
          <div class="col-4">
            <label for=""><?php echo get_string('block_student'); ?></label>
            <select name="block_student" id="block_student" class="form-control">
             <option value=""><?php echo get_string('block_student'); ?></option>
           </select>
         </div>
         <div class="col-4">
          <label for=""><?php echo get_string('class'); ?></label>
          <select name="groupid" id="groupid" class="form-control">
            <option value=""><?php echo get_string('class'); ?></option>
          </select>
        </div>
        <div class="col-4">
          <label for=""><?php echo get_string('namestudent'); ?></label>
          <input type="text" class="form-control" name="name" placeholder="Tên học sinh" value="<?php echo $name; ?>">
        </div>
        <div class="col-4">
          <label for=""><?php echo get_string('choose_month'); ?></label>
          <select name="month" id="" class="form-control">
           <option value="">Chọn tháng</option>
           <option value="1" <?php if($month==1) echo 'selected'; ?>><?php echo get_string('january'); ?></option>
           <option value="2" <?php if($month==2) echo 'selected'; ?>><?php echo get_string('february'); ?></option>
           <option value="3" <?php if($month==3) echo 'selected'; ?>><?php echo get_string('march'); ?></option>
           <option value="4" <?php if($month==4) echo 'selected'; ?>><?php echo get_string('april'); ?></option>
           <option value="5" <?php if($month==5) echo 'selected'; ?>><?php echo get_string('may'); ?></option>
           <option value="6" <?php if($month==6) echo 'selected'; ?>><?php echo get_string('june'); ?></option>
           <option value="7" <?php if($month==7) echo 'selected'; ?>><?php echo get_string('july'); ?></option>
           <option value="8" <?php if($month==8) echo 'selected'; ?>><?php echo get_string('august'); ?></option>
           <option value="9" <?php if($month==9) echo 'selected'; ?>><?php echo get_string('september'); ?></option>
           <option value="10" <?php if($month==10) echo 'selected'; ?>><?php echo get_string('october'); ?></option>
           <option value="11" <?php if($month==11) echo 'selected'; ?>><?php echo get_string('november'); ?></option>
           <option value="12" <?php if($month==12) echo 'selected'; ?>><?php echo get_string('december'); ?></option>
         </select>
       </div>
       <div class="col-4">
        <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
        <select name="school_yearid" id="" class="form-control">
         <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
         <?php 
         $sql="SELECT * FROM `school_year`";
         $school_year = $DB->get_records_sql($sql);
         foreach ($school_year as $key => $value) {
           # code...
          ?>
          <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
          <?php 
         }
         ?>
       </select>
     </div>
     <div class="col-4">
      <div style="    margin-top: 29px;"></div>
      <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
    </div>
  </form>
</div>
</div>

<div class="table-responsive" data-pattern="priority-columns" id="searchResults">
  <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
    <table id="tech-companies-1" class="table table-hover">
      <thead>
        <tr>
          <th>STT</th>
          <th data-priority="6"><?php echo get_string('namestudent'); ?></th>
          <th data-priority="6"><?php echo get_string('schools'); ?></th>
          <th data-priority="6"><?php echo get_string('class'); ?></th>
          <th data-priority="6"><?php echo get_string('classgroup'); ?></th>
          <th data-priority="6"><?php echo get_string('school_year'); ?></th>
          <th data-priority="6"><?php echo get_string('month'); ?></th>
          <th data-priority="6"><?php echo get_string('score'); ?></th>
          <th class="disabled-sorting text-right"><?php echo get_string('action'); ?></th>
        </tr>
      </thead>
      <tbody id="ajaxschools">
        <?php 
        $i=0;
        if($data['data']){
          foreach ($data['data'] as $unit) { 
            $i++;
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <?php 
              $info=get_info_student_in_unit($unit->userid,$unit->courseid,$unit->groupid,$unit->school_yearid);
              ?>
              <td><?php show_month_in_unit($unit->month);?></td>
              <td><?php echo $unit->score_1; ?></td>
              <td class="text-right">
                <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editunit.php?idedit=<?php echo $unit->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewunit.php?idview=<?php echo $unit->id; ?>" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                <a href="javascript:void(0)" onclick="delet_unittest(<?php echo $unit->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
            </tr>
          <?php }
        }
        ?>
      </tbody>
    </table>
  </form>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>
</div>
</div>
</div>
</div>
<?php

echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script>
  function delet_unittest(iddelete){
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php";
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('<?php echo get_string('del_unitest'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete:iddelete} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>
<script>
  var school = $('#schoolid').find(":selected").val();
  if (school) {
    var blockedit = "<?php echo $block_student; ?>";
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    if(blockedit){
      var groupedit="<?php echo $groupid; ?>";
      // var courseedit = "<?php echo $course->id; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
        $("#groupid").html(data);
      });
    }
    if(groupedit){
      var courseedit="<?php echo $idcourse; ?>";
      if(courseedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
        $("#courseid").html(data);
      });
     }else{
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
        $("#courseid").html(data);
      });
    }
  }
}
  // ajax add
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
        $("#groupid").html('<option value="">Chọn lớp</option>');
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
        $("#courseid").html(data);
      });
    }
  });
</script>