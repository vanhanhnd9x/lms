<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('elementary_semester_score'));
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);

$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$action = optional_param('action', '', PARAM_TEXT);
if ($action=='search') {
  $schoolid = optional_param('schoolid', '', PARAM_TEXT);
  $block_student = optional_param('block_student', '', PARAM_TEXT);
  $groupid = optional_param('groupid', '', PARAM_TEXT);
  $name = trim(optional_param('name', '', PARAM_TEXT));
  $school_year = optional_param('school_year', '', PARAM_TEXT);
  $school_status =1;
  $data=search_semester($schoolid,$block_student,$groupid,$name,$school_year,$school_status,$page,$number);
  $url= $CFG->wwwroot.'/manage/manage_score/semester_score.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&groupid='.$groupid.'&name='.$name.'&school_year='.$school_year.'&page=';
}else{
  $data=get_all_semester_tieuhoc($page,$number);
  $url=$CFG->wwwroot.'/manage/manage_score/semester_score.php?page=';
}
?>


<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/manage_score/add_semeste.php?school=1'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a> 
            <p style="margin-top: 10px"></p>
            <a href="<?php echo new moodle_url('/manage/manage_score/import_semester.php?school=1'); ?>" class="btn btn-info"><?php echo get_string('import_excel'); ?></a> 
          </div>
          <div class="col-md-10">
           <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
             <div class="col-4">
              <label for=""><?php echo get_string('schools'); ?></label>
              <select name="schoolid" id="schoolid" class="form-control">
               <option value=""><?php echo get_string('schools'); ?></option>
               <?php 
               $schools = get_all_shool();
               foreach ($schools as $key => $value) {
                ?>
                <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                <?php 
              }
              ?>
            </select>
          </div>
          <div class="col-4">
            <label for=""><?php echo get_string('block_student'); ?></label>
            <select name="block_student" id="block_student" class="form-control">
             <option value=""><?php echo get_string('block_student'); ?></option>
           </select>
         </div>
         <div class="col-4">
          <label for=""><?php echo get_string('class'); ?></label>
          <select name="groupid" id="groupid" class="form-control">
            <option value=""><?php echo get_string('class'); ?></option>
          </select>
        </div>
        <div class="col-4">
          <label for=""><?php echo get_string('namestudent'); ?></label>
          <input type="text" class="form-control" name="name" placeholder="Tên học sinh" value="<?php echo $name; ?>">
        </div>
        <div class="col-4">
          <label for=""><?php echo get_string('school_year'); ?></label>
          <select name="school_year" id="" class="form-control" >
           <option value=""><?php echo get_string('school_year'); ?></option>
           <?php 
           if (function_exists('get_all_school_year_in_semester')) {
                        # code...
            $school_year1=get_all_school_year_in_semester();
            if($school_year1){
              foreach ($school_year1 as $key => $value) {
                                # code...
                ?>
                <option value="<?php echo $value->id ; ?>"  <?php if (!empty($school_year)&&$school_year==$value->id) { echo'selected'; } ?>><?php echo $value->sy_start,' - ',$value->sy_end ; ?></option>
                <?php 
              }
            }
          }
          ?>
        </select>
      </div>
      <div class="col-4">
        <div style="    margin-top: 29px;"></div>
        <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
      </div>
    </form>
  </div>
</div>



<div class="table-responsive" data-pattern="priority-columns">
    <table id="tech-companies-1" class="table  table-striped table-hover">
      <thead>
        <tr>
          <th>STT</th>
          <th data-priority="6"><?php echo get_string('namestudent'); ?></th>
          <th data-priority="6"><?php echo get_string('speaking'); ?></th>
          <th data-priority="6"><?php echo get_string('listening'); ?></th>
          <th data-priority="6"><?php echo get_string('readingWriting'); ?></th>
          <th data-priority="6"><?php echo get_string('totalMidtermMark1'); ?></th>
          <th data-priority="6"><?php echo get_string('school_year'); ?></th>
          <th class="disabled-sorting text-right"><?php echo get_string('action'); ?></th>
        </tr>
      </thead>
      <tfoot>
      </tfoot>
      <tbody id="ajaxschools">
        <?php 
        if($data['data']){
         $i=0;
         foreach ($data['data'] as $sch) { 
          $i++;
          $student=$DB->get_record('user', array('id' => $sch->userid));
          $username= $student->lastname.' '.$student->firstname;
          $namehoc=$DB->get_record('school_year', array('id' => $sch->school_yearid));
          ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php  echo $username;?></td>
             <td><?php echo $sch->noi_1; ?></td>
            <td><?php echo $sch->nghe_1; ?></td>
            <td><?php echo $sch->doc_viet_1; ?></td>
            <td><?php echo $sch->tonghocki_1; ?></td>
            <td><?php echo $namehoc->sy_start,' - ',$namehoc->sy_end ; ?></td>
            <td class="text-right">
              <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $sch->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
              <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewsemester.php?idview=<?php echo $sch->id; ?>" class="btn btn-warning" title="Xem chi tiết">
                <i class="fa fa-eye" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)" onclick="delet_semester(<?php echo $sch->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
          </tr>
          <?php 
        }
      }else{
        echo'
        <tr><td colspan="8" style="text-align: center;">Dữ liệu trống</td></tr>';
      }

      ?>
    </tbody>
  </table>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php

echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script>
  function delet_semester(iddelete_semester){
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/semester_score.php";
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('<?php echo get_string('delete_semester'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete_semester:iddelete_semester} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>
<script>
  var school = $('#schoolid').find(":selected").val();
  if (school) {
    var blockedit = "<?php echo $block_student; ?>";
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    if(blockedit){
      var groupedit="<?php echo $groupid; ?>";
      // var courseedit = "<?php echo $course->id; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
        $("#groupid").html(data);
      });
    }
    if(groupedit){
      var courseedit="<?php echo $idcourse; ?>";
      if(courseedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
        $("#courseid").html(data);
      });
     }else{
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
        $("#courseid").html(data);
      });
    }
  }
}
  // ajax add
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
        $("#groupid").html('<option value="">Chọn lớp</option>');
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
        $("#courseid").html(data);
      });
    }
  });
</script>