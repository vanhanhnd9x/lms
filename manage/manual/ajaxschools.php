<?php 
require_once ("../../config.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
if (isset($_GET['city'])) {
	$city = $_GET['city'];
	$schools = $DB->get_records('schools', array('city' => $city));
	if (isset($_GET['district'])) {
		$district = $_GET['district'];
		$schools = $DB->get_records('schools', array('city' => $city, 'district' => $district));
		if (isset($_GET['commune'])) {
			$commune = $_GET['commune'];
			$schools = $DB->get_records('schools', array('city' => $city, 'commune' => $commune));
		}
	}
}

if(isset($_GET['key'])){
	$key = $_GET['key'];
	$schools = $DB->get_records_sql("SELECT * FROM schools WHERE `name` LIKE '%$key%' OR `phone` LIKE '%$key%' OR `representative_name` LIKE '%$key%' OR `email` LIKE '%$key%'");
}
$data = "";
foreach ($schools as $sch) {
	$xaphuong = get_info_xaphuong($sch->commune);
    $quanhuyen = get_info_quanhuyen($sch->district);
    $tinhthanh = get_info_tinhthanh($sch->city);
	$data .='
	<tr>
        <td><input type="checkbox" name="list[]" class="list" value="'. $sch->id. '"></td>
        <td>'.$sch->name.'</td>
        <td>'.$sch->representative_name.'</td>
        <td>'.$sch->address.' - '. $xaphuong->name.' - '. $quanhuyen->name.' - '. $tinhthanh->name .'</td>
        <td>'.$sch->email.'</td>
        <td>'.$sch->phone.'</td>
        <td class="text-right">
            <a href="'. new moodle_url('edit.php',array('sch_id'=>$sch->id)).'" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a href="'. new moodle_url('detail.php',array('sch_id'=>$sch->id)).'" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
            <a href="'. new moodle_url('?action=del',array('sch_id'=>$sch->id)).'" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>
    </tr>';
}

echo $data;
?>