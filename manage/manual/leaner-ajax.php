<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file processes AJAX enrolment actions and returns JSON for the manual enrolments plugin
 *
 * The general idea behind this file is that any errors should throw exceptions
 * which will be returned and acted upon by the calling AJAX script.
 *
 * @package    manage
 * @subpackage manual
 * @copyright  2012 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define('AJAX_SCRIPT', true);
require_once('../../config.php');
require_once($CFG->dirroot.'/manage/lib.php');

$s = required_param('s', PARAM_TEXT); // course id
$action = required_param('action', PARAM_ACTION);

 
switch ($action) {
    case 'searchCourse':
        $courses = search_enrol_get_my_courses_libary($s);
        
        $content .="<ul>";
        $finded = 0;
        foreach ($courses as $course) {
            $finded++;
            $content .="<li>";

            $content .="<div class=\"main-col\">";
            $content .= '<div class="icon-course-view">';
            $content .= '<a href="' . new moodle_url('/course/view.php', array('id' => $course->id)) .'" class="next-arrow"></a>';
            $content .= "</div>";
            $content .= "<div class=\"title\">";
            $link = new moodle_url('/course/view.php', array('id' => $course->id));
            $content .= "<a href=\"$link\"><span class=\"nav-reports\">$course->fullname</span></a>";
            $content .= "<div class=\"tip\">";
            $content .= $course->idnumber;
            $content .= "</div></div>"; // End main col

            $content .="<div class='right-col'>";

            $edit = new moodle_url('/course/view.php', array('id' => $course->id));
            $active = $course->visible ? 'Active' : 'Inactive';

            $content .= "<a href=\"$edit\"><span class=\"box-tag box-tag-purply\">Library</span></a>";
            $content .= "<a href=\"$edit\"><span class=\"box-tag box-tag-green\">$active</span></a>";
            $content .="</div>"; // End right col

            $content .="</li>";
        }
        $content .="</ul>";
        $content .="<div class=\"courses-found\">$finded ".get_string('coursefound','moodle')."</div>";
        break;
  
}
echo ($content);