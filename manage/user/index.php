<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/user/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php'); 


$PAGE->set_title(get_string('user'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$roleid = optional_param('roleid', '', PARAM_TEXT);
$lastname = trim(optional_param('lastname', '', PARAM_TEXT));
$firstname = trim(optional_param('firstname', '', PARAM_TEXT));
$phone1 = trim(optional_param('phone1', '', PARAM_TEXT));
$email = trim(optional_param('email', '', PARAM_TEXT));

$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
function search_to_user_in_schoolmanager_byCua($roleid=null,$lastname=null,$firstname=null,$phone1=null,$email=null,$page,$number){
    global $DB;
    if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
    }
    $sql=" 
    SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`role_assignments`.`roleid`,`user`.`description`,`user`.`sex`,`role`.`name`,`user`.`confirmed`
    FROM user 
    JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
    JOIN `role` ON `role`.`id` =   `role_assignments`.`roleid`
    WHERE  `user`.`del`=0 AND `role_assignments`.`roleid` IN (13,14,15,16,17,18,19,20,21) ";
    $sql_total=" 
    SELECT COUNT( DISTINCT `user`.`id`)
    FROM user 
    JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
    WHERE  `user`.`del`=0 AND `role_assignments`.`roleid` IN (13,14,15,16,17,18,19,20,21) ";
    if(!empty($roleid)){
        $sql.=" AND `role_assignments`.`roleid` ={$roleid}";
        $sql_total.=" AND `role_assignments`.`roleid` ={$roleid}";
    }
    if(!empty($lastname)){
        $sql.=" AND `user`.`lastname` LIKE '%{$lastname}%'";
        $sql_total.=" AND `user`.`lastname` LIKE '%{$lastname}%'";
    }
    if(!empty($firstname)){
        $sql.=" AND `user`.`firstname` LIKE '%{$firstname}%'";
        $sql_total.=" AND `user`.`firstname` LIKE '%{$firstname}%'";
    }
    if(!empty($phone1)){
        $sql.=" AND `user`.`phone1` LIKE '%{$phone1}%'";
        $sql_total.=" AND `user`.`phone1` LIKE '%{$phone1}%'";
    }
    if(!empty($email)){
        $sql.=" AND `user`.`email` LIKE '%{$email}%'";
        $sql_total.=" AND `user`.`email` LIKE '%{$email}%'";
    }
    $sql.=" ORDER BY `user`.`id` DESC LIMIT {$start} , {$number}";
    $data=$DB->get_records_sql($sql);
    $total_row=$DB->get_records_sql($sql_total);
    foreach ($total_row as $key => $value) {
        # code...
      $total_page=ceil((int)$key / (int)$number);
      break;
    }
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
return $kq;
}
$data=search_to_user_in_schoolmanager_byCua($roleid,$lastname,$firstname,$phone1,$email,$page,$number);
if ($action=='search') {
  
  $url= $CFG->wwwroot.'/manage/user/index.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&groupid='.$groupid.'&courseid='.$courseid.'&code='.$code.'&name='.$name.'&name_parent='.$name_parent.'&phone='.$phone.'&birthday='.$birthday.'&page=';
}else{
  $url= $CFG->wwwroot.'/manage/user/index.php?page=';
}
$vaitro=hien_thi_vai_tro_nguoi_dung();
// phan quyen
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='user';
$name1='user';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
$checkpopup=check_chuc_nang_pop_up($roleid,$moodle);

?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <?php 
                        if(!empty($checkthemmoi)){
                            ?>
                            <div class="col-md-2">
                                <a href="<?php echo new moodle_url('/manage/user/add_new.php'); ?>" class="btn btn-success">
                                    <?php echo get_string('new'); ?></a>
                            </div>
                            <?php 
                        }
                     ?>
                    
                    <div class="col-md-10 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <input type="" name="action" class="form-control" placeholder="Nhập tên..." value="search" hidden="">
                            <div class="col-4 form-group">
                                <label for=""><?php echo get_string('role'); ?> </label>
                                <select name="roleid" id="schoolid" class="form-control">
                                    <option value=""><?php echo get_string('role'); ?></option>
                                    <?php 
                                        foreach ($vaitro as $value) {
                                    ?>
                                        <option value="<?php echo $value['id']; ?>" <?php if (!empty($roleid)&&$roleid==$value['id']) { echo'selected'; } ?>>
                                            <?php echo $value['name']; ?>
                                        </option>
                                    <?php 
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-4 form-group">
                                <label for=""><?php echo get_string('lastname'); ?> </label>
                                <input type="text" name="lastname" class="form-control" value="<?php echo $lastname; ?>" placeholder="<?php echo get_string('lastname'); ?>">
                            </div>
                            <div class="col-4 form-group">
                                <label for=""><?php echo get_string('firstname'); ?> </label>
                                <input type="text" name="firstname" class="form-control" value="<?php echo $firstname; ?>" placeholder="<?php echo get_string('firstname'); ?>">
                            </div>
                            <div class="col-4 form-group">
                                <label for=""><?php echo get_string('phone'); ?> </label>
                                <input type="text" name="phone1" class="form-control" value="<?php echo $phone1; ?>" placeholder="<?php echo get_string('mobileph'); ?>">
                            </div>
                             <div class="col-4 form-group">
                                <label for=""><?php echo get_string('email_new'); ?> </label>
                                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" placeholder="<?php echo get_string('email'); ?>">
                            </div>
                            <div class="col-4 form-group">
                                <div style="    margin-top: 29px;"></div>
                                <button type="submit" class="btn btn-success">
                                    <?php echo get_string('search'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive" data-pattern="priority-columns">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)||!empty($checkpopup)){
                                        echo'<th class="text-right">'.get_string('action').'</th>';
                                    }
                                 ?>
                                <th>STT</th>
                               
                                <th>
                                    <?php echo get_string('lastname'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('firstname'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('sex'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('birthday'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('phone'); ?>
                                </th>
                                 <th>
                                    <?php echo get_string('email_new'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('username_new'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('role'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('note'); ?>
                                </th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (!empty($data['data'])) {
                                                    # code...
                              $i=0;
                              foreach ($data['data'] as $user) { 
                                $i++;
                                ?>
                            <tr>
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)||!empty($checkpopup)){
                                        echo' <td class="text-right">';
                                        $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$user->id);
                                        if(!empty($checkpopup)){
                                            if($user->confirmed==1){ ?> 
                                            <a href="javascript:void(0)" onclick="de_activated_user_by_cua(<?php echo $user->id;?>);" class="mauicon tooltip-animation" title="Khóa người dùng"><i class="fa fa-lock"></i></a>
                                            <?php }else{ ?>
                                            <a href="" class="mauicon tooltip-animation" onclick="activated_user_by_cua(<?php echo $user->id;?>);"><i class="fa fa-unlock" title="Mở khóa người dùng"></i></a>
                                        <?php 
                                            }
                                        }
                                        if(!empty($checkdelete)){
                                           ?>
                                           <a href="javascript:void(0)" onclick="delete_user(<?php echo $user->id;?>);" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                           <?php 
                                        }
                                        echo'</td>';
                                    }
                                 ?>
                                <td>
                                    <?php echo  $user->lastname;?>
                                </td>
                                <td>
                                    <?php echo $user->firstname; ?>
                                </td>
                                <td>
                                    <?php 
                                        switch ($user->sex) {
                                            case 1:echo get_string('women'); break;
                                            case 2:echo get_string('man'); break;
                                        }
                                     ?>
                                </td>
                                <td>
                                    <?php echo  $user->birthday;?>
                                </td>
                                <td>
                                    <?php echo  $user->phone1;?>
                                </td>
                                <td>
                                    <?php echo  $user->email;?>
                                </td>
                                <td>
                                    <?php echo  $user->username;?>
                                </td>
                                 <td>
                                    <?php echo  $user->name;?>
                                </td>
                                <td>
                                    <?php echo  $user->description;?>
                                </td>
                                
                                <!-- <td class="text-right">
                                    
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/user/edit.php?id_edit=<?php echo $user->id ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <?php if($user->confirmed==1){ ?> 
                                        <a href="javascript:void(0)" onclick="de_activated_user_by_cua(<?php echo $user->id;?>);" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>
                                        <?php }else{ ?>
                                        <a href="" class="btn btn-info btn-sm" onclick="activated_user_by_cua(<?php echo $user->id;?>);"><i class="fa fa-unlock"></i></a>
                                        <?php } ?>
                                    <a href="javascript:void(0)" onclick="delete_user(<?php echo $user->id;?>);" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td> -->
                            </tr>
                            <?php }
                        }
                        ?>
                        </tbody>
                    </table>
                    <?php 
                    if ($data['total_page']) {
                      if ($page > 2) { 
                        $startPage = $page - 2; 
                      } else { 
                        $startPage = 1; 
                      } 
                      if ($data['total_page'] > $page + 2) { 
                        $endPage = $page + 2; 
                      } else { 
                        $endPage =$data['total_page']; 
                      }
                    }
                     ?>
                    <p>
                        <?php echo get_string('total_page'); ?>:
                        <?php echo $data['total_page'] ?>
                    </p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                            for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                            # code...
                              ?>
                            <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>">
                                    <?php echo $i; ?></a></li>
                            <?php 
                            }
                            ?>
                            <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
    echo $OUTPUT->footer();
?>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<style>
    /*.mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }*/
</style>
<script>
function delete_user(iddelete) {
    var urlweb = "<?php echo $CFG->wwwroot ?>/manage/user/index.php";
    var urldelte = "<?php echo $CFG->wwwroot ?>/manage/user/load_ajax.php";
    var e = confirm('<?php echo get_string('delete_user'); ?>');
    if (e == true) {
        $.ajax({
            url: urldelte,
            type: "post",
            data: { iddelete: iddelete },
            success: function(response) {
                window.location = urlweb;
            }
        });
    }
}
function activated_user_by_cua(idactive){
    var urlweb = "<?php echo $CFG->wwwroot ?>/manage/user/index.php";
    var urlactive= "<?php echo $CFG->wwwroot ?>/manage/user/load_ajax.php";
    var e = confirm('<?php echo get_string('activeusers'); ?>');
    if (e == true) {
        $.ajax({
            url: urlactive,
            type: "post",
            data: { idactive: idactive },
            success: function(response) {
                window.location = urlweb;
            }
        });
    }
}
function de_activated_user_by_cua(iddeactive){
    var urlweb = "<?php echo $CFG->wwwroot ?>/manage/user/index.php";
    var urdeactivee = "<?php echo $CFG->wwwroot ?>/manage/user/load_ajax.php";
    var e = confirm('<?php echo get_string('de_activeusers'); ?>');
    if (e == true) {
        alert(iddeactive);
        $.ajax({
            url: urdeactivee,
            type: "post",
            data: { iddeactive: iddeactive },
            success: function(response) {
                window.location = urlweb;
            }
        });
    }
}
</script>