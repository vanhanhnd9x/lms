<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
if (!empty($_GET['username'])) {
	# code...
	$username = $DB->get_records('user', array('username'=>$_GET['username']));
	if (!empty($username)) {
		# code...
		echo'<div class="alert-danger">
		'.get_string('error_acount_student').'
		</div>';
		?>
		<script>
			$('#submitBtn').attr('disabled', true);
		</script>
		<?php 
	}else{
		?>
		<script>
			$('#submitBtn').attr('disabled', false);
		</script>
		<?php 
	}
}
function khoa_mo_khoa_user($iduser,$confirmed){
	global $DB;
 	$record = new stdClass();
  	$record->id = $iduser;
 	$record->confirmed = trim($confirmed);
 	return $DB->update_record('user', $record, false);

}
if(!empty($_POST['iddelete'])){
	$iddelete=$_POST['iddelete'];
	delete_student($iddelete);
}
if(!empty($_POST['iddeactive'])){
	$lock=0;
	$iduser=$_POST['iddeactive'];
	khoa_mo_khoa_user($iduser,$lock);
}
if(!empty($_POST['idactive'])){
	$lock=1;
	$iduser=$_POST['idactive'];
	khoa_mo_khoa_user($iduser,$lock);
}

