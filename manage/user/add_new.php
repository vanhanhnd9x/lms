<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/user/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
$PAGE->set_title(get_string('addnewuser'));
$PAGE->set_heading(get_string('addnewuser'));
$PAGE->set_pagelayout(get_string('addnewuser'));
require_login(0, false);
echo $OUTPUT->header();

?>
<?php
$action = optional_param('action', '', PARAM_TEXT);
$lastname = trim(optional_param('lastname', '', PARAM_TEXT));
$firstname = trim(optional_param('firstname', '', PARAM_TEXT));
$birthday = trim(optional_param('birthday', '', PARAM_TEXT));
$sex = (optional_param('sex', '', PARAM_TEXT));
$role = (optional_param('role', '', PARAM_TEXT));
$email = trim(optional_param('email', '', PARAM_TEXT));
$phone1 = (optional_param('phone1', '', PARAM_TEXT));
$username = trim(optional_param('username', '', PARAM_TEXT));
$password = trim(optional_param('password', '', PARAM_TEXT));
$address = trim(optional_param('address', '', PARAM_TEXT));
$note = trim(optional_param('note', '', PARAM_TEXT));
$error='';

if ($action=='addnewuser') {
  $new_user_id=add_user_by_cua_2($firstname,$lastname,$email,$phone1,$username,$password,$address,$birthday,$sex,$note);
  if(!empty($new_user_id)){
    $context = get_context_instance(CONTEXT_SYSTEM);
    role_assign($role, $new_user_id, $context->id);
    add_logs('user','add','/manage/user/edit.php?id_edit='.$new_user_id->id,$lastname.' '.$firstname); 
    echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/user/index.php");
    # code...
  }else{
     echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/user/add_new.php");
  }
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='user';
$name1='addnewuser';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$vaitro=hien_thi_vai_tro_nguoi_dung();
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">
        <form id="profile" action="" onsubmit="return validate();" method="post">
          <input type="text" hidden="" name="action" value="addnewuser">
            <div class="row">
              <div class="col-md-6 form-group">
              <label class="control-label"><?php print_string('lastname');?><span style="color:red">*</span></label>
              <input class="form-control" type="text" name="lastname" required placeholder="<?php print_string('lastname');?> ">
            </div>
            <div class="col-md-6 form-group">
              <label class="control-label"><?php print_string('firstname');?><span style="color:red">*</span></label>
              <input class="form-control" type="text" name="firstname" required placeholder="<?php print_string('firstname');?>">
            </div>
            <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('birthday');?></label>
               <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                <div class="input-group-addon">
                  <span class="glyphicon glyphicon-th"></span>
                </div>
                <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->birthday; ?>">
              </div>
            </div>
            <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('sex');?></label>
               <select name="sex" id="" class="form-control">
                <option value="1"><?php echo get_string('women'); ?></option>
                <option value="2"><?php echo get_string('man'); ?></option>
              </select>
            </div>
            <div class="col-md-6 form-group">
                 <label class="control-label"><?php print_string('role');?><span style="color:red">*</span></label>
                  <select name="role" id="" class="form-control">
                      <?php 
                        foreach ($vaitro as $value) {
                          ?>
                          <option value="<?php echo $value['id'] ?>"><?php echo $value['name']; ?></option>
                          <?php 
                        }
                       ?>
                  </select>
            </div>
            <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('email');?></label>
               <input class="form-control" type="text" name="email" placeholder="<?php print_string('email') ?>" >
             </div>
             <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('mobileph');?></label>
               <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('mobileph')) ?>"  pattern="^(01[2689]|02[0-9]|09|08|03)[0-9]{8}">
             </div>
             <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('username');?><span style="color:red">*</span></label>
               <input class="form-control" type="text" name="username" id="username" placeholder="<?php print_string('username') ?>" required >
               <div id="alert" >

               </div>
             </div>
             <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('password');?><span style="color:red">*</span></label>
               <input class="form-control" type="password" name="password" placeholder="<?php print_string('password') ?>" required>
             </div>
              
              <div class="col-md-6 ">
                <label class="control-label"><?php echo get_string('address'); ?></label>
                <input type="text" name="address" class="form-control" value="" placeholder="<?php print_string('address') ?>">
              </div>
              <!-- <div class="row"> -->
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label"><?php echo get_string('note'); ?></label>
                    <br>
                    <textarea name="note" id="note" cols="30" rows="10" class="form-control" value="<?php echo $note; ?>"><?php echo $note; ?>
                    </textarea>
                    <script>CKEDITOR.replace('note');</script>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-buttons" style="border-top: none !important">
                    <input type="submit" id="submitBtn" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
                    <?php print_r(get_string('or')) ?>
                    <a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                  </div>
                </div>
              <!-- </div> -->

            </form>
          </div>
        </div>
      </div>
    </div>

    <?php echo $OUTPUT->footer();  ?>
<script>
  $('#birthday').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
<script>
  $(document).ready(function() {
    $("#username").keydown(function() {
      var dInput = $('#username').val();
      if(dInput!=''){
        $.get("<?php echo $CFG->wwwroot ?>/manage/user/load_ajax.php?username="+dInput,function(data){
          $("#alert").html(data);

        });
      }
    });
    $("#username").keyup(function() {
      var dInput = $('#username').val();
      if(dInput!=''){
        $.get("<?php echo $CFG->wwwroot ?>/manage/user/load_ajax.php?username="+dInput,function(data){
          $("#alert").html(data);

        });
      }
    });
  });

</script>