<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/user/lib.php');

$PAGE->set_title(get_string('edituser'));
$PAGE->set_heading(get_string('edituser'));
$PAGE->set_pagelayout(get_string('edituser'));
require_login(0, false);
echo $OUTPUT->header();
global $DB;

$action = optional_param('action', '', PARAM_TEXT);
$id_edit = trim(optional_param('id_edit', '', PARAM_TEXT));
$lastname = trim(optional_param('lastname', '', PARAM_TEXT));
$firstname = trim(optional_param('firstname', '', PARAM_TEXT));
$birthday = trim(optional_param('birthday', '', PARAM_TEXT));
$sex = (optional_param('sex', '', PARAM_TEXT));
$email = trim(optional_param('email', '', PARAM_TEXT));
$phone1 = (optional_param('phone1', '', PARAM_TEXT));
$address = trim(optional_param('address', '', PARAM_TEXT));
$note = trim(optional_param('note', '', PARAM_TEXT));
$useredit=$DB->get_record('user', array(
  'id' => $id_edit
));
if ($action=='edituser') {
	$new_user_id=update_user_by_cua_3($id_edit,$firstname,$lastname,$email,$phone1,$address,$birthday,$sex,$description);
	if(!empty($new_user_id)){
    add_logs('user','update','/manage/user/edit.php?id_edit='.$new_user_id->id,$lastname.' '.$firstname); 
	   echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/user/index.php");
	    # code...
	}else{
	    echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/user/edit.php?id_edit=".$id_edit);
	}
 
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='user';
$name1='edituser';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$vaitro=hien_thi_vai_tro_nguoi_dung();
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">
        <form id="profile" action="" onsubmit="return validate();" method="post">
        	<input type="text" hidden="" value="edituser" name="action">
            <div class="row">
              <div class="col-md-6 form-group">
              <label class="control-label"><?php print_string('lastname');?><span style="color:red">*</span></label>
              <input class="form-control" type="text" name="lastname" required placeholder="<?php print_string('lastname');?> " value="<?php echo $useredit->lastname; ?>">
            </div>
            <div class="col-md-6 form-group">
              <label class="control-label"><?php print_string('firstname');?><span style="color:red">*</span></label>
              <input class="form-control" type="text" name="firstname" required placeholder="<?php print_string('firstname');?>" value="<?php echo $useredit->firstname; ?>">
            </div>
            <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('birthday');?></label>
               <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                <div class="input-group-addon">
                  <span class="glyphicon glyphicon-th"></span>
                </div>
                <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $useredit->birthday; ?>" >
              </div>
            </div>
            <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('sex');?></label>
               <select name="sex" id="" class="form-control">
                <option value="1" ><?php echo get_string('women'); ?></option>
                <option value="2" <?php  if(!empty($useredit->sex)&&($useredit->sex==2)) echo 'selected';?>><?php echo get_string('man'); ?></option>
              </select>
            </div>
            <!-- <div class="col-md-6 form-group">
                 <label class="control-label"><?php print_string('role');?><span style="color:red">*</span></label>
                  <select name="" id="" class="form-control">
                      <?php 
                        foreach ($vaitro as $value) {
                          ?>
                          <option value="<?php echo $value['id'] ?>"><?php echo $value['name']; ?></option>
                          <?php 
                        }
                       ?>
                  </select>
            </div> -->
            <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('email');?></label>
               <input class="form-control" type="text" name="email" placeholder="<?php print_string('email') ?>" value="<?php echo $useredit->email; ?>">
             </div>
             <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('mobileph');?></label>
               <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('mobileph')) ?>"  pattern="^(01[2689]|02[0-9]|09|08|03)[0-9]{8}" value="<?php echo $useredit->phone1; ?>">
             </div>
             <!-- <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('username');?><span style="color:red">*</span></label>
               <input class="form-control" type="text" name="username" id="username" placeholder="<?php print_string('username') ?>" required >
               <div id="alert" >

               </div>
             </div>
             <div class="col-md-6 form-group">
               <label class="control-label"><?php print_string('password');?><span style="color:red">*</span></label>
               <input class="form-control" type="password" name="password" placeholder="<?php print_string('password') ?>" required>
             </div> -->
              
              <div class="col-md-6 ">
                <label class="control-label"><?php echo get_string('address'); ?></label>
                <input type="text" name="address" class="form-control"  placeholder="<?php print_string('address') ?>" value="<?php echo $useredit->address; ?>">
              </div>
              <!-- <div class="row"> -->
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label"><?php echo get_string('note'); ?></label>
                    <br>
                    <textarea name="note" id="note" cols="30" rows="10" class="form-control" value="<?php echo $useredit->description; ?>"><?php echo $useredit->description; ?>
                    </textarea>
                    <script>CKEDITOR.replace('note');</script>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-buttons" style="border-top: none !important">
                    <input type="submit" id="submitBtn" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
                    <?php print_r(get_string('or')) ?>
                    <a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                  </div>
                </div>
              <!-- </div> -->

            </form>
          </div>
        </div>
      </div>
    </div>

    <?php echo $OUTPUT->footer();  ?>
<script>
  $('#birthday').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
    