<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function hien_thi_vai_tro_nguoi_dung(){
  $tacnhan=array(
    '1'=>array('id'=>13,'name'=>'HOD'),
    '2'=>array('id'=>14,'name'=>'DOM'),
    '3'=>array('id'=>15,'name'=>'Report PIC'),
    '4'=>array('id'=>16,'name'=>'Scheduling'),
    '5'=>array('id'=>17,'name'=>'Admin'),
    '6'=>array('id'=>18,'name'=>'MO'),
    '7'=>array('id'=>19,'name'=>'CS'),
    '8'=>array('id'=>20,'name'=>'Event'),
    '9'=>array('id'=>21,'name'=>'Administrator'),
  );
  return $tacnhan;
}
function add_user_by_cua_2($firstname,$lastname,$email,$phone1,$username,$password,$address,$birthday,$sex,$description) {
  global $DB, $USER;
  $record = new stdClass();
 
  $record->firstname = trim($firstname) ;
  $record->lastname = trim($lastname);
  $record->birthday =trim($birthday) ;
  $record->email = trim($email);
  $record->phone1 = trim($phone1);

  $record->username = trim($username);
  $password = hash_internal_user_password($password);
  $record->password = $password;

  $record->country = 'VN';
  $record->timezone = 'SE Asia Standard Time';
  $record->address = trim($address);
  $record->parent = 0;
  $record->mnethostid = 1;
  $record->confirmed = 1;
  $record->sex = $sex;
  $record->description = trim($description);
  // $record->time_end_int = strtotime($time_end);
  $record->del = 0;

  $new_user_id = $DB->insert_record('user', $record);
  return $new_user_id;
}
function update_user_by_cua_3($user_id,$firstname,$lastname,$email,$phone1,$address,$birthday,$sex,$description) {
  global $DB;
  $record = new stdClass();
  $record->id = $user_id;
  $record->firstname = trim($firstname);
  $record->lastname = trim($lastname);
  $record->birthday = trim($birthday);
  $record->email =trim($email) ;
  $record->phone1 = trim($phone1);
  $record->address =trim($address);
  $record->sex = $sex;
  $record->name_parent = trim($name_parent);
  $record->description = trim($description);
  return $DB->update_record('user', $record, false);
}
?>