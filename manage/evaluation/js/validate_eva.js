function validate() {
    var error = 0;

    if (document.getElementById('schoolid') && document.getElementById('schoolid').value == '') {
        $("#er_schools").show();
        error = 1;
    } else {
        $("#er_schools").hide();
    }
    if (document.getElementById('id_gv') && document.getElementById('id_gv').value == '') {
        $("#er_gv").show();
        error = 1;
    } else {
        $("#er_gv").hide();
    }


    if (error == 1) {
        return false;
    }
    return true;
}