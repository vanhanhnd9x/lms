<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_login(0, false);
$PAGE->set_title('Đánh giá giáo viên');
$PAGE->set_heading('Đánh giá giáo viên');
global $USER;
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='evaluation-ta';
$name1='edit';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$id=optional_param('id','',PARAM_TEXT);
if (empty($id)) {
  echo displayJsAlert('', $CFG->wwwroot . "/manage/evaluation/teachertutors.php");
}else{
  $record = get_evaluation($id);
  $school=$DB->get_record('schools', array(
    'del' => 0,
    'id' => $record->id_schools,
                          
  ));
  $user_evaluation_by=$DB->get_record('user', array(
    'id' => $record->evaluation_by
  ));
}
$action = optional_param('action','', PARAM_TEXT);
if($action == 'update'){
  $id_schools = optional_param('id_schools','', PARAM_TEXT);
  $id_gvnn = '';
  $id_gvtg = optional_param('id_gvnn','', PARAM_TEXT);
  $rhta = $record->rhta;

  $cr1 = optional_param('cr1','', PARAM_TEXT);
  $cr2 = optional_param('cr2','', PARAM_TEXT);
  $cr3 = optional_param('cr3','', PARAM_TEXT);
  $cr4 = optional_param('cr4','', PARAM_TEXT);
  $cr5 = optional_param('cr5','', PARAM_TEXT);
  $cr6 = optional_param('cr6','', PARAM_TEXT);
  $cr7 = optional_param('cr7','', PARAM_TEXT);
  $cr8 = optional_param('cr8','', PARAM_TEXT);
  $dobest = optional_param('dobest','', PARAM_TEXT);
  $dobetter = optional_param('dobetter','', PARAM_TEXT); 
  $total= $cr1+$cr2+$cr3+$cr4+$cr5;
  // update_evaluation($id, $id_gvnn, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter);
  // update_evaluation($id, $id_gvnn, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter);
  update_evaluation2($id, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter,$total);
  echo displayJsAlert('Cập nhật thành công', $CFG->wwwroot . "/manage/evaluation/teachertutors.php");
}
?>
<style type="text/css">
    .dg {
      margin: 5px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
}
.dg:hover, .dg:checked {
  background: red;
}
.dg:checked {
  background: #56be8e;
}
.error{
  color: red;
  display: block;
}

.diem{
  position: absolute;
  left: 40px;
  top: 5px;
}
.rela{
  position: relative;
}
</style>
<script type="text/javascript" src="js/validate_profile.js"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">


          <form action="<?php echo $CFG->wwwroot ?>/manage/evaluation/edit_eva_teachertutors.php?action=update" onsubmit="return validate();" id="userForm" method="post">
              <input hidden type="text" value="<?php echo $id ?>" name="id">
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your school?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="<?php echo $school->name ?>" disabled>
                     <!-- <select name="id_schools" id="" class="form-control">
                      <?php 
                      $schools = get_schools();
                      $school=$DB->get_record('schools', array(
                        'del' => 0,
                        'id' => $record->id_schools,
                          
                      ));
                      foreach ($schools as $value) {
                        if ($value->del==0) {
                          if ($value->id==$record->id_schools) {
                            echo "<option selected value='".$value->id."'>".$value->name."</option>";
                          }
                        }
                      }
                       ?>
                    </select> -->
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your TA?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="<?php echo get_a_teacher($record->id_gvtg)->lastname." ".get_a_teacher($record->id_gvtg)->firstname ?>" disabled >
                    <!-- <select name="id_gvnn" id="" class="form-control">
                      <option value="<?php echo $record->id_gvtg ?>" ><?php echo get_a_teacher($record->id_gvtg)->lastname." ".get_a_teacher($record->id_gvtg)->firstname ?>
                        
                      </option>
                    </select> -->
                  </div>
              </div>
              
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">RHT</label>
                  <div class="col-md-8">
                    <!-- <input disabled type="text" class="form-control" value="<?php $gv=get_a_teacher($record->rhta);echo $gv->lastname.' '.$gv->firstname; ?>"> -->
                    <input disabled type="text" class="form-control" value="<?php echo hien_thi_thong_tin_nguoi_quan_ly_cua_gv_danh_gia($record->rhta); ?>">
                    <input hidden type="text" class="form-control" name="rhta" value="<?php echo $record->rhta; ?>">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">Month</label>
                  <div class="col-md-8">
                    <input type="text" value="<?php echo $record->month;?>" class="form-control" disabled>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">School year</label>
                  <div class="col-md-8" id="school_year">
                    <input type="text" value="<?php echo show_show_yeah_in_unit($record->id_namhoc); ?>" class="form-control" disabled>
                  </div>
              </div>
            <br>
              
              <div>
                <div>
                  <label>1.Attitude <span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" value="1" name="cr1" <?php echo $record->cr1==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr1" <?php echo $record->cr1==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr1" <?php echo $record->cr1==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr1" <?php echo $record->cr1==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr1" <?php echo $record->cr1==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>2.Punctuality<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" value="1" name="cr2" <?php echo $record->cr2==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr2" <?php echo $record->cr2==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr2" <?php echo $record->cr2==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr2" <?php echo $record->cr2==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr2" <?php echo $record->cr2==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>3.Classroom management and lesson involvement<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" value="1" name="cr3" <?php echo $record->cr3==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr3" <?php echo $record->cr3==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr3" <?php echo $record->cr3==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr3" <?php echo $record->cr3==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr3" <?php echo $record->cr3==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>4.Rapport with students<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" value="1" name="cr4" <?php echo $record->cr4==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr4" <?php echo $record->cr4==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr4" <?php echo $record->cr4==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr4" <?php echo $record->cr4==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr4" <?php echo $record->cr4==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
                <div>
                  <label>5.Appearance/ Dress code<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" value="1" name="cr5" <?php echo $record->cr5==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr5" <?php echo $record->cr5==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr5" <?php echo $record->cr5==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr5" <?php echo $record->cr5==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr5" <?php echo $record->cr5==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
              </div>
              <div>
                <label>6.<?php echo get_string('dobest') ?>?</label>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea class="form-control" name="dobest"><?php echo $record->dobest ?></textarea>
                </div>
              </div>
              <div>
                <label>7.<?php echo get_string('dobetter') ?>?</label>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea class="form-control" name="dobetter"><?php echo $record->dobetter ?></textarea>
                </div>
              </div>
              <br><br><br>
              <div class="form-group row hidden-print">
                <div class="col-md-12">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                    <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teachertutors.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('back')) ?>
                  </a>
                </div>
              </div>
        </form>   
      </div>
  </div>
</div>

<?php
    echo $OUTPUT->footer();
    require_once($CFG->dirroot . '/manage/evaluation/validate.php');
?>
