<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_login(0, false);
$PAGE->set_title('Đánh giáo viên trợ giảng');
echo $OUTPUT->header();

$page = optional_param('page','0', PARAM_INT);
$perpage = optional_param('perpage','15', PARAM_INT);
$key = trim(optional_param('key','', PARAM_TEXT));
if (empty($key)) {
  $param = 1;
}else{
  $param = 'firstname LIKE "%'.$key.'%"';
  $param .= ' OR lastname LIKE "%'.$key.'%"';
  $param .= ' OR name LIKE "%'.$key.'%"';
  $param .= ' OR CONCAT(firstname, " ", lastname) LIKE "%'.$key.'%"';
  $param .= ' OR CONCAT(lastname, " ", firstname) LIKE "%'.$key.'%"';
}
$totalcount = 0;


function search_evaluation_ta_by_cua($page,$number,$id_gvtg=null,$evaluation_by=null,$schoolid=null,$month=null,$id_namhoc=null,$score_from=null, $score_to=null,$rth=null){
    global $DB;
    if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
    }
    $sql=" 
        SELECT `evaluation`.`id`, `user`.`firstname`, `user`.`lastname`, `evaluation`.`total`,`schools`.`name`,`evaluation`.`rhta`,`evaluation`.`rhta`,`evaluation`.`month`,`evaluation`.`id_namhoc`,`evaluation`.`evaluation_by`,`evaluation`.`time_cre`
        FROM `user`
        JOIN `evaluation` ON `user`.`id` =`evaluation`.`id_gvtg` 
        JOIN `schools` ON `schools`.`id` =`evaluation`.`id_schools`
        AND `evaluation`.`del`=0
        AND `user`.`del` =0
        AND `schools`.`del`=0
        AND `evaluation`.`status`=2
    ";
    $sql_count="SELECT  COUNT( DISTINCT `user`.`id`)FROM `user`
        JOIN `evaluation` ON `user`.`id` =`evaluation`.`id_gvtg` 
        JOIN `schools` ON `schools`.`id` =`evaluation`.`id_schools`
        AND `evaluation`.`del`=0
        AND `user`.`del` =0
        AND `schools`.`del`=0
        AND `evaluation`.`status`=2";
    if(!empty($id_gvtg)){
        $sql.=" AND `evaluation`.`id_gvtg` ={$id_gvtg}";
        $sql_count.=" AND `evaluation`.`id_gvtg` ={$id_gvtg}";

    }
    if(!empty($evaluation_by)){
        $sql.=" AND `evaluation`.`evaluation_by` ={$evaluation_by}";
        $sql_count.=" AND `evaluation`.`evaluation_by` ={$evaluation_by}";
    }
    if(!empty($schoolid)){
        $sql.=" AND `evaluation`.`id_schools` ={$schoolid}";
        $sql_count.=" AND `evaluation`.`id_schools` ={$schoolid}";
    }
    if(!empty($month)){
        $sql.=" AND `evaluation`.`month` ={$month}";
        $sql_count.=" AND `evaluation`.`month` ={$month}";
    }
     if(!empty($id_namhoc)){
        $sql.=" AND `evaluation`.`id_namhoc` ={$id_namhoc}";
        $sql_count.=" AND `evaluation`.`id_namhoc` ={$id_namhoc}";
    }
    if(!empty($rth)){
        $sql.=" AND `evaluation`.`rhta` ={$rth}";
        $sql_count.=" AND `evaluation`.`rhta` ={$rth}";
    }
    if(!empty($score_from)){
        $sql.=" AND `evaluation`.`total` >={$score_from}";
        $sql_count.=" AND `evaluation`.`total` >={$score_from}";
    }
    if(!empty($score_to)){
        $sql.=" AND `evaluation`.`total` <={$score_to}";
        $sql_count.=" AND `evaluation`.`total` <={$score_to}";
    }
    $sql .= " ORDER BY `evaluation`.`id` DESC LIMIT {$start} , {$number} ";
    $data=$DB->get_records_sql($sql);
    // $total_row=$DB->get_records_sql($sql_count);
    // foreach ($total_row as $key => $value) {
    //   $total_page=ceil((int)$key / (int)$number);
    //   break;
    // }
    $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
    $total_page=ceil((int)$total_row / (int)$number);
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
}
$action=optional_param('action','', PARAM_TEXT);
$id_gvtg=optional_param('id_gvtg','', PARAM_TEXT);
$schoolid=optional_param('schoolid','', PARAM_TEXT);
$month=optional_param('month','', PARAM_TEXT);
$id_namhoc=optional_param('id_namhoc','', PARAM_TEXT);
$score_from=optional_param('score_from','', PARAM_TEXT);
$score_to=optional_param('score_to','', PARAM_TEXT);
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;

if ($action=='del') {
    $id=optional_param('id','', PARAM_TEXT);
    del_evaluation($id);
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/evaluation/teachertutors.php");
}

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='evaluation-ta';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
$name1='ta_evaluation';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

if($roleid==8){
    $cua=search_evaluation_ta_by_cua($page,$number,$id_gvtg,$USER->id,$schoolid,$month,$id_namhoc,$score_from, $score_to,$rth);
    $gvnn = lay_ds_gvtg_thuoc_gvnn_quan_ly_by_cua($USER->id);
    // $gvnn = get_info_evl($USER->id, 8);
    // $sch = get_schools_course(null, $USER->id);
    // $sch = get_schools_course2($USER->id,null);var_dump($sch);
     $sch =lay_ds_truong_hoc_cua_giao_vien_dggv($USER->id,$idgvtg=null);
}else{
    $cua=search_evaluation_ta_by_cua($page,$number,$id_gvtg,null,$schoolid,$month,$id_namhoc,$score_from, $score_to,$rth);
    // $gvnn = get_stt_teacher(2);
    $gvnn = all_gvtg_schedule();
    $sch = get_schools();
}
if ($action=='search') {
  $url= $CFG->wwwroot.'/manage/evaluation/teachertutors.php?action=search&id_gvtg='.$id_gvtg.'&schoolid='.$schoolid.'&month='.$month.'&id_namhoc='.$id_namhoc.'&score_from='.$score_from.'&score_to='.$score_to.'&page=';
}else{
  $url= $CFG->wwwroot.'/manage/evaluation/teachertutors.php?page=';
}
?>



<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    
                    <div class="col-md-2">
                        <?php //if(is_teacher() || is_admin()){?>
                            <?php 
                                if(!empty($checkthemmoi)){
                                    ?>
                                     <button onclick="window.location.assign('<?php echo $CFG->wwwroot."/manage/evaluation/newgt.php?role_id=10"; ?>')" class="btn btn-success"><?php echo get_string('add') ?></button>
                                    <?php 
                                }
                             ?>
                       
                        <?php //} ?>
                    </div>
                    <div class="col-md-10">
                      <form action="" method="get" >
                        <input type="text" name="action" value="search" hidden="">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="gvnn"><?php echo get_string('GVTG') ?></label>
                                <select type="text" id="gvtg" class="saj form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true"  name="id_gvtg">
                                    <option value="">--None--</option>
                                    <?php 
                                      foreach ($gvnn as $val) {
                                        ?>
                                        <option value="<?php echo $val->id; ?>" <?php if($id_gvtg==$val->id) echo'selected'; ?>><?php echo $val->lastname,' ',$val->firstname; ?></option>
                                        <?php 
                                      }
                                       ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="sch"><?php echo get_string('schools') ?></label>
                                <select type="text" id="sch" class="saj form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true" name="schoolid">
                                    <option value="">--None--</option>
                                    <?php foreach ($sch as $key => $value): ?>
                                        <option value="<?php echo $value->id ?>" <?php if($schoolid==$value->id) echo 'selected'; ?>><?php echo $value->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="month"><?php echo get_string('month') ?></label>
                                <select type="text" id="month" class="form-control" name="month">
                                    <option value=""><?php echo get_string('choose_month'); ?></option>
                                    <option value="1" <?php if($month==1) echo 'selected'; ?>><?php echo get_string('january'); ?></option>
                                    <option value="2" <?php if($month==2) echo 'selected'; ?>><?php echo get_string('february'); ?></option>
                                    <option value="3" <?php if($month==3) echo 'selected'; ?>><?php echo get_string('march'); ?></option>
                                    <option value="4" <?php if($month==4) echo 'selected'; ?>><?php echo get_string('april'); ?></option>
                                    <option value="5" <?php if($month==5) echo 'selected'; ?>><?php echo get_string('may'); ?></option>
                                    <option value="6" <?php if($month==6) echo 'selected'; ?>><?php echo get_string('june'); ?></option>
                                    <option value="7" <?php if($month==7) echo 'selected'; ?>><?php echo get_string('july'); ?></option>
                                    <option value="8" <?php if($month==8) echo 'selected'; ?>><?php echo get_string('august'); ?></option>
                                    <option value="9" <?php if($month==9) echo 'selected'; ?>><?php echo get_string('september'); ?></option>
                                    <option value="10" <?php if($month==10) echo 'selected'; ?>><?php echo get_string('october'); ?></option>
                                    <option value="11" <?php if($month==11) echo 'selected'; ?>><?php echo get_string('november'); ?></option>
                                    <option value="12" <?php if($month==12) echo 'selected'; ?>><?php echo get_string('december'); ?></option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('school_year'); ?></label>
                                <select name="id_namhoc" id="" class="form-control" >
                                    <option value=""><?php echo get_string('school_year'); ?></option>
                                    <?php 
                                        $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
                                        $school_year = $DB->get_records_sql($sql);
                                        foreach ($school_year as $key => $value) {
                                         # code...
                                        ?>
                                        <option value="<?php echo $value->id ?>" <?php if($id_namhoc==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
                                        <?php 
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for=""><?php echo get_string('score') ?></label>
                                <input type="text" class="form-control" value="<?php echo $score_from; ?>" name="score_from" placeholder="<?php echo get_string('from') ?>...">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for=""><?php echo get_string('score') ?></label>
                                <input type="text" class="form-control" value="<?php echo $score_to; ?>" name="score_to" placeholder="<?php echo get_string('to') ?>...">
                            </div>
                            <div class="col-4 form-group">
                                <div style="    margin-top: 29px;"></div>
                                <button type="submit" class="btn btn-success">
                                    <?php echo get_string('search'); ?></button>
                            </div>
                        </div>
                      </form>
                    </div>
                    
                </div>
                <?php 
                    if(!empty($cua['data'])){
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">STT</th>
                                        <?php 
                                        if(!empty($hanhdong)||!empty($checkdelete)){
                                            echo'<th class="align-middle text-center">'.get_string('action').'</th>';
                                        }
                                        ?>
                                        <th><?php echo get_string('date_evaluation'); ?></th>
                                        <th><?php echo get_string('GVTG'); ?></th>
                                        <th><?php echo get_string('schools'); ?></th>
                                        <th><?php echo get_string('RHT'); ?></th>                           
                                        <th><?php echo get_string('score'); ?></th>
                                        <th><?php echo get_string('month'); ?></th>
                                        <th><?php echo get_string('school_year'); ?></th>
                                        <?php
                                            if($roleid!=8){
                                                echo'<th>'.get_string('evaluation_by').'</th>';
                                            } 
                                        ?>
                                        
                                    </tr>
                                </thead>
                                <tbody id="page">
                                   <?php 
                                        $i=0;
                                        foreach ($cua['data'] as $evaluation) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $i; ?></td>
                                                 <?php 
                                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                                        echo'<td class="text-center hidden-print">';
                                                         $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$evaluation->id);
                                                        if(!empty($checkdelete)){
                                                           ?>
                                                          <a onclick="return Confirm('Xóa đánh giá','Bạn có muốn xóa đánh giá<?php echo $evaluation->lastname,' ',$evaluation->firstname; ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=del',array('id'=>$evaluation->id)); ?>')" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                           <?php 
                                                        }
                                                        echo'</td>';
                                                    }
                                                 ?>
                                                 <td><?php if($evaluation->time_cre){echo date('d/m/Y',$evaluation->time_cre);} ?></td>
                                                <td><?php echo $evaluation->lastname,' ',$evaluation->firstname; ?></td>
                                                <td><?php echo $evaluation->name; ?></td>
                                                <!-- <td><?php echo hien_thi_ten_user_can_tim_by_cua($evaluation->rhta); ?></td> -->
                                                <td><?php echo hien_thi_thong_tin_nguoi_quan_ly_cua_gv_danh_gia($evaluation->rhta); ?></td>
                                                <td><?php echo $evaluation->total,'/',25; ?></td>
                                                <td><?php echo show_month_in_unit($evaluation->month); ?></td>
                                                <td><?php echo show_show_yeah_in_unit($evaluation->id_namhoc); ?></td>
                                                 <?php
                                                    if($roleid!=8){
                                                        ?>
                                                        <td><?php echo hien_thi_ten_user_can_tim_by_cua($evaluation->evaluation_by); ?></td>
                                                        <?php 
                                                    } 
                                                ?>
                                               
                                            </tr>
                                            <?php 
                                            
                                        }
                                   ?>
                                </tbody>
                            </table>
                          </div>
                        <?php 
                            if ($cua['total_page']) {
                              if ($page > 2) { 
                                $startPage = $page - 2; 
                              } else { 
                                $startPage = 1; 
                              } 
                              if ($cua['total_page'] > $page + 2) { 
                                $endPage = $page + 2; 
                              } else { 
                                $endPage =$cua['total_page']; 
                              }
                            }
                             ?>
                            <p>
                                <?php echo get_string('total_page'); ?>:
                                <?php echo $cua['total_page'] ?>
                            </p>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
                                        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <?php 
                                    for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                                    # code...
                                      ?>
                                    <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>">
                                            <?php echo $i; ?></a></li>
                                    <?php 
                                    }
                                    ?>
                                    <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
                                        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                    <?php 
                    }
                 ?>
                
            
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<script>
// $('.saj').change(function(event) {
//     $.get('ajaxtutors.php?gvnn='+<?php echo $USER->id ?>+'&sch='+$('#sch').val()+'&to='+$('#to').val()+'&from='+$('#from').val()+'&month='+$('#month').val()+'&gvtg='+$('#gvtg').val(), function(data){
//         $('#page').html(data);
//     });
// });
</script>
<?php
// }else
//     echo displayJsAlert('Không có quyền truy cập', $CFG->wwwroot);
    echo $OUTPUT->footer();
?>