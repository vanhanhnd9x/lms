<?php 
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
 ?>


<?php 
$gvtg = trim(optional_param('gvtg','', PARAM_TEXT));
$page        = optional_param('page', '0', PARAM_INT);
$perpage = optional_param('perpage','20', PARAM_INT);

$gvnn = trim(optional_param('gvnn','', PARAM_TEXT));
$sch = trim(optional_param('sch','', PARAM_TEXT));
$to = trim(optional_param('to','', PARAM_TEXT));
$from = trim(optional_param('from','', PARAM_TEXT));
$month = trim(optional_param('month','', PARAM_TEXT));

if(!empty($gvnn)){
	$param .= " AND id_gvnn=".$gvnn;
}
if(!empty($sch)){
	$param .= " AND id_schools=".$sch;
}
if(!empty($month)){
	$param .= " AND data_evalatuion LIKE '%" . date('y', time())."-". $month . "%' ";
}
if(!empty($from) && !empty($to) && $from < $to){
	$param .= " AND (cr1+cr2+cr3+cr4+cr5+cr6+cr7+cr8) BETWEEN ".$from." AND ".$to;
}

$totalcount = 0;

$teacher = get_evaluation_teacher(8, $gvtg, $page, $perpage, $totalcount, $param);

if (check_role_evl($USER->id)==3) {
  $teacher = get_evl_admin(1, $page, $perpage, $totalcount, $param);
}

// var_dump($teacher);
 $stt=$page*$perpage+1; 
 if(empty($teacher)){
 	echo '<span class="text-danger"><h5>'. get_string('searchnoresult').'</h5></span>';
 }
 foreach ($teacher as $val) {
    if ($val->del==0) {
?>
<tr>
  <td><?php echo $stt; $stt++; ?></td>
  <td><?php echo get_a_teacher($val->id_gvnn)->firstname." ".get_a_teacher($val->id_gvnn)->lastname ?></td>
  <td><?php echo get_schools($val->id_schools)->name; ?></td>
  <td><?php echo get_a_teacher($val->rhta)->firstname." ".get_a_teacher($val->rhta)->lastname ?></td>
  <td class="text-center"><?php echo ($val->cr1+$val->cr2+$val->cr3+$val->cr4+$val->cr5+$val->cr6+$val->cr7+$val->cr8) ?>/40</td>
  <td><?php echo date('m-Y', $val->date); ?></td>
  <td class="text-right hidden-print">
    <a href="<?php print new moodle_url('edit.php',array('id'=>$val->id)); ?>" class="<?php echo (int)date('d', time()) >=14 ? 'disabled' : '' ?> btn btn-info btn-sm tooltip-animation" title="<?php echo get_string('edit') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
    <a href="<?php print new moodle_url('detail.php',array('id'=>$val->id)); ?>" class="btn btn-warning btn-sm tooltip-animation" title='<?php echo get_string('detail') ?>'><i class="fa fa-eye" aria-hidden="true"></i></a>
    <a onclick="return confirm('<?php echo get_string('doyouwantdel') ?>')" href="<?php print new moodle_url('?action=del',array('id'=>$val->id)); ?>" class="btn btn-danger btn-sm tooltip-animation" title='<?php echo get_string('delete') ?>'><i class="fa fa-trash-o" aria-hidden="true"></i></a>
  </td>
</tr>
<?php }} ?>
<tr>
  <td colspan="7"><div><?php paginate($totalcount->count, $page, $perpage, "#"); ?></div></td>
</tr>
<script>
	$('a').on('click', function(event) {
		var x=$(this).attr('href');
    var page=x.replace('#page=', '');

    $.get('ajax.php?gvnn='+$('#gvnn').val()+'&sch='+$('#sch').val()+'&to='+$('#to').val()+'&from='+$('#from').val()+'&month='+$('#month').val()+'&gvtg=<?php echo $gvtg ?>'+"&page="+page, function(data){
        $('#page').html(data);
    });
	});
</script>
