<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.

 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
// danh gia giao vien
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php'); 
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('newevaluation'));
$PAGE->set_heading(get_string('newevaluation'));
global $USER;
global $DB;
$action = optional_param('action', '', PARAM_TEXT);
$id_namhoc = optional_param('id_namhoc', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
echo $OUTPUT->header();
date_default_timezone_set("Asia/Ho_Chi_Minh");
$role_id = 8;
$month =date('m');

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='evaluation-teacher';
$name1='newevaluation';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$thongtingvdg=$DB->get_record('user', array(
    'id' => $USER->id
  ),
  $fields='id,name_rht', $strictness=IGNORE_MISSING
); 
if($roleid==10){
  $school_list=hien_thi_ds_truong_duoc_them_gan_cho_gv(10,$USER->id);
  $dsql=ds_nguoiquanly_cua_gv_danhgiagv($USER->id);
}


$id_namhoc_now=get_id_school_year_now();
if($action == 'add'){
    $id_schools = optional_param('id_schools','', PARAM_TEXT);
    $id_gvnn = optional_param('id_gvnn','', PARAM_TEXT);
    $id_gvtg = '';
    // $id_namhoc=lay_nam_hoc_theo_giao_vien_va_tro_giang_by_cua($id_gvnn,$USER->id);
    // $courseid=lay_id_nhom_theo_giao_vien_va_tro_giang_by_cua($id_gvnn,$USER->id);
    // $rhta = $USER->name_rht;
    $quanly = optional_param('quanly','', PARAM_TEXT);
    $quanly[count($quanly)]=0;
    $rhta=implode(',', $quanly);
    $cr1 = optional_param('cr1','', PARAM_TEXT);
    $cr2 = optional_param('cr2','', PARAM_TEXT);
    $cr3 = optional_param('cr3','', PARAM_TEXT);
    $cr4 = optional_param('cr4','', PARAM_TEXT);
    $cr5 = optional_param('cr5','', PARAM_TEXT);
    $cr6 = optional_param('cr6','', PARAM_TEXT);
    $cr7 = optional_param('cr7','', PARAM_TEXT);
    $cr8 = optional_param('cr8','', PARAM_TEXT);
    $cr9 = optional_param('cr9','', PARAM_TEXT);
    $dobest = trim(optional_param('dobest','', PARAM_TEXT));
    $dobetter = trim(optional_param('dobetter','', PARAM_TEXT));
    $note = trim(optional_param('note','', PARAM_TEXT));
    $tt = 1;
    $evaluation_by=$USER->id; 
    $total=$cr1+$cr2+$cr3+$cr4+$cr5+$cr6+$cr7+$cr8+$cr9;
    foreach ($id_gvnn as $key => $id_gvnndanhgia) {
      # code...
      // $check=check_danh_gia_da_ton_tai_by_cua2($id_gvnn,$USER->id,$month,$tt,$id_namhoc);
      $check=check_danh_gia_da_ton_tai_by_cua3($id_gvnndanhgia,$USER->id,$month,$tt,$id_namhoc,$id_schools);
      if(!empty($check)){
        ?>
          <script>
          var urlweb= "<?php echo $CFG->wwwroot ?>/manage/evaluation/edit.php?id=<?php echo $check->id;?>";
          var urlweb2= "<?php echo $CFG->wwwroot ?>/manage/evaluation/new.php?role_id=8";
          var e=confirm("GVNN đã tồn tại đánh giá trong tháng và năm học này! Bạn có muốn tiến hành chỉnh sửa đánh giá?");
          if(e==true){
            window.location=urlweb;
          }else{
            window.location=urlweb2;
          }
        </script>
        <?php 
      }else{
         // $new_eval = new_evaluation2($id_gvnndanhgia, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter, $note, $role_id, $tt,$evaluation_by,$id_namhoc,$month,$total,$courseid); 
        $new_eval =new_evaluation3($id_gvnndanhgia, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $cr9, $dobest, $dobetter, $note, $role_id, $tt,$evaluation_by,$id_namhoc,$month,$total,$courseid);
        add_logs('evaluation', 'new', "/manage/evaluation/detail.php?id=".$new_eval->id, "đánh giá ");
        echo displayJsAlert(get_string('addsuccess'), $CFG->wwwroot . "/manage/evaluation/teacher.php");
      }
    }
    
}
// $gvnn = lay_ds_gvnn_quanly_gvtg_by_cua($USER->id);

?>
<style type="text/css">
    .dg {
      margin: 5px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
}
.dg:hover, .dg:checked {
  background: red;
}
.dg:checked {
  background: #56be8e;
}
.error{
  color: red;
  display: block;
}

.diem{
  position: absolute;
  left: 40px;
  top: 5px;
}
.rela{
  position: relative;
}
</style>
<?php //if (is_admin() || is_teacher_tutors()): ?>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form id="userForm" action="<?php echo $CFG->wwwroot ?>/manage/evaluation/new.php?action=add&role_id=<?php echo $role_id ?>" method="post">
             <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your school?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    <select name="id_schools" id="schools" class="form-control">
                      <option value="">--none--</option>
                      <?php 
                          if(!empty($school_list)){
                            foreach ($school_list as $key => $value) {
                              # code...
                              ?>
                              <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                              <?php 
                            }
                          }
                      ?>
                    </select>
                  </div>
              </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label">What is the name of your teachers?<span class="text-danger">*</span></label>
                <div class="col-md-8">
                  <select name="id_gvnn[]" id="id_gv" class="form-control multiple" multiple required="">
                   
                  </select>
                 <!--  <select name="id_gvnn" id="id_gv" class="form-control">
                    <option value="">--none--</option> -->
                    <?php 
                    // foreach ($gvnn as  $val) {
                    //   echo "<option value='".$val->id."' data-course='".$val->courseid."'>".$val->lastname." ".$val->firstname."</option>";
                    //   // $check=check_danh_gia_da_ton_tai_by_cua($val->id,$USER->id,$month,1);
                    //   // if(!empty($check)){

                    //   // }else{
                    //   //   echo "<option value='".$val->id."' data-course='".$val->courseid."'>".$val->lastname." ".$val->firstname."</option>";
                    //   // }
                      
                    // }
                     ?>
                  <!-- </select> -->
                </div>
              </div>

             
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">Who is your RHTA?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    <!-- <input type="text" value="<?php echo $thongtingvdg->name_rht; ?>" name="rhta" hidden>
                    <input type="text" value="<?php $gv=get_a_teacher($thongtingvdg->name_rht); echo $gv->lastname.' '.$gv->firstname;?>" class="form-control" required> -->
                    <select name="quanly[]" id="quanly" class="form-control multiple" multiple required>
                      <option value="" disabled>--Chọn người quản lý--</option>
                            <?php 
                              if(!empty($dsql)){
                                foreach ($dsql as $key => $value) {
                                  # code...
                                  ?>
                                  <option value="<?php echo $value->id; ?>"><?php echo $value->fullname; ?></option>
                                  <?php 
                                }
                              }
                          ?>
                  </select>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">Month</label>
                  <div class="col-md-8">
                    <input type="text" value="<?php echo date('m');?>" class="form-control" disabled>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">School year</label>
                  <div class="col-md-8" id="school_year">
                    <input type="text" value="<?php echo show_show_yeah_in_unit($id_namhoc_now); ?>" class="form-control" disabled>
                    <input type="text" value="<?php echo  $id_namhoc_now; ?>" class="form-control" hidden name="id_namhoc" >
                  </div>
              </div>
            <br>
            <br>
            <br>
            <?php if($role_id==8):?>
              <div>
                <div>
                  <label>1.Punctuality <?php //echo get_string('planning') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr1" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr1" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr1" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr1" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr1" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>2.Planning and preparation <?php // echo get_string('organization') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr2" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr2" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr2" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr2" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr2" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>3.Classroom management and organization <?php //echo get_string('interaction') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr3" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr3" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr3" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr3" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr3" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>4.Class activities and lesson content <?php //echo get_string('resources') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr4" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr4" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr4" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr4" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr4" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>5.Materials and resources <?php //echo get_string('punctality') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr5" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr5" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr5" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr5" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr5" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>6.Interaction with students <?php //echo get_string('activities') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr6" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr6" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr6" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr6" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr6" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>7.Interaction with TAs <?php //echo get_string('homework') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr7" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr7" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr7" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr7" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr7" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>8.Homework <?php //echo get_string('appearance') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr8" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr8" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr8" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr8" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr8" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
                <div>
                  <label>9.Professionalism (appearance, dress code, phone using) <?php //echo get_string('appearance') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr9" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr9" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr9" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr9" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr9" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
              </div>
            <?php endif; ?>
              <div>
                <label>10. <?php echo get_string('dobest') ?>?</label>
              </div>
              <div class=" form-group row">
                <div class="col-md-12">
                  <textarea class="form-control" name="dobest"></textarea>
                </div>
              </div>
              <div>
                <label>11. <?php echo get_string('dobetter') ?>?</label>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea class="form-control" name="dobetter"></textarea>
                </div>
              </div>
              <div class="form-group row hidden-print">
                <div class="col-md-11">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                  <?php print_r(get_string('or')) ?> 
                  <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teacher.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('cancel')) ?>
                  </a>
                </div>
              </div>
        </form>   
      </div>
  </div>
</div>  
<script>
  var trogiang="<?php echo $USER->id; ?>"
  // $('#id_gv').change(function(event) {
  //   var giaovien=$(this).val();
  //   $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?idgvnn=" +giaovien+"&idgvtg="+trogiang , function(data) {
  //           $("#schools").html(data);
  //       });
    
  // });
  // $('#schools').on('change', function() {
  //   var courseid = $('#schools option:selected').attr('status');
  //   // alert(courseid);
  //   $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?courseid=" +courseid, function(data) {
  //     $("#school_year").html(data);
  //   });
  //   // if (this.value) {
  //   //   $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
  //   //     $("#courseid").html(data);
  //   //   });
  //   // }
  // });
  // $('#id_gv').change(function(event) {
  //   $.get('ajaxSchools.php?gv='+$(this).val()+'&tg='+<?php echo $USER->id ?>, function(data){
  //       $('#idschools').html(data);
  //   });
  //    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?giaovien=" + $(this).val()+"&trogiang="+trogiang, function(data) {
  //           $("#school_year").html(data);
  //       });
  // });
  $('#schools').on('change', function() {
    var schools = $(this).val();
    // alert(courseid);
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?schools=" +schools+"&GVTG="+trogiang, function(data) {
      $("#id_gv").html(data);
    });
    
  });
</script>
<?php //else: ?>
  <!-- <div>
    <div class="alert alert-danger">
      <?php echo get_string('noteaccess') ?>
    </div>
    <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teacher.php' ?>" class="btn btn-danger"> 
      <?php print_r(get_string('cancel')) ?>
    </a>
  </div> -->
<?php// endif ?>

<?php
    if($roleid!=10){
      ?>
      <script>
        alert('Chỉ có GVTG mới có đủ dữ liệu tham gia chức năng này');
        $("#saveUser").prop("disabled",true);
      </script>
      <?php 
    }
    echo $OUTPUT->footer();
    require_once($CFG->dirroot . '/manage/evaluation/validate.php');
?>