<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('teacher_evaluation'));
$PAGE->set_heading(get_string('teacher_evaluation'));
global $USER;
echo $OUTPUT->header();
global $DB;
// if (is_teacher() || is_admin() || is_teacher_tutors()) {
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='evaluation-teacher';
$name1='statsmodedetailed';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$id=optional_param('id','',PARAM_TEXT);
if (empty($id)) {
  echo displayJsAlert('', $CFG->wwwroot . "/manage/evaluation/teacher.php");
}else{
  $record = get_evaluation($id);
  $gv = get_gv($record->id_gvnn);
}

add_logs('evaluation', 'view', "/manage/evaluation/detail.php?id=".$id, "đánh giá ".$gv->firstname.' '.$gv->lastname);
?>
<style type="text/css">
    .dg {
      margin: 5px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
}
.dg:hover, .dg:checked {
  background: red;
}
.dg:checked {
  background: #56be8e;
}
</style>
<script type="text/javascript" src="js/validate_profile.js"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">


          <form action="<?php echo $CFG->wwwroot ?>/manage/evaluation/evaluation.php?action=add" onsubmit="return validate();" id="userForm" method="post">

              
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your school?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                      <?php 
                      $schools =$DB->get_record('schools', array(
                        'id' => $record->id_schools
                      )); 
                      echo "<p>".$schools->name."</p>";
                      ?>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your teachers?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                      <?php 
                      // $gvnn = get_role(8);
                      // foreach ($gvnn as $val) {
                      //   if ($val->id==$record->id_gvnn) {
                      //     echo "<p>".$val->lastname." ".$val->firstname."</p>";
                      //   }
                      // }
                      echo "<p>".$gv->lastname." ".$gv->firstname."</p>";
                       ?>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">Who is your RHTA?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    <p><?php echo hien_thi_thong_tin_nguoi_quan_ly_cua_gv_danh_gia($record->rhta); ?></p>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">Month</label>
                  <div class="col-md-8">
                    <p><?php echo $record->month;?></p>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">School year</label>
                  <div class="col-md-8" id="">
                    <p><?php echo show_show_yeah_in_unit($record->id_namhoc); ?></p>
                  </div>
              </div>
            <br>
              
              <div>
                <div>
                  <label>1.Punctuality<?php //echo get_string('planning') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr1" <?php echo $record->cr1==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr1" <?php echo $record->cr1==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr1" <?php echo $record->cr1==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr1" <?php echo $record->cr1==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr1" <?php echo $record->cr1==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>2.Planning and preparation<?php //echo get_string('organization') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr2" <?php echo $record->cr2==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr2" <?php echo $record->cr2==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr2" <?php echo $record->cr2==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr2" <?php echo $record->cr2==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr2" <?php echo $record->cr2==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>3.Classroom management and organization<?php //echo get_string('interaction') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr3" <?php echo $record->cr3==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr3" <?php echo $record->cr3==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr3" <?php echo $record->cr3==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr3" <?php echo $record->cr3==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr3" <?php echo $record->cr3==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>4.Class activities and lesson content<?php //echo get_string('resources') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr4" <?php echo $record->cr4==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr4" <?php echo $record->cr4==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr4" <?php echo $record->cr4==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr4" <?php echo $record->cr4==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr4" <?php echo $record->cr4==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>5.Materials and resources<?php //echo get_string('punctality') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr5" <?php echo $record->cr5==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr5" <?php echo $record->cr5==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr5" <?php echo $record->cr5==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr5" <?php echo $record->cr5==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr5" <?php echo $record->cr5==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>6.Interaction with students<?php // echo get_string('activities') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr6" <?php echo $record->cr6==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr6" <?php echo $record->cr6==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr6" <?php echo $record->cr6==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr6" <?php echo $record->cr6==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr6" <?php echo $record->cr6==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>7.Interaction with TAs<?php //echo get_string('homework') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr7" <?php echo $record->cr7==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr7" <?php echo $record->cr7==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr7" <?php echo $record->cr7==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr7" <?php echo $record->cr7==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr7" <?php echo $record->cr7==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>8.Homework<?php //echo get_string('appearance') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr8" <?php echo $record->cr8==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr8" <?php echo $record->cr8==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr8" <?php echo $record->cr8==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr8" <?php echo $record->cr8==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr8" <?php echo $record->cr8==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
                <div>
                  <label>9.Professionalism (appearance, dress code, phone using)<?php //echo get_string('appearance') ?><span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="1" name="cr9" <?php echo $record->cr9==1? 'checked' : ''; ?>>1
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="2" name="cr9" <?php echo $record->cr9==2? 'checked' : ''; ?>>2
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="3" name="cr9" <?php echo $record->cr9==3? 'checked' : ''; ?>>3
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="4" name="cr9" <?php echo $record->cr9==4? 'checked' : ''; ?>>4
                  </div>
                  <br><br>
                  <div class="col">
                    <input disabled class="dg" type="radio" value="5" name="cr9" <?php echo $record->cr9==5? 'checked' : ''; ?>>5
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
              </div>
              <div>
                <label>10.<?php echo get_string('dobest') ?>?</label>
              </div>
              <div class="row">
                <div class="col-md-10">
                  <p><?php echo $record->dobest ?></p>
                </div>
              </div>
              <div>
                <label>13.<?php echo get_string('dobetter') ?>?</label>
              </div>
              <div class="row">
                <div class="col-md-10">
                  <p><?php echo $record->dobetter ?></p>
                </div>
              </div>
              
              <br><br><br>
              <div class="row">
                <div class="col-md-12">
                  <a href="javascript:window.print()" class="btn btn-primary waves-effect waves-light"><i class="fa fa-print m-r-5"></i> Print</a>
                    <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teacher.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('back')) ?>
                  </a>
                </div>
              </div>
        </form>   
      </div>
  </div>
</div>

<?php
// }else
  // echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot);
    echo $OUTPUT->footer();
?>
