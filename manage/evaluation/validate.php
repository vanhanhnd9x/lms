<script>
$('#userForm').validate({
	rules:{
		cr1:{
			required: true
		},
		cr2:{
			required: true
		},
		cr3:{
			required: true
		},
		cr4:{
			required: true
		},
		cr5:{
			required: true
		},
		cr6:{
			required: true
		},
		cr7:{
			required: true
		},
		cr8:{
			required: true
		},
		id_gvnn:{
			required: true
		},
		id_schools:{
			required: true
		}
	},
	messages:{
		cr1:{
			required : "<?php echo get_string('notempty') ?>"
		},
		cr2:{
			required: "<?php echo get_string('notempty') ?>"
		},
		cr3:{
			required: "<?php echo get_string('notempty') ?>"
		},
		cr4:{
			required: "<?php echo get_string('notempty') ?>"
		},
		cr5:{
			required: "<?php echo get_string('notempty') ?>"
		},
		cr6:{
			required: "<?php echo get_string('notempty') ?>"
		},
		cr7:{
			required: "<?php echo get_string('notempty') ?>"
		},
		cr8:{
			required: "<?php echo get_string('notempty') ?>"
		},
		id_gvnn:{
			required: "<?php echo get_string('notempty') ?>"
		},
		id_schools:{
			required: "<?php echo get_string('notempty') ?>"
		}
	}
});
</script>