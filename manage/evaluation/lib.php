<?php 
function new_evaluation($id_gvnn, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter, $note, $roleid, $tt){
    global $DB;
    $record = new stdClass();
    $record->id_gvnn = $id_gvnn;
    $record->id_schools = $id_schools;
    $record->id_gvtg = $id_gvtg;
    $record->rhta = $rhta;
    $record->cr1 = $cr1;
    $record->cr2 = $cr2;
    $record->cr3 = $cr3;
    $record->cr4 = $cr4;
    $record->cr5 = $cr5;
    $record->cr6 = $cr6;
    $record->cr7 = $cr7;
    $record->cr8 = $cr8;
    $record->dobest = $dobest;
    $record->dobetter = $dobetter;
    $record->del = 0;
    $record->note = $note;
    $record->role_id = $roleid;
    $record->date = time();
    $record->status = $tt;
    $new_criteria = $DB->insert_record('evaluation', $record);
    return $new_criteria;

}
function new_evaluation2($id_gvnn, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter, $note, $roleid, $tt,$evaluation_by,$id_namhoc,$month,$total,$courseid=null){
    global $DB;
    $record = new stdClass();
    $record->id_gvnn = $id_gvnn;
    $record->id_schools = $id_schools;
    $record->id_gvtg = $id_gvtg;
    $record->rhta = $rhta;
    $record->cr1 = $cr1;
    $record->cr2 = $cr2;
    $record->cr3 = $cr3;
    $record->cr4 = $cr4;
    $record->cr5 = $cr5;
    $record->cr6 = $cr6;
    $record->cr7 = $cr7;
    $record->cr8 = $cr8;
    $record->dobest = $dobest;
    $record->dobetter = $dobetter;
    $record->del = 0;
    $record->note = $note;
    $record->role_id = $roleid;
    $record->date = time();
    $record->status = $tt;
    $record->evaluation_by = $evaluation_by;
    $record->id_namhoc = $id_namhoc;
    $record->month = $month;
    $record->total = $total;
    $record->courseid = $courseid;
    $record->time_cre=time();
    $new_criteria = $DB->insert_record('evaluation', $record);
    return $new_criteria;

}
function new_evaluation3($id_gvnn, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $cr9, $dobest, $dobetter, $note, $roleid, $tt,$evaluation_by,$id_namhoc,$month,$total,$courseid=null){
    global $DB;
    $record = new stdClass();
    $record->id_gvnn = $id_gvnn;
    $record->id_schools = $id_schools;
    $record->id_gvtg = $id_gvtg;
    $record->rhta = $rhta;
    $record->cr1 = $cr1;
    $record->cr2 = $cr2;
    $record->cr3 = $cr3;
    $record->cr4 = $cr4;
    $record->cr5 = $cr5;
    $record->cr6 = $cr6;
    $record->cr7 = $cr7;
    $record->cr8 = $cr8;
    $record->cr9 = $cr9;
    $record->dobest = $dobest;
    $record->dobetter = $dobetter;
    $record->del = 0;
    $record->note = $note;
    $record->role_id = $roleid;
    $record->date = time();
    $record->status = $tt;
    $record->evaluation_by = $evaluation_by;
    $record->id_namhoc = $id_namhoc;
    $record->month = $month;
    $record->total = $total;
    $record->courseid = $courseid;
    $record->time_cre=time();

    $new_criteria = $DB->insert_record('evaluation', $record);
    return $new_criteria;

}
function update_evaluation($id, $id_gvnn, $id_schools, $id_gvtg, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter){
    global $DB;
    $record = new stdClass();
    $record->id = $id;
    $record->id_gvnn = $id_gvnn;
    $record->id_schools = $id_schools;
    $record->id_gvtg = $id_gvtg;
    $record->rhta = $rhta;
    $record->cr1 = $cr1;
    $record->cr2 = $cr2;
    $record->cr3 = $cr3;
    $record->cr4 = $cr4;
    $record->cr5 = $cr5;
    $record->cr6 = $cr6;
    $record->cr7 = $cr7;
    $record->cr8 = $cr8;
    $record->dobest = $dobest;
    $record->dobetter = $dobetter;
    $record->del = 0;
    $new_criteria = $DB->update_record('evaluation', $record, false);
    return $new_criteria;

}
function update_evaluation2($id, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter,$total){
    global $DB;
    $record = new stdClass();
    $record->id = $id;
    // $record->id_gvnn = $id_gvnn;
    // $record->id_schools = $id_schools;
    // $record->id_gvtg = $id_gvtg;
    $record->rhta = $rhta;
    $record->cr1 = $cr1;
    $record->cr2 = $cr2;
    $record->cr3 = $cr3;
    $record->cr4 = $cr4;
    $record->cr5 = $cr5;
    $record->cr6 = $cr6;
    $record->cr7 = $cr7;
    $record->cr8 = $cr8;
    $record->total = $total;
    $record->dobest = $dobest;
    $record->dobetter = $dobetter;
    $record->del = 0;
    $record->time_update = time();

    $new_criteria = $DB->update_record('evaluation', $record, false);
    return $new_criteria;

}
function update_evaluation3($id, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8,$cr9, $dobest, $dobetter,$total){
    global $DB;
    $record = new stdClass();
    $record->id = $id;
    // $record->id_gvnn = $id_gvnn;
    // $record->id_schools = $id_schools;
    // $record->id_gvtg = $id_gvtg;
    $record->rhta = $rhta;
    $record->cr1 = $cr1;
    $record->cr2 = $cr2;
    $record->cr3 = $cr3;
    $record->cr4 = $cr4;
    $record->cr5 = $cr5;
    $record->cr6 = $cr6;
    $record->cr7 = $cr7;
    $record->cr8 = $cr8;
    $record->cr9= $cr9;
    $record->total = $total;
    $record->dobest = $dobest;
    $record->dobetter = $dobetter;
    $record->del = 0;
    $record->time_update = time();
    $new_criteria = $DB->update_record('evaluation', $record, false);
    return $new_criteria;

}
function get_evaluation($id, $stt, $page, $perpage, &$totalcount, $param){
    global $DB;
    if (empty($id)) {
        $sql = "SELECT * FROM evaluation WHERE $param AND status = $stt ORDER BY id DESC LIMIT $page, $perpage";
        $rl = $DB->get_records_sql($sql);
        $totalcount = count($rl);
        return $rl;
    }else{
        return $DB->get_record('evaluation', array('id'=>$id, 'del'=>0));
    }
    
}

function get_role_evaluation($roleid, $page, $perpage, &$totalcount, $param)
{ 
    global $DB;
    $sql = "SELECT COUNT(*) AS 'count', `user`.firstname, `user`.lastname, `schools`.name
            FROM evaluation
            JOIN user ON `evaluation`.id_gvnn = `user`.id OR `evaluation`.rhta = `user`.id
            JOIN schools ON `evaluation`.id_schools = `schools`.id
            WHERE role_id=$roleid AND `evaluation`.del=0 AND ($param)";
    $totalcount = $DB->get_record_sql($sql);
    $page *= $perpage;

    $sql = "SELECT evaluation.*, `user`.firstname, `user`.lastname, `schools`.name
            FROM evaluation
            JOIN user ON `evaluation`.id_gvnn = `user`.id OR `evaluation`.rhta = `user`.id
            JOIN schools ON `evaluation`.id_schools = `schools`.id 
            WHERE role_id=$roleid AND `evaluation`.del=0 AND ($param) ORDER BY id DESC LIMIT $page, $perpage";
    return $DB->get_records_sql($sql);
}

function del_evaluation($id)
{
    global $DB;
    $record = new  stdClass();
    $record->id = $id;
    $record->del = 1;
    $DB->update_record('evaluation', $record, false);
}

function get_all_teacher($role=8){
    global $DB;
    $sql = "SELECT user.*, user.id AS 'id_gv', user.id AS 'id_tg' FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=$role";
    $row = $DB->get_records_sql($sql);
    return $row;
}
function get_stt_teacher($stt){
    global $DB;
    $sql = "SELECT DISTINCT id_gvnn AS 'id_gv', id_gvtg AS 'id_tg' FROM evaluation
      WHERE status = $stt";
    $row = $DB->get_records_sql($sql);
    return $row;
}

function get_role($role){
    global $DB;
    $sql="SELECT `user`.* FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=".$role;
    $row = $DB->get_records_sql($sql);
    return $row;
}

function get_a_teacher($id){
    global $DB;
    return $DB->get_record('user', array('id'=>$id));
}

function get_info_evl($userid, $roleid)
{
    global $DB;
    $sql = "SELECT
                course.id_gv, course.id AS 'courseid', groups.id_truong,course.fullname,
                course.id_tg,`user`.*
            FROM
                user 
                JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
                JOIN course ON course.id_gv=user.id OR user.id=course.id_tg
                JOIN group_course ON group_course.course_id = course.id
                JOIN groups ON groups.id=group_course.group_id
            WHERE
                `role_assignments`.`roleid` =$roleid AND user.id=$userid";
    return $DB->get_records_sql($sql);
}

function get_schools_course($id_tg=null, $teacher=null)
{
    global $DB;
    $sql = "SELECT schools.*
            FROM schools
            JOIN groups ON schools.id = groups.id_truong
            JOIN group_course on groups.id = group_course.group_id
            JOIN course ON group_course.course_id = course.id
            WHERE 1 
    ";
    if(!empty($id_tg)){
        $sql .= " AND course.id_tg = ".$id_tg;
    }
    if(!empty($teacher)){
        $sql .= " AND course.id_gv = ".$teacher;
    }
    return $DB->get_records_sql($sql);
}

function get_evaluation_teacher($role_id, $gvtg,$page, $perpage, &$totalcount, $param=null)
{
    global $DB;
    $sql = "SELECT COUNT(*) AS 'count' FROM evaluation
            WHERE role_id = $role_id AND del=0 $param";
    $totalcount = $DB->get_record_sql($sql);
    $sql = "SELECT * FROM evaluation
            WHERE role_id = $role_id AND del=0 $param 
            ORDER BY id DESC
            LIMIT $page, $perpage";
    return $DB->get_records_sql($sql);
}

function check_evaluation_gv($tg, $gv, $tt)
{
    global $DB;
    $sql = "SELECT count(*) as 'count' FROM evaluation WHERE del=0 AND id_gvtg = $tg AND id_gvnn = $gv AND status=$tt AND data_evalatuion LIKE '%".date('y-m', time())."%'";
    $result = $DB->get_record_sql($sql);
    if($result->count == 0){
        return 0;
    }
    return 1;
}

function check_role_evl($uid)
{
    global $DB;
    $sql = "
      SELECT roleid AS 'roleid' FROM role_assignments
      WHERE userid = $uid
    ";

    return $roleid = $DB->get_record_sql($sql)->roleid;
}

function get_evl_admin($stt, $page, $perpage, $totalcount, $param)
{
  global $DB;
    $sql = "
      SELECT * FROM evaluation
      WHERE status = $stt $param
      ORDER BY id DESC
      LIMIT $page, $perpage
    ";
    $rl = $DB->get_records_sql($sql);
    $totalcount = count($rl);
    return $rl;
}

function check_danh_gia_da_ton_tai_by_cua($idgiaovienduocdanhgia,$giaoviendanhgia,$month,$status){
    global $DB;
    if($status==1){
        $checkcua=$DB->get_record('evaluation', array(
          'status' => 1,
          'evaluation_by' =>$giaoviendanhgia,
          'month' => $month,
          'id_gvnn' => $idgiaovienduocdanhgia,
          'del' => 0,
        ));
    }
    if($status==2){
       $checkcua=$DB->get_record('evaluation', array(
          'status' => 2,
          'evaluation_by' => $giaoviendanhgia,
          'month' => $month,
          'id_gvtg' => $idgiaovienduocdanhgia,
          'del' => 0,
        ));
    }
    // $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $checkcua;
}
function check_danh_gia_da_ton_tai_by_cua2($idgiaovienduocdanhgia,$giaoviendanhgia,$month,$status,$id_namhoc){
    global $DB;
    if($status==1){
        $checkcua=$DB->get_record('evaluation', array(
          'status' => 1,
          'evaluation_by' =>$giaoviendanhgia,
          'month' => $month,
          'id_gvnn' => $idgiaovienduocdanhgia,
          'del' => 0,
          'id_namhoc' => $id_namhoc,
        ));
    }
    if($status==2){
       $checkcua=$DB->get_record('evaluation', array(
          'status' => 2,
          'evaluation_by' => $giaoviendanhgia,
          'month' => $month,
          'id_gvtg' => $idgiaovienduocdanhgia,
          'del' => 0,
          'id_namhoc' => $id_namhoc,
        ));
    }
    // $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $checkcua;
}
function check_danh_gia_da_ton_tai_by_cua3($idgiaovienduocdanhgia,$giaoviendanhgia,$month,$status,$id_namhoc,$schoolid){
    global $DB;
    if($status==1){
        $checkcua=$DB->get_record('evaluation', array(
          'status' => 1,
          'evaluation_by' =>$giaoviendanhgia,
          'month' => $month,
          'id_gvnn' => $idgiaovienduocdanhgia,
          'del' => 0,
          'id_namhoc' => $id_namhoc,
          'id_schools' => $schoolid,
        ));
    }
    if($status==2){
       $checkcua=$DB->get_record('evaluation', array(
          'status' => 2,
          'evaluation_by' => $giaoviendanhgia,
          'month' => $month,
          'id_gvtg' => $idgiaovienduocdanhgia,
          'del' => 0,
          'id_namhoc' => $id_namhoc,
          'id_schools' => $schoolid,
        ));
    }
    // $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $checkcua;
}
function hien_thi_ten_user_can_tim_by_cua($iduser){
    global $DB;
    $user=$DB->get_record('user', array(
      'id' => $iduser,
      'del' => 0,
    ));
    echo $user->lastname,' ',$user->firstname;
}

function lay_ds_gvnn_quanly_gvtg_by_cua($idgvtg){
    global $DB;
    $sql1="SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvtg} AND `role_assignments`.`roleid`=10)
    AND `role_assignments`.`roleid`= 8";
    $data=$DB->get_records_sql($sql1);
    return $data;
}
function lay_ds_gvtg_thuoc_gvnn_quan_ly_by_cua($idgvnn){
    global $DB;
    $sql1="SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvnn} AND `role_assignments`.`roleid`=8)
    AND `role_assignments`.`roleid`= 10";
    $data=$DB->get_records_sql($sql1);
    return $data;
   
}
function lay_ds_gvnn_quanly_gvtg_by_cua2($idgvtg,$schoolid){
    global $DB;
    $sql1="SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    JOIN `group_course` ON `group_course`.`course_id`= `course`.`id`
    JOIN `groups` ON `groups`.`id` =`group_course`.`group_id`
    WHERE `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvtg} AND `role_assignments`.`roleid`=10)
    AND `role_assignments`.`roleid`= 8
    AND `groups`.`id_truong`= {$schoolid}
    ";
    $data=$DB->get_records_sql($sql1);
    return $data;
}
function lay_ds_gvtg_thuoc_gvnn_quan_ly_by_cua2($idgvnn,$schoolid){
    global $DB;
    $sql1="SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    JOIN `group_course` ON `group_course`.`course_id`= `course`.`id`
    JOIN `groups` ON `groups`.`id` =`group_course`.`group_id`
    WHERE `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvnn} AND `role_assignments`.`roleid`=8)
    AND `role_assignments`.`roleid`= 10
    AND `groups`.`id_truong`={$schoolid}
    ";
    $data=$DB->get_records_sql($sql1);
    return $data;
   
}
function lay_nam_hoc_theo_giao_vien_va_tro_giang_by_cua($id_gvnn,$id_gvtg){
  global $DB;
  $sql=" SELECT `groups`.`id_namhoc`
  FROM `groups`
  JOIN `group_course` ON `group_course`.`group_id` = `groups`.`id`
  JOIN`course` on `course`.`id` =`group_course`.`course_id`
  WHERE `course`.`id_gv`={$id_gvnn} AND `course`.`id_tg`={$id_gvtg}
  ";
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function lay_id_nhom_theo_giao_vien_va_tro_giang_by_cua($id_gvnn,$id_gvtg){
    global $DB;
    $sql="SELECT `course`.`id` FROM `course` WHERE `course`.`id_gv`={$id_gvnn} AND `course`.`id_tg`={$id_gvtg}";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
     return $data;
     }
function lay_ds_id_nhom_cua_gvtg_bycua_dggv($idgvtg){
  global $DB;
  $sql="SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvtg} AND `role_assignments`.`roleid`=10";
  $data=$DB->get_records_sql($sql);
  return $data;
}
function lay_ds_id_nhom_cua_gvnn_bycua_dggv($idgvnn){
  global $DB;
  $sql="SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvnn} AND `role_assignments`.`roleid`=8";
  $data=$DB->get_records_sql($sql);
  return $data;
}

function lay_ds_truong_hoc_cua_giao_vien_dggv($idgvnn=null,$idgvtg=null){
    global $DB;
    $sql="SELECT `schools`.`id`, `schools`.`name` FROM `schools`
        JOIN `groups` ON `groups`.`id_truong`=`schools`.`id`
        JOIN `group_course` ON `group_course`.`group_id` =`groups`.`id`
        JOIN `course` ON `course`.`id` = `group_course`.`course_id`
        WHERE `schools`.`del`=0";
    if(!empty($idgvnn)){
        $sql.= " AND `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvnn} AND `role_assignments`.`roleid`=8)";
    }
    if(!empty($idgvtg)){
        $sql.= " AND `course`.`id` IN (SELECT DISTINCT  `course`.`id`FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `user`.`id`={$idgvtg} AND `role_assignments`.`roleid`=10)";
    }
    $data=$DB->get_records_sql($sql);
    return $data;
}
function get_list_id_school_gv_duoc_gan($userid){
    global $DB;
    $sql="SELECT `user`.`schoolid` FROM `user` WHERE `user`.`id`={$userid}";
    $data=$DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    $data=trim($data,"[");
    $data=trim($data,"]");
    return $data;
}
   
function hien_thi_ds_truong_duoc_them_gan_cho_gv($roleid,$userid){
    global $DB;
        $list_id_school=get_list_id_school_gv_duoc_gan($userid);
    $list_id_school_array=explode(',', $list_id_school);
       
    switch ($roleid) {
        case 8: $dshsduocgantheonhom=lay_ds_truong_hoc_cua_giao_vien_dggv($userid,$idgvtg=null); break;
        case 10: $dshsduocgantheonhom=lay_ds_truong_hoc_cua_giao_vien_dggv($idgvnn=null,$userid); break;
            
           
    }
    foreach ($dshsduocgantheonhom as $key => $value) {
            # code...
        if(!in_array($key, $list_id_school_array)){
            array_push($list_id_school_array, $key);
        }
    }
    $list_id_school_array=implode(',', $list_id_school_array);
        $sql2="
            SELECT `schools`.`id` ,`schools`.`name`,`schools`.`evaluation_student`
            FROM `schools` WHERE `schools`.`id` IN ({$list_id_school_array}) AND  `schools`.`del`=0
     ";
    $school_list=$DB->get_records_sql($sql2);
    return $school_list;
}
// trang add
// lay thông tin người quản lý của user
function hien_thi_thong_tin_nguoi_quan_ly_cua_gv_danh_gia($dk){
    global $DB;
    $hienthi=array();
    $sql2="
        SELECT `user`.`id` ,CONCAT(`user`.`lastname` ,' ' ,`user`.`firstname`) as fullname
        FROM `user` WHERE `user`.`id` IN ({$dk}) AND  `user`.`del`=0";
    $list_ql=$DB->get_records_sql($sql2);
    foreach ($list_ql as $key => $value) {
        array_push($hienthi,$value->fullname);
    }
    $qk=implode(',', $hienthi);
    return $qk;
}
function ds_nguoiquanly_cua_gv_danhgiagv($userid){
    global $DB;
    $gv=$DB->get_record('user', array(
        'id' =>$userid),
        $fields='id,firstname,lastname,name_rht', $strictness=IGNORE_MISSING
    );
    if(!empty($gv->name_rht)){
        $dk=str_replace( array('"','[',']'), '', $gv->name_rht );
        $sql2="
            SELECT `user`.`id` ,CONCAT(`user`.`lastname` ,' ' ,`user`.`firstname`) as fullname
            FROM `user` WHERE `user`.`id` IN ({$dk}) AND  `user`.`del`=0";
        $list_ql=$DB->get_records_sql($sql2);
        return $list_ql;
    }

   
}

 ?>
