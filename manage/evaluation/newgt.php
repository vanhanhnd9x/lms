<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
// danh gia giao vien tro giang
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('newevaluation'));
$PAGE->set_heading(get_string('newevaluation'));
global $USER;
global $DB;
$action = optional_param('action', '', PARAM_TEXT);
echo $OUTPUT->header();
date_default_timezone_set("Asia/Ho_Chi_Minh");
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='evaluation-ta';
$name1='newevaluation';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$thongtingvdg=$DB->get_record('user', array(
    'id' => $USER->id
  ),
  $fields='id,name_rht', $strictness=IGNORE_MISSING
); 
if($roleid==8){
  $school_list=hien_thi_ds_truong_duoc_them_gan_cho_gv(8,$USER->id);
  $dsql=ds_nguoiquanly_cua_gv_danhgiagv($USER->id);
  // var_dump($dsql);die;
}


$id_namhoc_now=get_id_school_year_now();
$month =date('m');
$role_id = 10;
$id_namhoc = optional_param('id_namhoc','', PARAM_TEXT);
$courseid = optional_param('courseid','', PARAM_TEXT);
if($action == 'add'){ 
    $id_schools = optional_param('id_schools','', PARAM_TEXT);
    $id_gvnn = '';
    $id_gvtg = optional_param('id_gvtg','', PARAM_TEXT); 
    $quanly = optional_param('quanly','', PARAM_TEXT);
    $quanly[count($quanly)]=0;
    $rhta=implode(',', $quanly);
    // var_dump($rht);die; 
    // $id_namhoc=lay_nam_hoc_theo_giao_vien_va_tro_giang_by_cua($USER->id,$id_gvtg);
// 
    // $courseid=lay_id_nhom_theo_giao_vien_va_tro_giang_by_cua($USER->id,$id_gvtg);
    $cr1 = optional_param('cr1','', PARAM_TEXT);
    $cr2 = optional_param('cr2','', PARAM_TEXT);
    $cr3 = optional_param('cr3','', PARAM_TEXT);
    $cr4 = optional_param('cr4','', PARAM_TEXT);
    $cr5 = optional_param('cr5','', PARAM_TEXT);
    $cr6 = optional_param('cr6','', PARAM_TEXT);
    $cr7 = optional_param('cr7','', PARAM_TEXT);
    $cr8 = optional_param('cr8','', PARAM_TEXT);
    $total=$cr1+$cr2+$cr3+$cr4+$cr5;
    // $rhta = $USER->name_rht;
    $dobest = trim(optional_param('dobest','', PARAM_TEXT));
    $dobetter = trim(optional_param('dobetter','', PARAM_TEXT));
    $note = trim(optional_param('note','', PARAM_TEXT));
    $tt =2;
    $evaluation_by=$USER->id; 
    foreach ($id_gvtg as $key => $gvtgdanhgia) {
      $check=check_danh_gia_da_ton_tai_by_cua3($gvtgdanhgia,$USER->id,$month,$tt,$id_namhoc,$id_schools);
      if(!empty($check)){
        ?>
         <script>
          var urlweb= "<?php echo $CFG->wwwroot ?>/manage/evaluation/edit_eva_teachertutors.php?id=<?php echo $check->id;?>";
          var urlweb2= "<?php echo $CFG->wwwroot ?>/manage/evaluation/newgt.php?role_id=10";
          var e=confirm("GVTG đã tồn tại đánh giá trong tháng và năm học này! Bạn có muốn tiến hành chỉnh sửa đánh giá?");
          if(e==true){
            window.location=urlweb;
          }else{
            window.location=urlweb2;
          }
        </script>
        <?php 

      }else{
        $new_eval = new_evaluation2($id_gvnn, $id_schools, $gvtgdanhgia, $rhta, $cr1, $cr2, $cr3, $cr4, $cr5, $cr6, $cr7, $cr8, $dobest, $dobetter, $note, $role_id, $tt,$evaluation_by,$id_namhoc,$month,$total, $courseid);
        // echo 1;die;
        add_logs('evaluation', 'new', "/manage/evaluation/detail.php?id=".$new_eval->id, "đánh giá ");
        echo displayJsAlert(get_string('addsuccess'), $CFG->wwwroot . "/manage/evaluation/teachertutors.php");
      }
      
    }
    
}

?>
<style type="text/css">
    .dg {
      margin: 5px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
}
.dg:hover, .dg:checked {
  background: red;
}
.dg:checked {
  background: #56be8e;
}
.error{
  color: red;
  display: block;
}

.diem{
  position: absolute;
  left: 40px;
  top: 5px;
}
.rela{
  position: relative;
}
</style>
<?php //if (is_admin() || is_teacher()): ?>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form id="userForm" action="" method="post">
            <input type="text" name="action" value="add" hidden="">
            <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your school?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    <select name="id_schools" id="schools" class="form-control schools" required>
                      <option value="">--none--</option>
                      <?php 
                          if(!empty($school_list)){
                            foreach ($school_list as $key => $value) {
                              # code...
                              ?>
                              <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                              <?php 
                            }
                          }
                      ?>
                      
                    </select>
                  </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label">What is the name of your TA?<span class="text-danger">*</span></label>
                <div class="col-md-8">
                  <select name="id_gvtg[]" id="id_gvtg" class="form-control multiple" multiple required>
                   
                  </select>
                  <!-- <select name="id_gvtg" id="id_gvtg" class="form-control" required="">
                    <option value="" disabled selected><?php echo get_string('GVTG'); ?></option>
                    <?php 
                    $gvtg = get_info_evl($USER->id, 8);
                    $gvtg =lay_ds_gvtg_thuoc_gvnn_quan_ly_by_cua($USER->id);
                    foreach ($gvtg as $val) {
                      echo "<option value='".$val->id."' data-course='".$val->courseid."' status='".$val->courseid."'>".$val->lastname." ".$val->firstname."</option>";
                    }
                     ?>
                  </select> -->
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label">Who is your RHT?</label>
                <div class="col-md-8">
                  <!-- <input type="text" value="<?php echo $thongtingvdg->name_rht; ?>" name="rhta" hidden>
                  <input type="text" value="<?php  $gv=get_a_teacher($thongtingvdg->name_rht);echo $gv->lastname.' '.$gv->firstname;?>" class="form-control" disabled> -->
                  <select name="quanly[]" id="quanly" class="form-control multiple" multiple required>
                      <option value="" disabled>--Chọn người quản lý--</option>
                            <?php 
                              if(!empty($dsql)){
                                foreach ($dsql as $key => $value) {
                                  # code...
                                  ?>
                                  <option value="<?php echo $value->id; ?>"><?php echo $value->fullname; ?></option>
                                  <?php 
                                }
                              }
                          ?>
                  </select>
                </div>
            </div>
             <div class="form-group row">
                  <label class="col-md-4 col-form-label">Month</label>
                  <div class="col-md-8">
                    <input type="text" value="<?php echo date('m');?>" class="form-control" disabled>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">School year</label>
                  <div class="col-md-8" id="school_year">
                    <input type="text" value="<?php echo show_show_yeah_in_unit($id_namhoc_now); ?>" class="form-control" disabled>
                    <input type="text" value="<?php echo  $id_namhoc_now; ?>" class="form-control" hidden name="id_namhoc" >
                  </div>
              </div>
            <br>
            <br>
            <br>
             <div class="alert-info">
                <label>1.Attitude<!-- <?php echo get_string('planning') ?> --> <span class="text-danger">*</span></label>
              </div>
              <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr1" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr1" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr1" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr1" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr1" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
              </div>

              <div class="alert-info">
                <label>2.Punctuality<?php // echo get_string('organization') ?><span class="text-danger">*</span></label>
              </div>
              <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr2" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr2" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr2" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr2" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr2" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
              </div>
              <div class="alert-info">
                <label>3.Classroom management and lesson involvement <?php //echo get_string('interaction') ?><span class="text-danger">*</span></label>
              </div>
              <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr3" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr3" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr3" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr3" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr3" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
              </div>
              <div class="alert-info">
                <label>4.Rapport with students <?php //echo get_string('resources') ?><span class="text-danger">*</span></label>
              </div>
              <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr4" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr4" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr4" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr4" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr4" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
              </div>
              <div class="alert-info">
                <label>5.Appearance/ Dress code <?php //echo get_string('resources') ?><span class="text-danger">*</span></label>
              </div>
              <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col rela">
                    <input class="dg" type="radio" value="1" name="cr5" ><span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="2" name="cr5" ><span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="3" name="cr5" ><span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="4" name="cr5" ><span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" value="5" name="cr5" ><span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
              </div>
              <div>
                <label>6. <?php echo get_string('dobest') ?>?</label>
              </div>
              <div class=" form-group row">
                <div class="col-md-12">
                  <textarea class="form-control" name="dobest"></textarea>
                </div>
              </div>
              <div>
                <label>7. <?php echo get_string('dobetter') ?>?</label>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea class="form-control" name="dobetter"></textarea>
                </div>
              </div>
              <div class="form-group row hidden-print">
                <div class="col-md-11">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                  <?php print_r(get_string('or')) ?> 
                  <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teachertutors.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('cancel')) ?>
                  </a>
                </div>
              </div>
              <td cl></td>
        </form>   
      </div>
  </div>
</div>  
<script>
  var giaovien="<?php echo $USER->id; ?>";
  // $('#id_gvtg').change(function(event) {
  //   var trogiang=$(this).val();
  //   $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?idgvnn=" +giaovien+"&idgvtg="+trogiang , function(data) {
  //           $("#schools").html(data);
  //       });
    
  // });
  // $('#schools').on('change', function() {
  //   var courseid = $('#schools option:selected').attr('status');
  //   // alert(courseid);
  //   $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?courseid=" +courseid, function(data) {
  //     $("#school_year").html(data);
  //   });
    
  // });
   $('#schools').on('change', function() {
    var schools = $(this).val();
    // alert(courseid);
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation/ajaxSchools.php?schools=" +schools+"&GVNN="+giaovien, function(data) {
      $("#id_gvtg").html(data);
    });
    
  });
</script>
<?php //else: ?>
 <!--  <div>
    <div class="alert alert-danger">
      <?php echo get_string('noteaccess') ?>
    </div>
    <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teacher.php' ?>" class="btn btn-danger"> 
      <?php print_r(get_string('cancel')) ?>
    </a>
  </div> -->
<?php //endif ?>

<?php
    if($roleid!=8){
      ?>
      <script>
        alert('Chỉ có GVNN đủ dữ liệu tham gia chức năng này');
        $('#saveUser').prop('disabled', true);
      </script>
      <?php 
    }
    echo $OUTPUT->footer();
    require_once($CFG->dirroot . '/manage/evaluation/validate.php');
?>