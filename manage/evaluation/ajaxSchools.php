<?php 
require('../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
$teacher = optional_param('gv','', PARAM_TEXT);
$tg = optional_param('tg','', PARAM_TEXT);
echo $teacher." ".$tg;
if(!empty($teacher) || !empty($tg)){
  $sch = get_schools_course($tg,$teacher);
  foreach ($sch as $key => $value) {
		  echo "<option value='".$value->id."'>".$value->name."</option>";
  }
}
function hienthi_truong_nhom_theo_nhomtruyenvao_dggv($course){
  global $DB;
  $sqlschool="SELECT `schools`.`id` FROM `course`
        JOIN `group_course` ON `group_course`.`course_id`= `course`.`id`
        JOIN `groups` ON `groups`.`id` =`group_course`.`group_id`
        JOIN `schools` ON `schools`.`id` =`groups`.`id_truong`
        WHERE `course`.`id` = {$course}";
  $id_schools=$DB->get_field_sql($sqlschool,null,$strictness=IGNORE_MISSING);
  $course=$DB->get_record('course', array(
    'id' => $course
  ));
  $school=$DB->get_record('schools', array(
    'id' => $id_schools
  ));
  $kq=array(
    'course'=>$course->fullname,
    'courseid'=>$course->id,
    'schools'=>$school->name,
    'idschools'=>$school->id,
  );
  return $kq;
}
if(!empty($_GET['idgvnn'])&&!empty($_GET['idgvtg'])){
	$dsnhomgv=lay_ds_id_nhom_cua_gvnn_bycua_dggv($_GET['idgvnn']);
	$dsnhomtg=lay_ds_id_nhom_cua_gvtg_bycua_dggv($_GET['idgvtg']);
	$result = array_intersect_key($dsnhomgv, $dsnhomtg);
	
	?>
	<!-- <div class="row form-group">
		<label class="col-md-4 col-form-label">What is the name of your school?<span class="text-danger">*</span></label>
	    <div class="col-md-8">
	     <select name="id_schools" id="schools" class="form-control" required> -->
	     	<option value=""><?php echo get_string('schools'); ?></option>
	     	<?php 
	     		foreach ($result as $value) {
				  $aa=hienthi_truong_nhom_theo_nhomtruyenvao_dggv($value->id);
				  ?>
				  <option value="<?php echo $aa['idschools']; ?>" status="<?php echo $aa['courseid']; ?>"><?php echo get_string('schools'),': ',$aa['schools'],' - ',get_string('classgroup'),': ',$aa['course']; ?></option>
				  <?php 
				}
	     	?>
	     	
	   <!--   </select>
	    </div>
	</div> -->
	
	<?php 
}
if(!empty($_GET['courseid'])){
	$sqlnamhoc="SELECT `groups`.`id_namhoc` FROM `course`
        JOIN `group_course` ON `group_course`.`course_id`= `course`.`id`
        JOIN `groups` ON `groups`.`id` =`group_course`.`group_id`
        WHERE `course`.`id` = {$_GET['courseid']}";
  	$id_namhoc=$DB->get_field_sql($sqlnamhoc,null,$strictness=IGNORE_MISSING);
  	?>
  	<input type="text" value="<?php echo show_show_yeah_in_unit($id_namhoc); ?>" class="form-control" disabled>
  	<input type="text" value="<?php echo $id_namhoc; ?>" class="form-control" hidden name="id_namhoc">
  	<input type="text" value="<?php echo $_GET['courseid']; ?>" class="form-control" hidden name="courseid">
  	<?php 
}
if(!empty($_GET['giaovien'])&&!empty($_GET['trogiang'])){
	$id_gvnn=$_GET['giaovien'];
	$id_gvtg=$_GET['trogiang'];
	$id_namhoc=lay_nam_hoc_theo_giao_vien_va_tro_giang_by_cua($id_gvnn,$id_gvtg);
	?>
	<input type="text" value="<?php echo show_show_yeah_in_unit($id_namhoc); ?>" class="form-control" disabled>
	<?php 
}

// danh sách gvtg2 thuoc truong gvn
if(!empty($_GET['schools'])){
	if(!empty($_GET['GVNN'])){
		$tg=lay_ds_gvtg_thuoc_gvnn_quan_ly_by_cua2($_GET['GVNN'],$_GET['schools']);
		foreach ($tg as $val) {
          	echo "<option value='".$val->id."' data-course='".$val->courseid."' status='".$val->courseid."'>".$val->lastname." ".$val->firstname."</option>";
        }
	}
	if(!empty($_GET['GVTG'])){
		$gvnn=lay_ds_gvnn_quanly_gvtg_by_cua2($_GET['GVTG'],$_GET['schools']);
		foreach ($gvnn as $val) {
          	echo "<option value='".$val->id."' data-course='".$val->courseid."' status='".$val->courseid."'>".$val->lastname." ".$val->firstname."</option>";
        }
	}
}	
 ?>
