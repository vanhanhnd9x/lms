<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title('Đánh giá giáo viên');
$PAGE->set_heading('Đánh giá giáo viên');
global $USER;
global $DB;
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='evaluation-ta';
$name1='statsmodedetailed';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$id=optional_param('id','',PARAM_TEXT);
if (empty($id)) {
  echo displayJsAlert('', $CFG->wwwroot . "/manage/evaluation/teachertutors.php");
}else{
  $record = get_evaluation($id);
  $gv = get_gv($record->id_gvtg);
}
?>
<style type="text/css">
    .dg {
      margin: 5px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
}
.dg:hover, .dg:checked {
  background: red;
}
.dg:checked {
  background: #56be8e;
}
.error{
  color: red;
  display: block;
}

.diem{
  position: absolute;
  left: 40px;
  top: 5px;
}
.rela{
  position: relative;
}
</style>
<script type="text/javascript" src="js/validate_profile.js"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">


          <form action="<?php echo $CFG->wwwroot ?>/manage/evaluation/edit_eva_teachertutors.php?action=update" onsubmit="return validate();" id="userForm" method="post">
              <input hidden type="text" value="<?php echo $id ?>" name="id">
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your school?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                       <?php 
                      $schools =$DB->get_record('schools', array(
                        'id' => $record->id_schools
                      )); 
                      echo "<p>".$schools->name."</p>";
                      ?>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">What is the name of your TA?<span class="text-danger">*</span></label>
                  <div class="col-md-8">
                    
                      <p><?php echo $gv->lastname." ".$gv->firstname; ?>
                      </p>
                    
                  </div>
              </div>
              
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">RHT</label>
                  <div class="col-md-8">
                    <!-- <?php $gv=get_a_teacher($record->rhta) ?> -->
                    <p> <?php echo hien_thi_thong_tin_nguoi_quan_ly_cua_gv_danh_gia($record->rhta); ?></p>
                  </div>
              </div>
               <div class="form-group row">
                  <label class="col-md-4 col-form-label">Month</label>
                  <div class="col-md-8">
                    <p><?php echo $record->month;?></p>
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-4 col-form-label">School year</label>
                  <div class="col-md-8" id="">
                    <p><?php echo show_show_yeah_in_unit($record->id_namhoc); ?></p>
                  </div>
              </div>
            <br>
              
              <div>
                <div>
                  <label>1.Attitude<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="1" name="cr1" <?php echo $record->cr1==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="2" name="cr1" <?php echo $record->cr1==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="3" name="cr1" <?php echo $record->cr1==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="4" name="cr1" <?php echo $record->cr1==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="5" name="cr1" <?php echo $record->cr1==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>2.Punctuality<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="1" name="cr2" <?php echo $record->cr2==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="2" name="cr2" <?php echo $record->cr2==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="3" name="cr2" <?php echo $record->cr2==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="4" name="cr2" <?php echo $record->cr2==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="5" name="cr2" <?php echo $record->cr2==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>3.Classroom management and lesson involvement<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="1" name="cr3" <?php echo $record->cr3==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="2" name="cr3" <?php echo $record->cr3==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="3" name="cr3" <?php echo $record->cr3==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="4" name="cr3" <?php echo $record->cr3==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="5" name="cr3" <?php echo $record->cr3==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>

                <div>
                  <label>4.Rapport with students<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="1" name="cr4" <?php echo $record->cr4==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="2" name="cr4" <?php echo $record->cr4==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="3" name="cr4" <?php echo $record->cr4==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="4" name="cr4" <?php echo $record->cr4==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="5" name="cr4" <?php echo $record->cr4==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
                <div>
                  <label>5.Appearance/ Dress code<<span class="text-danger">*</span></label>
                </div>
                <div class="row">
                  <div class="col">
                    Poor
                  </div>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="1" name="cr5" <?php echo $record->cr5==1? 'checked' : ''; ?>>
                    <span class="diem">1</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="2" name="cr5" <?php echo $record->cr5==2? 'checked' : ''; ?>>
                    <span class="diem">2</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="3" name="cr5" <?php echo $record->cr5==3? 'checked' : ''; ?>>
                    <span class="diem">3</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="4" name="cr5" <?php echo $record->cr5==4? 'checked' : ''; ?>>
                    <span class="diem">4</span>
                  </div>
                  <br><br>
                  <div class="col">
                    <input class="dg" type="radio" disabled value="5" name="cr5" <?php echo $record->cr5==5? 'checked' : ''; ?>>
                    <span class="diem">5</span>
                  </div>
                  <div class="col">
                    Excellent
                  </div>
                  <br><br>
                </div>
              </div>
              <div>
                <label>6.<?php echo get_string('dobest') ?>?</label>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <p><?php echo $record->dobest ?></p>
                </div>
              </div>
              <div>
                <label>7.<?php echo get_string('dobetter') ?>?</label>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <p><?php echo $record->dobetter ?></p>
                </div>
              </div>
              <br><br><br>
              <div class="form-group row hidden-print">
                <div class="col-md-12">
                    <a href="<?php echo $CFG->wwwroot . '/manage/evaluation/teachertutors.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('back')) ?>
                  </a>
                </div>
              </div>
        </form>   
      </div>
  </div>
</div>

<?php
    echo $OUTPUT->footer();
    require_once($CFG->dirroot . '/manage/evaluation/validate.php');
?>
