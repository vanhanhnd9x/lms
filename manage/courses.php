<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');
global $CFG,$DB,$USER;
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
// require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');

// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// if ($PAGE->user_allowed_editing()) {
//     $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
//     $PAGE->set_button($buttons);
// }
function get_status($month,$year,$courseid = null)
{
    global $DB;
    $sql ="SELECT DISTINCT status,id FROM diemdanh WHERE status IS NOT NULL AND month={$month} AND year={$year} AND course_id={$courseid}";
    return $DB->get_record_sql($sql);
}

// Check for valid admin user - no guest autologin
// require_login(0, false);
// $strmessages = '';
$PAGE->set_title(get_string('classgroup'));
$PAGE->set_heading(get_string('classgroup'));
echo $OUTPUT->header();
$page       = optional_param('page', '', PARAM_INT);
// $perpage    = optional_param('perpage', 20, PARAM_INT);
// $search     = optional_param('search', '', PARAM_TEXT);
// $param      =" AND (fullname LIKE '%".$search."%' OR groups.name LIKE '%".$search."%' OR schools.name LIKE '%".$search."%'     
//                 )";
require_login();
global $DB;

$action = optional_param('action', '', PARAM_TEXT);
// $id = optional_param('id', 0, PARAM_INT); //course_id
if ($action == 'delete') {
    $class_id = optional_param('id', '', PARAM_TEXT);
    delete_class($class_id);
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/courses.php");
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='course';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);

$name       = optional_param('name', '', PARAM_TEXT);
$gvnn       = optional_param('gvnn', '', PARAM_TEXT);
$gvtg       = optional_param('gvtg', '', PARAM_TEXT);
$class       = optional_param('class', '', PARAM_TEXT);
$schools    = optional_param('schools', '', PARAM_TEXT);

$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$url=$CFG->wwwroot.'/manage/courses.php?page=';
if($action=='search'){
$url=$CFG->wwwroot.'/manage/courses.php?action=search&name='.$name.'&class='.$class.'&schools='.$schools.'&gvnn='.$gvnn.'&gvtg='.$gvtg.'&page=';
}
if(in_array($roleid,array(3,13,14,15,16,17,18,19,20,21,11,12))){
    $data=search_course_cre_by_trang($name,$class,$schools,$gvnn,$gvtg,$userid,$page,$number);
    $totalcount = count(get_list_class(null,$perpage,$param));
    $truonghoc = $DB->get_records('schools', array('del'=>0));
}
if(is_teacher() || is_teacher_tutors()){
    $data=search_course_cre_by_trang($name,$class,$schools,$gvnn,$gvtg,$USER->id,$page,$number);
    $totalcount = count(get_list_class(null,$perpage,$param,$USER->id));
    $truonghoc=hien_thi_ds_truong_duoc_them_gan_cho_gv($roleid,$USER->id);
}                
if(is_student()){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");

}?>



<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mb-4">
                    <?php 
                        if(!empty($checkthemmoi)){
                            ?>
                            <div class="col-md-2">
                                <a href="<?php echo new moodle_url('/course/new_course/index.php'); ?>" class="btn btn-success"><?php print_r(get_string('new')) ?></a>
                            </div>
                            <?php 
                        }
                     ?>
                    
                    <div class="col-md-10">
                        <form action="" method="GET">
                            <div class="row">
                                <input type="text" hidden="" name="action" value="search">
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('classgroup') ?></label>
                                    <input type="text" id="name" name="name" class="saj form-control" placeholder="<?php echo get_string('classgroup') ?>..." value="<?php echo $name; ?>">   
                                </div>
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('class') ?></label>
                                    <input type="text" id="class" class="saj form-control" placeholder="<?php echo get_string('class') ?>..." name="class" value="<?php echo $class; ?>">   
                                </div>
                                <div class="col-md-4 form-group">
                                    <?php  ?>
                                    <label><?php echo get_string('schools') ?></label>
                                    <select type="text" id="schools" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true" name="schools">
                                        <option value="">--None--</option>
                                        <?php 
                                          foreach ($truonghoc as $vlu) {
                                            ?>
                                                <option value="<?php echo $vlu->id; ?>" <?php if(!empty($schools)&&($schools==$vlu->id)) echo 'selected'; ?>><?php echo $vlu->name; ?></option>
                                            <?php
                                          }
                                        ?>
                                        </select> 
                                </div>
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('foreignteacher') ?></label>
                                    <input type="text" id="gvnn" class="saj form-control" placeholder="<?php echo get_string('foreignteacher') ?>..." name="gvnn"  value="<?php echo $gvnn; ?>">   
                                </div>
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('tutorsteacher') ?></label>
                                    <input type="text" id="gvtg" class="saj form-control" placeholder="<?php echo get_string('tutorsteacher') ?>..." name="gvtg"  value="<?php echo $gvtg; ?>">   
                                </div>
                                 <div class="col-4 form-group">
                                    <div style="    margin-top: 29px;"></div>
                                    <button type="submit" class="btn btn-success">
                                        <?php echo get_string('search'); ?></button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                    
                </div>
                <?php if(!empty($data['data'])){ ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center align-middle">STT</th>
                                 <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="text-center align-middle">'.get_string('action').'</th>';} ?>
                                <th>
                                    <?php print_r(get_string('classgroup')) ?>
                                </th>
                                <th>
                                    <?php print_r(get_string('GVNN')) ?> 
                                </th>
                                <th>
                                    <?php print_r(get_string('email_new')) ?>
                                </th>
                                 <th>
                                    <?php print_r(get_string('phone')) ?>
                                </th>
                                <th>
                                    <?php print_r(get_string('GVTG')) ?>
                                </th>
                                <th>
                                    <?php print_r(get_string('email_new')) ?>
                                </th>
                                <th>
                                    <?php print_r(get_string('phone')) ?>
                                </th>
                                <th>
                                    <?php print_r(get_string('class')) ?> 
                                </th>
                                <th>
                                    <?php print_r(get_string('schools')) ?><br>
                                </th>
                                <th><?php print_r(get_string('syllabus_name')) ?></th>
                                <th><?php print_r(get_string('totalstudents')) ?></th>
                                <th>Điểm danh T<?=date('m')?></th>
                               
                            </tr>
                        </thead>
                        <!-- <tbody id="page"> -->
                        <tbody id="">
                            <?php 
                            if($page==1){
                                $i=0;
                            }else{
                                $i=($page-1)*$number;
                            } 
                            foreach ($data['data'] as $class) { 
                                $i++;
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $i;  ?></td>
                                <?php 
                                        if(!empty($hanhdong)||!empty($checkdelete)){
                                            echo'<td class="text-center">';
                                            $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$class->id);
                                            if(!empty($checkdelete)){
                                            ?>
                                            <a onclick="return Confirm('Xóa lớp học','Bạn có muốn xóa <?php echo $class->fullname ?> khỏi hệ thống?','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/settings/del_course.php?id=<?php echo $class->id ?>&action=delete')" class="mauicon" title="Xóa nhóm lớp">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                            <?php 
                                            }
                                            echo'</td>';
                                        }
                                    ?>
                                <td><?php echo $class->fullname ?></td>

                                <?php
                                    // $gvnn = get_info_giaovien(get_idgvnn_course($class->id));
                                    // $gvtg = get_info_giaovien(get_idgvtg_course($class->id));
                                ?>
                                <td><?php echo $class->giao_vien; ?></td>
                                <td><?php echo $class->gvmail; ?></td>
                                <td><?php echo $class->gvphone; ?></td>
                                <td><?php echo $class->tro_giang; ?></td>
                                <td><?php echo $class->tgmail; ?></td>
                                <td><?php echo $gvclasstg->tgphone; ?></td>
                                

                                <?php
                                    $members = count_members_enroll_of_course2($class->id);
                                ?>
                                <td class="text-center"><?php echo $class->name ?></td>
                                
                                <td><?php echo $class->tentruong ?></td>
                                <?php 
                                $book = get_book_schedule($class->id); ?>
                                <td>
                                    <?php 
                                    foreach ($book as $bk) {
                                        echo count($book)>=2 ? $bk->book . ',' : $bk->book;                                 
                                    } 
                                ?>
                                    
                                </td>
                                <td class="text-center"><?php echo $members; ?></td>
                                <td class="text-center">
                                    <?php $stt = get_status(date('m'),date('Y'),$class->id); ?>
                                    <?= $stt==1 ? 'Hoàn thành' : ''?>
                                </td>

                                <!-- <td class="text-right">
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/diligence/index.php?id=<?php echo $class->id ?>" class="btn btn-primary waves-effect waves-light btn-sm tooltip-animation" title="Điểm danh">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $CFG->wwwroot ?>/course/settings/index.php?id=<?php echo $class->id ?>" class="btn btn-info waves-effect waves-light btn-sm tooltip-animation" title="Chỉnh sửa">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $CFG->wwwroot ?>/course/people/index.php?id=<?php echo $class->id ?>" class="btn btn-warning waves-effect waves-light btn-sm tooltip-animation" title="Chi tiết nhóm lớp">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                 
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/examination/list_quiz_course.php?idcourse=<?php echo $class->id ?>" class="btn btn-info waves-effect waves-light btn-sm tooltip-animation" title="Danh sách bài test">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/add_unit_course.php?idcourse=<?php echo $class->id ?>" class="btn btn-dark waves-effect waves-light btn-sm tooltip-animation" title="Thêm điểm unit">
                                        <i class=" fa fa-paint-brush" aria-hidden="true"></i>
                                    </a>
                                    <a onclick="return Confirm('Xóa lớp học','Bạn có muốn xóa <?php echo $class->fullname ?> khỏi hệ thống?','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/settings/del_course.php?id=<?php echo $class->id ?>&action=delete')" class="btn waves-effect waves-light btn-danger btn-sm tooltip-animation" title="Xóa nhóm lớp">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                    </td> -->
                                    
                                
                            </tr>

                            <?php } ?>
                        </tbody>
                      
                    </table>
                    <?php 
                    if ($data['total_page']) {
                      if ($page > 2) { 
                        $startPage = $page - 2; 
                      } else { 
                        $startPage = 1; 
                      } 
                      if ($data['total_page'] > $page + 2) { 
                        $endPage = $page + 2; 
                      } else { 
                        $endPage =$data['total_page']; 
                      }
                    }
                     ?>
                    <p>
                        <?php echo get_string('total_page'); ?>:
                        <?php echo $data['total_page'] ?>
                    </p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                            for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                            # code...
                              ?>
                            <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>">
                                    <?php echo $i; ?></a></li>
                            <?php 
                            }
                            ?>
                            <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <?php }else{ ?>
                    <h4 class="text-danger">Không có bản ghi!</h4>
                <?php } ?>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<?php
    echo $OUTPUT->footer();
?>
<!-- <script type="text/javascript">
    $('.saj').keyup(function() {

        $.get("ajax_filter_course.php?name="+$('#name').val()+"&gvnn="+$('#gvnn').val()+"&gvtg="+$('#gvtg').val()+"&class="+$('#class').val()+"&schools="+$('#schools').val()+"&giaotrinh="+$('#giaotrinh').val(),function(data){
            $('#page').html(data);
        });
    });

    $('#schools').change(function() {
        $.get("ajax_filter_course.php?name="+$('#name').val()+"&gvnn="+$('#gvnn').val()+"&gvtg="+$('#gvtg').val()+"&class="+$('#class').val()+"&schools="+$('#schools').val()+"&giaotrinh="+$('#giaotrinh').val(),function(data){
            $('#page').html(data);
        });
    }); 
</script> -->

