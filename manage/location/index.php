<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/location/lib.php');
require_login(0, false);
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// Require Login.
// $PAGE->set_title(get_string('dashboards'));
$PAGE->set_title('Danh sách tỉnh thành');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Danh sách tỉnh thành');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Danh sách tỉnh thành');
$search = optional_param('search', '', PARAM_TEXT);
echo $OUTPUT->header();
if (function_exists('get_all_tinhthanh')) {
	$data=get_all_tinhthanh();
	# code...
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
                    <div class="col-md-2">
                        <a href="<?php echo $CFG->wwwroot ?>/manage/location/addlWards.php" class="btn btn-info">Thêm mới</a>
                    </div>
                    <div class="col-md-9 pull-right">
                        <!-- <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <div class="col-4">
                                <input type="text" name="searchClass" class="form-control" placeholder="Nhập nội dung..." id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-info">Tìm kiếm</button>
                            </div>
                        </form> -->
                    </div>
                </div>
				<div class="table-responsive" data-pattern="priority-columns">
					<form action="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php?action=deleteall" method="post">
						<div class="deleteall">
							<button class="btn ">Xóa</button>
						</div>
						<table id="tech-companies-1" class="table table-hover">
							<thead>
								<tr>
									<th data-priority="6">STT</th>
									<th data-priority="6">Tên tỉnh thành</th>
									<th class="text-right">Hành động</th>
								</tr>
							</thead>
							
							<tbody id="ketquatimkiem">
								<?php 
								if (!empty($data)) {
									$i=0;
											# code...
									foreach ($data as $key => $value) {
										$i++;
												# code...
										?>
										<tr>
											<td ><?php echo $i; ?></td>
											<td><?php echo $value->name; ?></td>
											<td class="text-right">
												<a href="<?php echo $CFG->wwwroot ?>/manage/location/edit_city.php?id=<?php echo $value->id ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php 
									}
								}
								?>

							</tbody>
						</table>
					</form>
				</div>
				<!-- end content-->
			</div>
			<!--  end card  -->
		</div>
		<!-- end col-md-12 -->
	</div>
</div>
	<?php 
	echo $OUTPUT->footer(); 
	?>
	<script>
		$('.deleteall').hide();
		$("#checkAll").click(function () {
			$(".check").prop('checked', $(this).prop('checked'));
		});
		$(document).ready(function() {
			$(":checkbox").click(function(event) {
				if ($(this).is(":checked"))
					$(".deleteall").show();
				else
					$(".deleteall").hide();
			});
		});
		
	</script>