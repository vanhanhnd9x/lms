<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function get_all_tinhthanh($page=null,$number=null){
	global $DB;
	// $data=$DB->get_records('tinhthanh');
	$sql="SELECT * FROM `tinhthanh` ORDER BY name ASC";
	$data=$DB->get_records_sql($sql);
	return $data;
}


function get_all_quanhuyen($page=null,$number=null,$name=null,$idtinhthanh=null){
	global $DB;
	if (!empty($page)&&!empty($number)) {
		if ($page>1) {
			$start=(($page-1)*$number);
		}else{
			$start=1;
		}
		$sql="SELECT * FROM `quanhuyen` ORDER BY name ASC LIMIT ".$start.",  ".$number."";
		$data1=$DB->get_records_sql($sql);
		$sql_total="SELECT COUNT(*) as total from quanhuyen";
		$total_row=$DB->get_records_sql($sql_total);
		
		foreach ($total_row as $key => $value) {
			$total_page=ceil((int)$key / (int)$number);
			break;
		}

		$kq=array(
			'data'=>$data1,
			'total_page'=>$total_page
		);
		return $kq;
	}
	
}

function save_quan_huyen($id=null,$name,$idtinhthanh){
	global $DB;
	if (!empty($id)) {
		$DB->set_field('quanhuyen', 'Name', $name, array('id' => $id));
		$DB->set_field('quanhuyen', 'idTinhThanh', $idtinhthanh, array('id' => $id));
		return $id;
	}else{
		$data = new stdClass();
		$data->id = $id;
		$data->Name = $name;
		$data->idTinhThanh = $idtinhthanh;
		$lastinsertid = $DB->insert_record('quanhuyen', $data);
		return $lastinsertid;
	}
}
function delete_quan_huyen($id){
	if (!empty($id)) {
		# code...
		global $DB;
		$DB->delete_records_select('quanhuyen', " id = $id ");
		return 1;
	}
}
function get_all_xaphuong($page=null,$number=null,$condition=null){
	// global $DB;
	// $data=$DB->get_records('quanhuyen',$condition);
	global $DB;
	if (!empty($page)&&!empty($number)) {
		if ($page>1) {
			$start=(($page-1)*$number);
		}else{
			$start=1;
		}
		$sql="SELECT * FROM `xaphuong` ORDER BY name ASC LIMIT ".$start.",  ".$number."";
		$data1=$DB->get_records_sql($sql);
		$sql_total="SELECT COUNT(*) as total from xaphuong";
		$total_row=$DB->get_records_sql($sql_total);

		foreach ($total_row as $key => $value) {
		# code...
			$total_page=ceil((int)$key / (int)$number);
			break;
		}
	// $total_page = ceil($total / $number);
		$kq=array(
			'data'=>$data1,
			'total_page'=>$total_page
		);
		return $kq;
	}
	
}
function save_xa_phuong($id=null,$name,$idquanhuyen){
	global $DB;
	if (!empty($id)) {
		# code...
		$DB->set_field('xaphuong', 'Name', $name, array('id' => $id));
		$DB->set_field('xaphuong', 'idQuanHuyen', $idquanhuyen, array('id' => $id));
		return $id;
	}else{
		$data = new stdClass();
		$data->id = $id;
		$data->Name = $name;
		$data->idQuanHuyen = $idquanhuyen;
		$lastinsertid = $DB->insert_record('xaphuong', $data);
		return $lastinsertid;
	}
}

function delete_xa_phuong($id){
	if (!empty($id)) {
		# code...
		global $DB;
		$DB->delete_records_select('xaphuong', " id = $id ");
		return 1;
	}
}
// sELECT * from {$CFG->prefix}commentwall where wallowner={$userid} order by posted desc limit {$offset},{$limit}
?>