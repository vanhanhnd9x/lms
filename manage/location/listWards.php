<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/location/lib.php');
require_login(0, false);
$PAGE->set_title('Danh sách xã phường');
$PAGE->set_heading('Danh sách xã phường');
$PAGE->set_pagelayout('Danh sách xã phường');
$search = optional_param('search', '', PARAM_TEXT);
echo $OUTPUT->header();

if (function_exists('get_all_xaphuong')) {
	global $DB;
	$page=isset($_GET['page'])?$_GET['page']:1;
	$number=30;
	$data=get_all_xaphuong($page,$number,$condition=null);
	# code...
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
                    <div class="col-md-2">
                        <a href="<?php echo $CFG->wwwroot ?>/manage/location/addlWards.php" class="btn btn-info">Thêm mới</a>
                    </div>
                    <div class="col-md-9 pull-right">
                        <!-- <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <div class="col-4">
                                <input type="text" name="searchClass" class="form-control" placeholder="Nhập nội dung..." id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-info">Tìm kiếm</button>
                            </div>
                        </form> -->
                    </div>
                </div>

				<div class="table-responsive" data-pattern="priority-columns">
					<form action="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php?action=deleteall" method="post">
						<div class="deleteall">
							<button class="btn ">Xóa</button>
						</div>
						<table id="tech-companies-1" class="table table-hover">
							<thead>
								<tr>
									<th data-priority="6">STT</th>
									<th data-priority="6">Tên xã phường</th>
									<th data-priority="6">Quận huyện</th>
									<th class="text-right">Hành động</th>
								</tr>
							</thead>

							<tbody id="ketquatimkiem">
								<?php 
								if (!empty($data['data'])) {
										// $i=0;
									$n=isset($_GET['page'])?$_GET['page']:1;
									$i=($n-1)*30;
											# code...
									foreach ($data['data'] as $key => $value) {
										$i++;
												# code...
										?>
										<tr>
											<td ><?php echo $i; ?></td>
											<td><?php echo $value->name; ?></td>
											<td><?php $quanhuyen=get_info_quanhuyen($value->idquanhuyen); echo $quanhuyen->name; ?></td>
											<td class="text-right">
												<a href="<?php echo $CFG->wwwroot ?>/manage/location/addlWards.php?id_edit=<?php echo $value->id ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<a href="<?php echo $CFG->wwwroot ?>/manage/location/listWards.php?id=<?php echo $value->id ?>&action=delete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</td>
										</tr>
										<?php 
									}
								}
								?>
								
							</tbody>
						</table>
					</form>
					<?php 
					if ($data['total_page']>2) {
							# code...

					}
					if ($data['total_page']) {
						if ($page > 2) { 
							$startPage = $page - 2; 
						} else { 
							$startPage = 1; 
						} 
						if ($data['total_page'] > $page + 2) { 
							$endPage = $page + 2; 
						} else { 
							$endPage =$data['total_page']; 
						}
					}
					?>
					<p>Tổng số trang: <?php echo $data['total_page'] ?></p>
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
								<a class="page-link" href="<?php echo $CFG->wwwroot ?>/manage/location/listWards.php?page=<?php echo $page-1;?>" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
									<span class="sr-only">Previous</span>
								</a>
							</li>
							<?php 
							for ($i=$startPage; $i <=$endPage ; $i++) { 
										# code...
								?>
								<li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $CFG->wwwroot ?>/manage/location/listWards.php?page=<?php echo $i;?>"><?php echo $i; ?></a></li>
								<?php 
							}
							?>
							<li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
								<a class="page-link" href="<?php echo $CFG->wwwroot ?>/manage/location/listWards.php?page=<?php echo $page+1;?>" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
									<span class="sr-only">Next</span>
								</a>

							</li>
						</ul>
					</nav>	
				</div>
				<!-- end content-->
			</div>
			<!--  end card  -->
		</div>
		<!-- end col-md-12 -->
	</div>
</div>

	<?php 
	echo $OUTPUT->footer(); 
	$action = optional_param('action', '', PARAM_TEXT);
	if ($action=='delete') {
		$id = optional_param('id', '', PARAM_TEXT);
		if (!empty($id)) {
			# code...
			$delete=delete_xa_phuong($id);
			if ($delete) {
				# code...
				echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/location/listWards.php");
			}else{
				echo displayJsAlert('Xóa thất bại', $CFG->wwwroot . "/manage/location/listWards.php");
			}
		}
		# code...
	}
	?>
	<script>
		$('.deleteall').hide();
		$("#checkAll").click(function () {
			$(".check").prop('checked', $(this).prop('checked'));
		});
		$(document).ready(function() {
			$(":checkbox").click(function(event) {
				if ($(this).is(":checked"))
					$(".deleteall").show();
				else
					$(".deleteall").hide();
			});
		});
		
	</script>
