<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/location/lib.php');
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// Require Login.
// $PAGE->set_title(get_string('dashboards'));
require_login(0, false);

$action = optional_param('action', '', PARAM_TEXT);
$id_edit = optional_param('id_edit', '', PARAM_TEXT);

if (!empty($id_edit)) {
	$PAGE->set_title('Sửa xã phường');
}else{
	$PAGE->set_title('Thêm mới xã phường');
}


if ($id_edit) {
	# code...
	$data_edit=get_info_xaphuong($id_edit);
	$quanhuyen=get_info_quanhuyen($data_edit->idquanhuyen);
}
if ($action=='add_wards') {
	# code...
	$name = optional_param('name', '', PARAM_TEXT);
	$quanhuyen = optional_param('idquanhuyen', '', PARAM_TEXT);
	$saveid=save_xa_phuong($id=null,$name,$quanhuyen);
	if(!empty($saveid)){
		echo displayJsAlert('Thêm thành công', $CFG->wwwroot . "/manage/location/listWards.php");
	}else{

	}
}
if ($action=='edit_wards') {
	# code...
	$name = optional_param('name', '', PARAM_TEXT);
	$idquanhuyen = optional_param('idquanhuyen', '', PARAM_TEXT);
	$id = optional_param('id', '', PARAM_TEXT);
	$update=save_xa_phuong($id,$name,$idquanhuyen);
	if(!empty($update)){
		echo displayJsAlert('Sửa thành công', $CFG->wwwroot . "/manage/location/listWards.php");
	}else{

	}
}
echo $OUTPUT->header();
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-10">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="<?php echo $CFG->wwwroot ?>/manage/location/addlWards.php" onsubmit="return validate();" method="post">
					<?php 
					if (!empty($id_edit)) {
							# code...
						echo'<input type="text" name="action" value="edit_wards" hidden="">';
						echo'<input type="text" name="id" value="'.$id_edit.'" hidden="">';
					}else{
						echo'<input type="text" name="action" value="add_wards" hidden="">';
					}
					?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Tỉnh thành</label>
								<select name="idtinhthanh" id="idtinhthanh" class="form-control" required="">
									<option value=" ">Chọn Tỉnh thành</option>
									<?php 
									if (function_exists('get_all_tinhthanh')) {
										$tinhthanh=get_all_tinhthanh();
										if ($tinhthanh) {
											foreach ($tinhthanh as $key => $value) {
												# code...
												if (!empty($quanhuyen->idtinhthanh)&&($quanhuyen->idtinhthanh==$value->id)) {$selected='selected';}else{$selected='';}
												echo'<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
											}
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Quận huyện</label>
								<select name="idquanhuyen" id="idquanhuyen" class="form-control" required="">
									<option value=" ">Chọn Quân huyện</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Tên xã phường</label>
								<div id="erCompany" class="alert alert-danger" style="display: none;">
								<?php print_string('pleasecompany') ?></div>
								<input type="text" id="name" name="name" class="form-control" value="<?php echo @$data_edit->name; ?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
echo $OUTPUT->footer();

?>
<script>
	var conceptName = $('#idtinhthanh').find(":selected").val();
	if (conceptName) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/location/load_ajax.php?idtinhthanh="+conceptName,function(data){
				$("#idquanhuyen").html(data);
			});
		}
	// alert(conceptName);
	$('#idtinhthanh').on('change', function() {
		var idtinhthanh = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/location/load_ajax.php?idtinhthanh="+idtinhthanh,function(data){
				$("#idquanhuyen").html(data);
			});
		}
	});
</script>