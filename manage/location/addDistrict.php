<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/location/lib.php');
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// Require Login.
// $PAGE->set_title(get_string('dashboards'));
require_login(0, false);

$action = optional_param('action', '', PARAM_TEXT);
$id_edit = optional_param('id_edit', '', PARAM_TEXT);

if (!empty($id_edit)) {
	$PAGE->set_title('Sửa quận huyện');
}else{
	$PAGE->set_title('Thêm mới quận huyện');
}


if ($id_edit) {
	$data_edit=get_info_quanhuyen($id_edit);
}
if ($action=='add_district') {
	# code...
	$name = optional_param('name', '', PARAM_TEXT);
	$idtinhthanh = optional_param('idtinhthanh', '', PARAM_TEXT);
	$id = '';
	$saveid=save_quan_huyen($id,$name,$idtinhthanh);
	if(!empty($saveid)){
		echo displayJsAlert('', $CFG->wwwroot . "/manage/location/listDistrict.php");
	}else{

	}
}
if ($action=='edit_district') {
	# code...
	$name = optional_param('name', '', PARAM_TEXT);
	$idtinhthanh = optional_param('idtinhthanh', '', PARAM_TEXT);
	$id = optional_param('id', '', PARAM_TEXT);
	$saveid=save_quan_huyen($id,$name,$idtinhthanh);
	if(!empty($saveid)){
		echo displayJsAlert('', $CFG->wwwroot . "/manage/location/listDistrict.php");
	}else{

	}
}
echo $OUTPUT->header();
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-10">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="<?php echo $CFG->wwwroot ?>/manage/location/addDistrict.php" onsubmit="return validate();" method="post">
					
					<?php 
					if (!empty($id_edit)) {
							# code...
						echo'<input type="text" name="action" value="edit_district" hidden="">';
						echo'<input type="text" name="id" value="'.$data_edit->id.'" hidden="">';
					}else{
						echo'<input type="text" name="action" value="add_district" hidden="">';
					}
					?>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Tỉnh thành</label>
								<select name="idtinhthanh" id="idtinhthanh" class="form-control" required="" >
									<?php 
									if (function_exists('get_all_tinhthanh')) {
										$tinhthanh=get_all_tinhthanh();
										if ($tinhthanh) {
											foreach ($tinhthanh as $key => $value) {
												# code...
												if (!empty($data_edit->idtinhthanh)&&($data_edit->idtinhthanh==$value->id)) {$selected='selected';}else{$selected='';}
												echo'<option value="'.$value->id.'" '.$selected.'>'.$value->name.'</option>';
											}
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
							<label class="control-label">Tên quận huyện</label>
							<div id="erCompany" class="alert alert-danger" style="display: none;">
								<?php print_string('pleasecompany') ?></div>
								<input type="text" id="name" name="name" class="form-control" value="<?php echo @$data_edit->name; ?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
