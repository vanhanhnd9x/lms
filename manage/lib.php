<?php

// $Id: lib.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * Library of functions and constants for module manage
 *
 * @author
 * @version $Id: lib.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */

function search_enrol_get_my_courses($keysearch) {
    global $DB, $USER;

    $params['userid'] = $USER->id;
    $params['active'] = ENROL_USER_ACTIVE;
    $params['enabled'] = ENROL_INSTANCE_ENABLED;
    $params['now1'] = round(time(), -2); // improves db caching
    $params['now2'] = $params['now1'];

//    $sql = "SELECT $coursefields $ccselect
//              FROM {course} c
//              JOIN (SELECT DISTINCT e.courseid
//                      FROM {enrol} e
//                      JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid in (".$id_list."))
//                     WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
//                   ) en ON (en.courseid = c.id)
//           $ccjoin
//             WHERE $wheres
//          $orderby";
    if (is_siteadmin()) {
    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $USER->id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $USER->id;
           $sql = "SELECT c.id,c.category,c.sortorder,c.shortname,c.fullname,c.idnumber,c.startdate,c.visible,c.groupmode,c.groupmodeforce , ctx.id AS ctxid, ctx.path AS ctxpath, ctx.depth AS ctxdepth, ctx.contextlevel AS ctxlevel, ctx.instanceid AS ctxinstance
              FROM {course} c
              JOIN (SELECT DISTINCT e.courseid
                      FROM {enrol} e
                      JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid in (".$id_list."))
                     WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                   ) en ON (en.courseid = c.id)
           LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = 50)";
}else{
    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $USER->id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $USER->id;
    $sql = "SELECT c.id,c.category,c.sortorder,c.shortname,c.fullname,c.idnumber,c.startdate,c.visible,c.groupmode,c.groupmodeforce , ctx.id AS ctxid, ctx.path AS ctxpath, ctx.depth AS ctxdepth, ctx.contextlevel AS ctxlevel, ctx.instanceid AS ctxinstance
              FROM {course} c
              JOIN (SELECT DISTINCT e.courseid
                      FROM {enrol} e
                      JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                     WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                   ) en ON (en.courseid = c.id)
           LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = 50)";
}
    if ($keysearch) {
        $where = " WHERE c.fullname LIKE '%$keysearch%' OR c.idnumber LIKE '%$keysearch%'";
        $sql .= $where;
    }

    $sql .= ' ORDER BY c.visible DESC,c.sortorder ASC';

    $courses = $DB->get_records_sql($sql, $params, 0, 20);

    return $courses;
}
//Tung Added new function for search course IPAD
function search_enrol_get_my_courses_libary($keysearch) {
    global $DB;
    
    $sql = "select * from course where visible=1 and id in(select course_id from course_settings where course_library=1)";

    if ($keysearch) {
        $where = " AND (fullname LIKE '%$keysearch%' OR idnumber LIKE '%$keysearch%')";
        $sql .= $where;
        
      }
    
    $sql .= ' ORDER BY visible DESC,sortorder ASC';

    $courses = $DB->get_records_sql($sql);

    return $courses;
}

//Chien Added new function for search class
function search_get_teacher($keysearch) {
    global $DB;
    
    $sql = "SELECT user.*  FROM user INNER JOIN role_assignments ON user.id = role_assignments.userid WHERE role_assignments.roleid=8";

    if ($keysearch) {
        $where = " AND (firstname LIKE '%$keysearch%' OR lastname LIKE '%$keysearch%' OR email LIKE '%$keysearch%')";
        $sql .= $where;
        
      }
    
    $sql .= ' ORDER BY id DESC';

    $class = $DB->get_records_sql($sql);

    return $class;
}

//Chien Added new function for search class
function search_get_class($keysearch) {
    global $DB;
    
    $sql = "select user.* from user join ";

    if ($keysearch) {
        $where = " AND (tenlop LIKE '%$keysearch%' OR malop LIKE '%$keysearch%')";
        $sql .= $where;
        
      }
    
    $sql .= ' ORDER BY id DESC, tenlop ASC';

    $class = $DB->get_records_sql($sql);

    return $class;
}
/**
 * @todo Finish documenting this function
 *
 * @param string $sort An SQL field to sort by
 * @param string $dir The sort direction ASC|DESC
 * @param int $page The page or records to return
 * @param int $recordsperpage The number of records to return per page
 * @param string $search A simple string to search for
 * @param string $firstinitial Users whose first name starts with $firstinitial
 * @param string $lastinitial Users whose last name starts with $lastinitial
 * @param string $extraselect An additional SQL select statement to append to the query
 * @param array $extraparams Additional parameters to use for the above $extraselect
 * @return array Array of {@link $USER} records
 */
function search_get_users_listing($sort = 'lastaccess', $dir = 'ASC', $page = 0, $recordsperpage = 0, $search = '', $firstinitial = '', $lastinitial = '', $extraselect = '', array $extraparams = null) {
    global $DB,$USER;

    $fullname = $DB->sql_fullname();
    
    $select = "deleted <> 1 AND id <> 1 ";
    
    $params = array();

    if (!empty($search)) {
        $search = trim($search);
        $select .= " AND (" . $DB->sql_like($fullname, ':search1', false, false) .
                " OR " . $DB->sql_like('email', ':search2', false, false) .
                " OR username = :search3)";
        $params['search1'] = "%$search%";
        $params['search2'] = "%$search%";
        $params['search3'] = "$search";
    }

    if ($firstinitial) {
        $select .= " AND " . $DB->sql_like('firstname', ':fni', false, false);
        $params['fni'] = "$firstinitial%";
    }
    if ($lastinitial) {
        $select .= " AND " . $DB->sql_like('lastname', ':lni', false, false);
        $params['lni'] = "$lastinitial%";
    }

    if ($extraselect) {
        $select .= " AND $extraselect";
        $params = $params + (array) $extraparams;
    }

    if ($sort) {
        $sort = " ORDER BY $sort $dir";
    }

/// warning: will return UNCONFIRMED USERS
    return $DB->get_records_sql("SELECT id, username, email, firstname, lastname, city, country, lastaccess, lastlogin, confirmed, mnethostid
                                   FROM {user}
                                  WHERE $select
                                  $sort",  $page, $recordsperpage);
}

function manage_print_log_of_selected_person($order = "l.time ASC", $page = 0, $perpage = 10, $url = "", $modname = "", $modid = 0, $modaction = "", $groupid = 0, $userid) {

    global $CFG, $DB, $OUTPUT, $USER;

    $uid = optional_param('uid', 0, PARAM_INT);

    $totalcount = 0;
    $select = 'userid = ' . $userid . ' AND course != 1'; // AND FROM_UNIXTIME(l.time) >= DATE_ADD(CURDATE(), INTERVAL -'.$noOfLastDays.' DAY)
    $logs = manage_get_logs($select, $params = null, $order = 'l.time DESC', $page, $perpage, $totalcount);

    $count = 0;
    $ldcache = array();
    $tt = getdate(time());
    $today = mktime(0, 0, 0, $tt["mon"], $tt["mday"], $tt["year"]);

    $strftimedatetime = get_string("strftimedatetime");

    echo "<div class=\"info\">\n";
    print_string("displayingrecords", "", $totalcount);
    echo "</div>\n";

    //echo $OUTPUT->paging_bar($totalcount, $page, $perpage, "$url&perpage=$perpage");

    $table = new html_table();
    $table->classes = array('logtable', 'generalbox');

    $table->data = array();

    // Make sure that the logs array is an array, even it is empty, to avoid warnings from the foreach.
    if (empty($logs)) {
        $logs = array();
    }

    foreach ($logs as $log) {

        $row = array();
        $row[] = '<div class="tip ' . $log->module . '">' . manage_format_log($log, $log->module, $log->userid, $log->course, $log->action) . '</div>';
        $row[] = '<div class="tip italic align-right">' . time_ago($log->time, '') . '</div>';
        $table->data[] = $row;
    }
    echo html_writer::table($table);
    $url = "$url&perpage=$perpage";
    $url .= is_numeric($uid) && $uid > 0 ? "&uid=$uid" : "";

    echo $OUTPUT->paging_bar($totalcount, $page, $perpage, $url);
}

function manage_print_log($order = "l.time ASC", $page = 0, $perpage = 10, $url = "", $modname = "", $modid = 0, $modaction = "", $groupid = 0) {

    global $CFG, $DB, $OUTPUT, $USER;

    $uid = optional_param('uid', 0, PARAM_INT);
    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);

    $id_list = display_parent($rows, $USER->id);
    $id_me = $id_list;
    if ($id_me == 0) {
        $id_me = $USER->id;
    }
    if(is_siteadmin()){
        
    }
 else {
        
    
    if ($id_list == 0) {
        $id_list = $USER->id;
    }
 }
    $id_list = display_children($rows, $id_list);
    $id_list .= $USER->id . "," . $id_me;

    //end fix
    $userid = is_numeric($uid) && $uid > 0 ? $uid : $id_list;
    $totalcount = 0;
    
        $select = 'userid in (' . $userid . ') AND course != 1'; // AND FROM_UNIXTIME(l.time) >= DATE_ADD(CURDATE(), INTERVAL -'.$noOfLastDays.' DAY)
    
//echo $select;
    $logs = manage_get_logs($select, $params = null, $order = 'l.time DESC', $page, $perpage, $totalcount);

    $count = 0;
    $ldcache = array();
    $tt = getdate(time());
    $today = mktime(0, 0, 0, $tt["mon"], $tt["mday"], $tt["year"]);

    $strftimedatetime = get_string("strftimedatetime");

    echo "<div class=\"info\">\n";
    print_string("displayingrecords", "", $totalcount);
    echo "</div>\n";

    //echo $OUTPUT->paging_bar($totalcount, $page, $perpage, "$url&perpage=$perpage");

    $table = new html_table();
    $table->classes = array('logtable', 'generalbox');

    $table->data = array();

    // Make sure that the logs array is an array, even it is empty, to avoid warnings from the foreach.
    if (empty($logs)) {
        $logs = array();
    }

    foreach ($logs as $log) {
        $row = array();
        $row[] = '<div class="tip ' . $log->module . '">' . manage_format_log($log, $log->module, $log->userid, $log->course, $log->action) . '</div>';
        $row[] = '<div class="tip italic align-right">' . time_ago($log->time, '') . '</div>';
        $table->data[] = $row;
    }
    echo html_writer::table($table);
    $url = "$url&perpage=$perpage";
    $url .= is_numeric($uid) && $uid > 0 ? "&uid=$uid" : "";

    echo $OUTPUT->paging_bar($totalcount, $page, $perpage, $url);
}

function manage_print_log_second($order = "l.time ASC", $page = 0, $perpage = 10, $url = "", $modname = "", $modid = 0, $modaction = "", $groupid = 0) {

    global $CFG, $DB, $OUTPUT, $USER;

    $uid = optional_param('uid', 0, PARAM_INT);
    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

    $id_list = display_children($rows, $USER->id);
    $id_list .= $USER->id;

    //end fix
    $userid = is_numeric($uid) && $uid > 0 ? $uid : $id_list;
    $totalcount = 0;
    $select = 'userid in (' . $userid . ') AND course != 1'; // AND FROM_UNIXTIME(l.time) >= DATE_ADD(CURDATE(), INTERVAL -'.$noOfLastDays.' DAY)
    //echo $select;
    $logs = manage_get_logs($select, $params = null, $order = 'l.time DESC', $page, $perpage, $totalcount);

    $count = 0;
    $ldcache = array();
    $tt = getdate(time());
    $today = mktime(0, 0, 0, $tt["mon"], $tt["mday"], $tt["year"]);

    $strftimedatetime = get_string("strftimedatetime");

    echo "<div class=\"info\">\n";
    print_string("displayingrecords", "", $totalcount);
    echo "</div>\n";

    //echo $OUTPUT->paging_bar($totalcount, $page, $perpage, "$url&perpage=$perpage");

    $table = new html_table();
    $table->classes = array('logtable', 'generalbox');

    $table->data = array();

    // Make sure that the logs array is an array, even it is empty, to avoid warnings from the foreach.
    if (empty($logs)) {
        $logs = array();
    }

    foreach ($logs as $log) {
        $row = array();
        $row[] = '<div class="tip ' . $log->module . '">' . manage_format_log($log, $log->module, $log->userid, $log->course, $log->action) . '</div>';
        $row[] = '<div class="tip italic align-right">' . time_ago($log->time, '') . '</div>';
        $table->data[] = $row;
    }
    echo html_writer::table($table);
    $url = "$url&perpage=$perpage";
    $url .= is_numeric($uid) && $uid > 0 ? "&uid=$uid" : "";

    echo $OUTPUT->paging_bar($totalcount, $page, $perpage, $url);
}

/**
 * @todo Get Log format
 *
 * @param string $module
 * @param int $uid
 * @param int $course
 * @param int $action
 */
function manage_format_log($log, $module, $uid, $course, $action) {
    global $CFG, $DB, $OUTPUT, $USER;
    $modulename = $module;
    switch ($module) {
        case 'user':
            switch ($action) {
                case 'login':
                    $action = get_string('loggedin');
                    break;
                case 'logout':
                    $action = get_string('loggedout');
                    break;
            }
            $htmlaction = '<span class="user-url">' . html_writer::link(new moodle_url("/manage/people/profile.php", array('id' => $log->userid)), fullname($log)) . '</span>';
            $htmlaction .= '<span class="action">' . $action . '</span>';
            break;
        case 'usernews':
            $modulename = get_string('news');
            switch ($action) {
                case 'create':
                    $htmlaction = '<span class="user-url">' . html_writer::link(new moodle_url("/manage/people/profile.php", array('id' => $log->userid)), fullname($log)) . '</span>';
                    $htmlaction .= '<span class="action">'.  get_string('postedsome').'</span>';
                    break;
            }
            break;
        case 'course':
            if (is_numeric($log->info)) {
                $sql = "SELECT * FROM course WHERE id = {$log->info}";
                $course = $DB->get_record_sql($sql);
                $coursename = $course->fullname;
            } else {
                $coursename = $log->info;
            }
            switch ($action) {
                case 'new':
                    $htmlaction = '<span class="action"> ' . html_writer::link(new moodle_url("/course/{$log->url}"), $coursename) . get_string('wascreate') .'</span>';
                    $htmlaction .= '<span class="user-url">' . html_writer::link(new moodle_url("/manage/people/profile.php", array('id' => $log->userid)), fullname($log)) . '</span>';
                    break;
                case 'view':
                    $htmlaction = '<span class="action"> ' . html_writer::link(new moodle_url("/course/{$log->url}"), $coursename) . get_string('wasviewed') .'</span>';
                    $htmlaction .= '<span class="user-url">' . html_writer::link(new moodle_url("/manage/people/profile.php", array('id' => $log->userid)), fullname($log)) . '</span>';
                    break;
                case 'update':
                    $htmlaction = '<span class="action"> ' . html_writer::link(new moodle_url("/course/{$log->url}"), $coursename) . get_string('wasupdated') .'</span>';
                    $htmlaction .= '<span class="user-url">' . html_writer::link(new moodle_url("/manage/people/profile.php", array('id' => $log->userid)), fullname($log)) . '</span>';
                    break;
                default:
                    $htmlaction = manage_format_log_default($log, $log->userid);
                    break;
            }
            break;
        default :
            $htmlaction = manage_format_log_default($log, $log->userid);
            break;
    }

    $html = '<span class="box-module"><SPAN>' . $modulename . '</SPAN></SPAN>' . $htmlaction;
    return $html;
}

/**
 * @todo Get Log format default
 *
 * @param string $module
 * @param int $uid
 * @param int $course
 * @param int $action
 */
function manage_format_log_default($log, $uid) {
    $htmlaction = '<span class="user-url">' . html_writer::link(new moodle_url("/manage/people/profile.php?id={$uid}"), fullname($log)) . '</span>';
    $htmlaction .= '<span class="action">' . $log->action . '</span>';
    return $htmlaction;
}

/**
 * Select all log records based on SQL criteria
 *
 * @todo Finish documenting this function
 *
 * @global object
 * @param string $select SQL select criteria
 * @param array $params named sql type params
 * @param string $order SQL order by clause to sort the records returned
 * @param string $limitfrom ?
 * @param int $limitnum ?
 * @param int $totalcount Passed in by reference.
 * @return object
 */
function manage_get_logs($select, array $params = null, $order = 'l.time DESC', $limitfrom = '', $limitnum = '', &$totalcount) {
    global $DB;

    if ($order) {
        $order = "ORDER BY $order";
    }

    $selectsql = "";
    $countsql = "";

    if ($select) {
        $select = "WHERE $select ";
    }

    $sql = "SELECT COUNT(*)
              FROM {log} l
           $select";

    $totalcount = $DB->count_records_sql($sql, $params);

    $sql = "SELECT l.*, u.firstname, u.lastname, u.picture
              FROM {log} l
              LEFT JOIN {user} u ON l.userid = u.id
           $select 
            $order";

    return $DB->get_records_sql($sql, $params, $limitfrom, $limitnum);
}

/**
 *
 * @global type $DB
 * @return type
 */
function get_logs_view_course($from, $to) {
    global $DB, $USER;
    // tung fix: load all data in account
//    $sql = "SELECT * FROM user ";
//    $rows = $DB->get_records_sql($sql);
//
//    $id_list = display_parent($rows, $USER->id);
//    $id_me = $id_list;
//    if ($id_me == 0) {
//        $id_me = $USER->id;
//    }
//    if ($id_list == 0) {
//        $id_list = $USER->id;
//    }
//
//    $id_list = display_children($rows, $id_list);
//    $id_list .= $USER->id . "," . $id_me;
    $sql2 = "SELECT user_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id";
    $params2['owner_id'] = $USER->id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $USER->id;
    //end fix
    $sql = "SELECT l.course, c.fullname
FROM log l
LEFT JOIN course c ON l.course = c.id
WHERE l.userid in (" . $id_list . ") AND l.module LIKE 'course' AND l.course <> 1
ORDER BY l.id DESC LIMIT $from, $to";

    return $DB->get_records_sql($sql);
}

function get_logs_view_course_second($from, $to) {
    global $DB, $USER;
//    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);
//    
//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  
//          
    $id_list = display_children($rows, $id_list);
    $id_list .= $USER->id;
//    //end fix
    $sql = "SELECT l.course, c.fullname
FROM log l
LEFT JOIN course c ON l.course = c.id
WHERE l.userid in (" . $id_list . ") AND l.module LIKE 'course' AND l.course <> 1
ORDER BY l.id DESC LIMIT $from, $to";

    return $DB->get_records_sql($sql);
}

/**
 *
 * @global type $DB
 * @return type
 */
function get_recent_achievements($num) {
    global $DB, $USER;

    $sql = "select cc.timecompleted , c.fullname from  course_completions cc
INNER JOIN course c ON cc.course = c.id
where timecompleted >0 and userid = $USER->id  ORDER BY cc.timecompleted DESC LIMIT 0, $num;";

    $completions = $DB->get_records_sql($sql);

    return $completions;
}

/**
 * Get Course Achivements
 * @global type $DB
 * @global type $USER
 * @return type
 */
function get_achievements($uid = 0) {
    global $DB, $USER;
    $uid = is_numeric($uid) && $uid > 0 ? $uid : $USER->id;

    $sql = "select cc.* , c.fullname from  course_completions cc
INNER JOIN course c ON cc.course = c.id
where timecompleted >0 and userid = $uid  ORDER BY cc.timecompleted DESC;";

    $completions = $DB->get_records_sql($sql);

    return $completions;
}

/**
 * Function get user team list
 * @param type $user
 */
function manage_get_team_list($searching = null) {
    global $DB, $USER;


    $userid = optional_param('id', 0, PARAM_INT);
    $userid = $userid ? $userid : $USER->uid;

    $query = "SELECT id, name FROM {groups} WHERE userid = $userid";
    $params = array();
    if ($searching) {
        $query .= " AND name LIKE '%$searching%'";
    }
    $query .= ' ORDER BY id DESC';

    return $DB->get_records_sql($query);
}

/**
 * Get Recent Views
 */
function manage_print_recently_viewed() {
    global $DB, $USER;
    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);

    $id_list = display_parent($rows, $USER->id);
    $id_me = $id_list;
    if ($id_me == 0) {
        $id_me = $USER->id;
    }
    if ($id_list == 0) {
        $id_list = $USER->id;
    }

    $id_list = display_children($rows, $id_list);
    $id_list .= $USER->id . "," . $id_me;
    //end fix
    $select = "userid in (" . $id_list . ") AND action = 'view' AND module IN ('course','user') AND course != 1";

    $rows = manage_get_logs($select, null, 'l.time DESC', 0, 10);

    echo "<ul>";
    $ids = array();
    $course_ids = array();
    foreach ($rows as $item) {
        if (!in_array($item->info, $course_ids)) {
            array_push($course_ids, $item->info);
            array_push($ids, $item->id);
        }
    }
    foreach ($rows as $row) {
        if (in_array($row->id, $ids)) {
            echo "<li>";
            switch (trim($row->module)) {
                case 'blog':
                    echo '<span title="" class="box-tag box-tag-blue"><span>blog</span></span>';
                    echo '<a href="' . ($row->url) . '">' . $row->info . '</a>';
                    break;
                case 'course':
                    $sql = "SELECT * FROM course WHERE id = {$row->info}";
                    $course = $DB->get_record_sql($sql);
                    $coursename = $course->fullname;
                    echo '<span title="" class="box-tag box-tag-blue"><span>course</span></span>';
                    echo '<a href="' . new moodle_url('/course/' . $row->url) . '">' . $coursename . '</a>'; //$row->action
                    break;
                case 'user';
                    echo '<span title="" class="box-tag box-tag-orange "><span>person</span></span>';
                    echo '<a href="' . $row->url . '">' . $row->firstname . ' ' . $row->lastname . '</a>';
                    break;
            }

            echo "</li>";
        }
    }
    echo "</ul>";
}

//Tung added
function manage_print_recently_viewed_second() {
    global $DB, $USER;
    // tung fix: load all data in account
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);
//    
//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

    $id_list = display_children($rows, $id_list);
    $id_list .= $USER->id;
    //end fix
    $select = "userid in (" . $id_list . ") AND action = 'view' AND module IN ('course','user') AND course != 1";

    $rows = manage_get_logs($select, null, 'l.time DESC', 0, 10);

    echo "<ul>";
    $ids = array();
    $course_ids = array();
    foreach ($rows as $item) {
        if (!in_array($item->info, $course_ids)) {
            array_push($course_ids, $item->info);
            array_push($ids, $item->id);
        }
    }
    foreach ($rows as $row) {
        if (in_array($row->id, $ids)) {
            echo "<li>";
            switch (trim($row->module)) {
                case 'blog':
                    echo '<span title="" class="box-tag box-tag-blue"><span>blog</span></span>';
                    echo '<a href="' . ($row->url) . '">' . $row->info . '</a>';
                    break;
                case 'course':
                    $sql = "SELECT * FROM course WHERE id = {$row->info}";
                    $course = $DB->get_record_sql($sql);
                    $coursename = $course->fullname;
                    echo '<span title="" class="box-tag box-tag-blue"><span>course</span></span>';
                    echo '<a href="' . new moodle_url('/course/' . $row->url) . '">' . $coursename . '</a>'; //$row->action
                    break;
                case 'user';
                    echo '<span title="" class="box-tag box-tag-orange "><span>person</span></span>';
                    echo '<a href="' . $row->url . '">' . $row->firstname . ' ' . $row->lastname . '</a>';
                    break;
            }

            echo "</li>";
        }
    }
    echo "</ul>";
}

/**
 * Form for select Courses
 * @global type $USER
 * @global type $CFG
 * @global type $DB
 * @global type $OUTPUT
 * @global type $SESSION
 * @param type $courseid
 * @param type $url
 * @param type $title
 * @return string
 */
function print_log_selector_course_form($courseid, $url, $title = null) {
    global $USER, $CFG, $DB, $OUTPUT, $SESSION;
    if (is_teacher() || is_admin()) {
        if ($ttt = enrol_get_my_courses_second()) {
            foreach ($ttt as $tt) {
                if ($tt->category) {
                    $courses["$tt->id"] = format_string($tt->fullname);
                } else {
                    $courses["$tt->id"] = format_string($tt->fullname) . ' (Site)';
                }
            }
        }
//    } elseif (is_admin()) {
//        if ($aaa = enrol_get_my_courses()) {
//            foreach ($aaa as $aa) {
//                if ($aa->category) {
//                    $courses["$aa->id"] = format_string($aa->fullname);
//                } else {
//                    $courses["$aa->id"] = format_string($aa->fullname) . ' (Site)';
//                }
//            }
//        }
    } else {

        if ($ccc = $DB->get_records("course", null, "fullname", "id,fullname,category")) {
            foreach ($ccc as $cc) {
                if ($cc->category) {
                    $courses["$cc->id"] = format_string($cc->fullname);
                } else {
                    $courses["$cc->id"] = format_string($cc->fullname) . ' (Site)';
                }
            }
        }
    }
    asort($courses);
    $form = "<h3>$title</h3>";
    $form .= "<form class=\"logselectform\" action=\"$url\" method=\"get\">\n";

    $form .= html_writer::select($courses, "id", $courseid, false);

    $form .= '<input type="submit" value="Xem" />';
    $form .= '</div>';
    $form .= '</form>';
    return $form;
}

/**
 * Prepare data for chart
 */
function prepare_data_for_chart($noOfLastDays = 30, $type = 0) {
    global $DB, $USER;

    $count = 0;
    switch ($type) {
        case 0: // unique logins
            $sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(time), '%m-%d-%y') report_date, COUNT(id) count_value 
                    FROM log 
                    WHERE action = 'login' AND userid=$USER->id AND FROM_UNIXTIME(time) >= DATE_ADD(CURDATE(), INTERVAL -$noOfLastDays DAY)
                    GROUP BY DATE_FORMAT(FROM_UNIXTIME(time), '%D %M') 
                    ORDER BY FROM_UNIXTIME(time) DESC;";
            break;
        case 1: // course sales
            $sql = "SELECT DATE_FORMAT(cs.course_sales_date, '%m-%d-%y') report_date, COUNT(c.id) count_value
                    FROM course c
                    JOIN (SELECT DISTINCT e.courseid
                            FROM enrol e
                            JOIN user_enrolments ue ON (ue.enrolid = e.id AND ue.userid = $USER->id)
                        ) en ON (en.courseid = c.id)
                        LEFT JOIN context ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = 50)
                        LEFT JOIN course_settings cs ON (cs.course_id = c.id)
                    WHERE c.id <> 1 AND cs.sell_course = 1 AND cs.course_sales_date >= DATE_ADD(CURDATE(), INTERVAL -$noOfLastDays DAY)
                    GROUP BY DATE_FORMAT(cs.course_sales_date, '%D %M') 
                    ORDER BY cs.course_sales_date DESC;";
            break;
        case 2: // course complete
            $sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(timecompleted), '%m-%d-%y') report_date, COUNT(id) count_value 
                    FROM course_completions
                    WHERE userid=$USER->id AND timecompleted IS NOT NULL AND FROM_UNIXTIME(timecompleted) >= DATE_ADD(CURDATE(), INTERVAL -$noOfLastDays DAY)
                    GROUP BY DATE_FORMAT(FROM_UNIXTIME(timecompleted), '%D %M') 
                    ORDER BY FROM_UNIXTIME(timecompleted) DESC;";
            break;
    }

    $rows = $DB->get_records_sql($sql);
    $result = "[";

    foreach ($rows as $row) {
        if ($count === 0) {
            $result = $result . "['" . $row->report_date . "', " . $row->count_value . "]";
        } else {
            $result = $result . ", ['" . $row->report_date . "', " . $row->count_value . "]";
        }
        $count++;
    }
    $result = $result . "]";
    if ($result == "[]") {
        $result = "[null]";
    }
    if ($count > 1)
        return $result;
    else
        return "[]";
}

function display_parent($rows, $parent_id) {
    // retrieve all children of $parent <br>  
    $id_list = "";
    foreach ($rows as $row) {
        if ($row->id == $parent_id) {
            $id_list = $row->parent;
            $id_list .= display_parent($rows, $row->parent);
        }
    }

    $id_list = substr($id_list, -2, 1);
    return $id_list;
}

function display_children($rows, $parent_id) {
    $id_list = "";
    foreach ($rows as $row) {
        if ($row->parent == $parent_id) {
            $id_list .= $row->id . ",";
            $id_list .= display_children($rows, $row->id);
        }
    }
    return $id_list;
}

function delete_usr($user_id){
    global $DB;
    $DB->delete_records_select('user','id = $user_id');
}
// trang add
// cac function phuc vu cho man hinh dang nhap cua gv hien thi lich hoc cua giao vien

function get_id_namhoc_theo_nhom_schedule($courseid){
  global $DB;
  $sql="SELECT DISTINCT `groups`.`id_namhoc` FROM `groups` JOIN `group_course` ON `group_course`.`group_id`= `groups`.`id` JOIN `course` ON `course`.`id` =`group_course`.`course_id` WHERE `course`.`id`={$courseid}";
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function hien_thi_lich_hoc_theo_nhom_cua_gv_login($courseid,$school_year){
    global $DB;
    // $sql="SELECT * FROM `schedule` JOIN `schedule_info` ON `schedule`.`id` =`schedule_info`.`scheduleid` WHERE `schedule`.`del`=0 AND `schedule`.`courseid`={$courseid} AND `schedule`.`school_yearid`={$school_year}";
     $sql="SELECT `schedule`.*, `schedule_info`.*, `course`.`fullname` 
     FROM `schedule` 
     JOIN `schedule_info` ON `schedule`.`id` =`schedule_info`.`scheduleid` 
     JOIN `course` on `course`.`id`= `schedule`.`courseid` 
     WHERE `schedule`.`del`=0 AND `course`.`id`={$courseid} AND `schedule`.`school_yearid`={$school_year}
        ";
    $data = $DB->get_records_sql($sql);
    return $data;
}

// lay id nam hoc hien tai
function get_id_school_year_now(){
    global $DB;
    $month=date('m');
    $year=date('Y');
    $yearstart=$year-1;
    $yearend=$year;
    if($month>8){
        $yearstart=$year;
        $yearend=$yearstart+1;
    }
    $sql = "SELECT DISTINCT `school_year`.`id` 
    FROM `school_year` 
    WHERE `school_year`.`sy_start`={$yearstart}
    AND  `school_year`.`sy_end`={$yearend}
    ";
    $data = $DB->get_field_sql($sql,NULL,$strictness=IGNORE_MISSING);
    return $data;

}
function lay_id_gvnn_thuoc_nhom_duocgan_manhinhdieukien($courseid){
        global $DB;
        $sql="SELECT DISTINCT  `user`.`id`
        FROM `user` 
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
        JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
        JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
        WHERE `course`.`id`={$courseid}
        AND `role_assignments`.`roleid`= 8";
        $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
        return $data;
    }
    function lay_id_gvtg_thuoc_nhom_duocgan_manhinhdieukien($courseid){
        global $DB;
        $sql="SELECT DISTINCT  `user`.`id`
        FROM `user` 
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
        JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
        JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
        WHERE `course`.`id`={$courseid}
        AND `role_assignments`.`roleid`= 10";
        $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
        return $data;
    }
    function id_nhom_cua_hoc_sinh_home($id){
    global $CFG, $DB;
    if (!empty($id)) {
        $sql = "SELECT `enrol`.`courseid`
        FROM `user` 
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
        WHERE `user`.`id`=:userid";
        $params = array('userid' => $id);
        $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
        return $data;
    }
    }
    function lay_id_gvtg_thuoc_nhom_lop_home($courseid){
        global $DB;
        $sql="SELECT DISTINCT  `user`.`id`
        FROM `user` 
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
        JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
        JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
        WHERE `course`.`id`={$courseid}
        AND `role_assignments`.`roleid`= 10";
        $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
        return $data;
    }

    // trang add
    function search_course_cre_by_trang($name=null,$class=null,$schools=null,$gvnn=null,$gvtg=null,$userid=null,$page,$number){
    global $DB;
    if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
    }
    // $sql = "SELECT DISTINCT
    //         C.id,
    //         C.fullname,
    //         groups.name,
    //         schools.name AS tentruong,
    //         GV.giao_vien, TG.tro_giang 
    //         FROM
    //         course C
    //         JOIN enrol ON enrol.courseid = C.id
    //         JOIN user_enrolments ON user_enrolments.enrolid = enrol.id
    //         JOIN group_course ON group_course.course_id = C.id
    //         JOIN groups ON group_course.group_id = groups.id
    //         JOIN schools ON groups.id_truong = schools.id
    //         LEFT JOIN (SELECT CONCAT(user.firstname,' ',user.lastname) AS giao_vien,user.id as userid, `enrol`.`courseid` FROM user 
    //             JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    //             JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    //             JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    //             WHERE `role_assignments`.`roleid`= 8 ) AS GV ON C.id = GV.courseid
    //         LEFT JOIN (SELECT CONCAT(user.firstname,' ',user.lastname) AS tro_giang,user.id as userid, `enrol`.`courseid` FROM user 
    //             JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    //             JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    //             JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    //             WHERE `role_assignments`.`roleid`= 10) AS TG ON C.id = TG.courseid
    //         WHERE
    //         NOT C.id =1";
             $sql = "SELECT DISTINCT
            C.id,
            C.fullname,
            groups.name,
            schools.name AS tentruong,
            GV.giao_vien, 
             GV.gvphone, 
              GV.gvmail, 
            TG.tgphone ,
            TG.tro_giang ,
            TG.tgmail 
            FROM
            course C
            JOIN enrol ON enrol.courseid = C.id
            JOIN user_enrolments ON user_enrolments.enrolid = enrol.id
            JOIN group_course ON group_course.course_id = C.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            LEFT JOIN (SELECT CONCAT(user.lastname,' ',user.firstname) AS giao_vien,user.id AS userid, `enrol`.`courseid`,  `user`.`phone2` AS gvphone, `user`.`email` AS gvmail
            FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`= 8 ) AS GV ON C.id = GV.courseid
            LEFT JOIN (SELECT CONCAT(user.lastname,' ',user.firstname) AS tro_giang,user.id AS userid, `enrol`.`courseid` ,  `user`.`phone2` AS tgphone, `user`.`email` AS tgmail
            FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`= 10) AS TG ON C.id = TG.courseid
            WHERE
            NOT C.id =1";
    $sql_count="SELECT  COUNT( DISTINCT C.id) 
           FROM
            course C
            JOIN enrol ON enrol.courseid = C.id
            JOIN user_enrolments ON user_enrolments.enrolid = enrol.id
            JOIN group_course ON group_course.course_id = C.id
            JOIN groups ON group_course.group_id = groups.id
            JOIN schools ON groups.id_truong = schools.id
            LEFT JOIN (SELECT CONCAT(user.lastname,' ',user.firstname) AS giao_vien,user.id AS userid, `enrol`.`courseid`,  `user`.`phone2` AS gvphone, `user`.`email` AS gvmail
            FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`= 8 ) AS GV ON C.id = GV.courseid
            LEFT JOIN (SELECT CONCAT(user.lastname,' ',user.firstname) AS tro_giang,user.id AS userid, `enrol`.`courseid` ,  `user`.`phone2` AS tgphone, `user`.`email` AS tgmail
            FROM user 
                JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
                JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                WHERE `role_assignments`.`roleid`= 10) AS TG ON C.id = TG.courseid
            WHERE
            NOT C.id =1 ";
    if($userid !=''){
        $sql .=" AND (`user_enrolments`.`userid`={$userid})";
        $sql_count .=" AND (`user_enrolments`.`userid`={$userid})";
    }      
    if(!empty($name)){
        $sql.=" AND fullname LIKE '%".$name."%' ";
        $sql_count.=" AND fullname LIKE '%".$name."%' ";
    }
    if(!empty($class)){
        $sql.=" AND groups.name LIKE '%".$class."%' ";
        $sql_count.=" AND groups.name LIKE '%".$class."%' ";
    }
    if(!empty($schools)){
        $sql.=" AND groups.id_truong = {$schools} ";
        $sql_count.=" AND groups.id_truong = {$schools} ";
    }
    if (!empty($gvnn)) {
        $sql.= " AND (GV.giao_vien LIKE '%".$gvnn."%') ";
        $sql_count.= " AND (GV.giao_vien LIKE '%".$gvnn."%') ";
        
    }
    if (!empty($gvtg)) {
        // $param .= " AND (tro_giang LIKE '%".$gvtg."%')";
        $sql.= " AND (TG.tro_giang LIKE '%{$gvtg}%') ";
        $sql_count.= " AND (TG.tro_giang LIKE '%{$gvtg}%') ";
    }
    $sql .= " ORDER BY C.fullname ASC LIMIT {$start} , {$number} ";
    $data= $DB->get_records_sql($sql);
    $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
    $total_page=ceil((int)$total_row / (int)$number);
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
}
function search_course_cre_by_trang2($name=null,$class=null,$schools=null,$gvnn=null,$gvtg=null,$userid=null,$page,$number){
    global $DB;
    if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
    }
     $sql="
     SELECT DISTINCT `course`.`id` , `course`.`fullname`, `groups`.`name` as groups_name , `schools`.`name` 
     FROM `course` 
     JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` 
     JOIN `groups` ON `group_course`.`group_id` = `groups`.`id` 
     JOIN `schools` ON `groups`.`id_truong` = `schools`.`id` 
     WHERE `schools`.`del`=0 ";
    $sql_count = "
    SELECT  COUNT( DISTINCT`course`.`id`)  
     FROM `course` 
     JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` 
     JOIN `groups` ON `group_course`.`group_id` = `groups`.`id` 
     JOIN `schools` ON `groups`.`id_truong` = `schools`.`id` 
     WHERE `schools`.`del`=0 ";
   
    if($userid !=''){
        $sql .=" AND `course`.`id` IN (SELECT DISTINCT `enrol`.`courseid` FROM user JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` WHERE `user`.`id`={$userid}) ";
        $sql_count .=" AND `course`.`id` IN (SELECT DISTINCT `enrol`.`courseid` FROM user JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` WHERE `user`.`id`={$userid}) ";
    }      
    if(!empty($name)){
        $sql.=" AND `course`.`fullname` LIKE '%".$name."%' ";
        $sql_count.=" AND `course`.`fullname` LIKE '%".$name."%' ";
    }
    if(!empty($class)){
        $sql.=" AND `groups`.`name` LIKE '%".$class."%' ";
        $sql_count.=" AND `groups`.`name` LIKE '%".$class."%' ";
    }
    if(!empty($schools)){
        $sql.=" AND `groups`.`id_truong` = {$schools} ";
        $sql_count.=" AND `groups`.`id_truong` = {$schools} ";
    }
    if (!empty($gvnn)) {
        $sql.= "  AND `course`.`id` IN (SELECT DISTINCT `enrol`.`courseid` FROM user JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 8 AND concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$gvnn}%') ";
        $sql_count.= "  AND `course`.`id` IN (SELECT DISTINCT `enrol`.`courseid` FROM user JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 8 AND concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$gvnn}%') ";
        
    }
    if (!empty($gvtg)) {
        // $param .= " AND (tro_giang LIKE '%".$gvtg."%')";
        $sql.= " AND `course`.`id` IN (SELECT DISTINCT `enrol`.`courseid` FROM user JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 10 AND concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$gvtg}%') ";
        $sql_count.= " AND `course`.`id` IN (SELECT DISTINCT `enrol`.`courseid` FROM user JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 10 AND concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$gvtg}%') ";
    }
    $sql .= " ORDER BY `course`.`fullname` ASC LIMIT {$start} , {$number} ";
    $data= $DB->get_records_sql($sql);
    $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
    $total_page=ceil((int)$total_row / (int)$number);
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
}
function count_members_enroll_of_course2($course){
    global $DB;
    $time = time();
    $sql = "SELECT COUNT(DISTINCT `user`.`id`)
            FROM user
                JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                JOIN user_enrolments ON `user_enrolments`.`userid` = `user`.`id`
                JOIN enrol ON `enrol`.`id` = `user_enrolments`.`enrolid`
                JOIN course ON `enrol`.`courseid` = `course`.`id`
            WHERE `user`.`id`NOT IN(2)
                AND `role_assignments`.`roleid`=5
                AND `user`.`del`=0
                AND `course`.`id` = {$course}
                AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function show_thong_tin_gvnn_gvtg_coursecre_by_trang($course){
    global $DB;
    $sql_gv="SELECT DISTINCT  `user`.`id`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id`={$course}
    AND `role_assignments`.`roleid`= 8 AND `user`.`id` NOT IN (2)";
    $idgv = $DB->get_field_sql($sql_gv,null,$strictness=IGNORE_MISSING);
    $sql_tg="SELECT DISTINCT  `user`.`id`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id`={$course}
    AND `role_assignments`.`roleid`= 10 AND `user`.`id` NOT IN (2)";
    $idtg= $DB->get_field_sql($sql_tg,null,$strictness=IGNORE_MISSING);
    $gvnn=$DB->get_record('user', array('id'=>$idgv), $fields='lastname,firstname,phone2,email', $strictness=IGNORE_MISSING);
    $gvtg=$DB->get_record('user', array('id'=>$idtg), $fields='lastname,firstname,phone2,email', $strictness=IGNORE_MISSING);
     echo'
        <td>'.$gvnn->lastname.' '.$gvnn->firstname.'</td>
        <td>'.$gvnn->mail.'</td>
        <td>'.$gvnn->phone2.'</td>
        <td>'.$gvtg->lastname.' '.$gvtg->firstname.'</td>
        <td>'.$gvtg->mail.'</td>
        <td>'.$gvtg->phone2.'</td>
      ';
}
?>