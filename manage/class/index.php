<?php
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');

require_login();
$PAGE->set_title('Danh sách lớp học');
$PAGE->set_heading('Danh sách lớp học');
echo $OUTPUT->header();


//List Class
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '1', PARAM_INT); // how many per page
$count         = $DB->count_records_select('class');
$sql = "SELECT * FROM class";

// $start = $page * $perpage;
// if ($start > count($sql)) {
//     $page = 0;
//     $start = 0;
// }
$rows = $DB->get_records_sql($sql);

$action = optional_param('action', '', PARAM_TEXT);

if ($action == 'delete') {
    $class_id = optional_param('id', '', PARAM_TEXT);
    delete_class($class_id);
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/class/index.php");
}

// if ($action == 'search') {
// $search = optional_param('search', '', PARAM_TEXT);

// $select .= " AND ".$DB->sql_like('tenlop', ':tenlop', false, false);
//         $params['tenlop'] = "%$search%";
//         $test = $DB->count_records_select('class', $select, $params);
//        print_r($test);
//     }


?>
<script src="<?php print new moodle_url('/manage/search.js'); ?>"></script>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <div class="col-md-2">
                        <a href="<?php echo new moodle_url('/manage/class/new_class.php'); ?>" class="btn btn-info">Thêm mới</a>
                    </div>
                    <div class="col-md-9 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <div class="col-4">
                                <input type="text" name="searchClass" class="form-control" placeholder="Nhập nội dung..." id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-info">Tìm kiếm</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
                    <table id="tech-companies-1" class="table  table-striped table-hover">
                        <thead>
                            <tr>
                                <th data-priority="6">Mã Lớp</th>
                                <th data-priority="6">Tên Lớp</th>
                                <th data-priority="6">Giáo Viên Chủ Nhiệm</th>
                                <th data-priority="6">Khối học sinh</th>
                                <th data-priority="6">Mô tả</th>
                                <th class="disabled-sorting text-right">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($rows as $class) { ?>
                            <tr>
                                <td><a href="" style="color: #000">
                                    <?php echo $class->malop ?></a>
                                </td>
                                <td><?php echo $class->tenlop ?></td>
                                <td><?php echo $class->id_gv ?></td>
                                <td><?php echo $class->id_khoi ?></td>
                                <td><?php echo $class->mota ?></td>

                                <td class="text-right">
                                    <a href="<?php print new moodle_url('/manage/class/edit_class.php',array('class_id'=>$class->id)); ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="<?php print new moodle_url('?action=delete',array('id'=>$class->id)); ?>" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
    echo $OUTPUT->footer();
?>
