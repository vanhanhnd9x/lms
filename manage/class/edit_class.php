<?php
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
// require_once($CFG->dirroot . '/teams/config.php');

require_login();
$PAGE->set_title('Chỉnh sửa lớp học');
$PAGE->set_heading('Chỉnh sửa lớp học');
echo $OUTPUT->header();

$class_id = optional_param('class_id', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
$class = get_class_from_id($class_id);

if ($action == 'update_class') {
  $class_id = optional_param('class_id', '', PARAM_TEXT);
  $tenlop = optional_param('tenlop', '', PARAM_TEXT);
  $malop = optional_param('malop', '', PARAM_TEXT);
  $gvcn = optional_param('gvcn', '', PARAM_TEXT);
  $khoilop = optional_param('khoilop', '', PARAM_TEXT);
  $mota = optional_param('mota', '', PARAM_TEXT);

  $new_class_id = update_class($class_id,$tenlop,$malop,$gvcn,$khoilop,$mota);

  // redirect($CFG->wwwroot . "/manage/class/class.php");
  echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/class/index.php");
}
?>

<div class="row">
  <div class="col-md-10">
    <div class="card-box">
        <form id="TypeValidation" class="" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/manage/class/edit_class.php?action=update_class&class_id=<?php echo $class_id;?>" method="post">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tên Lớp:</label>
                  <input type="text" value="<?php echo $class->tenlop ?>" name="tenlop" maxlength="255" id="tenlop" class="form-control" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label>Mã Lớp:</label>
                    <input type="text" value="<?php echo $class->malop ?>" name="malop" maxlength="255" id="malop" class="form-control " required>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Giáo viên chủ nhiệm:</label>
                  <!-- <label class="control-label">Chọn 1 giáo viên chủ nhiệm lớp:</label> -->
                  <select name="gvcn" id="gvcn" class="form-control" data-style="select-with-transition" title="" data-size="7">
                    <option value="0" <?php echo $class->id_gv == 0 ? 'selected="selected"' : ''; ?>>GV1</option>
                    <option value="1" <?php echo $class->id_gv == 1 ? 'selected="selected"' : ''; ?>>GV2</option>
                    <option value="2" <?php echo $class->id_gv == 2 ? 'selected="selected"' : ''; ?>>GV3</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Khối học:</label>
                  <!-- <label class="control-label">Chọn 1 giáo viên chủ nhiệm lớp:</label> -->
                  <select name="khoilop" id="khoilop" class="form-control" data-style="select-with-transition" title="" data-size="7">
                    <option value="0" >GV1</option>
                    <option value="1" >GV2</option>
                    <option value="2" >GV3</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                    <label>Mô tả:</label>
                    <textarea name="mota" class="form-control"><?php echo $class->mota ?></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <input type="submit" class="btn btn-success" value="Lưu">
                  <?php print_r(get_string('or')) ?>
                <a href="<?php echo $CFG->wwwroot ?>/manage/class/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
              </div>
            </div>  
        </form>
    </div>
  </div>
</div>

<?php 
echo $OUTPUT->footer();
?>