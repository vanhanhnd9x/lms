<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
$PAGE->set_title(get_string('import_semester'));
$PAGE->set_heading(get_string('import_semester'));
$PAGE->set_pagelayout(get_string('import_semester'));
require_login(0, false);
echo $OUTPUT->header();
$yeah=date('Y');
$url=$CFG->dirroot . '/manage/manage_score/excel_for_semester/';
if(!file_exists($url)){
	mkdir($url, 0777, true);
}
$action = optional_param('action', '', PARAM_TEXT);
$school = optional_param('school', '', PARAM_TEXT);

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='semester';
$name1='import_excel';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
if ($action=='import_semester') {
	$schoolid = optional_param('schoolid', '', PARAM_TEXT);
	$block_student = optional_param('block_student', '', PARAM_TEXT);
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	$groupid = optional_param('groupid', '', PARAM_TEXT);
	$studentid = optional_param('studentid', '', PARAM_TEXT);
	$number = optional_param('number', '', PARAM_TEXT);
	$school_year = optional_param('school_year', '', PARAM_TEXT);
	if ($_FILES['excel']['error'] > 0)
		echo "Upload lỗi rồi!";
	else {
		// $url=$CFG->dirroot . '/manage/excel_for_semester/';
		if($_FILES['excel']['type']=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
			//Đường dẫn file
			$newparth= $url.$_FILES["excel"]["name"];
			// move_uploaded_file($_FILES["excel"]["tmp_name"], $newparth); //Move the file from the temporary position till a new one.
			copy($_FILES["excel"]["tmp_name"], $newparth);
			$file =  $newparth;
			$objFile = PHPExcel_IOFactory::identify($file);
			$objData = PHPExcel_IOFactory::createReader($objFile);

			$objData->setReadDataOnly(true);
			$objPHPExcel = $objData->load($file);
			$sheet = $objPHPExcel->setActiveSheetIndex(0);
			$Totalrow = $sheet->getHighestRow();
			$LastColumn = $sheet->getHighestColumn();
			$TotalCol = PHPExcel_Cell::columnIndexFromString($LastColumn);
			$data = [];
			switch ($school) {
				case 1:
				for ($i = 3; $i <= $Totalrow; $i++) {

					$uid=get_id_user_by_code($sheet->getCellByColumnAndRow(0, $i)->getValue());
					if(!empty($uid)){
						$check= check_diem_hoc_ki($uid,$school_year);
						if(empty($check)){
							//hk1
							$s1_1=$sheet->getCellByColumnAndRow(2, $i)->getValue();
							$s2_1=$sheet->getCellByColumnAndRow(3, $i)->getValue();
							$s3_1=$sheet->getCellByColumnAndRow(4, $i)->getValue();
							$s4_1=$sheet->getCellByColumnAndRow(5, $i)->getValue();
							$s5_1=$sheet->getCellByColumnAndRow(6, $i)->getValue();
							$noi_1=$s1_1+$s2_1+$s3_1+$s4_1+$s5_1;
							$nghe_1=$sheet->getCellByColumnAndRow(7, $i)->getValue();
							$doc_viet_1= $sheet->getCellByColumnAndRow(8, $i)->getValue();
							$tonghocki_1=$noi_1+$nghe_1+$doc_viet_1;
							$note= $sheet->getCellByColumnAndRow(9, $i)->getValue();
							//hk2
							// hk 2
							$s1_2 = $sheet->getCellByColumnAndRow(10, $i)->getValue();
							$s2_2 = $sheet->getCellByColumnAndRow(11, $i)->getValue();
							$s3_2 = $sheet->getCellByColumnAndRow(12, $i)->getValue();
							$s4_2 = $sheet->getCellByColumnAndRow(13, $i)->getValue();
							$s5_2 = $sheet->getCellByColumnAndRow(14, $i)->getValue();
							$noi_2=$s1_2+$s2_2+$s3_2+$s4_2+$s5_2;
							$nghe_2 = $sheet->getCellByColumnAndRow(15, $i)->getValue();
							$doc_viet_2 = $sheet->getCellByColumnAndRow(16, $i)->getValue();
							$note_2 = $sheet->getCellByColumnAndRow(17, $i)->getValue();
							$tonghocki_2 =$noi_2+$nghe_2+$doc_viet_2;

							save_semester2($schoolid,$block_student,$courseid,$groupid,$uid,$school_year,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2);
						}
					}
				}
				echo displayJsAlert('', $CFG->wwwroot . "/manage/manage_score/semester_score.php");
				break;
				case 2:
				for ($i = 3; $i <= $Totalrow; $i++) {
					$uid=get_id_user_by_code($sheet->getCellByColumnAndRow(0, $i)->getValue());

					if(!empty($uid)){
						$check= check_diem_hoc_ki($uid,$school_year);
						if(empty($check)){
							//hk1
							$s1_1=$sheet->getCellByColumnAndRow(2, $i)->getValue();
							$s2_1=$sheet->getCellByColumnAndRow(3, $i)->getValue();
							$s3_1=$sheet->getCellByColumnAndRow(4, $i)->getValue();
							$s4_1=$sheet->getCellByColumnAndRow(5, $i)->getValue();
							$s5_1=$sheet->getCellByColumnAndRow(6, $i)->getValue();
							$noi_1=$s1_1+$s2_1+$s3_1+$s4_1+$s5_1;
							$nghe_1=$sheet->getCellByColumnAndRow(7, $i)->getValue();
							$viet_luan_1= $sheet->getCellByColumnAndRow(8, $i)->getValue();
							$tv_np_1= $sheet->getCellByColumnAndRow(9, $i)->getValue();
							$doc_hieu_1= $sheet->getCellByColumnAndRow(10, $i)->getValue();
							$tvnp_dochieu_viet_1=$tv_np_1+$doc_hieu_1+$viet_luan_1;
							$tonghocki_1=$noi_1+$nghe_1+$tvnp_dochieu_viet_1;
							$note= $sheet->getCellByColumnAndRow(11, $i)->getValue();
							// hk2
							// hk 2
							$s1_2 = $sheet->getCellByColumnAndRow(12, $i)->getValue();
							$s2_2 = $sheet->getCellByColumnAndRow(13, $i)->getValue();
							$s3_2 = $sheet->getCellByColumnAndRow(14, $i)->getValue();
							$s4_2 = $sheet->getCellByColumnAndRow(15, $i)->getValue();
							$s5_2 = $sheet->getCellByColumnAndRow(16, $i)->getValue();
							$noi_2=$s1_2+$s2_2+$s3_2+$s4_2+$s5_2;
							$nghe_2 = $sheet->getCellByColumnAndRow(17, $i)->getValue();
							$viet_luan_2 = $sheet->getCellByColumnAndRow(18, $i)->getValue();
							$tv_np_2 = $sheet->getCellByColumnAndRow(19, $i)->getValue();
							$doc_hieu_2 = $sheet->getCellByColumnAndRow(20, $i)->getValue();
							$tvnp_dochieu_viet_2= $tv_np_2+$doc_hieu_2+$viet_luan_2;
							$tonghocki_2  =$noi_2+$nghe_2+$tvnp_dochieu_viet_2;
							$note_2  = $sheet->getCellByColumnAndRow(21, $i)->getValue();

							save_semester2($schoolid,$block_student,$courseid,$groupid,$uid,$school_year,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2);
						}
					}
				}
				echo displayJsAlert('', $CFG->wwwroot . "/manage/manage_score/semester_score_high_school.php");
				break;
				
			}
			// for ($i = 2; $i <= $Totalrow; $i++) {
			// 	for ($j = 0; $j < $TotalCol; $j++) {
			// 		$data[$i - 2][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
			// 	}
			// }
			// echo '<pre>';
			unlink($file);
		}
	}

}
$idnamhochientai=get_id_school_year_now();
$namhoc=$DB->get_record('school_year', array(
	'id' => $idnamhochientai
));
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post" enctype="multipart/form-data">
					<div class="row">
						<!-- <div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Tìm kiếm học sinh</label>
								<input type="text" id="key" name="key" class="form-control" value="">
							</div>
							<div class="" id="return_student">
								
							</div>
						</div> -->
						<div class="col-6">
							<label for=""><?php echo get_string('school_year'); ?></label>
							<input type="text" placeholder="<?php echo $namhoc->sy_start.'-'.$namhoc->sy_end; ?>" class="form-control" disabled>
								<input type="text"  class="form-control" name="school_year" value="<?php echo $idnamhochientai; ?>" hidden>
							<!-- <select name="school_year" id="school_yearid_semester" class="form-control" required>
								<option value=""><?php echo get_string('choose_the_school_year'); ?></option>
								<?php 
								if (function_exists('get_all_school_year_in_semester')) {
                        # code...
									$school_year1=get_all_school_year_in_semester();
									if($school_year1){
										foreach ($school_year1 as $key => $value) {
                                # code...
											?>
											<option value="<?php echo $value->id ; ?>"  <?php if (!empty($school_year)&&$school_year==$value->id) { echo'selected'; } ?>><?php echo $value->sy_start,' - ',$value->sy_end ; ?></option>
											<?php 
										}
									}
								}
								?>
							</select> -->
						</div>
						<input type="text" name="action" value="import_semester" hidden="">
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control" required>
								<option value=""><?php echo get_string('schools'); ?></option>
								<?php 
								// $schools = get_all_shool();
								switch ($school) {
									case 1: $schools = danhsachtruongtieuhoc();break;
									case 2: $schools = danhsachtruongtrunghoc();break;
								}
								foreach ($schools as $key => $value) {
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control" required>
								<option value=""><?php echo get_string('block_student'); ?></option>
							</select>
						</div>

						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value=""><?php echo get_string('class'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value=""><?php echo get_string('classgroup'); ?></option>
							</select>
						</div>
						
						
						<div class="col-md-6" id="return_number">
							<label class="control-label"><?php echo get_string('selectfile'); ?><span style="color:red">*</span></label>
							<input type="file" value="Chọn file" required="" class="form-control" name="excel">
							<?php 
								switch ($school) {
									case 1: 
										$urldow = $CFG->wwwroot . '/manage/manage_score/tem/import_semester1_tieuhoc.xlsx';
										$urlweb = $CFG->wwwroot . '/manage/manage_score/semester_score.php';
									break;
									case 2:
										$urldow = $CFG->wwwroot . '/manage/manage_score/tem/import_semester1_trunghoc.xlsx';
										$urlweb = $CFG->wwwroot . '/manage/manage_score/semester_score_high_school.php';
									break;
								}
							?>
							<div class="alert-link"><a href="<?php echo $urldow; ?>"><?php echo get_string('dowload_file_sample'); ?></a></div>
						</div>
						<div class="col-md-12" style="margin-top: 10px">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $urlweb; ?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	var idnamhoc="<?php echo $idnamhochientai; ?>";
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?schoolid="+cua+"&school_year_check="+idnamhoc,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var idnamhoc1 = $('#block_student option:selected').attr('status');
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?block_student="+block+"&school_year_check="+idnamhoc1,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var groupid = this.value ;
		var idnamhoc2 = $('#groupid option:selected').attr('status');
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?groupid_check="+groupid+"&school_year_check="+idnamhoc2,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var courseid = this.value ;
		var idnamhoc3 = $('#courseid option:selected').attr('status');
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?courseid_check="+courseid+"&school_year_check="+idnamhoc3,function(data){
				$("#studentid").html(data);
			});
		}
	}); 
	
</script>
<!-- <script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var groupid = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var courseid = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+courseid,function(data){
				$("#studentid").html(data);
			});
		}
	}); 
	$('#number').on('change keyup', function() {
		var sanitized = $(this).val();
		if(sanitized){
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?number="+sanitized,function(data){
				$("#return_number").html(data);
			});
		}
	});
</script> -->