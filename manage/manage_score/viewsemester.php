<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title(get_string('viewsemester'));
$PAGE->set_heading(get_string('viewsemester'));
$PAGE->set_pagelayout(get_string('viewsemester'));
require_login(0, false);
echo $OUTPUT->header();


$action = optional_param('action', '', PARAM_TEXT);
$idview = optional_param('idview', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
$semester=$DB->get_record('semester', array(
	'id' => $idview
));

$school_year=$DB->get_record('school_year', array(
	'id' => $semester->school_yearid
));

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='semester';
$name1='viewsemester';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="edit_semeste" hidden="">
						<?php $info=show_info_student_in_unit($semester->userid,$semester->courseid,$semester->groupid); ?>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?></label>
								<input type="text" name="action" value="<?php echo $school_year->sy_start,' - ',$school_year->sy_end; ?>" disabled class="form-control">
								
							</div>
						</div>
						<?php 
						if (!empty($semester->school)) {
							switch ($semester->school) {
								case 1:
								?>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester1'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s1_1" value="<?php echo  $semester->s1_1;?>" required="" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s2_1" value="<?php echo  $semester->s2_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s3_1" value="<?php echo  $semester->s3_1;?>" required=""  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s4_1" value="<?php echo  $semester->s4_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s5_1" value="<?php echo  $semester->s5_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening') ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="nghe_1" value="<?php echo  $semester->nghe_1;?>" required="" min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('readingWriting'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('readingWriting'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->doc_viet_1;?>" required="" min="0" max="55">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('totalMidtermMark1'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('totalMidtermMark1'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->tonghocki_1;?>" required="" min="0" max="55">
								</div>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester2'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness</label>
									<input type="number" class="form-control" name="s1_2" value="<?php echo  $semester->s1_2;?>" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
									<input type="number" class="form-control" name="s2_2" value="<?php echo  $semester->s2_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
									<input type="number" class="form-control" name="s3_2" value="<?php echo  $semester->s3_2;?>"   min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy </label>
									<input type="number" class="form-control" name="s4_2" value="<?php echo  $semester->s4_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5</label>
									<input type="number" class="form-control" name="s5_2" value="<?php echo  $semester->s5_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening') ?></label>
									<input type="number" class="form-control" name="nghe_2" value="<?php echo  $semester->nghe_2;?>"  min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('readingWriting'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('readingWriting'); ?></label>
									<input type="number" class="form-control" name="doc_viet_2" value="<?php echo  $semester->doc_viet_2;?>"  min="0" max="55">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('totalMidtermMark2'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('totalMidtermMark2'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->tonghocki_2;?>" required="" min="0" max="55">
								</div>
								<?php if(($semester->tonghocki_2>0)){
									?>
									<div class="col-md-12">
										<div class="alert-info">
											<h4><?php echo get_string('overall'); ?></h4>
										</div>
									</div>
									<div class="col-md-6">
										<label class="control-label"><?php echo get_string('overall'); ?><span style="color:red">*</span></label>
										<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->tongket;?>" required="" min="0" max="55">
									</div>
									<?php 
								} ?>
								
								<?php 
								break;
								case 2:
								?>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester1'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s1_1" value="<?php echo  $semester->s1_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s2_1" value="<?php echo  $semester->s2_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s3_1" value="<?php echo  $semester->s3_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s4_1" value="<?php echo  $semester->s4_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s5_1" value="<?php echo  $semester->s5_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening') ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="nghe_1" value="<?php echo  $semester->nghe_1;?>" required="" min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('essay'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('essay'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="viet_luan_1" value="<?php echo  $semester->viet_luan_1;?>" required="" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('vocabularyGramar'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('vocabularyGramar'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="tv_np_1" value="<?php echo  $semester->tv_np_1;?>" required="" min="0" max="35">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('reading'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('reading'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_hieu_1" value="<?php echo  $semester->doc_hieu_1;?>" required="" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('totalMidtermMark1'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('totalMidtermMark1'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->tonghocki_1;?>" required="" min="0" max="55">
								</div>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester2'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness</label>
									<input type="number" class="form-control" name="s1_2" value="<?php echo  $semester->s1_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
									<input type="number" class="form-control" name="s2_2" value="<?php echo  $semester->s2_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
									<input type="number" class="form-control" name="s3_2" value="<?php echo  $semester->s3_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy</label>
									<input type="number" class="form-control" name="s4_2" value="<?php echo  $semester->s4_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5</label>
									<input type="number" class="form-control" name="s5_2" value="<?php echo  $semester->s5_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening'); ?></label>
									<input type="number" class="form-control" name="nghe_2" value="<?php echo  $semester->nghe_2;?>"  min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('essay'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('essay'); ?></label>
									<input type="number" class="form-control" name="viet_luan_2" value="<?php echo  $semester->viet_luan_2;?>" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('vocabularyGramar'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('vocabularyGramar'); ?></label>
									<input type="number" class="form-control" name="tv_np_2" value="<?php echo  $semester->tv_np_2;?>"  min="0" max="35">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('reading'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('reading'); ?></label>
									<input type="number" class="form-control" name="doc_hieu_2" value="<?php echo  $semester->doc_hieu_2;?>" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('totalMidtermMark2'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('totalMidtermMark2'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->tonghocki_2;?>" required="" min="0" max="55">
								</div>
								<?php if(($semester->tonghocki_2>0)){
									?>
									<div class="col-md-12">
										<div class="alert-info">
											<h4><?php echo get_string('overall'); ?></h4>
										</div>
									</div>
									<div class="col-md-6">
										<label class="control-label"><?php echo get_string('overall'); ?><span style="color:red">*</span></label>
										<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->tongket;?>" required="" min="0" max="55">
									</div>
									<?php 
								} ?>
								
								<?php 
								break;
							}
						}
						?>
						<div class="col-md-12" >
							<div class="row" id="return_school_status">
								
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								
								<?php 
								if(!empty($idstudent)){
									$urlRedrict=$CFG->wwwroot.'/manage/manage_score/list_semester_student.php?idstudent='.$idstudent;
								}else{
									switch ($semester->school) {
										case 1:
										$urlRedrict=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
										break;
										case 2:
										$urlRedrict= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
										break;
									}
								}
								
								?>
								<a href="<?php echo $urlRedrict; ?>" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
								<!-- <a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a> -->
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var course = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var group = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
				$("#studentid").html(data);
			});
		}
	});
	$('#school_status').on('change', function() {
		var status = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?school_status="+status,function(data){
				$("#return_school_status").html(data);
			});
		}
	});
</script>