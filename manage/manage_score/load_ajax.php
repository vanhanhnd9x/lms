 <?php 
 require_once('../../config.php');
 require_once($CFG->dirroot . '/manage/manage_score/lib.php');
 require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
 global $USER, $CFG, $DB;

 if (!empty($_GET['key'])) {
 	$key=trim($_GET['key']);
 	$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 5 AND (`user`.`firstname` LIKE '%{$key}%' OR `user`.`lastname` LIKE '%{$key}%' OR `user`.`username` LIKE '%{$key}%')";
 	$data = $DB->get_records_sql($sql);
 	if ($data) {
 		?>
 		<table id="tech-companies-1" class="table table-hover">
 			<thead>
 				<tr>
 					<th></th>
 					<th data-priority="6"></th>
 					<th data-priority="6">Tên học sinh</th>
 					<th data-priority="6">Ngày sinh</th>
 					<th data-priority="6">Email</th>
 					<th data-priority="6">Điện thoại</th>
 				</tr>
 			</thead>
 			<tbody>
 				<?php 
 				foreach ($data as $key => $value) {
 						# code...
 					?>
 					<tr>
 						<td></td>
 						<td><?php echo $value->lastname,' ',$value->firstname; ?></td>
 						<td><?php echo $value->birthday; ?></td>
 						<td><?php echo $value->email; ?></td>
 						<td><?php echo $value->phone1; ?></td>
 					</tr>
 					<?php 
 					
 				}
 				?>
 			</tbody>
 		</table>
 		<?php 
 	}
 }
 if (!empty($_POST['iddelete'])) {
 	# code...
 	$id=$_POST['iddelete'];
 	$delete=delete_unittest($id);
 }

 if (!empty($_POST['iddelete_semester'])) {
 	# code...
 	$id=$_POST['iddelete_semester'];
 	$delete=delete_semester($id);
 }
 if(!empty($_GET['month'])&&!empty($_GET['idcourse'])&&!empty($_GET['groupid'])&&!empty($_GET['school_year'])){
 		$idcourse=$_GET['idcourse'];
 		$month=$_GET['month'];
 		$groupid=$_GET['groupid'];
 		$school_year=$_GET['school_year'];
 		?>
 		<table class="table table-striped table-bordered">
			 <thead>
			     <tr>
			        <th>STT</th>
			        <th><?php echo get_string('student'); ?></th>
			        <th><?php echo get_string('score'); ?></th>
			        <th><?php echo get_string('note'); ?></th>
			     </tr>
			 </thead>
			 <?php 
			  $hs=laydanhsachhocsinhcuahomlop_danhgiahocsinh($idcourse);
			  if($hs){
			   $i=0;
			   foreach ($hs as  $h) {
			    $i++;
			    $uniths=$DB->get_record('unittest', array(
					'userid' => $h->id,
					'courseid' => $idcourse,
					'groupid' => $groupid,
					'school_yearid' => $school_year,
					'month' => $month,
					'del'=>0
				));
				if($i==1){
					?>
					<script>
						var thangdiem="<?php echo $uniths->note_2; ?>";
						$("#thangdiem").html('<input type="number" class="form-control"  value="'+thangdiem+'" id="" name="thangdiem" min="0" required="">');

					</script>
					<?php 
				}
			    ?>
			     <tr>
					<td><?php echo $i; ?></td>
					<td><?php echo  $h->lastname,' ',$h->firstname ?></td>
					<td>
						<input type="text" min="0" class="form-control" name="score[]" value="<?php echo $uniths->score_1; ?>" pattern="[0-9]{1,10}" title="Vui lòng nhập số">
						<input type="text" class="" hidden="" name="user_id[]" value="<?php echo $h->id; ?>">
						<input type="text" class="" hidden="" name="id_unittest[]" value="<?php echo $uniths->id; ?>">
					</td>
					<td>
						<input type="text" min="0" class="form-control" name="note[]" value="<?php echo $uniths->note_1; ?>">
					</td>
				</tr>
			    <?php 
			   }
			  }
			  ?>
			                   
		</table>
 		<?php 

 }

 //
 if(!empty($_GET['schoolid'])&&!empty($_GET['school_year_check'])){
 	$schoolid=$_GET['schoolid'];
 	$idnamhoc=$_GET['school_year_check'];
	$sql="SELECT * from block_student where `block_student`.`schoolid`={$schoolid} AND `block_student`.`del`=0";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">'.get_string('block_student').'</option>';
	if (!empty($data)) {
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>"<?php if(!empty($_GET['blockedit'])&&$_GET['blockedit']== $value->id) echo'selected';?> status="<?php echo $idnamhoc; ?>"><?php echo $value->name; ?></option>
			<?php 
		}
	}
 }
 if(!empty($_GET['block_student'])&&!empty($_GET['school_year_check'])){
 	$block_studentid=$_GET['block_student'];
	$idnamhoc=$_GET['school_year_check'];
	# code...
	$sql="SELECT `groups`.`id`,`groups`.`name` from groups
	JOIN `block_student` ON `block_student`.`id`=`groups`.`id_khoi`
	where `groups`.`id_khoi`={$block_studentid} AND `block_student`.`del`=0
	AND  `groups`.`id_namhoc`={$idnamhoc}
	";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">'. get_string('class').'</option>';
	if($data){
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" <?php if(!empty($_GET['groupedit'])&&$_GET['groupedit']== $value->id) echo'selected';?> status="<?php echo $idnamhoc; ?>"><?php echo $value->name; ?></option>
			<?php 
		}
	}else{
		?>
		<script>
			$("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
		</script>
		<?php 
	}
 }
 if(!empty($_GET['groupid_check'])&&!empty($_GET['school_year_check'])){
 	$groupid_check=$_GET['groupid_check'];
	$idnamhoc=$_GET['school_year_check'];
	$sql_course="SELECT `course`.`id`, `history_course_name`.`course_name` 
	FROM `history_course_name`
	JOIN `groups` ON `groups`.`id`= `history_course_name`.`groupid`
	JOIN `course` ON `course`.`id` = `history_course_name`.`courseid`
	WHERE `groups`.`id`={$groupid_check}
	AND `history_course_name`.`school_year_id`={$idnamhoc}";
	$list_course = $DB->get_records_sql($sql_course);
	echo'<option value="">'. get_string('classgroup').'</option>';
	if(!empty($list_course)){
		foreach ($list_course as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" <?php if(!empty($_GET['courseedit'])&&($_GET['courseedit']==$value->id)) echo 'selected'; ?> status="<?php echo $idnamhoc; ?>" ><?php echo $value->course_name; ?></option>
			<?php 
		}
	}
 }
 if(!empty($_GET['courseid_check'])&&!empty($_GET['school_year_check'])){
 	$courseid_check=$_GET['courseid_check'];
	$idnamhoc=$_GET['school_year_check'];
	$time=time();
	$sql="SELECT DISTINCT `user`.`id`,`user`.`firstname`,`user`.`lastname`,`role_assignments`.`roleid`
	from `user` 
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid` 
    JOIN `history_student` ON `history_student`.`iduser`=`user`.`id`
    JOIN `course` ON `course`.`id` =`history_student`.`idgroups`
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `course`.`id`= {$courseid_check}
    AND `history_student`.`idschoolyear`={$idnamhoc}
    AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)
    ORDER BY `user`.`firstname` ASC
    ";
    $data = $DB->get_records_sql($sql);
	echo'<option value="">'.get_string('student').'</option>';
	if ($data) { 
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" status="<?php echo $courseid_check; ?>"><?php echo $value->lastname,' ',$value->firstname; ?></option>
			<?php 
		}
	}

 }


 /// tai file ds sách học sinh của nhóm lớp đó để phục vụ nhap diem excel
 ?>