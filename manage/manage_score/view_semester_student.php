<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/report/in.php');
$PAGE->set_title(get_string('viewsemester'));
$PAGE->set_heading(get_string('viewsemester'));
$PAGE->set_pagelayout(get_string('viewsemester'));
require_login(0, false);
echo $OUTPUT->header();


$action = optional_param('action', '', PARAM_TEXT);
$idview = optional_param('idview', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
$semester=$DB->get_record('semester', array(
	'id' => $idview
));

$school_year=$DB->get_record('school_year', array(
	'id' => $semester->school_yearid
));
$truong=$DB->get_record('schools', array(
    'id' =>$semester->schoolid
	    ),
	$fields='id,name,level', $strictness=IGNORE_MISSING
);
$hs=$DB->get_record('user', array(
    'id' =>$semester->userid
	    ),
	$fields='id,firstname,lastname', $strictness=IGNORE_MISSING
);
$schedule=$DB->get_record('schedule', array(
  'courseid' => $semester->courseid,
  'school_yearid' => $semester->school_yearid,
  'del'=>0
));

$history_course_name=$DB->get_record('history_course_name', array(
  'school_year_id' => $semester->school_yearid,
  'courseid' => $semester->courseid,
  'groupid' => $semester->groupid,
));
$nhomvasach=$history_course_name->course_name;
if($schedule->book){$nhomvasach=$nhomvasach.' & '.$schedule->book; }
$teacher=ten_giao_vien_xuat_bao_cao_theo_nhom( $semester->courseid);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='semester';
$name1='viewsemester';
// $check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
// if(empty($check_in)){
//     echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
// }

// $sql="SELECT * FROM `school_year`";
// $data = $DB->get_records_sql($sql);
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<div class="col-md-12">
							<p class="text-center" style="font-size: 20px; text-transform:uppercase;"> 
								<span style="font-weight: bold;"><?php echo $truong->name; ?>- FINAL REPORT <?php echo $school_year->sy_start; ?> - <?php echo $school_year->sy_end; ?></span>
								<br>
								<?php 
								if($semester->tonghocki_2>0){
									echo "
										<span style='font-style: italic;'>".$truong->name." - BÁO CÁO KẾT QUẢ HỌC TẬP ".$school_year->sy_start." - ".$school_year->sy_end."</span>
									";

								}else{
									echo "
										<span style='font-style: italic;'>".$truong->name." - BÁO CÁO KẾT QUẢ HỌC TẬP HKI ".$school_year->sy_start." - ".$school_year->sy_end."</span>
									";
								}
								?>
								
								<br>
							</p>
						</div>
						
						<div class="col-md-6">
							<p>
								<label class="control-label">Student/</label><span class="chunghieng">Học sinh:</span> <span class="chudam"><?php echo $hs->lastname,' ',$hs->firstname; ?></span>
								<br><label class="control-label">Class & Book/</label><span class="chunghieng">Nhóm lớp và giáo trình :</span><span class="chudam"><?php echo $nhomvasach; ?></span>
								<br><br>
								<?php 
									if($semester->tonghocki_2>0){
										echo '<label class="control-label">Overall/</label><span class="chunghieng"> Tổng điểm TB cả năm:</span><span class="chudam">'.$semester->tongket.'/100</span>';
									}
								?>
								
							</p>
						</div>
						<div class="col-md-6">
							<?php 
								switch ($semester->school) {
									case 1:
									if($semester->tonghocki_2>0){
										?>
										<p>
											<label class="control-label">Speaking/</label><span class="chunghieng">Nói: <span class="chudam"><?php echo $semester->noi_2; ?>/25</span>
											<br><label class="control-label">Listening/</label><span class="chunghieng">Nghe: </span> <span class="chudam"><?php echo $semester->nghe_2; ?>/20</span>
											<br><label class="control-label">Reading & Writing/ </label><span class="chunghieng">Đọc hiểu & Viết: </span><span class="chunghieng"><span class="chudam"><?php echo $semester->doc_viet_2; ?>/55</span>
											<br><label class="control-label">Final mark/</label><span class="chunghieng"> Tổng điểm học kỳ II:</span><span class="chudam"><?php echo $semester->tonghocki_2; ?>/100</span>
											<br><label class="control-label">Semester 1 mark/ </label><span class="chunghieng"> Tổng điểm học kỳ I:</span><span class="chudam"><?php echo $semester->tonghocki_1; ?>/100</span>
										</p>
										<?php 
									}else{
										?>
										<p>
											<label class="control-label">Speaking/</label><span class="chunghieng">Nói: <span class="chudam"><?php echo $semester->noi_1; ?>/25</span>
											<br><label class="control-label">Listening/</label><span class="chunghieng">Nghe: </span> <span class="chudam"><?php echo $semester->nghe_1; ?>/20</span>
											<br><label class="control-label">Reading & Writing/ </label><span class="chunghieng">Đọc hiểu & Viết: </span><span class="chunghieng"><span class="chudam"><?php echo $semester->doc_viet_1; ?>/55</span>
											<br><label class="control-label">Total/</label><span class="chunghieng">Tổng điểm:</span> <span class="chudam"><?php echo $semester->tonghocki_1; ?>/100</span>
										</p>
										<?php 
									}
									?>
									<?php 
									break;
									case 2:
										if($semester->tonghocki_2>0){
											?>
											<p>
												<label class="control-label">Speaking/</label><span class="chunghieng">Nói: <span class="chudam"><?php echo $semester->noi_2; ?>/25</span>
												<br><label class="control-label">Listening/</label><span class="chunghieng">Nghe: </span> <span class="chudam"><?php echo $semester->nghe_2; ?>/20</span>
												<br><label class="control-label">Vocabulary, Grammar, Reading & Writing/</label><span class="chunghieng">Từ vựng, Ngữ pháp, Đọc hiểu & Viết: </span><span class="chunghieng"><span class="chudam"><?php echo $semester->tvnp_dochieu_viet_2; ?>/55</span>
												<br><label class="control-label">Final mark/</label><span class="chunghieng"> Tổng điểm học kỳ II:</span><span class="chudam"><?php echo $semester->tonghocki_2; ?>/100</span>
												<br><label class="control-label">Semester 1 mark/ </label><span class="chunghieng"> Tổng điểm học kỳ I:</span><span class="chudam"><?php echo $semester->tonghocki_1; ?>/100</span>
											</p>
											<?php 
										}else{
										?>
											<p>
												<label class="control-label">Speaking/</label><span class="chunghieng">Nói: <span class="chudam"><?php echo $semester->noi_1; ?>/25</span>
												<br><label class="control-label">Listening/</label><span class="chunghieng">Nghe: </span> <span class="chudam"><?php echo $semester->nghe_1; ?>/20</span>
												<br><label class="control-label">Vocabulary, Grammar, Reading & Writing/</label><span class="chunghieng">Từ vựng, Ngữ pháp, Đọc hiểu & Viết: </span><span class="chunghieng"><span class="chudam"><?php echo $semester->tvnp_dochieu_viet_1; ?>/55</span>
												<br><label class="control-label">Total/</label><span class="chunghieng">Tổng điểm:</span> <span class="chudam"><?php echo $semester->tonghocki_1; ?>/100</span>
											</p>
										<?php 
										}
									break;
								}
							?>
						</div>
						<div class="col-md-12" >
							<?php 
								if($semester->tonghocki_2>0){
									echo "<p>
										<span>(Overall = 25% semester I mark + 75% final mark/ Tổng điểm TB cả năm = 25% tổng điểm HK I + 75% tổng điểm HK II)</span>
										<br>
										<span class='chudam'>Test comments (directly related to the student's final test)/</span> <span class='chunghieng'>Nhận xét về các kỹ năng trong bài thi Học kỳ II</span>
									</p>";

								}else{
									echo "<p>
										<span class='chudam'>Test comments (directly related to the student's semester I test)/ /</span> <span class='chunghieng' Nhận xét về các kỹ năng trong bài thi Học kỳ I</span>
									</p>";
								}
								if($semester->tonghocki_2>0){
									$nx1=semestershow_nhan_xet_ky_nang_noihk2($semester->noi_2);
									$nx2=semestershow_nx_ky_nang_nghehk2($semester->nghe_2);
								}else{
									$nx1=semestershow_nhan_xet_ky_nang_noihk1($semester->noi_1);
									$nx2=semestershow_nx_ky_nang_nghehk1($semester->nghe_1);
								}
								switch ($semester->school) {
									case 1:
									if($semester->tonghocki_2>0){
										$nx3=semestershow_nx_ky_nang_dochieuvaviethk2($semester->doc_viet_2);
									}else{
										$nx3=semestershow_nx_ky_nang_dochieuvaviethk1($semester->doc_viet_1);
									}
									break;
									case 2:
									if($semester->tonghocki_2>0){
										$nx3=semestershow_nx_ky_nang_dochieuvaviethk2($semester->tvnp_dochieu_viet_2);
									}else{
										$nx3=semestershow_nx_ky_nang_dochieuvaviethk1($semester->tvnp_dochieu_viet_1);
									}
									break;
								}
							?>
							<table class=" table table-bordered t">
								<tr>
									<td class="chudam">1</td>
									<td>
										<span class="chudam">Speaking skill </span>
										<br><span class="chunghieng">Kỹ năng nói</span>
									</td>
									<td><?php echo $nx1; ?></td>
								</tr>
								<tr>
									<td class="chudam">2</td>
									<td>
										<span class="chudam">Listening skill </span>
										<br><span class="chunghieng">Kỹ năng nghe</span>
									</td>
									<td><?php echo $nx2; ?></td>
								</tr>
								<tr>
									<td class="chudam">3</td>
									<?php 
										switch ($semester->school) {
											case 1:
											echo '<td>
													<span class="chudam">Reading & Writing skills</span>
													<br> <span class="chunghiem">Kỹ năng Đọc hiểu & Viết</span>
												</td>';
											break;
											case 2:
											echo'<td>
												<span class="chudam">Vocabulary, Grammar, Reading & Writing skills</span>
												<br> <span class="chunghiem">Từ vựng, ngữ pháp, đọc hiểu và viết</span>
											</td>
											';
											break;
										}
									?>
									
									<td><?php echo $nx3; ?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-4"></div>
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<div class="text-center">
								<label for="" class="chudam"><?php echo $teacher; ?><br>Teacher</label>
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								
								<?php 
								if(!empty($idstudent)){
									$urlRedrict=$CFG->wwwroot.'/manage/manage_score/list_semester_student.php?idstudent='.$idstudent;
								}else{
									switch ($semester->school) {
										case 1:
										$urlRedrict=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
										break;
										case 2:
										$urlRedrict= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
										break;
									}
								}
								
								?>
								<!-- <a href="<?php echo $urlRedrict; ?>" class="btn btn-success"><?php print_r(get_string('back')) ?></a> -->
								<a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<style>
	.chudam{
		font-weight: bold;
	}
	.chunghieng{
		font-style: italic;
	}
</style>
<?php
echo $OUTPUT->footer();

?>
