<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('high_school_semester_score'));
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);

$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$name = trim(optional_param('name', '', PARAM_TEXT));
$school_year = optional_param('school_year', '', PARAM_TEXT);
$school_status =2;
$data=search_semester($schoolid,$block_student,$groupid,$courseid,$name,$school_year,2,$page,$number,$userid=null);
if ($action=='search') {
  $url= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&groupid='.$groupid.'&courseid='.$courseid.'&name='.$name.'&school_year='.$school_year.'&page=';
}else{
  // $data=get_all_semester_trunghoc($page,$number);
  $url=$CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php?page=';
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='semester';
$name1='high_school_semester_score';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkgan=lay_ds_cn_gan($roleid,$moodle);
?>


<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <!-- <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/manage_score/add_semeste.php?school=2'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a> 
            <p style="margin-top: 10px"></p>
            <a href="<?php echo new moodle_url('/manage/manage_score/import_semester.php?school=2'); ?>" class="btn btn-info"><?php echo get_string('import_excel'); ?></a> 
          </div> -->
           <?php 
            if(!empty($checkgan)){
              echo'<div class="col-md-2">';
              foreach ($checkgan as  $value) {
                ?>
                <a href="<?php echo $CFG->wwwroot,$value->link; ?>2" class="btn btn-success"><?php echo get_string($value->name); ?></a> 
                <p style="margin-top: 10px"></p>
                <?php 
              }
              echo'</div>';
            }
          ?>
          <div class="col-md-10">
           <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
             <div class="col-4">
              <label for=""><?php echo get_string('schools'); ?></label>
              <select name="schoolid" id="schoolid" class="form-control">
               <option value=""><?php echo get_string('schools'); ?></option>
               <?php 
               $schools = danhsachtruongtrunghoc();
               foreach ($schools as $key => $value) {
                ?>
                <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                <?php 
              }
              ?>
            </select>
          </div>
          <div class="col-4">
            <label for=""><?php echo get_string('block_student'); ?></label>
            <select name="block_student" id="block_student" class="form-control">
             <option value=""><?php echo get_string('block_student'); ?></option>
           </select>
         </div>
          <div class="col-4">
            <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
            <select name="school_year" id="school_yearid" class="form-control" >
              <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                      <?php 
                        // $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
                        // $school_year = $DB->get_records_sql($sql);
                        // foreach ($school_year as $key => $value) {
                        //   # code...
                        //  ?>
                         <!-- <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option> -->
                          <?php 
                        // }
                       ?>
            </select>
          </div>
          <div class="col-md-4">
            <label class="control-label"><?php echo get_string('class'); ?></label>
            <select name="groupid" id="groupid" class="form-control" >
              <option value=""><?php echo get_string('class'); ?></option>
            </select>
              </div>
          <div class="col-md-4">
            <label class="control-label"><?php echo get_string('classgroup'); ?></label>
            <select name="courseid" id="courseid" class="form-control" >
              <option value=""><?php echo get_string('classgroup'); ?></option>
             </select>
          </div>
         <!-- <div class="col-4">
          <label for=""><?php echo get_string('class'); ?></label>
          <select name="groupid" id="groupid" class="form-control">
            <option value=""><?php echo get_string('class'); ?></option>
          </select>
        </div>
        <div class="col-md-4">
          <label class="control-label"><?php echo get_string('classgroup'); ?></label>
          <select name="courseid" id="courseid" class="form-control" >
            <option value=""><?php echo get_string('classgroup'); ?></option>
          </select>
        </div> -->
       <div class="col-4">
        <label for=""><?php echo get_string('namestudent'); ?></label>
        <input type="text" class="form-control" name="name" placeholder="<?php echo get_string('namestudent'); ?>" value="<?php echo $name; ?>">
      </div>
      <!-- <div class="col-4">
        <label for=""><?php echo get_string('school_year'); ?></label>
        <select name="school_year" id="" class="form-control" >
         <option value=""><?php echo get_string('school_year'); ?></option>
         <?php 
         if (function_exists('get_all_school_year_in_semester')) {
                        # code...
          $school_year1=get_all_school_year_in_semester();
          if($school_year1){
            foreach ($school_year1 as $key => $value) {
                                # code...
              ?>
              <option value="<?php echo $value->id ; ?>"  <?php if (!empty($school_year)&&$school_year==$value->id) { echo'selected'; } ?>><?php echo $value->sy_start,' - ',$value->sy_end ; ?></option>
              <?php 
            }
          }
        }
        ?>
      </select>
    </div> -->
    <div class="col-4">
      <div style="    margin-top: 29px;"></div>
      <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
    </div>
  </form>
</div>
</div>



<div class="table-responsive" data-pattern="priority-columns">
    <table id="tech-companies-1" class="table  table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">STT</th>
          <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-center">'.get_string('action').'</th>';} ?>
          <th><?php echo get_string('classgroup'); ?></th>
          <th><?php echo get_string('codestudent'); ?></th>
          <th><?php echo get_string('namestudent'); ?></th>
          <th><?php echo get_string('speaking'); ?></th>
          <th><?php echo get_string('essay'); ?></th>
          <th><?php echo get_string('reading'); ?></th>
          <th><?php echo get_string('listening'); ?></th>
          <th><?php echo get_string('vocabularyGramar'); ?></th>
          <th><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
          <th><?php echo get_string('totalMidtermMark1'); ?></th>

          <th class="mauchu"><?php echo get_string('speaking'); ?></th>
          <th class="mauchu"><?php echo get_string('essay'); ?></th>
          <th class="mauchu"><?php echo get_string('reading'); ?></th>
          <th class="mauchu"><?php echo get_string('listening'); ?></th>
          <th class="mauchu"><?php echo get_string('vocabularyGramar'); ?></th>
          <th class="mauchu"><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
          <th class="mauchu"><?php echo get_string('totalMidtermMark2'); ?></th>
          <th><?php echo get_string('overall'); ?></th>
          <th><?php echo get_string('school_year'); ?></th>
         
        </tr>
      </thead>
      <tfoot>
      </tfoot>
      <tbody id="ajaxschools">
        <?php 
        if($data['data']){
         $i=0;
         foreach ($data['data'] as $sch) { 
          $i++;
           $username= $sch->lastname.' '.$sch->firstname;
           if($sch->tonghocki_2>0){
            $tonghk2=$sch->tonghocki_2;
            $tong=$sch->tongket;
          }else{
            $tonghk2=0;
            $tong=0;
          }
          $course_name=hienthitennhomloptheotungnamhoc($sch->courseid,$sch->groupid,$sch->school_yearid);
          ?>
          <tr>
            <td class="text-center"><?php echo $i; ?></td>
             <?php 
                if(!empty($hanhdong)||!empty($checkdelete)){
                  echo'<td class="text-center">';
                  $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$sch->id);
                  if(!empty($checkdelete)){
                   ?>
                  <a href="javascript:void(0)" onclick="delet_semester(<?php echo $sch->id;?>);" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                   <?php 
                  }
                  echo'</td>';
                }
            ?>
            <td><?php echo $course_name; ?></td>
            <td><?php echo $sch->code; ?></td>
            <td><?php  echo $username;?></td>
            <td class="text-center"><?php echo $sch->noi_1; ?></td>
            <td class="text-center"><?php echo $sch->viet_luan_1; ?></td>
            <td class="text-center"><?php echo $sch->doc_hieu_1; ?></td>
            <td class="text-center"><?php echo $sch->nghe_1; ?></td>
            <td class="text-center"><?php echo $sch->tv_np_1; ?></td>
            <td class="text-center"><?php echo $sch->tvnp_dochieu_viet_1; ?></td>
            <td class="text-center"><?php echo $sch->tonghocki_1; ?></td>

            <td class="text-center mauchu"><?php echo $sch->noi_2; ?></td>
            <td class="text-center mauchu"><?php echo $sch->viet_luan_2; ?></td>
            <td class="text-center mauchu"><?php echo $sch->doc_hieu_2; ?></td>
            <td class="text-center mauchu"><?php echo $sch->nghe_2; ?></td>
            <td class="text-center mauchu"><?php echo $sch->tv_np_2; ?></td>
            <td class="text-center mauchu"><?php echo $sch->tvnp_dochieu_viet_2; ?></td>
            <td class="text-center mauchu"><?php echo $tonghk2; ?></td>
            <td class="text-center"><?php echo $tong; ?></td>

            <td><?php echo show_show_yeah_in_unit($sch->school_yearid) ; ?></td>
           
            <!-- <td class="text-right">
              <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $sch->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
              <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewsemester.php?idview=<?php echo $sch->id; ?>" class="btn btn-warning" title="Xem chi tiết">
                <i class="fa fa-eye" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)" onclick="delet_semester(<?php echo $sch->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td> -->
          </tr>
          <?php 
        }
      }else{
        echo'

        <tr><td colspan="8" style="text-align: center;">'.get_string('no_records').'</td></tr>
        ';
      }

      ?>
    </tbody>
  </table>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>

</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
    .mauchu{
      color: #4bb747;
    }
</style>
<?php

echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script>
  function delet_semester(iddelete_semester){
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/semester_score_high_school.php";
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('<?php echo get_string('delete_semester'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete_semester:iddelete_semester} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>
<script>
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
        $("#school_yearid").html(data);
      });
    }
  });
   $('#school_yearid').on('change', function() {
    var blockid = $('#school_yearid option:selected').attr('status');
    var school_yearid = this.value ;
    // alert(courseid);
    $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
      $("#groupid").html(data);
    });
    
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    var school_yearid2 = $('#groupid option:selected').attr('status');
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
        $("#courseid").html(data);
      });
    }
  });
  var school = $('#schoolid').find(":selected").val();
  var blockedit = "<?php echo $block_student; ?>";
  var school_yearedit="<?php echo $school_year; ?>";
  var groupeit="<?php echo $groupid; ?>";
  var courseedit="<?php echo $courseid; ?>";
  if(school){
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    }
    if(blockedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+blockedit+"&school_yearedit="+school_yearedit,function(data){
            $("#school_yearid").html(data);
        });
    }
    if(school_yearedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockedit+"&school_year="+school_yearedit+"&groupedit="+groupeit, function(data) {
          $("#groupid").html(data);
        }); 
    }
    if(courseedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupeit+"&school_yearid2="+school_yearedit+"&courseedit="+courseedit,function(data){
            $("#courseid").html(data);
          });
    }
    
//   var school = $('#schoolid').find(":selected").val();
//   if (school) {
//     var blockedit = "<?php echo $block_student; ?>";
//     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
//       $("#block_student").html(data);
//     });
//     if(blockedit){
//       var groupedit="<?php echo $groupid; ?>";
//       // var courseedit = "<?php echo $course->id; ?>";
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
//         $("#groupid").html(data);
//       });
//     }
//     if(groupedit){
//       var courseedit="<?php echo $courseid; ?>";
//       if(courseedit){
//        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
//         $("#courseid").html(data);
//       });
//      }else{
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
//         $("#courseid").html(data);
//       });
//     }
//   }
// }
//   // ajax add
//   $('#schoolid').on('change', function() {
//     var cua = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
//         $("#block_student").html(data);
//         $("#groupid").html('<option value="">Chọn lớp</option>');
//       });
//     }
//   });
//   $('#block_student').on('change', function() {
//     var block = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
//         $("#groupid").html(data);
//       });
//     }
//   });
//   $('#groupid').on('change', function() {
//     var groupid = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
//         $("#courseid").html(data);
//       });
//     }
//   });
</script>