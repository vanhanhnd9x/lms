<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');


$PAGE->set_title(get_string('unit_test'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$school_year = optional_param('school_year', '', PARAM_TEXT);
$code = trim(optional_param('code', '', PARAM_TEXT));
$name = trim(optional_param('name', '', PARAM_TEXT));
$month = trim(optional_param('month', '', PARAM_TEXT));
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
// nam hoc hien tai

if ($action=='search') {
  $url= $CFG->wwwroot.'/manage/manage_score/index.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&school_year='.$school_year.'&groupid='.$groupid.'&courseid='.$courseid.'&code='.$code.'&name='.$name.'&month='.$month.'&page=';
}else{

  $url= $CFG->wwwroot.'/manage/manage_score/index.php?page=';
} 
$data=search_unittest($schoolid,$block_student,$groupid,$courseid,$month,$school_yearid,$name,$code,$page,$number,null);

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='unittest';
$name1='list_unitest';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoachucnangcong($roleid,$moodle);
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
$schools = get_all_shool();
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <?php 
            if(!empty($checkthemmoi)){
              ?>
              <div class="col-md-2">
                <a href="<?php echo new moodle_url('/manage/manage_score/add_unittest.php'); ?>" class="btn btn-success"><?php echo get_string('new'); ?></a>
              </div>
              <?php
            }
          ?>
           <div class="col-md-10 pull-right">
              <form action="" method="get" accept-charset="utf-8" class="form-row">
                  <input type="" name="action" class="form-control" placeholder="Nhập tên..." value="search" hidden="">
                <div class="col-4 form-group">
                  <label for=""><?php echo get_string('schools'); ?> </label>
                  <select name="schoolid" id="schoolid" class="form-control">
                    <option value=""><?php echo get_string('schools'); ?></option>
                    <?php 
                      foreach ($schools as $value) {
                      ?>
                      <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>> <?php echo $value->name; ?></option>
                      <?php 
                      }
                    ?>
                  </select>
                </div>
                <div class="col-4 form-group">
                    <label for=""><?php echo get_string('block_student'); ?></label>
                    <select name="block_student" id="block_student" class="form-control">
                        <option value=""> <?php echo get_string('block_student'); ?></option>
                    </select>
                </div>
                <div class="col-4">
                    <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
                    <select name="school_year" id="school_yearid" class="form-control" >
                      <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                    </select>
                </div>
                <div class="col-4 form-group">
                     <label for=""><?php echo get_string('class'); ?></label>
                     <select name="groupid" id="groupid" class="form-control">
                        <option value=""><?php echo get_string('class'); ?></option>
                    </select>
                </div>
                <div class="col-md-4">
                  <label class="control-label"><?php echo get_string('classgroup'); ?> </label>
                  <select name="courseid" id="courseid" class="form-control" >
                    <option value=""><?php echo get_string('classgroup'); ?></option>
                  </select>
                </div>
                <div class="col-4 form-group">
                    <label for="">
                        <?php echo get_string('codestudent'); ?></label>
                    <input type="" name="code" class="form-control" placeholder="<?php echo get_string('codestudent'); ?>" value="<?php echo trim($code); ?>">
                </div>
                <div class="col-4 form-group">
                    <label for="">
                        <?php echo get_string('full_name'); ?></label>
                    <input type="" name="name" class="form-control" placeholder="<?php echo get_string('full_name'); ?>" value="<?php echo trim($name); ?>">
                </div>
                <div class="col-4">
                  <label for=""><?php echo get_string('test'); ?></label>
                  <select name="month" id="month" class="form-control">
                      <option value="" ><?php echo get_string('test') ?></option>
                      <option value="1" ><?php echo get_string('test_1') ?></option>
                      <option value="2" ><?php echo get_string('test_2') ?></option>
                      <option value="3" ><?php echo get_string('test_3') ?></option>
                      <option value="4" ><?php echo get_string('test_4') ?></option>
                      <option value="5" ><?php echo get_string('test_5') ?></option>
                      <option value="6" ><?php echo get_string('test_6') ?></option>
                      <option value="7" ><?php echo get_string('test_7') ?></option>
                      <option value="8" ><?php echo get_string('test_8') ?></option>
                      <option value="9" ><?php echo get_string('test_9') ?></option>
                      <option value="10" ><?php echo get_string('test_10') ?></option>
                      <option value="11" ><?php echo get_string('test_11') ?></option>
                      <option value="12" ><?php echo get_string('test_12') ?></option>
                      <option value="13" ><?php echo get_string('test_13') ?></option>
                      <option value="14" ><?php echo get_string('test_14') ?></option>
                      <option value="15" ><?php echo get_string('test_15') ?></option>
                      
                  </select>
               </div>
                <div class="col-4 form-group">
                    <div style="    margin-top: 29px;"></div>
                    <button type="submit" class="btn btn-success">
                    <?php echo get_string('search'); ?></button>
                </div>
            </form>
        </div>
  </div>
  <div class="table-responsive" data-pattern="priority-columns">
    <table id="tech-companies-1" class="table  table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">STT</th>
           <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-center">'.get_string('action').'</th>';} ?>
          <th><?php echo get_string('codestudent'); ?></th>
          <th><?php echo get_string('full_name'); ?></th>
          <th><?php echo get_string('schools'); ?></th>
          <th><?php echo get_string('class'); ?></th>
          <th><?php echo get_string('classgroup'); ?></th>
          <th><?php echo get_string('school_year'); ?></th>
          <th><?php echo get_string('test'); ?></th>
          <th><?php echo get_string('score'); ?></th>
          <th><?php echo get_string('note'); ?></th>
          
          <!-- <th><?php echo get_string('view_list_unitest'); ?></th> -->
          <!-- <th><?php echo get_string('add_unitest'); ?></th> -->
        </tr>
      </thead>
      <tbody>
        <?php 
        if (!empty($data['data'])) {
                                # code...
          // $i=0;
            $i=($page-1)*20;
              foreach ($data['data'] as $student) { 
                $i++;
                $course2=show_name_course_by_cua2($student->userid,$student->school_yearid);
                
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <?php 
                  if(!empty($hanhdong)||!empty($checkdelete)){ 
                    ?>
                    <td class="text-center">
                     <?php $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$student->id); ?>
                    <!--  <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/list_unittest_student.php?idstudent=<?php echo $student->id ?>" class="btn btn-warning" title="Xem điểm unit"><i class="fa fa-eye" aria-hidden="true"></i></a>  -->
                    <?php 
                      if(!empty($checkdelete)){
                        ?>
                        <a href="javascript:void(0)" onclick="delet_unitest(<?php echo $student->id; ?>);" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        <?php 
                      }
                     ?>
                  </td>
                    <?php 
                  } 
                  ?>
                  <td><?php echo $student->code; ?></td>
                  <td><?php echo $student->studentname; ?></td>
                  <td><?php echo $student->school_name; ?></td>
                  <td><?php echo $student->group_name; ?></td>
                  <td><?php echo $course2; ?></td>
                  <td><?php echo $student->namhoc; ?></td>
                  <td><?php
                    switch ($student->month) {
                      case 1: echo get_string('test_1'); break;
                      case 2: echo get_string('test_2'); break;
                      case 3: echo get_string('test_3'); break;
                      case 4: echo get_string('test_4'); break;
                      case 5: echo get_string('test_5'); break;
                      
                      case 6: echo get_string('test_6'); break;
                      case 7: echo get_string('test_7'); break;
                      case 8: echo get_string('test_8'); break;
                      case 9: echo get_string('test_9'); break;
                      case 10: echo get_string('test_10'); break;

                      case 11: echo get_string('test_11'); break;
                      case 12: echo get_string('test_12'); break;
                      case 13: echo get_string('test_13'); break;
                      case 14: echo get_string('test_14'); break;
                      case 15: echo get_string('test_15'); break;
                    }?>
                 </td>
                 <td><?php echo $student->score_1,'/',$student->note_2; ?></td>
                 <td><?php echo $student->note_1; ?></td>
                  
                 
                 <!--  <td>
                     <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/list_unittest_student.php?idstudent=<?php echo $student->id ?>" class="btn btn-warning" title="Xem điểm unit"><i class="fa fa-eye" aria-hidden="true"></i></a> 
                  </td> -->
                </tr>
              <?php }
        }
        ?>
      </tbody>
    </table>
    <?php 
    if ($data['total_page']) {
      if ($page > 2) { 
        $startPage = $page - 2; 
      } else { 
        $startPage = 1; 
      } 
      if ($data['total_page'] > $page + 2) { 
        $endPage = $page + 2; 
      } else { 
        $endPage =$data['total_page']; 
      }
      ?>
      <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <?php 
        for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
          ?>
          <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
          <?php 
        }
        ?>
        <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
      <?php
    }
    ?>
    
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<!-- end row -->
<?php
echo $OUTPUT->footer();
$action = optional_param('action', '', PARAM_TEXT);
if ($action='delete') {
    # code...
  $iddelete = optional_param('iddelete', '', PARAM_TEXT);
  $return=delete_student($iddelete);
  if($return){
    echo displayJsAlert('Xoa thanh cong', $CFG->wwwroot . "/manage/student/index.php");
  }
}
?>
<script>
$('#schoolid').on('change', function() {
    var cua = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
            $("#block_student").html(data);
        });
    }
});
$('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
        $("#school_yearid").html(data);
      });
    }
  });
   $('#school_yearid').on('change', function() {
    var blockid = $('#school_yearid option:selected').attr('status');
    var school_yearid = this.value ;
    $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
      $("#groupid").html(data);
    });
    
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    var school_yearid2 = $('#groupid option:selected').attr('status');
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
        $("#courseid").html(data);
      });
    }
  });
    var school = $('#schoolid').find(":selected").val();
    var blockedit = "<?php echo $block_student; ?>";
    var school_yearedit="<?php echo $school_year; ?>";
    var groupeit="<?php echo $groupid; ?>";
    var courseedit="<?php echo $courseid; ?>";
  if(school){
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    }
    if(blockedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+blockedit+"&school_yearedit="+school_yearedit,function(data){
            $("#school_yearid").html(data);
        });
    }
    if(school_yearedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockedit+"&school_year="+school_yearedit+"&groupedit="+groupeit, function(data) {
          $("#groupid").html(data);
        }); 
    }
    if(courseedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupeit+"&school_yearid2="+school_yearedit+"&courseedit="+courseedit,function(data){
            $("#courseid").html(data);
          });
    }
    
</script>
<script>
  function delet_unitest(iddelete){
    var urlweb= "<?php echo $url; ?>";
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('<?php echo get_string('del_unitest'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete:iddelete} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>
