<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');
$PAGE->set_title(get_string('addsemester'));
$PAGE->set_heading(get_string('addsemester'));
$PAGE->set_pagelayout(get_string('addsemester'));
require_login(0, false);
echo $OUTPUT->header();

$school = optional_param('school', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='semester';
$name1='new';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
if ($action=='add_semeste') {
	// thong tin chung
	$schoolid = optional_param('schoolid', '', PARAM_TEXT);
	$block_student = optional_param('block_student', '', PARAM_TEXT);
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	$groupid = optional_param('groupid', '', PARAM_TEXT);
	$studentid = optional_param('studentid', '', PARAM_TEXT);
	$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
	$semester_status = optional_param('semester_status', '', PARAM_TEXT);
	$school_status = optional_param('school_status', '', PARAM_TEXT);
	$note = optional_param('note', '', PARAM_TEXT);
	$note_2 = optional_param('note_2', '', PARAM_TEXT);
	//điểm học kì 1
	// diem chung
	$s1_1 = optional_param('s1_1', '', PARAM_TEXT);
	$s2_1 = optional_param('s2_1', '', PARAM_TEXT);
	$s3_1 = optional_param('s3_1', '', PARAM_TEXT);
	$s4_1 = optional_param('s4_1', '', PARAM_TEXT);
	$s5_1 = optional_param('s5_1', '', PARAM_TEXT);
	$noi_1=$s1_1+$s2_1+$s3_1+$s4_1+$s5_1;
	$nghe_1 = optional_param('nghe_1', '', PARAM_TEXT);
	//diem cua rieng tieu hoc
	$doc_viet_1 = optional_param('doc_viet_1', '', PARAM_TEXT);
	//diem cua rieng trung hoc
	$viet_luan_1 = optional_param('viet_luan_1', '', PARAM_TEXT);
	$tv_np_1 = optional_param('tv_np_1', '', PARAM_TEXT);
	$doc_hieu_1 = optional_param('doc_hieu_1', '', PARAM_TEXT);
	//điểm học kì 2
	// diem chung
	$s1_2 = optional_param('s1_2', '', PARAM_TEXT);
	$s2_2 = optional_param('s2_2', '', PARAM_TEXT);
	$s3_2 = optional_param('s3_2', '', PARAM_TEXT);
	$s4_2 = optional_param('s4_2', '', PARAM_TEXT);
	$s5_2 = optional_param('s5_2', '', PARAM_TEXT);
	$noi_2=$s1_2+$s2_2+$s3_2+$s4_2+$s5_2;
	$nghe_2 = optional_param('nghe_2', '', PARAM_TEXT);
	//diem cua rieng tieu hoc
	$doc_viet_2 = optional_param('doc_viet_2', '', PARAM_TEXT);
	//diem cua rieng trung hoc
	$viet_luan_2 = optional_param('viet_luan_2', '', PARAM_TEXT);
	$tv_np_2 = optional_param('tv_np_2', '', PARAM_TEXT);
	$doc_hieu_2 = optional_param('doc_hieu_2', '', PARAM_TEXT);
	// kiểm tra tồn tại của điểm

	$tvnp_dochieu_viet_1= $tv_np_1+$doc_hieu_1+$viet_luan_1;
	$tvnp_dochieu_viet_2= $tv_np_2+$doc_hieu_2+$viet_luan_2;
	switch ($school) {
		case 1:
		$tonghocki_1 =$noi_1+$nghe_1+$doc_viet_1;
		$tonghocki_2 =$noi_2+$nghe_2+$doc_viet_2;
		$url=$CFG->wwwroot . "/manage/manage_score/semester_score.php";
		break;
		case 2:
		$tonghocki_1 =$noi_1+$nghe_1+$tvnp_dochieu_viet_1;
		$tonghocki_2 =$noi_2+$nghe_2+$tvnp_dochieu_viet_2;
		$url=$CFG->wwwroot . "/manage/manage_score/semester_score_high_school.php";
		break;
	}
	$check=check_diem_hoc_ki($studentid,$school_yearid);
	if (!empty($check)) {
		?>
		<script>
			var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $check;?>";
			var urlweb3= "<?php echo $CFG->wwwroot ?>/manage/manage_score/add_semeste.php?school=<?php echo $school;?>";
			var e=confirm("<?php echo get_string('issetsemester'); ?>");
			if(e==true){
				window.location=urlweb;
			}else{

			}
		</script>
		<?php 
		# code...
	}else{
		$newid=save_semester2($schoolid,$block_student,$courseid,$groupid,$studentid,$school_yearid,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2);
		if(!empty($newid)){
			add_notifications_for_student($studentid,'ĐIỂM HỌC KÌ','Thông báo điểm học kì của học sinh','/manage/manage_score/list_semester_student.php');
			echo displayJsAlert(get_string('success'),$url);
		}else{
			echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/manage_score/add_semeste.php?school=".$school);
		}
	}

	
	
}
$sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
$data = $DB->get_records_sql($sql);
$idnamhochientai=get_id_school_year_now();
$namhoc=$DB->get_record('school_year', array(
	'id' => $idnamhochientai
));
$time=time();

?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="my_form_cua" action=""  method="post">
					<div class="row">
						<input type="text" name="action" value="add_semeste" hidden="">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?></label>
								<input type="text" placeholder="<?php echo $namhoc->sy_start.'-'.$namhoc->sy_end; ?>" class="form-control" disabled>
								<input type="text"  class="form-control" name="school_yearid" value="<?php echo $idnamhochientai; ?>" hidden>
								<!-- <select name="school_yearid" id="school_yearid" class="form-control" required>
									<?php if($data){
										foreach ($data as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id ?>"><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select> -->
							</div>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control">
								<option value=""><?php echo get_string('schools'); ?></option>
								<?php 
								switch ($school) {
									case 1: $schools = danhsachtruongtieuhoc();break;
									case 2: $schools = danhsachtruongtrunghoc();break;
								}
								
								foreach ($schools as $key => $value) {
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control">
								<option value=""><?php echo get_string('block_student'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value=""><?php echo get_string('class'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('classgroup'); ?>  <span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value=""><?php echo get_string('classgroup'); ?></option>
							</select>
						</div>
						
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
							<select name="studentid" id="studentid" class="form-control" required>
								<option value=""><?php echo get_string('student'); ?></option>
							</select>
						</div>
						
						
						<?php 
						switch ($school) {
							// nhap diem hoc sinh tieu hoc
							case 1:
							?>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;"><?php echo get_string('semester1'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('speaking'); ?></h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s1_1" value="" required="" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s2_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s3_1" value="" required=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s4_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s5_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('listening'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('listening'); ?><span style="color:red">*</span></label>
								<input type="number" class="form-control" name="nghe_1" value="" required="" min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('readingWriting'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('readingWriting'); ?><span style="color:red">*</span></label>
								<input type="number" class="form-control" name="doc_viet_1" value="" required="" min="0" max="55">
							</div>
							<div class="col-md-12" >
								<label class="control-label"><?php echo get_string('note'); ?></label>
								<!-- <textarea name="note" class="form-control"></textarea> -->
								<select name="note" id="" class="form-control">
									<option value=""><?php echo get_string('note'); ?></option>
									<option value="1">Nhận xét điểm hk1 1</option>
									<option value="2">Nhận xét điểm hk1 2</option>
								</select>
							</div>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;"><?php echo get_string('semester2'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('speaking'); ?></h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness</label>
								<input type="number" class="form-control" name="s1_2" value=""  max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
								<input type="number" class="form-control" name="s2_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences</label>
								<input type="number" class="form-control" name="s3_2" value=""   min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy </label>
								<input type="number" class="form-control" name="s4_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5</label>
								<input type="number" class="form-control" name="s5_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('listening'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('listening'); ?></label>
								<input type="number" class="form-control" name="nghe_2" value=""  min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('readingWriting'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('readingWriting'); ?></label>
								<input type="number" class="form-control" name="doc_viet_2" value=""  min="0" max="55">
							</div>
							<?php 
							break;
							// end nhap diem hoc sinh tieu hoc
							// nhap diem hoc sinh trung học
							case 2:
							?>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;"><?php echo get_string('semester1'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('speaking'); ?></h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s1_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s2_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s3_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s4_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s5_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('listening'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('listening'); ?><span style="color:red">*</span></label>
								<input type="number" class="form-control" name="nghe_1" value="" required="" min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('essay'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('essay'); ?><span style="color:red">*</span></label>
								<input type="number" class="form-control" name="viet_luan_1" value="" required="" min="0" max="10">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('vocabularyGramar'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('vocabularyGramar'); ?><span style="color:red">*</span></label>
								<input type="number" class="form-control" name="tv_np_1" value="" required="" min="0" max="35">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('reading'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('reading'); ?><span style="color:red">*</span></label>
								<input type="number" class="form-control" name="doc_hieu_1" value="" required="" min="0" max="10">
							</div>
							<div class="col-md-12" >
								<label class="control-label"><?php echo get_string('note'); ?></label>
								<select name="note" id="" class="form-control">
									<option value=""><?php echo get_string('note'); ?></option>
									<option value="1">Nhận xét điểm hk1 1</option>
									<option value="2">Nhận xét điểm hk1 2</option>
								</select>
								<!-- <textarea name="note" class="form-control"></textarea> -->
							</div>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;"><?php echo get_string('semester2'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('speaking'); ?></h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness</label>
								<input type="number" class="form-control" name="s1_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
								<input type="number" class="form-control" name="s2_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
								<input type="number" class="form-control" name="s3_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy </label>
								<input type="number" class="form-control" name="s4_2" value="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5</label>
								<input type="number" class="form-control" name="s5_2" value="" min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('listening'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('listening'); ?></label>
								<input type="number" class="form-control" name="nghe_2" value=""  min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('essay'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('essay'); ?></label>
								<input type="number" class="form-control" name="viet_luan_2" value=""  min="0" max="10">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('vocabularyGramar'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('vocabularyGramar'); ?></label>
								<input type="number" class="form-control" name="tv_np_2" value=""  min="0" max="35">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4><?php echo get_string('reading'); ?></h4>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label"><?php echo get_string('reading'); ?></label>
								<input type="number" class="form-control" name="doc_hieu_2" value=""  min="0" max="10">
							</div>
							<?php 
							break;
							// nhap diem hoc sinh trung học
						}
						?>
						<div class="col-md-12" >
							<label class="control-label"><?php echo get_string('note'); ?></label>
							<!-- <textarea name="note_2" class="form-control"></textarea> -->
							<select name="note_2" id="" class="form-control">
								<option value=""><?php echo get_string('note'); ?></option>
								<option value="1">Nhận xét điểm hk1 1</option>
								<option value="2">Nhận xét điểm hk1 2</option>
							</select>
						</div>
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " >
								<!-- <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " onclick="myFunction()"> -->
								<?php print_r(get_string('or')) ?>
								<?php 
								switch ($school) {
									case 1:
										$urlRedrict=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
									break;
									case 2:
										$urlRedrict= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
									break;
								}
								?>
								<a href="<?php echo $urlRedrict ?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	var idnamhoc="<?php echo $idnamhochientai; ?>";
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?schoolid="+cua+"&school_year_check="+idnamhoc,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var idnamhoc1 = $('#block_student option:selected').attr('status');
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?block_student="+block+"&school_year_check="+idnamhoc1,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var groupid = this.value ;
		var idnamhoc2 = $('#groupid option:selected').attr('status');
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?groupid_check="+groupid+"&school_year_check="+idnamhoc2,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var courseid = this.value ;
		var idnamhoc3 = $('#courseid option:selected').attr('status');
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?courseid_check="+courseid+"&school_year_check="+idnamhoc3,function(data){
				$("#studentid").html(data);
			});
		}
	}); 
	
</script>
<!-- <script>
	function myFunction() {
		var x, text;
		var userid = document.getElementById("studentid").value;
		var school_yearid = document.getElementById("school_yearid").value;
		$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?userid="+userid+"&school_yearid="+school_yearid,function(data){
			$("#my_form_cua").html(data);
		});

	}
</script> -->