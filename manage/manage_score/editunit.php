<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title(get_string('edit_unitest'));
$PAGE->set_heading(get_string('edit_unitest'));
//now the page contents
$PAGE->set_pagelayout(get_string('edit_unitest'));
require_login(0, false);
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$idedit = optional_param('idedit', '', PARAM_TEXT);
$unittest=$DB->get_record('unittest', array(
	'id' => $idedit
));
if ($action=='edit_unit') { 
	# code...
	$note_1 = optional_param('note_1', '', PARAM_TEXT);
	$score_1 = optional_param('score_1', '', PARAM_TEXT);
	$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
	$month = optional_param('month', '', PARAM_TEXT);
	if(($month!=$unittest->month)||($school_yearid!=$unittest->school_yearid)){
		$check=check_unit_test_user($unittest->userid,$school_yearid,$month);
	}
	if(!empty($check)){
		echo displayJsAlert(get_string('issetunit'), $CFG->wwwroot . "/manage/manage_score/editunit.php?idedit=".$check);
	}else{
		if($unittest->time_update<time()){
			update_school_year_month($idedit,$school_yearid,$month);
			save_note_score($idedit,$note_1,$score_1);
			echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/index.php");
		}else{
			echo displayJsAlert(get_string('error'), $CFG->wwwroot ."/manage/manage_score/index.php");
		}
		 
	}
	
}
$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='unittest';
$moodle2='unittest';
$name1='edit';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="edit_unit" hidden="">
						<?php $info=show_info_student_in_unit($unittest->userid,$unittest->courseid,$unittest->groupid); ?>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('choose_the_school_year'); ?></label>
								<select name="school_yearid" id="" class="form-control">
									<?php if($data){
										foreach ($data as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id; ?>" <?php if(!empty($unittest->school_yearid)&&($unittest->school_yearid==$value->id)) echo'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('test'); ?></label>
								<select name="month" id="" class="form-control">
									<option value="1" <?php if(!empty($unittest->month)&&($unittest->month==1)) echo'selected'; ?>><?php echo get_string('test_1'); ?></option>
									<option value="2" <?php if(!empty($unittest->month)&&($unittest->month==2)) echo'selected'; ?>><?php echo get_string('test_2'); ?></option>
									<option value="3" <?php if(!empty($unittest->month)&&($unittest->month==3)) echo'selected'; ?>><?php echo get_string('test_3'); ?></option>
									<option value="4" <?php if(!empty($unittest->month)&&($unittest->month==4)) echo'selected'; ?>><?php echo get_string('test_4'); ?></option>
									<option value="5" <?php if(!empty($unittest->month)&&($unittest->month==5)) echo'selected'; ?>><?php echo get_string('test_5'); ?></option>
									<option value="6" <?php if(!empty($unittest->month)&&($unittest->month==6)) echo'selected'; ?>><?php echo get_string('test_6'); ?></option>
									<option value="7" <?php if(!empty($unittest->month)&&($unittest->month==7)) echo'selected'; ?>><?php echo get_string('test_7'); ?></option>
									<option value="8" <?php if(!empty($unittest->month)&&($unittest->month==8)) echo'selected'; ?>><?php echo get_string('test_8'); ?></option>
									<option value="9" <?php if(!empty($unittest->month)&&($unittest->month==9)) echo'selected'; ?>><?php echo get_string('test_9'); ?></option>
									<option value="10" <?php if(!empty($unittest->month)&&($unittest->month==10)) echo'selected'; ?>><?php echo get_string('test_10'); ?>
									<option value="11" <?php if(!empty($unittest->month)&&($unittest->month==11)) echo'selected'; ?>><?php echo get_string('test_11'); ?></option>
									<option value="12" <?php if(!empty($unittest->month)&&($unittest->month==12)) echo'selected'; ?>><?php echo get_string('test_12'); ?></option>
									<option value="12" <?php if(!empty($unittest->month)&&($unittest->month==13)) echo'selected'; ?>><?php echo get_string('test_13'); ?></option>
									<option value="12" <?php if(!empty($unittest->month)&&($unittest->month==14)) echo'selected'; ?>><?php echo get_string('test_14'); ?></option>
									<option value="12" <?php if(!empty($unittest->month)&&($unittest->month==15)) echo'selected'; ?>><?php echo get_string('test_15'); ?></option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('score'); ?></label>
								<input type="text" id="score" name="score_1" class="form-control" value="<?php echo $unittest->score_1;  ?>" min="0" pattern="[0-9]{1,10}" title="Vui lòng nhập số">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('note'); ?></label>
								<input type="text" id="note" name="note_1" class="form-control" value="<?php echo $unittest->note_1;  ?>" >
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php"class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
