<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php'); 
require_once($CFG->dirroot . '/manage/block-student/lib.php'); 
require_once ($CFG->dirroot. '/manage/notification/lib.php');

$PAGE->set_title(get_string('add_unitest'));
$PAGE->set_heading(get_string('add_unitest'));
$PAGE->set_pagelayout(get_string('add_unitest'));
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$number = optional_param('number', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$month = optional_param('month', '', PARAM_TEXT);
$note = optional_param('note', '', PARAM_TEXT);
$score = optional_param('score', '', PARAM_TEXT);
$thangdiem = optional_param('thangdiem', '', PARAM_TEXT);
 if ($action=='add_unittest') {
 	
 	$check=check_unit_test_user($idstudent,$school_yearid,$month);
 	if(!empty($check)){
 		// echo displayJsAlert(get_string('issetunit'), $CFG->wwwroot . "/manage/manage_score/editunit.php?idedit=".$check);

 		?>
 		<script>
 			var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/editunit.php?idedit=<?php echo $check;?>";
 			var e=confirm("<?php echo get_string('issetunit'); ?>");
 			if(e==true){
 				window.location=urlweb;
 			}
 		</script>
 		<?php 
		
 	}else{
 		// $newid=save_unittest($idstudent,$courseid,$groupid,$school_yearid,$month,$number);
 		$newid=save_unittest3($idstudent,$courseid,$groupid,$school_yearid,$month,$score,$note,$thangdiem);
 		add_notifications_for_student($idstudent,'BÀI KIỂM TRA NHỎ','Thông báo về điểm kiểm tra nhỏ của học sinh','/manage/manage_score/list_unittest_student.php');
 		echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/index.php");

 		
 	}
 	// $newid=save_unittest($studentid,$courseid,$groupid,$school_yearid,$month);
	

}
$idnamhochientai=get_id_school_year_now();
$namhoc=$DB->get_record('school_year', array(
	'id' => $idnamhochientai
));
$schools = get_all_shool();
$time=time();
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<input type="text" name="action" value="add_unittest" hidden="">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?></label>
								<input type="text" placeholder="<?php echo $namhoc->sy_start.'-'.$namhoc->sy_end; ?>" class="form-control" disabled>
								<input type="text"  class="form-control" name="school_yearid" value="<?php echo $idnamhochientai; ?>" hidden>
								<!-- <select name="school_yearid" id="school_yearid" class="form-control" required>
									<?php if($data){
										foreach ($data as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id ?>"><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select> -->
							</div>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control">
								<option value=""><?php echo get_string('schools'); ?></option>
								<?php 
								// switch ($school) {
								// 	case 1: $schools = danhsachtruongtieuhoc();break;
								// 	case 2: $schools = danhsachtruongtrunghoc();break;
								// }
								
								foreach ($schools as $key => $value) {
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control">
								<option value=""><?php echo get_string('block_student'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value=""><?php echo get_string('class'); ?></option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('classgroup'); ?>  <span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value=""><?php echo get_string('classgroup'); ?></option>
							</select>
						</div>
						
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
							<select name="idstudent" id="studentid" class="form-control" required>
								<option value=""><?php echo get_string('student'); ?></option>
							</select>
						</div>
						<div class="col-md-6"> 
							<label class="control-label"><?php echo get_string('test'); ?><span style="color:red">*</span></label>
							<select name="month" id="month" class="form-control">
		 						<option value="1" ><?php echo get_string('test_1') ?></option>
		 						<option value="2" ><?php echo get_string('test_2') ?></option>
		 						<option value="3" ><?php echo get_string('test_3') ?></option>
		 						<option value="4" ><?php echo get_string('test_4') ?></option>
		 						<option value="5" ><?php echo get_string('test_5') ?></option>
		 						<option value="6" ><?php echo get_string('test_6') ?></option>
		 						<option value="7" ><?php echo get_string('test_7') ?></option>
		 						<option value="8" ><?php echo get_string('test_8') ?></option>
		 						<option value="9" ><?php echo get_string('test_9') ?></option>
		 						<option value="10" ><?php echo get_string('test_10') ?></option>
		 						<option value="11" ><?php echo get_string('test_11') ?></option>
		 						<option value="12" ><?php echo get_string('test_12') ?></option>
		 						<option value="13" ><?php echo get_string('test_13') ?></option>
		 						<option value="14" ><?php echo get_string('test_14') ?></option>
		 						<option value="15" ><?php echo get_string('test_15') ?></option>
		 						
		 					</select>
						</div>
						<div class="col-md-6" > 
							<label class="control-label"><?php echo get_string('point_ladder'); ?><span style="color:red">*</span></label>
							<div id="thangdiem"><input type="text" class="form-control"  value="" id="" name="thangdiem" required="" pattern="[0-9]{1,10}" title="Vui lòng nhập số"></div>
							
						</div>
						<div class="col-md-6" > 
							<label class="control-label"><?php echo get_string('score'); ?></label>
							<div id=""><input type="text" class="form-control"  value="" id="" name="score" pattern="[0-9]{1,10}" title="Vui lòng nhập số"></div>
						</div>
						<div class="col-md-6" > 
							<label class="control-label"><?php echo get_string('note'); ?></label>
							<div id=""><input type="text" class="form-control"  value="" id="" name="note"></div>
						</div>
						<!-- <div class="col-md-12" id="return_number">
							
						</div> -->
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

// if(!empty($cua[0])&&empty(!$cua[1])&&is_numeric($cua[0])&&is_numeric($cua[1])){
  
// }else{
//   ?>
    <script>
//      alert('<?php echo get_string('errortunit'); ?>');
//      $('#submitBtn').prop("disabled", true);
//    </script>
   <?php 
// }
?>
<script>
	var idnamhoc="<?php echo $idnamhochientai; ?>";
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?schoolid="+cua+"&school_year_check="+idnamhoc,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var idnamhoc1 = $('#block_student option:selected').attr('status');
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?block_student="+block+"&school_year_check="+idnamhoc1,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var groupid = this.value ;
		var idnamhoc2 = $('#groupid option:selected').attr('status');
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?groupid_check="+groupid+"&school_year_check="+idnamhoc2,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var courseid = this.value ;
		var idnamhoc3 = $('#courseid option:selected').attr('status');
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?courseid_check="+courseid+"&school_year_check="+idnamhoc3,function(data){
				$("#studentid").html(data);
			});
		}
	}); 
	
</script>