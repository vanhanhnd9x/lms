<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_login(0, false);


$action = optional_param('action', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
if(is_student()){
  $idstudent=$USER->id;
}
$student=$DB->get_record('user', array(
  'id' => $idstudent
));
if(empty($idstudent)){
   echo displayJsAlert('Error', $CFG->wwwroot . "/manage/manage_score/index.php");
}
// $PAGE->set_title(get_string('list_semester'));
$title=get_string('list_semester').' '.get_string('student').':'.$student->lastname.' '.$student->firstname.' - '.get_string('classgroup').':'.show_name_course_by_cua($idstudent);
$PAGE->set_title($title);
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id); 
if(!is_student()){
  $moodle='student';
  $name1='list_semester_student';
  $check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
  if(empty($check_in)){
      echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
  }
}

$level_school=lay_level_truong_hoc_theo_hs($idstudent); 
$number=20;
$page=isset($_GET['page'])?$_GET['page']:1;
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);

if($action=='search'){
  $level_school = optional_param('schoolstatus', '', PARAM_TEXT);
  $url=$CFG->wwwroot.'/manage/manage_score/list_semester_student.php?action=search&idstudent='.$idstudent.'&schoolstatus='. $schoolstatus .'&school_yearid='.$school_yearid;
}else{
  $url=$CFG->wwwroot.'/manage/manage_score/list_semester_student.php?idstudent='.$idstudent.'&page=';
}



?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <?php
              // if(!is_student()){
              //   ?>
                 <!-- <div class="col-md-2">
                   <a href="<?php echo new moodle_url('/manage/manage_score/add_unittest.php?idstudent='.$idstudent); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a> 
                 </div> -->
                 <?php 
              // }
          ?>
          
          <div class="col-md-10">
           <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" value="search" hidden="">
             <input type="" name="idstudent" class="form-control"  value="<?php echo $idstudent; ?>" hidden="">
             <input type="" name="schoolstatus" class="form-control"  value="<?php echo $level_school; ?>" hidden="">
           <div class="col-4">
            <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
            <select name="school_yearid" id="" class="form-control">
             <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
             <?php 
             $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
             $school_year = $DB->get_records_sql($sql);
             foreach ($school_year as $key => $value) {
               # code...
              ?>
              <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
              <?php 
             }
             ?>
           </select>
         </div>
     <div class="col-4">
      <div style="    margin-top: 29px;"></div>
      <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
    </div>
  </form>
</div>
</div>

<div class="table-responsive" data-pattern="priority-columns" id="searchResults">
    <?php 
    // load diem tieu hoc
      if($level_school==1):
        $data=lay_diem_hoc_ky_theo_hoc_sinh_cua($school_yearid,1,$page,$number,$idstudent);
        // var_dump($data);
        ?>
        <table id="tech-companies-1" class="table  table-striped table-hover">
          <thead>
            <tr>
              <th class="text-center">STT</th>
              <th><?php echo get_string('classgroup'); ?></th>
              <th><?php echo get_string('codestudent'); ?></th>
              <th><?php echo get_string('namestudent'); ?></th>
              <th><?php echo get_string('speaking'); ?></th>
              <th><?php echo get_string('listening'); ?></th>
              <th><?php echo get_string('readingWriting'); ?></th>
              <th><?php echo get_string('totalMidtermMark1'); ?></th>

              <th class="mauchu"><?php echo get_string('speaking'); ?></th>
              <th class="mauchu"><?php echo get_string('listening'); ?></th>
              <th class="mauchu"><?php echo get_string('readingWriting'); ?></th>
              <th class="mauchu"><?php echo get_string('totalMidtermMark2'); ?></th>
              <th class=""><?php echo get_string('overall'); ?></th>
              <th class=""><?php echo get_string('school_year'); ?></th>
              <th class="disabled-sorting text-right"><?php echo get_string('action'); ?></th>
            </tr>
          </thead>
          <tbody id="ajaxschools">
              <?php 
              if($data['data']){
               $i=0;
               foreach ($data['data'] as $sch) { 
                $i++;
                $username= $sch->lastname.' '.$sch->firstname;
                if($sch->tonghocki_2>0){
                  $tonghk2=$sch->tonghocki_2;
                  $tong=$sch->tongket;
                }else{
                  $tonghk2=0;
                  $tong=0;
                }
                // $namehoc=$DB->get_record('school_year', array('id' => $sch->school_yearid));
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <td><?php echo $sch->fullname; ?></td>
                  <td><?php echo $sch->code; ?></td>
                  <td><?php  echo $username;?></td>
                  <td class="text-center"><?php echo $sch->noi_1; ?></td>
                  <td class="text-center"><?php echo $sch->nghe_1; ?></td>
                  <td class="text-center"><?php echo $sch->doc_viet_1; ?></td>
                  <td class="text-center"><?php echo $sch->tonghocki_1; ?></td>

                  <td class="text-center mauchu"><?php echo $sch->noi_2; ?></td>
                  <td class="text-center mauchu"><?php echo $sch->nghe_2; ?></td>
                  <td class="text-center mauchu"><?php echo $sch->doc_viet_2; ?></td>
                  <td class="text-center mauchu"><?php echo $tonghk2; ?></td>

                  <td class="text-center"><?php echo $tong; ?></td>
                  <td><?php echo show_show_yeah_in_unit($sch->school_yearid); ?></td>
                  <td class="text-center">
                    <?php
                     if(!is_student()){
                      
                      ?>
                      <!-- <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $sch->id; ?>&idstudent=<?php echo $idstudent; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                      <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/view_semester_student.php?idview=<?php echo $sch->id; ?>&idstudent=<?php echo $idstudent; ?>" class=" mauicon" title="Xem chi tiết">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <!-- <a href="javascript:void(0)" onclick="delet_semester(<?php echo $sch->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                      <?php 
                     }
                     if(is_student()){
                      ?>
                        <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/view_semester_student.php?idview=<?php echo $sch->id; ?>&idstudent=<?php echo $idstudent; ?>" class="mauicon" title="Xem chi tiết">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <?php 
                     }
                    ?>
                    
                  </td>
                </tr>
          <?php 
        }
      }else{
        echo'
        <tr><td colspan="8" style="text-align: center;">'.get_string('no_records').'</td></tr>';
      }

      ?>
    </tbody>
  </table>
        <?php 
      endif;
      // load diem trung hoc
      if(in_array($level_school,array('2','3'))):
        $data=lay_diem_hoc_ky_theo_hoc_sinh_cua($school_yearid,2,$page,$number,$idstudent);
        ?>
          <table id="tech-companies-1" class="table  table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">STT</th>
                <th><?php echo get_string('classgroup'); ?></th>
                <th><?php echo get_string('codestudent'); ?></th>
                <th><?php echo get_string('namestudent'); ?></th>
                <th><?php echo get_string('speaking'); ?></th>
                <th><?php echo get_string('listening'); ?></th>
                <?php 
                    if(!is_student()){
                      echo '
                      <th>'.get_string('essay').'</th>
                      <th>'.get_string('reading').'</th>
                      <th>'.get_string('vocabularyGramar').'</th>
                      ';
                    }
                 ?>
                

                <th><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
                <th><?php echo get_string('totalMidtermMark1'); ?></th>

                <th class="mauchu"><?php echo get_string('speaking'); ?></th>
                <th class="mauchu"><?php echo get_string('listening'); ?></th>

                 <?php 
                    if(!is_student()){
                      echo '
                      <th>'.get_string('essay').'</th>
                      <th>'.get_string('reading').'</th>
                      <th>'.get_string('vocabularyGramar').'</th>
                      ';
                    }
                 ?>

                <th class="mauchu"><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
                <th class="mauchu"><?php echo get_string('totalMidtermMark2'); ?></th>
                <th><?php echo get_string('overall'); ?></th>
                <th><?php echo get_string('school_year'); ?></th>
                <th class="disabled-sorting text-right"><?php echo get_string('action'); ?></th>
              </tr>
            </thead>
            <tfoot>
            </tfoot>
            <tbody id="ajaxschools">
              <?php 
              if($data['data']){
               $i=0;
               foreach ($data['data'] as $sch) { 
                $i++;
                $username= $sch->lastname.' '.$sch->firstname;
                if($sch->tonghocki_2>0){
                  $tonghk2=$sch->tonghocki_2;
                  $tong=$sch->tongket;
                }else{
                  $tonghk2=0;
                  $tong=0;
                }
                // $namehoc=$DB->get_record('school_year', array('id' => $sch->school_yearid));
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <td><?php echo $sch->fullname; ?></td>
                  <td><?php echo $sch->code; ?></td>
                  <td><?php  echo $username;?></td>
                  <td class="text-center"><?php echo $sch->noi_1; ?></td>
                  <td class="text-center"><?php echo $sch->nghe_1; ?></td>
                   <?php 
                    if(!is_student()){
                      echo '
                      <th class="text-center">'.$sch->viet_luan_1.'</th>
                      <th class="text-center">'.$sch->doc_hieu_1.'</th>
                      <th class="text-center">'.$sch->tv_np_1.'</th>
                      ';
                    }
                 ?>
                  <!-- <td class="text-center"><?php echo $sch->viet_luan_1; ?></td>
                  <td class="text-center"><?php echo $sch->doc_hieu_1; ?></td>
                  <td class="text-center"><?php echo $sch->tv_np_1; ?></td> -->

                  <td class="text-center"><?php echo $sch->tvnp_dochieu_viet_1; ?></td>
                  <td class="text-center"><?php echo $sch->tonghocki_1; ?></td>

                  <td class="text-center mauchu"><?php echo $sch->noi_2; ?></td>
                  <td class="text-center mauchu"><?php echo $sch->nghe_2; ?></td>
                   <?php 
                    if(!is_student()){
                      echo '
                      <th class="text-center mauchu">'.$sch->viet_luan_2.'</th>
                      <th class="text-center mauchu">'.$sch->doc_hieu_2.'</th>
                      <th class="text-center mauchu">'.$sch->tv_np_2.'</th>
                      ';
                    }
                 ?>
                  <!-- <td class="text-center mauchu"><?php echo $sch->viet_luan_2; ?></td>
                  <td class="text-center mauchu"><?php echo $sch->doc_hieu_2; ?></td>
                  <td class="text-center mauchu"><?php echo $sch->tv_np_2; ?></td> -->

                  <td class="text-center mauchu"><?php echo $sch->tvnp_dochieu_viet_2; ?></td>
                  <td class="text-center mauchu"><?php echo $tonghk2; ?></td>
                  <td class="text-center"><?php echo $tong; ?></td>

                  <td><?php echo show_show_yeah_in_unit($sch->school_yearid) ; ?></td>
                  <td class="text-center">
                    <?php 
                      if(is_student()){
                        ?>
                         <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewsemester.php?idview=<?php echo $sch->id; ?>&idstudent=<?php echo $idstudent; ?>" class="mauicon" title="Xem chi tiết">
                          <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        <?php 
                      }
                      if(!is_student()){
                        ?>
                        <!--  <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $sch->id; ?>&idstudent=<?php echo $idstudent; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                        <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewsemester.php?idview=<?php echo $sch->id; ?>&idstudent=<?php echo $idstudent; ?>" class="mauicon" title="Xem chi tiết">
                          <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                       <!--  <a href="javascript:void(0)" onclick="delet_semester(<?php echo $sch->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                        <?php 
                      }
                    ?>
                    
                  </td>
                </tr>
                <?php 
              }
            }else{
              echo'

              <tr><td colspan="8" style="text-align: center;">Dữ liệu trống</td></tr>
              ';
            }

            ?>
          </tbody>
        </table>
        <?php 
        
      endif;
    ?>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>
</div>
</div>
</div>
</div>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
     .mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }
    .mauchu{
      color: #4bb747;
    }
</style>
<?php

echo $OUTPUT->footer();
?>
<script>
  function delet_semester(iddelete_semester){
    var studentid="<?php echo $idstudent; ?>"
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/list_semester_student.php?idstudent="+studentid;
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('<?php echo get_string('delete_semester'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete_semester:iddelete_semester} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>