<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');

$PAGE->set_title(get_string('view_unitest'));
$PAGE->set_heading(get_string('view_unitest'));
//now the page contents
$PAGE->set_pagelayout(get_string('view_unitest'));
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idview = optional_param('idview', '', PARAM_TEXT);


$unittest=$DB->get_record('unittest', array(
	'id' => $idview
));
$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
$school_year=$DB->get_record('school_year', array(
	'id' => $unittest->school_yearid
));
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="edit_unit" hidden="">
						<?php $info=show_info_student_in_unit($unittest->userid,$unittest->courseid,$unittest->groupid); ?>

						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?></label>
								<input type="text" class="form-control" value="<?php echo $school_year->sy_start,' - ',$school_year->sy_end; ?>" disabled="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('month'); ?></label>
								<input type="text" value="<?php echo show_month_in_unit( $unittest->month); ?>" class="form-control" disabled>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('score') ?></label>
								<input type="text" id="score" name="score[]" class="form-control" value="<?php echo $unittest->score_1;  ?>" disabled>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('note') ?></label>
										<input type="text" id="note" name="note[]" class="form-control" value="<?php echo $unittest->note_1;  ?>" disabled>
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group label-floating">
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/ist_unittest_student.php?idstudent=<?php echo $unittest->userid;?>" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
							</div>
						</div>


					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
