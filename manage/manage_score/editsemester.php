<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title(get_string('editsemester'));
$PAGE->set_heading(get_string('editsemester'));
$PAGE->set_pagelayout(get_string('editsemester'));
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idedit = optional_param('idedit', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
$course = optional_param('course', '', PARAM_TEXT);
$semester=$DB->get_record('semester', array(
	'id' => $idedit
));
$namhoc=$DB->get_record('school_year', array(
	'id' => $semester->school_yearid
));

$urlRequest=$CFG->wwwroot . "/manage/manage_score/editsemester.php?idedit=".$semester->id;
if(!empty($idstudent)){
$urlRequest=$CFG->wwwroot . "/manage/manage_score/list_semester_student.php?idstudent=".$idstudent;
}
if(!empty($course)){
$urlRequest=$CFG->wwwroot . "/manage/manage_score/list_semester_student_course.php?courseid=".$course;
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='semester';
$name1='editsemester';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

	// diem hk 1
	$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
	$semester_status = optional_param('semester_status', '', PARAM_TEXT);
	$s1_1 = optional_param('s1_1', '', PARAM_TEXT);
	$s2_1 = optional_param('s2_1', '', PARAM_TEXT);
	$s3_1 = optional_param('s3_1', '', PARAM_TEXT);
	$s4_1 = optional_param('s4_1', '', PARAM_TEXT);
	$s5_1 = optional_param('s5_1', '', PARAM_TEXT);
	$nghe_1 = optional_param('nghe_1', '', PARAM_TEXT);
	//diem cua rieng tieu hoc
	$doc_viet_1 = optional_param('doc_viet_1', '', PARAM_TEXT);
	//diem cua rieng trung hoc
	$viet_luan_1 = optional_param('viet_luan_1', '', PARAM_TEXT);
	$tv_np_1 = optional_param('tv_np_1', '', PARAM_TEXT);
	$doc_hieu_1 = optional_param('doc_hieu_1', '', PARAM_TEXT);
	$note_2 = optional_param('note_2', '', PARAM_TEXT);
	$note = optional_param('note', '', PARAM_TEXT);

	$time_close=mktime(11, 59, 59, 1,1, $namhoc->sy_end);
	$time_now=time();
	$disabled='';
	if($time_close<$time_now){
		$disabled='disabled';
		$s1_1=$semester->s1_1;
		$s2_1=$semester->s2_1;
		$s3_1=$semester->s3_1;
		$s4_1=$semester->s4_1;
		$s5_1=$semester->s5_1;
		$nghe_1=$semester->nghe_1;
		$doc_viet_1=$semester->doc_viet_1;
		$viet_luan_1=$semester->viet_luan_1;
		$tv_np_1=$semester->tv_np_1;
		$doc_hieu_1=$semester->doc_hieu_1;
		$note=$semester->note;
	}
if ($action=='edit_semeste') {
	
	$noi_1=$s1_1+$s2_1+$s3_1+$s4_1+$s5_1;
	
	//điểm học kì 2
	// diem chung
	$s1_2 = optional_param('s1_2', '', PARAM_TEXT);
	$s2_2 = optional_param('s2_2', '', PARAM_TEXT);
	$s3_2 = optional_param('s3_2', '', PARAM_TEXT);
	$s4_2 = optional_param('s4_2', '', PARAM_TEXT);
	$s5_2 = optional_param('s5_2', '', PARAM_TEXT);
	$noi_2=$s1_2+$s2_2+$s3_2+$s4_2+$s5_2;
	$nghe_2 = optional_param('nghe_2', '', PARAM_TEXT);
	//diem cua rieng tieu hoc
	$doc_viet_2 = optional_param('doc_viet_2', '', PARAM_TEXT);
	//diem cua rieng trung hoc
	$viet_luan_2 = optional_param('viet_luan_2', '', PARAM_TEXT);
	$tv_np_2 = optional_param('tv_np_2', '', PARAM_TEXT);
	$doc_hieu_2 = optional_param('doc_hieu_2', '', PARAM_TEXT);
	// kiểm tra tồn tại của điểm
	$tvnp_dochieu_viet_1= $tv_np_1+$doc_hieu_1+$viet_luan_1;
	$tvnp_dochieu_viet_2= $tv_np_2+$doc_hieu_2+$viet_luan_2;
	switch ($semester->school) {
		case 1:
		$tonghocki_1 =$noi_1+$nghe_1+$doc_viet_1;
		$tonghocki_2 =$noi_2+$nghe_2+$doc_viet_2;
		break;
		case 2:
		$tonghocki_1 =$noi_1+$nghe_1+$tvnp_dochieu_viet_1;
		$tonghocki_2 =$noi_2+$nghe_2+$tvnp_dochieu_viet_2;
		break;
	}

	if ($school_yearid==$semester->school_yearid){ 
		// update_semester($idedit,$school_yearid,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,,$note,$note_2);
		update_semester($idedit,$school_yearid,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2);
		echo displayJsAlert(get_string('success'), $urlRequest);
		// if(!empty($idstudent)){
		// 		echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/list_semester_student.php?idstudent=".$idstudent);
		// }else{
		// 	echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/editsemester.php?idedit=".$semester->id);
		// }
		

	}
	if ($semester->school_yearid!=$school_yearid) { 
		# code...
		$check=check_diem_hoc_ki($semester->userid,$school_yearid);
		if(!empty($check)){
			?>
			<script>
				var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $check;?>";
				var e=confirm("<?php echo get_string('issetsemester'); ?>");
				if(e==true){
					window.location=urlweb;
				}
			</script>
			<?php 

		}else{
			// update_semester($idedit,$school_yearid,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,,$note,$note_2);
			update_semester($idedit,$school_yearid,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2);
			echo displayJsAlert(get_string('success'),$urlRequest);
			// if(!empty($idstudent)){
			// 	echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/list_semester_student.php?idstudent=".$idstudent);
			// }else{
			// 	echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/editsemester.php?idedit=".$semester->id);
			// }
			

		}
	}
	
	
	
	
}


?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="edit_semeste" hidden="">
						<?php
						 // $info=show_info_student_in_unit($semester->userid,$semester->courseid,$semester->groupid);
						 $info=show_info_student_in_unit2($semester->userid,$semester->courseid,$semester->groupid,$semester->school_yearid);
						  ?>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?><span style="color:red">*</span></label>
								<input type="text" placeholder="<?php echo $namhoc->sy_start.'-'.$namhoc->sy_end; ?>" class="form-control" disabled>
								<input type="text"  class="form-control" name="school_yearid" value="<?php echo $semester->school_yearid; ?>" hidden>
								<!-- <select name="school_yearid" id="" class="form-control" required>
									<?php 
									$namhoc=hien_thi_nam_hoc_tai_semeter();
									if($namhoc){
										foreach ($namhoc as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id; ?>" <?php if(!empty($semester->school_yearid)&&($semester->school_yearid==$value->id)) echo'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select> -->
							</div>
						</div>
						<?php 
						if (!empty($semester->school)) {
							switch ($semester->school) {
								case 1:
								?>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester1'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s1_1" value="<?php echo  $semester->s1_1;?>" required="" max="5" <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s2_1" value="<?php echo  $semester->s2_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s3_1" value="<?php echo  $semester->s3_1;?>" required=""  min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s4_1" value="<?php echo  $semester->s4_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s5_1" value="<?php echo  $semester->s5_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="nghe_1" value="<?php echo  $semester->nghe_1;?>" required="" min="0" max="20"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('readingWriting'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('readingWriting'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->doc_viet_1;?>" required="" min="0" max="55"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12" >
									<label class="control-label"><?php echo get_string('note'); ?></label>
									<!-- <textarea name="note" class="form-control" value="<?php echo  $semester->note;?>" ><?php echo  $semester->note;?></textarea> -->
									<select name="note" id="" class="form-control" <?php echo $disabled; ?>>
										<option value=""><?php echo get_string('note'); ?></option>
										<option value="1" <?php if(($semester->note)&&($semester->note==1)) echo 'selected'; ?>>Nhận xét điểm hk1 1</option>
										<option value="2" <?php if(($semester->note)&&($semester->note==2)) echo 'selected'; ?>>Nhận xét điểm hk1 2</option>
									</select>
								</div>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester2'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness</label>
									<input type="number" class="form-control" name="s1_2" value="<?php echo  $semester->s1_2;?>" max="5" >
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
									<input type="number" class="form-control" name="s2_2" value="<?php echo  $semester->s2_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
									<input type="number" class="form-control" name="s3_2" value="<?php echo  $semester->s3_2;?>"   min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy </label>
									<input type="number" class="form-control" name="s4_2" value="<?php echo  $semester->s4_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5</label>
									<input type="number" class="form-control" name="s5_2" value="<?php echo  $semester->s5_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening'); ?></label>
									<input type="number" class="form-control" name="nghe_2" value="<?php echo  $semester->nghe_2;?>"  min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('readingWriting'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('readingWriting'); ?></label>
									<input type="number" class="form-control" name="doc_viet_2" value="<?php echo  $semester->doc_viet_2;?>"  min="0" max="55">
								</div>
								<?php 
								break;
								case 2:
								?>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester1'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s1_1" value="<?php echo  $semester->s1_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s2_1" value="<?php echo  $semester->s2_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s3_1" value="<?php echo  $semester->s3_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s4_1" value="<?php echo  $semester->s4_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s5_1" value="<?php echo  $semester->s5_1;?>" required="" min="0" max="5"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="nghe_1" value="<?php echo  $semester->nghe_1;?>" required="" min="0" max="20"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('essay'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('essay'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="viet_luan_1" value="<?php echo  $semester->viet_luan_1;?>" required="" min="0" max="10"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('vocabularyGramar'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"<?php echo get_string('vocabularyGramar'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="tv_np_1" value="<?php echo  $semester->tv_np_1;?>" required="" min="0" max="35"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('reading'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('reading'); ?><span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_hieu_1" value="<?php echo  $semester->doc_hieu_1;?>" required="" min="0" max="10"  <?php echo $disabled; ?>>
								</div>
								<div class="col-md-12" >
									<label class="control-label"><?php echo get_string('note'); ?></label>
									<!-- <textarea name="note" class="form-control" value="<?php echo  $semester->note;?>"><?php echo  $semester->note;?></textarea> -->
									<select name="note" id="" class="form-control" <?php echo $disabled; ?>>
										<option value=""><?php echo get_string('note'); ?></option>
										<option value="1" <?php if(($semester->note)&&($semester->note==1)) echo 'selected'; ?>>Nhận xét điểm hk1 1</option>
										<option value="2" <?php if(($semester->note)&&($semester->note==2)) echo 'selected'; ?>>Nhận xét điểm hk1 2</option>
									</select>
								</div>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;"><?php echo get_string('semester2'); ?></h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('speaking'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness</label>
									<input type="number" class="form-control" name="s1_2" value="<?php echo  $semester->s1_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
									<input type="number" class="form-control" name="s2_2" value="<?php echo  $semester->s2_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
									<input type="number" class="form-control" name="s3_2" value="<?php echo  $semester->s3_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy</label>
									<input type="number" class="form-control" name="s4_2" value="<?php echo  $semester->s4_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5</label>
									<input type="number" class="form-control" name="s5_2" value="<?php echo  $semester->s5_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('listening'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('listening'); ?></label>
									<input type="number" class="form-control" name="nghe_2" value="<?php echo  $semester->nghe_2;?>"  min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('essay'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('essay'); ?></label>
									<input type="number" class="form-control" name="viet_luan_2" value="<?php echo  $semester->viet_luan_2;?>" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('vocabularyGramar'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('vocabularyGramar'); ?></label>
									<input type="number" class="form-control" name="tv_np_2" value="<?php echo  $semester->tv_np_2;?>"  min="0" max="35">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4><?php echo get_string('reading'); ?></h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo get_string('reading'); ?></label>
									<input type="number" class="form-control" name="doc_hieu_2" value="<?php echo  $semester->doc_hieu_2;?>" min="0" max="10">
								</div>
								<?php 
								break;
							}
						}
						?>
						<div class="col-md-12" >
							<label class="control-label"><?php echo get_string('note'); ?></label>
							<!-- <textarea name="note" class="form-control" value="<?php echo  $semester->note_2;?>"><?php echo  $semester->note_2;?></textarea> -->
							<select name="note_2" id="" class="form-control" >
								<option value=""><?php echo get_string('note'); ?></option>
								<option value="1" <?php if(($semester->note_2)&&($semester->note_2==1)) echo 'selected'; ?>>Nhận xét điểm hk1 1</option>
								<option value="2" <?php if(($semester->note_2)&&($semester->note_2==2)) echo 'selected'; ?>>Nhận xét điểm hk1 2</option>
							</select>
						</div>
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<?php
								switch ($semester->school) {
									case 1:
									$urlback=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
									break;
									case 2:
									$urlback= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
									break;
								} 
								if(!empty($idstudent)){
									$urlback= $CFG->wwwroot.'/manage/manage_score/list_semester_student.php?idstudent='.$idstudent;
								}
								if(!empty($course)){
									$urlback=$CFG->wwwroot . "/manage/manage_score/list_semester_student_course.php?courseid=".$course;
								}
								?>
								<a href="<?php echo $urlback ?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
								<!-- <a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a> -->
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var course = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var group = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
				$("#studentid").html(data);
			});
		}
	});
	$('#school_status').on('change', function() {
		var status = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?school_status="+status,function(data){
				$("#return_school_status").html(data);
			});
		}
	});
</script>