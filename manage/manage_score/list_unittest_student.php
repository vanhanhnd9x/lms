<?php
require("../../config.php");
// require("../../grade/lib.php");
global $CFG,$DB,$USER;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_login();
$action = optional_param('action', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);

if(is_student()){
  $idstudent=$USER->id;
}
$student=$DB->get_record('user', array(
  'id' => $idstudent
));
if(empty($idstudent)){
   echo displayJsAlert('Error', $CFG->wwwroot . "/manage/manage_score/index.php");
}
$title=get_string('list_unitest').' '.get_string('student').':'.$student->lastname.' '.$student->firstname.' - '.get_string('classgroup').':'.show_name_course_by_cua($idstudent);
$PAGE->set_title($title);
// $PAGE->set_title(get_string('list_unitest'));
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id); 
if(!is_student()){
  $moodle='student';
  $name1='list_unitest_student';
  $moodle2='unittest';

  $hanhdong=lay_ds_cn_gan($roleid,$moodle2);
  $checkdelete=check_chuc_nang_xoachucnangcong($roleid,$moodle2);

  $check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
  $check_in2=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle2,$name1);
  if(empty($check_in)||!empty($check_in2)){
      echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
  }
}
function show_test_in_unit($test){
  switch ($test) {
    case 1: echo get_string('test_1'); break;
    case 2: echo get_string('test_2'); break;
    case 3: echo get_string('test_3'); break;
    case 4: echo get_string('test_4'); break;
    case 5: echo get_string('test_5'); break;
    case 6: echo get_string('test_6'); break;
    case 7: echo get_string('test_7'); break;
    case 8: echo get_string('test_8'); break;
    case 9: echo get_string('test_9'); break;
    case 10: echo get_string('test_10'); break;
    case 11: echo get_string('test_11'); break;
    case 12: echo get_string('test_12'); break;
    case 13: echo get_string('test_13'); break;
    case 14: echo get_string('test_14'); break;
    case 15: echo get_string('test_15'); break;
  }
}
$number=20;
$page=isset($_GET['page'])?$_GET['page']:1;
  $schoolid = trim(optional_param('schoolid', '', PARAM_TEXT));
  $block_student = trim(optional_param('block_student', '', PARAM_TEXT));
  $groupid = trim(optional_param('groupid', '', PARAM_TEXT));
  $courseid = trim(optional_param('courseid', '', PARAM_TEXT));
  $name = trim(optional_param('name', '', PARAM_TEXT));
  $code = trim(optional_param('code', '', PARAM_TEXT));
  $month = trim(optional_param('month', '', PARAM_TEXT));
  $school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
  $data=search_unittest($schoolid,$block_student,$groupid,$courseid,$month,$school_yearid,$name,$code,$page,$number,$idstudent);
if($action=='search'){
  $url=$CFG->wwwroot.'/manage/manage_score/list_unittest_student.php?action=search&idstudent='.$idstudent.'&month='.$month.'&school_yearid='.$school_yearid;
}else{
  $url=$CFG->wwwroot.'/manage/manage_score/list_unittest_student.php?idstudent='.$idstudent.'&page=';
}



?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <!-- <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/manage_score/add_unittest.php?idstudent='.$idstudent); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a> 
          </div> -->
          <div class="col-md-12">
            
           <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" value="search" hidden="">
             <input type="" name="idstudent" class="form-control"  value="<?php echo $idstudent; ?>" hidden="">
          <div class="col-4">
            <label for=""><?php echo get_string('test'); ?></label>
            <select name="month" id="month" class="form-control">
                <option value="" ><?php echo get_string('test') ?></option>
                <option value="1" ><?php echo get_string('test_1') ?></option>
                <option value="2" ><?php echo get_string('test_2') ?></option>
                <option value="3" ><?php echo get_string('test_3') ?></option>
                <option value="4" ><?php echo get_string('test_4') ?></option>
                <option value="5" ><?php echo get_string('test_5') ?></option>
                <option value="6" ><?php echo get_string('test_6') ?></option>
                <option value="7" ><?php echo get_string('test_7') ?></option>
                <option value="8" ><?php echo get_string('test_8') ?></option>
                <option value="9" ><?php echo get_string('test_9') ?></option>
                <option value="10" ><?php echo get_string('test_10') ?></option>
                <option value="11" ><?php echo get_string('test_11') ?></option>
                <option value="12" ><?php echo get_string('test_12') ?></option>
                <option value="13" ><?php echo get_string('test_13') ?></option>
                <option value="14" ><?php echo get_string('test_14') ?></option>
                <option value="15" ><?php echo get_string('test_15') ?></option>
                
            </select>
         </div>
       <div class="col-4">
        <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
        <select name="school_yearid" id="" class="form-control">
         <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
         <?php 
         $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
         $school_year = $DB->get_records_sql($sql);
         foreach ($school_year as $key => $value) {
           # code...
          ?>
          <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
          <?php 
         }
         ?>
       </select>
     </div>
     <div class="col-4">
      <div style="    margin-top: 29px;"></div>
      <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
    </div>
  </form>
</div>
</div>

<div class="table-responsive" data-pattern="priority-columns" id="searchResults">
  <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
    <table id="tech-companies-1" class="table table-hover">
      <thead>
        <tr>
          <th>STT</th>
          
          
          <th><?php echo get_string('test'); ?></th>
          <th><?php echo get_string('school_year'); ?></th>
          <th><?php echo get_string('score'); ?></th>
          <th><?php echo get_string('note'); ?></th>
        </tr>
      </thead>
      <tbody id="ajaxschools">
        <?php 
        $i=0;
        if($data['data']){
          foreach ($data['data'] as $unit) { 
            $i++;
            ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php show_test_in_unit($unit->month);?></td>
              <td><?php echo show_show_yeah_in_unit($unit->school_yearid); ?></td>
              <td><?php echo $unit->score_1,'/',$unit->note_2; ?></td>
              <td><?php echo $unit->note_1; ?></td>
              <?php 
                if(!is_student()){
                  
                  
                  ?>
                  <!-- <td class="text-right">
                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editunit.php?idedit=<?php echo $unit->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewunit.php?idview=<?php echo $unit->id; ?>" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="javascript:void(0)" onclick="delet_unittest(<?php echo $unit->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                  </td> -->
                  <?php 
                }
              ?>
             
            </tr>
          <?php }
        }
        ?>
      </tbody>
    </table>
  </form>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>
</div>
</div>
</div>
</div>
<?php

echo $OUTPUT->footer();
?>
<script>
  function delet_unittest(iddelete){
    var studentid="<?php echo $idstudent; ?>"
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/list_unittest_student.php?idstudent="+studentid;
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('<?php echo get_string('del_unitest'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete:iddelete} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>
