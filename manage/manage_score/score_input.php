<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_login(0, false);
$PAGE->set_title('Điểm đầu vào ');
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);



?>


<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <div class="col-md-2">
                        <a href="<?php echo new moodle_url('/manage/manage_score/add_score_input.php'); ?>" class="btn btn-info">Thêm mới</a> 
                    </div>
                    <div class="col-md-10">
          </div>
      </div>

      <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
        <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
            <table id="tech-companies-1" class="table table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên học sinh</th>
                        <th>Tên trường</th>
                        <th>Tên lớp</th>
                        <th>Năm học</th>
                        <th>Điểm đầu vào</th>
                        <th class="disabled-sorting text-right">Hành động</th>
                    </tr>
                </thead>
                <tbody id="ajaxschools">
                    <?php 
                    $i=0;
                    if($data['data']){
                        foreach ($data['data'] as $unit) { 
                            $i++;
                            ?>
                            <tr>
                                
                                <td class="text-right">
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editunit.php?idedit=<?php echo $unit->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewunit.php?idview=<?php echo $unit->id; ?>" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="javascript:void(0)" onclick="delet_unittest(<?php echo $unit->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php }
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </div>
</div>
</div>
</div>
</div>
<?php

echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script>
    function delet_unittest(iddelete){
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
        var e=confirm('Bạn có muốn xóa điểm unittest này không?');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {iddelete:iddelete} ,
                success: function (response) {
                   window.location=urlweb;
               }
           });
        }
    }
</script>
