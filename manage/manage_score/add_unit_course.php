<?php 
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php'); 
require_once($CFG->dirroot . '/manage/block-student/lib.php'); 
require_once ($CFG->dirroot. '/manage/notification/lib.php');

$PAGE->set_title(get_string('add_unitest'));
$PAGE->set_heading(get_string('add_unitest'));
$PAGE->set_pagelayout(get_string('add_unitest'));
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idcourse=optional_param('idcourse', '', PARAM_TEXT);
$month=optional_param('month', '', PARAM_TEXT);
$user_id=optional_param('user_id', '', PARAM_TEXT);
$score=optional_param('score', '', PARAM_TEXT);
$note=optional_param('note', '', PARAM_TEXT);
$id_unittest=optional_param('id_unittest', '', PARAM_TEXT);
$thangdiem=optional_param('thangdiem', '', PARAM_TEXT);
function lay_lop_cua_nhom_lop($idcourse){
	global $DB;
	$sql="SELECT `group_course`.`group_id`
		FROM `group_course`
		WHERE `group_course`.`course_id`={$idcourse}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  	return $data;
}
$groupid=lay_lop_cua_nhom_lop($idcourse);
if(empty($groupid)){

}
$group=$DB->get_record('groups', array(
	'id' => $groupid
));
$school_year=$DB->get_record('school_year', array(
	'id' => $group->id_namhoc
));
$course_info=$DB->get_record('course', array(
	'id' => $idcourse
));
$school=$DB->get_record('schools', array(
	'id' => $group->id_truong
));
$list_student=laydanhsachhocsinhcuahomlop_danhgiahocsinh($idcourse);
if($action=='save_unitest_group'){
	foreach ($user_id as $key => $value) {
		if(!empty($id_unittest[$key])){
			// update_unittest_by_cua($id_unittest[$key],$value,$idcourse,$groupid,$group->id_namhoc,$month,$score[$key],$note[$key]);
			update_unittest_by_cua2($id_unittest[$key],$value,$idcourse,$groupid,$group->id_namhoc,$month,$score[$key],$note[$key],$thangdiem);
		}else{
			// save_unittest2($value,$idcourse,$groupid,$group->id_namhoc,$month,$score[$key],$note[$key]);
			// save_unittest2($value,$idcourse,$groupid,$group->id_namhoc,$month,$score[$key],$note[$key]);
			save_unittest3($value,$idcourse,$groupid,$group->id_namhoc,$month,$score[$key],$note[$key],$thangdiem);

			// them vao thong bao
			add_notifications_for_student($value,'BÀI KIỂM TRA NHỎ','Thông báo về điểm kiểm tra nhỏ của học sinh','/manage/manage_score/list_unittest_student.php');
		}
	}
	echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/manage_score/add_unit_course.php?idcourse=".$idcourse);
	
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<input type="text" hidden="" name="action" value="save_unitest_group">
					<div class="row">
						<div class="col-md-4">
							<label class="control-label"><?php echo get_string('schools'); ?></label>
							<input type="text" class="form-control" disabled value="<?php echo $school->name; ?>">
						</div>
						<div class="col-md-4">
							<label class="control-label"><?php echo get_string('class'); ?></label>
							<input type="text" class="form-control" disabled value="<?php echo $group->name; ?>">
						</div>
						<div class="col-md-4">
							<label class="control-label"><?php echo get_string('classgroup'); ?></label>
							<input type="text" class="form-control" disabled value="<?php echo $course_info->fullname; ?>">
						</div>
						<div class="col-md-4"> 
							<label class="control-label"><?php echo get_string('test'); ?><span style="color:red">*</span></label>
							<select name="month" id="month" class="form-control">
		 						<option value="1" ><?php echo get_string('test_1') ?></option>
		 						<option value="2" ><?php echo get_string('test_2') ?></option>
		 						<option value="3" ><?php echo get_string('test_3') ?></option>
		 						<option value="4" ><?php echo get_string('test_4') ?></option>
		 						<option value="5" ><?php echo get_string('test_5') ?></option>
		 						<option value="6" ><?php echo get_string('test_6') ?></option>
		 						<option value="7" ><?php echo get_string('test_7') ?></option>
		 						<option value="8" ><?php echo get_string('test_8') ?></option>
		 						<option value="9" ><?php echo get_string('test_9') ?></option>
		 						<option value="10" ><?php echo get_string('test_10') ?></option>
		 						<option value="11" ><?php echo get_string('test_11') ?></option>
		 						<option value="12" ><?php echo get_string('test_12') ?></option>
		 						<option value="13" ><?php echo get_string('test_13') ?></option>
		 						<option value="14" ><?php echo get_string('test_14') ?></option>
		 						<option value="15" ><?php echo get_string('test_15') ?></option>
		 						
		 					</select>
						</div>
						<div class="col-md-4" > 
							<label class="control-label"><?php echo get_string('point_ladder'); ?><span style="color:red">*</span></label>
							<div id="thangdiem"><input type="text" class="form-control"  value="" id="" name="thangdiem" min="0" required="" pattern="[0-9]{1,10}" title="Vui lòng nhập số" ></div>
							
						</div>
						<div class="col-md-4"> 
							<label class="control-label"><?php echo get_string('school_year'); ?></label>
							<input type="text" class="form-control" disabled value="<?php echo $school_year->sy_start,'-', $school_year->sy_end; ?>">
						</div>
						<div class="" ></div>
						<div class="col-md-12"  id="load_unitest_cua" style="margin-top: 15px">
							<div class="table-responsive">
			                    <table class="table table-striped table-bordered">
			                        <thead>
			                            <tr>
			                                <th>STT</th>
			                                <th><?php echo get_string('student'); ?></th>
			                                <th><?php echo get_string('score'); ?></th>
			                                <th><?php echo get_string('note'); ?></th>
			                            </tr>
			                        </thead>
			                        <?php 
			                        	// $hs=laydanhsachhocsinhcuahomlop_danhgiahocsinh($idcourse);
			                        	if($list_student){
			                        		$i=0;
			                        		foreach ($list_student as  $h) {
			                        			$i++;
			                        			?>
			                        			 <tr>
						                        	<td>1</td>
						                        	<td><?php echo  $h->lastname,' ',$h->firstname ?></td>
						                        	<td>
						                        		<input type="text" min="0" class="form-control" name="score[]" pattern="[0-9]{1,10}" title="Vui lòng nhập số" >
						                        		<input type="text" class="" hidden="" name="user_id[]" value="<?php echo $h->id; ?>">
						                        		<input type="text" class="" hidden="" name="id_unittest[]" value="">
						                        	</td>
						                        	<td>
						                        		<input type="text"  class="form-control" name="note[]">
						                        	</td>
						                        </tr>
			                        			<?php 
			                        			# code...
			                        		}
			                        	}
			                         ?>
			                       
			                    </table>
			                </div>
						</div>
						<div class="col-md-12">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();
?>
<script>
var idcourse = "<?php echo $idcourse; ?>";
var groupid = "<?php echo $groupid; ?>";
var school_year = "<?php echo $school_year->id; ?>";
var month1 = $('#month').find(":selected").val();
    if(month1>0){
      	$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?month=" + month1+"&idcourse="+idcourse+"&groupid="+groupid+"&school_year="+school_year, function(data) {
            $("#load_unitest_cua").html(data);
       	});
    }
$('#month').on('change', function() {
    var month = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?month=" + month+"&idcourse="+idcourse+"&groupid="+groupid+"&school_year="+school_year, function(data) {
            $("#load_unitest_cua").html(data);
        });
    }
});
</script>