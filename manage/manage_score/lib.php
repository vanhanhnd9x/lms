<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;

/* 
	cần phải có
	function lưu lại, chỉnh sửa thông tin của một điểm thành phần
	cụ thể: userid,couresid,groupid,school_yearid,month,time_cre
	function lưu lại, cập nhật điểm số và ghi chú điểm cụ thể là
	note_1-
	score_1
*/
	function danhsachtruongtieuhoc(){
		global $DB;
		$sql="SELECT id, name,district FROM schools WHERE del =0 AND level =1";
		$data = $DB->get_records_sql($sql); 
		return $data;
	}
	function danhsachtruongtrunghoc(){
		global $DB;
		$sql="SELECT id, name,district FROM schools WHERE del =0 AND level IN(2,3)";
		$data = $DB->get_records_sql($sql); 
		return $data;
	}
	function search_unittest($schoolid=null,$block_student=null,$groupid=null,$courseid=null,$month=null,$school_yearid=null,$name=null,$code=null,$page,$number,$userid=null){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$sql="SELECT DISTINCT  `unittest`.`id`, `unittest`.`userid`, `unittest`.`courseid`, `unittest`.`groupid`,`unittest`.`school_yearid`,`unittest`.`month`,`unittest`.`score_1`,`unittest`.`note_1`,`unittest`.`note_2`, `groups`.`name` as group_name , `schools`.`name` as school_name , concat_ws(' ',`user`.`lastname`,`user`.`firstname`) as studentname ,`user`.`code`,concat_ws('-',`school_year`.`sy_start`,`school_year`.`sy_end`) as namhoc
		FROM `unittest`
		JOIN `user` ON `user`.`id` = `unittest`.`userid`
		JOIN `groups` ON `groups`.`id` = `unittest`.`groupid`
		JOIN `schools` ON `schools`.`id` =`groups`.`id_truong`
		JOIN `school_year` ON `school_year`.`id` =`unittest`.`school_yearid`
		WHERE `unittest`.`del`=0 AND `user`.`del` =0 AND `schools`.`del` =0 ";
	$sql_total="SELECT  COUNT(DISTINCT `unittest`.`id`) 
		FROM `unittest`
		JOIN `user` ON `user`.`id` = `unittest`.`userid`
		JOIN `groups` ON `groups`.`id` = `unittest`.`groupid`
		JOIN `schools` ON `schools`.`id` =`groups`.`id_truong`
		JOIN `school_year` ON `school_year`.`id` =`unittest`.`school_yearid`
		WHERE `unittest`.`del`=0 AND `user`.`del` =0 AND `schools`.`del` =0 ";
	if(!empty($schoolid)){
		$sql.=" AND `groups`.`id_truong`={$schoolid}";
		$sql_total.=" AND `groups`.`id_truong`={$schoolid}";
	}
	if(!empty($block_student)){
		$sql.=" AND `groups`.`id_khoi`={$block_student}";
		$sql_total.=" AND `groups`.`id_khoi`={$block_student}";
	}
	if(!empty($groupid)){
		$sql.=" AND `groups`.`id`={$groupid}";
		$sql_total.=" AND `groups`.`id`={$groupid}";
	}
	if(!empty($courseid)){
		$sql.=" AND `unittest`.`courseid`={$courseid}";
		$sql_total.=" AND `unittest`.`courseid`={$courseid}";
	}
	if(!empty($month)){
		$sql.=" AND `unittest`.`month`={$month}";
		$sql_total.=" AND `unittest`.`month`={$month}";
	}
	if(!empty($school_yearid)){
		$sql.=" AND `unittest`.`school_yearid`={$school_yearid}";
		$sql_total.=" AND `unittest`.`school_yearid`={$school_yearid}";
	}
	if(!empty($name)){
		$sql.=" AND concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$name}%'";
		$sql_total.=" AND concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$name}%'";
	}
	if(!empty($code)){
		$sql.=" AND `user`.`code` LIKE '%{$code}%'";
		$sql_total.=" AND `user`.`code` LIKE '%{$code}%'";
	}
	if(!empty($userid)){
		$sql.=" AND `unittest`.`userid` = {$userid}";
		$sql_total.=" AND `unittest`.`userid` = {$userid}";
	}
	$sql.=" ORDER BY `unittest`.`id` DESC  LIMIT {$start}, {$number}";
	$data=$DB->get_records_sql($sql);
	$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
	$total_page=ceil((int)$total_row / (int)$number);
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page
	);
	return $kq;
}
function get_all_unittest_admin($page=null,$number=null){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$sql="SELECT `unittest`.`id`, `unittest`.`userid`, `unittest`.`courseid`, `unittest`.`groupid`,`unittest`.`school_yearid`,`unittest`.`month`,`unittest`.`score_1` FROM `unittest` WHERE `unittest`.`del`=0";
	$data=$DB->get_records_sql($sql);
	$sql_total="SELECT COUNT( DISTINCT `unittest`.`id`) FROM `unittest` WHERE `unittest`.`del`=0  ";
	$total_row=$DB->get_records_sql($sql_total);

	foreach ($total_row as $key => $value) {
        # code...
		$total_page=ceil((int)$key / (int)$number);
		break;
	}
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page
	);
	return $kq;
}
function save_unittest($userid,$courseid,$groupid,$school_yearid,$month,$number){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->userid =$userid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->time_cre = time();
	$record->time_update = 0;
	$record->number = $number;
	$record->del = 0;
	$id_unittest = $DB->insert_record('unittest', $record);
	return $id_unittest;
}
function save_unittest2($userid,$courseid,$groupid,$school_yearid,$month,$score_1,$note_1){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->userid =$userid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->score_1 =$score_1;
	$record->note_1 =$note_1;
	$record->time_cre = time();
	$record->time_update = 0;
	$record->del = 0;
	$id_unittest = $DB->insert_record('unittest', $record);
	return $id_unittest;
}
function save_unittest3($userid,$courseid,$groupid,$school_yearid,$month,$score_1,$note_1,$thangdiem){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->userid =$userid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->score_1 =$score_1;
	$record->note_1 =$note_1;
	$record->note_2 =$thangdiem;
	$record->time_cre = time();
	$record->time_update = 0;
	$record->del = 0;
	$id_unittest = $DB->insert_record('unittest', $record);
	return $id_unittest;
}
function update_unittest_by_cua($unittestid,$userid,$courseid,$groupid,$school_yearid,$month,$score_1,$note_1){
	global $DB;
	$record = new stdClass();
	$record->id = $unittestid;
	$record->userid =$userid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->score_1 =$score_1;
	$record->note_1 =$note_1;
	$record->time_update =time();
	return $DB->update_record('unittest', $record, false);
}
function update_unittest_by_cua2($unittestid,$userid,$courseid,$groupid,$school_yearid,$month,$score_1,$note_1,$thangdiem){
	global $DB;
	$record = new stdClass();
	$record->id = $unittestid;
	$record->userid =$userid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->score_1 =$score_1;
	$record->note_1 =$note_1;
	$record->note_2 =$thangdiem;
	$record->time_update =time();
	return $DB->update_record('unittest', $record, false);
}
function update_school_year_month($unittestid,$school_yearid,$month){
	global $DB;
	$record = new stdClass();
	$record->id = $unittestid;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->time_update =time();
	return $DB->update_record('unittest', $record, false);
}
function save_note_score($unittestid,$note,$score){
	global $DB;
	$record = new stdClass();
	$record->id = $unittestid;
	$record->note_1 = $note;
	$record->score_1 = $score;

	return $DB->update_record('unittest', $record, false);
}
function delete_unittest($id){
		// global $DB;
		// $DB->delete_records_select('unittest', " id = $id ");
		// return 1;
	global $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->del = 1;
	return $DB->update_record('unittest', $record, false);
}
function show_info_student_in_unit($userid,$courseid,$groupid){
	global $CFG, $DB;
    // lấy tên học sinh
	$student=$DB->get_record('user', array(
		'id' => $userid
	));
    // lấy thông tin khóa học<nhom lop>
	$course=$DB->get_record('course', array(
		'id' => $courseid
	));
		 // lay thong tin lop
	$group =$DB->get_record('groups', array(
		'id' => $groupid
	)); 
    // thông tin khôi
	$block_student=$DB->get_record('block_student', array(
		'id' => $group->id_khoi
	));
    // thong tin  truong
	$school=$DB->get_record('schools', array(
		'id' => $block_student->schoolid
	)); 
	
	$yeah=$school_year->sy_start.' - '.$school_year->sy_end;
	$username= $student->lastname.' '.$student->firstname;
	?>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $school->name;?>" class="form-control" disabled>
	</div>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $block_student->name;?>" class="form-control" disabled>
	</div>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('class'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $group->name;?>" class="form-control" disabled>
	</div>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
		<input type="text" value="<?php echo $course->fullname;?>" class="form-control" disabled>
	</div>
	
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('student'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $username;?>" class="form-control" disabled>
	</div>
	<?php 

}
function save_semester($schoolid,$block_studentid,$courseid,$groupid,$userid,$school_yearid,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->schoolid =$schoolid;
	$record->block_studentid =$block_studentid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->userid =$userid;
	$record->school_yearid =$school_yearid;
	$record->school =$school;
		// tính điểm học kì 1
	$record->s1_1 =$s1_1;
	$record->s2_1 =$s2_1;
	$record->s3_1 =$s3_1;
	$record->s4_1 =$s4_1;
	$record->s5_1 =$s5_1;
	$record->noi_1 =$noi_1;
	$record->nghe_1 =$nghe_1;
	$record->doc_viet_1 =$doc_viet_1;
	$record->viet_luan_1 =$viet_luan_1;
	$record->tv_np_1 =$tv_np_1;
	$record->doc_hieu_1 =$doc_hieu_1;
	$record->tvnp_dochieu_viet_1 =$tvnp_dochieu_viet_1;
	$record->tonghocki_1 =$tonghocki_1;
		// tính tổng học kì 2
	$record->s1_2 =$s1_2;
	$record->s2_2 =$s2_2;
	$record->s3_2 =$s3_2;
	$record->s4_2 =$s4_2;
	$record->s5_2 =$s5_2;
	$record->noi_2 =$noi_2;
	$record->nghe_2 =$nghe_2;
	$record->doc_viet_2 =$doc_viet_2;
	$record->viet_luan_2 =$viet_luan_2;
	$record->tv_np_2 =$tv_np_2;
	$record->doc_hieu_2 =$doc_hieu_2;
	$record->tvnp_dochieu_viet_2 =$tvnp_dochieu_viet_2;
	$record->tonghocki_2 =$tonghocki_2;


	$record->note =$note;

	$record->tongket =0.25*$tonghocki_1+0.75*$tonghocki_2;

	$record->time_cre = time();
	$record->del = 0;
	$id_new = $DB->insert_record('semester', $record);
	return $id_new;
}
function save_semester2($schoolid,$block_studentid,$courseid,$groupid,$userid,$school_yearid,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->schoolid =$schoolid;
	$record->block_studentid =$block_studentid;
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->userid =$userid;
	$record->school_yearid =$school_yearid;
	$record->school =$school;
		// tính điểm học kì 1
	$record->s1_1 =$s1_1;
	$record->s2_1 =$s2_1;
	$record->s3_1 =$s3_1;
	$record->s4_1 =$s4_1;
	$record->s5_1 =$s5_1;
	$record->noi_1 =$noi_1;
	$record->nghe_1 =$nghe_1;
	$record->doc_viet_1 =$doc_viet_1;
	$record->viet_luan_1 =$viet_luan_1;
	$record->tv_np_1 =$tv_np_1;
	$record->doc_hieu_1 =$doc_hieu_1;
	$record->tvnp_dochieu_viet_1 =$tvnp_dochieu_viet_1;
	$record->tonghocki_1 =$tonghocki_1;
		// tính tổng học kì 2
	$record->s1_2 =$s1_2;
	$record->s2_2 =$s2_2;
	$record->s3_2 =$s3_2;
	$record->s4_2 =$s4_2;
	$record->s5_2 =$s5_2;
	$record->noi_2 =$noi_2;
	$record->nghe_2 =$nghe_2;
	$record->doc_viet_2 =$doc_viet_2;
	$record->viet_luan_2 =$viet_luan_2;
	$record->tv_np_2 =$tv_np_2;
	$record->doc_hieu_2 =$doc_hieu_2;
	$record->tvnp_dochieu_viet_2 =$tvnp_dochieu_viet_2;
	$record->tonghocki_2 =$tonghocki_2;


	$record->note =$note;
	$record->note_2 =$note_2;

	$record->tongket =0.25*$tonghocki_1+0.75*$tonghocki_2;

	$record->time_cre = time();
	$record->del = 0;
	$id_new = $DB->insert_record('semester', $record);
	return $id_new;
}
	//update điểm tổng kết năm
function update_semester($idsemester,$school_yearid,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2,$note,$note_2){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->id = $idsemester;
	$record->school_yearid =$school_yearid;
		// tính điểm học kì 1
	$record->s1_1 =$s1_1;
	$record->s2_1 =$s2_1;
	$record->s3_1 =$s3_1;
	$record->s4_1 =$s4_1;
	$record->s5_1 =$s5_1;
	$record->noi_1 =$noi_1;
	$record->nghe_1 =$nghe_1;
	$record->doc_viet_1 =$doc_viet_1;
	$record->viet_luan_1 =$viet_luan_1;
	$record->tv_np_1 =$tv_np_1;
	$record->doc_hieu_1 =$doc_hieu_1;
	$record->tvnp_dochieu_viet_1 =$tvnp_dochieu_viet_1;
	$record->tonghocki_1 =$tonghocki_1;
		// tính tổng học kì 2
	$record->s1_2 =$s1_2;
	$record->s2_2 =$s2_2;
	$record->s3_2 =$s3_2;
	$record->s4_2 =$s4_2;
	$record->s5_2 =$s5_2;
	$record->noi_2 =$noi_2;
	$record->nghe_2 =$nghe_2;
	$record->doc_viet_2 =$doc_viet_2;
	$record->viet_luan_2 =$viet_luan_2;
	$record->tv_np_2 =$tv_np_2;
	$record->doc_hieu_2 =$doc_hieu_2;
	$record->tvnp_dochieu_viet_2 =$tvnp_dochieu_viet_2;
	$record->tonghocki_2 =$tonghocki_2;
	
	$record->note =$note;
	$record->note_2 =$note_2;

	$record->time_edit=time();

	$record->tongket =0.25*$tonghocki_1+0.75*$tonghocki_2;
	
	return $DB->update_record('semester', $record, false);
}

function delete_semester($id){
	global $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->del = 1;
	return $DB->update_record('semester', $record, false);
}

function get_info_student_in_unit($userid,$courseid,$groupid,$school_yearid){
	global $CFG, $DB;
    // lấy tên học sinh
	$student=$DB->get_record('user', array(
		'id' => $userid
	));
    // lấy thông tin khóa học<nhom lop>
	$course=$DB->get_record('course', array(
		'id' => $courseid
	));
		// lay thong tin lop
	$group =$DB->get_record('groups', array(
		'id' => $groupid
	));
    // thông tin khôi
	$block_student=$DB->get_record('block_student', array(
		'id' => $group->id_khoi
	));
    // thong tin  truong
	$school=$DB->get_record('schools', array(
		'id' => $block_student->schoolid
	)); 
	
    // thong tin nam hoc
	$school_year=$DB->get_record('school_year', array(
		'id' => $school_yearid
	));
	$yeah=$school_year->sy_start.' - '.$school_year->sy_end;
	$username= $student->lastname.' '.$student->firstname;
	echo'
	<td>'.$username.'</td>
	<td>'.$school->name.'</td>
	<td>'.$group->name.'</td>
	<td>'.$course->fullname.'</td>
	<td>'.$yeah.'</td>
	';
}
function check_diem_hoc_ki($userid,$school_yearid){
	global $CFG, $DB;
	$sql = "SELECT `semester`.`id`
	FROM `semester` 
	WHERE `semester`.`userid`={$userid}
	AND `semester`.`school_yearid`={$school_yearid}
	AND `semester`.`del`=0
	";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_all_semester_tieuhoc($page=null,$number=null){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$sql="
	SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname`
	FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE `semester`.`school`=1 AND `user`.`del`=0 AND `semester`.`del`=0 LIMIT {$start} , {$number}";
	$sql_total="SELECT COUNT( DISTINCT `semester`.`id`)FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE `semester`.`school`=1 AND `user`.`del`=0 AND `semester`.`del`=0";
	$data=$DB->get_records_sql($sql);
	// $total_row=$DB->get_records_sql($sql_total);

	// foreach ($total_row as $key => $value) {
 //        # code...
	// 	$total_page=ceil((int)$key / (int)$number);
	// 	break;
	// }
	$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
	$total_page=ceil((int)$total_row / (int)$number);
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page
	);
	return $kq;
}
function search_semester($schoolid=null,$block_student=null,$groupid=null,$courseid=null,$name=null,$school_year=null,$school_status,$page,$number,$userid=null){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$sql="
	SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname` 
	FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND `semester`.`del`=0 
	
	";
	$sql_total="
	SELECT COUNT( DISTINCT `semester`.`id`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND `semester`.`del`=0
	";
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid`={$schoolid}";
		$sql_total.=" AND `semester`.`schoolid`={$schoolid}";
	}
	if(!empty($block_student)){
		$sql.=" AND `semester`.`block_studentid`={$block_student}";
		$sql_total.=" AND `semester`.`block_studentid`={$block_student}";
	}
	if(!empty($groupid)){
		$sql.=" AND `semester`.`groupid`={$groupid}";
		$sql_total.=" AND `semester`.`groupid`={$groupid}";
	}
	if(!empty($courseid)){
		$sql.=" AND `semester`.`courseid`={$courseid}";
		$sql_total.=" AND `semester`.`courseid`={$courseid}";
	}
	if(!empty($name)){
		$sql.=" AND `semester`.`userid` IN (SELECT `user`.`id` FROM `user` WHERE `user`.`del`=0 AND( `user`.`firstname` LIKE '%{$name}%' OR `user`.`lastname` LIKE '%{$name}%' ))";
		$sql_total.=" AND `semester`.`userid` IN (SELECT `user`.`id` FROM `user` WHERE `user`.`del`=0 AND( `user`.`firstname` LIKE '%{$name}%' OR `user`.`lastname` LIKE '%{$name}%' ))";
	}
	if(!empty($school_year)){
		$sql.=" AND `semester`.`school_yearid`={$school_year}";
		$sql_total.=" AND `semester`.`school_yearid`={$school_year}";
	}
	if(!empty($school_status)){
		$sql.=" AND `semester`.`school`={$school_status}";
		$sql_total.=" AND `semester`.`school`={$school_status}";
	}
	if(!empty($userid)){
		$sql.=" AND `semester`.`userid`={$userid}";
		$sql_total.=" AND `semester`.`userid`={$userid}";
		// $sql.=" AND `user`.`id`={$userid}";
		// $sql_total.=" AND `user`.`id`={$userid}";
	}
	$sql.=" LIMIT {$start} , {$number}";
	$data=$DB->get_records_sql($sql);

	// $total_row=$DB->get_records_sql($sql_total);

	// foreach ($total_row as $key => $value) {
 //        # code...
	// 	$total_page=ceil((int)$key / (int)$number);
	// 	break;
	// }
	$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
	$total_page=ceil((int)$total_row / (int)$number);
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page
	);
	return $kq;

}
function get_all_semester_trunghoc($page=null,$number=null){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$sql="SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname` 
	FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE `semester`.`school`=2 AND `user`.`del`=0 AND `semester`.`del`=0 LIMIT {$start} , {$number}";
	$sql_total="SELECT COUNT( DISTINCT `semester`.`id`)  FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE `semester`.`school`=2 AND `user`.`del`=0 AND `semester`.`del`=0  ";
	$data=$DB->get_records_sql($sql);
	// $total_row=$DB->get_records_sql($sql_total);
	$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
	$total_page=ceil((int)$total_row / (int)$number);
	// foreach ($total_row as $key => $value) {
 //        # code...
	// 	$total_page=ceil((int)$key / (int)$number);
	// 	break;
	// }
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page
	);
	return $kq;
}
function get_all_school_year_in_semester(){
	global $DB, $USER;
	$sql="SELECT * FROM `school_year` ";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function show_month_in_unit($month){
  switch ($month) {
    case 1: echo get_string('january'); break;
    case 2: echo get_string('february'); break;
    case 3: echo get_string('march'); break;
    case 4: echo get_string('april'); break;
    case 5: echo get_string('may'); break;
    case 6: echo get_string('june'); break;
    case 7: echo get_string('july'); break;
    case 8: echo get_string('august'); break;
    case 9: echo get_string('september'); break;
    case 10: echo get_string('october'); break;
    case 11: echo get_string('november'); break;
    case 12: echo get_string('december'); break;
  }
}
// kiem tra diem unit tet
function check_unit_test_user($userid,$school_yearid,$month){
	global $CFG, $DB;
	$sql = "SELECT `unittest`.`id` FROM `unittest` 
	WHERE `unittest`.`userid`={$userid} 
	AND `unittest`.`school_yearid`= {$school_yearid} 
	AND `unittest`.`month`= {$month}
	AND `unittest`.`del`=0 ";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function hien_thi_danh_sach_hs_nhom($idnhom){
	global $DB, $USER;
	$sql="
	SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`role_assignments`.`roleid` 
	FROM `user` 
	JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
	JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
	WHERE `course`.`id`= {$idnhom} 
	AND `role_assignments`.`roleid`=5 
	AND `user`.`del`=0
     ";
    $data=$DB->get_records_sql($sql);
	return $data;
}
function show_show_yeah_in_unit($id){
  global $DB;
  $yeah=$DB->get_record('school_year',array('id'=>$id));
  echo $yeah->sy_start.'-'.$yeah->sy_end;
}
function hien_thi_nam_hoc_tai_semeter(){
	global $DB;
	$sql="SELECT * FROM `school_year`";
	$data = $DB->get_records_sql($sql);
	return $data;
}


function lay_diem_hoc_ky_theo_hoc_sinh_cua($school_year=null,$school_status,$page,$number,$userid=null){
  global $DB, $USER;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="
  SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname` 
  FROM `semester`
  JOIN `user` ON `semester`.`userid`= `user`.`id` 
  JOIN `course` ON `course`.`id` = `semester`.`courseid`
  WHERE  `user`.`del`=0
  AND `semester`.`del`=0 
  ";
  $sql_total="
  SELECT COUNT( DISTINCT `semester`.`id`) FROM `semester` 
  JOIN `user` ON `semester`.`userid`= `user`.`id` 
  JOIN `course` ON `course`.`id` = `semester`.`courseid`
  WHERE  `user`.`del`=0
  AND `semester`.`del`=0
  ";
 
  if(!empty($school_year)){
    $sql.=" AND `semester`.`school_yearid`={$school_year}";
    $sql_total.=" AND `semester`.`school_yearid`={$school_year}";
  }
  if(!empty($school_status)){
    $sql.=" AND `semester`.`school`={$school_status}";
    $sql_total.=" AND `semester`.`school`={$school_status}";
  }
  if(!empty($userid)){
    $sql.=" AND `semester`.`userid`={$userid}";
    $sql_total.=" AND `semester`.`userid`={$userid}";
    // $sql.=" AND `user`.`id`={$userid}";
    // $sql_total.=" AND `user`.`id`={$userid}";
  }
  $sql.=" LIMIT {$start} , {$number}";
  $data=$DB->get_records_sql($sql);

  $total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);;
  $total_page=ceil((int)$total_row / (int)$number);
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page
  );
  return $kq;

}
function lay_level_truong_hoc_theo_hs($idstudent){
  global $DB;
  $sql="
  SELECT `schools`.`level` FROM `groups`
  JOIN `groups_members` ON `groups`.`id` =`groups_members`.`groupid` 
  JOIN `schools` ON `schools`.`id`= `groups`.`id_truong` 
  WHERE `groups_members`.`userid`={$idstudent}";
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
// lay id nam hoc hien tai cua nhom 

function layidnamhoccuanhomlophientai($courseid){
  global $DB;
  $sql="
    SELECT DISTINCT `groups`.`id_namhoc` FROM `groups` JOIN `group_course` ON `group_course`.`group_id`=`groups`.`id` JOIN `course` ON `course`.`id` =`group_course`.`course_id` WHERE `course`.`id` ={$courseid}
  ";
  $data=$DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function layidlopcuanhom_manage_score($courseid){
  global $DB;
  $sql="
    SELECT DISTINCT `groups`.`id` FROM `groups` JOIN `group_course` ON `group_course`.`group_id`=`groups`.`id` JOIN `course` ON `course`.`id` =`group_course`.`course_id` WHERE `course`.`id` ={$courseid}
  ";
  $data=$DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function hienthitennhomloptheotungnamhoc($courseid,$groupid,$school_yearid){
  global $DB;
   $sql="
    SELECT DISTINCT `history_course_name`.`course_name` FROM `history_course_name`
    WHERE `history_course_name`.`courseid`={$courseid}
    AND `history_course_name`.`groupid`={$groupid}
    AND `history_course_name`.`school_year_id`={$school_yearid}
  ";
  $data=$DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function show_info_student_in_unit2($userid,$courseid,$groupid,$school_year){
	global $CFG, $DB;
    // lấy tên học sinh
	$student=$DB->get_record('user', array(
		'id' => $userid
	));
    // lấy thông tin khóa học<nhom lop>
	$course=hienthitennhomloptheotungnamhoc($courseid,$groupid,$school_year);
		 // lay thong tin lop
	$group =$DB->get_record('groups', array(
		'id' => $groupid
	)); 
    // thông tin khôi
	$block_student=$DB->get_record('block_student', array(
		'id' => $group->id_khoi
	));
    // thong tin  truong
	$school=$DB->get_record('schools', array(
		'id' => $block_student->schoolid
	)); 
	$username=$student->lastname.' '.$student->firstname;
	?>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $school->name;?>" class="form-control" disabled>
	</div>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $block_student->name;?>" class="form-control" disabled>
	</div>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('class'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $group->name;?>" class="form-control" disabled>
	</div>
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
		<input type="text" value="<?php echo $course;?>" class="form-control" disabled>
	</div>
	
	<div class="col-md-6">
		<label class="control-label"><?php echo get_string('student'); ?> <span style="color:red">*</span></label>
		<input type="text" value="<?php echo $username;?>" class="form-control" disabled>
	</div>
	<?php 

}
function semestershow_nhan_xet_ky_nang_noihk1($noi){
	$nx='';
	if($noi>=21.1){
		$nx="Your child's listening and comprehension skills are excellent. Your child has a strong grasp of vocabulary and grammatical structures taught at this level of study. Please keep up hard work.Kỹ năng nghe và hiểu của con tốt. Con nắm chắc các cấu trúc ngữ pháp và từ vựng được dạy ở trình độ này. Con hãy tiếp tục phát huy.";
	}
	if((16.1<=$noi)&&($noi<21.1)){
		$nx="Your child's listening skills are good. Your child can listen and comprehend almost all the structures and vocabulary at this level of study. However, your child should continue to expand the vocabulary and develop in this area for improved results.Kỹ năng nghe hiểu của con khá. Con nghe và hiểu được hầu hết các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để đạt kết quả tốt hơn.";
	}
	if((12.4<=$noi)&&($noi<16.1)){
		$nx="Your child's ability to apply grammartical structures learned thus far while speaking is average. There is much room for improvement in basic structures, grammar, and vocabulary.Khi thực hành nói, khả năng ứng dụng các cấu trúc ngữ pháp đã được học của con ở mức trung bình. Con cần ôn tập lại các mẫu câu, từ vựng và ngữ pháp đã học của trình độ này để giao tiếp hiệu quả hơn.";
	}
	if((0<$noi)&&($noi<12.4)){
		$nx="Your child's spoken ability at this level of study is below average. Your child makes common mistakes when speaking, and needs to be more proactive and attentive in class. Your child also needs to revise structures and vocabulary learned on this course for improved results.Khả năng nói của con ở trình độ này chưa đạt yêu cầu. Khi thực hành nói, con vẫn còn mắc một số lỗi thông thường, con cần mạnh dạn và tập trung hơn trong giờ học. Con cũng cần ôn tập lại các mẫu câu, từ vựng đã học để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
 function semestershow_nx_ky_nang_nghehk1($nghe){
 	$nx='';
 	if($nghe>=16.9){
		$nx="Your child's listening and comprehension skills are excellent. Your child has a strong grasp of vocabulary and grammatical structures taught at this level of study. Please keep up hard work.Kỹ năng nghe và hiểu của con tốt. Con nắm chắc các cấu trúc ngữ pháp và từ vựng được dạy ở trình độ này. Con hãy tiếp tục phát huy.";
	}
	if((12.9<=$nghe)&&($nghe<16.9)){
		$nx="Your child's listening skills are good. Your child can listen and comprehend almost all the structures and vocabulary at this level of study. However, your child should continue to expand the vocabulary and develop in this area for improved results.Kỹ năng nghe hiểu của con khá. Con nghe và hiểu được hầu hết các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để đạt kết quả tốt hơn.";
	}
	if((9.9<=$nghe)&&($nghe<12.9)){
		$nx="Your child's listening skills are average. Your child is able to understand most of the structures and vocabulary at this level, but needs improvement. Your child should try to expand vocabulary and practise listening skills more at home.Kỹ năng nghe của con ở mức trung bình. Con hiểu được phần lớn các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần cố gắng nhiều hơn bằng cách mở rộng vốn từ vựng và luyện nghe nhiều hơn ở nhà.";
	}
	if((0<$nghe)&&($nghe<9.9)){
		$nx="Your child's listening skills for this level of study are below average. Your child needs to be more attentive in class and practise more at home. Please encourage your child to include English songs, TV programs, or other audio programs in the study. Kỹ năng nghe của con ở trình độ này chưa đạt yêu cầu. Con cần chú ý tập trung hơn trong giờ học và luyện thêm các bài nghe ở nhà. Phụ huynh hãy khuyến khích con nghe thêm các bài hát, các chương trình tiếng Anh trên đài và ti vi để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
  function semestershow_nx_ky_nang_dochieuvaviethk1($kynang){
 	$nx='';
 	if($kynang>=46.5){
		$nx="Your child's reading and writing skills are very good. Your child's level of reading <br>comprehension and written fluency are strong for this level of study. Kỹ năng đọc hiểu và<br> viết của con tốt. Con có thể đọc, nhớ và viết được từ đồng thời sử dụng thành thạo các cấu<br> trúc câu theo yêu cầu của trình độ này.";
	}
	if((35.5<=$kynang)&&($kynang<46.5)){
		$nx="Your child's reading and writing skills are good for this level. Your child can read, and<br> reproduce most of the words and sentence structures. To improve further, your child should<br> read short stories in English that are suitable to your child's level of study.Kỹ năng đọc<br> hiểu và viết của con khá. Con có thể đọc, vận dụng lại được hầu hết các từ và cấu trúc câu<br> ở trình độ này. Con nên luyện đọc các truyện ngắn bằng tiếng Anh phù hợp với trình độ để đạt kết quả tốt hơn.";
	}
	if((27.2<=$kynang)&&($kynang<35.5)){
		$nx="Your child's reading and writing skills for this level of study are average. Your child<br>must work hard to continue to develop skills in this area. Please encourage your child to <br>ractise reading and writing more at home to get better results.Kỹ năng đọc hiểu và viết<br> của con ở mức trung bình. Con cần học chăm chỉ hơn để cải thiện các kỹ năng này. Phụ huynh<br>hãy động viên con thực hành đọc và viết nhiều hơn ở nhà để đạt kết quả cao hơn.";

	}
	if((0<$kynang)&&($kynang<27.2)){
		$nx="Your child's comprehensive reading and writing skills are below average. Your child needs<br> to try harder for improved results.Khả năng đọc hiểu và viết của con chưa đạt yêu cầu. Con<br> cần cố gắng nhiều hơn nữa để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
 // nhan xet hoc ky 2
 function semestershow_nhan_xet_ky_nang_noihk2($noi){
	$nx='';
	if($noi>=21.1){
		$nx="During the examination, your child's performance was excellent. Your child was able to use<br> most sentence structures correctly, confidently and with few errors. Con đã thể hiện xuất<br> sắc trongbài thi. Con đã vận dụng được hầu hết các mẫu câu đã học một cách thuần thục, tự<br> tin và ít khi mắc lỗi.";
	}
	if((16.1<=$noi)&&($noi<21.1)){
		$nx="During the examination, your child's ability to communicate was good for this level of <br>study. Your child could use most of the structures learned thus far on the course, however,<br> your child needs to practise more to be able to communicate more fluently and <br>accurately.Trong bài thi, con thể hiện kỹ năng giao tiếp ở mức khá. Con đã biết vận dụng <br>hầu hết các cấu trúc ngữ pháp đã học, tuy nhiên, con cần thực hành nhiều hơn để có thể giao<br> tiếp trôi chảy và chính xác.";
	}
	if((12.4<=$noi)&&($noi<16.1)){
		$nx="During the examination, your child's ability to apply grammatical structures learned thus<br> far in communication is average. There is much room for improvement in basic structures,<br> grammar and vocabulary.Trong bài thi, khả năng vận dụng các cấu trúc ngữ pháp đã học của <br>con trong giao tiếp ở mức trung bình. Con cần luyện tập thêm các mẫu câu, từ vựng và ngữ <br>pháp đã học để giao tiếp hiệu quả hơn.";
	}
	if((0<$noi)&&($noi<12.4)){
		$nx="During the examination, your child's spoken ability at this level of study was below <br>average. Your child made common mistakes when speaking. Your child needs to revise <br>structures and vocabulary learned on this course for improved results.Trong bài thi, kỹ<br> năng nói của con chưa đạt yêu cầu. Con vẫn còn mắc một số lỗi thông thường. Con cần ôn tập <br>lại các mẫu câu và từ vựng đã học của trình độ này để đạt được kết quả học tập tốt hơn.";
	}
	return $nx;
 }
  function semestershow_nx_ky_nang_nghehk2($nghe){
 	$nx='';
 	if($nghe>=16.9){
		$nx="During the examination, your child's listening and comprehension skills were excellent.<br> Your child had a strong grasp of vocabulary and grammatical structures taught at this level<br> of study.Trong bài thi, kỹ năng nghe hiểu của con được đánh giá tốt. Con nắm chắc các từ <br>vựng và cấu trúc ngữ pháp được dạy ở trình độ này.";
	}
	if((12.9<=$nghe)&&($nghe<16.9)){
		$nx="During the examination, your child's listening and comprehension skills were good. Your<br> child was able to listen and comprehend almost all the structures and vocabulary at this<br> level of study. However, your child should continue to expand vocabulary and develop in <br>this area for improved results.Trong bài thi, kỹ năng nghe hiểu của con được đánh giá ở mức<br> khá. Con nghe hiểu được hầu hết các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên,<br> con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để đạt kết quả tốt hơn.";
	}
	if((9.9<=$nghe)&&($nghe<12.9)){
		$nx="During the examination, your child's listening and comprehension skills were average. Your<br><br> child was able to understand most of structures and vocabulary at this level, but needs <br>improvement. Your child should try to expand their vocabulary, and practise listening <br>skills more at home.Trong bài thi, kỹ năng nghe hiểu của con được đánh giá ở mức trung <br>bình. Con hiểu được phần lớn các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên,<br> con cần cố gắng hơn bằng cách mở rộng vốn từ vựng và luyện nghe nhiều hơn ở nhà.";
	}
	if((0<$nghe)&&($nghe<9.9)){
		$nx="During the examination, your child's listening and comprehension skills for this level of<br> study was below average. Your child needs to be more attentive in class and practise more <br>at home.Trong bài thi, kỹ năng nghe hiểu của con chưa đạt yêu cầu. Con cần chú ý tập trung<br> hơn trong giờ học và thực hành thêm ở nhà.";
	}
	return $nx;
 }
 function semestershow_nx_ky_nang_dochieuvaviethk2($kynang){
 	$nx='';
 	if($kynang>=46.5){
		$nx="During the examination, your child's reading and writing skills were very good. Your <br>child's level of reading comprehension and written fluency were strong for this level of <br>study. Trong bài thi, kỹ năng đọc hiểu và viết của con được đánh giá rất tốt. Con có thể <br>đọc hiểu và viết thành thạo theo yêu cầu của trình độ này.";
	}
	if((35.5<=$kynang)&&($kynang<46.5)){
		$nx="During the examination, your child's reading and writing skills were good for this level.<br>Your child can read and reproduce most of the words and sentence structures at this level. <br>Trong bài thi, kỹ năng đọc hiểu và viết của con được đánh giá ở mức độ khá. Con có thể đọc,<br> vận dụng lại được hầu hết các từ và cấu trúc câu ở trình độ này. ";
	}
	if((27.2<=$kynang)&&($kynang<35.5)){
		$nx="During the examination, your child's reading and writing skills for this level of study<br> were average. You should practise reading and writing more at home to get better <br>results.Trong bài thi, kỹ năng đọc hiểu và viết của con được đánh giá ở mức trung bình. Con<br> nên thực hành đọc và viết nhiều hơn ở nhà để đạt kết quả cao hơn.";

	}
	if((0<$kynang)&&($kynang<27.2)){
		$nx="During the examination, your child's comprehensive reading and writing skills were below<br> average. Your child needs to try harder for improved results.Trong bài thi, khả năng đọc <br>hiểu và viết của con chưa đạt yêu cầu. Con cần cố gắng nhiều hơn nữa để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
?>