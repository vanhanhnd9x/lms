<?php
require("../../config.php");
// require("../../grade/lib.php");
global $CFG,$DB,$USER;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
require_login();

$action = optional_param('action', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$idnamhoc=get_id_school_year_now();
$id_namhoc=layidnamhoccuanhomlophientai($courseid);
$groupsid=layidlopcuanhom_manage_score($courseid);
$code=trim(optional_param('code', '', PARAM_TEXT));
$lastname=trim(optional_param('lastname', '', PARAM_TEXT));
$firstname=trim(optional_param('firstname', '', PARAM_TEXT));

$groups=$DB->get_record('groups', array(
  'id' => $groupsid
));
$schools=$DB->get_record('schools', array(
  'id' => $groups->id_truong
));
$course=$DB->get_record('course', array(
  'id' => $courseid
));
$school_year=$DB->get_record('school_year', array(
  'id' => $id_namhoc
));
if(is_student()){
   echo displayJsAlert('Error', $CFG->wwwroot . "/manage/manage_score/index.php");
}
if(empty($courseid)){
   echo displayJsAlert('Error', $CFG->wwwroot . "/manage/manage_score/index.php");
}
$PAGE->set_title(get_string('semester'));
// $PAGE->set_title(get_string('list_unitest'));
echo $OUTPUT->header();
$list_student=laydanhsachhocsinhcuahomlop_danhgiahocsinh($courseid);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id); 
$number=20;
$page=isset($_GET['page'])?$_GET['page']:1;
if($action=='search'){
  $url=$CFG->wwwroot.'/manage/manage_score/list_semester_student_course.php?action=search&courseid='.$courseid.'&code='.$code.'&lastname='.$lastname.'&firstname='.$firstname;
}else{
  $url=$CFG->wwwroot.'/manage/manage_score/list_semester_student_course.php?courseid='.$courseid.'&page=';
}
function lay_diem_hoc_ky_theo_hoc_sinh_cua_nhom_lo_manage_score($school_year,$school_status,$page,$number,$code=null,$lastname=null,$firstname=null,$courseid){
  global $DB, $USER;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $time=time();
  $sql="
  SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname` 
  FROM `semester`
  JOIN `user` ON `semester`.`userid`= `user`.`id` 
  JOIN `course` ON `course`.`id` = `semester`.`courseid`
  WHERE  `user`.`del`=0
  AND `semester`.`del`=0 
  AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null) 
  ";
  $sql_total="
  SELECT COUNT( DISTINCT `semester`.`id`) FROM `semester` 
  JOIN `user` ON `semester`.`userid`= `user`.`id` 
  JOIN `course` ON `course`.`id` = `semester`.`courseid`
  WHERE  `user`.`del`=0
  AND `semester`.`del`=0
  AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)
  ";
 
  if(!empty($school_year)){
    $sql.=" AND `semester`.`school_yearid`={$school_year}";
    $sql_total.=" AND `semester`.`school_yearid`={$school_year}";
  }
  if(!empty($school_status)){
    $sql.=" AND `semester`.`school`={$school_status}";
    $sql_total.=" AND `semester`.`school`={$school_status}";
  }
  if(!empty($courseid)){
    $sql.=" AND `semester`.`courseid`={$courseid}";
    $sql_total.=" AND `semester`.`courseid`={$courseid}";
  }
  if (!empty($code)) {
    $sql .= " AND `user`.`code` LIKE '%{$code}%'";
    $sql_count .= " AND `user`.`code` LIKE '%{$code}%'";

  }
  if (!empty($lastname)) {
   $sql .= " AND `user`.`lastname` LIKE '%{$lastname}%' ";
   $sql_count .= " AND `user`.`lastname` LIKE '%{$lastname}%'";
  }
  if (!empty($firstname)) {
   $sql .= " AND `user`.`firstname` LIKE '%{$firstname}%' ";
   $sql_count .= " AND `user`.`firstname` LIKE '%{$firstname}%'";
  }
  $sql.=" LIMIT {$start} , {$number}";
  $data=$DB->get_records_sql($sql);

  $total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);;
  $total_page=ceil((int)$total_row / (int)$number);
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page
  );
  return $kq;

}


?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <div class="col-md-2">
           <!--  <a href="<?php echo new moodle_url('/manage/manage_score/add_unittest.php?idstudent='.$idstudent); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>  -->
            <a href="#myModal" class="btn btn-success" data-toggle="modal" ><?php echo get_string('new'); ?></a>
            <p style="margin-top: 10px"></p>
            <a href="<?php echo new moodle_url('/manage/manage_score/import_semester_course.php?courseid='.$courseid.'&school_year='.$id_namhoc); ?>" class="btn btn-success"><?php echo get_string('import_excel'); ?></a> 
               
          </div>
          <div class="col-md-10">
          <form action="" method="get" accept-charset="utf-8" class="form-row">
            <div class="col-4 form-group">
              <label for=""><?php echo get_string('schools'); ?></label>
              <input type="" name="" class="form-control"  value="<?php echo $schools->name; ?>" disabled>
          </div>
          <div class="col-4 form-group">
              <label for=""><?php echo get_string('groups'); ?></label>
              <input type="" name="" class="form-control"  value="<?php echo $groups->name; ?>" disabled>
          </div>
          <div class="col-4 form-group">
              <label for=""><?php echo get_string('classgroup'); ?></label>
              <input type="" name="" class="form-control"  value="<?php echo $course->fullname; ?>" disabled>
          </div>
          <div class="col-4 form-group">
              <label for=""><?php echo get_string('school_year'); ?></label>
              <input type="" name="" class="form-control"  value="<?php echo $school_year->sy_start.' - '.$school_year->sy_end; ?>" disabled>
          </div>
             <input type="" name="action" class="form-control" value="search" hidden="">
             <input type="" name="courseid" class="form-control"  value="<?php echo $courseid; ?>" hidden="">
          <div class="col-4 form-group">
              <label for=""><?php echo get_string('codestudent'); ?></label>
              <input type="" name="code" class="form-control" placeholder="<?php echo get_string('codestudent'); ?>" value="<?php echo trim($code); ?>">
          </div>
          <div class="col-4 form-group">
              <label for=""><?php echo get_string('lastname'); ?></label>
              <input type="" name="lastname" class="form-control" placeholder="<?php echo get_string('lastname'); ?>" value="<?php echo trim($lastname); ?>">
          </div>
          <div class="col-4 form-group">
              <label for=""><?php echo get_string('firstname'); ?></label>
              <input type="" name="firstname" class="form-control" placeholder="<?php echo get_string('firstname'); ?>" value="<?php echo trim($firstname); ?>">
          </div>
     <div class="col-4">
      <div style="    margin-top: 29px;"></div>
      <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
    </div>
  </form>
</div>
</div>
<div class="table-responsive" data-pattern="priority-columns" id="searchResults">
    <?php 
    // load diem tieu hoc
      if($schools->level==1):
        // $data=search_semester($schoolid=null,$block_student=null,$groupid=null,$name=null,$school_yearid,1,$page,$number,$idstudent);
        // $data=lay_diem_hoc_ky_theo_hoc_sinh_cua($school_yearid,1,$page,$number,$idstudent);
        $data=lay_diem_hoc_ky_theo_hoc_sinh_cua_nhom_lo_manage_score($id_namhoc,1,$page,$number,$code,$lastname,$firstname,$courseid);
        // var_dump($data);
        ?>
        <table id="tech-companies-1" class="table  table-striped table-hover">
          <thead>
            <tr>
              <th class="text-center">STT</th>
              <th><?php echo get_string('codestudent'); ?></th>
              <th><?php echo get_string('namestudent'); ?></th>
              <th><?php echo get_string('speaking'); ?></th>
              <th><?php echo get_string('listening'); ?></th>
              <th><?php echo get_string('readingWriting'); ?></th>
              <th><?php echo get_string('totalMidtermMark1'); ?></th>
              <th><?php echo get_string('speaking'); ?></th>
              <th><?php echo get_string('listening'); ?></th>
              <th><?php echo get_string('readingWriting'); ?></th>
              <th><?php echo get_string('totalMidtermMark2'); ?></th>
              <th><?php echo get_string('overall'); ?></th>
            </tr>
          </thead>
          <tbody id="ajaxschools">
              <?php 
              if($data['data']){
               $i=0;
               foreach ($data['data'] as $sch) { 
                $i++;
                $username= $sch->lastname.' '.$sch->firstname;
                if($sch->tonghocki_2>0){
                  $tonghk2=$sch->tonghocki_2;
                  $tong=$sch->tongket;
                }else{
                  $tonghk2=0;
                  $tong=0;
                }
                // $namehoc=$DB->get_record('school_year', array('id' => $sch->school_yearid));
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <td><?php echo $sch->code; ?></td>
                  <td><?php  echo $username;?></td>
                  <td class="text-center"><?php echo $sch->noi_1; ?></td>
                  <td class="text-center"><?php echo $sch->nghe_1; ?></td>
                  <td class="text-center"><?php echo $sch->doc_viet_1; ?></td>
                  <td class="text-center"><?php echo $sch->tonghocki_1; ?></td>
                  <td class="text-center"><?php echo $sch->noi_2; ?></td>
                  <td class="text-center"><?php echo $sch->nghe_2; ?></td>
                  <td class="text-center"><?php echo $sch->doc_viet_2; ?></td>
                  <td class="text-center"><?php echo $tonghk2; ?></td>
                  <td class="text-center"><?php echo $tong; ?></td>
                 
                  
                </tr>
          
          <?php 
        }
      }else{
        echo'
        <tr><td colspan="8" style="text-align: center;">'.get_string('no_records').'</td></tr>';
      }

      ?>
    </tbody>
  </table>
        <?php 
      endif;
      // load diem trung hoc
      if(in_array($schools->level,array('2','3'))):
        $data=lay_diem_hoc_ky_theo_hoc_sinh_cua_nhom_lo_manage_score($id_namhoc,2,$page,$number,$code,$lastname,$firstname,$courseid);
        ?>
          <table id="tech-companies-1" class="table  table-striped table-hover">
            <thead>
              <tr>
                <th >STT</th>
                <th ><?php echo get_string('codestudent'); ?></th>
                <th ><?php echo get_string('namestudent'); ?></th>
                <th ><?php echo get_string('speaking'); ?></th>
                <th ><?php echo get_string('essay'); ?></th>
                <th ><?php echo get_string('reading'); ?></th>
                <th ><?php echo get_string('listening'); ?></th>
                <th ><?php echo get_string('vocabularyGramar'); ?></th>
                <th ><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
                <th ><?php echo get_string('totalMidtermMark1'); ?></th>

                <th ><?php echo get_string('speaking'); ?></th>
                <th ><?php echo get_string('essay'); ?></th>
                <th ><?php echo get_string('reading'); ?></th>
                <th ><?php echo get_string('listening'); ?></th>
                <th ><?php echo get_string('vocabularyGramar'); ?></th>
                <th ><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
                <th ><?php echo get_string('totalMidtermMark2'); ?></th>
                <th ><?php echo get_string('overall'); ?></th>
              </tr>
            </thead>
            <tfoot>
            </tfoot>
            <tbody id="ajaxschools">
              <?php 
              if($data['data']){
               $i=0;
               foreach ($data['data'] as $sch) { 
                $i++;
                $username= $sch->lastname.' '.$sch->firstname;
                if($sch->tonghocki_2>0){
                  $tonghk2=$sch->tonghocki_2;
                  $tong=$sch->tongket;
                }else{
                  $tonghk2=0;
                  $tong=0;
                }
                // $namehoc=$DB->get_record('school_year', array('id' => $sch->school_yearid));
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <td><?php echo $sch->code; ?></td>
                  <td><?php  echo $username;?></td>
                  <td class="text-center"><?php echo $sch->noi_1; ?></td>
                  <td class="text-center"><?php echo $sch->viet_luan_1; ?></td>
                  <td class="text-center"><?php echo $sch->doc_hieu_1; ?></td>
                  <td class="text-center"><?php echo $sch->nghe_1; ?></td>
                  <td class="text-center"><?php echo $sch->tv_np_1; ?></td>
                  <td class="text-center"><?php echo $sch->tvnp_dochieu_viet_1; ?></td>
                  <td class="text-center"><?php echo $sch->tonghocki_1; ?></td>

                  <td class="text-center"><?php echo $sch->noi_2; ?></td>
                  <td class="text-center"><?php echo $sch->viet_luan_2; ?></td>
                  <td class="text-center"><?php echo $sch->doc_hieu_2; ?></td>
                  <td class="text-center"><?php echo $sch->nghe_2; ?></td>
                  <td class="text-center"><?php echo $sch->tv_np_2; ?></td>
                  <td class="text-center"><?php echo $sch->tvnp_dochieu_viet_2; ?></td>
                  <td class="text-center"><?php echo $tonghk2; ?></td>
                  <td class="text-center"><?php echo $tong; ?></td>

                  
                </tr>
                <?php 
              }
            }else{
              echo'

              <tr><td colspan="8" style="text-align: center;">'.get_string('no_records').'</td></tr>
              ';
            }

            ?>
          </tbody>
        </table>
        <?php 
        
      endif;
    ?>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>
</div>
</div>
</div>
</div>
<div class="container">
<!-- <a href="#myModal" class="btn btn-primary" data-toggle="modal">Launch demo modal</a>
-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <!-- <h4 class="modal-title">Lựa chọn học sinh</h4> -->
        </div>
        <div class="modal-body">
          <form action="<?php echo $CFG->wwwroot ?>/manage/manage_score/add_semester_student.php" method="get" >
            <div class="row">
              <div class="col-md-12">
                <label for="" class=""><?php echo get_string('student'); ?><span style="color:red">*</span></label>
                <select name="idstudent" id="" class="form-control" required="">
                  <option value=""><?php echo get_string('student'); ?></option>
                  <?php 
                     foreach ($list_student as  $value) {
                       # code...
                      echo '<option value="'.$value->id.'">'.$value->lastname.' '.$value->firstname.'</option>';
                     }
                   ?>
                  
                </select>
                <input type="text" hidden="" name="course" value="<?php echo $courseid; ?>">
                <input type="text" hidden="" name="school_year" value="<?php echo $id_namhoc; ?>">
              </div>
              <div class="col-md-12" style="margin-top: 10px;">
                  <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('next')) ?> " id="submitBtn">
                  <button type="button" class="btn btn-danger" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
              </div>
            </div>
          </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


  </div>
  <style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<?php

echo $OUTPUT->footer();
?>

