<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../config.php");
require("../grade/lib.php");
global $CFG, $USER,$DB;

require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->libdir . '/completionlib.php'); // Cuong added
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/lib/completionlib.php');
require_once($CFG->dirroot . '/lib/completion/completion_completion.php');
require_once($CFG->dirroot . '/manage/news/lib.php'); // Require for right block news
require_once($CFG->dirroot . '/manage/notification/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');


//Quang added to fix switch role to learner, when click in menu --> back to teacher role
// $current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
// $id = optional_param('id', -1, PARAM_INT);//course id
// if($current_switchrole!=-1&&$id !=-1){
// $context = get_context_instance(CONTEXT_COURSE, $id);
// role_switch($current_switchrole, $context);
// require_login($id);

// }
// else{

// }
// $mobile = detect_mobile();
// if($mobile === true){
//     echo displayJsAlert('', $CFG->wwwroot.'/mobile/home.php');
// } 

// $PAGE->set_url('/mod/noticeboard/view.php');

require_login(0,false);
$PAGE->set_title(get_string('home'));
$PAGE->set_heading(get_string('home'));

echo $OUTPUT->header();

$lastestNews = news_list($USER->parent,0, 'lastmodified DESC', 0, 10);
$sothongbao=conunt_notifications_student($USER->id);


$idnamhoc=get_id_school_year_now();
$courseid=id_nhom_cua_hoc_sinh_home($USER->id);
$idgvtg=lay_id_gvtg_thuoc_nhom_lop_home($courseid);
$schedule=hien_thi_lich_hoc_theo_nhom_cua_gv_login($courseid,$idnamhoc);
$gvtg=$DB->get_record('user', array(
  'id' => $idgvtg
));
if(!is_student()){
     echo displayJsAlert('', $CFG->wwwroot . "/manage/");
}
if(is_student()): 

$unittestOfUser = get_unittest($USER->id);
$chuyencan = get_diligence_hs($USER->id);
?>
<div class="row">
    <div class="col-md-12 text-right">
        <a class="notification" href="<?php echo $CFG->wwwroot ?>/manage/notification/" id="modalclick"><i class="fa fa-bell" style="font-size: 14pt;"></i><span class="badge" style="background: red; color: #fff"><?php echo $sothongbao; ?></span></a>
    </div>
</div>
<div class="row" style="margin-top: 30px;">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title md-4"><?php echo get_string('tkb'); ?></h4>
                <!-- lấy user đăng nhập truyền vào 177 -->
                <?php $course_records = get_teams_user($USER->id); ?> 

             <table class="table table-bordered">
                <tr>
                    <th>STT</th>
                    <th><?php echo get_string('date') ?></th>
                    <th><?php echo get_string('time') ?></th>
                    <th><?php echo get_string('syllabus_name') ?></th>
                    <th><?php echo get_string('classgroup') ?></th>
                    <th><?php echo get_string('class_room') ?></th>
                </tr>
                <tr>
                <?php 
                    if(!empty($schedule)){
                        $i=0;
                        foreach ($schedule as $value) {
                            # code...
                            $i++;
                            switch ($value->day) {
                                case 2: $day= get_string('monday'); break;
                                case 3: $day= get_string('tuesday'); break;
                                case 4: $day=  get_string('wednesday'); break;
                                case 5: $day=  get_string('thursday'); break;
                                case 6: $day=  get_string('friday'); break;
                                case 7: $day=  get_string('saturday'); break;
                                case 8: $day=  get_string('sunday'); break;
                            }
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $day; ?></td>
                                <td><?php echo $value->time_start.' - '.$value->time_end; ?></td>
                                <td><?php echo $value->book; ?></td>
                                <td><?php echo $value->fullname; ?></td>
                                <td><?php echo $value->location; ?></td>
                            </tr>
                            <?php
                        }
                    }
                ?> 
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 30px;">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title md-4"><?php echo get_string('GVTG'); ?></h4>
            <!--  <table class="table table-bordered">
                <tr>
                    <th><?php echo get_string('date') ?></th>
                    <th><?php echo get_string('time') ?></th>
                    <th><?php echo get_string('syllabus_name') ?></th>
                    <th><?php echo get_string('classgroup') ?></th>
                    <th><?php echo get_string('class_room') ?></th>
                </tr>
                
                <tr>
                </tr>
            </table> -->
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label"><?php echo get_string('firstname'); ?></label>
                    <input type="" name="" class="form-control"  value="<?php echo $gvtg->lastname.' '.$gvtg->firstname; ?>" disabled>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><?php echo get_string('email'); ?> </label>
                    <input type="" name="" class="form-control"  value="<?php echo $gvtg->email; ?>" disabled>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><?php echo get_string('mobileph'); ?> </label>
                    <input type="" name="" class="form-control"  value="<?php echo $gvtg->phone2; ?>" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title md-4"><?php echo get_string('attendance') ?></h4>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                      <tr>
                      <?php 
                      $nd  = get_n_t(date('m'));
                      foreach ($nd as $key => $value): ?>
                        <th><?php echo $value['ngay'] ?></th>
                      <?php endforeach ?>
                      </tr>
                      <tr>
                        <?php foreach($nd as $key=>$value) {?>
                            <th class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
                            if($value['thu']=='Sat'){echo 'bg-warning';} ?>" colspan="" rowspan="" headers="" scope=""> <?php  echo $value["thu"] ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                      <?php foreach($nd as $key=>$value) {?>
                            <td><?php 
                              $user_dd = getDd($USER->id,get_u_g($USER->id),date('m'));
                                $day = $value["ngay"];
                                $month= date('m');
                                $year = date('Y');
                                // var_dump($ss);
                                foreach ($user_dd as $item) {
                                    if ($item->day == $day && $item->month == $month && $item->year == $year) {
                                        echo get_string('absent');
                                    }
                                }
                                 ?></td>
                        <?php } ?>
                    </tr>
                    </table>
            </div>
        </div>
    </div>
</div> -->

<!-- <div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title mb-4"><?php echo get_string('unit_test') ?></h4>

            <div class="form-group">
                <label for="" class="col-md-2"><?php echo get_string('input_score') ?></label>
                <span><?php echo get_gv($USER->id)->input_score ?></span>    
            </div>

            <div class="form-group">
                <label for="" class="col-md-2"><?php echo get_string('point_unitest') ?></label><span><?php echo get_string('year').' '.date('Y') ?></span>
                <table class="table table-bordered">
                    <tr>
                        <th></th>
                        <?php for ($i=1; $i <=12 ; $i++) : ?>
                          <th><option><?php echo get_string('month')." ".$i ?></option></th>
                          <?php endfor ?>
                    </tr>
                    <tr>
                        <th><?php echo get_string('score') ?></th>
                        <?php for($i=1;$i<=12;$i++) : ?>
                            <td>
                            <?php foreach ($unittestOfUser as $val): ?>
                                <?php if ($i==$val->month): ?>
                                    <?php echo $val->score_1 ?>
                                <?php endif ?>              
                            <?php endforeach ?>
                            </td>
                        <?php endfor; ?>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div> -->
<?php endif; ?>
<!--  <div class="modal fade" id="modal_notification" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="dismiss_modal()">&times;</button>
          <h4 class="modal-title"><b>Thông báo</b></h4>
        </div>
        <div class="modal-body">
            <p>Không có thông báo</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="dismiss_modal()">Close</button>
        </div>
      </div>
    </div>
  </div> -->

<?php
    echo $OUTPUT->footer();
?>