<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../../config.php')) {
    header('Location: ../../install.php');
    die;
}
require('../../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/teacher/lib.php');
// require_once($CFG->dirroot . '/teams/config.php');
require_login(0, false);
$PAGE->set_title(get_string('editmyprofile'));
$PAGE->set_heading(get_string('editmyprofile'));
global $USER;
$user_id = optional_param('user_id', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
echo $OUTPUT->header();
$user = get_user_from_id($user_id);

if ($action == 'update_rht') {
    $user = get_user_from_id($user_id);
    $firstname = optional_param('firstname', '', PARAM_TEXT);
    $lastname = optional_param('lastname', '', PARAM_TEXT);
    // $username = optional_param('username', '', PARAM_TEXT);
    // $birthday = optional_param('birthday', '', PARAM_TEXT);

    $name_ac = optional_param('ac', '', PARAM_TEXT);
    $email =  optional_param('email', '', PARAM_TEXT);;
    $profile_img = upload_img('avatar', $CFG->dirroot.'/account/logo/');
    if(empty($profile_img)) {
      $profile_img = $user->profile_img;
    }
    $phone2 = optional_param('phone2', '', PARAM_TEXT);
    $description = optional_param('description', '', PARAM_TEXT);


    // switch ($role_id) {
    //     case 'admin':
    //         $role_id = 3;
    //         break;
    //     case 'learner':
    //         $role_id = 5;
    //         break;
    //     case 'teacher':
    //         $role_id = 8;
    //         break;
    //     case 'teachertutors':
    //         $role_id = 10;
    //         break;
    //     default:
    //         break;
    // }

    
    $new_user_id = update_rht_teacher($user_id, $firstname,$lastname,$email,$profile_img,$phone2,$name_ac,$area,$country,$description);

    echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/teacher/managerteacher/rhta.php");
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='rhta';
$namecheck='edit';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$namecheck);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$listschools = $DB->get_records_sql('SELECT * FROM schools');


?>

<script type="text/javascript" src="<?php print new moodle_url('/manage/people/js/validate_profile.js'); ?>"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form action="<?php echo $CFG->wwwroot ?>/manage/teacher/managerteacher/edit_rhta.php?action=update_rht" onsubmit="return validate();" method="post" enctype="multipart/form-data">
            <div class="row">
              <input id="user_id" name="user_id" type="hidden" value="<?php echo $user_id; ?>">
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('lastname')) ?><span class="text-danger">*</span></label>
                
                  <div id="erLast" class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php print_string('enter_lastname') ?>   
                  </div>
                  <input class="form-control" id="lastname" name="lastname" type="text" value="<?php echo $user->lastname  ?>" required>
                
              </div>
              <div class="form-group col-md-6">
                  <label class="col-form-label"><?php print_r(get_string('firstname')) ?><span class="text-danger">*</span></label>
                
                    <div id="erFirst" class="alert alert-danger" role="alert" style="display: none;">
                        <?php print_string('enter_firstname') ?>
                    </div>
                    <input class="form-control" id="firstname" name="firstname" type="text" value="<?php echo $user->firstname ?>" required>
              </div>

              

              <div class="form-group col-md-6">
                <label class="col-form-label">Avatar </label><br>
                
                    <input type="file" id="file" class="btn-light" name="avatar" />
                    <div id="er_img"></div>
                    <img src="<?php echo $CFG->wwwroot.'/account/logo/'.$user->profile_img ?>" alt="" width="100px">
                
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('email_new')) ?><span class="text-danger">*</span>:</label>
                <input class="form-control" id="email" name="email" type="emailHelp" value="<?php echo $user->email  ?>" required>
                <span id="erEmail" class="text-danger" style="display: none;"><?php print_string('enter_emailid') ?></span>
              </div>
    

              <div class="form-group col-md-6">
                  <label class="col-form-label"><?php print_r(get_string('phone')) ?></label>
                  <input class="form-control" id="phone2" name="phone2" type="text" value="<?php echo $user->phone2  ?>">
                  <span id="er_phone" class="text-danger"></span>
              </div>



              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('coordinator')) ?> (TAAC)</label>
                <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $user->name_ac ?>">

              </div>


              <div class="form-group col-md-12">
                <label class="col-form-label"><?php print_r(get_string('description')) ?></label>
                <textarea name="description" class="form-control"><?php echo $user->description  ?></textarea>
              </div>



              <hr>
              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                  <?php print_r(get_string('or')) ?> 
                  <a href="<?php echo $CFG->wwwroot . '/manage/teacher/managerteacher/rhta.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('cancel')) ?>
                  </a>
                </div>
              </div>
        </form>   
      </div>
  </div>
</div>

<?php
    echo $OUTPUT->footer();
?>
<script type="text/javascript">
$('#file').change(function(event) {
    var er=0;
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      // lay dung luong va kieu file tu the input file
        if($('#file').get(0).files.length==0){
          // $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Chọn ảnh đại diện</div>');
          er=0;
        }else{
          var fname = $('#file')[0].files[0].name;
          var fsize = $('#file')[0].files[0].size;
          var ftype = $('#file')[0].files[0].type;

          if (ftype=='image/png' || ftype=='image/gif' || ftype=='image/jpeg' || ftype=='image/pjpeg') {
            
            if (fsize > <?php echo get_real_size(ini_get('upload_max_filesize')) ?>) {
              
              $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Dung lượng file < <?php echo ini_get('upload_max_filesize') ?></div>');
              er=1;
              
            }else{
              $("#er_img").html('<div class="alert alert-success" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>File hợp lệ</div>');
              er=0;
            }
          }else{
            $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Không đúng định dạng ảnh</div>');
            er=1;
          }

        }
         
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    }

    if(er==1){
      $("#saveUser").attr('disabled','disabled');
    }else{
      $("#saveUser").removeAttr('disabled');
    }
});
</script>