<?php 
require('../../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/common/header_lib.php');
$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 15, PARAM_INT);

$name = optional_param('name', '', PARAM_TEXT);
$phone = optional_param('phone', '', PARAM_TEXT);
$email = optional_param('email', '', PARAM_TEXT);
$coordinator = optional_param('coordinator', '', PARAM_TEXT);

$params = " AND (   
                user.email LIKE '%".$email."%' 
                 AND user.phone2 LIKE '%".$phone."%' 
                 AND user.name_ac LIKE '%".$coordinator."%' 
                 AND (
                 		CONCAT(user.firstname,' ',user.lastname) LIKE '%".$name."%'
                 OR CONCAT(user.lastname,' ',user.firstname) LIKE '%".$name."%'
                 )
            )";

$teachers     = get_list_taacrhta($page,$perpage,$params);
$totalcount   = count(get_list_taacrhta(null,$perpage,$params));
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='rhta';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
 ?>

<?php if (!empty($teachers)): ?>
 <?php $i=0; foreach ($teachers as $teacher) { $i++;?>
<tr>
  <td class="text-center"><?php echo $i ?></td>
  <?php 
    if(!empty($hanhdong)||!empty($checkdelete)){
        echo'<td class="text-center hidden-print">';
          $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$teacher->id);
        if(!empty($checkdelete)){
            ?>
          <a onclick="return Confirm('Xóa giáo viên','Bạn có muốn xóa <?php echo $teacher->lastname .' '. $teacher->firstname ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=delete',array('id'=>$teacher->id)); ?>')" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            <?php 
        }
        echo'</td>';
    }
  ?>
  <td><?php echo $teacher->code ?></td>
  <td><a href="" style="color: #000">
      <?php echo $teacher->lastname .' '. $teacher->firstname ?></a>
  </td>
  <td>
    <?php
    if(!empty($teacher->profile_img)){
      $path1= $CFG->dirroot. '/account/logo/'.$teacher->profile_img;
      if( file_exists($path1)){
          echo '  <img src="'.$CFG->wwwroot.'/account/logo/'.$teacher->profile_img.'" style="width: 60px">';
      }else{
           echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
      }
    }else{
        echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
    } ?>
  </td>
  
  <td><?php echo $teacher->phone2 ?></td>
  <td><?php echo $teacher->email ?></td>
  
  <td><?php echo $teacher->name_ac  ?></td>

  <!-- <td class="text-right">
      <a href="<?php print new moodle_url('/manage/teacher/managerteacher/edit_rhta.php',array('user_id'=>$teacher->id)); ?>" class="btn waves-effect waves-light btn-info btn-sm "><i class="fa fa-pencil" aria-hidden="true"></i></a>
      <a onclick="return Confirm('Xóa giáo viên','Bạn có muốn xóa <?php echo $teacher->firstname .' '. $teacher->lastname ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=delete',array('id'=>$teacher->id)); ?>')" class="btn waves-effect waves-light btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
  </td> -->
  
</tr>
<?php } ?>
<tr class="active">
  <td colspan="10">
    <div class="float-left">
      <?php
        paginate($totalcount,$page,$perpage,"#"); 
      ?>
    </div>
  </td>
</tr>
<script>
	$('a').on('click', function(event) {
		var x=$(this).attr('href');
		var page=x.replace('#page=', '');
		$.get("ajaxrhta.php?name="+$("#name").val()+"&phone="+$("#phone").val()+"&email="+$("#email").val()+"&coordinator="+$("#coordinator").val()+"&page="+page, function(data){
            $('#page').html(data);
        });
	});
</script>
<?php else: ?>
<tr>
  <td colspan="10">
    <span class="text-danger"><h5><?php echo get_string('searchnoresult') ?></h5></span>
  </td>
</tr>
<?php endif ?>