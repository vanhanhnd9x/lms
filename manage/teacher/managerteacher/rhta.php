<?php
if (!file_exists('../../../config.php')) {
  header('Location: ../../install.php');
  die;
}

require('../../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');

require_login();
$PAGE->set_title(get_string('managert'));
$PAGE->set_heading(get_string('managert'));
echo $OUTPUT->header();


$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 15, PARAM_INT);
$search       = optional_param('search', '', PARAM_TEXT);

$params = " AND (firstname LIKE '%".trim($search)."%'
                        OR lastname LIKE '%".trim($search)."%'    
                        OR email LIKE '%".trim($search)."%' 
                        OR phone2 LIKE '%".trim($search)."%' 
                        OR birthday LIKE '%".trim($search)."%' 
                        OR CONCAT(firstname,' ',lastname) LIKE '%".$search."%'
                        OR CONCAT(lastname,' ',firstname) LIKE '%".$search."%'
                    )";

$teachers     = get_list_taacrhta($page,$perpage,$params);
$totalcount   = count(get_list_taacrhta(null,$perpage,$params));



// print_r($rows);


$userid      = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
if($action=='delete'){
    // $DB->delete_records('user',array('id' => $userid));
    // trang edit
    del_teacher($userid);
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/teacher/managerteacher/rhta.php");
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='rhta';
$namecheck='managert';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$namecheck);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);

?>
<script src="<?php print new moodle_url('/manage/search.js'); ?>"></script>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <?php 
                        if(!empty($checkthemmoi)){
                            ?>
                            <div class="col-md-2">
                                <a href="<?php echo new moodle_url('/manage/teacher/managerteacher/new_rhta.php'); ?>" class="btn btn-success"><?php print_r(get_string('new')) ?></a>
                            </div>
                            <?php 
                        }
                     ?>
                    
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label><?php echo get_string('name') ?></label>
                                <input type="text" id="name" class="saj form-control form-group" placeholder="<?php print_r(get_string('name')) ?>...">

                                <label><?php echo get_string('phone') ?></label>
                                <input type="text" id="phone" class="saj form-control" placeholder="<?php print_r(get_string('phone')) ?>..."> 

                            </div>
                            <div class="col-md-4 form-group">
                                <label><?php echo get_string('email') ?></label>
                                <input type="text" id="email" class="saj form-control form-group" placeholder="<?php print_r(get_string('email')) ?>...">
                                
                                <label><?php echo get_string('coordinator') ?></label>
                                <input type="text" id="coordinator" class="saj form-control" placeholder="<?php print_r(get_string('coordinator')) ?>..."> 
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(!empty($totalcount)){ ?>
                <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo'<th class="align-middle text-center">'.get_string('action').'</th>';
                                    }
                                ?>
                                <th>
                                    <?php print_r(get_string('codemanagement')) ?>  
                                </th>
                                <th><?php print_r(get_string('name')) ?></th>
                                <th>Avatar</th>
                                <th><?php print_r(get_string('phone')) ?></th>
                                <th><?php print_r(get_string('email_new')) ?></th>
                                <th><?php print_r(get_string('coordinator')) ?></th>
                            </tr>
                        </thead>
                        <tbody id="page">
                            <?php $i=$page*$perpage+1; foreach ($teachers as $teacher) { ?>
                            <tr>
                                <td class="text-center"><?php echo $i++; ?></td>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo'<td class="text-center hidden-print">';
                                         $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$teacher->id);
                                        if(!empty($checkdelete)){
                                           ?>
                                          <a onclick="return Confirm('Xóa giáo viên','Bạn có muốn xóa <?php echo $teacher->lastname .' '. $teacher->firstname ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=delete',array('id'=>$teacher->id)); ?>')" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                           <?php 
                                        }
                                        echo'</td>';
                                    }
                                ?>
                                <td><?php echo $teacher->code ?></td>
                                <td><a href="" style="color: #000">
                                    <?php echo $teacher->lastname .' '. $teacher->firstname ?></a>
                                </td>
                                <td>
                                    <?php
                                 if(!empty($teacher->profile_img)){
                                    $path1= $CFG->dirroot. '/account/logo/'.$teacher->profile_img;
                                    if( file_exists($path1)){
                                        echo '  <img src="'.$CFG->wwwroot.'/account/logo/'.$teacher->profile_img.'" style="width: 60px">';
                                    }else{
                                        echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
                                    }
                                 }else{
                                     echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
                                 } ?>
                                </td>
                                
                                <td><?php echo $teacher->phone2 ?></td>
                                <td><?php echo $teacher->email ?></td>
                                
                                <td><?php echo $teacher->name_ac  ?></td>
                            </tr>
                            <?php } ?>
                            <tr class="active">
                                <td colspan="10">
                                    <div class="float-left">
                                        <?php
                                            if($search==""){
                                                $url ="index.php?";
                                            }else{
                                                $url ="index.php?search=".$search.'&';
                                            } 
                                            
                                            paginate($totalcount,$page,$perpage,$url); 
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php }else{ ?>
                    <h4 class="text-danger">Không có bản ghi!</h4>
                <?php } ?>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<?php
    echo $OUTPUT->footer();
?>
<script>
$(document).ready(function(){
  $("#searchTeacher").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#page tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<script type="text/javascript">
  $(".saj").keyup(function() {
        $.get("ajaxrhta.php?name="+$("#name").val()+"&phone="+$("#phone").val()+"&email="+$("#email").val()+"&coordinator="+$("#coordinator").val(), function(data){
            $('#page').html(data);
        });
    });
</script>