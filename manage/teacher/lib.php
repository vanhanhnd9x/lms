<?php 
require_once('../../../config.php');
global $USER, $CFG, $DB;

function update_foreign_teacher($user_id, $firstname,$lastname,$typeoflabor,$schoolid,$email,$profile_img,$phone2,$country,$name_rht,$name_ac,$time_start,$time_end,$description) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->firstname = trim($firstname);
    $record->lastname = trim($lastname);
    $record->email = trim($email);
    $record->profile_img = $profile_img;
    // $record->code = trim($code);
    $record->typeoflabor = $typeoflabor;
    $record->schoolid = trim($schoolid);
    $record->phone2 = trim($phone2);

    // $record->username = trim($username);
    
    // kích hoạt hoặc khóa tài khoản
    $record->confirmed = 1;

    $record->name_rht =trim($name_rht);
    $record->name_ac =trim($name_ac);
    $record->country = trim($country);
    $record->time_start = $time_start;
    $record->time_end = $time_end;
    $record->description = $description;
    return $DB->update_record('user', $record);
}


function update_foreign_tutorsteacher($user_id, $firstname,$lastname,$typeoflabor,$schoolid,$email,$profile_img,$phone2,$name_rht,$name_ac,$time_start,$time_end,$description) {
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->firstname = trim($firstname);
    $record->lastname = trim($lastname);
    $record->email = trim($email);
    $record->profile_img = $profile_img;
    // $record->code = trim($code);
    $record->typeoflabor = $typeoflabor;
    $record->schoolid = $schoolid;
    $record->phone2 = $phone2;

    // $record->username = $username;
    
    // kích hoạt hoặc khóa tài khoản
    $record->confirmed = 1;

    $record->name_rht =$name_rht;
    $record->name_ac =$name_ac;
    $record->time_start = $time_start;
    $record->time_end = $time_end;
    $record->description = $description;
    return $DB->update_record('user', $record, false);
}

function update_rht_teacher($user_id, $firstname,$lastname,$email,$profile_img,$phone2,$name_ac,$area,$country,$description){
    global $DB;
    $record = new stdClass();
    $record->id = $user_id;
    $record->firstname = trim($firstname);
    $record->lastname = trim($lastname);
    // $record->birthday = $birthday;
    $record->email = $email;
    $record->profile_img = $profile_img;
    $record->country = $country;
    $record->area = $area;
    $record->phone2 = $phone2;
    
    // kích hoạt hoặc khóa tài khoản
    $record->confirmed = 1;

    $record->name_ac =$name_ac;
    $record->description = $description;
    return $DB->update_record('user', $record, false);
}

// Mã GV
function update_code_teacher($number){
   global $DB;
   $record = new stdClass();
   $record->id = 1;
   $record->number = $number;
   $record->time_update =time();
   return $DB->update_record('code_teacher', $record, false);
}
function get_number_code_teacher(){
  global $CFG, $DB;
  $number_code=$DB->get_record('code_teacher', array(
    'id' => '1'
  ));
  return $number_code->number;
}

function update_code_teacher_tg($number){
   global $DB;
   $record = new stdClass();
   $record->id = 2;
   $record->number = $number;
   $record->time_update =time();
   return $DB->update_record('code_teacher', $record, false);
}
function get_number_code_teacher_tg(){
  global $CFG, $DB;
  $number_code=$DB->get_record('code_teacher', array(
    'id' => '2'
  ));
  return $number_code->number;
}

function update_code_teacher_qlgvnn($number){
   global $DB;
   $record = new stdClass();
   $record->id = 3;
   $record->number = $number;
   $record->time_update =time();
   return $DB->update_record('code_teacher', $record, false);
}
function get_number_code_teacher_qlgvnn(){
  global $CFG, $DB;
  $number_code=$DB->get_record('code_teacher', array(
    'id' => '3'
  ));
  return $number_code->number;
}
function update_code_teacher_qlgvtg($number){
   global $DB;
   $record = new stdClass();
   $record->id = 4;
   $record->number = $number;
   $record->time_update =time();
   return $DB->update_record('code_teacher', $record, false);
}
function get_number_code_teacher_qlgvtg(){
  global $CFG, $DB;
  $number_code=$DB->get_record('code_teacher', array(
    'id' => '4'
  ));
  return $number_code->number;
}
// trang add

?>