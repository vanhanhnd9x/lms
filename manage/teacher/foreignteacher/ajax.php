<?php 
require('../../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/common/header_lib.php');

$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 20, PARAM_INT);

// $search       = optional_param('search', '', PARAM_TEXT);

$name = optional_param('name', '', PARAM_TEXT);
$phone = optional_param('phone', '', PARAM_TEXT);
// $email = optional_param('email', '', PARAM_TEXT);
$schools = optional_param('schools', '', PARAM_TEXT);
$typeoflabor = optional_param('typeoflabor', '', PARAM_TEXT);
// $manager = optional_param('manager', '', PARAM_TEXT);
$coordinator = optional_param('coordinator', '', PARAM_TEXT);

$search = " AND (user.firstname LIKE '%".trim($name)."%' 
            OR user.lastname LIKE '%".trim($name)."%' 
            OR CONCAT(user.firstname,' ',user.lastname) LIKE '%".$name."%'
            OR CONCAT(user.lastname,' ',user.firstname) LIKE '%".$name."%'
        ) AND user.phone2 LIKE '%".trim($phone)."%' AND user.name_ac LIKE '%".trim($coordinator)."%'
        ";

$search .= " AND user.schoolid LIKE '%".$schools."%'";
if(!empty($typeoflabor)){
    $search .= " AND user.typeoflabor LIKE '%".$typeoflabor."%'";
}

$teachers     = get_list_teacher($page,$perpage,$search);
$totalcount   = count(get_list_teacher(null,$perpage,$search));


$userid      = optional_param('id', 0, PARAM_INT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);

$moodle='teacher';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
?>



<?php  $i=$page*$perpage+1; foreach ($teachers as $teacher) { ?>
<tr>
    <td class="text-center"><?php echo $i; $i++; ?></td>
    <?php 
    if(!empty($hanhdong)||!empty($checkdelete)){
        echo'<td class="text-center hidden-print">';
            $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$teacher->id);
        if(!empty($checkdelete)){
            ?>
            <a onclick="return Confirm('Xóa giáo viên','Bạn có muốn xóa <?php echo $teacher->lastname .' '. $teacher->firstname ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=delete',array('id'=>$teacher->id)); ?>')" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            <?php 
        }
        echo'</td>';
    }
    ?>
    <td><?php echo $teacher->code ?></td>
    <td>
        <a href="" style="color: #000"><?php echo $teacher->lastname .' '. $teacher->firstname ?></a>
    </td>
    <td><?php
    if(!empty($teacher->profile_img)){
         $path1= $CFG->dirroot. '/account/logo/'.$teacher->profile_img;
        if( file_exists($path1)){
             echo '  <img src="'.$CFG->wwwroot.'/account/logo/'.$teacher->profile_img.'" style="width: 60px">';
        }else{
             echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
        }
    }else{
        echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
    } ?>
    </td>
    <td><?php echo $teacher->phone2 ?></td>
    <td><?php echo $teacher->email ?></td>
    <?php
        $schools = json_decode($teacher->schoolid);
        $rht = json_decode($teacher->name_rht);
    ?>
    <td>
        <?php foreach ($schools as $val) {
            $school = get_info_truong($val);
        ?>
            <?php echo $school->name ?><br>
        <?php }?>
    </td>
    <td>
       <?php 
            echo $teacher->typeoflabor==1 ? 'GVNN fulltime' : ''; 
            echo $teacher->typeoflabor==2 ? 'GVNN parttime' : '' ;
            echo $teacher->typeoflabor==3 ? 'GVNN cover' : '' ;
            echo $teacher->typeoflabor==4 ? 'GVNN share' : ''; 
       ?>
    </td>
    <td><?php foreach ($rht as $val) {
            $rht = get_info_giaovien($val);
        ?>
            <?= $rht->lastname.' '.$rht->firstname ?><br> 
        <?php }?>
    </td>
    <td><?php echo $teacher->name_ac  ?></td>

    
</tr>
                            
<?php } ?>

<tr class="active">
    <td colspan="11">
        <div class="float-left">
            <?php paginate($totalcount,$page,$perpage,"#");  ?>
        </div>
    </td>
</tr>

<script>
    $('a').on('click', function(event) {
        var x=$(this).attr('href');
        var page=x.replace('#page=', '');
        $.get("ajax.php?name="+$('#name').val()+"&phone="+$('#phone').val()+"&schools="+$('#schools').val()+"&manager="+$('#manager').val()+"&coordinator="+$('#coordinator').val()+"&typeoflabor="+$('#typeoflabor').val()+"&page="+page, function(data){
      $('#page').html(data);
    });
        $.get("ajax.php?name="+$('#name').val()+"&phone="+$('#phone').val()+"&schools="+$('#schools').val()+"&manager="+$('#manager').val()+"&coordinator="+$('#coordinator').val()+"&typeoflabor="+$('#typeoflabor').val()+"&page="+page,function(data){
      $('#page').html(data);
    }); 
    });
</script>