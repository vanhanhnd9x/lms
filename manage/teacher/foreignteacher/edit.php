<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../../config.php')) {
    header('Location: ../../install.php');
    die;
}
require('../../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/teacher/lib.php');
// require_once($CFG->dirroot . '/teams/config.php');
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
if($action=='view'){
  $PAGE->set_title(get_string('detail'));
  $PAGE->set_heading(get_string('detail'));

}else{
  $PAGE->set_title(get_string('editmyprofile'));
  $PAGE->set_heading(get_string('editmyprofile'));
}

global $USER;
$user_id = optional_param('user_id', '', PARAM_TEXT);
echo $OUTPUT->header();
$user = get_user_from_id($user_id);


if ($action == 'update_gvnn') {
    $user = get_user_from_id($user_id);
    $firstname = optional_param('firstname', '', PARAM_TEXT);
    $lastname = optional_param('lastname', '', PARAM_TEXT);
    $typeoflabor = optional_param('typeoflabor', '', PARAM_TEXT);
    // $code = optional_param('code', '', PARAM_TEXT);

    $country = optional_param('country', '', PARAM_TEXT);
    $name_rht = json_encode(optional_param('rht', '', PARAM_TEXT));
    $name_ac = optional_param('ac', '', PARAM_TEXT);
    $time_start = optional_param('start', '', PARAM_TEXT);
    $time_end = optional_param('end', '', PARAM_TEXT);
    $schoolid = json_encode(optional_param('schoolid', '', PARAM_TEXT));
    $email = optional_param('email', '', PARAM_TEXT);;

    $profile_img = upload_img('avatar', $CFG->dirroot.'/account/logo/');

    if(empty($profile_img)) {
      $profile_img = $user->profile_img;
    }
    
    $phone2 = optional_param('phone', '', PARAM_TEXT);
    $description = optional_param('description', '', PARAM_TEXT);


    switch ($role_id) {
        case 'admin':
            $role_id = 3;
            break;
        case 'learner':
            $role_id = 5;
            break;
        case 'teacher':
            $role_id = 8;
            break;
        case 'teachertutors':
            $role_id = 10;
            break;
        default:
            break;
    }

    
    $new_user_id = update_foreign_teacher($user_id, $firstname,$lastname,$typeoflabor,$schoolid,$email,$profile_img,$phone2,$country,$name_rht,$name_ac,$time_start,$time_end,$description);
    
    echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/teacher/foreignteacher/index.php");
}

$listschools = $DB->get_records_sql('SELECT * FROM schools where del=0');
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='teacher';
$namecheck='edit';
if($action=='view'){
  $namecheck='statsmodedetailed';
}
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$namecheck);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

?>

<script type="text/javascript" src="<?php print new moodle_url('/manage/people/js/validate_profile.js'); ?>"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form action="<?php echo $CFG->wwwroot ?>/manage/teacher/foreignteacher/edit.php?action=update_gvnn" onsubmit="return validate();" method="post" enctype="multipart/form-data">
            <div class="row">
              <input id="user_id" name="user_id" type="hidden" value="<?php echo $user_id; ?>">

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('lastname')) ?><span class="text-danger">*</span></label>
                
                  <input class="form-control" id="lastname" name="lastname" type="text" value="<?php echo $user->lastname  ?>" required>
                  <span id="erLast" class="text-danger" style="display: none;">
                      <?php print_string('enter_lastname') ?>   
                  </span>
                
              </div>
            
              <div class="form-group col-md-6">
                  <label class="col-form-label"><?php print_r(get_string('firstname')) ?> <span class="text-danger">*</span></label>
                  
                    <input class="form-control" id="firstname" name="firstname" type="text" value="<?php echo $user->firstname ?>" required>
                    <span id="erFirst" class="text-danger" style="display: none;">
                        <?php print_string('enter_firstname') ?>
                    </span>
                  
              </div>

              
              
              <div class="form-group col-md-6">
                  <label class="col-form-label"><?php print_r(get_string('codeteacher')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="magv" name="code" type="text" value="<?php echo $user->code  ?>" disabled>
              </div>
              <div class="form-group col-md-6">
                <label><?php echo get_string('typeoflabor') ?></label>
                <select id="typeoflabor" name="typeoflabor" class="form-control">
                    <option value="">--None--</option>
                    <option value="1" <?php echo $user->typeoflabor==1 ? 'selected' : '' ?>>GVNN fulltime</option>
                    <option value="2" <?php echo $user->typeoflabor==2 ? 'selected' : '' ?>>GVNN parttime</option>
                    <option value="3" <?php echo $user->typeoflabor==3 ? 'selected' : '' ?>>GVNN cover</option>
                    <option value="4" <?php echo $user->typeoflabor==4 ? 'selected' : '' ?>>GVNN share</option>
                </select> 
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('yourlogo')) ?></label> <br>
                  <input type="file" id="file" class="btn-light" name="avatar"/><br>
                  <div id="er_img"></div>
                  <img src="<?php echo $CFG->wwwroot.'/account/logo/'.$user->profile_img ?>" alt="" width="100px">
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('email_new')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="email" name="email" type="text" value="<?php echo $user->email  ?>" required>
                  <span id="erEmail" class="text-danger" style="display: none;"><?php print_string('enter_emailid') ?></span>
              </div>

              <div class="form-group col-md-6">
                  <label class="col-form-label"><?php print_r(get_string('phone')) ?> </label>
                  <input class="form-control" id="phone" name="phone" type="text" value="<?php echo $user->phone2  ?>">
                  <span id="er_phone" class="text-danger"></span>
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('schools')) ?></label>
                  
                  <select name="schoolid[]" id="schoolid" class="form-control multiple" multiple data-size="7">
                    <?php 
                    $schoool = json_decode($user->schoolid);
                    foreach ($listschools as $val) { ?>
                        <option value="<?php echo $val->id ?>" <?php  foreach ($schoool as $sch) { ?>
                                                                <?php  echo $sch == $val->id ? 'selected' : ''; }?> >
                            <?php echo $val->name ?>
                        </option>
                    <?php }?>
                  </select>
              </div>


              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('nationality')) ?></label>
                <input class="form-control" type="text" name="country" placeholder="Nhập quốc tịch..." value="<?php echo $user->country  ?>">
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('manager')) ?> (RHT)</label>
                  <?php $taacrhta = get_list_acrht(); $name_rht = json_decode($user->name_rht); ?>
                  <select name="rht[]" id="rht" class="form-control multiple" multiple>
                    <option value="" disabled>--Chọn người quản lý--</option>
                    <?php foreach ($taacrhta as $rhta) { ?>
                        <option value="<?php echo $rhta->id ?>" <?php  foreach ($name_rht as $val) { ?> 
                                                                 <?php  echo $val == $rhta->id ? 'selected' : ''; }?> >
                          <?php echo $rhta->lastname.' '.$rhta->firstname ?>
                        </option>
                    <?php } ?>
                  </select>
              </div>
              
              <div class="col-md-12">
                <div class="row">
                  <div class="form-group col-md-6">
                    <label class="col-form-label"><?php print_r(get_string('coordinator')) ?> (AC)</label>
                    <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $user->name_ac ?>">
                  </div>
                </div>
              </div>
            

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('startdate')) ?></label>
                  
                      <input class="form-control datepicker"  name="start" id="date_start" type="text" placeholder="dd/mm/yyyy" value="<?php echo $user->time_start  ?>">
                  
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('endate')) ?></label>
                      <input class="form-control datepicker" name="end" id="date_end" type="text" placeholder="dd/mm/yyyy" value="<?php echo $user->time_end  ?>">
              </div>

              <div class="form-group col-md-12">
                <label class="col-form-label"><?php print_r(get_string('description')) ?></label>
                    <textarea name="description" class="form-control"><?php echo $user->description  ?></textarea>
              </div>



              <hr>
              <div class="form-group">
                <div class="col-md-12">
                  <?php 
                    if($action=='view'){
                      ?>
                       <a href="<?php echo $CFG->wwwroot . '/manage/teacher/foreignteacher/index.php' ?>" class="btn btn-success"> 
                        <?php print_r(get_string('back')) ?>
                      </a>
                      <?php 
                    }else{
                      ?>
                       <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                      <?php print_r(get_string('or')) ?> 
                      <a href="<?php echo $CFG->wwwroot . '/manage/teacher/foreignteacher/index.php' ?>" class="btn btn-danger"> 
                        <?php print_r(get_string('cancel')) ?>
                      </a>
                      <?php 
                    }
                   ?>
                 
                </div>
              </div>
            </div>
        </form>   
      </div>
  </div>
</div>

<?php
    echo $OUTPUT->footer();
?>

<script type="text/javascript">
$('#file').change(function(event) {
    var er=0;
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      // lay dung luong va kieu file tu the input file
        if($('#file').get(0).files.length==0){
          $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Chọn ảnh đại diện</div>');
          er=1;
        }else{
          var fname = $('#file')[0].files[0].name;
          var fsize = $('#file')[0].files[0].size;
          var ftype = $('#file')[0].files[0].type;

          if (ftype=='image/png' || ftype=='image/gif' || ftype=='image/jpeg' || ftype=='image/pjpeg') {
            
            if (fsize > <?php echo get_real_size(ini_get('upload_max_filesize')) ?>) {
              
              $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Dung lượng file < <?php echo ini_get('upload_max_filesize') ?></div>');
              er=1;
              
            }else{
              $("#er_img").html('<div class="alert alert-success" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>File hợp lệ</div>');
              er=0;
            }
          }else{
            $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Không đúng định dạng ảnh</div>');
            er=1;
          }

        }
         
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    }

    if(er==1){
      $("#saveUser").attr('disabled','disabled');
    }else{
      $("#saveUser").removeAttr('disabled');
    }
});
</script>
<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css'>
<script src='//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script type="text/javascript">
      $("#date_start").datepicker({
        minDate: new Date(25, 10 - 1,2000 ),
        maxDate: "",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_end").datepicker("option","minDate", selected)
        },
         disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
      $("#date_end").datepicker({
        minDate: 0,
        maxDate:"",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_start").datepicker("option","maxDate", selected)
        },
         disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
      jQuery(function($){
        $.datepicker.regional['vi'] = {
          closeText: 'Đóng',
          prevText: '<Trước',
          nextText: 'Tiếp>',
          currentText: 'Hôm nay',
          monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
          'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
          monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
          'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
          dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
          dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
          dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
          weekHeader: 'Tu',
          dateFormat: 'dd/mm/yy',
          firstDay: 0,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''};
          $.datepicker.setDefaults($.datepicker.regional['vi']);
        });
</script>
