<?php
if (!file_exists('../../../config.php')) {
  header('Location: ../../install.php');
  die;
}

require('../../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');

require_login();
$PAGE->set_title(get_string('listforeignteacher'));
$PAGE->set_heading(get_string('listforeignteacher'));
echo $OUTPUT->header();


//List GVNN
$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 20, PARAM_INT);
$search       = optional_param('search', '', PARAM_TEXT);
$param = " AND (firstname LIKE '%".trim($search)."%' 
            OR lastname LIKE '%".trim($search)."%' 
            OR email LIKE '%".trim($search)."%' 
            OR phone2 LIKE '%".trim($search)."%' 
            OR CONCAT(firstname,' ',lastname) LIKE '%".$search."%'
            OR CONCAT(lastname,' ',firstname) LIKE '%".$search."%'
        )";
$teachers     = get_list_teacher($page,$perpage,$param);
// var_dump(count($teachers));die;

$totalcount   = count(get_list_teacher(null,$perpage,$param));


$userid      = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
if($action=='delete'){
    $DB->delete_records('user',array('id' => $userid));
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/teacher/foreignteacher");
}

$schools = $DB->get_records('schools', array('del'=>0));

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='teacher';
$namecheck='foreignteacher';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$namecheck);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <?php 
                        if(!empty($checkthemmoi)){
                            ?>
                            <div class="col-md-2">
                                <a href="<?php echo new moodle_url('/manage/teacher/foreignteacher/new.php'); ?>" class="btn btn-success"><?php print_r(get_string('new')) ?></a>
                            </div>
                            <?php 
                        }
                     ?>
                    
                    <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('name') ?></label>
                                    <input type="text" id="name" class="saj form-control" placeholder="<?php echo get_string('name') ?>..."> 
                                </div>
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('phone') ?></label>
                                   <input type="text" id="phone" class="saj form-control" placeholder="<?php echo get_string('phone') ?>...">   
                                </div>
                                
                                <div class="col-md-4 form-group">
                                    <label><?php echo get_string('schools') ?></label>
                                    <select type="text" id="schools" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                        <option value="">--None--</option>
                                        <?php 
                                          foreach ($schools as $vlu) {
                                            echo '<option value="'.$vlu->id.'">'.$vlu->name.'</option>';
                                          }
                                        ?>
                                        </select> 
                                </div>
                                <?php 
                                    if($roleid!=10){
                                        ?>
                                        <div class="col-md-4 form-group">
                                            <label><?php echo get_string('typeoflabor') ?></label>
                                            <select id="typeoflabor" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                                <option value="">--None--</option>
                                                <option value="1">GVNN fulltime</option>
                                                <option value="2">GVNN parttime</option>
                                                <option value="3">GVNN cover</option>
                                                <option value="4">GVNN share</option>
                                            </select> 
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label><?php echo get_string('manager') ?></label>
                                            <input type="text" id="manager" class="saj form-control" placeholder="<?php echo get_string('manager') ?>..."> 
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label><?php echo get_string('coordinator') ?></label>
                                            <input type="text" id="coordinator" class="saj form-control" placeholder="<?php echo get_string('coordinator') ?>..."> 
                                        </div>
                                        <?php 
                                    }
                                ?>
                                
                            </div>
                    </div>
                </div>
                <?php if(!empty($totalcount)){ ?>
                <div class="table-responsive" data-pattern="priority-columns">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center align-middle">STT</th>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete))
                                    {
                                        echo'<th class="align-middle text-center">'.get_string('action').'</th>';
                                    }
                                ?>
                                <th><?php print_r(get_string('codeteacher')); ?></th>
                                <th><?php print_r(get_string('name')); ?></th>
                                <th class="text-center align-middle">Avatar</th>
                                <th><?php print_r(get_string('phone')); ?> </th>
                                <th><?php print_r(get_string('email_new')); ?></th>
                                <th><?php print_r(get_string('schools')); ?></th>
                                <?php 
                                    if($roleid!=10){
                                        echo'
                                            <th>'.get_string('typeoflabor').'</th>
                                            <th>'.get_string('manager').'</th>
                                            <th>'.get_string('coordinator').'</th>
                                        ';
                                    }
                                 ?>
                            </tr>
                        </thead>
                        <tbody id="page">
                            <?php $stt=$page*$perpage+1; foreach ($teachers as $teacher) {?>
                            <tr>
                                <td class="text-center"><?php echo $stt++; ?></td>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo'<td class="text-center hidden-print">';
                                         $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$teacher->id);
                                        if(!empty($checkdelete)){
                                           ?>
                                            <a onclick="return Confirm('Xóa giáo viên','Bạn có muốn xóa <?php echo $teacher->lastname .' '. $teacher->firstname ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=delete',array('id'=>$teacher->id)); ?>')" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                           <?php 
                                        }
                                        echo'</td>';
                                    }
                                ?>
                                <td><?php echo $teacher->code ?></td>
                                <td><a href="" style="color: #000">
                                    <?php echo $teacher->lastname .' '. $teacher->firstname ?></a>
                                </td>
                                <td><?php
                                    if(!empty($teacher->profile_img)){
                                      $path1= $CFG->dirroot. '/account/logo/'.$teacher->profile_img;
                                      if( file_exists($path1)){
                                          echo '  <img src="'.$CFG->wwwroot.'/account/logo/'.$teacher->profile_img.'" style="width: 60px">';
                                      }else{
                                           echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
                                      }
                                    }else{
                                        echo '<img src="'.$CFG->wwwroot.'/account/logo/avata.jpg" style="width: 60px">';
                                    } ?>
                                </td>
                                <td><?php echo $teacher->phone2 ?></td>
                                <td><?php echo $teacher->email ?></td>
                                <?php
                                    $schools = json_decode($teacher->schoolid);
                                    $rht = json_decode($teacher->name_rht);
                                    
                                ?>
                                <td><?php foreach ($schools as $val) {
                                        $school = get_info_truong($val);
                                        ?>
                                        <?php echo $school->name ?><br>
                                   <?php }?>
                               </td>
                               <?php 
                                    if($roleid!=10){
                                        ?>
                                         <td>
                                           <?php 
                                                echo $teacher->typeoflabor==1 ? 'GVNN fulltime' : ''; 
                                                echo $teacher->typeoflabor==2 ? 'GVNN parttime' : '' ;
                                                echo $teacher->typeoflabor==3 ? 'GVNN cover' : '' ;
                                                echo $teacher->typeoflabor==4 ? 'GVNN share' : ''; 
                                           ?>
                                        </td>
                                        <td><?php foreach ($rht as $val) {
                                                $rht = get_info_giaovien($val);
                                            ?>
                                                <?= $rht->lastname.' '.$rht->firstname ?><br> 
                                            <?php }?>
                                        </td>
                                        <td><?php echo $teacher->name_ac  ?></td>
                                        <?php 
                                    }
                                ?>
                               
                            </tr>
                            <?php } ?>
                            
                                <tr>
                                    <td colspan="11">
                                        <div class="float-left">
                                            <?php
                                                if($search==""){
                                                    $url ="index.php?";
                                                }else{
                                                    $url ="index.php?search=".$search.'&';
                                                } 
                                                
                                                paginate($totalcount,$page,$perpage,$url); 
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            
                        </tbody>
                    </table>
                </div>
                <?php }else{ ?>
                    <h4 class="text-danger">Không có bản ghi!</h4>
                <?php } ?>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
    echo $OUTPUT->footer();
?>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<script>
    $('.saj').keyup(function() {
        $.get("ajax.php?name="+$('#name').val()+"&phone="+$('#phone').val()+"&schools="+$('#schools').val()+"&manager="+$('#manager').val()+"&coordinator="+$('#coordinator').val()+"&typeoflabor="+$('#typeoflabor').val(),function(data){
            $('#page').html(data);
        });
    });

    $('#schools').change(function() {
        $.get("ajax.php?name="+$('#name').val()+"&phone="+$('#phone').val()+"&schools="+$('#schools').val()+"&manager="+$('#manager').val()+"&coordinator="+$('#coordinator').val()+"&typeoflabor="+$('#typeoflabor').val(),function(data){
            $('#page').html(data);
        });
    });
    $('#typeoflabor').change(function() {
        $.get("ajax.php?name="+$('#name').val()+"&phone="+$('#phone').val()+"&schools="+$('#schools').val()+"&manager="+$('#manager').val()+"&coordinator="+$('#coordinator').val()+"&typeoflabor="+$('#typeoflabor').val(),function(data){
            $('#page').html(data);
        });
    });    
     
</script>