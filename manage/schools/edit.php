<?php 
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('updateschools'));
echo $OUTPUT->header();
$schools = get_schools(optional_param('sch_id', '', PARAM_TEXT));
// $city = get_city();
$district = get_district($schools->city);
$commune = get_commune($schools->district);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schools';
$name1='updateschools';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$action = optional_param('action', '', PARAM_TEXT);
$url=$CFG->dirroot . '/manage/schools/img/';

if ($action == 'update') {
  
 
  $id = optional_param('id', '', PARAM_TEXT);
  // $ha1 = upload_img('ha1', $CFG->dirroot.'/manage/schools/img/');
  // $ha2 = upload_img('ha2', $CFG->dirroot.'/manage/schools/img/');
  // if (empty($ha1)) {
  //   $ha1= get_schools($id)->img1;
  // }
  // if (empty($ha2)) {
  //   $ha2= get_schools($id)->img2;
  // }
  if(!empty($_FILES['ha1']['name'])){
    if($_FILES['ha1']['name']!=$schools->img1){
      $ha1 = upload_img('ha1', $CFG->dirroot.'/manage/schools/img/');
      unlink($url.$schools->img1);
    }else{
      $ha1= get_schools($id)->img1;
    }
  }else{
     $ha1= get_schools($id)->img1;
  }
  if(!empty($_FILES['ha2']['name'])){
    if($_FILES['ha2']['name']!=$schools->img2){
      $ha2 = upload_img('ha2', $CFG->dirroot.'/manage/schools/img/');
      unlink($url.$schools->img2);
    }else{
       $ha2= get_schools($id)->img2;
    }

  }else{
      $ha2= get_schools($id)->img2;
  }
  $arr = array(
    'id' => $id,
    'name' => trim(optional_param('schoolsname', '', PARAM_TEXT)),
    'address' => trim(optional_param('address', '', PARAM_TEXT)),
    'city' => 1,
    'district' => trim(optional_param('quanhuyen', '', PARAM_TEXT)),
    'commune' => trim(optional_param('xaphuong', '', PARAM_TEXT)),
    // 'phone' => trim(optional_param('phone', '', PARAM_TEXT)),
    'email' => trim(optional_param('email', '', PARAM_TEXT)),
    'representative_name' => trim(optional_param('representative_name', '', PARAM_TEXT)),
    'birthday' => trim(optional_param('birthday', '', PARAM_TEXT)),
    // 'Representative_address' => trim(optional_param('representative_address', '', PARAM_TEXT)),
    'Representative_email' => trim(optional_param('representative_email', '', PARAM_TEXT)),
    'Representative_phone' => trim(optional_param('representative_phone', '', PARAM_TEXT)),
    'deputy' => trim(optional_param('deputy', '', PARAM_TEXT)),
    'deputy_phone' => trim(optional_param('deputy_phone', '', PARAM_TEXT)),
    // 'deputy_addr' => trim(optional_param('deputy_addr', '', PARAM_TEXT)),
    'deputy_mail' => trim(optional_param('deputy_mail', '', PARAM_TEXT)),
    'deputy_bir' => trim(optional_param('deputy_bir', '', PARAM_TEXT)),
    'note' => trim(optional_param('note', '', PARAM_TEXT)),
    'deputy2' => trim(optional_param('deputy2', '', PARAM_TEXT)),
    'deputy2_phone' => trim(optional_param('deputy2_phone', '', PARAM_TEXT)),
    // 'deputy2_addr' => trim(optional_param('deputy2_addr', '', PARAM_TEXT)),
    'deputy2_mail' => trim(optional_param('deputy2_mail', '', PARAM_TEXT)),
    'deputy2_bir' => trim(optional_param('deputy2_bir', '', PARAM_TEXT)),
    'img1' => $ha1,
    'img2' => $ha2,
    'time_update' => date('h:i, d-m-y',time()),
    'duration' => trim(optional_param('duration', '', PARAM_TEXT)),
    'yearlink' => trim(optional_param('yearlink', '', PARAM_TEXT)),
    'level' => trim(optional_param('level', '', PARAM_INT)),
    'evaluation_student' => trim(optional_param('evaluation_student', '', PARAM_INT))
  );
  update_schools($arr);
  $schools = get_schools($id);
  add_logs('schools', 'update', '/manage/schools/edit.php?sch_id='.$schools->id, $schools->name);
  echo displayJsAlert(get_string('updatesuccess'), $CFG->wwwroot . "/manage/schools");
}
 ?>

  
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<style type="text/css">
  label.error{
    color: #f1556c;
  }
</style>
<div class="row">
  <div class="col-md-12">

    <div class="card-box">
        <div class="card-content">
            <br>
            <div class="material-datatables" style="clear: both;">
              <form  action="<?php echo $CFG->wwwroot ?>/manage/schools/edit.php?action=update" id="formAdd" method="post"  enctype="multipart/form-data">
                <input type="text" name="id" value="<?php echo $schools->id ?>" hidden>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('nameschools'); ?><span style="color:red">*</span></label>
                        <input class="form-control" value="<?php echo $schools->name ?>" type="text" name="schoolsname" id="schoolsname">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('addrschools'); ?></label>
                        <input class="form-control" value="<?php echo $schools->address ?>" type="text" name="address">
                    </div>
                  </div>
                </div>

                <div class="row">
                  
                  <div class="col-md-3">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('district'); ?></label>
                        <select name="quanhuyen" id="quanHuyenChange" class="form-control">
                          <option value="-1">--None--</option>
                          <?php 
                            foreach ($district as $val) {
                              if ($val->id == $schools->district) {
                                echo '<option selected value="'.$val->id.'">'.$val->name.'</option>';
                              }else
                                echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                            }
                          ?>

                        </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('commune'); ?> </label>
                        <select name="xaphuong" id="xaPhuongChange" class="form-control">
                          <?php 
                            foreach ($commune as $val) {
                              if ($val->id == $schools->commune) {
                                echo '<option selected value="'.$val->id.'">'.$val->name.'</option>';
                              }else
                                echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('emailschools'); ?></label>
                        <input class="form-control" value="<?php echo $schools->email ?>" type="text" name="email" id="email">
                    </div>
                  </div>
                </div><br><br>

                <div class="row">
                  <div class="col-md-6">
                    <!-- <img height="100px" src="<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img1 ?>"> -->
                    <!-- <a href="<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img1 ?>" target="_blank" title="Xem bản đồ" rel="follow, index">Xem bản đồ</a> -->
                   
                    
                    <div class="form-group label-floating">
                      <label class="control-label">
                        <?php echo get_string('mapschools'); ?>
                        </label>
                         <?php 
                            if(!empty($schools->img1)){
                              ?>
                              <a style="color: blue;" onclick="window.open('<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img1; ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                              </a>
                              <?php 
                            }
                           ?>
                      <div>
                        <input class="form-control file" type="file" id="ha1" name="ha1">
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <!-- <img height="100px" src="<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img2 ?>" alt="Thời khóa biểu"> -->
                    <!-- <a href="<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img2 ?>" target="_blank" title="Xem thời khóa biểu" rel="follow, index">Xem thời khóa biểu</a> -->
                     
                    <div class="form-group label-floating">
                      <label class="control-label">
                        <?php echo get_string('tkb'); ?>
                      </label>
                      <?php 
                      if(!empty($schools->img2)){
                        ?>
                        <a style="color: blue;" onclick="window.open('<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img2; ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        <?php 
                      }
                     ?>
                      <div>
                        <input class="form-control file" type="file" id="ha2" name="ha2">
                        <div></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('yearlink') ?></label>
                      <div>
                        <input class="form-control " type="number" name="yearlink" id="" value="<?php echo $schools->yearlink ?>">
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('classlink') ?></label>
                      <div>
                        <input class="form-control " type="number" name="duration" id="" value="<?php echo $schools->duration ?>">
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('level') ?></label>
                      <div>
                        <select name="level" id="" class="form-control">
                          <option <?php echo $schools->level == 1 ? 'selected' : '' ?> value="1"><?php echo get_string('level1'); ?></option>
                          <option <?php echo $schools->level == 2 ? 'selected' : '' ?> value="2"><?php echo get_string('level2'); ?></option>
                          <!-- <option <?php echo $schools->level == 3 ? 'selected' : '' ?> value="3"><?php echo get_string('level3'); ?></option> -->
                        </select>
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <!-- trang edit -->
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('evaluation_student') ?></label>
                        <select name="evaluation_student" id="" class="form-control">
                          <option value="1"><?php echo get_string('yes'); ?></option>
                          <option value="2" <?php echo $schools->evaluation_student == 2 ? 'selected' : '' ?>><?php echo get_string('no'); ?></option>
                        </select>
                    </div>
                  </div>
                </div>
                <hr style="border: 1px solid #dddccc">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('principal'); ?></label>
                        <input class="form-control" value="<?php echo $schools->representative_name ?>" type="text" name="representative_name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('birthday'); ?></label>
                        <input class="datepicker form-control" value="<?php echo $schools->birthday ?>" type="text" placeholder="dd/mm/yy"  name="birthday">
                    </div>
                  </div>                  
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('phone'); ?></label>
                        <input class="form-control" value="<?php echo $schools->representative_phone ?>" type="text" name="representative_phone">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label">Email</label>
                        <input class="form-control" type="text" name="representative_email" value="<?php echo $schools->representative_email ?>" id="representative_email">
                        
                    </div>
                  </div>
                </div>
                <hr style="border: 1px solid #dddccc">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('deputy'); ?> 1</label>
                        <input class="form-control" type="text" name="deputy"  value="<?php echo $schools->deputy ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?></label>
                        <input class="datepicker form-control" type="text" placeholder="dd/mm/yy"  name="deputy_bir"  value="<?php echo $schools->deputy_bir ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('phone'); ?></label>
                        <input class="form-control" type="text" name="deputy_phone"  value="<?php echo $schools->deputy_phone ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label>Email</label>
                        <input class="form-control" type="text" name="deputy_mail"  value="<?php echo $schools->deputy_mail ?>" id="deputy_mail">
                        
                    </div>
                  </div>
                </div><hr>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('deputy'); ?> 2</label>
                        <input class="form-control" type="text" name="deputy2"  value="<?php echo $schools->deputy2 ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?></label>
                        <input class="datepicker form-control" type="text" placeholder="dd/mm/yy"  name="deputy2_bir"  value="<?php echo $schools->deputy2_bir ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('phone'); ?></label>
                        <input class="form-control" type="text" name="deputy2_phone"  value="<?php echo $schools->deputy2_phone ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label>Email</label>
                        <input class="form-control" type="text" name="deputy2_mail" id="deputy2_mail"  value="<?php echo $schools->deputy2_mail ?>">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('noteschools'); ?></label>
                      <br>
                      <textarea name="note" id="note" cols="30" rows="10" class="form-control">
                        <?php echo $schools->note ?>
                      </textarea>
                      <script>CKEDITOR.replace('note');</script>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-buttons" style="border-top: none !important">
                      <input id="btnSub" type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
                      <?php print_r(get_string('or')) ?>
                      <a href="<?php echo $CFG->wwwroot ?>/manage/schools/" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                    </div>
                  </div>
                </div>

                

              </form>
            </div>
        <!-- end content-->
    </div>
      <!--  end card  -->
  </div>
    <!-- end col-md-12 -->
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
  

<?php 
  echo $OUTPUT->footer(); 
?>
<?php include_once 'validator.php'; ?>