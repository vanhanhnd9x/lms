 <?php
require("../../config.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');

require_login(0, false);
$PAGE->set_title(get_string('schools'));
echo $OUTPUT->header();
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '20', PARAM_INT); // how many per page

$key         = trim(optional_param('search', '', PARAM_TEXT));
$qh          = optional_param('district', '', PARAM_INT);
if (empty($key)) {
    $param= "`district`=".$qh;
}
if (empty($qh)) {
    $param= "(`name` LIKE '%".$key."%' OR `representative_name` LIKE '%".$key."%' OR `email` LIKE '%".$key."%' OR `address` LIKE '%".$key."%')";
}
if (!empty($key) && !empty($qh)) {
    $param= " `district`=".$qh." AND(`name` LIKE '%".$key."%' OR `representative_name` LIKE '%".$key."%' OR `email` LIKE '%".$key."%' OR `address` LIKE '%".$key."%')";
}

$schools = paging_get_schools($page, $perpage, $param);
// var_dump($schools);
$city = get_city();
$district = get_district(1);
$action = optional_param('action', '', PARAM_TEXT);
if($action == 'del'){
    $schoolid = optional_param('sch_id', '', PARAM_TEXT);
    if (!empty(get_block($schoolid))) {
        echo '<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>'.get_string('warning').'</strong>'.get_string('truongtontaikhoi').'
  </div>';
    }else{
        $schools = get_schools($schoolid)->name;
        del_schools(optional_param('sch_id', '', PARAM_TEXT));
        add_logs('schools', 'delete', '', $schools);
        echo displayJsAlert(get_string('deletedsucessfully'), $CFG->wwwroot . "/manage/schools");
    }
}
if($action == 'del_list'){
    $list = optional_param('list', '', PARAM_TEXT);
    if (empty($list)) {
        echo displayJsAlert(get_string('selectdel'), $CFG->wwwroot . "/manage/schools");
    }else{
        $dl = 0;
        foreach ($list as $key => $value) {
            if (!empty(get_block($value))) {
                $dl = 1;
            }
        }
         if ($dl==1) {
            echo '<div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>'.get_string('warning').'</strong>'.get_string('truongtontaikhoi').'
                </div>';
        }else{
            del_list($list);
            echo displayJsAlert(get_string('deletedsucessfully'), $CFG->wwwroot . "/manage/schools");
        }
    }
}
// phan quyen
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schools';
$name1='listschools';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
// add_to_log($course->id, 'course', 'view', "view.php?id=$course->id", "$course->id");
// if(is_teacher() || is_admin()){
?>
<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mr_bottom15 hidden-print">
                        <?php 
                            if(!empty($checkthemmoi)){
                                ?>
                                 <div class="col-md-2">
                                    <a href="<?php echo new moodle_url('/manage/schools/new.php'); ?>" class="btn btn-success"><?php echo get_string('add') ?></a>
                                </div>
                                <?php 
                            }
                        ?>
                       

                        <!-- tìm kiếm submit -->
                         <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="schoolsname"><?php echo get_string('nameschools') ?></label>
                                    <input type="text" id="schoolsname" class="saj form-control" placeholder="<?php echo get_string('nameschools') ?>...">
                                    </div>
                                    <div class="form-group">
                                        <label for="ht"><?php echo get_string('principal') ?></label>
                                    <input type="text" id="ht" class="saj form-control" placeholder="<?php echo get_string('principal') ?>...">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                    <input type="text" id="email" class="saj form-control" placeholder="Email...">

                                    </div>
                                    <div class="form-group">
                                        <label for="addr"><?php echo get_string('addrschools') ?></label>
                                      <select type="text" id="addr" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                        <option value="">--None--</option>
                                        <?php 
                                          foreach ($district as $vlu) {
                                            if ($vlu->id == $qh) {
                                                echo '<option selected value="'.$vlu->id.'">'.$vlu->name.'</option>';
                                            }else
                                                echo '<option value="'.$vlu->id.'">'.$vlu->name.'</option>';
                                          }
                                        ?>
                                      </select>
                                    </div>
                                </div>
                            </div>                      

                            <!-- <form action="" class="row form-group" method="get">
                                <div class="col-md-6">
                                    <input type="text" name="search" class="form-control" placeholder="<?php echo get_string('search'); ?>" value="<?php echo $key ?>">
                                </div>
                                <div class="col-md-3 hidden-print">
                                  <select name="district" class="form-control">
                                    <option value=""><?php echo get_string('district') ?> </option>
                                    <?php 
                                    foreach ($district as $vlu) {
                                      if ($vlu->id == $qh) {
                                          echo '<option selected value="'.$vlu->id.'">'.$vlu->name.'</option>';
                                      }else
                                          echo '<option value="'.$vlu->id.'">'.$vlu->name.'</option>';
                                    }
                                     ?>
                                  </select>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" id="search" class="btn btn-info"><?php echo get_string('search'); ?></button>
                                </div>
                            </form> -->
                        </div>
                    </div>
                    <?php if (!empty($schools)): ?>
                    <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
                        
                    <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
                        <table class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <?php 
                                if(!empty($checkdelete)){
                                    echo '<td class="hidden-print"><input type="checkbox" name="" id="checkAll"></td>';
                                }
                             ?>
                            <th class="align-middle text-center">STT</th>
                            <?php 
                                if(!empty($hanhdong)||!empty($checkdelete)){
                                    echo'<th class="align-middle text-center">'.get_string('action').'</th>';
                                }
                            ?>
                            <th><?php echo get_string('nameschools') ?></th>
                            <th><?php echo get_string('principal') ?></th>
                            <th>Email</th>
                            <th><?php echo get_string('addrschools') ?></th>
                           
                            
                          </tr>
                        </thead>
                            <tbody id="page">
                                <?php $stt=$page*$perpage+1; foreach ($schools as $sch) { 
                                    if (true) {
                                    ?>
                                <tr>
                                    <?php 
                                        if(!empty($checkdelete)){
                                            echo '  <td class="hidden-print"><input type="checkbox" name="list[]" class="list" value="'.$sch->id.'"></td>';
                                        }
                                     ?>
                                  
                                    <td class="text-center"><?php echo $stt++; ?></td>
                                    <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo'<td class="text-center hidden-print">';
                                         $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$sch->id);
                                        if(!empty($checkdelete)){
                                           ?>
                                           <a onclick="return check_del()" href="<?php print new moodle_url('?action=del',array('sch_id'=>$sch->id)); ?>" class="mauicon" title='<?php echo get_string('delete') ?>'><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                           <?php 
                                        }
                                        echo'</td>';
                                    }
                                     ?>
                                    <td><?php echo $sch->name ?></td>
                                    <td><?php echo $sch->representative_name ?></td>
                                    <td><?php echo $sch->email ?></td>
                                    <td><?php echo get_addr($sch->id) ?></td>
                                    
                                    <!-- <td class="text-right hidden-print">
                                        <a href="<?php print new moodle_url('edit.php',array('sch_id'=>$sch->id)); ?>" class="btn btn-info btn-sm tooltip-animation" title='<?php echo get_string('update') ?>'><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="<?php print new moodle_url('detail.php',array('sch_id'=>$sch->id)); ?>" class="btn btn-warning btn-sm tooltip-animation" title='<?php echo get_string('detail') ?>'><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a onclick="return check_del()" href="<?php print new moodle_url('?action=del',array('sch_id'=>$sch->id)); ?>" class="btn btn-danger btn-sm tooltip-animation" title='<?php echo get_string('delete') ?>'><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td> -->
                                </tr>
                                <?php }} ?>
                                <tr id="dellist" style="display: none">
                                    <th colspan="7"><input type="submit" class="btn btn-simple btn-danger btn-sm btn-icon remove hidden-print" value="<?php echo get_string('delete') ?>" onclick="return check_del()"></th>
                                </tr>
                                <tr>
                                    <td colspan="7">
                                    <?php 
                                    global $OUTPUT;
                                    $param = ' del=0 AND'.$param;
                                    $totalcount = get_count_records('schools', $param);
                                    // var_dump($totalcount);
                                    if ($key!='' && $qh!='') {
                                        $url = "index.php?search=".$key."&district=".$qh."&";
                                    }else{
                                        if ($key=='') {
                                            $url = "index.php?district=".$qh."&";
                                        }
                                        if ($qh=='' || $qh==0) {
                                            $url = "index.php?search=".$key."&";
                                        }
                                        if ($key=='' && $qh=='') {
                                            $url = "index.php?";
                                        }
                                    }
                                    paginate($totalcount, $page, $perpage, $url);
                                     ?>
                                     </td>
                                 </tr>
                            </tbody>
                        </table>
                    </form>
                    </div>
                    <?php else: ?>
                        <?php if (empty($key)): ?>
                          <span class="text-danger">Không có bản ghi!</span>
                        <p>(<a href="<?php echo $CFG->wwwroot ?>/manage/schools"><?php echo get_string('back') ?></a>)</p>    
                        <?php else: ?>
                            
                        <span class="text-danger"><h5><?php echo get_string('searchnoresultkeyword') ?> <b>"<?php echo $key ?>!"</b></h5></span>
                        <p>(<a href="<?php echo $CFG->wwwroot ?>/manage/schools"><?php echo get_string('back') ?></a>)</p>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>
            <!-- end content-->
        
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<?php
// }else
    // echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot);
    echo $OUTPUT->footer();
?>

<script>
    $("#checkAll").click(function () {
        $(".list").prop('checked', $(this).prop('checked'));
    });
    function check_del(){
        return confirm("<?php echo get_string('doyouwantdel') ?>");
    }
</script>
<script>
  $(document).ready(function() {
    $(":checkbox").click(function(event) {
      if ($('.list').is(":checked"))
          $("#dellist").show();
      else
          $("#dellist").hide();
    });
  });
</script>

<script>
  $('.saj').keyup(function() {
    $.get('ajaxsearch.php?schoolsname='+$('#schoolsname').val()+'&ht='+$('#ht').val()+'&email='+$('#email').val()+'&addr='+$('#addr').val(), function(data){
      $('#page').html(data);
    });
  });
  $('#addr').change(function() {
    $.get('ajaxsearch.php?schoolsname='+$('#schoolsname').val()+'&ht='+$('#ht').val()+'&email='+$('#email').val()+'&addr='+$('#addr').val(), function(data){
      $('#page').html(data);
    });
  });
</script>