<?php 
require("../../config.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/common/header_lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '20', PARAM_INT); // how many per page

$schoolsname = trim(optional_param('schoolsname', '', PARAM_TEXT));
$ht = trim(optional_param('ht', '', PARAM_TEXT));
$email = trim(optional_param('email', '', PARAM_TEXT));
$addr = trim(optional_param('addr', '', PARAM_INT));

$param = '1 ';

if(!empty($schoolsname)){
	$param .= 'AND `name` LIKE "%'.$schoolsname.'%" ';
}

if(!empty($ht)){
	$param .= 'AND `representative_name` LIKE "%'.$ht.'%" ';
}

if(!empty($email)){
	$param .= 'AND `email` LIKE "%'.$email.'%" ';
}

if(!empty($addr)){
	$param .= 'AND `district` = '.$addr;
}

$totalcount = 0;
$schools = paging_get_schools($page, $perpage, $param, $totalcount);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schools';
$name1='listschools';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle); 
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
 ?>


<?php if (empty($schools)): ?>
	<tr>
    <td colspan="7">
    	<span class="text-danger"><h5><?php echo get_string('searchnoresult') ?></h5></span>
    </td>
 </tr>
<?php else: ?>
	
<?php $stt=$page*$perpage+1; foreach ($schools as $sch) { ?>
<tr>
    <td class="hidden-print"><input type="checkbox" name="list[]" class="list" value="<?php echo $sch->id ?>"></td>
    <td class="text-center"><?php echo $stt++; ?></td>
    <?php 
      if(!empty($hanhdong)||!empty($checkdelete)){
          echo'<td class="text-center hidden-print">';
            $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$sch->id);
          if(!empty($checkdelete)){
                                           ?>
              <a onclick="return check_del()" href="<?php print new moodle_url('?action=del',array('sch_id'=>$sch->id)); ?>" class="mauicon" title='<?php echo get_string('delete') ?>'><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              <?php 
          }
           echo'</td>';
      }
        ?>
    <td><?php echo $sch->name ?></td>
    <td><?php echo $sch->representative_name ?></td>
    <td><?php echo $sch->email ?></td>
    <td><?php echo get_addr($sch->id) ?></td>
    <!-- <td class="text-right hidden-print">
        <a href="<?php print new moodle_url('edit.php',array('sch_id'=>$sch->id)); ?>" class="btn btn-info btn-sm tooltip-animation" title='<?php echo get_string('update') ?>'><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <a href="<?php print new moodle_url('detail.php',array('sch_id'=>$sch->id)); ?>" class="btn btn-warning btn-sm tooltip-animation" title='<?php echo get_string('detail') ?>'><i class="fa fa-eye" aria-hidden="true"></i></a>
        <a onclick="return check_del()" href="<?php print new moodle_url('?action=del',array('sch_id'=>$sch->id)); ?>" class="btn btn-danger btn-sm tooltip-animation" title='<?php echo get_string('delete') ?>'><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </td> -->
     
</tr>
<?php } ?>
<tr id="dellist" style="display: none">
    <th colspan="7"><input type="submit" class="btn btn-simple btn-danger btn-sm btn-icon remove hidden-print" value="<?php echo get_string('delete') ?>" onclick="return check_del()"></th>
</tr>
<tr>
  <td colspan="7">
  <?php 
  paginate($totalcount, $page, $perpage, "#");
   ?>
   </td>
</tr>

<script>
  $(document).ready(function() {
    $(":checkbox").click(function(event) {
      if ($('.list').is(":checked"))
          $("#dellist").show();
      else
          $("#dellist").hide();
    });
  });
</script>
<script>
	$('a').on('click', function(event) {
		var x=$(this).attr('href');
		var page=x.replace('#page=', '');
		$.get('ajaxsearch.php?schoolsname='+$('#schoolsname').val()+'&ht='+$('#ht').val()+'&email='+$('#email').val()+'&addr='+$('#addr').val()+"&page="+page, function(data){
      $('#page').html(data);
    });
	});
</script>
<?php endif ?>