<?php
require_once '../../config.php';
require_once($CFG->dirroot . '/manage/schools/lib.php');
global $CFG,$DB;
if (optional_param('action', '', PARAM_TEXT)=='add') {
	$time = date('h:i, d-m-y',time());
	$addr = optional_param('address', '', PARAM_TEXT);
	$city = $DB->get_record('tinhthanh', array('id'=>optional_param('city', '', PARAM_TEXT)));
	$quanhuyen = $DB->get_record('quanhuyen', array('id'=>optional_param('quanhuyen', '', PARAM_TEXT)));
	$xaphuong = $DB->get_record('xaphuong', array('id'=>optional_param('xaphuong', '', PARAM_TEXT)));
	$addr .= ", ".$xaphuong->name." - ".$quanhuyen->name." - ".$city->name;
	$arr = array(
		'name' => optional_param('schoolsname', '', PARAM_TEXT),
		'address' => $addr,
		'phone' => optional_param('phone', '', PARAM_TEXT),
		'representative_name' => optional_param('representative_name', '', PARAM_TEXT),
		'email' => optional_param('email', '', PARAM_TEXT),
		'phone' => optional_param('phone', '', PARAM_TEXT),
		'birthday' => optional_param('birthday', '', PARAM_TEXT),
		'time_create' => $time,
		'note' => optional_param('note', '', PARAM_TEXT),
		'Representative_address' => optional_param('Representative_address', '', PARAM_TEXT),
		'Representative_email' => optional_param('Representative_email', '', PARAM_TEXT),
		'Representative_phone' => optional_param('Representative_phone', '', PARAM_TEXT)
	);
	add_schools($arr);
	echo "string";
	header('Location:'.$CFG->wwwroot.'/manage/schools/');
}

if (optional_param('action', '', PARAM_TEXT) =='update') {
	$time = date('h:i, d-m-y',time());
	$addr = optional_param('address', '', PARAM_TEXT);
	$city = $DB->get_record('tinhthanh', array('id'=>optional_param('city', '', PARAM_TEXT)));
	$quanhuyen = $DB->get_record('quanhuyen', array('id'=>optional_param('quanhuyen', '', PARAM_TEXT)));
	$xaphuong = $DB->get_record('xaphuong', array('id'=>optional_param('xaphuong', '', PARAM_TEXT)));
	if(empty($city) || empty($quanhuyen) || empty($xaphuong)){
		$addr .= ", ".optional_param('xaphuong', '', PARAM_TEXT).", ".optional_param('quanhuyen', '', PARAM_TEXT).", ".optional_param('city', '', PARAM_TEXT);
	}else
		$addr .= ", ".$xaphuong->name.", ".$quanhuyen->name.", ".$city->name;
	$arr = array(
		'id' => optional_param('id', '', PARAM_TEXT),
		'name' => optional_param('schoolsname', '', PARAM_TEXT),
		'address' => $addr,
		'phone' => optional_param('phone', '', PARAM_TEXT),
		'representative_name' => optional_param('representative_name', '', PARAM_TEXT),
		'email' => optional_param('email', '', PARAM_TEXT),
		'phone' => optional_param('phone', '', PARAM_TEXT),
		'birthday' => optional_param('birthday', '', PARAM_TEXT),
		'time_update' => $time,
		'note' => optional_param('note', '', PARAM_TEXT),
		'representative_address' => optional_param('Representative_address', '', PARAM_TEXT),
		'representative_email' => optional_param('Representative_email', '', PARAM_TEXT),
		'representative_phone' => optional_param('Representative_phone', '', PARAM_TEXT)
	);
	update_schools($arr);
	header('Location:'.$CFG->wwwroot.'/manage/schools.php');
}

if(optional_param('action', '', PARAM_TEXT) == 'del') 
{
	if(isset(optional_param('del', '', PARAM_TEXT)))
	{
		del_schools(optional_param('del', '', PARAM_TEXT));
	}
}

?>