<?php 
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('addschools'));
echo $OUTPUT->header();
$schools = get_schools();
$district = get_district(1);
// if(is_admin()){
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schools';
$name1='addschools';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$action = optional_param('action', '', PARAM_TEXT);
if ($action == 'add') {
  $ha1 = upload_img('ha1', $CFG->dirroot.'/manage/schools/img/');
  $ha2 = upload_img('ha2', $CFG->dirroot.'/manage/schools/img/');
  $name=optional_param('schoolsname', '', PARAM_TEXT);
  $address=optional_param('address', '', PARAM_TEXT);
  $district=optional_param('quanhuyen', '', PARAM_TEXT);
  $commune=optional_param('xaphuong', '', PARAM_TEXT);
  $email=optional_param('email', '', PARAM_TEXT);
  $representative_name=optional_param('representative_name', '', PARAM_TEXT);
  $birthday=optional_param('birthday', '', PARAM_TEXT);
  $Representative_email=optional_param('representative_email', '', PARAM_TEXT);
  $Representative_phone=optional_param('representative_phone', '', PARAM_TEXT);
  $deputy=optional_param('deputy', '', PARAM_TEXT);
  $deputy_phone=optional_param('deputy_phone', '', PARAM_TEXT);
  $deputy_mail=optional_param('deputy_mail', '', PARAM_TEXT);
  $deputy_bir=optional_param('deputy_bir', '', PARAM_TEXT);
  $note=optional_param('note', '', PARAM_TEXT);
  $deputy2=optional_param('deputy2', '', PARAM_TEXT);
  $deputy2_phone=optional_param('deputy2_phone', '', PARAM_TEXT);
  $deputy2_mail=optional_param('deputy2_mail', '', PARAM_TEXT);
  $deputy2_bir=optional_param('deputy2_bir', '', PARAM_TEXT);
  $duration=optional_param('duration', '', PARAM_TEXT);
  $yearlink=optional_param('yearlink', '', PARAM_TEXT);
  $level=optional_param('level', '', PARAM_INT);
  $evaluation_student =optional_param('evaluation_student ', '', PARAM_INT);

  $arr = array(
    'name' => trim($name),
    'address' => trim($address),
    'city' => 1,
    'district' => trim($district),
    'commune' => trim($commune),
    
    'email' => trim($email),
    'representative_name' => trim($representative_name),
    'birthday' => trim($birthday),
    
    'Representative_email' => trim($Representative_email),
    'Representative_phone' => trim($Representative_phone),
    'deputy' => trim($deputy),
    'deputy_phone' => trim($deputy_phone),
    
    'deputy_mail' => trim($deputy_mail),
    'deputy_bir' => trim($deputy_bir),
    'note' => trim($note),
    'deputy2' => trim($deputy2),
    'deputy2_phone' => trim($deputy2_phone),
    
    'deputy2_mail' => trim($deputy2_mail),
    'deputy2_bir' => trim($deputy2_bir),
    'time_create' => date('h:i, d-m-y',time()),
    'img1' => $ha1,
    'img2' => $ha2,
    'del' => 0,
    'duration' => trim($duration),
    'yearlink' => trim($yearlink),
    'level' => trim($level),
    'evaluation_student ' => trim($evaluation_student)
  );

  add_schools($arr);
  echo displayJsAlert(get_string('addsuccess'), $CFG->wwwroot . "/manage/schools");
}
 ?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<style type="text/css">
  label.error{
    color: #f1556c;
  }
</style>
<div class="row">
  <div class="col-md-12">

    <div class="card-box">
        <div class="card-content">
            <br>
            <div class="material-datatables" style="clear: both;">
              <form  action="<?php echo $CFG->wwwroot ?>/manage/schools/new.php?action=add" id="formAdd" method="post" enctype="multipart/form-data">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('nameschools'); ?><span style="color:red">*</span></label>
                        <input class="form-control" type="text" name="schoolsname" id="schoolsname">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('addrschools'); ?></label>
                        <input class="form-control" type="text" name="address">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('district'); ?></label>
                        <select name="quanhuyen" id="quanHuyenChange" class="form-control">
                          <option value="-1">---None---</option>
                          <?php 
                            foreach ($district as $val) {
                              echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('commune'); ?></label>
                        <select name="xaphuong" id="xaPhuongChange" class="form-control">
                          <option value="-1">---None---</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('emailschools'); ?></label>
                        <input class="form-control" type="text" name="email" id="email">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('mapschools'); ?></label>
                      <div>
                        <input class="form-control file" type="file" id="ha1" name="ha1">
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('tkb'); ?></label>
                      <div>
                        <input class="form-control file" type="file" name="ha2">
                        <div></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('yearlink') ?></label>
                      <div>
                        <input class="form-control " type="number" name="yearlink" id="">
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('classlink') ?></label>
                      <div>
                        <input class="form-control " type="number" name="duration" id="">
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('level') ?></label>
                      <div>
                        <select name="level" id="" class="form-control">
                          <option value="1"><?php echo get_string('level1'); ?></option>
                          <option value="2"><?php echo get_string('level2'); ?></option>
                          <!-- <option value="3"><?php echo get_string('level3'); ?></option> -->
                        </select>
                        <div></div>
                      </div>
                    </div>
                  </div>
                  <!-- trang edit -->
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('evaluation_student') ?></label>
                        <select name="evaluation_student" id="" class="form-control">
                          <option value="1"><?php echo get_string('yes'); ?></option>
                          <option value="2"><?php echo get_string('no'); ?></option>
                        </select>
                    </div>
                  </div>
                </div>
                <hr style="border: 1px solid #dddccc">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('principal'); ?></label>
                        <input class="form-control" type="text" name="representative_name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?></label>
                        <input class="form-control datepicker" type="text" placeholder="dd/mm/yyyy" name="birthday" id="birthday" style="margin-top: 0px;">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('phone'); ?></label>
                        <input class="form-control" type="text" name="representative_phone" id="representative_phone">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label">Email</label>
                        <input class="form-control" type="text" name="representative_email" id="representative_email">
                        
                    </div>
                  </div>
                </div>
                <hr style="border: 1px solid #dddccc">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('deputy'); ?> 1</label>
                        <input class="form-control" type="text" name="deputy">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?></label>
                        <input class="form-control datepicker" type="text" placeholder="dd/mm/yyyy" name="deputy_bir">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <!-- <div class="col-md-4">
                    <div class="form-group label-floating">
                      <label>Địa chỉ</label>
                        <input class="form-control" type="text" name="deputy_addr">
                    </div>
                  </div> -->
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('phone'); ?></label>
                        <input class="form-control" type="text" name="deputy_phone" id="deputy_phone">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label>Email</label>
                        <input class="form-control" type="text" name="deputy_mail" id="deputy_mail">
                        
                    </div>
                  </div>
                </div><hr>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('deputy'); ?> 2</label>
                        <input class="form-control" type="text" name="deputy2">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?></label>
                        <input class="form-control datepicker" type="text" placeholder="dd/mm/yyyy" name="deputy2_bir">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <!-- <div class="col-md-4">
                    <div class="form-group label-floating">
                      <label>Địa chỉ</label>
                        <input class="form-control" type="text" name="deputy2_addr">
                    </div>
                  </div> -->
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('phone'); ?></label>
                        <input class="form-control" type="text" name="deputy2_phone" id="deputy2_phone">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label>Email</label>
                        <input class="form-control" type="text" name="deputy2_mail" id="deputy2_mail">
                        
                    </div>
                  </div>
                </div>

                <hr style="border: 1px solid #dddccc">
                <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label"><?php echo get_string('noteschools'); ?></label>
                    <br>
                    <textarea name="note" id="note" cols="30" rows="10" class="form-control">
                    </textarea>
                    <script>CKEDITOR.replace('note');</script>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-buttons" style="border-top: none !important">
                    <input id="btnSub" type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
                    <?php print_r(get_string('or')) ?>
                    <a href="<?php echo $CFG->wwwroot ?>/manage/schools/" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                  </div>
                </div>
                </div>
              </form>
            </div>
        <!-- end content-->
    </div>
      <!--  end card  -->
  </div>
    <!-- end col-md-12 -->
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script>
  $('#representative_phone').keyup(function(){
      var kq=$(this).val();
      var count = kq.length;
      if(count<12){
          $('#btnSub').removeAttr('disabled');
        }else{
          $('#btnSub').attr('disabled', 'disabled');
          alert('<?php echo get_string('invalidphone') ?>');
      }
  });
  $('#deputy_phone').keyup(function(){
      var kq=$(this).val();
      var count = kq.length;
      if(count<12){
          $('#btnSub').removeAttr('disabled');
        }else{
          $('#btnSub').attr('disabled', 'disabled');
          alert('<?php echo get_string('invalidphone') ?>');
      }
  });
  $('#deputy2_phone').keyup(function(){
      var kq=$(this).val();
      var count = kq.length;
      if(count<12){
          $('#btnSub').removeAttr('disabled');
        }else{
          $('#btnSub').attr('disabled', 'disabled');
          alert('<?php echo get_string('invalidphone') ?>');
      }
  });
  var d = new Date();
  var strDate = d.getFullYear() + "-" + d.getMonth() + "-" + (d.getDate()-1);
  $('#birthday').datepicker({
        maxDate: new Date(strDate),
        inline: true
    });
</script>
<?php 
// }else
  // echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot);
  echo $OUTPUT->footer(); ?>
<?php include_once 'validator.php'; ?>