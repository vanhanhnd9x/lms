<?php 
require("../../config.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');
// require_once($CFG->dirroot . '/teams/lib.php');
require_login(0, false);
$PAGE->set_title(get_string('detailschools'));
echo $OUTPUT->header();
$sch_id   = optional_param('sch_id', '', PARAM_TEXT);
$schools  = get_schools($sch_id); 
$city     = get_city();
$district = get_district($schools->city);
$commune  = get_commune($schools->district);
$quan=$DB->get_record('quanhuyen', array(
  'id' => $schools->district
));
// if (is_teacher() || is_admin()) {
// add_logs('schools', 'view', '/manage/schools/detail.php?sch_id='.$sch_id, $schools->name);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schools';
$name1='detailschools';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
 ?>

<div class="row">
  <div class="col-md-12">

    <div class="card-box">
        <div class="card-content">
            <br>
            <div class="material-datatables" style="clear: both;">
              <form  action="<?php echo $CFG->wwwroot ?>/manage/schools/edit.php?action=update" onsubmit="return validate();" method="post">
                <input type="text" disabled name="id" value="<?php echo $schools->id ?>" hidden>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('nameschools'); ?>:</label>
                        <p><?php echo $schools->name ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('addrschools'); ?>:</label>
                        <p><?php echo $schools->address ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('district'); ?>:</label>
                        
                          <?php 
                          echo '<p>'.$quan->name.'</p>';
                          // var_dump($quan);
                          //   foreach ($district as $val) {
                          //     if ($val->id == $schools->district) {
                          //       echo '<p>'.$val->name.'</p>';
                          //     }
                          //   }
                          ?>

                        
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('commune'); ?> :</label>
                        
                          <?php 
                            foreach ($commune as $val) {
                              if ($val->id == $schools->commune) {
                                echo '<p>'.$val->name.'</p>';
                              }
                            }
                          ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('emailschools'); ?>:</label>
                        <p><?php echo $schools->email ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('yearlink'); ?>:</label>
                      <p><?php echo $schools->yearlink ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('classlink') ?>:</label>
                      <p><?php echo $schools->duration ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('level') ?>:</label>
                      <p><?php echo $schools->level==1? get_string('level1') : $schools->level==2? get_string('level2') : get_string('level3') ?></p>
                    </div>
                  </div>

                   
                  <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label"><?php echo get_string('mapschools'); ?>:</label>
                        <div class="">
                         <!--  <img class="rounded" src="img/<?php echo $schools->img1 ?>" alt="" width="60%" id="myImg"> -->
                          <!-- <a href="<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img1 ?>" target="_blank" title="Xem bản đồ" rel="follow, index">Xem bản đồ</a> -->
                           <?php 
                            if(!empty($schools->img1)){
                              ?>
                              <a style="color: blue;" onclick="window.open('<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img1; ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');">
                                Xem bản đồ
                              </a>
                              <?php 
                            }
                           ?>
                        </div>
                      </div>
                    </div>
                     <!-- The Modal -->
                    <div id="myModal" class="modal">
                      <span class="close">&times;</span>
                      <img class="modal-content" id="img01">
                      <div id="caption"></div>
                    </div>

                    
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label"><?php echo get_string('tkb'); ?>:</label>
                        <div class="">
                          <?php 
                          if(!empty($schools->img2)){
                            ?>
                            <a style="color: blue;" onclick="window.open('<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img2; ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');">
                             Xem thời khóa biểu
                            </a>
                            <?php 
                          }
                         ?>
                          <!-- <img class="rounded" src="img/<?php echo $schools->img2 ?>" alt="" width="60%" id="image2"> -->
                          <!-- <a href="<?php echo $CFG->wwwroot.'/manage/schools/img/'.$schools->img2 ?>" target="_blank" title="Xem thời khóa biểu" rel="follow, index">Xem thời khóa biểu</a> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <hr>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('principal'); ?>:</label>
                        <p><?php echo $schools->representative_name ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('birthday'); ?>:</label>
                        <p><?php echo $schools->birthday ?></p>
                    </div>
                  </div>                  
                </div>
                
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('phone'); ?></label>
                        <p><?php echo $schools->representative_phone ?></p>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label class="control-label">Email:</label>
                        <p><?php echo $schools->representative_email ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('deputy'); ?> 1:</label>
                        <p><?php echo $schools->deputy ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?>:</label>
                        <p><?php echo $schools->deputy_bir ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('phone'); ?></label>
                        <p><?php echo $schools->deputy_phone ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label>Email:</label>
                        <p><?php echo $schools->deputy_mail ?></p>
                    </div>
                  </div>
                </div><hr>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('deputy'); ?> 2:</label>
                        <p><?php echo $schools->deputy2 ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('birthday'); ?>:</label>
                        <p><?php echo $schools->deputy2_bir ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label><?php echo get_string('phone'); ?></label>
                        <p><?php echo $schools->deputy2_phone ?></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group label-floating">
                      <label>Email:</label>
                        <p><?php echo $schools->deputy2_mail ?></p>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group label-floating">
                      <label class="control-label"><?php echo get_string('noteschools'); ?>:</label>
                      <p><?php echo $schools->note ?></p>
                    </div>
                  </div>
                </div>
                <br>
                <br>
                <br>
                <div class="col-md-12 hidden-print">
                  <div class="form-buttons" style="border-top: none !important">
                    <a href="javascript:window.print()" class="btn btn-primary waves-effect waves-light"><i class="fa fa-print m-r-5"></i> Print</a>
                    <a href="<?php echo $CFG->wwwroot ?>/manage/schools/" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                  </div>
                </div>

              </form>
            </div>
        <!-- end content-->
    </div>
      <!--  end card  -->
  </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
</div>
<style>
/*body {font-family: Arial, Helvetica, sans-serif;}*/

/*#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}
*/
#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
<script>
  // Get the modal
  var modal = document.getElementById('myModal');

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  var img = document.getElementById('myImg');
  var img2 = document.getElementById('image2');
  var modalImg = document.getElementById("img01");
  var captionText = document.getElementById("caption");
  img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
  }
  img2.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
  }
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() { 
    modal.style.display = "none";
  }
  </script>
<?php
// }else
//  echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot);
 echo $OUTPUT->footer(); ?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
