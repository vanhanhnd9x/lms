<?php 
require_once '../../config.php';
global $CFG, $DB, $USER;

// function get_count_record($table, $param='')
// {
//   global $DB;
//   return $DB->get_record_sql("SELECT count(id) AS 'count' FROM ".$table.$param);
// }

function get_schools($id){
  global $DB;
  if(empty($id)){
    return $DB->get_records_sql("SELECT * FROM schools WHERE del=0");
  }else
    return $DB->get_record('schools', array('id'=>$id, 'del'=>0));
}

function paging_get_schools($page=0, $perpage=3, $param, &$count)
{
  global $DB;
  $page_number = $page*$perpage;
  $count = count($DB->get_records_sql("SELECT * FROM schools WHERE del=0 AND $param"));
  if (empty($param)) {
    $param = '1';
  }
  return $DB->get_records_sql("SELECT * FROM schools WHERE del=0 AND $param LIMIT $page_number, $perpage");
}

function add_schools($school) {
  global $DB, $USER;
  $record = new stdClass();
  foreach ($school as $key => $val) {
      $record->$key = $val;
  }

  $new_schools = $DB->insert_record('schools', $record);
  return $new_schools;
}

function update_schools($school) {
  global $DB, $USER;
  $record = new stdClass();
  foreach ($school as $key => $val) {
      $record->$key = $val;
  }

  $update_schools = $DB->update_record('schools', $record, false);
  return $update_schools;
}

function del_schools($id){
  global $DB;
  $record = new  stdClass();
  $record->id = $id;
  $record->del = 1;
  $DB->update_record('schools', $record, false);
}

function del_list(array $list){
  global $DB;
  foreach ($list as $val) {
    $schools = get_schools($val)->name;
    del_schools($val);
    add_logs('schools', 'delete', '', $schools);
  }
}

function get_addr($id){
  global $DB;
  $sch = $DB->get_record('schools', array('id'=>$id));
  $xp = $DB->get_record('xaphuong', array('id'=>$sch->commune));
  $qh = $DB->get_record('quanhuyen', array('id'=>$sch->district));
  $tt = $DB->get_record('tinhthanh', array('id'=>$sch->city));
  $addr = $xp->name."-".$qh->name."-".$tt->name;
  return $addr;
}

function get_block($schoolid)
{
  global $DB;
  $block = $DB->get_records('block_student', array(
    'schoolid' => $schoolid,
    'del' => 0
  ));

  return $block;
}

 ?>