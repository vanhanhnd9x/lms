<?php 
require("../../config.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schools/lib.php');

if (!empty($_GET['key']) || !empty($_GET['qh'])) {
  $key = $_GET['key'];
  $qh=$_GET['qh'];
  if (empty($key)) {
    $sql= "SELECT * FROM schools WHERE `del`=0 AND `district`=$qh";
  }
  if (empty($qh)) {
    $sql= "SELECT * FROM schools WHERE `del`=0 AND(`name` LIKE '%$key%' OR `representative_name` LIKE '%$key%' OR `email` LIKE '%$key%' OR `address` LIKE '%$key%')";
  }
  if (!empty($key) && !empty($qh)) {
    $sql= "SELECT * FROM schools WHERE `del`=0 AND `district`=$qh AND(`name` LIKE '%$key%' OR `representative_name` LIKE '%$key%' OR `email` LIKE '%$key%' OR `address` LIKE '%$key%')";
  }
  $schools = $DB->get_records_sql($sql);
}else{
  $schools = $DB->get_records('schools', array('del'=>0));
}
$data = "";
foreach ($schools as $sch) {
	$data .='
	<tr>
        <td class="hidden-print"><input type="checkbox" name="list[]" class="list" value="'. $sch->id. '"></td>
        <td>'.$sch->name.'</td>
        <td>'.get_addr($sch->id).'</td>
        <td>'.$sch->representative_name.'</td>
        <td>'.$sch->email.'</td>
        <td class="text-right hidden-print">
            <a href="'. new moodle_url('edit.php',array('sch_id'=>$sch->id)).'" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a href="'. new moodle_url('detail.php',array('sch_id'=>$sch->id)).'" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
            <a onclick="return check_del()" href="'. new moodle_url('?action=del',array('sch_id'=>$sch->id)).'" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>
    </tr>';
}
$data .= '<tr>
            <th colspan="6"><input type="submit" class="btn btn-simple btn-danger btn-sm btn-icon remove hidden-print" value="'. get_string('delete').'" onclick="return check_del()"></th>
          </tr>';
if (sizeof($schools)==0) {
    $data = '<b class="text-danger">'.get_string('searchresultsdonotexist').'</b>';
}

echo $data;
?>