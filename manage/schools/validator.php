<script>
	$('#formAdd').validate({
	rules:{
		schoolsname:{
			required:true
		},
		email:{
			email:true
		},
		representative_email:{
			email:true
		},
		deputy_mail:{
			email:true
		},
		deputy2_mail:{
			email:true
		},
		representative_phone:{
			number:true
		},
		deputy_phone:{
			number:true
		},
		deputy2_phone:{
			number:true
		}
	},
	messages:{
		schoolsname:{
			required:"<?php echo get_string('notempty') ?>"
		},
		email:{
			email:"<?php echo get_string('invalidemail') ?>"
		},
		representative_email:{
			email:"<?php echo get_string('invalidemail') ?>"
		},
		deputy_mail:{
			email:"<?php echo get_string('invalidemail') ?>"
		},
		deputy2_mail:{
			email:"<?php echo get_string('invalidemail') ?>"
		},
		representative_phone:{
			number:"<?php echo get_string('invalidphone') ?>"
		},
		deputy_phone:{
			number:"<?php echo get_string('invalidphone') ?>"
		},
		deputy2_phone:{
			number:"<?php echo get_string('invalidphone') ?>"
		}
	}
});
$.validator.methods.email = function( value, element ) {
  return this.optional( element ) || /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test( value );
}
// $.validator.methods.number = function( value, element ) {
//   return this.optional( element ) || /^([0]|\(84\)|\(\+84\))[0-9]{8,10}$/.test( value );
// }

$('.file').change(function(event) {
	var err=0;
  if (window.File && window.FileReader && window.FileList && window.Blob)
  {
		if($(this).get(0).files.length==0){
			$(this).siblings().html('');
			err=0;
    }else{
    	var fname = $(this)[0].files[0].name;
      var fsize = $(this)[0].files[0].size;
      var ftype = $(this)[0].files[0].type;

      if (ftype=='image/png' || ftype=='image/gif' || ftype=='image/jpeg' || ftype=='image/pjpeg' || ftype=='application/pdf') {
        if (fsize > 5000000) {
          $(this).siblings().html('<div class="text-danger"><b><?php echo get_string('sizesys') ?> < 5M</b></div>');
          err=1;
        }else{
          $(this).siblings().html('');
          err=0;
        }
      }else{
        $(this).siblings().html('<div class="text-danger"><b><?php echo get_string('notimage') ?></b></div>');
        err=1;
      }
    }
  }else{
    alert("Please upgrade your browser, because your current browser lacks some new features we need!");
  }
  if (err==1) {
  	$('#btnSub').attr('disabled', 'disabled');
  }else{
  	$('#btnSub').removeAttr('disabled');
  }
});

// $('.so').change(function() {
// 	var err=0;
// 	var reg = /^[0-9]+$/;
// 	if(!reg.test($('#yearlink').val()) && $('#yearlink').val()!=''){
//   	err=1;
//     $('#yearlink').siblings('div').html('<label id="schoolsname-error" class="error" for="schoolsname"><?php echo get_string('notformatnumber') ?></label>');
//   }else {
//   	$('#yearlink').siblings('div').html('');
//   }
// 	if(!reg.test($('#duration').val()) && $('#duration').val()!=''){
//   	err=1;
//     $('#duration').siblings('div').html('<label id="schoolsname-error" class="error" for="schoolsname"><?php echo get_string('notformatnumber') ?></label>');
//   }else {
//   	$('#duration').siblings('div').html('');
//   }

//   if(!reg.test($('#level').val()) && $('#level').val()!=''){
//   	err=1;
//     $('#level').siblings('div').html('<label id="schoolsname-error" class="error" for="schoolsname"><?php echo get_string('notformatnumber') ?></label>');
//   }else {
//   	$('#level').siblings('div').html('');
//   }

// 	if (err==1) {
//   	$('#btnSub').attr('disabled', 'disabled');
//   }else{
//   	$('#btnSub').removeAttr('disabled');
//   }
// });
</script>