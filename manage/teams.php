<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');

// Require Login.
require_login();

$PAGE->set_title("Teams");
$PAGE->set_heading("Teams");

//now the page contents
// $PAGE->set_pagelayout('teams');
echo $OUTPUT->header();
$PAGE->requires->js('/manage/manage.js');

$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/search.js');


$teamId = optional_param('id', 0, PARAM_INT); // course id
//List My Course

$teams = manage_get_team_list();
?>

    <div id="admin-fullscreen">

        <div id="admin-fullscreen-left" class="focus-panel"> 
            <!-- Left Content -->
            <div class="body">
                <div class="panel-head">
                    <form method="get" action="">

                        <div class="field-help">
                            <label id="searchPrompt" for="searchBox">Search by group name</label>
                            <input type="text" value="" name="searchTeam" id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                        </div>
                    </form>
                </div>


                <div id="searchResults">
                    <ul>
                        <?php
                        $finded = 0;
                        foreach ($teams as $team) {
                            $finded++;
                            ?>
                            <li>

                                <div class="main-col">

                                    <div class="title">

                                        <a href="<?php echo new moodle_url('/group/group.php?courseid=1&id', array('id' => $team->id)); ?>">
                                            <span class="nav-reports"><?php echo $team->name ?></span></a>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="courses-found"><?php echo $finded; ?> groups found</div>
                </div>
            </div>
        </div>
        <div id="admin-fullscreen-right">

            <div class="side-panel">
                <div class="action-buttons">
                    <ul>
                        <li>
                            <a href="<?php print new moodle_url('/group/group.php', array('courseid' => 1)); ?>" class="big-button drop">
                                <span class="left">
                                    <span class="mid">
                                        <span class="right">
                                            <span class="icon_add">Add a new group</span>
                                        </span></span></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>  <!-- End Right -->
    </div>
    

    <?php

echo $OUTPUT->footer();
?>
