<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../config.php");
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/lib/completionlib.php');
require_once($CFG->dirroot . '/lib/completion/completion_completion.php');
require_once($CFG->dirroot . '/manage/lib.php');


//Quang added to fix switch role to learner, when click in menu --> back to teacher role
$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
$id = optional_param('id', -1, PARAM_INT); //course id
if ($current_switchrole != -1 && $id != -1) {
    $context = get_context_instance(CONTEXT_COURSE, $id);
    role_switch($current_switchrole, $context);
    require_login($id);
} else {
    require_login();
}

$PAGE->set_url('/mod/noticeboard/view.php');
$PAGE->set_title(get_string('myachieve'));
$PAGE->set_heading(get_string('myachieve'));

echo $OUTPUT->header();

global $USER;
$courses = get_courses_achievments($USER->id);
?>

<div id="admin-fullscreen">

    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <div class="panel-head clearfix">
                <h3><?php print_r(get_string('achievements')) ?></h3>
                <div class="subtitle"><?php print_r(get_string('thesearecourse')) ?></div>
            </div>
            <div class="item-page-list">

                <table class="grid">
                    <tbody>
<?php
foreach ($courses as $course) {
    ?>
                            <tr>

                                <td class="main-col">
                                    <div class="title">
                            <?php echo $course->fullname; ?>
                                    </div>

                                    <div class="tip">
    <?php
    $ccompletion = new completion_completion(array('course' => $course->id, 'userid' => $USER->id));
    ?>
                                        <?php print_r(get_string('achievedon')) ?> <?php echo date('d/m/Y', $ccompletion->timecompleted); ?>

                                    </div>
                                </td>

                            </tr>
                                        <?php
                                    }
                                    ?>
                    </tbody></table>

            </div>


        </div> 
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div id="admin-fullscreen-right">

        <div class="side-panel">


            <h3><?php print_r(get_string('recentachie')) ?></h3>

<?php
$archivements = get_recent_achievements(30);
?>
            <ul>
<?php
if (!empty($archivements)) {
    foreach ($archivements as $c) {
        ?>
                        <li>   

                            <div class="side-title"><?php echo $c->fullname; ?></div>

                            <div class="padded tip">                
                        <?php print_r(get_string('completeon'));
                        echo date('M d, Y', $c->timecompleted); ?>

                            </div>
                            <div class="clear"></div>
                        </li>
    <?php }
} ?>


            </ul>


        </div>
    </div>  <!-- End Right -->

</div>      
<?php
echo $OUTPUT->footer();
?>