<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 *
 * @author
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}


require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/manage/people/lib.php');
// Require Login.
require_login();


$PAGE->set_title(get_string('people'));
$PAGE->set_heading(get_string('people'));
//now the page contents
$PAGE->set_pagelayout(get_string('people'));
echo $OUTPUT->header();
$file_extension=end(explode('.',$_FILES["file"]["name"]));

if ($file_extension== "csv") {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {
            parseCsv($_FILES["file"]["tmp_name"]);
        }
    } 
    
    if ($file_extension!= "csv"&&$file_extension!=''){
        echo "Please upload a csv file";
    }


?>

<div id="page-body">        
        
       
        <table class="admin-fullscreen">
            <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        

        <div id="courseList" class="focus-panel">
            
            <div class="panel-head">
              <h3><?php print_r(get_string('importuser')) ?></h3>
              <div class="subtitle"><?php print_r(get_string('in2easy')) ?></div>
            </div>
            <div class="body"> 
                <div style="margin-left:10px;">
                <ol>
                  <li><?php print_r(get_string('firstyouupload')) ?>
                    <ul>
                      <li><?php print_r(get_string('firstname')) ?></li>
                        <li><?php print_r(get_string('lastname')) ?></li>
                        <li><?php print_r(get_string('username')) ?> <i>- <?php print_r(get_string('usuallyanemail')) ?></i></li>
                    </ul>
                    <br>
                </li>
                <li><?php print_r(get_string('secondyouwill')) ?></li>
              </ol> 
                  <p class="italic"><?php print_r(get_string('tip')) ?> <a href="<?php echo $CFG->wwwroot ?>/manage/people/UserImportTemplate.csv"><?php print_r(get_string('usethistemplate')) ?></a>.</p>
                </div> 
            <form method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot?>/manage/import_people.php">
              <div class="form-sub-heading"><?php print_r(get_string('selectfile')) ?></div>
            <table class="simple-form">
                <tbody><tr>
                    <th>
                        <?php print_r(get_string('csvfile')) ?>
                    </th>
                    <td>
                        <input type="file" name="file" id="file">
                        
                    </td>
                </tr>
<tr>
                    <th>
                       &nbsp;
                    </th>
                    <td>
                    </td>
                </tr> 
<tr>
                    <th>
                       &nbsp;
                    </th>
                    <td>
                      <input type="checkbox" value="true" name="SkipFirstRow" id="SkipFirstRow"><input type="hidden" value="false" name="SkipFirstRow"> <label for="SkipFirstRow"><?php print_r(get_string('thefirstrow')) ?></label>
                    </td>
                </tr>                                  
            </tbody></table>           

            <div class="form-buttons">
              <input type="submit" class="btnlarge" value="<?php print_r(get_string('import')) ?>">
                <?php print_r(get_string('or')) ?> 
              <a href="<?php echo $CFG->wwwroot ?>/manage/people.php"><?php print_r(get_string('cancel')) ?></a>
            </div>
            </form>                
                
            </div>
            
        </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        



                    </div>
                </td>
            </tr>
        </tbody></table>
        
        
    </div>

    <?php echo $OUTPUT->footer(); ?>
