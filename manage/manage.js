/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function init(){
    
    var searchBox = document.getElementById('searchBox');
    var searchPrompt = document.getElementById('searchPrompt');
    

    searchBox.onfocus = function() {
            searchPrompt.style.display = 'none';
		};
    searchBox.onblur = function() {
			 searchPrompt.style.display = 'block';
    
            if(searchBox.value){
                    searchPrompt.style.display = 'none';
            }
		};
        
    
}

/* for other browsers */

window.onload = init;