<?php 
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
// require_once($CFG->dirroot . '/manage/evaluation_student/phpdocx/classes/CreateDocxFromTemplate.php'); 
global $DB;
require_login(0, false);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$month = optional_param('month1', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$truong=$DB->get_record('schools', array(
  'id' => $schoolid
));
$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
$text1=hien_thi_nhan_xet_theo_thaidohoctap();
$text2=hien_thi_nhan_xet_theo_dodunghoctap();
$text3=hien_thi_nhan_xet_theo_thamgiaxaydungbai();
function lay_tengvnn_thuoc_nhom_lop_xuadanhgia($courseid){
    global $DB;
    $sql="SELECT DISTINCT  `user`.`id`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id`={$courseid}
    AND `role_assignments`.`roleid`= 8 AND `user`.`del`=0";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    $gvnn=$DB->get_record('user', array(
      'id' => $data
    ));
    $name=$gvnn->lastname.' '.$gvnn->firstname;
    return $name;
}
function thongtingvdanhgia_danhgiahs($userid){
    global $DB;
    $gv=$DB->get_record('user', array(
      'id' => $userid
    ));
    $name=$gv->lastname.' '.$gv->firstname;
    $kq=array(
        'name'=>$name,
        'sdt'=>$gv->phone2,
        'email'=>$gv->email,
    );
    return $kq;
}
function lay_danh_gia_xuat_bao_cao($block_student,$month,$school_yearid){
	global $DB;
	 $sql="SELECT `evalucation_student`.*,`user`.`code`, `user`.`firstname`,`user`.`lastname`, `course`.`fullname` , `history_course_name`.`course_name`,`groups`.`name`
	  FROM `evalucation_student` 
	  JOIN `user` ON `evalucation_student`.`userid`= `user`.`id` 
	  JOIN `course` ON `course`.`id` =`evalucation_student`.`courseid`
	  JOIN history_course_name ON `history_course_name`.`courseid` =`course`.`id`
	  JOIN `groups` on `groups`.`id` =`evalucation_student`.`groupid` 
	  WHERE `user`.`del` =0 
	  AND `evalucation_student`.`del`=0 
	  AND `groups`.`id_khoi`= {$block_student}
	  AND `evalucation_student`.`month`= {$month}
	  AND `history_course_name`.`school_year_id`= {$school_yearid}
	  ORDER BY `history_course_name`.`course_name`, `user`.`firstname` ASC
	  ";
 	$data=$DB->get_records_sql($sql);
 	return $data;
}

$data=lay_danh_gia_xuat_bao_cao($block_student,$month,$school_yearid);
// var_dump($data);die;
?>
<!-- <div style=" text-align: left;    width: 180px;height: 60px;">
                <img src="'.$CFG->wwwroot.'//theme/nimble/assets/images/logo-llv.png" alt="">
                </div>
                <div class="title " style="text-transform: uppercase; text-align: right;">
                    SCHOOLS LINK PROGRAM - STUDENT EVALUATION<br>
                    CHƯƠNG TRÌNH TIẾNG ANH CHẤT LƯỢNG CAO - ĐÁNH GIÁ HỌC SINH<br>
                    THÁNG '.$month.' NĂM HỌC '.$namhoc->sy_start.'-'.$namhoc->sy_end.' '.$truong->name.'
                </div> -->
<?php
$dau='
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <title>Document</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
 </head>
 <body>
     
    ';
 //    $dau='
 // <!DOCTYPE html>
 // <html lang="en">
 // <head>
 //     <meta charset="UTF-8">
 //     <title>Document</title>
 //     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

 // </head>
 // <body>
 //     <div class="">
 //         <div class="row">
 //             <div class="col-md-12">
 //                  <table class="table table-bordered">
 //                      <tr>
 //                        <td class="" style=" text-align: left;"">
 //                          <img src="'.$CFG->wwwroot.'//theme/nimble/assets/images/logo-llv.png" alt="" style= width: 150px;height: 60px;">
 //                        </td>
 //                        <td class=""  style="text-transform: uppercase; text-align: right;">
 //                             SCHOOLS LINK PROGRAM - STUDENT EVALUATION<br>
 //                            CHƯƠNG TRÌNH TIẾNG ANH CHẤT LƯỢNG CAO - ĐÁNH GIÁ HỌC SINH<br>
 //                            THÁNG '.$month.' NĂM HỌC '.$namhoc->sy_start.'-'.$namhoc->sy_end.' '.$truong->name.'     
 //                        </td>
 //                      </tr>
 //                  </table>
              
                
                 
 //             </div>
 //         </div>
 //     </div>
 //     <br></br>
 //     <br></br>
 //    ';
$cuoi=' </body>
 </html>
 ';
  if(!empty($school_yearid)&&!empty($month)&&!empty($block_student)){
          header("Content-Type: application/vnd.msword");
          header("Expires: 0");//no-cache
          header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
          header("content-disposition: attachment;filename=danh-gia-hoc-sinh.doc");
  }          
          echo $dau;
           if(!empty($data)){
                $i=0;
                foreach ($data as  $value) {
                    # code...
                    $i++;
                    $tong=lay_tong_tiet_trong_thang($value->month,$value->school_yearid,$value->courseid);
                    if(empty($tong)){ $tong=0;}
                    $gvnn=lay_tengvnn_thuoc_nhom_lop_xuadanhgia($value->courseid);
                    $gvdg=thongtingvdanhgia_danhgiahs($value->evalucation_by);
                    $htlm[$i]='
                    <div class="">
                       <div class="row">
                           <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                      <td class="" style=" text-align: left;"">
                                        <img src="'.$CFG->wwwroot.'//theme/nimble/assets/images/logo-llv.png" alt="" style= width: 150px;height: 30px;">
                                      </td>
                                      <td class=""  style="text-transform: uppercase; text-align: right; font-size:13px;font-style: italic ">
                                           SCHOOLS LINK PROGRAM - STUDENT EVALUATION<br>
                                          CHƯƠNG TRÌNH TIẾNG ANH CHẤT LƯỢNG CAO - ĐÁNH GIÁ HỌC SINH<br>
                                          
                                      </td>
                                    </tr>
                                </table>
                           </div>
                       </div>
                   </div>
                   <br></br>
                   <br></br>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <p><span style="font-weight: bold;">Name of student/ Họ tên học sinh:</span> '.$value->lastname.' '.$value->firstname.'<br>
                                <span style="font-weight: bold;">Class/ Lớp:</span> '.$value->name.'<br>
                                <span style="font-weight: bold;">Class group/ Nhóm lớp:</span>  '.$value->course_name.'<br>
                                <span style="font-weight: bold;">School/ Trường học:</span> '.$truong->name.'<br>
                                <span style="font-weight: bold;">Teacher/ GVNN:</span> '.$gvnn.'</p>
                            </div>
                            
                        </div>
                     </div>
                     <div class="">
                         <div class="row">
                            <div class="col-md-12">
                               <table class="table table-bordered">
                                        <tr>
                                           <td class="">
                                                <span style="font-weight: bold;">A = Excellent/ Xuất sắc</span>
                                            </td>
                                            <td class="">
                                                <span style="font-weight: bold;">B = Working well/ Tốt</span>
                                            </td>
                                        </tr>
                                         <tr>
                                           <td class="">
                                                <span style="font-weight: bold;">C = Would benefit from more effort/ Cần cố gắng hơn</span>
                                            </td>
                                            <td class="">
                                               <span style="font-weight: bold;">D = Unsatisfactory/ Chưa đạt yêu cầu</span>
                                            </td>
                                        </tr>
                                </table>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <table class="table" style="border: 1px solid black;">
                                    <thead>
                                        <tr>
                                           <td style="border: 1px solid black;text-align: center;">
                                                <span style="font-weight: bold;">Criteria</span><br>Tiêu chí đánh giá
                                            </td>
                                            <td style="border: 1px solid black;text-align: center;">
                                                <span style="font-weight: bold;"> A-Attendance</span><br>
                                                Chuyên cần
                                            </td>
                                            <td style="border: 1px solid black;text-align: center;"> 
                                                <span style="font-weight: bold;">L-Learning attitude </span>
                                                <br>
                                                Thái độ học tập
                                            </td >
                                            <td style="border: 1px solid black;text-align: center;">
                                                <span style="font-weight: bold;">Learning material</span><br>
                                                Đồ dùng học tập
                                             </td>
                                            <td style="border: 1px solid black;text-align: center;">
                                                <span style="font-weight: bold;">P-Participation</span><br>
                                                Tham gia xây dựng bài
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                          <td  style="border: 1px solid black;text-align: center;"><span style="font-weight: bold;">Evaluation</span>/ Đánh giá</td>
                                          <td  style="border: 1px solid black;text-align: center;">'.$value->attendance.'/'.$tong.'</td>
                                          <td  style="border: 1px solid black;text-align: center;">'.$value->learning_attitude.'</td>
                                          <td  style="border: 1px solid black;text-align: center;">'.$value->material.'</td>
                                          <td  style="border: 1px solid black;text-align: center;">'.$value->participation.'</td>
                                        </tr>
                                        <tr>
                                          <td style="border: 1px solid black;text-align: center;"><span style="font-weight: bold;">Comments/</span> Nhận xét</td>
                                          <td colspan="4" style="border: 1px solid black;">
                                          '.$text1[$value->learning_attitude][$value->cmt1].'
                                          '.$text2[$value->material][$value->cmt2].'
                                          '.$text3[$value->participation][$value->cmt3].'
                                          </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                     </div>
                     <br>
                     <div class="">
                         <div class="row">
                             <div class="col-md-12">
                                <p><span style="font-weight: bold;">Done by Teaching Assistant/</span> Nhận xét bởi GVTG: '.$gvdg['name'].'<br>
                                <span style="font-weight: bold;">Phone number/</span> Số điện thoại: '.$gvdg['sdt'].'<br>
                                <span style="font-weight: bold;">Email/</span> Hòm thư điện tử:: '.$gvdg['email'].'<br>
                                </p>
                             </div>
                         </div>
                     </div>
                     <br><br><br><br><br><br><br><br><br><br>
                    ';
                    echo $htlm[$i];
                }
            }
          echo $cuoi;
          unset($dau,$cuoi,$htlm,$data,$value,$text1,$text2,$text3,$tong,$gvnn,$gvdg);
?>