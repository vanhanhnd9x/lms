<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');
global $USER;

require_login();
$PAGE->set_title(get_string('add_evaluation_student'));
$PAGE->set_heading(get_string('add_evaluation_student'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$course = optional_param('course', '', PARAM_TEXT);
$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
if(is_student()){
     echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
function get_id_group_course_student_evaluation($iduser){
    global $DB;
    $params = array('userid' => $iduser);
    $sql1 = "SELECT DISTINCT `groups`.`id` 
    FROM `groups` 
    JOIN `groups_members` ON `groups`.`id`=`groups_members`.`groupid`
    WHERE `groups_members`.`userid`=:userid";
    $groupid = $DB->get_field_sql($sql1, $params,$strictness=IGNORE_MISSING);
    $sql = "SELECT DISTINCT `course`.`id`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
    WHERE `user`.`id`=:userid";
    $courseid = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    $kq=array(
      '0'=>$groupid,
      '1'=>$courseid
    );
    return $kq;
}
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
if(empty($idstudent)){
   echo displayJsAlert('Error', $CFG->wwwroot . "/manage/evaluation_student/index.php");
}
$cua=get_id_group_course_student_evaluation($idstudent);
$group=$DB->get_record('groups', array('id'=>$cua[0]));
if ($action=='add_evaluation_student') {
  # code...
  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $groupid = optional_param('groupid', '', PARAM_TEXT);
  $school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
  $month = optional_param('month', '', PARAM_TEXT);
  $toplic = optional_param('toplic', '', PARAM_TEXT);
  $attendance = optional_param('attendance', '', PARAM_TEXT);
  $learning_attitude = optional_param('learning_attitude', '', PARAM_TEXT);
  $material = optional_param('material', '', PARAM_TEXT);
  $participation = optional_param('participation', '', PARAM_TEXT);
  $cmt1 = optional_param('cmt1', '', PARAM_TEXT);
  $cmt2 = optional_param('cmt2', '', PARAM_TEXT);
  $cmt3 = optional_param('cmt3', '', PARAM_TEXT);
  $check=check_danh_gia_hoc_sinh_theo_thang_namhoc($idstudent,$month,$school_yearid);
  if($check==1){
    ?>
    <script>
      alert('<?php echo get_string('isset_evaluation_student'); ?>'); 
    </script>
    <?php 
    echo displayJsAlert('Error', $CFG->wwwroot . "/manage/evaluation_student/addnew.php");
  }elseif($check==2){
    // $newid=save_evaluation_student($courseid,$groupid,$idstudent,$month,$school_yearid,$toplic,$attendance,$learning_attitude,$material,$participation,$comment,$other_comment,$USER->id);
    $newid=save_evaluation_student($courseid,$groupid,$idstudent,$month,$school_yearid,$toplic,$attendance,$learning_attitude,$material,$participation,$cmt1,$cmt2,$cmt3,$USER->id);

    if($newid){
      add_notifications_for_student($idstudent,'ĐÁNH GIÁ HỌC SINH','Thông báo về đánh giá của học sinh','/manage/evaluation_student/list_evaluation_student.php');
      echo displayJsAlert('Success', $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student_course.php?course=".$course);
      
    }else{
      echo displayJsAlert('Error', $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student_course.php?course=".$course);
    }
  }
  
}
?>
 
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">

          <form  action="" onsubmit="return validate();" method="post">
            <input type="text" class="form-control" name="action" value="add_evaluation_student" hidden="">
            <input type="text" class="form-control" name="groupid" value="<?php echo $cua[0]; ?>" hidden="">
            <input type="text" class="form-control" name="courseid" value="<?php echo $cua[1]; ?>" hidden="">
            <input type="text" class="form-control" name="school_yearid" value="<?php echo $group->id_namhoc; ?>" hidden="">
            <div class="row">
              <?php echo show_school_class_group_student_evaluation($idstudent); ?>
          <div class="col-md-6">
            <label class="control-label"><?php echo get_string('school_year'); ?></label>
            <input type="text" value="<?php echo show_school_yeah_in_evaluation_by_cua($group->id_namhoc); ?>" class="form-control" disabled>
          </div>
          <div class="col-md-6">
            <label class="control-label"><?php echo get_string('choose_month'); ?></label>
            <select name="month" id="month" class="form-control">
              <option value="1" <?php if(date('m')==1) echo 'selected';?>><?php echo get_string('january'); ?></option>
              <option value="2" <?php if(date('m')==2) echo 'selected';?>><?php echo get_string('february'); ?></option>
              <option value="3" <?php if(date('m')==3) echo 'selected';?>><?php echo get_string('march'); ?></option>
              <option value="4" <?php if(date('m')==4) echo 'selected';?>><?php echo get_string('april'); ?></option>
              <option value="5" <?php if(date('m')==5) echo 'selected';?>><?php echo get_string('may'); ?></option>
              <option value="6" <?php if(date('m')==6) echo 'selected';?>><?php echo get_string('june'); ?></option>
              <option value="7" <?php if(date('m')==7) echo 'selected';?>><?php echo get_string('july'); ?></option>
              <option value="8" <?php if(date('m')==8) echo 'selected';?>><?php echo get_string('august'); ?></option>
              <option value="9" <?php if(date('m')==9) echo 'selected';?>><?php echo get_string('september'); ?></option>
              <option value="10" <?php if(date('m')==10) echo 'selected';?>><?php echo get_string('october'); ?></option>
              <option value="11" <?php if(date('m')==11) echo 'selected';?>><?php echo get_string('november'); ?></option>
              <option value="12" <?php if(date('m')==12) echo 'selected';?>><?php echo get_string('december'); ?></option>
            </select>
          </div>
          <div class="col-md-6">
            <label for="" class="control-label"><?php echo get_string('topic_month'); ?> <span style="color:red">*</span></label>
            <input type="text" class="form-control" value="" name="toplic" required="" >
          </div>
          <div class="col-md-6">
            <label class="control-label"><?php echo get_string('attendance'); ?>(Số ngày đi học)<span style="color:red">*</span></label>
            <div id="chuyencan"> 
              <input type="text" name="attendance" id="" placeholder="<?php echo get_string('attendance'); ?>" class="form-control">
            </div>
          </div>
          <div class="col-md-12">
            <div class="alert-info">
              <h4><?php echo get_string('assessment'); ?></h4>
            </div>
          </div>
          <div class="col-md-12"></div>
          <div class="col-md-4">
            <label class="control-label"><?php echo get_string('learning_attitude') ?><span style="color:red">*</span></label>
            <select name="learning_attitude" id="thaido" class="form-control" required>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
              <option value="D">D</option>
            </select>
          </div>
          <div class="col-md-4">
            <label class="control-label"><?php echo get_string('material'); ?><span style="color:red">*</span></label>
            <select name="material" id="dodung" class="form-control" required>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
              <option value="D">D</option>
            </select>
          </div>
          <div class="col-md-4">
            <label class="control-label"><?php echo get_string('participation'); ?><span style="color:red">*</span></label>
            <select name="participation" id="xdbai" class="form-control" required>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
              <option value="D">D</option>
            </select>
          </div>
          <div class="col-md-12">
              <label for="" class="control-label"><?php echo get_string('other_comment'); ?></label>
              <select name="cmt1" id="loadthaido" class="form-control">
                <option value=""></option>
              </select>
              <select name="cmt2" id="loaddongdung" class="form-control">
                <option value=""></option>
              </select>
               <select name="cmt3" id="loadxdbai" class="form-control">
                <option value=""></option>
              </select>
          </div>
          <div class="col-md-6" style="margin-top: 10px;">
            <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
            <?php 
              print_r(get_string('or'));
              if(!empty($course)){
                ?>
                <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/list_evaluation_student_course.php?course=<?php echo $course; ?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                <?php 
              }
             ?>
            
           
          </div>
          <div class="col-md-6" style="margin-top: 10px;">
             <p id="huongdan">Hướng dẫn <a href="javascript:void(0)"><i class="fa fa-question-circle" aria-hidden="true"></i></a></p>
          </div> 
        </form>
        <!-- The Modal -->
        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('huongdan');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
          modal.style.display = "block";
          // modalImg.src = this.src;
          // captionText.innerHTML = this.alt;
          document.getElementById("img01").src ="<?php echo $CFG->wwwroot; ?>/manage/evaluation_student/img/huongdan.png";
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() { 
          modal.style.display = "none";
        }
        </script>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
</div>
</div>
<?php 
echo $OUTPUT->footer();
if(!empty($cua[0])&&empty(!$cua[1])&&is_numeric($cua[0])&&is_numeric($cua[1])){
  
}else{
  ?>
   <script>
     alert('<?php echo get_string('error_evaluation_student'); ?>');
     $('#submitBtn').prop("disabled", true);
   </script>
  <?php 
}
?>

<script>
  // javascript load thai do
  var thaido1 = $('#thaido').find(":selected").val();
  if (thaido1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido1,function(data){
        $("#loadthaido").html(data);
      });
  }
  $('#thaido').on('change', function() {
    var thaido = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido,function(data){
        $("#loadthaido").html(data);
      });
    }
  });
  // javascript load do dung hoc tao
  var dodung1 = $('#dodung').find(":selected").val();
  if (dodung1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung1,function(data){
        $("#loaddongdung").html(data);
      });
  }
  $('#dodung').on('change', function() {
    var dodung = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung,function(data){
        $("#loaddongdung").html(data);
      });
    }
  });
  // javascript load tham gia xd bai
  var xdbai1 = $('#xdbai').find(":selected").val();
  if (xdbai1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai1,function(data){
        $("#loadxdbai").html(data);
      });
  }
  $('#xdbai').on('change', function() {
    var xdbai = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai,function(data){
        $("#loadxdbai").html(data);
      });
    }
  });
  var userid="<?php echo $idstudent; ?>";
  var school_year = "<?php echo $group->id_namhoc; ?>";
  var course = "<?php echo $cua[1]; ?>";
  $('#month').on('change', function() {
      var month1 = this.value ;
      if(month1){
        $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month1+"&userid="+userid+"&course="+course,function(data){
          $("#chuyencan").html(data);
        });
      }
  }); 
  var month = $('#month').find(":selected").val();
  if(month){
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month+"&userid="+userid+"&course="+course,function(data){
      $("#chuyencan").html(data);
    });
  }
</script>
<style>

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 60%;
  /*max-width: 700px;*/
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
