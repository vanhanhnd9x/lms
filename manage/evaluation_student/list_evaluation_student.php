<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');

$PAGE->set_title(get_string('list_evaluation_student'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$month = optional_param('month', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
if(is_student()){
  $idstudent=$USER->id;
  $courseid=id_nhom_cua_hoc_sinh_home($idstudent);
  $idgvtg=lay_id_gvtg_thuoc_nhom_lop_home($courseid);
  $gvtg=$DB->get_record('user', array(
    'id' => $idgvtg
  ));
}

if($action=='send'){
    $evaluation_studentid      = optional_param('id', 0, PARAM_INT);
    send_evaluation_student($evaluation_studentid);
    echo displayJsAlert('Đã gửi yêu cầu', $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student.php?idstudent=".$idstudent);
}
if($action=='lock'){
    $evaluation_studentid      = optional_param('id', 0, PARAM_INT);
    lock_evaluation_student($evaluation_studentid);
    echo displayJsAlert('Đã khóa đánh giá', $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student.php?idstudent=".$idstudent);
}
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
// view = 1 đánh giá bị khóa
function get_all_evaluation_student_bycua($idstudent,$page,$number,$month=null,$school_year=null){
  global $DB;
  if ($page>1) {
  $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="SELECT * FROM `evalucation_student` WHERE `evalucation_student`.`del`=0 AND `evalucation_student`.`userid`={$idstudent} ";
  $sql_total="SELECT COUNT( DISTINCT `evalucation_student`.`id`) as total from evalucation_student
  WHERE `evalucation_student`.`del`=0 AND `evalucation_student`.`userid`={$idstudent} ";
  if(!empty($month)){
    $sql.="AND `evalucation_student`.`month`={$month} ";
    $sql_total.="AND `evalucation_student`.`month`={$month} ";
  }
  if(!empty($school_year)){
    $sql.="AND `evalucation_student`.`school_yearid`={$school_year} ";
    $sql_total.="AND `evalucation_student`.`school_yearid`={$school_year} ";
  }
  $sql.=" ORDER BY  `evalucation_student`.`time_cre` DESC LIMIT {$start}, {$number}";
  $data=$DB->get_records_sql($sql);
  
  $total_row=$DB->get_records_sql($sql_total);
  foreach ($total_row as $key => $value) {
      # code...
    $total_page=ceil((int)$key / (int)$number);
    break;
  }
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page 
  );
  return $kq;
}

$data=get_all_evaluation_student_bycua($idstudent,$page,$number,$month,$school_yearid);
if ($action=='search') {
 $url= $CFG->wwwroot.'/manage/evaluation_student/list_evaluation_student.php?idstudent='.$idstudent.'&page=';
}else{
  
  $url= $CFG->wwwroot.'/manage/evaluation_student/list_evaluation_student.php?action=search&idstudent='.$idstudent.'&month='.$month.'&school_yearid='.$school_yearid.'&page=';
}



?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <!-- <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/evaluation_student/addnew.php?idstudent='.$idstudent); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
          </div> -->
          <?php
              if(is_student()): 
          ?>
          <div class="col-md-12">
            <h4 class="header-title md-4"><?php echo get_string('GVTG'); ?></h4>
          </div>
          <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                    <label class="control-label"><?php echo get_string('firstname'); ?></label>
                    <input type="" name="" class="form-control"  value="<?php echo $gvtg->lastname.' '.$gvtg->firstname; ?>" disabled>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><?php echo get_string('email'); ?> </label>
                    <input type="" name="" class="form-control"  value="<?php echo $gvtg->email; ?>" disabled>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><?php echo get_string('mobileph'); ?> </label>
                    <input type="" name="" class="form-control"  value="<?php echo $gvtg->phone2; ?>" disabled>
                </div>
              </div>
          </div>
        <?php endif; ?>
          <div class="col-md-12 pull-right">
            <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
             <input type="" name="idstudent" class="form-control" placeholder="Nhập tên..."  value="<?php echo $idstudent; ?>" hidden="">
             <div class="col-4">
              <label for=""><?php echo get_string('choose_month'); ?></label>
              <select name="month" id="" class="form-control">
               <option value=""><?php echo get_string('choose_month'); ?></option>
               <option value="1" <?php if($month==1) echo 'selected'; ?>><?php echo get_string('january'); ?></option>
               <option value="2" <?php if($month==2) echo 'selected'; ?>><?php echo get_string('february'); ?></option>
               <option value="3" <?php if($month==3) echo 'selected'; ?>><?php echo get_string('march'); ?></option>
               <option value="4" <?php if($month==4) echo 'selected'; ?>><?php echo get_string('april'); ?></option>
               <option value="5" <?php if($month==5) echo 'selected'; ?>><?php echo get_string('may'); ?></option>
               <option value="6" <?php if($month==6) echo 'selected'; ?>><?php echo get_string('june'); ?></option>
               <option value="7" <?php if($month==7) echo 'selected'; ?>><?php echo get_string('july'); ?></option>
               <option value="8" <?php if($month==8) echo 'selected'; ?>><?php echo get_string('august'); ?></option>
               <option value="9" <?php if($month==9) echo 'selected'; ?>><?php echo get_string('september'); ?></option>
               <option value="10" <?php if($month==10) echo 'selected'; ?>><?php echo get_string('october'); ?></option>
               <option value="11" <?php if($month==11) echo 'selected'; ?>><?php echo get_string('november'); ?></option>
               <option value="12" <?php if($month==12) echo 'selected'; ?>><?php echo get_string('december'); ?></option>
             </select>
           </div>
           <div class="col-4">
            <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
            <select name="school_yearid" id="" class="form-control">
             <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
             <?php 
             $sql="SELECT * FROM `school_year`";
             $school_year = $DB->get_records_sql($sql);
             foreach ($school_year as $key => $value) {
               # code...
              ?>
              <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
              <?php 
             }
             ?>
           </select>
         </div>
            <div class="col-4">
              <div style="    margin-top: 29px;"></div>
              <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
            </div>
       </form>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th class="text-center">STT</th>
          <th class="text-center"><?php echo get_string('action'); ?></th> 
          <th><?php echo get_string('month'); ?></th>
          <th><?php echo get_string('school_year'); ?></th>
          <th><?php echo get_string('topic_month'); ?></th>
          <th><?php echo get_string('attendance'); ?></th>
          <th><?php echo get_string('learning_attitude') ?></th>
          <th><?php echo get_string('material'); ?></th>
          <th><?php echo get_string('participation'); ?></th>
          <th><?php echo get_string('other_comment'); ?></th>
          
        </tr>
      </thead>
      <tbody>
        <?php 
        if (!empty($data['data'])) {
                                # code...
          // $i=0;
            $i=($page-1)*20;
              foreach ($data['data'] as $value) { 
                $i++;
                $tong=lay_tong_tiet_trong_thang($value->month,$value->school_yearid,$value->courseid);
                if(empty($tong)){ $tong=0;}
                
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <td class="text-center">
                    <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/view_evalution_student.php?idedit=<?php echo $value->id ?>&idstudent=<?php echo $idstudent; ?>" class="mauicon"><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                  <td class="text-center"><?php echo show_month_in_evaluation_by_cua($value->month); ?></td>
                  <td class="text-center"><?php echo show_school_yeah_in_evaluation_by_cua($value->school_yearid); ?></td>
                  <td ><?php echo $value->toplic; ?></td>
                  <td class="text-center"><?php echo $value->attendance.'/',$tong; ?></td>
                  <td class="text-center"><?php echo $value->learning_attitude; ?></td>
                  <td class="text-center"><?php echo $value->material; ?></td>
                  <td class="text-center"><?php echo $value->participation; ?></td>
                  <td>
                    <?php 
                        $text1=hien_thi_nhan_xet_theo_thaidohoctap();
                        $text2=hien_thi_nhan_xet_theo_dodunghoctap();
                        $text3=hien_thi_nhan_xet_theo_thamgiaxaydungbai();
                        // echo $text1[$value->learning_attitude][$value->cmt1],'<br>';
                        // echo $text2[$value->material][$value->cmt2],'<br>';
                        // echo $text3[$value->participation][$value->cmt3],'<br>';
                        if($text1[$value->learning_attitude][$value->cmt1]){
                          echo $text1[$value->learning_attitude][$value->cmt1],'<br>';
                        }
                        if($text2[$value->material][$value->cmt2]){
                          echo $text2[$value->material][$value->cmt2],'<br>';
                        }
                        if($text3[$value->participation][$value->cmt3]){
                          echo $text3[$value->participation][$value->cmt3],'<br>';
                        }
                     ?>

                  </td>
                  
                  <!-- <td class="text-right">
                    <?php if($value->view==0): ?>
                      <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/edit.php?idedit=<?php echo $value->id ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <?php if(is_admin()): ?>
                        <a onclick="return Confirm('Khóa đánh giá','Bạn có muốn khóa đánh giá này!','Yes','Cancel','<?php print new moodle_url('?action=lock',array('id'=>$value->id,'idstudent'=>$idstudent)); ?>')" title="Khóa đánh giá" class="btn btn-warning">Khóa</a>
                      <?php  endif; ?>
                    <?php endif; ?>
                    <?php if($value->view==1): ?>
                      <a onclick="return Confirm('Gửi yêu cầu','Bạn sẽ gửi 1 yêu cầu cho quản trị viên để được xác nhận mở khóa đánh giá!','Yes','Cancel','<?php print new moodle_url('?action=send',array('id'=>$value->id,'idstudent'=>$idstudent)); ?>')" title="Gửi yêu cầu mở khóa" class="btn btn-info">Mở khóa</a>
                    <?php  endif;?>
                    <?php if($value->view==2): ?>
                      <span class="text-white bg-warning p-2">Đang chờ xử lý mở khóa</span>
                    <?php  endif;?>
                      <a href="javascript:void(0)" onclick="delete_evaluation(<?php echo $value->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                  </td> -->
                </tr>
              <?php }
        }
        ?>
      </tbody>
    </table>
    <?php 
    if ($data['total_page']) {
      if ($page > 2) { 
        $startPage = $page - 2; 
      } else { 
        $startPage = 1; 
      } 
      if ($data['total_page'] > $page + 2) { 
        $endPage = $page + 2; 
      } else { 
        $endPage =$data['total_page']; 
      }
    }
    ?>
    <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <?php 
        for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
          ?>
          <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
          <?php 
        }
        ?>
        <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<style>
    .mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }
</style>
<?php
echo $OUTPUT->footer();
$action = optional_param('action', '', PARAM_TEXT);
if ($action='delete') {
    # code...
  $iddelete = optional_param('iddelete', '', PARAM_TEXT);
  $return=delete_student($iddelete);
  if($return){
    echo displayJsAlert('Xoa thanh cong', $CFG->wwwroot . "/manage/student/index.php");
  }
}
?>

<script>
  function delete_evaluation(iddelete){
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/evaluation_student/list_evaluation_student.php?idstudent=<?php echo $idstudent; ?>";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php";
        var e=confirm('<?php echo get_string('delete_evaluation_student'); ?>');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {iddelete:iddelete} ,
                success: function (response) {
                 window.location=urlweb;
             }
         });
        }
    }
</script>
