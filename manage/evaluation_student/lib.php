<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function show_month_in_evaluation_by_cua($number){
  switch ($number) {
    case 1: echo get_string('january'); break;
    case 2: echo get_string('february'); break;
    case 3: echo get_string('march'); break;
    case 4: echo get_string('april'); break;
    case 5: echo get_string('may'); break;
    case 6: echo get_string('june'); break;
    case 7: echo get_string('july'); break;
    case 8: echo get_string('august'); break;
    case 9: echo get_string('september'); break;
    case 10: echo get_string('october'); break;
    case 11: echo get_string('november'); break;
    case 12: echo get_string('december'); break;
  }
}
function get_all_evaluation_course($course,$page,$number,$month=null,$school_yearid=null){
  global $DB;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="SELECT `evalucation_student`.*, `user`.`code`,`user`.`lastname`,`user`.`firstname` FROM `evalucation_student`
  JOIN `user` ON `user`.`id` =`evalucation_student`.`userid`
  WHERE `user`.`del`=0
  AND `evalucation_student`.`del`=0 
  AND `evalucation_student`.`courseid` ={$course}";
  $sql_total="SELECT COUNT( DISTINCT `evalucation_student`.`id`) 
  FROM `evalucation_student`
  JOIN `user` ON `user`.`id` =`evalucation_student`.`userid`
  WHERE `user`.`del`=0
  AND `evalucation_student`.`del`=0  
  AND `evalucation_student`.`courseid` ={$course}";
  if(!empty($month)){
    $sql.=" AND `evalucation_student`.`month` ={$month} ";
    $sql_total.=" AND `evalucation_student`.`month` ={$month} ";
  }
  if(!empty($school_yearid)){
    $sql.=" AND `evalucation_student`.`school_yearid` ={$school_yearid} ";
    $sql_total.=" AND `evalucation_student`.`school_yearid` ={$school_yearid} ";
  }
  $sql.=" ORDER BY `user`.`firstname` ASC LIMIT {$start}, {$number}";
  $data=$DB->get_records_sql($sql);
  $total_row=$DB->get_records_sql($sql_total);
  foreach ($total_row as $key => $value) {
              # code...
    $total_page=ceil((int)$key / (int)$number);
    break;
  }
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page
  );
  return $kq;
}
function laydanhsachhocsinhcuahomlop_danhgiahocsinh($course){
  global $DB;
  $time=time();
  $sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname` 
  FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
  JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
  JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
  JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
  WHERE `role_assignments`.`roleid`= 5 
  AND `user`.`del`=0 
  AND `course`.`id`={$course}
  AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)
  ORDER BY `user`.`firstname` ASC
  ";
  $data=$DB->get_records_sql($sql);
  return $data;
}
function save_evaluation_student($courseid,$groupid,$userid,$month,$school_yearid,$toplic,$attendance,$learning_attitude,$material,$participation,$cmt1,$cmt2,$cmt3,$evalucation_by){
	global $USER, $CFG, $DB;
	$record = new stdClass();
	$record->courseid =$courseid;
	$record->groupid =$groupid;
	$record->userid =$userid;
	$record->month =$month;
	$record->school_yearid =$school_yearid;
	$record->toplic =$toplic;
	$record->attendance =$attendance;
	$record->learning_attitude =$learning_attitude;
	$record->material =$material;
    $record->participation =$participation;
    $record->cmt1 =$cmt1;
    $record->cmt2 =$cmt2;
	$record->cmt3 =$cmt3;
	$record->evalucation_by =$evalucation_by;
	
	$record->time_cre = time();
	$record->del = 0;
	$record->view = 0;
	$id_new = $DB->insert_record('evalucation_student', $record);
	return $id_new;
}
function check_danh_gia_hoc_sinh_theo_thang_namhoc($userid,$month,$school_yearid){
	global $CFG, $DB;
	$sql="SELECT * FROM `evalucation_student` 
	WHERE  `evalucation_student`.`userid`={$userid}
	AND    `evalucation_student`.`month`={$month}
	AND    `evalucation_student`.`school_yearid`={$school_yearid}
	AND    `evalucation_student`.`del`=0
	";
	$check = $DB->get_records_sql($sql);
	if(!empty($check)){
		return 1;
	}else{
		return 2;
	}
}
function update_evaluation_student($idevaluation,$school_yearid,$month,$toplic,$attendance,$learning_attitude,$material,$participation,$cmt1,$cmt2,$cmt3){
	global $DB;
	$record = new stdClass();
	$record->id = $idevaluation;
	$record->school_yearid =$school_yearid;
	$record->month =$month;
	$record->toplic =$toplic;
	$record->attendance =$attendance;
	$record->learning_attitude =$learning_attitude;
	$record->material =$material;
	$record->participation =$participation;
    $record->cmt1 =$cmt1;
	$record->cmt2 =$cmt2;
	$record->cmt3 =$cmt3;
	return $DB->update_record('evalucation_student', $record, false);
}
function delete_evaluation_student($id){
	global $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->del =1;
	return $DB->update_record('evalucation_student', $record, false);
}
function show_school_class_group_student_evaluation($iduser){
  
    global $DB;
    $params = array('userid' => $iduser);
    $sql1 = "SELECT DISTINCT `groups`.`id` 
    FROM `groups` 
    JOIN `groups_members` ON `groups`.`id`=`groups_members`.`groupid`
    WHERE `groups_members`.`userid`=:userid";
    $groupid = $DB->get_field_sql($sql1, $params,$strictness=IGNORE_MISSING);
    $group=$DB->get_record('groups', array(
      'id' => $groupid
    ));
    $school=$DB->get_record('schools', array(
      'id' => $group->id_truong
    ));
    $sql = "SELECT DISTINCT `course`.`fullname`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
    WHERE `user`.`id`=:userid";
    $course = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);

    $student=$DB->get_record('user', array(
      'id' => $iduser
    ));
    echo'<div class="col-md-6">
        <label for="" class="control-label">'.get_string('schools').'<span style="color:red">*</span></label>
        <input type="text" class="form-control" value="'.$school->name.'"  disabled="">
        </div>
        <div class="col-md-6">
        <label for="" class="control-label">'.get_string('class').' <span style="color:red">*</span></label>
        <input type="text" class="form-control" value="'.$group->name.'"  disabled="">
        </div>
        <div class="col-md-6">
        <label for="" class="control-label">'.get_string('classgroup').' <span style="color:red">*</span></label>
        <input type="text" class="form-control" value="'.$course.'"  disabled="">
        </div>
        <div class="col-md-6">
        <label for="" class="control-label">'.get_string('student').' <span style="color:red">*</span></label>
        <input type="text" class="form-control" value="'.$student->lastname.' '.$student->firstname.'"  disabled="">
        </div>';
}
function get_school_class_group_student_evaluation($iduser){
  
    global $DB;
    $params = array('userid' => $iduser);
    $sql1 = "SELECT DISTINCT `groups`.`id` 
    FROM `groups` 
    JOIN `groups_members` ON `groups`.`id`=`groups_members`.`groupid`
    WHERE `groups_members`.`userid`={$iduser}";
    $groupid = $DB->get_field_sql($sql1,null,$strictness=IGNORE_MISSING);
    $group=$DB->get_record('groups', array(
      'id' => $groupid
    ));
    $school=$DB->get_record('schools', array(
      'id' => $group->id_truong
    ));
    $sql = "SELECT DISTINCT `course`.`fullname`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
    WHERE `user`.`id`={$iduser}";
    $course = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    echo'<td>'.$school->name.'</td>
        <td>'.$group->name.'</td>
        <td>'.$course.'</td>';
}
function show_school_yeah_in_evaluation_by_cua($id){
  global $DB;
  $yeah=$DB->get_record('school_year', array(
      'id' => $id
    ));
  if($yeah){
     echo $yeah->sy_start,'-',$yeah->sy_end;
 }else{
    echo'';
 }
 
}

function hien_thi_nhan_xet_theo_thaidohoctap(){
    $thaido=array(
        'A'=>array(
            '1'=>'',
            '2'=>'Con hợp tác tốt và biết giúp đỡ bạn bè trong học tập. Con hãy tiếp tục phát huy nhé!',
            '3'=>'Con tiến bộ nhiều so với tháng trước. Con tiếp tục phát huy nhé!',
            '4'=>'Con tự tin và có ý thức học tốt.',
            '5'=>'Con có ý thức học tập tốt, trong giờ con ngồi học ngoan và tập trung nghe giảng.',
            '6'=>'Con có thái độ học tập tích cực và siêng năng. Giáo viên tin con sẽ tiến bộ nhiều nếu được phụ huynh động viên thường xuyên.',
        ),
        'B'=>array(
            '1'=>'',
            '2'=>'Con học tốt nhưng thi thoảng con còn mất tập trung trong giờ học.',
            '3'=>'Con là một học sinh năng động, hoạt bát trong lớp nhưng đôi khi con còn hơi quá hiếu động và làm ảnh hưởng đến các bạn khác.',
            '4'=>'Con ngoan, ý thức học tập tốt nhưng con cần hòa đồng với các bạn hơn con nhé!',
            '5'=>'Con ngoan, có chú ý bài. Tuy nhiên gần đây con chưa tập trung lắm trong giờ học.',
            '6'=>'Con khá tích cực nhưng cũng vẫn còn đôi lần đi lại tự do và nói chuyện trong giờ.',
        ),
        'C'=>array(
            '1'=>'',
            '2'=>'Con còn trêu bạn trong giờ học. Con lưu ý điều chỉnh để không làm ảnh hưởng đến việc học tập của bản thân và của bạn nhé!',
            '3'=>'Con dễ không vui hoặc bực bội khi đội của con chưa chiến thắng. Mong phụ huynh động viên con để con có thái độ tích cực hơn trong các hoạt động học tập!',
            '4'=>'Con còn hay mất tập trung và nghịch đồ chơi trong lớp. Con lưu ý không mang đồ chơi đến lớp và tập trung hơn vào bài học nhé!',
        ),
        'D'=>array(
            '1'=>'',
            '2'=>'Con cần chú ý nghe hướng dẫn của giáo viên hơn và không nên để ý đến các bạn hay nói chuyện riêng trong lớp.',
            '3'=>'Con còn nói chuyện và làm ảnh hưởng đến các bạn khác trong lớp. Con hãy tập trung nghe lời giáo viên hướng dẫn trong lớp nhé!',
        ),

    );
    return $thaido;

}

function hien_thi_nhan_xet_theo_thamgiaxaydungbai(){
    $thamgiaxaydungbai=array(
        'A'=>array(
            '1'=>'',
            '2'=>'Con hăng hái tham gia phát biểu ý kiến trong lớp.',
            '3'=>'Con làm việc nhóm tốt, con thường xuyên phối hợp cùng các bạn trong nhóm.',
            '4'=>'Con tích cực và trả lời tốt trong các hoạt động học tập theo yêu cầu của giáo viên. ',
            '5'=>'Con luôn xung phong rất nhanh trước các bạn khác để trả lời câu hỏi của giáo viên.',
            '6'=>'Con mạnh dạn trong giao tiếp, trả lời câu hỏi và biết trình bày ý kiến của mình trước tập thể lớp.',
            '7'=>'Con biết kết hợp lời nói với cử chỉ, điệu bộ, nét mặt khi giao tiếp với bạn và khi trả lời câu hỏi của giáo viên.',
            '8'=>'Con biết vận dụng những điều đã học vào thực tế, tham gia thực hành và vận dụng vào các hoạt động nói tốt.',
            '9'=>'Con hay tương tác, thực hành với các bạn khi thầy giao các nhiệm vụ học tập và các trò chơi thực hành trên lớp.',
            '10'=>'Con nhanh nhẹn, năng động trong lớp. Các hoạt động thực hành tham gia năng nổ và nhiệt tình, con phối hợp tốt với các bạn.',
        ),
        'B'=>array(
            '1'=>'',
            '2'=>'Con có tinh thần và thái độ học tập tích cực nhưng cần rèn thêm về kỹ năng đọc.',
            '3'=>'Con hiểu bài nhưng áp dụng còn lúng túng, nên khi xây dựng bài còn chưa thể trình bày thoát ý, chưa diễn đạt được một cách trọn vẹn.',
            '4'=>'Con còn nhút nhát, rụt rè khi giơ tay phát biểu, khi giáo viên mời con thì con vẫn còn lưỡng lự khi trình bày ý kiến.',
            '5'=>'Đôi lúc con cần chú ý thầy giảng nhiều hơn. Con cần hăng hái phát biểu trên lớp nhiều hơn nữa.',
            '6'=>'Con có tinh thần học tập tích cực nhưng cần rèn thêm về kỹ năng đọc. Đôi khi con sử dụng cấu trúc câu chưa tốt cần chú ý luyện tập thêm.Giọng đọc còn nhỏ.',
        ),
        'C'=>array(
            '1'=>'',
            '2'=>'Con chưa mạnh dạn trong giao tiếp, chưa tự tin nói, bày tỏ ý kiến của mình trong nhóm, trước lớp.',
            '3'=>'Con cần chú ý các âm khó như ( "r", "s", "j", "z", "t", "k", "c") trong khi nói hoặc đọc',
            '4'=>'Con còn đùn đẩy cho bạn đứng dậy để trả lời câu hỏi, báo cáo học tập khi làm việc nhóm.',
        ),
        'D'=>array(
            '1'=>'',
            '2'=>'Kỹ năng nghe của con còn hạn chế, khi nghe nên chú ý vào các từ khóa.',
            '3'=>'Con sử dụng từ vựng còn chưa tốt, tiếp thu kiến thức còn chậm nên khi đứng dậy trả lời còn sai nhiều chỗ.',
        ),

    );
    return $thamgiaxaydungbai;

}

function hien_thi_nhan_xet_theo_dodunghoctap(){
    $dodunghoctap=array(
        'A'=>array(
            '1'=>'',
            '2'=>'Con mang đầy đủ sách và đồ dùng học tập.',
            '3'=>'Con chuẩn bị đầy đủ đồ dùng học tập trước các buổi học. ',
            '4'=>'Con giữ gìn sách và đồ dùng học tập cẩn thận.',
        ),
        'B'=>array(
            '1'=>'',
            '2'=>'Con nhớ mang sách nhưng thường quên mang đồ dùng học tập.',
            '3'=>'Con thỉnh thoảng còn quên không mang đồ dùng học tập',
            '4'=>'Đồ dùng học tập của con còn chưa đầy đủ',
        ),
        'C'=>array(
            '1'=>'',
            '2'=>'Con cần chủ động mang sách,vở và đồ dùng học tập của mình đến lớp',
            '3'=>'Con cần mang đầy đủ sách khi đến lớp.',
        ),
        'D'=>array(
            '1'=>'',
            '2'=>'Con cần mang đầy đủ sách và đồ dùng học tập đến lớp.',
            '3'=>'Con thường xuyên mượn đồ dùng học tập của bạn',
        ),

    );
    return $dodunghoctap;

}
function lay_tong_tiet_trong_thang($month,$school_year,$course){
    global $DB;
    $sql = "
    SELECT DISTINCT `diemdanh`.`sotiet` FROM `diemdanh` 
    WHERE `diemdanh`.`month` = {$month}  
    AND `diemdanh`.`schoolyearid` = {$school_year}
    AND `diemdanh`.`course_id` = {$course} 
    AND `diemdanh`.`sotiet` IS NOT NULL
    ";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function lay_so_ngay_nghi_cua_hs($month,$school_year,$userid){
    global $DB;
    $sql = "SELECT COUNT(DISTINCT `diemdanh`.`id`) FROM `diemdanh`  
    WHERE `diemdanh`.`month` = {$month} 
    AND  `diemdanh`.`schoolyearid` = {$school_year}
    AND  `diemdanh`.`userid` = {$userid}";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function lay_id_lop_cua_nhom_taidanhgia($course){
    global $DB;
    $sql=" SELECT DISTINCT `groups`.`id` FROM `groups` JOIN `group_course` ON `groups`.`id`= `group_course`.`group_id` WHERE `group_course`.`course_id`={$course}";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function get_school_class_group_student_evaluation2($iduser){
    global $DB;
    $params = array('userid' => $iduser);
    $sql1 = "SELECT DISTINCT `groups`.`id` 
    FROM `groups` 
    JOIN `groups_members` ON `groups`.`id`=`groups_members`.`groupid`
    WHERE `groups_members`.`userid`=:userid";
    $groupid = $DB->get_field_sql($sql1, $params,$strictness=IGNORE_MISSING);
    $group=$DB->get_record('groups', array(
      'id' => $groupid
    ));
    $school=$DB->get_record('schools', array(
      'id' => $group->id_truong
    ));
    $sql = "SELECT DISTINCT `course`.`fullname`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
    WHERE `user`.`id`=:userid";
    $course = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    echo'<td>'.$school->name.'</td>
        <td>'.$group->name.'</td>
        <td>'.$course.'</td>';
}
function get_all_evaluation_studentbycua($schoolid=null,$block_student=null,$groupid=null,$courseid=null,$code=null,$name=null,$month=null,$school_yearid=null,$page,$number,$evalucation_by=null){
  global $DB;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="SELECT DISTINCT `evalucation_student`.*,`user`.`code`, `user`.`firstname`,`user`.`lastname`, `course`.`fullname` 
  FROM `evalucation_student` 
  JOIN `user` ON `evalucation_student`.`userid`= `user`.`id` 
  JOIN `course` ON `course`.`id` =`evalucation_student`.`courseid`
  JOIN `groups` on `groups`.`id` =`evalucation_student`.`groupid` 
  WHERE `user`.`del` =0 
  AND `evalucation_student`.`del`=0 ";
  $sql_total="SELECT COUNT( DISTINCT `evalucation_student`.`id`) 
  FROM `evalucation_student` 
  JOIN `user` ON `evalucation_student`.`userid`= `user`.`id` 
  JOIN `course` ON `course`.`id` =`evalucation_student`.`courseid`
  JOIN `groups` on `groups`.`id` =`evalucation_student`.`groupid` 
  WHERE `user`.`del` =0 
  AND `evalucation_student`.`del`=0";
  if(!empty($schoolid)){
    $sql.=" AND `groups`.`id_truong`= {$schoolid}";
    $sql_total.=" AND `groups`.`id_truong`= {$schoolid}";
  }
  if(!empty($block_student)){
    $sql.=" AND `groups`.`id_khoi`= {$block_student}";
    $sql_total.=" AND `groups`.`id_khoi`= {$block_student}";
  }
  if(!empty($groupid)){
    $sql.=" AND `groups`.`id`= {$groupid}";
    $sql_total.=" AND `groups`.`id`= {$groupid}";
  }
  if(!empty($courseid)){
    $sql.=" AND `course`.`id`= {$courseid}";
    $sql_total.=" AND `course`.`id`= {$courseid}";
  }
  if(!empty($code)){
    $sql.=" AND `user`.`code`= {$code}";
    $sql_total.=" AND `user`.`code`= {$code}";
  }
  if(!empty($name)){
    $sql.=" AND (`user`.`firstname` LIKE '%{$name}%' OR `user`.`firstname` LIKE '%{$lastname}%')";
    $sql_total.=" AND (`user`.`firstname` LIKE '%{$name}%' OR `user`.`firstname` LIKE '%{$lastname}%')";
  }
  if(!empty($month)){
    $sql.=" AND `evalucation_student`.`month`= {$month}";
    $sql_total.=" AND `evalucation_student`.`month`= {$month}";
  }
  if(!empty($school_yearid)){
    $sql.=" AND `evalucation_student`.`school_yearid`= {$school_yearid}";
    $sql_total.=" AND `evalucation_student`.`school_yearid`= {$school_yearid}";
  }
  if(!empty($evalucation_by)){
    $sql.=" AND `evalucation_student`.`evalucation_by`= {$evalucation_by}";
    $sql_total.=" AND `evalucation_student`.`evalucation_by`= {$evalucation_by}";
  }
  $sql.=" ORDER BY `evalucation_student`.`id` DESC LIMIT {$start}, {$number}";
  $data=$DB->get_records_sql($sql);
  $total_row=$DB->get_records_sql($sql_total);
  foreach ($total_row as $key => $value) {
              # code...
    $total_page=ceil((int)$key / (int)$number);
    break;
  }
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page
  );
  return $kq;
}

function get_gvnn_gvtg(){
  global $DB;
  $sql = "SELECT DISTINCT user.*,roleid  FROM user JOIN role_assignments ON user.id = role_assignments.userid WHERE (role_assignments.roleid=8 OR role_assignments.roleid=10) AND user.del=0";
  return $DB->get_records_sql($sql);
}
?>