<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');


require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$month = optional_param('month', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$course = optional_param('course', '', PARAM_TEXT);
$infocourse=$DB->get_record('course', array(
  'id' => $course
));
$tile=get_string('list_evaluation_student').' '.get_string('classgroup') .' '.$infocourse->fullname;
$PAGE->set_title($tile);
echo $OUTPUT->header();
if(empty($course)){
  echo displayJsAlert('Error', $CFG->wwwroot . "/manage/courses.php");
}
if($action=='send'){
    $evaluation_studentid      = optional_param('id', 0, PARAM_INT);
    send_evaluation_student($evaluation_studentid);
    echo displayJsAlert('Đã gửi yêu cầu', $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student_course.php?course=".$course);
}
if($action=='lock'){
    $evaluation_studentid      = optional_param('id', 0, PARAM_INT);
    lock_evaluation_student($evaluation_studentid);
    echo displayJsAlert('Đã khóa đánh giá', $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student_course.php?course=".$course);
}
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;

$groupid=lay_id_lop_cua_nhom_taidanhgia($course); 
$group=$DB->get_record('groups', array(
  'id' => $groupid
)); 
$thongtin_truong=$DB->get_record('schools', array(
    'id' => $group->id_truong
    ),
    $fields='id,level,evaluation_student', $strictness=IGNORE_MISSING
  );
$data=get_all_evaluation_course($course,$page,$number,$month,$group->id_namhoc);
if ($action=='search') {
 $url= $CFG->wwwroot.'/manage/evaluation_student/list_evaluation_student_course.php?action=search&course='.$course.'&month='.$month.'&school_yearid='.$school_yearid.'&page=';
}else{
   $url= $CFG->wwwroot.'/manage/evaluation_student/list_evaluation_student_course.php?course='.$course.'&page=';
}
$list_student=laydanhsachhocsinhcuahomlop_danhgiahocsinh($course);

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='course';
$name1='list_evaluation_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <?php 
            switch ($thongtin_truong->evaluation_student) {
              case 2:
                ?>
                  <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong><?php echo get_string('error'); ?>!</strong> <?php echo get_string('error_participate_evaluation_student'); ?>.
                    </div>
                  </div>
                <?php 
              break;
            }
          ?>
         
          <div class="col-md-2" id="add_new">
           <!--  <a href="<?php echo new moodle_url('/manage/evaluation_student/add_evaluation_in_course.php?course='.$course); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a> -->
            <a href="#myModal" class="btn btn-success" data-toggle="modal"  ><?php echo get_string('new'); ?></a>
          </div>
          <div class="col-md-9 pull-right">
            <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
             <input type="" name="course" class="form-control" placeholder="Nhập tên..."  value="<?php echo $course; ?>" hidden="">
             <div class="col-4">
              <label for=""><?php echo get_string('choose_month'); ?></label>
              <select name="month" id="" class="form-control">
               <option value=""><?php echo get_string('choose_month'); ?></option>
               <option value="1" <?php if($month==1) echo 'selected'; ?>><?php echo get_string('january'); ?></option>
               <option value="2" <?php if($month==2) echo 'selected'; ?>><?php echo get_string('february'); ?></option>
               <option value="3" <?php if($month==3) echo 'selected'; ?>><?php echo get_string('march'); ?></option>
               <option value="4" <?php if($month==4) echo 'selected'; ?>><?php echo get_string('april'); ?></option>
               <option value="5" <?php if($month==5) echo 'selected'; ?>><?php echo get_string('may'); ?></option>
               <option value="6" <?php if($month==6) echo 'selected'; ?>><?php echo get_string('june'); ?></option>
               <option value="7" <?php if($month==7) echo 'selected'; ?>><?php echo get_string('july'); ?></option>
               <option value="8" <?php if($month==8) echo 'selected'; ?>><?php echo get_string('august'); ?></option>
               <option value="9" <?php if($month==9) echo 'selected'; ?>><?php echo get_string('september'); ?></option>
               <option value="10" <?php if($month==10) echo 'selected'; ?>><?php echo get_string('october'); ?></option>
               <option value="11" <?php if($month==11) echo 'selected'; ?>><?php echo get_string('november'); ?></option>
               <option value="12" <?php if($month==12) echo 'selected'; ?>><?php echo get_string('december'); ?></option>
             </select>
           </div>
           <div class="col-4">
            <label for=""><?php echo get_string('school_year'); ?></label>
            <input type="text" class="form-control" value="<?php echo show_school_yeah_in_evaluation_by_cua($group->id_namhoc) ?>" disabled>
            <!-- <select name="school_yearid" id="" class="form-control">
             <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
             <?php 
             $sql="SELECT * FROM `school_year`";
             $school_year = $DB->get_records_sql($sql);
             foreach ($school_year as $key => $value) {
               # code...
              ?>
              <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
              <?php 
             }
             ?>
           </select> -->
         </div>
            <div class="col-4">
              <div style="    margin-top: 29px;"></div>
              <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
            </div>
       </form>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>STT</th>
          <th class="text-right"><?php echo get_string('action'); ?></th>
          <th><?php echo get_string('student'); ?></th>
          <th><?php echo get_string('month'); ?></th>
          <th><?php echo get_string('school_year'); ?></th>
          <th><?php echo get_string('topic_month'); ?></th>
          <th><?php echo get_string('attendance'); ?></th>
          <th><?php echo get_string('learning_attitude') ?></th>
          <th><?php echo get_string('material'); ?></th>
          <th><?php echo get_string('participation'); ?></th>
          <th><?php echo get_string('other_comment'); ?></th>
          
        </tr>
      </thead>
      <tbody>
        <?php 
        if (!empty($data['data'])) { 
                                # code...
          // $i=0;
            $i=($page-1)*20;
              foreach ($data['data'] as $value) { 
                $i++;
                $tong=lay_tong_tiet_trong_thang($value->month,$value->school_yearid,$value->courseid);
                if(empty($tong)){ $tong=0;}
                ?>
                <tr>
                  <td class="text-center"><?php echo $i; ?></td>
                  <td class="text-right">
                    <?php if($value->view==0): ?>
                      <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/edit.php?idedit=<?php echo $value->id ?>&course=<?php echo $course; ?>" class="mauicon tooltip-animation"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <?php if(is_admin()): ?>
                        <a onclick="return Confirm('Khóa đánh giá','Bạn có muốn khóa đánh giá này!','Yes','Cancel','<?php print new moodle_url('?action=lock',array('id'=>$value->id,'course'=>$course)); ?>')" title="Khóa đánh giá" class="mauicon tooltip-animation"><i class="fa fa-lock" aria-hidden="true"></i></a>
                      <?php  endif; ?>
                    <?php endif; ?>
                    <?php if($value->view==1): ?>
                      <a onclick="return Confirm('Gửi yêu cầu','Bạn sẽ gửi 1 yêu cầu cho quản trị viên để được xác nhận mở khóa đánh giá!','Yes','Cancel','<?php print new moodle_url('?action=send',array('id'=>$value->id,'course'=>$course)); ?>')" title="Gửi yêu cầu mở khóa" class=" mauicon tooltip-animation"><i class="fa fa-unlock-alt" aria-hidden="true"></a>
                    <?php  endif;?>
                    <?php if($value->view==2): ?>
                      <span class="mauicon">Đang chờ xử lý mở khóa</span>
                    <?php  endif;?>
                      <!-- <a href="javascript:void(0)" onclick="delete_evaluation(<?php echo $value->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                  </td>
                  <td><?php echo $value->lastname,' ',$value->firstname; ?></td>
                  <td class="text-center"><?php echo show_month_in_evaluation_by_cua($value->month); ?></td>
                  <td class="text-center"><?php echo show_school_yeah_in_evaluation_by_cua($value->school_yearid); ?></td>
                  <td ><?php echo $value->toplic; ?></td>
                  <td class="text-center"><?php echo $value->attendance.'/'.$tong; ?></td>
                  <td class="text-center"><?php echo $value->learning_attitude; ?></td>
                  <td class="text-center"><?php echo $value->material; ?></td>
                  <td class="text-center"><?php echo $value->participation; ?></td>
                  <td>
                    <?php 
                        $text1=hien_thi_nhan_xet_theo_thaidohoctap();
                        $text2=hien_thi_nhan_xet_theo_dodunghoctap();
                        $text3=hien_thi_nhan_xet_theo_thamgiaxaydungbai();
                        // echo $text1[$value->learning_attitude][$value->cmt1],'<br>';
                        // echo $text2[$value->material][$value->cmt2],'<br>';
                        // echo $text3[$value->participation][$value->cmt3],'<br>';
                        if($text1[$value->learning_attitude][$value->cmt1]){
                          echo $text1[$value->learning_attitude][$value->cmt1],'<br>';
                        }
                        if($text2[$value->material][$value->cmt2]){
                          echo $text2[$value->material][$value->cmt2],'<br>';
                        }
                        if($text3[$value->participation][$value->cmt3]){
                          echo $text3[$value->participation][$value->cmt3],'<br>';
                        }
                     ?>

                  </td>
                  
                </tr>
              <?php }
        }
        ?>
      </tbody>
    </table>
    <?php 
    if ($data['total_page']) {
      if ($page > 2) { 
        $startPage = $page - 2; 
      } else { 
        $startPage = 1; 
      } 
      if ($data['total_page'] > $page + 2) { 
        $endPage = $page + 2; 
      } else { 
        $endPage =$data['total_page']; 
      }
      ?>
      <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
      <nav aria-label="Page navigation example">
        <ul class="pagination">
          <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
            <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <?php 
          for ($i=$startPage; $i <=$endPage ; $i++) { 
                                          # code...
            ?>
            <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
            <?php 
          }
          ?>
          <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
            <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>
      <?php 
    }
    ?>
    
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<div class="container">
<!-- <a href="#myModal" class="btn btn-primary" data-toggle="modal">Launch demo modal</a>
-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <!-- <h4 class="modal-title">Lựa chọn học sinh</h4> -->
        </div>
        <div class="modal-body">
          <form action="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/addnew.php" method="get" >
            <div class="row">
              <div class="col-md-12">
                <label for="" class=""><?php echo get_string('student'); ?><span style="color:red">*</span></label>
                <select name="idstudent" id="" class="form-control" required="">
                  <option value=""><?php echo get_string('student'); ?></option>
                  <?php 
                     foreach ($list_student as  $value) {
                       # code...
                      echo '<option value="'.$value->id.'">'.$value->lastname.' '.$value->firstname.'</option>';
                     }
                   ?>
                  
                </select>
                <input type="text" hidden="" name="course" value="<?php echo $course; ?>">
              </div>
              <div class="col-md-12" style="margin-top: 10px;">
                  <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('next')) ?> " id="submitBtn">
                  <button type="button" class="btn btn-danger" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
              </div>
            </div>
          </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


  </div>

  <style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<style>
    .mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }
</style>
<?php
echo $OUTPUT->footer();
 switch ($thongtin_truong->evaluation_student) {
    case 2:
    ?>
    <script>
      $("#add_new").html('<a href="<?php echo $CFG->wwwroot; ?>//manage/courses.php" class="btn btn-success"   ><?php echo get_string('back'); ?></a>');
    </script>
    <?php 
    break;
 }
  

?>

<script>
  function delete_evaluation(iddelete){
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/evaluation_student/list_evaluation_student_course.php?course=<?php echo $course; ?>";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php";
        var e=confirm('<?php echo get_string('delete_evaluation_student'); ?>');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {iddelete:iddelete} ,
                success: function (response) {
                 window.location=urlweb;
             }
         });
        }
    }
</script>
