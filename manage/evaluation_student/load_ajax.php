<?php 
 require_once('../../config.php');
 require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
 global $USER, $CFG, $DB;

if(!empty($_POST['iddelete'])){
		$iddelete=$_POST['iddelete'];
		delete_evaluation_student($iddelete);
}

if(!empty($_GET['school_year'])&&!empty($_GET['month'])&&!empty($_GET['userid'])&&!empty($_GET['course'])){
	$school_year=$_GET['school_year'];
	$month=$_GET['month'];
	$userid=$_GET['userid'];
	$course=$_GET['course'];
	$tongtiet=lay_tong_tiet_trong_thang($month,$school_year,$course); 
	$ngaynghi=lay_so_ngay_nghi_cua_hs($month,$school_year,$userid);
	$songayhoc=$tongtiet-$ngaynghi;
	if($songayhoc==0){
		?>
		<script>
			$('#submitBtn').prop("disabled", true);
			alert('Hãy tiến hành điểm danh cho học sinh');
		</script>
		<?php 
	}else{
		?>
		<script>
			$('#submitBtn').removeAttr("disabled");
		</script>
		<?php 
	}
	?>
	<i class="fa fa-pencil" aria-hidden="true" id="edit"></i>
	<i class="fa fa-times" aria-hidden="true" id="remove"></i>
	<input type="number" value="<?php echo $songayhoc; ?>" name="attendance" id="giatrishow" class="form-control" placeholder="<?php echo get_string('attendance'); ?>" required min="1" disabled >

	<input type="number" value="<?php echo $songayhoc; ?>" name="attendance" id="giatrian" class="form-control" placeholder="<?php echo get_string('attendance'); ?>" hidden >

	<input type="number" value="<?php echo $tongtiet; ?>" name="total_lessons" id="" class="form-control" placeholder="<?php echo get_string('attendance'); ?>" hidden>

	<script>
		$("#remove").hide();
		$('#edit').click(function(){
			$("#edit").hide();
			$("#remove").show();
			$('#giatrishow').removeAttr("disabled");
			$('#giatrian').prop("disabled", true);

		});
		$('#remove').click(function(){
			$("#edit").show();
			$("#remove").hide();
			$('#giatrishow').prop("disabled", true);
			$('#giatrian').removeAttr("disabled");

		});
	</script>
	<?php 
	// echo'<input type="number" value="'.$songayhoc.'" name="attendance" id="" class="form-control" placeholder="'.get_string('attendance').'">';
}
if(!empty($_GET['thaido'])){
	$thaido=$_GET['thaido'];
	$list=hien_thi_nhan_xet_theo_thaidohoctap();
	foreach ($list[$thaido] as $key => $value) {
		# code...
		?>
		<option value="<?php echo $key; ?>" <?php if(!empty($_GET['idcmt1'])&&($_GET['idcmt1']==$key)) echo 'selected'; ?>><?php echo $value; ?></option>
		<?php 
	}
}
if(!empty($_GET['dodung'])){
	$dodung=$_GET['dodung'];
	$list=hien_thi_nhan_xet_theo_dodunghoctap();
	foreach ($list[$dodung] as $key => $value) {
		# code...
		?>
		<option value="<?php echo $key; ?>" <?php if(!empty($_GET['idcmt2'])&&($_GET['idcmt2']==$key)) echo 'selected'; ?>><?php echo $value; ?></option>
		<?php 
	}
}
if(!empty($_GET['xdbai'])){
	$xdbai=$_GET['xdbai'];
	$list=hien_thi_nhan_xet_theo_thamgiaxaydungbai();
	foreach ($list[$xdbai] as $key => $value) {
		# code...
		?>
		<option value="<?php echo $key; ?>" <?php if(!empty($_GET['idcmt3'])&&($_GET['idcmt3']==$key)) echo 'selected'; ?>><?php echo $value; ?></option>
		<?php 
	}
}
 if(!empty($_GET['school_gvtg'])&&!empty($_GET['schoolyear_gvtg'])&&!empty($_GET['id_gvtg_danhgia'])){
 	$idtruong=$_GET['school_gvtg'];
 	$idnamhoc=$_GET['schoolyear_gvtg'];
 	$ta=$_GET['id_gvtg_danhgia'];

 	$sql="
	SELECT  `course`.`id`,`history_course_name`.`groupid`,`history_course_name`.`course_name`
	FROM `user` 
	JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
	JOIN `history_course_name` ON `history_course_name`.`courseid`=`course`.`id` 
	JOIN `groups` ON `groups`.`id` = `history_course_name`.`groupid`
	JOIN `schools` ON `groups`.`id_truong`= {$idtruong}
	JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
	WHERE `user`.`id`={$ta}
	AND `role_assignments`.`roleid`=10
	AND `history_course_name`.`school_year_id`={$idnamhoc}";
	$data = $DB->get_records_sql($sql);

	echo'<option value="">'.get_string('classgroup').'</option>';
	if ($data) { 
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" status="<?php echo $value->groupid; ?>"><?php echo $value->course_name; ?></option>
			<?php 
		}
	}

 }
 if(!empty($_GET['nhomlop'])&&!empty($_GET['namhoc'])&&!empty($_GET['lophoc'])){
 	// check lop co phai la tieu hoc hay khong
 	$nhomlop=$_GET['nhomlop'];
 	$namhoc=$_GET['namhoc'];
 	$lophoc=$_GET['lophoc'];
 	$time=time();
	$sql="SELECT DISTINCT `user`.`id`,`user`.`firstname`,`user`.`lastname`,`role_assignments`.`roleid`
	from `user` 
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid` 
    JOIN `history_student` ON `history_student`.`iduser`=`user`.`id`
    JOIN `course` ON `course`.`id` =`history_student`.`idgroups`
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `course`.`id`= {$nhomlop}
    AND `history_student`.`idschoolyear`={$namhoc}
    AND `history_student`.`idclass`={$lophoc}
    AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)
    ORDER BY `user`.`firstname` ASC
    ";
    $data = $DB->get_records_sql($sql);
    // echo'<option value="">'.get_string('student').'</option>';
	if ($data) { 
		?>
		<!-- <div class="col-md-6"> -->
			<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
			<select name="studentid" id="" class="form-control" required>
				<option value=""><?php echo get_string('student'); ?></option>
				<?php 
				foreach ($data as $key => $value) {
					?>
					<option value="<?php echo $value->id; ?>" status="<?php echo $nhomlop; ?>"><?php echo $value->lastname,'',$value->firstname; ?></option>
					<?php 
				}
				 ?>
			</select>
		<!-- </div> -->
		<input type="text"  class="form-control" name="groupid" value="<?php echo $lophoc; ?>" hidden>
		<?php 
		
	}else{
		?>
		<!-- <div class="col-md-6"> -->
			<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
			<select name="studentid" id="" class="form-control" required>
				<option value=""><?php echo get_string('student'); ?></option>
			</select>
		<!-- </div> -->
		<?php 
	}

 }
?>
