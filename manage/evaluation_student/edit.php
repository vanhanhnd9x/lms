<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
global $USER;

require_login();
$PAGE->set_title(get_string('edit_evaluation_student'));
$PAGE->set_heading(get_string('edit_evaluation_student'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idedit = optional_param('idedit', '', PARAM_TEXT);
$course = optional_param('course', '', PARAM_TEXT);
$evaluation_student=$DB->get_record('evalucation_student', array(
  'id' => $idedit
));
$idnamhochientai=get_id_school_year_now();
$namhoc=$DB->get_record('school_year', array(
  'id' => $idnamhochientai
));

if($evaluation_student->school_yearid!=$idnamhochientai){
  ?>
  <script>
    $("#submitBtn").prop("disabled",true);
  </script>
  <?php 
}else{
  $thang=$evaluation_student->month +1;
  $time_close=mktime(11, 59, 59, $thang,1, date('Y'));
  $time_hien_tai=time();
  if($time_hien_tai>$time_close){
    lock_evaluation_student($idedit);
    ?>
    <script>
      $("#submitBtn").prop("disabled",true);
    </script>
    <?php 
  }

}
$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
if(!empty($course)){
  $url="/manage/evaluation_student/edit.php?course=".$course."&idedit=";
  $url1="/manage/evaluation_student/list_evaluation_student_course.php?course=".$course."&idedit=";
}else{
  $url="/manage/evaluation_student/edit.php?idedit=";
  $url1="/manage/evaluation_student/index.php";
}


if ($action=='edit_evaluation_student') {
  # code...
  $school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
  $month = $evaluation_student->month;
  // $month = optional_param('month', '', PARAM_TEXT);
  $toplic = optional_param('toplic', '', PARAM_TEXT);
  $attendance = optional_param('attendance', '', PARAM_TEXT);
  $learning_attitude = optional_param('learning_attitude', '', PARAM_TEXT);
  $material = optional_param('material', '', PARAM_TEXT);
  $participation = optional_param('participation', '', PARAM_TEXT);
  $cmt1 = optional_param('cmt1', '', PARAM_TEXT);
  $cmt2 = optional_param('cmt2', '', PARAM_TEXT);
  $cmt3 = optional_param('cmt3', '', PARAM_TEXT);
  $other_comment = optional_param('other_comment', '', PARAM_TEXT);
  if(($month!=$evaluation_student->month)||($school_yearid!=$evaluation_student->school_yearid)){
    $check=check_danh_gia_hoc_sinh_theo_thang_namhoc($evaluation_student->userid,$month,$school_yearid);
    if($check==1){
      ?>
      <script>
        alert('<?php echo get_string('isset_evaluation_student'); ?>'); 
      </script>
      <?php 
      echo displayJsAlert('Error', $CFG->wwwroot.''.$url.''.$idedit);
    }elseif($check==2){
      // update_evaluation_student($idedit,$school_yearid,$month,$toplic,$attendance,$learning_attitude,$material,$participation,$comment,$other_comment);
      update_evaluation_student($idedit,$school_yearid,$month,$toplic,$attendance,$learning_attitude,$material,$participation,$cmt1,$cmt2,$cmt3);
      echo displayJsAlert('Success', $CFG->wwwroot.''.$url1);
    }
  }else{
     // update_evaluation_student($idedit,$school_yearid,$month,$toplic,$attendance,$learning_attitude,$material,$participation,$comment,$other_comment);
     update_evaluation_student($idedit,$school_yearid,$month,$toplic,$attendance,$learning_attitude,$material,$participation,$cmt1,$cmt2,$cmt3);
      echo displayJsAlert('Success', $CFG->wwwroot.''.$url1);
  }
}
if(is_student()){
     echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<?php if($evaluation_student->view==0){?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">
          <form  action="" onsubmit="return validate();" method="post">
            <input type="text" class="form-control" name="action" value="edit_evaluation_student" hidden="">
            <input type="text" class="form-control" name="school_yearid" value="<?php echo $evaluation_student->school_yearid; ?>" hidden="">
            <div class="row">
              <?php $info=show_info_student_in_unit($evaluation_student->userid,$evaluation_student->courseid,$evaluation_student->groupid); ?>
              <div class="col-md-6">
                <label class="control-label"><?php echo get_string('school_year'); ?></label>
                <input type="text" value="<?php echo show_school_yeah_in_evaluation_by_cua($evaluation_student->school_yearid); ?>" class="form-control" disabled>
              </div>
              <div class="col-md-6">
                <label class="control-label"><?php echo get_string('month'); ?></label>
                <input type="text" class="form-control" value="<?php echo show_month_in_evaluation_by_cua($evaluation_student->month); ?>" disabled="" >
                <!-- <select name="month" id="month" class="form-control">
                  <option value="1" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==1) echo'selected'; ?>><?php echo get_string('january'); ?></option>
                  <option value="2" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==2) echo'selected'; ?>><?php echo get_string('february'); ?></option>
                  <option value="3" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==3) echo'selected'; ?>><?php echo get_string('march'); ?></option>
                  <option value="4" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==4) echo'selected'; ?>><?php echo get_string('april'); ?></option>
                  <option value="5" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==5) echo'selected'; ?>><?php echo get_string('may'); ?></option>
                  <option value="6" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==6) echo'selected'; ?>><?php echo get_string('june'); ?></option>
                  <option value="7" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==7) echo'selected'; ?>><?php echo get_string('july'); ?></option>
                  <option value="8" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==8) echo'selected'; ?>><?php echo get_string('august'); ?></option>
                  <option value="9" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==9) echo'selected'; ?>><?php echo get_string('september'); ?></option>
                  <option value="10" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==10) echo'selected'; ?>><?php echo get_string('october'); ?></option>
                  <option value="11" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==11) echo'selected'; ?>><?php echo get_string('november'); ?></option>
                  <option value="12" <?php if(!empty($evaluation_student->month)&&$evaluation_student->month==12) echo'selected'; ?>><?php echo get_string('december'); ?></option>
                </select> -->
              </div>
              <div class="col-md-6">
                <label for="" class="control-label"><?php echo get_string('topic_month'); ?>  <span style="color:red">*</span></label>
                <input type="text" class="form-control" value="<?php echo $evaluation_student->toplic; ?>" name="toplic" required="">
              </div>
              <div class="col-md-12">
                <div class="alert-info">
                  <h4><?php echo get_string('assessment'); ?></h4>
                </div>
              </div>
              <div class="col-md-6">
                <label class="control-label"><?php echo get_string('attendance'); ?>(Số ngày đi học)<span style="color:red">*</span></label>
                 <div id="chuyencan"> <input type="text" name="attendance" id="" placeholder="<?php echo get_string('attendance'); ?>" value="<?php echo $evaluation_student->attendance;  ?>" class="form-control"></div>
              </div>
              <div class="col-md-6"></div>
              <div class="col-md-4">
                <label class="control-label"><?php echo get_string('learning_attitude') ?><span style="color:red">*</span></label>
                <select name="learning_attitude" id="thaido" class="form-control" required>
                  <option value="A" <?php if(!empty($evaluation_student->learning_attitude)&&$evaluation_student->learning_attitude=='A') echo'selected'; ?>>A</option>
                  <option value="B" <?php if(!empty($evaluation_student->learning_attitude)&&$evaluation_student->learning_attitude=='B') echo'selected'; ?>>B</option>
                  <option value="C" <?php if(!empty($evaluation_student->learning_attitude)&&$evaluation_student->learning_attitude=='C') echo'selected'; ?>>C</option>
                  <option value="D" <?php if(!empty($evaluation_student->learning_attitude)&&$evaluation_student->learning_attitude=='D') echo'selected'; ?>>D</option>
                </select>
              </div>
              <div class="col-md-4">
                <label class="control-label"><?php echo get_string('material'); ?><span style="color:red">*</span></label>
                <select name="material" id="dodung" class="form-control" required>
                  <option value="A" <?php if(!empty($evaluation_student->material)&&$evaluation_student->material=='A') echo'selected'; ?>>A</option>
                  <option value="B" <?php if(!empty($evaluation_student->material)&&$evaluation_student->material=='B') echo'selected'; ?>>B</option>
                  <option value="C" <?php if(!empty($evaluation_student->material)&&$evaluation_student->material=='C') echo'selected'; ?>>C</option>
                  <option value="D" <?php if(!empty($evaluation_student->material)&&$evaluation_student->material=='D') echo'selected'; ?>>D</option>
                </select>
              </div>
              <div class="col-md-4">
                <label class="control-label"><?php echo get_string('participation'); ?><span style="color:red">*</span></label>
                <select name="participation" id="xdbai" class="form-control" required>
                  <option value="A" <?php if(!empty($evaluation_student->participation)&&$evaluation_student->participation=='A') echo'selected'; ?>>A</option>
                  <option value="B" <?php if(!empty($evaluation_student->participation)&&$evaluation_student->participation=='B') echo'selected'; ?>>B</option>
                  <option value="C" <?php if(!empty($evaluation_student->participation)&&$evaluation_student->participation=='C') echo'selected'; ?>>C</option>
                  <option value="D" <?php if(!empty($evaluation_student->participation)&&$evaluation_student->participation=='D') echo'selected'; ?>>D</option>
                </select>
              </div>
              <div class="col-md-12">
                  <label for="" class="control-label"><?php echo get_string('other_comment'); ?></label>
                  <select name="cmt1" id="loadthaido" class="form-control">
                    <option value=""></option>
                  </select>
                  <select name="cmt2" id="loaddongdung" class="form-control">
                    <option value=""></option>
                  </select>
                   <select name="cmt3" id="loadxdbai" class="form-control">
                    <option value=""></option>
                  </select>
              </div>
              <div class="col-md-12" style="margin-top: 10px;">
                <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
                <?php 
                print_r(get_string('or'));
                if(!empty($course)){
                  ?>
                  <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/list_evaluation_student_course.php?course=<?php echo $course;?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                  <?php 
                }else{
                  ?>
                  <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                  <?php 
                }
                 ?>

                
              </div>
            </form>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
</div>
<?php }else{ ?>
    <h5 class="text-white bg-warning p-2">Đánh giá đã bị khóa. Vui lòng yêu cầu mở khóa để chỉnh sửa!</h5>
<?php } ?>
  <?php 
  echo $OUTPUT->footer();

  ?>

  <script>
     // javascript load thai do
  var idcmt1="<?php echo $evaluation_student->cmt1; ?>";
  var idcmt2="<?php echo $evaluation_student->cmt2; ?>";
  var idcmt3="<?php echo $evaluation_student->cmt3; ?>";
  var thaido1 = $('#thaido').find(":selected").val();
  if (thaido1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido1+"&idcmt1="+idcmt1,function(data){
        $("#loadthaido").html(data);
      });
  }
  $('#thaido').on('change', function() {
    var thaido = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido,function(data){
        $("#loadthaido").html(data);
      });
    }
  });
  // javascript load do dung hoc tao
  var dodung1 = $('#dodung').find(":selected").val();
  if (dodung1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung1+"&idcmt2="+idcmt2,function(data){
        $("#loaddongdung").html(data);
      });
  }
  $('#dodung').on('change', function() {
    var dodung = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung,function(data){
        $("#loaddongdung").html(data);
      });
    }
  });
  // javascript load tham gia xd bai
  var xdbai1 = $('#xdbai').find(":selected").val();
  if (xdbai1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai1+"&idcmt3="+idcmt3,function(data){
        $("#loadxdbai").html(data);
      });
  }
  $('#xdbai').on('change', function() {
    var xdbai = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai,function(data){
        $("#loadxdbai").html(data);
      });
    }
  });


    $('#schoolid').on('change', function() {
      var cua = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
          $("#block_student").html(data);
        });
      }
    });
    $('#block_student').on('change', function() {
      var block = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
          $("#courseid").html(data);
        });
      }
    });
    $('#courseid').on('change', function() {
      var course = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
          $("#groupid").html(data);
        });
      }
    });
    $('#groupid').on('change', function() {
      var group = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
          $("#studentid").html(data);
        });
      }
    });
    var userid="<?php echo $evaluation_student->userid; ?>";
    var school_year = "<?php echo $evaluation_student->school_yearid; ?>";
    var month1 = "<?php echo $evaluation_student->month; ?>";
    var course = "<?php echo $evaluation_student->courseid; ?>";
     if(month1){
          $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month1+"&userid="+userid+"&course="+course,function(data){
          $("#chuyencan").html(data);
        });
      }
    // $('#month').on('change', function() {
    //     var month1 = this.value ;
    //     if(month1){
    //       $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month1+"&userid="+userid,function(data){
    //         $("#chuyencan").html(data);
    //       });
    //     }
    // }); 
    // var month = $('#month').find(":selected").val();
    // if(month){
    //   $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month+"&userid="+userid,function(data){
    //     $("#chuyencan").html(data);
    //   });
    // }
  </script>
  
