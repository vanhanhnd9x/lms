<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
global $USER;

require_login();
$PAGE->set_title(get_string('view_evaluation_student'));
$PAGE->set_heading(get_string('view_evaluation_student'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$idedit = optional_param('idedit', '', PARAM_TEXT);
$idstudent = optional_param('idstudent', '', PARAM_TEXT);
if(empty($idedit)){
  echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/evaluation_student/list_evaluation_student.php");
}
$evaluation_student=$DB->get_record('evalucation_student', array(
  'id' => $idedit
));
$student=$DB->get_record('user', array(
  'id' => $evaluation_student->userid
));
    // lấy thông tin khóa học<nhom lop>
$course=$DB->get_record('course', array(
  'id' => $evaluation_student->courseid
));
     // lay thong tin lop
$group =$DB->get_record('groups', array(
  'id' => $evaluation_student->groupid
)); 
    // thông tin khôi
$block_student=$DB->get_record('block_student', array(
  'id' => $group->id_khoi
));
    // thong tin  truong
$school=$DB->get_record('schools', array(
  'id' => $group->id_truong
)); 
$school_year=$DB->get_record('school_year', array(
  'id' => $evaluation_student->school_yearid
)); 
    
$yeah=$school_year->sy_start.' - '.$school_year->sy_end;
$username= $student->lastname.' '.$student->firstname;
$tong=lay_tong_tiet_trong_thang($evaluation_student->month,$evaluation_student->school_yearid,$evaluation_student->courseid);
if(empty($tong)){ $tong=0;}
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">
          <form  action="" onsubmit="return validate();" method="post">
            <input type="text" class="form-control" name="action" value="edit_evaluation_student" hidden="">
            <input type="text" class="form-control" name="school_yearid" value="<?php echo $evaluation_student->school_yearid; ?>" hidden="">
            <div class="form-group row">
                <label class="col-md-4 col-form-label">Name of student/ Họ tên học sinh</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" value="<?php echo $username; ?>" disabled="">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label">Class/ Lớp</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" value="<?php echo $group->name; ?>" disabled="">
                </div>
            </div>
             <div class="form-group row">
                <label class="col-md-4 col-form-label">Class group/ Nhóm lớp</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" value="<?php echo $course->fullname; ?>" disabled="">
                </div>
            </div>
             <div class="form-group row">
                <label class="col-md-4 col-form-label">School / Trường</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" value="<?php echo $school->name; ?>" disabled="">
                </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                  <label class="control-label" style="color: red;">A = Excellent/Xuất sắc</label>
              </div>
              <div class="col-md-6">
                  <label class="control-label" style="color: red;">B = Working Well/Tốt</label>
              </div>
              <div class="col-md-6">
                  <label class="control-label" style="color: red;">C = Would benefit from more effort/Cần cố gắng hơn</label>
              </div>
              <div class="col-md-6">
                  <label class="control-label" style="color: red;">D = Unsatisfactory/Chưa đặt yêu cầu</label>
              </div>
              <div class="col-md-12">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">
                        Criteria<br>Tiêu chí đánh giá
                      </th>
                      <th scope="col">
                        A-Attendance<br>
                        Chuyên cần
                      </th>
                      <th scope="col">
                        L-Learning attitude 
                        <br>
                        Thái độ học tập
                      </th>
                        <th scope="col">
                        Learning material<br>
                        Đồ dùng học tập
                      </th>
                        <th scope="col">
                        P-Participation<br>
                        Tham gia xây dựng bài
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Evaluation/ Đánh giá</th>
                      <td class="text-center"><?php echo $evaluation_student->attendance.'/'.$tong; ?></td>
                      <td class="text-center"><?php echo $evaluation_student->learning_attitude; ?></td>
                      <td class="text-center"><?php echo $evaluation_student->material; ?></td>
                      <td class="text-center"><?php echo $evaluation_student->participation; ?></td>
                    </tr>
                    <tr>
                      <th scope="row">Comments/Nhận xét</th>
                      <td colspan="4">
                        <?php 
                          $text1=hien_thi_nhan_xet_theo_thaidohoctap();
                          $text2=hien_thi_nhan_xet_theo_dodunghoctap();
                          $text3=hien_thi_nhan_xet_theo_thamgiaxaydungbai();
                          if($text1[$evaluation_student->learning_attitude][$evaluation_student->cmt1]){
                            echo $text1[$evaluation_student->learning_attitude][$evaluation_student->cmt1],'<br>';
                          }
                          if($text2[$evaluation_student->material][$evaluation_student->cmt2]){
                            echo $text2[$evaluation_student->material][$evaluation_student->cmt2],'<br>';
                          }
                          if($text3[$evaluation_student->participation][$evaluation_student->cmt3]){
                            echo $text3[$evaluation_student->participation][$evaluation_student->cmt3],'<br>';
                          }
                        ?>
                      </td>
                      
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-12" style="margin-top: 10px;">
                <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/list_evaluation_student.php?idstudent=<?php echo $idstudent; ?>" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
              </div>
            </form>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
</div>

  <?php 
  echo $OUTPUT->footer();

  ?>

  <script>
     // javascript load thai do
  var idcmt1="<?php echo $evaluation_student->cmt1; ?>";
  var idcmt2="<?php echo $evaluation_student->cmt2; ?>";
  var idcmt3="<?php echo $evaluation_student->cmt3; ?>";
  var thaido1 = $('#thaido').find(":selected").val();
  if (thaido1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido1+"&idcmt1="+idcmt1,function(data){
        $("#loadthaido").html(data);
      });
  }
  $('#thaido').on('change', function() {
    var thaido = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido,function(data){
        $("#loadthaido").html(data);
      });
    }
  });
  // javascript load do dung hoc tao
  var dodung1 = $('#dodung').find(":selected").val();
  if (dodung1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung1+"&idcmt2="+idcmt2,function(data){
        $("#loaddongdung").html(data);
      });
  }
  $('#dodung').on('change', function() {
    var dodung = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung,function(data){
        $("#loaddongdung").html(data);
      });
    }
  });
  // javascript load tham gia xd bai
  var xdbai1 = $('#xdbai').find(":selected").val();
  if (xdbai1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai1+"&idcmt3="+idcmt3,function(data){
        $("#loadxdbai").html(data);
      });
  }
  $('#xdbai').on('change', function() {
    var xdbai = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai,function(data){
        $("#loadxdbai").html(data);
      });
    }
  });


    $('#schoolid').on('change', function() {
      var cua = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
          $("#block_student").html(data);
        });
      }
    });
    $('#block_student').on('change', function() {
      var block = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
          $("#courseid").html(data);
        });
      }
    });
    $('#courseid').on('change', function() {
      var course = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
          $("#groupid").html(data);
        });
      }
    });
    $('#groupid').on('change', function() {
      var group = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
          $("#studentid").html(data);
        });
      }
    });
    var userid="<?php echo $evaluation_student->userid; ?>";
    var school_year = "<?php echo $evaluation_student->school_yearid; ?>";
    $('#month').on('change', function() {
        var month1 = this.value ;
        if(month1){
          $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month1+"&userid="+userid,function(data){
            $("#chuyencan").html(data);
          });
        }
    }); 
    // var month = $('#month').find(":selected").val();
    // if(month){
    //   $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month+"&userid="+userid,function(data){
    //     $("#chuyencan").html(data);
    //   });
    // }
  </script>
  
