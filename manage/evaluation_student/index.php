<?php
    require("../../config.php");
    require("../../grade/lib.php");
    global $CFG;
    require_once($CFG->dirroot . '/common/lib.php');
    require_once($CFG->dirroot . '/manage/student/lib.php');
    require_once($CFG->dirroot . '/manage/block-student/lib.php');
    require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
    require_once($CFG->dirroot . '/manage/evaluation/lib.php');
    require_once($CFG->dirroot . '/manage/schedule/lib.php');
    
    $PAGE->set_title(get_string('list_evaluation_student'));
    echo $OUTPUT->header();
    require_login(0, false);
    $action = optional_param('action', '', PARAM_TEXT);
    $schoolid = optional_param('schoolid', '', PARAM_TEXT);
    $block_student = optional_param('block_student', '', PARAM_TEXT);
    $groupid = optional_param('groupid', '', PARAM_TEXT);
    $courseid = optional_param('courseid', '', PARAM_TEXT);
    $code = trim(optional_param('code', '', PARAM_TEXT));
    $name = trim(optional_param('name', '', PARAM_TEXT));
    $month = trim(optional_param('month', '', PARAM_TEXT));
    $school_year = trim(optional_param('school_year', '', PARAM_TEXT));
    $evalucation_by = optional_param('evalucation_by', '', PARAM_TEXT);
    
    $page=isset($_GET['page'])?$_GET['page']:1;
    $number=20;
    $roleid=lay_role_id_cua_user_dang_nhap($USER->id);
    $schools = get_all_shool();
    if($roleid==10){
      $evalucation_by=$USER->id;
      $schools = hien_thi_ds_truong_duoc_them_gan_cho_gv(10,$USER->id);
    }
    $cua=get_all_evaluation_studentbycua($schoolid,$block_student,$groupid,$courseid,$code,$name,$month,$school_year,$page,$number,$evalucation_by);
    if($action=='send'){
        $evaluation_studentid      = optional_param('id', 0, PARAM_INT);
        send_evaluation_student($evaluation_studentid);
        echo displayJsAlert('Đã gửi yêu cầu', $CFG->wwwroot . "/manage/evaluation_student/index.php");
    }
    if($action=='lock'){
        $evaluation_studentid      = optional_param('id', 0, PARAM_INT);
        lock_evaluation_student($evaluation_studentid);
        echo displayJsAlert('Đã khóa đánh giá', $CFG->wwwroot . "/manage/evaluation_student/index.php");
    }

    if($action=='lock_list'){
        $list_lock      = optional_param('list_lock', 0, PARAM_INT);
        foreach ($list_lock as $evaluation_studentid) {
            lock_evaluation_student($evaluation_studentid);
        } 
        echo displayJsAlert('Đã khóa đánh giá', $CFG->wwwroot . "/manage/evaluation_student/index.php");
    }
    if ($action=='search') {
      $url= $CFG->wwwroot.'/manage/evaluation_student/index.php?action=search&evalucation_by='.$evalucation_by.'&schoolid='.$schoolid.'&block_student='.$block_student.'&groupid='.$groupid.'&courseid='.$courseid.'&code='.$code.'&name='.$name.'&month='.$month.'&school_year='.$school_year.'&page=';
    }else{
      $url= $CFG->wwwroot.'/manage/evaluation_student/index.php?page=';
    }

    $evaluation_by = get_gvnn_gvtg();
    
    $moodle='evaluation-student';
    $name1='evaluation_student';
    $check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
    if(empty($check_in)){
        echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
    }
    $hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
    $checkdelete=check_chuc_nang_xoa($roleid,$moodle);
    $checkpopup=check_chuc_nang_pop_up($roleid,$moodle);
    $checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
    $thang=date('m');
    ?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <?php 
                        if(!empty($checkthemmoi)){
                          ?>
                    <div class="col-md-2">
                        <a href="<?php echo new moodle_url('/manage/evaluation_student/new_evaluationstudent.php'); ?>" class="btn btn-success"><?php echo get_string('new'); ?></a>
                      
                        <div id="dellist" style="display: none" class="mt-3">
                            <a onclick="get_check_lock()" title="Khóa đánh giá" class="btn btn-warning btn-sm">Khóa đánh giá</a>
                        </div>
                    </div>
                    <?php
                        }
                        ?>
                    <div class="col-md-10 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
                            <!-- <input type="" name="evalucation_by" class="form-control" placeholder="Nhập tên..."  value="<?php //echo $evalucation_by; ?>" hidden=""> -->
                            <div class="col-4 form-group">
                                <label for="">
                                <?php echo get_string('schools'); ?> </label>
                                <select name="schoolid" id="schoolid" class="form-control">
                                    <option value=""><?php echo get_string('schools'); ?></option>
                                    <?php 
                                        foreach ($schools as $key => $value) {
                                         ?>
                                    <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>>
                                        <?php echo $value->name; ?>
                                    </option>
                                    <?php 
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                <?php echo get_string('block_student'); ?></label>
                                <select name="block_student" id="block_student" class="form-control">
                                    <option value="">
                                        <?php echo get_string('block_student'); ?>
                                    </option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
                                <select name="school_year" id="school_yearid" class="form-control" >
                                    <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                                </select>
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                <?php echo get_string('class'); ?></label>
                                <select name="groupid" id="groupid" class="form-control">
                                    <option value=""><?php echo get_string('class'); ?></option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('classgroup'); ?> </label>
                                <select name="courseid" id="courseid" class="form-control" >
                                    <option value=""><?php echo get_string('classgroup'); ?></option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('codestudent'); ?></label>
                                <input type="" name="code" class="form-control" placeholder="<?php echo get_string('codestudent'); ?>" value="<?php echo trim($code); ?>">
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('namestudent'); ?></label>
                                <input type="" name="name" class="form-control" placeholder="<?php echo get_string('namestudent'); ?>" value="<?php echo trim($name); ?>">
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Giáo viên đánh giá </label>
                                <select name="evalucation_by" id="evalucation_by" class="form-control multiple">
                                    <option value="0" >-- Chọn giáo viên đánh giá --</option>
                                    <?php foreach ($evaluation_by as $val): ?>
                                        <?php $gv =''; if($val->roleid==8){ $gv='GNNN'; }else{$gv ='GVTG';} ?>
                                        <option value="<?=$val->id?>" <?= $evalucation_by==$val->id ? "selected" : "" ?>><?= $val->lastname.' '.$val->firstname .' - '.$gv ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('choose_month'); ?></label>
                                <select name="month" id="" class="form-control">
                                    <option value=""><?php echo get_string('choose_month'); ?></option>
                                    <option value="1" <?php if($month==1) echo 'selected'; ?>><?php echo get_string('january'); ?></option>
                                    <option value="2" <?php if($month==2) echo 'selected'; ?>><?php echo get_string('february'); ?></option>
                                    <option value="3" <?php if($month==3) echo 'selected'; ?>><?php echo get_string('march'); ?></option>
                                    <option value="4" <?php if($month==4) echo 'selected'; ?>><?php echo get_string('april'); ?></option>
                                    <option value="5" <?php if($month==5) echo 'selected'; ?>><?php echo get_string('may'); ?></option>
                                    <option value="6" <?php if($month==6) echo 'selected'; ?>><?php echo get_string('june'); ?></option>
                                    <option value="7" <?php if($month==7) echo 'selected'; ?>><?php echo get_string('july'); ?></option>
                                    <option value="8" <?php if($month==8) echo 'selected'; ?>><?php echo get_string('august'); ?></option>
                                    <option value="9" <?php if($month==9) echo 'selected'; ?>><?php echo get_string('september'); ?></option>
                                    <option value="10" <?php if($month==10) echo 'selected'; ?>><?php echo get_string('october'); ?></option>
                                    <option value="11" <?php if($month==11) echo 'selected'; ?>><?php echo get_string('november'); ?></option>
                                    <option value="12" <?php if($month==12) echo 'selected'; ?>><?php echo get_string('december'); ?></option>
                                </select>
                            </div>
                            <div class="col-4">
                                <div style="    margin-top: 29px;"></div>
                                <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
                                <!-- <input type="submit" class="btn btn-info" value="Xuất báo cáo" name="export"> -->
                                <?php if(!empty($checkpopup)){ echo'<a href="#myModal" class="btn btn-success" data-toggle="modal" >'.get_string('export_file').'</a>';} ?>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="hidden-print"><input type="checkbox" name="" id="checkAll"></th>
                                <th>STT</th>
                                <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-right">'.get_string('action').'</th>';} ?>
                                <th><?php echo get_string('classgroup'); ?></th>
                                <th><?php echo get_string('codestudent'); ?></th>
                                <th><?php echo get_string('student'); ?></th>
                                <th><?php echo get_string('date_evaluation'); ?></th>
                                <th><?php echo get_string('month'); ?></th>
                                <th><?php echo get_string('school_year'); ?></th>
                                <th><?php echo get_string('topic_month'); ?></th>
                                <th><?php echo get_string('attendance'); ?></th>
                                <th><?php echo get_string('learning_attitude') ?></th>
                                <th><?php echo get_string('material'); ?></th>
                                <th><?php echo get_string('participation'); ?></th>
                                <th><?php echo get_string('other_comment'); ?></th>
                                <!-- <th><?php //echo get_string('view_evaluation_student'); ?></th>
                                    <th><?php //echo get_string('add_evaluation_student'); ?></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <form action="?action=lock_list" id="evaluationstudent" method="get" accept-charset="utf-8">
                            <input type="hidden" name="action" value="lock_list">
                            <?php 
                                if (!empty($cua['data'])) {
                                                        # code...
                                  // $i=0;
                                    $i=($page-1)*20;
                                      foreach ($cua['data'] as $student) { 
                                        $i++;
                                        $tong=lay_tong_tiet_trong_thang($student->month,$student->school_yearid,$student->courseid);
                                        // $coursename=show_name_course_by_cua2($student->userid,$student->school_yearid);
                                         $coursename=get_info_course_in_schedule2($student->courseid,$student->school_yearid);
                                        if(empty($tong)){ $tong=0;}
                                        ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="list_lock[]" class="list" value="<?=$student->id?>">
                                </td>
                                <td class="text-center"><?php echo $i; ?></td>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                      echo'<td class="text-right">';
                                      foreach ($hanhdong as  $value) {
                                        # code...
                                        if($value->type=='edit'){
                                          if($student->view==0):
                                            ?>
                                <a href="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/edit.php?idedit=<?php echo $student->id ?>" class="mauicon tooltip-animation"><i class="fa fa-pencil" aria-hidden="true" title="Chỉnh sửa đánh giá"></i></a>
                                <a onclick="return Confirm('Khóa đánh giá','Bạn có muốn khóa đánh giá này!','Yes','Cancel','<?php print new moodle_url('?action=lock',array('id'=>$student->id)); ?>')" title="Khóa đánh giá" class="mauicon tooltip-animation"><i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php 
                                    endif;
                                    if($student->view==1):
                                      ?>
                                <a onclick="return Confirm('Gửi yêu cầu','Bạn sẽ gửi 1 yêu cầu cho quản trị viên để được xác nhận mở khóa đánh giá!','Yes','Cancel','<?php print new moodle_url('?action=send',array('id'=>$student->id)); ?>')" title="Gửi yêu cầu mở khóa" class="mauicon tooltip-animation"><i class="fa fa-unlock-alt" aria-hidden="true"></i></a>
                                <?php 
                                    endif;
                                     if($student->view==2):
                                      echo'<span class="mauicon">Đang chờ xử lý mở khóa</span>';
                                    endif;
                                    }else{
                                    echo'<a href="'.$CFG->wwwroot.''.$value->link.''.$student->id.'" class="mauicon tooltip-animation" title="'.get_string( $value->name).'">
                                          <i class="'.$value->icon.'" aria-hidden="true"></i>
                                    </a>';
                                    }
                                    
                                    }
                                    
                                    if(!empty($checkdelete)){
                                    ?>
                                <a href="javascript:void(0)" onclick="delete_evaluation(<?php echo $student->id;?>);" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                <?php 
                                    }
                                     echo'</td>';
                                    }
                                ?>
                                <td><?php echo $coursename; ?></td>
                                <td><?php echo $student->code; ?></td>
                                <td><?php echo  $student->lastname,' ',$student->firstname; ?> </td>
                                <td><?php if($student->time_cre){echo date('d/m/Y',$student->time_cre);} ?></td>
                                <td class="text-center"><?php echo show_month_in_evaluation_by_cua($student->month); ?></td>
                                <td ><?php echo show_school_yeah_in_evaluation_by_cua($student->school_yearid); ?></td>
                                <td class="text-center"><?php echo $student->toplic; ?></td>
                                <td class="text-center"><?php echo $student->attendance.'/'.$tong; ?></td>
                                <td class="text-center"><?php echo $student->learning_attitude; ?></td>
                                <td class="text-center"><?php echo $student->material; ?></td>
                                <td class="text-center"><?php echo $student->participation; ?></td>
                                <td style="word-wrap: break-word;">
                                    <?php 
                                        $text1=hien_thi_nhan_xet_theo_thaidohoctap();
                                        $text2=hien_thi_nhan_xet_theo_dodunghoctap();
                                        $text3=hien_thi_nhan_xet_theo_thamgiaxaydungbai();
                                        if($text1[$student->learning_attitude][$student->cmt1]){
                                          echo $text1[$student->learning_attitude][$student->cmt1],'<br>';
                                        }
                                        if($text2[$student->material][$student->cmt2]){
                                          echo $text2[$student->material][$student->cmt2],'<br>';
                                        }
                                        if($text3[$student->participation][$student->cmt3]){
                                          echo $text3[$student->participation][$student->cmt3],'<br>';
                                        }
                                        // echo $text2[$student->material][$student->cmt2],'<br>';
                                        // echo $text3[$student->participation][$student->cmt3],'<br>';
                                        ?>
                                </td>
                            </tr>
                            <?php }
                                }
                                ?>
                            </form>
                        </tbody>
                    </table>
                    <?php 
                        if ($cua['total_page']) {
                          if ($page > 2) { 
                            $startPage = $page - 2; 
                          } else { 
                            $startPage = 1; 
                          } 
                          if ($cua['total_page'] > $page + 2) { 
                            $endPage = $page + 2; 
                          } else { 
                            $endPage =$cua['total_page']; 
                          }
                        }
                        ?>
                    <p><?php echo get_string('total_page'); ?>: <?php echo $cua['total_page'] ?></p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                                for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                                # code...
                                  ?>
                            <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
                            <?php 
                                }
                                ?>
                            <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
    background: #4bb747 !important;
    border: 1px solid #4bb747 !important;
    color:white !important;
    }
    .pagination > .active >  a:hover{
    background: #218838 !important;
    border: 1px solid #218838 !important;
    color:white !important;
    }
</style>
<style>
    .mauicon{
    color:#106c37 !important;
    }
    .mauicon:hover{
    color:#4bb747 !important;
    }
</style>
<div class="container">
    <!-- <a href="#myModal" class="btn btn-primary" data-toggle="modal">Launch demo modal</a>
        -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <!-- <h4 class="modal-title">Lựa chọn học sinh</h4> -->
                </div>
                <div class="modal-body">
                    <form action="<?php echo $CFG->wwwroot ?>/manage/evaluation_student/export_evaluation_student.php" method="get" >
                        <div class="row">
                            <div class="col-12">
                                <label for=""><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
                                <select name="schoolid" id="schoolid1" class="form-control" required>
                                    <option value=""><?php echo get_string('schools'); ?></option>
                                    <?php 
                                        foreach ($schools as $key => $value) {
                                         ?>
                                    <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                                    <?php 
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="col-12">
                                <label for=""><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
                                <select name="block_student" id="block_student1" class="form-control" required>
                                    <option value=""><?php echo get_string('block_student'); ?></option>
                                </select>
                            </div>
                            <div class="col-12">
                                <label for=""><?php echo get_string('choose_month'); ?></label>
                                <select name="month1" id="" class="form-control">
                                    <option value=""><?php echo get_string('choose_month'); ?></option>
                                    <option value="1" <?php if($thang==1) echo 'selected'; ?>><?php echo get_string('january'); ?></option>
                                    <option value="2" <?php if($thang==2) echo 'selected'; ?>><?php echo get_string('february'); ?></option>
                                    <option value="3" <?php if($thang==3) echo 'selected'; ?>><?php echo get_string('march'); ?></option>
                                    <option value="4" <?php if($thang==4) echo 'selected'; ?>><?php echo get_string('april'); ?></option>
                                    <option value="5" <?php if($thang==5) echo 'selected'; ?>><?php echo get_string('may'); ?></option>
                                    <option value="6" <?php if($thang==6) echo 'selected'; ?>><?php echo get_string('june'); ?></option>
                                    <option value="7" <?php if($thang==7) echo 'selected'; ?>><?php echo get_string('july'); ?></option>
                                    <option value="8" <?php if($thang==8) echo 'selected'; ?>><?php echo get_string('august'); ?></option>
                                    <option value="9" <?php if($thang==9) echo 'selected'; ?>><?php echo get_string('september'); ?></option>
                                    <option value="10" <?php if($thang==10) echo 'selected'; ?>><?php echo get_string('october'); ?></option>
                                    <option value="11" <?php if($thang==11) echo 'selected'; ?>><?php echo get_string('november'); ?></option>
                                    <option value="12" <?php if($thang==12) echo 'selected'; ?>><?php echo get_string('december'); ?></option>
                                </select>
                            </div>
                            <div class="col-12">
                                <label for=""><?php echo get_string('school_year'); ?><span style="color:red">*</span></label>
                                <select name="school_yearid" id="" class="form-control" required>
                                    <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                                    <?php
                                        $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
                                        $listschool_year = $DB->get_records_sql($sql);
                                        foreach ($listschool_year as $key => $value) {
                                        ?>
                                    <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
                                    <?php 
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px;">
                                <input type="submit" class="btn btn-success" value=" <?php echo get_string('export_file'); ?> " id="submitBtn">
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<?php
    echo $OUTPUT->footer();
    ?>
<script>
    $('#schoolid1').on('change', function() {
      var cua = this.value;
      if (this.value) {
          $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
              $("#block_student1").html(data);
          });
      }
    });
    var school1 = $('#schoolid1').find(":selected").val();
    if(school1){
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school1+"&blockedit="+blockedit,function(data){
        $("#block_student1").html(data);
      });
    }
    //   $('#schoolid').on('change', function() {
    //     var cua = this.value;
    //     if (this.value) {
    //         $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
    //             $("#block_student").html(data);
    //         });
    //     }
    // });
    // $('#block_student').on('change', function() {
    //     var block = this.value ;
    //     if (this.value) {
    //       $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
    //         $("#school_yearid").html(data);
    //       });
    //     }
    //   });
    //    $('#school_yearid').on('change', function() {
    //     var blockid = $('#school_yearid option:selected').attr('status');
    //     var school_yearid = this.value ;
    //     // alert(courseid);
    //     $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
    //       $("#groupid").html(data);
    //     });
      
    //   });
    //   $('#groupid').on('change', function() {
    //     var groupid = this.value ;
    //     var school_yearid2 = $('#groupid option:selected').attr('status');
    //     if (this.value) {
    //       $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
    //         $("#courseid").html(data);
    //       });
    //     }
    //   });
    //     var school = $('#schoolid').find(":selected").val();
    //     var blockedit = "<?php echo $block_student; ?>";
    //     var school_yearedit="<?php echo $school_year; ?>";
    //     var groupeit="<?php echo $groupid; ?>";
    //     var courseedit="<?php echo $courseid; ?>";
    //   if(school){
    //     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
    //       $("#block_student").html(data);
    //     });
    //       if(blockedit){
    //           $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+blockedit+"&school_yearedit="+school_yearedit,function(data){
    //               $("#school_yearid").html(data);
    //           });
    //       }
    //       if(school_yearedit){
    //          $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockedit+"&school_year="+school_yearedit+"&groupedit="+groupeit, function(data) {
    //             $("#groupid").html(data);
    //           }); 
    //       }
    //       if(courseedit){
    //           $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupeit+"&school_yearid2="+school_yearedit+"&courseedit="+courseedit,function(data){
    //               $("#courseid").html(data);
    //             });
    //       }
    //     }
    $('#schoolid').on('change', function() {
      var cua = this.value;
      if (this.value) {
          $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
              $("#block_student").html(data);
          });
      }
    });
    $('#block_student').on('change', function() {
      var block = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
          $("#school_yearid").html(data);
        });
      }
    });
     $('#school_yearid').on('change', function() {
      var blockid = $('#school_yearid option:selected').attr('status');
      var school_yearid = this.value ;
      // alert(courseid);
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
        $("#groupid").html(data);
      });
      
    });
    $('#groupid').on('change', function() {
      var groupid = this.value ;
      var school_yearid2 = $('#groupid option:selected').attr('status');
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
          $("#courseid").html(data);
        });
      }
    });
      var school = $('#schoolid').find(":selected").val();
      var blockedit = "<?php echo $block_student; ?>";
      var school_yearedit="<?php echo $school_year; ?>";
      var groupeit="<?php echo $groupid; ?>";
      var courseedit="<?php echo $courseid; ?>";
      
    if(school){
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
        $("#block_student").html(data);
      });
      }
      if(blockedit){
          $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+blockedit+"&school_yearedit="+school_yearedit,function(data){
              $("#school_yearid").html(data);
          });
      }
      if(school_yearedit){
         $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockedit+"&school_year="+school_yearedit+"&groupedit="+groupeit, function(data) {
            $("#groupid").html(data);
          }); 
      }
      if(courseedit){
          $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupeit+"&school_yearid2="+school_yearedit+"&courseedit="+courseedit,function(data){
              $("#courseid").html(data);
            });
      }
</script>
<script>
    function delete_evaluation(iddelete){
          var urlweb= "<?php echo $CFG->wwwroot ?>/manage/evaluation_student/index.php";
          var urldelte= "<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php";
          var e=confirm('<?php echo get_string('delete_evaluation_student'); ?>');
          if(e==true){
              $.ajax({
                  url:  urldelte,
                  type: "post",
                  data: {iddelete:iddelete} ,
                  success: function (response) {
                   window.location=urlweb;
               }
           });
          }
      }
</script>
<script>
    $("#checkAll").click(function () {
        $(".list").prop('checked', $(this).prop('checked'));
    });
    function get_check_lock(){
        $('#evaluationstudent').submit();
    }
</script>
<script>
  $(document).ready(function() {
    $(":checkbox").click(function(event) {
      if ($('.list').is(":checked"))
          $("#dellist").show();
      else
          $("#dellist").hide();
    });
  });
</script>