<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');
require_once ($CFG->dirroot. '/manage/evaluation_student/lib.php');
require_once ($CFG->dirroot. '/manage/evaluation/lib.php');
$PAGE->set_title(get_string('add_evaluation_student'));
$PAGE->set_heading(get_string('add_evaluation_student'));
$PAGE->set_pagelayout(get_string('add_evaluation_student'));
require_login(0, false);
echo $OUTPUT->header();

$school = optional_param('school', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
if($roleid!=10){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$schools=hien_thi_ds_truong_duoc_them_gan_cho_gv(10,$USER->id);
// var_dump($schools);
// $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
// $data = $DB->get_records_sql($sql);
$idnamhochientai=get_id_school_year_now();
$namhoc=$DB->get_record('school_year', array(
	'id' => $idnamhochientai
));
if(empty($idnamhochientai)){
	?>
    <script>
      alert('Hãy thêm năm học ứng với năm hiện tại'); 
    </script>
    <?php 
	echo displayJsAlert('Error', $CFG->wwwroot . "/manage/evaluation_student/index.php");
}
$time=time();
if ($action=='add_evaluation_student') {
  # code...
  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $groupid = optional_param('groupid', '', PARAM_TEXT);
  $studentid = optional_param('studentid', '', PARAM_TEXT);
  $school_yearid =$idnamhochientai;
  $month = optional_param('month', '', PARAM_TEXT);
  $toplic = optional_param('toplic', '', PARAM_TEXT);
  $attendance = optional_param('attendance', '', PARAM_TEXT);
  $learning_attitude = optional_param('learning_attitude', '', PARAM_TEXT);
  $material = optional_param('material', '', PARAM_TEXT);
  $participation = optional_param('participation', '', PARAM_TEXT);
  $cmt1 = optional_param('cmt1', '', PARAM_TEXT);
  $cmt2 = optional_param('cmt2', '', PARAM_TEXT);
  $cmt3 = optional_param('cmt3', '', PARAM_TEXT);
  $check=check_danh_gia_hoc_sinh_theo_thang_namhoc($studentid,$month,$school_yearid);
  if($check==1){
//     ?>
    <script>
      	alert('<?php echo get_string('isset_evaluation_student'); ?>'); 
//     </script>
    <?php 
    echo displayJsAlert('Error', $CFG->wwwroot . "/manage/evaluation_student/add_new_by_ta.php");
  }elseif($check==2){
    $newid=save_evaluation_student($courseid,$groupid,$studentid,$month,$school_yearid,$toplic,$attendance,$learning_attitude,$material,$participation,$cmt1,$cmt2,$cmt3,$USER->id);

    if($newid){
      add_notifications_for_student($studentid,'ĐÁNH GIÁ HỌC SINH','Thông báo về đánh giá của học sinh','/manage/evaluation_student/list_evaluation_student.php');
       echo displayJsAlert('Success', $CFG->wwwroot . "/manage/evaluation_student/index.php");
      
    }else{
       echo displayJsAlert('Error', $CFG->wwwroot . "/manage/evaluation_student/add_new_by_ta.php");
    }
  }
  
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="my_form_cua" action=""  method="post">
					<div class="row">
						<input type="text" name="action" value="add_evaluation_student" hidden="">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('school_year'); ?></label>
								<input type="text" placeholder="<?php echo $namhoc->sy_start.'-'.$namhoc->sy_end; ?>" class="form-control" disabled>
								<input type="text"  class="form-control" name="school_yearid" value="<?php echo $idnamhochientai; ?>" hidden>
								<!-- <select name="school_yearid" id="school_yearid" class="form-control" required>
									<?php if($data){
										foreach ($data as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id ?>"><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select> -->
							</div>
						</div>
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control">
								<option value=""><?php echo get_string('schools'); ?></option>
								<?php 
								foreach ($schools as $key => $value) {
									if($value->evaluation_student==1){
										?>
										<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
										<?php 
									}
								}
								?>
							</select>
						</div>
						
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('classgroup'); ?>  <span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value=""><?php echo get_string('classgroup'); ?></option>
							</select>
						</div>
						<div id="studentid" class="col-md-6">
							<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
							<select name="studentid" id="" class="form-control" required>
								<option value=""><?php echo get_string('student'); ?></option>
							</select>
						</div>
						<!-- <div class="col-md-6">
							<label class="control-label"><?php echo get_string('student'); ?><span style="color:red">*</span></label>
							<select name="studentid" id="" class="form-control" required>
								<option value=""><?php echo get_string('student'); ?></option>
							</select>
						</div> -->
						<div class="col-md-6">
				            <label class="control-label"><?php echo get_string('month'); ?></label>
				            <input type="text" placeholder="<?php echo date('m'); ?>" class="form-control" disabled>
							<input type="text"  class="form-control" name="month" value="<?php echo date('m'); ?>" hidden>
				           <!--  <select name="month" id="month" class="form-control">
				              <option value="1" <?php if(date('m')==1) echo 'selected';?>><?php echo get_string('january'); ?></option>
				              <option value="2" <?php if(date('m')==2) echo 'selected';?>><?php echo get_string('february'); ?></option>
				              <option value="3" <?php if(date('m')==3) echo 'selected';?>><?php echo get_string('march'); ?></option>
				              <option value="4" <?php if(date('m')==4) echo 'selected';?>><?php echo get_string('april'); ?></option>
				              <option value="5" <?php if(date('m')==5) echo 'selected';?>><?php echo get_string('may'); ?></option>
				              <option value="6" <?php if(date('m')==6) echo 'selected';?>><?php echo get_string('june'); ?></option>
				              <option value="7" <?php if(date('m')==7) echo 'selected';?>><?php echo get_string('july'); ?></option>
				              <option value="8" <?php if(date('m')==8) echo 'selected';?>><?php echo get_string('august'); ?></option>
				              <option value="9" <?php if(date('m')==9) echo 'selected';?>><?php echo get_string('september'); ?></option>
				              <option value="10" <?php if(date('m')==10) echo 'selected';?>><?php echo get_string('october'); ?></option>
				              <option value="11" <?php if(date('m')==11) echo 'selected';?>><?php echo get_string('november'); ?></option>
				              <option value="12" <?php if(date('m')==12) echo 'selected';?>><?php echo get_string('december'); ?></option>
				            </select> -->
				          </div>
				          <div class="col-md-6">
				            <label for="" class="control-label"><?php echo get_string('topic_month'); ?> <span style="color:red">*</span></label>
				            <input type="text" class="form-control" value="" name="toplic" required="" >
				          </div>
				          <div class="col-md-6">
				            <label class="control-label"><?php echo get_string('attendance'); ?>(Số ngày đi học)<span style="color:red">*</span></label>
				            <div id="chuyencan"> 
				              	<input type="text" name="attendance" id="" placeholder="<?php echo get_string('attendance'); ?>" class="form-control" required>
				            </div>
				          </div>
				          <div class="col-md-12">
				            <div class="alert-info">
				              <h4><?php echo get_string('assessment'); ?></h4>
				            </div>
				          </div>
				          <div class="col-md-12"></div>
				          <div class="col-md-4">
				            <label class="control-label"><?php echo get_string('learning_attitude') ?><span style="color:red">*</span></label>
				            <select name="learning_attitude" id="thaido" class="form-control" required>
				              <option value="A">A</option>
				              <option value="B">B</option>
				              <option value="C">C</option>
				              <option value="D">D</option>
				            </select>
				          </div>
				          <div class="col-md-4">
				            <label class="control-label"><?php echo get_string('material'); ?><span style="color:red">*</span></label>
				            <select name="material" id="dodung" class="form-control" required>
				              <option value="A">A</option>
				              <option value="B">B</option>
				              <option value="C">C</option>
				              <option value="D">D</option>
				            </select>
				          </div>
				          <div class="col-md-4">
				            <label class="control-label"><?php echo get_string('participation'); ?><span style="color:red">*</span></label>
				            <select name="participation" id="xdbai" class="form-control" required>
				              <option value="A">A</option>
				              <option value="B">B</option>
				              <option value="C">C</option>
				              <option value="D">D</option>
				            </select>
				          </div>
				          <div class="col-md-12">
				              <label for="" class="control-label"><?php echo get_string('other_comment'); ?></label>
				              <select name="cmt1" id="loadthaido" class="form-control">
				                <option value=""></option>
				              </select>
				              <select name="cmt2" id="loaddongdung" class="form-control">
				                <option value=""></option>
				              </select>
				               <select name="cmt3" id="loadxdbai" class="form-control">
				                <option value=""></option>
				              </select>
				          </div>
						
						
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
								<!-- <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " onclick="myFunction()"> -->
								<?php print_r(get_string('or')) ?>
								<?php 
								switch ($school) {
									case 1:
										$urlRedrict=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
									break;
									case 2:
										$urlRedrict= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
									break;
								}
								?>
								<a href="<?php echo $CFG->wwwroot.'/manage/evaluation_student/index.php' ?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	var thaido1 = $('#thaido').find(":selected").val();
  if (thaido1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido1,function(data){
        $("#loadthaido").html(data);
      });
  }
  $('#thaido').on('change', function() {
    var thaido = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?thaido="+thaido,function(data){
        $("#loadthaido").html(data);
      });
    }
  });
  // javascript load do dung hoc tao
  var dodung1 = $('#dodung').find(":selected").val();
  if (dodung1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung1,function(data){
        $("#loaddongdung").html(data);
      });
  }
  $('#dodung').on('change', function() {
    var dodung = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?dodung="+dodung,function(data){
        $("#loaddongdung").html(data);
      });
    }
  });
  // javascript load tham gia xd bai
  var xdbai1 = $('#xdbai').find(":selected").val();
  if (xdbai1) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai1,function(data){
        $("#loadxdbai").html(data);
      });
  }
  $('#xdbai').on('change', function() {
    var xdbai = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?xdbai="+xdbai,function(data){
        $("#loadxdbai").html(data);
      });
    }
  });
</script>
<script>
	var idnamhoc="<?php echo $idnamhochientai; ?>";
	var gvtg="<?php echo $USER->id; ?>";
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_gvtg="+cua+"&schoolyear_gvtg="+idnamhoc+"&id_gvtg_danhgia="+gvtg,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
    var groups = $('#courseid option:selected').attr('status');
    var courseid = this.value ;
    // alert(courseid);
    $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?nhomlop=" +courseid+"&namhoc="+idnamhoc+"&lophoc="+groups, function(data) {
      $("#studentid").html(data);
    });
    
  });
	// var userid="<?php echo $idstudent; ?>";
  var school_year = "<?php echo $idnamhochientai; ?>";
  var month1 = "<?php echo date('m'); ?>";
  $('#studentid').on('change', function() {
      var userid = $('#studentid option:selected').val() ;
      var course = $('#studentid option:selected').attr('status');
        $.get("<?php echo $CFG->wwwroot ?>/manage/evaluation_student/load_ajax.php?school_year="+school_year+"&month="+month1+"&userid="+userid+"&course="+course,function(data){
          $("#chuyencan").html(data);
        });
  }); 
  
</script>
<!-- <script>
	function myFunction() {
		var x, text;
		var userid = document.getElementById("studentid").value;
		var school_yearid = document.getElementById("school_yearid").value;
		$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?userid="+userid+"&school_yearid="+school_yearid,function(data){
			$("#my_form_cua").html(data);
		});

	}
</script> -->