<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

// Check for valid admin user - no guest autologin
require_login(0, false);
$strmessages = '';
$PAGE->set_title(get_string('report'));
$PAGE->set_heading(get_string('report'));
//now the page contents
$PAGE->set_pagelayout(get_string('report'));
echo $OUTPUT->header();
?>

<div id="admin-fullscreen">

    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
          
                        <div class="panel-head">
                          <h3><?php print_r(get_string('quickreport')) ?></h3>
                          <div class="subtitle"><?php print_r(get_string('thesereport')) ?></div>
                        </div>

                        
                            <table class="item-page-list">
                                <tbody><tr>
                                        <td>
                                          <div class="title"><a href="<?php echo new moodle_url('/course/report/outline/index.php'); ?>"><?php print_r(get_string('activityreport'))?></a></div>
                                            <div class="tip"><?php print_r(get_string('activityreport'));?></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <div class="title"><a href="<?php echo new moodle_url('/course/report/log/index.php', array('id' => 1)); ?>"><?php print_r(get_string('logsreport')) ?></a></div>
                                          <div class="tip"><?php print_r(get_string('selectacoursetoviewlog'))  ?></div>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                          <div class="title"><a href="<?php echo new moodle_url('/grade/report/outcomes/index.php'); ?>"><?php print_r(get_string('outcomesreport')) ?></a></div>
                                            <div class="tip"><?php print_r(get_string('outcomesreport')) ?></div>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                          <div class="title"><a href="<?php echo new moodle_url('/grade/report/user/index.php', array('userid'=> 0)); ?>"><?php print_r(get_string('userreport')) ?></a></div>
                                          <div class="tip"><?php print_r(get_string('userreport')) ?></div>
                                        </td>
                                    </tr>
                                    
                                     <tr>
                                        <td>
                                          <div class="title"><a href="<?php echo new moodle_url('/course/report/participation/index.php'); ?>"><?php print_r(get_string('participationreport')) ?></a></div>
                                            <div class="tip"><?php print_r(get_string('participationreport')) ?></div>
                                        </td>
                                    </tr>
                                   
                                    
                                </tbody></table>
                             
             
               
        </div> 
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div id="admin-fullscreen-right">
        <div class="side-panel"></div>
    </div>  <!-- End Right -->

</div>      

<?php
echo $OUTPUT->footer();
?>
