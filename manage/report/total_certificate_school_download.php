<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;

$PAGE->set_title('Báo cáo số lượng học sinh ');
$PAGE->set_heading('Báo cáo số lượng học sinh ');
require_login(0, false);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
function search_for_export_excel_tongchungchi($schoolid=null){
	global $DB;
	$sql="SELECT id, name FROM schools WHERE del =0 ";
	if(!empty($schoolid)){
		$sql.=" AND id= {$schoolid}";
	}
	$sql.=" ORDER BY name ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function dem_so_hs_lop_theo_nam_hoc_tongchungchi($schoolid,$school_yearid){
	global $DB;
	$time=time();
	$sql="SELECT COUNT(DISTINCT `history_student`.`iduser`)  
	FROM `history_student` 
	JOIN `user` ON `history_student`.`iduser`=`user`.`id`
	WHERE `user`.`del`=0
	AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)
	AND   `history_student`.`idschool`={$schoolid}
	AND  `history_student`.`idschoolyear`= {$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function lay_tongtiethoccuahstheonanhoc($userid,$school_yearid){
	global $DB;
	$sql="
	SELECT COUNT( DISTINCT `history_course_name`.`sotiet`) FROM `history_course_name` JOIN `history_student`  ON `history_course_name`.`courseid` = `history_student`.`idgroups` WHERE `history_student`.`iduser`={$userid} AND `history_student`.`idschoolyear`={$school_yearid}
	";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function check_chuyencanphailonhon75($tongtiet ,$userid,$school_yearid){
	global $DB;
	$sql= " SELECT ({$tongtiet} -(SELECT COUNT( DISTINCT `diemdanh`.`id`) FROM `diemdanh` WHERE `diemdanh`.`userid`={$userid} AND `diemdanh`.`schoolyearid`={$school_yearid})) / {$tongtiet} *100 >75";
	// $data = $DB->get_records_sql($sql);
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function check_chuyencanphailonhon75_2($userid,$school_yearid){
	global $DB;
	$sql= " 
	SELECT (
		(SELECT COUNT( DISTINCT `history_course_name`.`sotiet`) FROM `history_course_name` JOIN `history_student`  ON `history_course_name`.`courseid` = `history_student`.`idgroups` WHERE `history_student`.`iduser`={$userid} AND `history_student`.`idschoolyear`={$school_yearid})
	 -
	 	(SELECT COUNT( DISTINCT `diemdanh`.`id`) FROM `diemdanh` WHERE `diemdanh`.`userid`={$userid} AND `diemdanh`.`schoolyearid`={$school_yearid})
	) /
	(SELECT COUNT( DISTINCT `history_course_name`.`sotiet`) FROM `history_course_name` JOIN `history_student`  ON `history_course_name`.`courseid` = `history_student`.`idgroups` WHERE `history_student`.`iduser`={$userid} AND `history_student`.`idschoolyear`={$school_yearid}) *100 >75 ";
	// $data = $DB->get_records_sql($sql);
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hoc_sinh_du_dk_lay_chung_chi_tong_chung_chi($schoolid,$school_yearid){
	global $DB;
	$time=time();
	$hs=0;
	// sql lay id hs co diem hk lon hon 60
	$sql_userid="SELECT DISTINCT `semester`.`id` FROM `semester` 
	JOIN `user` ON `semester`.`userid` = `user`.`id` 
	AND `user`.`del`=0 
	AND `semester`.`tonghocki_2` >0 
	and `semester`.`tongket`>60 
	AND `semester`.`schoolid`={$schoolid} 
	AND `semester`.`school_yearid`={$school_yearid} 
	AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)";
	$list_userid = $DB->get_records_sql($sql_userid);
	if($list_userid){
		foreach ($list_userid as $value) {
			$chek=check_chuyencanphailonhon75_2($value->id,$school_yearid);
			$hs=$hs+$chek;
		}
	}
	return $hs;
	
}

$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
$title="BÁO CÁO TỔNG CHỨNG CHỈ NĂM HỌC  ".$namhoc->sy_start.'-'.$namhoc->sy_end; 
$data=search_for_export_excel_tongchungchi($schoolid=null);
	
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/total_certificate_school.xlsx');
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
	$numRow = 3; 
	$i=0;
	foreach ($data as $key => $row) {
		$i++;
		// $hs=dem_hs_nhom($row->id);
		$tonghs=dem_so_hs_lop_theo_nam_hoc_tongchungchi($row->id,$school_yearid); 
		$thoadk=dem_so_hoc_sinh_du_dk_lay_chung_chi_tong_chung_chi($row->id,$school_yearid);
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->name);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $tonghs);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $thoadk);
		

		$numRow++;
	}
	
	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-tong-chung-chi.xlsx"');
	$objWriter->save('php://output');
	exit;
	
?>