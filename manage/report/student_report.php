<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title(get_string('student_report'));
$PAGE->set_heading(get_string('student_report'));
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);

echo $OUTPUT->header();
require_login(0, false);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='report';
$name1='student_report';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
$data = $DB->get_records_sql($sql);
$idnamhochientai=get_id_school_year_now();
$namhoc=$DB->get_record('school_year', array(
  'id' => $idnamhochientai
));
$time=time();
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
<!-- <div class="col-md-2">
<a href="<?php echo new moodle_url('/manage/student/add_student.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
</div> -->
<div class="col-md-12 pull-right">
	<form action="<?php echo  $CFG->wwwroot;?>/manage/report/student_report_download.php" method="get" accept-charset="utf-8" class="form-row">
		<!-- <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="export" hidden=""> -->
    <div class="col-4">
      <div class="form-group label-floating">
        <label class="control-label"><?php echo get_string('school_year'); ?></label>
        <input type="text" placeholder="<?php echo $namhoc->sy_start.'-'.$namhoc->sy_end; ?>" class="form-control" disabled>
        <input type="text"  class="form-control" name="school_yearid" value="<?php echo $idnamhochientai; ?>" hidden>
      </div>
    </div>
		<div class="col-4">
			<label for=""><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
			<select name="schoolid" id="schoolid" class="form-control" required="">
				<option value=""><?php echo get_string('schools'); ?></option>
				<?php 
				$schools = get_all_shool();
				foreach ($schools as $key => $value) {
					?>
					<option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
					<?php 
				}
				?>
			</select>
		</div>
		<!--  <div class="col-4">
	         <label for=""><?php echo get_string('block_student'); ?><span style="color:red">*</sp</label>
	         <select name="block_student" id="block_student" class="form-control" required>
	             	<option value=""><?php echo get_string('block_student'); ?></option>
	        </select>
    </div>
         <div class="col-4">
                    <label for=""><?php echo get_string('choose_the_school_year'); ?><span style="color:red">*</span></label>
                    <select name="school_yearid" id="school_yearid" class="form-control" required="">
                      <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                   </select>
                </div>
              <div class="col-md-4">
                <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
                <select name="groupid" id="groupid" class="form-control" required>
                  <option value=""><?php echo get_string('class'); ?></option>
                </select>
              </div> -->
        <div class="col-md-4">
          <label class="control-label"><?php echo get_string('block_student'); ?> </label>
          <select name="block_student" id="block_student" class="form-control">
            <option value=""><?php echo get_string('block_student'); ?></option>
          </select>
        </div>
      <div class="col-md-4">
        <label class="control-label"><?php echo get_string('class'); ?></label>
        <select name="groupid" id="groupid" class="form-control" >
          <option value=""><?php echo get_string('class'); ?></option>
        </select>
      </div>
      <!-- required -->
		<div class="col-4">
			<div style="    margin-top: 29px;"></div>
			<button type="submit" class="btn btn-success"><?php echo get_string('export_excel'); ?></button>
		</div>
	</form>
</div>
</div>
<div class="table-responsive" data-pattern="priority-columns">

</div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
?>
<script>
	var idnamhoc="<?php echo $idnamhochientai; ?>";
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?schoolid="+cua+"&school_year_check="+idnamhoc,function(data){
        $("#block_student").html(data);
      });
    }
  });
  $('#block_student').on('change', function() {
    var idnamhoc1 = $('#block_student option:selected').attr('status');
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?block_student="+block+"&school_year_check="+idnamhoc1,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    var idnamhoc2 = $('#groupid option:selected').attr('status');
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?groupid_check="+groupid+"&school_year_check="+idnamhoc2,function(data){
        $("#courseid").html(data);
      });
    }
  });
</script>