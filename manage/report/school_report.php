<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title(get_string('school_report'));
$PAGE->set_heading(get_string('school_report'));
require_login(0, false);
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='report';
$name1='school_report';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

	
 ?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
					<div class="col-md-12 pull-right">
						<form action="<?php echo $CFG->wwwroot ?>/manage/report/school_report_download.php " method="get" accept-charset="utf-8" class="form-row">
							<!-- <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="export" hidden=""> -->
							<div class="col-4">
								<label for=""><?php echo get_string('schools'); ?> </label>
								<select name="schoolid" id="schoolid" class="form-control">
									<option value=""><?php echo get_string('schools'); ?></option>
									<?php 
									$schools = get_all_shool();
									foreach ($schools as $key => $value) {
										?>
										<option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
										<?php 
									}
									?>
								</select>
							</div>
							<div class="col-4">
								<div style="    margin-top: 29px;"></div>
								<button type="submit" class="btn btn-success"><?php echo get_string('export_excel') ?></button>
							</div>
						</form>
					</div>
				</div>
				<div class="table-responsive" data-pattern="priority-columns">

				</div>
			</div>
<!-- end content-->
		</div>
<!--  end card  -->
	</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
?>