<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
require_once($CFG->dirroot . '/manage/report/in.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
function show_tongket_hsth1($diem){
	$xeploai="Chờ kết quả thi";
	if($diem>=84.5){
		$xeploai= 'Giỏi'; 
	}
	if(($diem>=64.5)&&($diem<84.5)){
		$xeploai='Khá'; 
	}
	if(($diem>=49.5)&&($diem<64.5)){
		$xeploai= 'TB'; 
	}
	if($diem<49.5){
		$xeploai= 'Dưới TB'; 
	}
	return $xeploai;
}
function show_tongke_hsthpt1($diem){
	$xeploai="Chờ kết quả thi";
	if($diem>=79.5){
		$xeploai='Giỏi'; 
	}
	if(($diem>=64.5)&&($diem<79.5)){
		$xeploai= 'Khá'; 
	}
	if(($diem>=49.5)&&($diem<64.5)){
		$xeploai= 'TB'; 
	}
	if($diem<49.5){
		$xeploai= 'Dưới TB'; 
	}
	return $xeploai;

}

function lay_danh_sach_xuat_file_hoctap_cuoinam_hs_tieuhoc($school_yearid,$schoolid=null,$course=null){
	global $DB;
	$sql=" 
	SELECT `semester`.`id`, `semester`.`tonghocki_1`, `semester`.`s1_2`, `semester`.`s2_2`, `semester`.`s3_2`, `semester`.`s4_2`, `semester`.`s5_2`, `semester`.`noi_2`, `semester`.`nghe_2`, `semester`.`doc_viet_2`, `semester`.`tonghocki_2`, `semester`.`tongket`, `semester`.`courseid`, `user`.`firstname`, `user`.`lastname`, `course`.`fullname`, `schools`.`name`, `user`.`code`,`semester`.`note`,`semester`.`note_2` 
	FROM `semester` 
	JOIN `user` ON `user`.`id` = `semester`.`userid` 
	JOIN `course` ON `course`.`id` =`semester`.`courseid` 
	JOIN `groups` ON `groups`.`id` =`semester`.`groupid` 
	JOIN `schools` ON `schools`.`id` = `semester`.`schoolid` 
	WHERE `semester`.`school`=1 
	AND `semester`.`del`=0
	AND `semester`.`school_yearid` ={$school_yearid}
	";
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid`={$schoolid}";
	}
	if(!empty($course)){
		$sql.=" AND `semester`.`courseid`={$course}";
	}
	$sql.=" ORDER BY `user`.`firstname`,`user`.`lastname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function lay_danh_sach_xuat_file_hoctap_cuoinam_hs_thpt($school_yearid,$schoolid=null,$course=null){
	global $DB;
	$sql=" 
		SELECT 
		`semester`.`id`,
		`semester`.`tonghocki_1`, 
		`semester`.`s1_2`, 
		`semester`.`s2_2`, 
		`semester`.`s3_2`, 
		`semester`.`s4_2`, 
		`semester`.`s5_2`,
		`semester`.`noi_2`,
		`semester`.`viet_luan_2`,
		`semester`.`doc_hieu_2`,
		`semester`.`nghe_2`,
		`semester`.`tv_np_2`,
		`semester`.`tvnp_dochieu_viet_2`,
		`semester`.`tonghocki_2`,
		`semester`.`tongket`,
		`semester`.`courseid`,
		`semester`.`note`,
		`semester`.`note_2`,
		 `user`.`firstname`, 
		 `user`.`lastname`,
		 `course`.`fullname`,
		 `user`.`code`
		FROM `semester` 
		JOIN `user` ON `user`.`id` = `semester`.`userid` 
		JOIN `course` ON `course`.`id` =`semester`.`courseid` 
		JOIN `groups` ON `groups`.`id` =`semester`.`groupid` 
		JOIN `schools` ON `schools`.`id` = `semester`.`schoolid` 
		 WHERE `semester`.`school`=2 AND `semester`.`del`=0 
		 AND `semester`.`school_yearid` ={$school_yearid}
	";
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid`={$schoolid}";
	}
	if(!empty($course)){
		$sql.=" AND `semester`.`courseid`={$course}";
	}
	$sql.=" ORDER BY `user`.`firstname`,`user`.`lastname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}

$school=$DB->get_record('schools', array(
  'id' => $schoolid
));
$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
$course=$DB->get_record('course', array(
  'id' => $courseid
));
$teacher=ten_giao_vien_xuat_bao_cao_theo_nhom($courseid); 
$schedule=$DB->get_record('schedule', array(
  'courseid' => $courseid,
  'school_yearid' => $school_yearid,
  'del'=>0
));
$history_course_name=$DB->get_record('history_course_name', array(
  'school_year_id' => $school_yearid,
  'courseid' => $courseid,
  'groupid' => $groupid,
));
// $book =hien_thi_ten_sach_cua_nhom2($courseid,$school_yearid);
// $course_and_book=$course->fullname;
$course_and_book=$history_course_name->course_name;
if($schedule->book){
	$course_and_book=$course_and_book."&".$schedule->book;
}
$title='BÁO CÁO KẾT QUẢ HỌC TẬP NĂM HỌC '.$namhoc->sy_start.'-'.$namhoc->sy_end;
$t1="BẢNG ĐIỂM ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$t2="SPREADSHEET  ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$texten=$school->name." - FINAL REPORT ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$texten=mb_convert_case($texten, MB_CASE_UPPER, "UTF-8");
$textvi=$school->name." - BÁO CÁO KẾT QUẢ HỌC TẬP NĂM HỌC ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$textvi=mb_convert_case($textvi, MB_CASE_UPPER, "UTF-8");
$day_report=date('d-M-y',time());

	if($schoolstatus==1){
		$data=lay_danh_sach_xuat_file_hoctap_cuoinam_hs_tieuhoc($school_yearid,$schoolid,$courseid);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/year_end_academic_report_tieuhoc.xlsx');
		$objWorkSheet = $objPHPExcel->createSheet();
		$numRow = 7; 
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A1', $title) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A3', $t1) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A5', $t2) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A6', $school->name) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A8' , $course_and_book);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A10' , $day_report);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A12' , $school->name);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A14' , $teacher);
		$i=0;
		foreach ($data as $key => $row) {
			$i++;
			$name = $row->lastname.' '.$row->firstname;
			if($row->noi_2 >0) $tong=$row->tongket; else $tong='';
			if($tong>0){$xeploai=show_tongket_hsth1($tong);}else{$xeploai='Chờ kết quả thi';}
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('B' . $numRow, $i);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('C' . $numRow, $row->code);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('D' . $numRow, $name);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('E' . $numRow, $row->tonghocki_1);
			
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('F' . $numRow, $row->nghe_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('G' . $numRow, $row->doc_viet_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('H' . $numRow, $row->noi_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('I' . $numRow, $row->tonghocki_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('J' . $numRow, $tong);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('K' . $numRow, $xeploai);
			$numRow++;
			# code...

			$nxnoi=show_nhan_xet_ky_nang_noihk2($row->noi_2); 
			$nxnghe=show_nx_ky_nang_nghehk2($row->nghe_2);
			$nxndochieuviet=show_nx_ky_nang_dochieuvaviethk2($row->doc_viet_2);
			// cua add
			$objWorkSheet->setTitle("Worksheet$work_sheet");
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A1',$texten );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A2',$textvi );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D3',$school->name );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('K3',$i );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D4',$name );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D5',$course_and_book );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D6',$day_report );


         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J4',$row->noi_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J5',$row->nghe_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J6',$row->doc_viet_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J7',$row->tonghocki_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J8',$tong );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E8',$row->tonghocki_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E11',$nxnoi );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E12',$nxnghe );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E13',$nxndochieuviet );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A15',$row->note );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('I17',$teacher );

		}
		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Bao-cao-hoc-tap-cuoi-nam.xlsx"');
		$objWriter->save('php://output');
		exit;

	}elseif($schoolstatus==2){

		// $data=lay_danh_sach_xuat_file_hoctap_cuoinam_hs_thpt($school_yearid,$schoolid);
		$data=lay_danh_sach_xuat_file_hoctap_cuoinam_hs_thpt($school_yearid,$schoolid,$courseid);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/year_end_academic_report_trunghoc.xlsx');
		$objWorkSheet = $objPHPExcel->createSheet();
		$numRow = 7; 
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A1', $title) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A3', $t1) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A5', $t2) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A6', $school->name) ;
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A8' , $course_and_book);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A10' , $day_report);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A12' , $school->name);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A14' , $teacher);
		$i=0;
		foreach ($data as $key => $row) {
			$i++;
			$name = $row->lastname.' '.$row->firstname;
			if($row->noi_2 >0) $tong=$row->tongket; else $tong='';
			if($tong>0){$xeploai=show_tongke_hsthpt1($tong);}else{$xeploai='Chờ kết quả thi';}
			// $objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $row1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('B' . $numRow, $i);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('C' . $numRow, $row->code);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('D' . $numRow, $name);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('E' . $numRow, $row->tonghocki_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('F' . $numRow, $row->viet_luan_2);

			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('G' . $numRow, $row->nghe_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('H' . $numRow, $row->tv_np_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('I' . $numRow, $row->doc_hieu_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('J' . $numRow, $row->tvnp_dochieu_viet_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('K' . $numRow, $row->noi_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('L' . $numRow, $row->tonghocki_2);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('M' . $numRow,$tong);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('N' . $numRow,$xeploai);
			$numRow++;
			# code...

			$nxnoi=show_nhan_xet_ky_nang_noihk2($row->noi_2); 
			$nxnghe=show_nx_ky_nang_nghehk2($row->nghe_2);
			$nxndochieuviet=show_nx_ky_nang_dochieuvaviethk2($row->tvnp_dochieu_viet_2);
			// cua add
			$objWorkSheet->setTitle("Worksheet$work_sheet");
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A1',$texten );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A2',$textvi );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D3',$school->name );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('K3',$i );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D4',$name );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D5',$course_and_book );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D6',$day_report );


         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J4',$row->noi_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J5',$row->nghe_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J6',$row->tvnp_dochieu_viet_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J7',$row->tonghocki_2 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J8',$tong );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E8',$row->tonghocki_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E11',$nxnoi );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E12',$nxnghe );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E13',$nxndochieuviet );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A15',$row->note );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('I18',$teacher );
		}
		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Bao-cao-hoc-tap-cuoi-nam.xlsx"');
		$objWriter->save('php://output');
		exit;
	}
	
?>