<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
function search_school_to_export($idschool=null){
	global $DB;
	$sql="SELECT * FROM `schools` WHERE `schools`.`del`=0  ";
	if(!empty($idschool)){
		$sql.=" AND `schools`.`id`={$idschool}";
	}
	$sql.=" ORDER BY `schools`.`name` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;

}
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
	$data=search_school_to_export($schoolid);
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/school_report.xlsx');
	$numRow = 4;
	$i=1;
	foreach ($data as $row) {
		// thong tin hieu truong
		$quan=$DB->get_record('quanhuyen', array(
		  'id' => $row->district
		));
		$mot=$numRow+1;
		$hai=$numRow+2;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->name);

		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, 'Hiệu trưởng');
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->representative_name);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->birthday);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->representative_phone);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->representative_email);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->address);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $quan->name);
		// thong tin hieu pho 1
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $mot, $i+1);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $mot, $row->name);

		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $mot,'Hiệu phó 1');
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $mot, $row->deputy);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $mot, $row->deputy_bir);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $mot, $row->deputy_phone);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $mot, $row->deputy_mail);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $mot, '');

		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $mot, $quan->name);
		// thong tin hieu pho 2
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $hai, $i+2);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $hai, $row->name);

		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $hai, 'Hiệu phó 2');
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $hai, $row->deputy2);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $hai, $row->deputy2_bir);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $hai, $row->deputy2_phone);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $hai, $row->deputy2_mail);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $hai, '');

		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $hai, $quan->name);
		$numRow = $numRow+3;
		$i = $i+3;
	}

	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-truong-hoc.xlsx"');
	$objWriter->save('php://output');
	exit;

