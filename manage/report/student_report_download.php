<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);

$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
// danh sach hoc sinh trong lop
function dshscotronglop_nhung_k_thuocnhom($groupid){
	global $DB;
	$sql=" SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex`  FROM user 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` 
	JOIN `groups` ON `groups`.`id`= `groups_members`.`groupid` 
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `user`.`id` NOT IN (SELECT DISTINCT `user`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` JOIN `group_course` on `group_course`.`course_id`=`course`.`id` WHERE `group_course`.`group_id`={$groupid})

	AND `groups`.`id`={$groupid}
	ORDER BY `user`.`firstname` ASC
	";
	$data = $DB->get_records_sql($sql);
	return $data;
}

// danh sach hoc sinh trong nhom lop
function danhsachhocsinhtrongnhomlop_by_cua($groupid){
	global $DB;
	$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` ,`course`.`fullname`,`course`.`coefficient_name` FROM `user`
	 JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	 JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	 JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
	 JOIN `group_course` on `group_course`.`course_id`=`course`.`id` 
	 JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	 WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0
	 AND  `group_course`.`group_id`={$groupid}
	 ORDER BY `course`.`coefficient_name`,`course`.`fullname`, `user`.`firstname` ASC
	 ";
	// $sql = "SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` ,`course`.`fullname` FROM user 
	// JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	// JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` 
	// JOIN `groups` ON `groups`.`id`= `groups_members`.`groupid` 
	// JOIN `group_course` on `group_course`.`group_id`= `groups`.`id` 
	// JOIN `course` on `course`.`id`=`group_course`.`course_id` 
	// WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 

	// ";
	// if(!empty($groupid)){
	// 	$sql .= " AND `groups`.`id`={$groupid}";
	// }
	$data = $DB->get_records_sql($sql);
	return $data;
}
function danhsachhocsinhtrongnhomlop_theonamhoc($groupid,$school_yearid){
	global $DB;
	$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` ,`history_course_name`.`course_name` 
	FROM `user` 
	JOIN`history_student` ON `history_student`.`iduser` = `user`.`id` 
	JOIN `groups` ON `groups`.`id` =`history_student`.`idclass` 
	JOIN`course` ON `course`.`id` =`history_student`.`idgroups` 
	JOIN `history_course_name` ON `history_course_name`.`courseid` = `course`.`id` 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid`= 5 
	AND `user`.`del`=0 
	AND `history_course_name`.`school_year_id`={$school_yearid} 
	AND `history_student`.`idschoolyear`= {$school_yearid} 
	AND `groups`.`id`={$groupid} 
	ORDER BY `history_course_name`.`course_name`, `user`.`firstname` ASC
	 ";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function ds_hoc_sinh_tien_hanh_xuat_file_bao_cao_hs($schoolid,$block_student,$groupid,$school_yearid){
	global $DB;
	$sql="SELECT DISTINCT `user`.`id`,concat_ws(' ',`user`.`lastname`,`user`.`firstname`) as fullname ,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` ,`history_course_name`.`course_name`
	FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN `history_student` ON `history_student`.`iduser`=`user`.`id` 
	JOIN `history_course_name` ON `history_student`.`idgroups`=`history_course_name`.`courseid`
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0  
	";
    if (!empty($schoolid)) {
      $sql.=" AND `history_student`.`idschool`={$schoolid}";
      // $sql.=" AND `groups`.`id_truong`={$schoolid}";
    }
    if (!empty($block_student)) {
      $sql .= " AND `history_student`.`idblock`={$block_student} "; 
    }
    if (!empty($groupid)) {
      $sql .= " AND `history_student`.`idclass`={$groupid}";
    }
    if(!empty($courseid)){
      $sql.=" AND `history_student`.`idgroups`={$courseid} ";
    }
    if(!empty($school_yearid)){
        $sql.=" AND `history_student`.`idschoolyear`={$school_yearid} ";
    }
    $sql.=" ORDER BY `history_course_name`.`course_name`,`user`.`firstname` ASC ";
    $data=$DB->get_records_sql($sql);
    return $data;
}
// $cua=ds_hoc_sinh_tien_hanh_xuat_file_bao_cao_hs($schoolid,$block_student,$groupid,$school_yearid);
// var_dump($cua);die;
$school=$DB->get_record('schools', array(
  'id' => $schoolid
));
$group=$DB->get_record('groups', array(
  'id' => $groupid
));
if(!empty($school_yearid)){
	$idnamhoc=$school_yearid;
}else{
	$idnamhoc=$group->id_namhoc;
}
$namhoc=$DB->get_record('school_year', array(
  'id' => $idnamhoc
));
$title1="DANH SÁCH HỌC SINH LỚP ".$group->name." TRƯỜNG ".$school->name."
NĂM HỌC ".$namhoc->sy_start.'-'.$namhoc->sy_end;

	// $data=search_student_to_excel_bycua($schoolid,$block_student,$groupid); 
// $course2=show_name_course_by_cua2($student->id,$dk_namhoc_lay_tennhom);
$data=ds_hoc_sinh_tien_hanh_xuat_file_bao_cao_hs($schoolid,$block_student,$groupid,$school_yearid);
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/student_report.xlsx');
	$numRow = 3;
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title1);
	$i=0;
	foreach ($data as $row) {

		$i++;
		switch ($row->sex) {
			case 1: $sex='Nữ';break;
			case 2: $sex='Nam';break;
		}
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->code);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->fullname);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->birthday);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $sex);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->course_name);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->name_parent);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->phone1);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->email);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->time_start);
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->time_end);
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->description);
		$numRow++;
	}
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-hoc-sinh.xlsx"');
	$objWriter->save('php://output');
	exit;

// if(!empty($school_yearid)){
// 	$data=danhsachhocsinhtrongnhomlop_theonamhoc($groupid,$school_yearid);
// 	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
// 	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/student_report.xlsx');
// 	$numRow = 3;
// 	$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title1);
// 	$i=0;
// 	foreach ($data as $row) {

// 		$i++;
// 		$name = $row->lastname.' '.$row->firstname;
// 		switch ($row->sex) {
// 			case 1: $sex='Nữ';break;
// 			case 2: $sex='Nam';break;
// 		}
		
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->code);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $name);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->birthday);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $sex);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->course_name);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->name_parent);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->phone1);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->email);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->time_start);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->time_end);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->description);
// 		$numRow++;
// 	}
// 	$objPHPExcel->setActiveSheetIndex(0);
// 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 	ob_end_clean();
// 	// We'll be outputting an excel file
// 	header('Content-type: application/vnd.ms-excel');
// 	header('Content-Disposition: attachment; filename="Bao-cao-hoc-sinh.xlsx"');
// 	$objWriter->save('php://output');
// 	exit;

// }else{
// 	$data=danhsachhocsinhtrongnhomlop_by_cua($groupid);

// 	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
// 	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/student_report.xlsx');
// 	$numRow = 3;
// 	$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title1);
// 	$i=0;
// 	foreach ($data as $row) {

// 		$i++;
// 		$name = $row->lastname.' '.$row->firstname;
// 		$course = $row->coefficient_name.' '.$row->fullname;
// 		switch ($row->sex) {
// 			case 1: $sex='Nữ';break;
// 			case 2: $sex='Nam';break;
// 		}
		
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->code);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $name);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->birthday);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $sex);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $course);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->name_parent);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->phone1);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->email);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->time_start);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->time_end);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->description);
// 		$numRow++;
// 	}
	
// 	$data2=dshscotronglop_nhung_k_thuocnhom($groupid); 
// 	$numRow2=$numRow;
// 	$i2=$i;
// 	foreach ($data2 as  $value) {
// 		$i2++;
// 		$name = $value->lastname.' '.$value->firstname;
// 		switch ($value->sex) {
// 			case 1: $sex='Nữ';break;
// 			case 2: $sex='Nam';break;
// 		}
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow2, $i2);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow2, $value->code);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow2, $name);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow2, $value->birthday);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow2, $sex);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow2, 'Chưa được gán vào nhóm');
// 		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow2, $value->name_parent);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow2, $value->phone1);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow2, $value->email);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow2, $value->time_start);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow2, $value->time_end);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow2, $value->description);
// 		$numRow2++;
// 	}
// 	$objPHPExcel->setActiveSheetIndex(0);

// 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 	ob_end_clean();
// 	// We'll be outputting an excel file
// 	header('Content-type: application/vnd.ms-excel');
// 	header('Content-Disposition: attachment; filename="Bao-cao-hoc-sinh.xlsx"');
// 	$objWriter->save('php://output');
// 	exit;
// }
	
?>