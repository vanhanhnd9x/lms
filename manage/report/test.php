<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title(get_string('semester1_report'));
$PAGE->set_heading(get_string('semester1_report'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='report';
$name1='semester1_report';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
} 
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
					<div class="col-md-12 pull-right">
						<form action="<?php echo  $CFG->wwwroot;?>/manage/report/semester1_report_dowload.php" method="get" accept-charset="utf-8" class="form-row">
							<!-- <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="export" hidden=""> -->
						 
					    	<div class="col-4">
								<label for=""><?php echo get_string('type_school'); ?></label>
								<select name="schoolstatus" id="schoolstatus" class="form-control">
									<option value="1"><?php echo get_string('elementary'); ?></option>
									<option value="2"><?php echo get_string('high_school'); ?></option>
									
								</select>
							</div>
					    	<div class="col-4">
								<label for=""><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
								<select name="schoolid" id="schoolid" class="form-control" required>
									<option value=""><?php echo get_string('schools'); ?></option>
									<?php 
									//$schools = get_all_shool();
									//foreach ($schools as $key => $value) {
										?>
										<!-- <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option> -->
										<?php 
									//}
									?>
								</select>
							</div>
							<div class="col-md-4">
								<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
								<select name="block_student" id="block_student" class="form-control">
									<option value=""><?php echo get_string('block_student'); ?></option>
								</select>
							</div>
							<div class="col-4">
						        <label for=""><?php echo get_string('choose_the_school_year'); ?><span style="color:red">*</span></label>
						        <select name="school_yearid" id="school_yearid" class="form-control" required="">
							        <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
							        <?php 
							         	// $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
							         	// $school_year = $DB->get_records_sql($sql);
							         	// foreach ($school_year as $key => $value) {
							          // 	 # code...
							          // 	?>
							         	 <!-- <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option> -->
							           	<?php 
							         	// }
							         ?>
						       </select>
					    	</div>
							<div class="col-md-4">
								<label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
								<select name="groupid" id="groupid" class="form-control" required>
									<option value=""><?php echo get_string('class'); ?></option>
								</select>
							</div>
							<div class="col-md-4">
								<label class="control-label"><?php echo get_string('classgroup'); ?>  <span style="color:red">*</span></label>
								<select name="courseid" id="courseid" class="form-control" required>
									<option value=""><?php echo get_string('classgroup'); ?></option>
								</select>
							</div>
							<div class="col-4">
								<div style="    margin-top: 29px;"></div>
								<button type="submit" class="btn btn-info"><?php echo get_string('export_excel'); ?></button>
							</div>
						</form>
					</div>
				</div>
			<div class="table-responsive" data-pattern="priority-columns">

			</div>
		</div>
<!-- end content-->
	</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
?>

<script>

 // ajax edit
 var school = $('#schoolstatus').find(":selected").val();
 var schoolid= "<?php echo $schoolid; ?>";
 if (school) {
 	$.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?type="+school+"&schoolid="+schoolid,function(data){
  			$("#schoolid").html(data);
  	});
}
  // ajax add
  $('#schoolstatus').on('change', function() {
  	var cua = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?type="+cua+"&schoolid="+schoolid,function(data){
  			$("#schoolid").html(data);
  		});
  	}
  });
   $('#schoolid').on('change', function() {
  	var cua = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
  			$("#block_student").html(data);
  		});
  	}
  });
  $('#block_student').on('change', function() {
  	var block = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
  			$("#school_yearid").html(data);
  		});
  	}
  });
   $('#school_yearid').on('change', function() {
    var blockid = $('#school_yearid option:selected').attr('status');
    var school_yearid = this.value ;
    // alert(courseid);
    $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
      $("#groupid").html(data);
    });
    
  });
  $('#groupid').on('change', function() {
  	var groupid = this.value ;
  	var school_yearid2 = $('#groupid option:selected').attr('status');
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
  			$("#courseid").html(data);
  		});
  	}
  });
</script>