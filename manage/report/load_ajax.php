<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;

function get_shool_theo_cap($cap){
	global $CFG, $DB;
	$sql="SELECT id, name FROM schools WHERE del=0";
	if($cap==1){
		$sql.=" AND level =1";
	}
	if($cap==2){
		$sql.=" AND level IN (2,3)";
	}
	$data=$DB->get_records_sql($sql);
	return $data;
}
if(!empty($_GET['type'])){
	$cap=$_GET['type'];
	$school=get_shool_theo_cap($cap);
	echo '<option value="">'.get_string('schools').'</option>';
	foreach ($school as $value) {
		# code...
		?>
		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
		<?php 
	}
}
if(!empty($_GET['block_student'])){
	echo '<option value="">'.get_string('school_year').'</option>';
	$sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
	$school_year = $DB->get_records_sql($sql);
	foreach ($school_year as $key => $value) {
							          	 # code...
		?>
	<option value="<?php echo $value->id; ?>" status="<?php echo $_GET['block_student']; ?>" <?php if(!empty($_GET['school_yearedit'])&&($_GET['school_yearedit']==$value->id)) echo'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
		<?php 
	}
}
if(!empty($_GET['blockid'])&&!empty($_GET['school_year'])){
	echo '<option value="">'.get_string('class').'</option>';
	$idblock=$_GET['blockid'];
	$school_yearid=$_GET['school_year'];
	$sql_group="SELECT DISTINCT `groups`.`id`, `groups`.`name`
	FROM `groups` 
	JOIN `groups_year` ON `groups`.`id`= `groups_year`.`groupid` 
	JOIN `block_student` ON `block_student`.`id`= `groups`.`id_khoi` 
	WHERE `block_student`.`id`={$idblock} 
	AND `block_student`.`del`=0 
	AND `groups_year`.`schoolyearid`={$school_yearid}";
	$list_group = $DB->get_records_sql($sql_group);
	if(!empty($list_group)){
		foreach ($list_group as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" status="<?php echo $school_yearid; ?>" <?php if(!empty($_GET['groupedit'])&&($_GET['groupedit']==$value->id)) echo 'selected'; ?>><?php echo $value->name; ?></option>
			<?php 
		} 
	}
}
if(!empty($_GET['groupid'])&&!empty($_GET['school_yearid2'])){
	echo '<option value="">'.get_string('classgroup').'</option>';
	$groupid=$_GET['groupid'];
	$school_yearid2=$_GET['school_yearid2'];
	// $sql_course="SELECT `course`.`id`, `history_course_name`.`course_name` 
	// FROM `history_course_name`
	// JOIN `groups` ON `groups`.`id`= `history_course_name`.`groupid`
	// JOIN `course` ON `course`.`id` = `history_course_name`.`courseid`
	// WHERE `groups`.`id`={$groupid}
	// AND `history_course_name`.`school_year_id`={$school_yearid2}";
	$sql_course="SELECT `course`.`id`, `history_course_name`.`course_name` 
	FROM `history_course_name`
	JOIN `groups` ON `groups`.`id`= `history_course_name`.`groupid`
	JOIN `course` ON `course`.`id` = `history_course_name`.`courseid`
	WHERE `groups`.`id`={$groupid}
	AND `history_course_name`.`school_year_id`={$school_yearid2}";
	$list_course = $DB->get_records_sql($sql_course);
	if(!empty($list_course)){
		foreach ($list_course as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" <?php if(!empty($_GET['courseedit'])&&($_GET['courseedit']==$value->id)) echo 'selected'; ?>><?php echo $value->course_name; ?></option>
			<?php 
		}
	}
}
?>