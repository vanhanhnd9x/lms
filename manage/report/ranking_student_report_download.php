<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$semester = optional_param('semester', '', PARAM_TEXT);

// lấy nhóm them lớp 
function lay_nhom_cua_truong($schoolid){
	global $DB;
	$sql="SELECT DISTINCT `course`.`id`, `course`.`fullname` 
	FROM `course` 
	JOIN `group_course` ON `group_course`.`course_id` =`course`.`id` 
	JOIN `groups` ON `groups`.`id` =`group_course`.`group_id`
	WHERE `groups`.`id_truong` = {$schoolid} ORDER BY `course`.`fullname` ASC";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function lay_nhom_cua_truong2($schoolid,$school_yearid){
	global $DB;
	$sql="SELECT DISTINCT `course`.`id`, `course`.`fullname` , `history_course_name`.`course_name` 
	FROM `course` 
	JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id` 
	JOIN `groups` ON `groups`.`id` =`history_course_name`.`groupid` 
	WHERE `groups`.`id_truong` = {$schoolid} 
	AND `history_course_name`.`school_year_id`={$school_yearid} 
	ORDER BY `course`.`fullname` ASC";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function dem_hs_nhom($courseid){
	global $DB;
	$sql="SELECT COUNT(DISTINCT `user`.`id`) 
	FROM `course` 
	JOIN `enrol` ON `enrol`.`courseid` = `course`.`id` 
	JOIN `user_enrolments` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	JOIN `user` ON `user_enrolments`.`userid` = `user`.`id` 
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 AND `course`.`id`={$courseid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_hs_nhom2($courseid,$school_yearid){
	global $DB;
	$sql="SELECT COUNT(DISTINCT `user`.`id`) 
	FROM `course` 
	JOIN `enrol` ON `enrol`.`courseid` = `course`.`id` 
	JOIN `user_enrolments` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	JOIN `user` ON `user_enrolments`.`userid` = `user`.`id` 
	JOIN `history_student` ON `history_student`.`iduser`= `user`.`id` 
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid`= 5
	AND `user`.`del`=0 
	AND `course`.`id`= {$courseid}
	AND `history_student`.`idschoolyear`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hs_gioi($courseid,$school_yearid,$schoolstatus,$semester){
	global $DB;
	$sql=" SELECT COUNT(DISTINCT `semester`.`userid`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`courseid` = {$courseid} 
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
		$x=84.5;
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
		$x=79.5;
	}
	if($semester==1){
		$sql.=" AND `semester`.`tonghocki_1` >= {$x} ";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` >= {$x}";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hs_kha($courseid,$school_yearid,$schoolstatus,$semester){
	global $DB;
	$sql=" SELECT COUNT(DISTINCT `semester`.`userid`) FROM `semester` 
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`courseid` = {$courseid} 
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
		$x=64.5;
		$y=84.5;
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
		$x=64.5;
		$y=79.5;
	}
	if($semester==1){
		$sql.=" AND `semester`.`tonghocki_1` >= {$x} AND  `semester`.`tonghocki_1`<{$y}";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` >= {$x} AND  `semester`.`tongket`<{$y}";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hs_tb($courseid,$school_yearid,$schoolstatus,$semester){
	global $DB;
	$sql=" SELECT COUNT(DISTINCT `semester`.`id`) FROM `semester` 
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND `semester`.`del`=0 
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`courseid` = {$courseid} AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
	}
	if($semester==1){
		$sql.=" AND `semester`.`tonghocki_1` >= 49.5  AND `semester`.`tonghocki_1` <64.5 ";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` >= 49.5 AND `semester`.`tongket` <64.5";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hs_duoitb($courseid,$school_yearid,$schoolstatus,$semester){
	global $DB;
	$sql=" SELECT COUNT(DISTINCT `semester`.`id`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`courseid` = {$courseid} 
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
	}
	if($semester==1){
		$sql.="   AND `semester`.`tonghocki_1` <49.5 ";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` <49.5 ";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
$school=$DB->get_record('schools', array(
  'id' => $schoolid
));
$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
	// $data=lay_nhom_cua_truong($schoolid);
	$data=lay_nhom_cua_truong2($schoolid,$school_yearid);
	
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/ranking_student_report.xlsx');
	
	switch ($semester) {
		case 1: $title="KẾT QUẢ HỌC TẬP HỌC KÌ I NĂM HỌC ".$namhoc->sy_start.'-'.$namhoc->sy_end; 	break;
		case 1: $title="KẾT QUẢ HỌC TẬP NĂM HỌC ".$namhoc->sy_start.'-'.$namhoc->sy_end; 	break;
	}
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', $title);
	$objPHPExcel->getActiveSheet()->SetCellValue('B2', $school->name);
	$numRow = 4; 
	$i=0;
	$dem=count($data); 
	$tongsiso=0;
	$tonghsgioi=0;
	$tonghskha=0;
	$tonghstb=0;
	$tonghsdtb=0;
	$tonghshoanthanh=0;
	foreach ($data as $key => $row) {
		$i++;
		// $hs=dem_hs_nhom($row->id); 
		$hs=dem_hs_nhom2($row->id,$school_yearid); 
		$hsgioi=dem_so_hs_gioi($row->id,$school_yearid,$schoolstatus,$semester);
		$hskha=dem_so_hs_kha($row->id,$school_yearid,$schoolstatus,$semester);
		$hstb=dem_so_hs_tb($row->id,$school_yearid,$schoolstatus,$semester);
		$hsdtb=dem_so_hs_duoitb($row->id,$school_yearid,$schoolstatus,$semester);

		$hoanthanh=$hskha +$hstb;
		$tylhoanthanh=($hoanthanh/$hs)*100;

		$tlgioi=($hsgioi/$hs)*100;
		$tlkha=($hskha/$hs)*100;
		$tltb=($hstb/$hs)*100;
		$tlduoitb=($hsdtb/$hs)*100;

		$tongsiso=$tongsiso+$hs;
		$tonghsgioi=$hsgioi+$tonghsgioi;
		$tonghskha=$hskha+$tonghskha;
		$tonghstb=$hstb+$tonghstb;
		$tonghsdtb=$hsdtb+$tonghsdtb;
		$tonghshoanthanh=$tonghshoanthanh+$hoanthanh;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->course_name);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $hs);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $hsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $hskha);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $hstb);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $hsdtb);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, round($$tlgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, round($tlkha));
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, round($tltb));
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, round($tlduoitb));
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $hsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $hoanthanh);
		$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $hsdtb);
		$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, round($tlgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('P' . $numRow, round($tylhoanthanh));
		$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $numRow, round($tlduoitb));

		$numRow++;
	}
	$cua=$dem+4;
	$tongtlhsgioi=($tonghsgioi/$tongsiso)*100;
	$tongtlhskha=($tonghskha/$tongsiso)*100;
	$tongtlhstb=($tonghstb/$tongsiso)*100;
	$tongtlhsdtb=($tonghsdtb/$tongsiso)*100;
	$tongtlhshoanthanh=($tonghshoanthanh/$tongsiso)*100;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $cua, 'Tổng');
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $cua, $dem);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $cua, $tongsiso);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $cua, $tonghsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $cua, $tonghskha);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $cua, $tonghstb);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $cua, $tonghsdtb);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $cua, round($tongtlhsgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $cua, round($tongtlhskha));
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $cua, round($tongtlhstb));
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $cua, round($tongtlhsdtb));

		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $cua, $tonghsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $cua, $tonghshoanthanh);
		$objPHPExcel->getActiveSheet()->SetCellValue('N' . $cua, $tonghsdtb);
		$objPHPExcel->getActiveSheet()->SetCellValue('O' . $cua, round($tongtlhsgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('P' . $cua, round($tongtlhshoanthanh));
		$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $cua, round($tongtlhsdtb));

	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-ranking-theo-truong.xlsx"');
	$objWriter->save('php://output');
	exit;
	
?>