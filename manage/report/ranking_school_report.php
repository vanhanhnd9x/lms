<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title(get_string('report_ranking_school'));
$PAGE->set_heading(get_string('report_ranking_school'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
// $schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$semester = optional_param('semester', '', PARAM_TEXT);
// xóa file trên serve
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='report';
$name1='report_ranking_school';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
					<div class="col-md-12 pull-right">
						<form action="<?php echo $CFG->wwwroot ?>/manage/report/ranking_school_report_download.php " method="get" accept-charset="utf-8" class="form-row">
							<!-- <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="export" hidden=""> -->
						 	<div class="col-4">
						        <label for=""><?php echo get_string('choose_the_school_year'); ?><span style="color:red">*</span></label>
						        <select name="school_yearid" id="" class="form-control" required="">
							        <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
							        <?php 
							         	$sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
							         	$school_year = $DB->get_records_sql($sql);
							         	foreach ($school_year as $key => $value) {
							          	 # code...
							          	?>
							         	<option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
							          	<?php 
							         	}
							         ?>
						       </select>
					    	</div>
					    	<div class="col-4">
						        <label for=""><?php echo get_string('choose_semester'); ?><span style="color:red">*</span></label>
						        <select name="semester" id="" class="form-control" required="">
							        <option value="1"><?php echo get_string('semester1'); ?></option>
							        <option value="2"><?php echo get_string('all_year'); ?></option>
							        
						       </select>
					    	</div>
					    	<div class="col-4">
						        <label for=""><?php echo get_string('type_school'); ?><span style="color:red">*</span></label>
						        <select name="schoolstatus" id="" class="form-control" required="">
							        <option value="1"><?php echo get_string('elementary'); ?></option>
							        <option value="2"><!-- <?php echo get_string('high_school'); ?> --> THCS</option>
							        
						       </select>
					    	</div>
							<div class="col-4">
								<div style="    margin-top: 29px;"></div>
								<button type="submit" class="btn btn-success"><?php echo get_string('export_excel'); ?></button>
							</div>
						</form>
					</div>
				</div>
			<div class="table-responsive" data-pattern="priority-columns">

			</div>
		</div>
<!-- end content-->
	</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
?>

