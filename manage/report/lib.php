<?php 
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $DB, $USER, $CFG;


function export_dshs($idnamhoc, $idschool)
{
	global $DB;
	$sql = "
		SELECT semester.* FROM semester
		INNER JOIN user ON user.id = semester.userid
		JOIN `course` ON course.id = semester.courseid
		WHERE semester.schoolid={$idschool} AND semester.school_yearid={$idnamhoc}
		ORDER BY course.fullname,user.firstname ASC
	";
	return $DB->get_records_sql($sql);
}

function get_name_course($id=-1)
{
	global $DB;
	$sql = "
		SELECT * FROM course
		WHERE id=$id
	";
	return $DB->get_record_sql($sql);
}

function get_tongsotiet($courseid)
{
	global $DB;
	$sql = "
		SELECT SUM(sotiet) AS 'sum' FROM diemdanh
		WHERE course_id=$courseid;
	";
	return $DB->get_record_sql($sql)->sum;
}

function get_ngaynghi($userid)
{
	global $DB;
	$sql = "
		SELECT COUNT(userid) AS 'count' FROM diemdanh
		WHERE userid=$userid;
	";
	return $DB->get_record_sql($sql)->count;
}

function get_truong_hs($idtruong, $idnamhoc)
{
	global $DB;
	$sql = "
		SELECT groups_members.userid AS 'id' FROM groups_members
		JOIN groups
		ON groups_members.groupid = groups.id
		WHERE groups.id_truong = $idtruong AND groups.id_namhoc = $idnamhoc
	";
	return $DB->get_records_sql($sql);
}

function get_nhomlop_user($userid)
{
	global $DB;
	$sql = "
		SELECT enrol.courseid AS 'id' FROM enrol
		JOIN user_enrolments
		ON user_enrolments.enrolid = enrol.id
		WHERE user_enrolments.userid = $userid
	";
	return $DB->get_record_sql($sql)->id;
}


//-----------------------------------------
function get_user_from_id($user_id) {
    global $DB;
    $user = $DB->get_record('user', array(
        'id' => $user_id
    ));

    return $user;
}

function get_giaotrinh($idnhomlop, $namhoc) {
    global $DB;
    $user = $DB->get_record('schedule', array(
        'courseid' => $idnhomlop,
        'school_yearid' => $namhoc
    ));

    return $user;
}

function pout($input, $param)
{
	if($input< $param){
		return $input;
	}else
	  return '';
}

// cua add 
// nhận xét học sinh theo từng năm học

 ?>
