<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
function lay_danh_sach_giao_vien_theo_dk($idschool=null){
	global $DB;
	// $sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone2`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex`,`user`.`email`, `user`.`country`,`user`.`profile_img` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 8 AND `user`.`del`=0";
	if(!empty($idschool)){
		// $sql.=" AND `user`.`id` IN (SELECT DISTINCT `course`.`id_gv` FROM `course` JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` JOIN `groups` ON `groups`.`id` =`group_course`.`group_id` WHERE `groups`.`id_truong`= {$idschool})";
		$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone2`,`role_assignments`.`roleid`,`user`.`name_rht`,`user`.`code`,`user`.`time_start`, `user`.`typeoflabor`,`user`.`profile_img`,`user`.`country`,`user`.`name_ac`,`user`.`name_rht`
		FROM `user` 
		JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
		JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
		JOIN `course` ON `enrol`.`courseid`=`course`.`id`
		JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
		JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` 
		JOIN `groups` ON `groups`.`id` = `group_course`.`group_id` 
		WHERE `role_assignments`.`roleid`=8 AND `groups`.`id_truong` = {$idschool} AND `user`.`del`=0 ORDER BY `user`.`firstname` ASC";
	}else{
		$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone2`,`role_assignments`.`roleid`,`user`.`name_rht`,`user`.`name_ac`,`user`.`code`,`user`.`time_start`, `user`.`typeoflabor` ,`user`.`country`,`user`.`profile_img`
		FROM user 
		JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
		
		WHERE `role_assignments`.`roleid`= 8 AND `user`.`del`=0 ORDER BY `user`.`firstname` ASC
	    ";
	}
	$data = $DB->get_records_sql($sql);
	return $data;

}
function ds_truong_gvnn_day($idteacher){
	global $DB;
	$sql="SELECT DISTINCT`schools`.`name` 
		FROM `user` 
		JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
		JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
		JOIN `course` ON `enrol`.`courseid`=`course`.`id`
		JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
		JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` 
		JOIN `groups` ON `groups`.`id` = `group_course`.`group_id` 
		JOIN `schools` ON `schools`.`id` = `groups`.`id_truong`
		WHERE `role_assignments`.`roleid`=8 
		AND `user`.`del`=0
		AND `user`.`id`= {$idteacher}";
	// $sql="SELECT DISTINCT `schools`.`name` 
	// FROM `schools` JOIN `groups` ON `groups`.`id_truong`= `schools`.`id` 
	// JOIN `group_course` on `group_course`.`group_id` =`groups`.`id` 
	// JOIN `course` on `course`.`id` = `group_course`.`course_id` 
	// WHERE `course`.`id_gv`={$idteacher}";
	$data=$DB->get_records_sql($sql);
	$truong="";
	foreach ($data as $key => $value) {
		# code...
		$truong.=$value->name.',';
	}
	return $truong;

}
// xóa file trên serve
	
	$data=lay_danh_sach_giao_vien_theo_dk($schoolid); 

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/teacher_report.xlsx');
	// $objPHPExcel = new PHPExcel(); 
	// $objPHPExcel->getActiveSheet()->setTitle('Báo cáo lịch học');
	// $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STT');
	// $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Avatar');
	// $objPHPExcel->getActiveSheet()->SetCellValue('C1','Tên GVNN'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Quốc tịch');
	// $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Trường'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'SĐT'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Email'); 
	$numRow = 2;
	$i=0;
	foreach ($data as  $row) {
		$i++;
		# code...
		$name = $row->lastname.' '.$row->firstname;
		$rth=$DB->get_record('user',array('id'=>$row->name_rht,'del'=>0));
		$namerth = $rth->lastname.' '.$rth->firstname;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->code);
		if(!empty($row->profile_img)){
			$path1= $CFG->dirroot. '/account/logo/'.$row->profile_img;
			if( file_exists($path1)){
				$path= $CFG->dirroot. '/account/logo/'.$row->profile_img;
			}else{
				$path= $CFG->dirroot. '/account/logo/avata.jpg';
			}
		}else{
			$path= $CFG->dirroot. '/account/logo/avata.jpg';
		}
		$truong=ds_truong_gvnn_day($row->id);
		$objDrawing = new PHPExcel_Worksheet_Drawing(); 
		$objDrawing->setName('test_img'); 
		$objDrawing->setDescription('test_img'); 
		$objDrawing->setPath($path); 
		$objDrawing->setCoordinates('C'.$numRow);      
		//setOffsetX works properly 
		$objDrawing->setOffsetX(5); 
		$objDrawing->setOffsetY(5);     
		//set width, height 
		$objDrawing->setWidth(70); 
		$objDrawing->setHeight(70); 
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); 

		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $name);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->country);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $truong);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->phone2);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->email);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $namerth);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->name_ac);
		$numRow++;
	}
	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-GVNN.xlsx"');
	$objWriter->save('php://output');
	exit;
?>