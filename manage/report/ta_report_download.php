<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title('Báo cáo GVTG');
$PAGE->set_heading('Báo cáo GVTG');
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
function lay_danh_sach_giao_vien_tg_theo_dk($idschool=null){
	global $DB;
	if(!empty($idschool)){
		$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone2`,`role_assignments`.`roleid`,`user`.`name_rht`,`user`.`name_ac`,`user`.`code`,`user`.`time_start`, `user`.`typeoflabor` 
		FROM `user` 
		JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
		JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
		JOIN `course` ON `enrol`.`courseid`=`course`.`id`
		JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
		JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` 
		JOIN `groups` ON `groups`.`id` = `group_course`.`group_id` 
		WHERE `role_assignments`.`roleid`=10 AND `groups`.`id_truong` = {$idschool} AND `user`.`del`=0 ORDER BY `user`.`firstname` ASC";
	}else{
		$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone2`,`role_assignments`.`roleid`,`user`.`name_rht`,`user`.`name_ac`,`user`.`code`,`user`.`time_start`, `user`.`typeoflabor` 
		FROM user 
		JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
		WHERE `role_assignments`.`roleid`= 10 AND `user`.`del`=0 ORDER BY `user`.`firstname` ASC
	    ";
	}
	$data = $DB->get_records_sql($sql);
	return $data;

}
// lay danh sách nhóm mà gvtg quản lý
function show_loaihinhlaodongcuata(){
    $loaihinh=array(
    	'1'=>'GVTG trường biên chế',
    	'2'=>'GVTG trường hợp đồng',
    	'3'=>'GVTG LLV toàn thời gian',
    	'4'=>'GVTG LLV bán thời gian',
    );
    return $loaihinh;
}
function hien_thi_ds_nhom_lop_cuatrogiang($idta){
	global $DB;
	$sql=" SELECT DISTINCT `course`.`fullname` FROM `course` WHERE `course`.`id_tg`={$idta}";
	$data=$DB->get_records_sql($sql);
	$nhom="";
	foreach ($data as $key => $value) {
		# code...
		$nhom.=$value->fullname.','; 
	}
	return $nhom;
}
function dem_so_nhom_cua_trogiang($idta){
	global $DB;
	$sql=" SELECT count(DISTINCT `course`.`id`)  FROM `course` WHERE `course`.`id_tg`={$idta}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_truong_cuatrogiang($idta){
	global $DB;
	$sql="SELECT count(DISTINCT `schools`.`id`) 
	FROM `schools` JOIN `groups` ON `groups`.`id_truong`= `schools`.`id` 
	JOIN `group_course` on `group_course`.`group_id` =`groups`.`id` 
	JOIN `course` on `course`.`id` = `group_course`.`course_id` 
	WHERE `course`.`id_tg`={$idta}
	AND `schools`.`del`=0
	";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function hien_thi_ds_truong_cuatrogiang($idta){
	global $DB;
	$sql="SELECT DISTINCT `schools`.`name` 
	FROM `schools` JOIN `groups` ON `groups`.`id_truong`= `schools`.`id` 
	JOIN `group_course` on `group_course`.`group_id` =`groups`.`id` 
	JOIN `course` on `course`.`id` = `group_course`.`course_id` 
	WHERE `course`.`id_tg`={$idta} 
	AND `schools`.`del`=0 ";
	$data=$DB->get_records_sql($sql);
	$truong="";
	foreach ($data as $key => $value) {
		# code...
		$truong.=$value->name.',';
	}
	return $truong;
}
function show_mang($mang){
	$chuoi=implode(",", $mang);
	return $chuoi;
}
$loaihinh=show_loaihinhlaodongcuata();
	$data=lay_danh_sach_giao_vien_tg_theo_dk($schoolid);

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/ta_report.xlsx');
	// $objPHPExcel = new PHPExcel(); 
	
	 
	// $objPHPExcel->getActiveSheet()->setTitle('Báo cáo GVTG');
	// $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STT');
	// $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Tên GVTG');
	// $objPHPExcel->getActiveSheet()->SetCellValue('C1','Ngày sinh'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SĐT');
	// $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Email'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Nhóm lớp'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Trường'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'RHTA'); 
	$numRow = 2;
	$i=0;

	foreach ($data as  $row) {
		$namerta='';
		if(!empty($row->name_rht )){
			$rhta=$DB->get_record('user', array(
  			'id' => $row->name_rht,
  			'del'=>0
			));
			$namerta = $rhta->lastname.' '.$rhta->firstname;
		}
		# code...
		
		$name = $row->lastname.' '.$row->firstname;
		// $nhom=hien_thi_ds_nhom_lop_cuatrogiang($row->id);
		$sonhom=dem_so_nhom_cua_trogiang($row->id);
		$truong=hien_thi_ds_truong_cuatrogiang($row->id);
		$sotruong=dem_so_truong_cuatrogiang($row->id);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $row->code);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->lastname);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->firstname);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $name);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->birthday);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->phone2);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->email);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->time_start);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $loaihinh[$row->typeoflabor]);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, 'TA');
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $sotruong);
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $sonhom);
		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $truong);
		$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $namerta);
		$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, $row->name_ac);
		$numRow++;
	}
	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-GVTG.xlsx"');
	$objWriter->save('php://output');
	exit;
?>