<?php 
require_once('../../config.php');
global $DB, $USER, $CFG;
function show_nhan_xet_ky_nang_noi($noi){
	$nx='';
	if($noi>=21.1){
		$nx="Your child is able to use most sentence structures correctly,confidently and with few
			 errors. Please encourage your child to continue the hard work.
			Con có thể thực hành và vận dụng hầu hết các mẫu câu đã học một cách thuần thục,
			tự tin và rất ít khi mắc lỗi. Phụ huynh hãy động viên con tiếp tục phát huy.";
	}
	if((16.1<=$noi)&&($noi<21.1)){
		$nx="Your child's ability to communicate is good for this level of study.
			 Your child can use most of the structures learned thus far on the course, however, 
			 your child needs to practise more to be able to communicate more fluently and accurately.
			 Khả năng giao tiếp của con ở mức độ khá. Con cũng đã biết vận dụng hầu hết các cấu trúc
			 ngữ pháp đã học, tuy nhiên, con cần thực hành nhiều hơn nữa để có thể giao tiếp trôi 
			 chảy và đảm bảo độ chính xác.";
	}
	if((12.4<=$noi)&&($noi<16.1)){
		$nx="Your child's ability to apply grammartical structures learned thus far while 
		speaking is average. There is much room for improvement in basic structures, grammar, 
		and vocabulary.
		Khi thực hành nói, khả năng ứng dụng các cấu trúc ngữ pháp đã được học của con ở mức trung 
		bình.Con cần ôn tập lại các mẫu câu, từ vựng và ngữ pháp đã học của trình độ này để giao tiếp
		hiệu quả hơn.";
	}
	if($noi<12.4){
		$nx="Your child's spoken ability at this level of study is below average.
			 Your child makes common mistakes when speaking, and needs to be more proactive and
			 attentive in class. Your child also needs to revise structures and vocabulary learned
			 on this course for improved results.
			 Khả năng nói của con ở trình độ này chưa đạt yêu cầu. Khi thực hành nói, 
			 con vẫn còn mắc một số lỗi thông thường, con cần mạnh dạn và tập trung hơn trong giờ học. 
			 Con cũng cần ôn tập lại các mẫu câu, từ vựng đã học để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
 function show_nx_ky_nang_nghe($nghe){
 	$nx='';
 	if($nghe>=15.9){
		$nx="Your child's listening and comprehension skills are excellent. Your child has a strong
			 grasp of vocabulary and grammatical structures taught at this level of study. 
			 Please keep up hard work.
			Kỹ năng nghe và hiểu của con tốt. Con nắm chắc các cấu trúc ngữ pháp và từ vựng được dạy 
			ở trình độ này. Con hãy tiếp tục phát huy.";
	}
	if((12.9<=$nghe)&&($nghe<15.9)){
		$nx="Your child's listening skills are good. Your child can listen and comprehend almost all
			 the structures and vocabulary at this level of study. However, your child should continue
			 to expand the vocabulary and develop in this area for improved results.
			Kỹ năng nghe hiểu của con khá. Con nghe và hiểu được hầu hết các cấu trúc ngữ pháp và từ
			vựng ở trình độ này. Tuy nhiên, con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để
			đạt kết quả tốt hơn."
;
	}
	if((9.9<=$nghe)&&($nghe<12.9)){
		$nx="Your child's listening skills are average. Your child is able to understand most of the
			 structures and vocabulary at this level, but needs improvement. Your child should try to
			 expand vocabulary and practise listening skills more at home.
			Kỹ năng nghe của con ở mức trung bình. Con hiểu được phần lớn các cấu trúc ngữ pháp và từ
			vựng ở trình độ này. Tuy nhiên, con cần cố gắng nhiều hơn bằng cách mở rộng vốn từ vựng và 
			luyện nghe nhiều hơn ở nhà."
;
	}
	if($nghe<9.9){
		$nx="Your child's listening skills for this level of study are below average. Your child needs
			 to be more attentive in class and practise more at home. Please encourage your child to
			 include English songs, TV programs, or other audio programs in the study. 
			Kỹ năng nghe của con ở trình độ này chưa đạt yêu cầu. Con cần chú ý tập trung hơn trong
			giờ học và luyện thêm các bài nghe ở nhà. Phụ huynh hãy khuyến khích con nghe thêm các bài 
			hát, các chương trình tiếng Anh trên đài và ti vi để đạt kết quả tốt hơn."
;
	}
	return $nx;
 }
 function show_nx_ky_nang_dochieuvaviet($kynang){
 	$nx='';
 	if($kynang>=43.7){
		$nx="Your child's reading and writing skills are very good. Your child's level of reading
			 comprehension and written fluency are strong for this level of study. 
			Kỹ năng đọc hiểu và viết của con tốt. Con có thể đọc, nhớ và viết được từ đồng thời sử 
			dụng thành thạo các cấu trúc câu theo yêu cầu của trình độ này."
;
	}
	if((35.5<=$kynang)&&($kynang<43.7)){
		$nx="Your child's reading and writing skills are good for this level. Your child can read, and
			 reproduce most of the words and sentence structures. To improve further, your child 
			 should read short stories in English that are suitable to your child's level of study.
			Kỹ năng đọc hiểu và viết của con khá. Con có thể đọc, vận dụng lại được hầu hết các từ và
			cấu trúc câu ở trình độ này. Con nên luyện đọc các truyện ngắn bằng tiếng Anh phù hợp với
			trình độ để đạt kết quả tốt hơn."
;
	}
	if((27.2<=$kynang)&&($kynang<35.5)){
		$nx="Your child's reading and writing skills for this level of study are average. Your child
			 must work hard to continue to develop skills in this area. Please encourage your child to
			 practise reading and writing more at home to get better results.
			Kỹ năng đọc hiểu và viết của con ở mức trung bình. Con cần học chăm chỉ hơn để cải thiện
			các kỹ năng này. Phụ huynh hãy động viên con thực hành đọc và viết nhiều hơn ở nhà để đạt
			kết quả cao hơn.";

	}
	if($kynang<27.2){
		$nx="Your child's comprehensive reading and writing skills are below average. Your child needs
		 to try harder for improved results.
		Khả năng đọc hiểu và viết của con chưa đạt yêu cầu. Con cần cố gắng nhiều hơn nữa để đạt kết 
		quả tốt hơn.";
	}
	return $nx;
 }
 function ten_giao_vien_xuat_bao_cao_theo_nhom($courseid){
 	global $DB;
 	$sql_idteacher=" SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` ={$courseid} AND `role_assignments`.`roleid`= 8";
	$idgvnn=$DB->get_field_sql($sql_idteacher,null,$strictness=IGNORE_MISSING);
	$gvnn=$DB->get_record('user', array(
		'id' => $idgvnn,
		'del'=>0
	));
	$name= $gvnn->lastname.' '.$gvnn->firstname;
	return $name;
 }

 /// hien thi nhan xet theo nam hoc
 // nhan xet hoc ky 1
 function show_nhan_xet_ky_nang_noihk1($noi){
	$nx='';
	if($noi>=21.1){
		$nx="Your child's listening and comprehension skills are excellent. Your child has a strong grasp of vocabulary and grammatical structures taught at this level of study. Please keep up hard work.Kỹ năng nghe và hiểu của con tốt. Con nắm chắc các cấu trúc ngữ pháp và từ vựng được dạy ở trình độ này. Con hãy tiếp tục phát huy.";
	}
	if((16.1<=$noi)&&($noi<21.1)){
		$nx="Your child's listening skills are good. Your child can listen and comprehend almost all the structures and vocabulary at this level of study. However, your child should continue to expand the vocabulary and develop in this area for improved results.Kỹ năng nghe hiểu của con khá. Con nghe và hiểu được hầu hết các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để đạt kết quả tốt hơn.";
	}
	if((12.4<=$noi)&&($noi<16.1)){
		$nx="Your child's ability to apply grammartical structures learned thus far while speaking is average. There is much room for improvement in basic structures, grammar, and vocabulary.Khi thực hành nói, khả năng ứng dụng các cấu trúc ngữ pháp đã được học của con ở mức trung bình. Con cần ôn tập lại các mẫu câu, từ vựng và ngữ pháp đã học của trình độ này để giao tiếp hiệu quả hơn.";
	}
	if((0<$noi)&&($noi<12.4)){
		$nx="Your child's spoken ability at this level of study is below average. Your child makes common mistakes when speaking, and needs to be more proactive and attentive in class. Your child also needs to revise structures and vocabulary learned on this course for improved results.Khả năng nói của con ở trình độ này chưa đạt yêu cầu. Khi thực hành nói, con vẫn còn mắc một số lỗi thông thường, con cần mạnh dạn và tập trung hơn trong giờ học. Con cũng cần ôn tập lại các mẫu câu, từ vựng đã học để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
 function show_nx_ky_nang_nghehk1($nghe){
 	$nx='';
 	if($nghe>=16.9){
		$nx="Your child's listening and comprehension skills are excellent. Your child has a strong grasp of vocabulary and grammatical structures taught at this level of study. Please keep up hard work.Kỹ năng nghe và hiểu của con tốt. Con nắm chắc các cấu trúc ngữ pháp và từ vựng được dạy ở trình độ này. Con hãy tiếp tục phát huy.";
	}
	if((12.9<=$nghe)&&($nghe<16.9)){
		$nx="Your child's listening skills are good. Your child can listen and comprehend almost all the structures and vocabulary at this level of study. However, your child should continue to expand the vocabulary and develop in this area for improved results.Kỹ năng nghe hiểu của con khá. Con nghe và hiểu được hầu hết các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để đạt kết quả tốt hơn.";
	}
	if((9.9<=$nghe)&&($nghe<12.9)){
		$nx="Your child's listening skills are average. Your child is able to understand most of the structures and vocabulary at this level, but needs improvement. Your child should try to expand vocabulary and practise listening skills more at home.Kỹ năng nghe của con ở mức trung bình. Con hiểu được phần lớn các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần cố gắng nhiều hơn bằng cách mở rộng vốn từ vựng và luyện nghe nhiều hơn ở nhà.";
	}
	if((0<$nghe)&&($nghe<9.9)){
		$nx="Your child's listening skills for this level of study are below average. Your child needs to be more attentive in class and practise more at home. Please encourage your child to include English songs, TV programs, or other audio programs in the study. Kỹ năng nghe của con ở trình độ này chưa đạt yêu cầu. Con cần chú ý tập trung hơn trong giờ học và luyện thêm các bài nghe ở nhà. Phụ huynh hãy khuyến khích con nghe thêm các bài hát, các chương trình tiếng Anh trên đài và ti vi để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
  function show_nx_ky_nang_dochieuvaviethk1($kynang){
 	$nx='';
 	if($kynang>=46.5){
		$nx="Your child's reading and writing skills are very good. Your child's level of reading comprehension and written fluency are strong for this level of study. Kỹ năng đọc hiểu và viết của con tốt. Con có thể đọc, nhớ và viết được từ đồng thời sử dụng thành thạo các cấu trúc câu theo yêu cầu của trình độ này.";
	}
	if((35.5<=$kynang)&&($kynang<46.5)){
		$nx="Your child's reading and writing skills are good for this level. Your child can read, and reproduce most of the words and sentence structures. To improve further, your child should read short stories in English that are suitable to your child's level of study.Kỹ năng đọc hiểu và viết của con khá. Con có thể đọc, vận dụng lại được hầu hết các từ và cấu trúc câu ở trình độ này. Con nên luyện đọc các truyện ngắn bằng tiếng Anh phù hợp với trình độ để đạt kết quả tốt hơn.";
	}
	if((27.2<=$kynang)&&($kynang<35.5)){
		$nx="Your child's reading and writing skills for this level of study are average. Your child must work hard to continue to develop skills in this area. Please encourage your child to practise reading and writing more at home to get better results.Kỹ năng đọc hiểu và viết của con ở mức trung bình. Con cần học chăm chỉ hơn để cải thiện các kỹ năng này. Phụ huynh hãy động viên con thực hành đọc và viết nhiều hơn ở nhà để đạt kết quả cao hơn.";

	}
	if((0<$kynang)&&($kynang<27.2)){
		$nx="Your child's comprehensive reading and writing skills are below average. Your child needs to try harder for improved results.Khả năng đọc hiểu và viết của con chưa đạt yêu cầu. Con cần cố gắng nhiều hơn nữa để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
 // nhan xet hoc ky 2
 function show_nhan_xet_ky_nang_noihk2($noi){
	$nx='';
	if($noi>=21.1){
		$nx="During the examination, your child's performance was excellent. Your child was able to use most sentence structures correctly, confidently and with few errors. Con đã thể hiện xuất sắc trongbài thi. Con đã vận dụng được hầu hết các mẫu câu đã học một cách thuần thục, tự tin và ít khi mắc lỗi.";
	}
	if((16.1<=$noi)&&($noi<21.1)){
		$nx="During the examination, your child's ability to communicate was good for this level of study. Your child could use most of the structures learned thus far on the course, however, your child needs to practise more to be able to communicate more fluently and accurately.Trong bài thi, con thể hiện kỹ năng giao tiếp ở mức khá. Con đã biết vận dụng hầu hết các cấu trúc ngữ pháp đã học, tuy nhiên, con cần thực hành nhiều hơn để có thể giao tiếp trôi chảy và chính xác.";
	}
	if((12.4<=$noi)&&($noi<16.1)){
		$nx="During the examination, your child's ability to apply grammatical structures learned thus far in communication is average. There is much room for improvement in basic structures, grammar and vocabulary.Trong bài thi, khả năng vận dụng các cấu trúc ngữ pháp đã học của con trong giao tiếp ở mức trung bình. Con cần luyện tập thêm các mẫu câu, từ vựng và ngữ pháp đã học để giao tiếp hiệu quả hơn.";
	}
	if((0<$noi)&&($noi<12.4)){
		$nx="During the examination, your child's spoken ability at this level of study was below average. Your child made common mistakes when speaking. Your child needs to revise structures and vocabulary learned on this course for improved results.Trong bài thi, kỹ năng nói của con chưa đạt yêu cầu. Con vẫn còn mắc một số lỗi thông thường. Con cần ôn tập lại các mẫu câu và từ vựng đã học của trình độ này để đạt được kết quả học tập tốt hơn.";
	}
	return $nx;
 }
  function show_nx_ky_nang_nghehk2($nghe){
 	$nx='';
 	if($nghe>=16.9){
		$nx="During the examination, your child's listening and comprehension skills were excellent. Your child had a strong grasp of vocabulary and grammatical structures taught at this level of study.Trong bài thi, kỹ năng nghe hiểu của con được đánh giá tốt. Con nắm chắc các từ vựng và cấu trúc ngữ pháp được dạy ở trình độ này.";
	}
	if((12.9<=$nghe)&&($nghe<16.9)){
		$nx="During the examination, your child's listening and comprehension skills were good. Your child was able to listen and comprehend almost all the structures and vocabulary at this level of study. However, your child should continue to expand vocabulary and develop in this area for improved results.Trong bài thi, kỹ năng nghe hiểu của con được đánh giá ở mức khá. Con nghe hiểu được hầu hết các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần tiếp tục củng cố và mở rộng thêm vốn từ vựng để đạt kết quả tốt hơn.";
	}
	if((9.9<=$nghe)&&($nghe<12.9)){
		$nx="During the examination, your child's listening and comprehension skills were average. Your child was able to understand most of structures and vocabulary at this level, but needs improvement. Your child should try to expand their vocabulary, and practise listening skills more at home.Trong bài thi, kỹ năng nghe hiểu của con được đánh giá ở mức trung bình. Con hiểu được phần lớn các cấu trúc ngữ pháp và từ vựng ở trình độ này. Tuy nhiên, con cần cố gắng hơn bằng cách mở rộng vốn từ vựng và luyện nghe nhiều hơn ở nhà.";
	}
	if((0<$nghe)&&($nghe<9.9)){
		$nx="During the examination, your child's listening and comprehension skills for this level of study was below average. Your child needs to be more attentive in class and practise more at home.Trong bài thi, kỹ năng nghe hiểu của con chưa đạt yêu cầu. Con cần chú ý tập trung hơn trong giờ học và thực hành thêm ở nhà.";
	}
	return $nx;
 }
 function show_nx_ky_nang_dochieuvaviethk2($kynang){
 	$nx='';
 	if($kynang>=46.5){
		$nx="During the examination, your child's reading and writing skills were very good. Your child's level of reading comprehension and written fluency were strong for this level of study. Trong bài thi, kỹ năng đọc hiểu và viết của con được đánh giá rất tốt. Con có thể đọc hiểu và viết thành thạo theo yêu cầu của trình độ này.";
	}
	if((35.5<=$kynang)&&($kynang<46.5)){
		$nx="During the examination, your child's reading and writing skills were good for this level. Your child can read and reproduce most of the words and sentence structures at this level. Trong bài thi, kỹ năng đọc hiểu và viết của con được đánh giá ở mức độ khá. Con có thể đọc, vận dụng lại được hầu hết các từ và cấu trúc câu ở trình độ này. ";
	}
	if((27.2<=$kynang)&&($kynang<35.5)){
		$nx="During the examination, your child's reading and writing skills for this level of study were average. You should practise reading and writing more at home to get better results.Trong bài thi, kỹ năng đọc hiểu và viết của con được đánh giá ở mức trung bình. Con nên thực hành đọc và viết nhiều hơn ở nhà để đạt kết quả cao hơn.";

	}
	if((0<$kynang)&&($kynang<27.2)){
		$nx="During the examination, your child's comprehensive reading and writing skills were below average. Your child needs to try harder for improved results.Trong bài thi, khả năng đọc hiểu và viết của con chưa đạt yêu cầu. Con cần cố gắng nhiều hơn nữa để đạt kết quả tốt hơn.";
	}
	return $nx;
 }
?>