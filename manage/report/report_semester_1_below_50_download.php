<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);

function lay_hoc_sinh_diem_thihk1duoi50($idschool=null,$idschool_year){
	global $DB;
	// $sql="SELECT * FROM `semester` WHERE `semester`.`del`=0 AND `semester`.`tonghocki_1`< 50 AND `semester`.`school_yearid`={$idschool_year}";
	$sql="SELECT DISTINCT `semester`.`id`,  `semester`.`tonghocki_1`, `user`.`firstname`, `user`.`lastname`,`user`.`birthday`, `user`.`code`, `user`.`name_parent`, `user`.`phone1`, `user`.`email`, `user`.`description`, `course`.`fullname`, `course`.`id` as courseid , `schools`.`name` as schoolname ,`history_course_name`.`course_name`
	FROM `semester` 
	JOIN `user` ON `user`.`id`= `semester`.`userid` 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN `course` ON `semester`.`courseid`=`course`.`id` 
	JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
	JOIN `schools` on `schools`.`id` = `semester`.`schoolid` 
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `semester`.`del`=0 
	AND `semester`.`tonghocki_1`< 50 
	AND `semester`.`school_yearid`={$idschool_year}
	AND `history_course_name`.`school_year_id`={$idschool_year}
	";
	if(!empty($idschool)){
		$sql.=" AND `semester`.`schoolid`={$idschool}";
	}
	$sql.=" ORDER BY `course`.`fullname`, `user`.`firstname` ASC ";
	$data = $DB->get_records_sql($sql);
	return $data;
}


	$data=lay_hoc_sinh_diem_thihk1duoi50($schoolid,$school_yearid); 
	
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/report_semester_1_below_50.xlsx');
	$numRow = 3;
	$i=0;
	foreach ($data as $value) { 
		# code...
		$i++;
		// $truong=hienthi_ten_truong($value->userid);
		// var_dump($school_yearid);die;
		// $book=hien_thi_ten_sach_cua_nhom3($value->$courseid,$school_yearid); 
		$book1=$DB->get_record('schedule',array(
			'courseid'=>$value->courseid,
			'school_yearid'=>$school_yearid,
			'del'=>0
		));
		
		$book="";
		if(!empty($book1->book)){$book=$book1->book;} 
		$name = $value->lastname.' '.$value->firstname;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $value->code);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $name);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $value->birthday);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $value->schoolname);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $value->course_name);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $book);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $value->tonghocki_1);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow,	$value->name_parent);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow,	$value->phone1);
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $value->email);
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $value->description);
		// $objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $value->description);
		$numRow ++;
	}

	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-DSHS-dien-HKI-duoi-50.xlsx"');
	$objWriter->save('php://output');
	exit;
	
?>