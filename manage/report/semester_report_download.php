<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
// require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
require_once($CFG->dirroot . '/manage/report/in.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
// xóa file trên serve
function hien_thi_ten_sach_cua_nhom($idcourse,$idschool_year){
	global $DB;
	 $sql = "SELECT DISTINCT `schedule`.`book` 
    FROM `schedule` 
    WHERE `schedule`.`courseid`={$idcourse}
    AND `schedule`.`school_yearid`={$idschool_year}
    AND `schedule`.`del`=0
    ";
    $data = $DB->get_field_sql($sql,NULL,$strictness=IGNORE_MISSING);
    return $data;

}
//tieu hoc
function lay_danh_sach_xuatbycua($school_yearid,$schoolid,$courseid){
	global $DB;
	$sql=" 
		SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname` 
		FROM `semester`
		JOIN `user` ON `semester`.`userid`= `user`.`id` 
		JOIN `course` ON `course`.`id` = `semester`.`courseid`
		WHERE  `user`.`del`=0
		AND `semester`.`del`=0
		AND `semester`.`school`=1 
		AND `semester`.`school_yearid` ={$school_yearid}
	";
	// if(!empty($school_yearid)){
	// 	$sql.=" AND `semester`.`school_yearid` ={$school_yearid}";
	// }
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid` ={$schoolid}";
	}
	if(!empty($courseid)){
		$sql.=" AND `semester`.`courseid` ={$courseid}";
	}
	$sql.=" ORDER BY  `user`.`firstname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
//trung hoc co so
function lay_danh_sach_xuatbycua2($school_yearid,$schoolid,$courseid){
	global $DB;
	$sql=" 
		SELECT `semester`.*, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname` 
		FROM `semester`
		JOIN `user` ON `semester`.`userid`= `user`.`id` 
		JOIN `course` ON `course`.`id` = `semester`.`courseid`
		WHERE  `user`.`del`=0
		AND `semester`.`del`=0 
		AND `semester`.`school`=2
		AND `semester`.`school_yearid` ={$school_yearid}
	";
	// if(!empty($school_yearid)){
	// 	$sql.=" AND `semester`.`school_yearid` ={$school_yearid}";
	// }
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid` ={$schoolid}";
	}
	if(!empty($courseid)){
		$sql.=" AND `semester`.`courseid` ={$courseid}";
	}
	$sql.=" ORDER BY  `user`.`firstname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function show_tongket_hsth($diem){
	$xeploai="Chờ kết quả thi";
	if($diem>=84.5){
		$xeploai= 'Giỏi'; 
	}
	if(($diem>=64.5)&&($diem<84.5)){
		$xeploai='Khá'; 
	}
	if(($diem>=49.5)&&($diem<64.5)){
		$xeploai= 'TB'; 
	}
	if($diem<49.5){
		$xeploai= 'Dưới TB'; 
	}
	return $xeploai;
}
function show_tongke_hsthpt($diem){
	$xeploai="Chờ kết quả thi";
	if($diem>=79.5){
		$xeploai='Giỏi'; 
	}
	if(($diem>=64.5)&&($diem<79.5)){
		$xeploai= 'Khá'; 
	}
	if(($diem>=49.5)&&($diem<64.5)){
		$xeploai= 'TB'; 
	}
	if($diem<49.5){
		$xeploai= 'Dưới TB'; 
	}
	return $xeploai;

}
function show_show_yeah_in_report($id){
  global $DB;
  $yeah=$DB->get_record('school_year',array('id'=>$id));
  $cua= $yeah->sy_start.'-'.$yeah->sy_end;
  return  $cua;
}
$school=$DB->get_record('schools', array(
  'id' => $schoolid
));
$course=$DB->get_record('course', array(
  'id' => $courseid
));
$history_course_name=$DB->get_record('history_course_name', array(
  'school_year_id' => $school_yearid,
  'courseid' => $courseid,
  'groupid' => $groupid,
));
// $book =hien_thi_ten_sach_cua_nhom($courseid,$school_yearid);
$schedule=$DB->get_record('schedule', array(
  'courseid' => $courseid,
  'school_yearid' => $school_yearid,
  'del'=>0
));
// $course_and_book=$course->fullname;
$course_and_book=$history_course_name->course_name;
if($schedule->book){
	$course_and_book=$course_and_book." & ".$schedule->book;
}
$namehoc=show_show_yeah_in_report($school_yearid);
$teacher=ten_giao_vien_xuat_bao_cao_theo_nhom($courseid);
$title='BÁO CÁO KẾT QUẢ HỌC TẬP NĂM HỌC '.$namehoc;
$day_report=date('d-M-y',time());
$text2='SPREADSHEET '.$nam;
$text3='BẢNG ĐIỂM '.$nam;
	if($schoolstatus==1){
		$data=lay_danh_sach_xuatbycua($school_yearid,$schoolid,$courseid); 
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/semester_report_tieuhoc.xlsx');
		$objPHPExcel->getActiveSheet()->SetCellValue('A4', $text3);
		$objPHPExcel->getActiveSheet()->SetCellValue('A5', $text2);
		$numRow = 7; 
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title) ;
		$objPHPExcel->getActiveSheet()->SetCellValue('A6' , $school->name);
		$objPHPExcel->getActiveSheet()->SetCellValue('A8' , $course_and_book);
		$objPHPExcel->getActiveSheet()->SetCellValue('A10' , $day_report);
		$objPHPExcel->getActiveSheet()->SetCellValue('A12' , $school->name);
		$objPHPExcel->getActiveSheet()->SetCellValue('A14' , $teacher);
		// $numRow = 7;
		$i=0;
		foreach ($data as $key => $row) { 
			$i++;
			$name = $row->lastname.' '.$row->firstname;
			if($row->noi_2 >0) $tong=$row->tongket; else $tong='';
			if($tong>0){$xeploai=show_tongket_hsth($tong);}else{$xeploai='Chờ kết quả thi';}
			// $objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $row1);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $i);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->code);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $name);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->s1_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->s2_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->s3_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->s4_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->s5_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->nghe_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->doc_viet_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->noi_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $row->tonghocki_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $row->s1_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, $row->s2_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('P' . $numRow, $row->s3_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $numRow, $row->s4_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('R' . $numRow, $row->s5_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('S' . $numRow, $row->nghe_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('T' . $numRow, $row->doc_viet_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('U' . $numRow, $row->noi_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('V' . $numRow, $row->tonghocki_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('W' . $numRow, $tong);
			$objPHPExcel->getActiveSheet()->SetCellValue('X' . $numRow, $xeploai);
		
			$numRow++;
			# code...
		}
		// $objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Bao-cao-hoc-tap-nam-hoc.xlsx"');
		$objWriter->save('php://output');
		exit;

	}elseif($schoolstatus==2){
		$data=lay_danh_sach_xuatbycua2($school_yearid,$schoolid,$courseid);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/semester_report_trunghoc.xlsx');
		$nam=show_show_yeah_in_report($school_yearid);
		$text2='SPREADSHEET '.$nam;
		$objPHPExcel->getActiveSheet()->SetCellValue('A5', $text2);
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title) ;
		$objPHPExcel->getActiveSheet()->SetCellValue('A7' , $course_and_book);
		$objPHPExcel->getActiveSheet()->SetCellValue('A9' , $day_report);
		$objPHPExcel->getActiveSheet()->SetCellValue('A11' , $school->name);
		$objPHPExcel->getActiveSheet()->SetCellValue('A13' , $teacher);
		$numRow = 7;
		$i=0;
		foreach ($data as $key => $row) {
			$i++;
			$name = $row->lastname.' '.$row->firstname;
			if($row->noi_2 >0) $tong=$row->tongket; else $tong='';
			if($tong>0){$xeploai=show_tongke_hsthpt($tong);}else{$xeploai='Chờ kết quả thi';}
			// $objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $row1);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $i);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->code);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $name);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->s1_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->s2_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->s3_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->s4_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->s5_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->viet_luan_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->nghe_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->tv_np_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $row->doc_hieu_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $row->tvnp_dochieu_viet_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, $row->noi_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('P' . $numRow, $row->tonghocki_1);
			$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $numRow, $row->s1_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('R' . $numRow, $row->s2_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('S' . $numRow,	$row->s3_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('T' . $numRow,	$row->s4_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('U' . $numRow, $row->s5_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('V' . $numRow, $row->viet_luan_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('W' . $numRow, $row->nghe_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('X' . $numRow, $row->tv_np_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('Y' . $numRow, $row->doc_hieu_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('Z' . $numRow, $row->tvnp_dochieu_viet_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('AA' . $numRow,	$row->noi_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('AB' . $numRow,$row->tonghocki_2);
			$objPHPExcel->getActiveSheet()->SetCellValue('AC' . $numRow,$tong);
			$objPHPExcel->getActiveSheet()->SetCellValue('AD' . $numRow,$xeploai);
			$numRow++;
			# code...
		}
		
		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Bao-cao-hoc-tap-nam-hoc.xlsx"');
		$objWriter->save('php://output');
		exit;
	}
	
?>