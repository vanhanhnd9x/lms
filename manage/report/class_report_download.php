
<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$idnamhocnow=get_id_school_year_now(); var_dump($idnamhocnow);
function search_class_to_export_by_cua($schoolid=null,$block_student=null,$groupid=null,$idnamhoc=null){
	global $CFG,$DB,$USER;
	$sql="SELECT `groups`.* FROM `groups` WHERE `groups`.`id_namhoc`={$idnamhoc}";
	if(!empty($schoolid)&&empty($block_student)&&empty($groupid)){
		$sql.=" AND `groups`.`id_truong`={$schoolid}";
	}elseif(!empty($schoolid)&&!empty($block_student)&&empty($groupid)){
		$sql.=" AND `groups`.`id_khoi`={$block_student}";
	}elseif(!empty($schoolid)&&!empty($block_student)&&!empty($groupid)){
		$sql.=" AND `groups`.`id`={$groupid}";
	}
	$sql.=" ORDER BY `groups`.`name` ASC";
	$data=$DB->get_records_sql($sql);
	return $data;
}

function number_student_in_class_ex_by_cua($idgroup){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `groups_members`.`userid`) 
	FROM `groups_members`  
	JOIN user ON `user`.id = `groups_members`.`userid`
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid`
	WHERE `groups_members`.`groupid` = {$idgroup}
	AND  `role_assignments`.`roleid`= 5 AND `user`.`del`=0 ";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function number_course_in_class_ex_by_cua($idgroup){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `group_course`.`id`) FROM `group_course`  
	WHERE `group_course`.`group_id` = {$idgroup}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
// khi xuat ds lop hoc theo nam hoc
function lay_lop_theo_nam_hoc_cuatruong($schoolid,$school_yearid){
	global $CFG,$DB;
	$sql="SELECT `groups`.* FROM `groups` 
	JOIN groups_year on `groups_year`.`groupid`=`groups`.`id` 
	WHERE `groups`.`id_truong`={$schoolid} 
	AND `groups_year`.`schoolyearid`={$school_yearid} 
	ORDER BY `groups`.`name` ASC";
	$data=$DB->get_records_sql($sql);
	return $data;
}

function so_hs_cua_lop_theo_nam_hoc($idgroup,$school_yearid,$schoolid){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `history_student`.`iduser`) 
	FROM `history_student`
	WHERE `history_student`.`idschool`={$schoolid}
	And `history_student`.`idclass`={$idgroup}
	and `history_student`.`idschoolyear`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function so_nhom_cua_lop_theo_nam_hoc($idgroup,$school_yearid,$schoolid){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `history_student`.`idgroups`) 
	FROM `history_student`
	WHERE `history_student`.`idschool`={$schoolid}
	And `history_student`.`idclass`={$idgroup}
	and `history_student`.`idschoolyear`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function so_nhom_cua_lop_theo_nam_hoc2($idgroup,$school_yearid,$schoolid){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `history_student`.`iduser`) 
	FROM `history_student`
	WHERE `history_student`.`idschool`={$schoolid}
	And `history_student`.`idclass`={$idgroup}
	and `history_student`.`idschoolyear`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function show_show_yeah_in_report($id){
  global $DB;
  $yeah=$DB->get_record('school_year',array('id'=>$id));
  $cua= $yeah->sy_start.'-'.$yeah->sy_end;
  return  $cua;
}

$school=$DB->get_record('schools', array(
	'id' => $schoolid
));

$nam=show_show_yeah_in_report($school_yearid);

$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);

$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/class_report.xlsx');
$numRow = 3;
$tonghs=0;
$tongnhom=0; 
	if(!empty($school_yearid)){
		$title="DANH SÁCH CÁC LỚP - TRƯỜNG ".$school->name."
				NĂM HỌC ".$nam;							
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
		$data=lay_lop_theo_nam_hoc_cuatruong($schoolid,$school_yearid);
		$n=count($data);
		$i=0;
		foreach ($data as $row) {
			
			$i++;
			// $hs=so_hs_cua_lop_theo_nam_hoc($row->id,$school_yearid,$schoolid);
			$hs=so_nhom_cua_lop_theo_nam_hoc2($row->id,$school_yearid,$schoolid);
			$nhom=so_nhom_cua_lop_theo_nam_hoc($row->id,$school_yearid,$schoolid);
			$tonghs=$tonghs+$hs;
			$tongnhom=$tongnhom+$nhom;
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $hs);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $nhom);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->gvcn);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->phone);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->email);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->description);
			$numRow++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

	}else{
		$title="DANH SÁCH CÁC LỚP - TRƯỜNG ".$school->name;							
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
		$data=search_class_to_export_by_cua($schoolid,$block_student,$groupid,$idnamhocnow);
		$n=count($data);
		$i=0;
		foreach ($data as $row) {
			$i++;
			$hs=number_student_in_class_ex_by_cua($row->id);
			$nhom=number_course_in_class_ex_by_cua($row->id);
			$tonghs=$tonghs+$hs;
			$tongnhom=$tongnhom+$nhom;
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $hs);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $nhom);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->gvcn);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->phone);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->email);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->description);
			$numRow++;
		}
		$objPHPExcel->setActiveSheetIndex(0);
	}
	
	// tinh tong
	if(!empty($data)){
		$sodem=$i+3;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $sodem, 'Tổng');
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $sodem, $n);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $sodem, $tonghs);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $sodem, $tongnhom);
	}
	

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-lop-hoc.xlsx"');
	$objWriter->save('php://output');
	exit;