<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
function lay_thong_tin_xuat_file_hsxs($schoolid,$school_yearid){
	global $DB;
	$truong=$DB->get_record('schools', array(
        'id' =>$schoolid
	    ),
	    $fields='id,name,level', $strictness=IGNORE_MISSING
	);
	$sql="
	SELECT `semester`.`id`, `semester`.`tongket`, `user`.`lastname`, `user`.`firstname`, `user`.`code`,`course`.`fullname`,`history_course_name`.`course_name`
	FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
	WHERE  `user`.`del`=0
	AND `semester`.`del`=0 
	
	";
	if($truong->level==1){
		$sql.=" AND `semester`.`tongket` >85";
	}
	if($truong->level==2){
		$sql.=" AND `semester`.`tongket` >80";
	}
	if(!empty($school_yearid)){
		$sql.=" AND `semester`.`school_yearid`={$school_yearid}";
	}
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid`={$schoolid}";
	}
	$sql.=" ORDER BY   `user`.`firstname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function dem_so_hoc_cua_truong_hoc($schoolid){
	global $DB;
	$sql="SELECT COUNT( DISTINCT `user`.`id`) as total 
	FROM user 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN groups_members ON `user`.id = `groups_members`.`userid` 
	JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid` 
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `groups`.`id_truong`={$schoolid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hoc_cua_truong_hoc2($schoolid,$school_yearid){
	global $DB;
	// $sql="SELECT COUNT( DISTINCT `user`.`id`) as total
	// FROM user 
	// JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	// JOIN groups_members ON `user`.id = `groups_members`.`userid` 
	// JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid` 
	// JOIN `history_student` ON `history_student`.`iduser`= `user`.`id` 
	// WHERE `role_assignments`.`roleid`= 5 
	// AND `user`.`del`=0 
	// AND `groups`.`id_truong`={$schoolid} 
	// AND `history_student`.`idschoolyear`={$school_yearid}";
	$sql="SELECT COUNT( DISTINCT `user`.`id`) as total
	FROM user 
	
	JOIN `history_student` ON `history_student`.`iduser`= `user`.`id` 
	WHERE  `user`.`del`=0 
	AND `history_student`.`idschool`={$schoolid} 
	AND `history_student`.`idschoolyear`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_nhom_cua_truong_hoc($schoolid){
	global $DB;
	$sql="SELECT COUNT( DISTINCT `course`.`id`) as total FROM course 
	JOIN group_course ON `group_course`.`course_id` = `course`.`id` 
	JOIN `groups` ON `groups`.`id` = `group_course`.`group_id` 
	WHERE `groups`.`id_truong`={$schoolid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_nhom_cua_truong_hoc2($schoolid,$school_yearid){
	global $DB;
	// $sql="SELECT COUNT( DISTINCT `course`.`id`) as total FROM course JOIN group_course ON `group_course`.`course_id` = `course`.`id` JOIN `groups` ON `groups`.`id` = `group_course`.`group_id` JOIN `groups_year` ON `groups_year`.`groupid` = `groups`.`id` WHERE `groups`.`id_truong`={$schoolid} AND `groups_year`.`schoolyearid`={$school_yearid}";
	$sql="SELECT COUNT( DISTINCT `course`.`id`) as total 
	FROM course 
	JOIN history_course_name ON `history_course_name`.`courseid` = `course`.`id` 
	JOIN `groups` ON `groups`.`id` = `history_course_name`.`groupid` 
	WHERE `groups`.`id_truong`={$schoolid} AND `history_course_name`.`school_year_id`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}

$school=$DB->get_record('schools', array(
  'id' => $schoolid
));
$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
// $tonghs=dem_so_hoc_cua_truong_hoc($schoolid);
// $tongnhom=dem_so_nhom_cua_truong_hoc($schoolid);
$tongnhom=dem_so_nhom_cua_truong_hoc2($schoolid,$school_yearid);
$tonghs=dem_so_hoc_cua_truong_hoc2($schoolid,$school_yearid); 
$data=lay_thong_tin_xuat_file_hsxs($schoolid,$school_yearid);
	$title='DANH SÁCH NHẬN PHẦN THƯỞNG HỌC SINH XUẤT SẮC NĂM HỌC '.$namhoc->sy_start.'-'.$namhoc->sy_end;

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/report_outstanding_students.xlsx');
	$numRow = 7;
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', $title);
	$objPHPExcel->getActiveSheet()->SetCellValue('C3', $school->name);
	$objPHPExcel->getActiveSheet()->SetCellValue('C4', $tongnhom);
	$objPHPExcel->getActiveSheet()->SetCellValue('C5', $tonghs);
	
	$i=0;
	foreach ($data as $key => $row) {
		# code...
		$i++;
		$name = $row->lastname.' '.$row->firstname;
		// $coursename=$row->coefficient_name.''.$row->fullname;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->course_name);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $name);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->tongket);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, '');
		$numRow++;
	}
	
	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-hs-xuat-sac.xlsx"');
	$objWriter->save('php://output');
	exit;
?>