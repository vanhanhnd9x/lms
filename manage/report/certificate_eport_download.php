<?php 
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
require_once($CFG->dirroot . '/manage/report/lib.php'); 
require_once($CFG->dirroot . '/manage/schools/lib.php'); 

$namhc = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$action = optional_param('type', 'gg', PARAM_TEXT);

function baocaodshsnhanchungchi($schoolid,$school_yearid){
	global $DB;
	$time=time();
	$sql=" 
		SELECT  `user`.`id`,  `user`.`firstname`, `user`.`lastname`, `user`.`code`,`history_course_name`.`course_name`,`history_course_name`.`courseid` 
		FROM `user` 
		JOIN `history_student` ON `history_student`.`iduser`= `user`.`id`
		JOIN `course` ON `history_student`.`idgroups`=`course`.`id`
		JOIN `history_course_name` ON `course`.`id` =`history_course_name`.`courseid`
		WHERE `user`.`del` =0
        AND `history_student`.`idschool`={$schoolid} 
        AND `history_student`.`idschoolyear`={$school_yearid} 
        AND `history_course_name`.`school_year_id`={$school_yearid}
        AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)
		ORDER By  `history_course_name`.`course_name`, `user`.`firstname` ASC
 
	";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function check_dk_diemhk($userid,$school_year){
	global $DB;
	$sql=" SELECT `semester`.`tongket` FROM `semester` 
	WHERE `semester`.`del`=0 
	AND `semester`.`userid`={$userid}
	AND `semester`.`school_yearid`={$school_year} 
	AND `semester`.`tonghocki_2` >0 AND `semester`.`tongket` < 65 ";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}

function laytongtiethoccuanhom_1($courseid,$school_yearid){
	global $DB;
	$sql=" SELECT `history_course_name`.`sotiet` FROM `history_course_name` 
	WHERE `history_course_name`.`courseid`={$courseid}
	AND `history_course_name`.`school_year_id`={$school_yearid}
	";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function tongngaynghicuahs($userid,$courseid,$school_yearid){
	global $DB;
	$sql="SELECT COUNT( DISTINCT `diemdanh`.`id`)  FROM `diemdanh` WHERE `diemdanh`.`userid`={$userid} AND `diemdanh`.`course_id`={$courseid} AND `diemdanh`.`schoolyearid`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
$title='DANH SÁCH HỌC SINH TRƯỜNG '. $name_school->name.' NĂM HỌC '.$namhoc->sy_start.'-'.$namhoc->sy_end;
$dulieu=baocaodshsnhanchungchi($schoolid,$namhc); 
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/report_certificate.xlsx');
	$name_school = get_info_school_with_block($schoolid);
	$objPHPExcel->getActiveSheet()->SetCellValue('A2',$title); 
		$row=4;
		foreach ($dulieu as $key => $hs) {
			$kq_out='';
			$gt = get_giaotrinh($hs->courseid,$namhc);
			// $kq = (($hs->tonghocki_1*0.25)+($hs->tonghocki_2*0.75));
			// $kq = round($kq, 2);
			// $cc = (1-get_ngaynghi($hs->userid)/get_tongsotiet($hs->courseid))*100;
			// if (!$kq<60) {
			// 	$kq_out=$kq;
			// }
			// if (true) {
				$chuyencan='';
				$showhk='';
				$hk=check_dk_diemhk($hs->id,$namhc);
				if(!empty($hk)){
					$showhk=$hk;
				}
				$tongtiet=laytongtiethoccuanhom_1($hs->courseid,$namhc); 
				$songaynghi=tongngaynghicuahs($hs->id,$hs->courseid,$namhc);
				$kq=($tongtiet-$songaynghi)/$tongtiet *100;
				if(($kq<75)&&($kq>0)) $chuyencan=$kq;

				$info = $hs->lastname.' '.$hs->firstname;
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $row-3);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $info);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $hs->course_name); 
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $gt->book);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $hs->code); 
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $chuyencan);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $showhk);
				$row++;
			// }
		}
			$objPHPExcel->setActiveSheetIndex(0);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			// ob_end_clean();
			// We'll be outputting an excel file
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="danh_sach_hoc_sinh_nhan_chung_chi.xlsx"');
			$objWriter->save('php://output');
			exit;
// if ($action == 'total') {
// 	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
// 	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/report_certificate_total.xlsx');
// 	$ds = get_truong_hs($schoolid, $namhc);
// 		$objPHPExcel = new PHPExcel(); 
// 				$objPHPExcel->getActiveSheet()->setTitle('Báo cáo tổng số chứng chỉ');
// 				$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STT');
// 				$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Mã học sinh');
// 				$objPHPExcel->getActiveSheet()->SetCellValue('C1','Tên học sinh'); 
// 				$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Ngày sinh');
// 				$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Địa chỉ'); 
// 				$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Trường'); 
// 				$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Nhóm lớp'); 
// 			$row=2;
// 			foreach ($ds as $key => $hs) {
// 				if (true) {
					
// 					$info = get_user_from_id($hs->id);
// 					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $row-1);
// 					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $info->code);
// 					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $info->lastname.' '.$info->firstname); 
// 					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $info->birthday);
// 					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $info->address); 
// 					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, get_schools($schoolid)->name); 
// 					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, get_name_course(get_nhomlop_user($hs->id)));
// 					echo $row;
// 					$row++;
// 				}
// 			}
// 				$objPHPExcel->setActiveSheetIndex(0);
// 				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 				// ob_end_clean();
// 				// We'll be outputting an excel file
// 				header('Content-type: application/vnd.ms-excel');
// 				header('Content-Disposition: attachment; filename="Tong_chung_chi.xlsx"');
// 				$objWriter->save('php://output');
// }else{

// 	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
// 	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/report_certificate.xlsx');

// 	$name_school = get_info_school_with_block($schoolid);
	
// 	$objPHPExcel->getActiveSheet()->SetCellValue('A2','DANH SÁCH HỌC SINH TRƯỜNG '. $name_school->name); 

// 	$ds = export_dshs($namhc, $schoolid);
		
// 		$row=4;
// 		foreach ($ds as $key => $hs) {
// 			$kq_out='';
// 			$gt = get_giaotrinh($hs->courseid,$namhc);
// 			$kq = (($hs->tonghocki_1*0.25)+($hs->tonghocki_2*0.75));
// 			$kq = round($kq, 2);
// 			$cc = (1-get_ngaynghi($hs->userid)/get_tongsotiet($hs->courseid))*100;
// 			if (!$kq<60) {
// 				$kq_out=$kq;
// 			}
// 			if (true) {
				
// 				$info = get_user_from_id($hs->userid);
// 				$course_name = get_name_course($hs->courseid);
// 				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $row-3);
// 				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $info->lastname.' '.$info->firstname);
// 				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $course_name->coefficient_name.$course_name->fullname); 
// 				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $gt->book);
// 				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $info->code); 
// 				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, pout($cc, 75));
// 				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, pout($kq, 60));
// 				$row++;
// 			}
// 		}
// 			$objPHPExcel->setActiveSheetIndex(0);
// 			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 			// ob_end_clean();
// 			// We'll be outputting an excel file
// 			header('Content-type: application/vnd.ms-excel');
// 			header('Content-Disposition: attachment; filename="danh_sach_hoc_sinh_nhan_chung_chi.xlsx"');
// 			$objWriter->save('php://output');
// }

 ?>