<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);

function lay_lich_hoc_de_xuatfile($school_yearid=null,$schoolid=null){
	global $DB;
	$sql="SELECT 
	`schedule`.`id`,
	`schedule`.`book`,
	`schedule`.`courseid`,
	`schedule`.`id`,
	`course`.`fullname`, 
	`groups`.`name` AS groupname,
	`schools`.`name`,
	`schedule`.`school_yearid`,
	`history_course_name`.`course_name`
	FROM `schedule` 
	JOIN `groups` ON `groups`.`id` =`schedule`.`groupid` 
	JOIN `schools` ON `groups`.`id_truong` = `schools`.`id` 
	JOIN `course` ON `schedule`.`courseid` = `course`.`id` 
	JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
	JOIN `block_student` ON `groups`.`id_khoi` = `block_student`.`id`
	WHERE `schedule`.`del`=0 AND `block_student`.`del` = 0 
	 ";
	 if(!empty($school_yearid)){
	 	$sql.=" AND `schedule`.`school_yearid`= {$school_yearid}";
	 }
	 if(!empty($schoolid)){
	 	$sql.=" AND `groups`.`id_truong`= {$schoolid}";
	 }
	$sql.=" ORDER BY `schools`.`name`,`history_course_name`.`course_name` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;

}
// lay so hoc sinh trong nhom 
function lay_tong_so_hs_trong_nhom($idnhom){
	global $DB;
	$sql=" SELECT COUNT(DISTINCT `user`.`id`)
	FROM `user` 
	JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
	JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid`
	WHERE `role_assignments`.`roleid`= 5 
	AND `user`.`del`=0 
	AND `course`.`id`= {$idnhom}";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function lay_tong_so_hs_trong_nhom2($idnhom,$school_yearid){
	global $DB;
	// $sql=" SELECT COUNT(DISTINCT `user`.`id`)
	// FROM `user` 
	// JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	// JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
	// JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
	// JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid`
 //    JOIN `history_student` ON `history_student`.`iduser` =`user`.`id`
	// WHERE `role_assignments`.`roleid`= 5 
	// AND `user`.`del`=0 
	// AND `history_student`.`idgroups`={$idnhom}
	// AND `history_student`.`idschoolyear`={$school_yearid}
	// ";
	$sql=" SELECT COUNT(DISTINCT `user`.`id`)
	FROM `user` 
    JOIN `history_student` ON `history_student`.`iduser` =`user`.`id`
	WHERE  `user`.`del`= 0
	AND `history_student`.`idgroups`={$idnhom}
	AND `history_student`.`idschoolyear`={$school_yearid}
	";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function show_gvnn_gvtg_in_course($idcourse){
	global $DB;
	$course=$DB->get_record('course', array(
	  'id' => $idcourse
	));
	$sql_idteacher=" SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` ={$idcourse} AND `role_assignments`.`roleid`= 8";
	$idgvnn=$DB->get_field_sql($sql_idteacher,null,$strictness=IGNORE_MISSING);
	$gvnn=$DB->get_record('user', array(
	  'id' => $idgvnn
	));
	$namegv=$gvnn->lastname.' '.$gvnn->firstname;
	
	$sql_idta=" SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` ={$idcourse} AND `role_assignments`.`roleid`= 10";
	$idgvtg=$DB->get_field_sql($sql_idta,null,$strictness=IGNORE_MISSING);
	$gvtg=$DB->get_record('user', array(
	  'id' => $idgvtg
	));
	$namegvtg=$gvtg->lastname.' '.$gvtg->firstname;
	$kq=array(
		'gvnn'=>$namegv,
		'sdtgvnn'=>$gvnn->phone2,
		'emailgvnn'=>$gvnn->email,
		'gvtg'=>$namegvtg,
		'sdtgvtg'=>$gvtg->phone2,
		'emailgvtg'=>$gvtg->email,
	);
	return $kq;
}
function thoi_gian_hoc_trong_thoi_khoa_bieu($idscheduke){
	global $DB;
	$kq=array();
	$location=array();
	$sql="SELECT `schedule_info`.* 
	FROM `schedule_info` 
	JOIN `schedule` ON `schedule`.`id`= `schedule_info`.`scheduleid`
	WHERE `schedule_info`.`scheduleid`={$idscheduke}
	AND `schedule`.`del`=0
	ORDER BY `schedule_info`.`day` ASC";
	$data=$DB->get_records_sql($sql);
	foreach ($data as $key => $value) {
		# code...
		// $cua= 'Thứ '.$value->day.', Phòng: '.$value->location;
		array_push($location,$value->location);
		$time=$value->time_start.' - '.$value->time_end;
		$kq[$value->day]= $time;
	}
	$location=implode(',', $location);
	$return=array(
		'time'=>$kq,
		'location'=>$location
	);
	return $return;
}
function show_name_course_schedule_report($courseid,$school_yearid){
  global $CFG, $DB;
    $sql = "SELECT DISTINCT `history_course_name`.`course_name` FROM `history_course_name` JOIN `course` ON `course`.`id`=`history_course_name`.`courseid` WHERE `history_course_name`.`courseid`={$courseid} AND `history_course_name`.`school_year_id`={$school_yearid} ";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
	$data=lay_lich_hoc_de_xuatfile($school_yearid,$schoolid);
	
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/schedule_report.xlsx');
	$numRow = 5;
	$i=0;
	foreach ($data as $key => $row) {
		# code...
		$i++;
		// $hs=lay_tong_so_hs_trong_nhom($row->courseid);
		$hs=lay_tong_so_hs_trong_nhom2($row->courseid,$school_yearid);
		$kq=show_gvnn_gvtg_in_course($row->courseid);
		$time=thoi_gian_hoc_trong_thoi_khoa_bieu($row->id); 
		// $coursename=show_name_course_schedule_report($row->courseid,$school_yearid);
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->name);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->groupname);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->course_name);

		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $time['time'][2]);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $time['time'][3]);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $time['time'][4]);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $time['time'][5]);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $time['time'][6]);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $time['time'][7]);

		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $time['location']);
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->book);
		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $hs);


		$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $kq['gvnn']);
		$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, $kq['sdtgvnn']);
		$objPHPExcel->getActiveSheet()->SetCellValue('P' . $numRow, $kq['emailgvnn']);
		$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $numRow, $kq['gvtg']);
		$objPHPExcel->getActiveSheet()->SetCellValue('R' . $numRow, $kq['sdtgvtg']);
		$objPHPExcel->getActiveSheet()->SetCellValue('S' . $numRow, $kq['emailgvtg']);
		$numRow++;
	}
	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-lich-hoc.xlsx"');
	$objWriter->save('php://output');
	exit;
?>