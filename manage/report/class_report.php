<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title(get_string('report_class'));
$PAGE->set_heading(get_string('report_class'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT); 
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='report';
$name1='report_class';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
<!-- <div class="col-md-2">
<a href="<?php echo new moodle_url('/manage/student/add_student.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
</div> -->
<div class="col-md-12 pull-right">
	<form action="<?php echo $CFG->wwwroot ?>/manage/report/class_report_download.php " method="get" accept-charset="utf-8" class="form-row">
		<div class="col-4">
			<label for=""><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
			<select name="schoolid" id="schoolid" class="form-control" required>
				<option value=""><?php echo get_string('schools'); ?></option>
				<?php 
				$schools = get_all_shool();
				foreach ($schools as $key => $value) {
					?>
					<option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
					<?php 
				}
				?>
			</select>
		</div>
    <div class="col-4">
          <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
          <select name="school_yearid" id="" class="form-control" >
            <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
            <?php 
              $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
              $school_year = $DB->get_records_sql($sql);
              foreach ($school_year as $key => $value) {
                 # code...
                ?>
              <option value="<?php echo $value->id ?>" <?php if($school_yearid==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
                <?php 
              }
             ?>
         </select>
      </div>
		<!-- <div class="col-4">
			<label for=""><?php echo get_string('block_student'); ?></label>
			<select name="block_student" id="block_student" class="form-control">
				<option value=""><?php echo get_string('block_student'); ?></option>
			</select>
		</div> -->
		<!-- <div class="col-4">
			<label for=""><?php echo get_string('class'); ?></label>
			<select name="groupid" id="groupid" class="form-control">
				<option value=""><?php echo get_string('class'); ?></option>
			</select>
		</div> -->

		<div class="col-4">
			<div style="    margin-top: 29px;"></div>
			<button type="submit" class="btn btn-success"><?php echo get_string('export_excel'); ?></button>
		</div>
	</form>
</div>
</div>
<div class="table-responsive" data-pattern="priority-columns">

</div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
?>

<script>

 // ajax edit
 var school = $('#schoolid').find(":selected").val();
 if (school) {
 	var blockedit = "<?php echo $block_student; ?>";
 	$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
 		$("#block_student").html(data);
 	});
 	if(blockedit){
 		var groupedit="<?php echo $groupid; ?>";
      // var courseedit = "<?php echo $course->id; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
      	$("#groupid").html(data);
      });
  }
  if(groupedit){
  	var courseedit="<?php echo $idcourse; ?>";
  	if(courseedit){
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
  			$("#courseid").html(data);
  		});
  	}else{
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
  			$("#courseid").html(data);
  		});
  	}
  }
}
  // ajax add
  $('#schoolid').on('change', function() {
  	var cua = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
  			$("#block_student").html(data);
  		});
  	}
  });
  $('#block_student').on('change', function() {
  	var block = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
  			$("#groupid").html(data);
  		});
  	}
  });
  $('#groupid').on('change', function() {
  	var groupid = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
  			$("#courseid").html(data);
  		});
  	}
  });
</script>