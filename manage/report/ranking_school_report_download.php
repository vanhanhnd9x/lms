<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
// $schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$semester = optional_param('semester', '', PARAM_TEXT);


function lay_danhsachtruong_xuatbaocao($status=null){
	global $DB;
	$sql="SELECT DISTINCT `schools`.`id`, `schools`.`name`, `quanhuyen`.`Name` AS quan
	FROM `schools`
	JOIN `quanhuyen` ON `schools`.`district` =`quanhuyen`.`id`
	WHERE `schools`.`del`=0 ";
	if(!empty($status)){
		$sql.=" AND `schools`.`level`={$status}";
	}
	$sql.=" ORDER BY `quanhuyen`.`Name`, `schools`.`name` ASC";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function dem_hocsinh($schoolid){
	global $DB;
	$sql=" SELECT COUNT( DISTINCT `groups_members`.`userid`) 
	FROM `groups_members`
	JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid` 
	JOIN `user` ON `user`.`id` = `groups_members`.`userid` 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid`
	WHERE `groups`.`id_truong` ={$schoolid}
	AND `role_assignments`.`roleid`= 5 
	AND `user`.`del`=0";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;

}
function dem_hocsinh2($schoolid,$school_yearid){
	global $DB;
	$sql=" SELECT COUNT( DISTINCT `user`.`id`) 
	FROM `groups_members` 
	JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid` 
	JOIN `user` ON `user`.`id` = `groups_members`.`userid` 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN `history_student` ON `history_student`.`iduser` = `user`.`id` 
	WHERE `groups`.`id_truong` ={$schoolid} 
	AND `role_assignments`.`roleid`= 5 
	AND `user`.`del`=0 
	AND `history_student`.`idschoolyear`={$school_yearid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;

}
function dem_hs_kha($schoolid,$school_yearid,$semester,$schoolstatus){
	global $DB;
	$sql="SELECT COUNT(DISTINCT `semester`.`userid`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND  `semester`.`school_yearid` = {$school_yearid} 
	AND `semester`.`schoolid` = {$schoolid} 
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
		$x=64.5;
		$y=84.5;
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
		$x=64.5;
		$y=79.5;
	}
	if($semester==1){
		$sql.=" AND `semester`.`tonghocki_1` >= {$x} AND  `semester`.`tonghocki_1`<{$y}";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` >= {$x} AND  `semester`.`tongket`<{$y}";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;

}

function dem_hs_gioi($schoolid,$school_yearid,$semester,$schoolstatus){
	global $DB;
	$sql=" SELECT COUNT(DISTINCT `semester`.`userid`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`schoolid` = {$schoolid} 
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
		$x=84.5;
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
		$x=79.5;
	}
	if($semester==1){
		$sql.=" AND `semester`.`tonghocki_1` >= {$x} ";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` >= {$x}";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_hs_tb($schoolid,$school_yearid,$semester,$schoolstatus){
	global $DB;
	$sql="SELECT COUNT(DISTINCT `semester`.`userid`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0 
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`schoolid` = {$schoolid} 
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
	}
	if($semester==1){
		$sql.=" AND `semester`.`tonghocki_1` >= 49.5  AND `semester`.`tonghocki_1` <64.5 ";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` >= 49.5 AND `semester`.`tongket` <64.5";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_hs_duoitb($schoolid,$school_yearid,$semester,$schoolstatus){
	global $DB;
	$sql="SELECT COUNT(DISTINCT `semester`.`userid`) FROM `semester`
	JOIN `user` ON `semester`.`userid`= `user`.`id` 
	JOIN `course` ON `course`.`id` = `semester`.`courseid`
	WHERE  `user`.`del`=0 
	AND  `semester`.`school_yearid` = {$school_yearid}  
	AND `semester`.`schoolid` = {$schoolid}
	AND `semester`.`del` = 0";
	
	if($schoolstatus==1){
		$sql.=" AND `semester`.`school` =1";
	}
	if($schoolstatus==2){
		$sql.=" AND `semester`.`school` =2";
	}
	if($semester==1){
		$sql.="   AND `semester`.`tonghocki_1` <49.5 ";
	}
	if($semester==2){
		$sql.=" AND `semester`.`s2_1` >0  AND `semester`.`tongket` <49.5 ";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
};
// $cua=dem_hocsinh2(101,17);
	// $data=get_all_shool();
	$data=lay_danhsachtruong_xuatbaocao($schoolstatus); 
	// switch ($schoolstatus) {
	// 	case 1: $data=danhsachtruongtieuhoc();break;
	// 	case 2: $data=danhsachtruongtrunghoc();break;
		
	// }
	// $objPHPExcel = new PHPExcel(); 
	
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/ranking_school_report.xlsx');
	if($schoolstatus==1) {$objPHPExcel->getActiveSheet()->setTitle('Báo cáo Ranking tổng hợp TH');}
	if($schoolstatus==2) {$objPHPExcel->getActiveSheet()->setTitle('Báo cáo Ranking tổng hợp THPT');}
	$numRow = 3; 
	$i=0;
	$tonghs=0;
	$tonghsgioi=0;
	$tonghskha=0;
	$tonghstb=0;
	$tonghsduoitb=0;
	$tonghshoanthanh=0;
	$dem=count($data); 
	foreach ($data as $key => $row) {
		$i++;
		$hs=dem_hocsinh2($row->id,$school_yearid); 
		// $hs=dem_hocsinh($row->id); 
		$hsgioi=dem_hs_gioi($row->id,$school_yearid,$semester,$schoolstatus);
		$hskha=dem_hs_kha($row->id,$school_yearid,$semester,$schoolstatus);
		$hstb=dem_hs_tb($row->id,$school_yearid,$semester,$schoolstatus);
		$hsdtb=dem_hs_duoitb($row->id,$school_yearid,$semester,$schoolstatus);

		$hoanthanh=$hskha +$hstb;
		$tylhoanthanh=($hoanthanh/$hs)*100;

		$tlgioi=($hsgioi/$hs)*100;
		$tlkha=($hskha/$hs)*100;
		$tltb=($hstb/$hs)*100;
		$tlduoitb=($hsdtb/$hs)*100;
		// danh cho tinh tong
		$tonghs=$tonghs+$hs;
		$tonghsgioi=$tonghsgioi+$hsgioi;
		$tonghskha=$tonghskha+$hskha;
		$tonghstb=$tonghstb+$hstb;
		$tonghsduoitb=$tonghsduoitb+$hsdtb;
		$tonghshoanthanh=$tonghshoanthanh+$hoanthanh;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $row->quan);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $row->name);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $hs);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $hsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $hskha);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $hstb);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $hsdtb);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, round($tlgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, round($tlkha));
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, round($tltb));
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, round($tlduoitb));

		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $hsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $hoanthanh);
		$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, $hsdtb);
		$objPHPExcel->getActiveSheet()->SetCellValue('P' . $numRow, round($tlgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $numRow, round($tylhoanthanh));
		$objPHPExcel->getActiveSheet()->SetCellValue('R' . $numRow, round($tlduoitb));
		$numRow++;
	}
	$tongtilehsgioi=($tonghsgioi/$tonghs)*100; 
	$tongtilehskha=($tonghskha/$tonghs)*100;
	$tongtilehstb=($tonghstb/$tonghs)*100;
	$tongtilehsduoitb=($tonghsduoitb/$tonghs)*100;
	$tongtilehshoanthanh=($tonghshoanthanh/$tonghs)*100;

	$cua=$dem+3;
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $cua, 'Tổng');
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $cua, '');
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $cua, '');
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $cua, $tonghs);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $cua, $tonghsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $cua, $tonghskha);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $cua, $tonghstb);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $cua, $tonghsduoitb);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $cua, round($tongtilehsgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $cua, round($tongtilehskha));
		$objPHPExcel->getActiveSheet()->SetCellValue('K' . $cua, round($tongtilehstb));
		$objPHPExcel->getActiveSheet()->SetCellValue('L' . $cua, round($tongtilehsduoitb));

		$objPHPExcel->getActiveSheet()->SetCellValue('M' . $cua, $tonghsgioi);
		$objPHPExcel->getActiveSheet()->SetCellValue('N' . $cua, $tonghshoanthanh);
		$objPHPExcel->getActiveSheet()->SetCellValue('O' . $cua, $tonghsduoitb);
		$objPHPExcel->getActiveSheet()->SetCellValue('P' . $cua, round($tongtilehsgioi));
		$objPHPExcel->getActiveSheet()->SetCellValue('Q' . $cua, round($tongtilehshoanthanh));
		$objPHPExcel->getActiveSheet()->SetCellValue('R' . $cua, round($tongtilehsduoitb));

	$objPHPExcel->setActiveSheetIndex(0);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="BC-ranking-tong-hop.xlsx"');
	$objWriter->save('php://output');
	exit;
	
?>