<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;
$PAGE->set_title(get_string('report_number_student'));
$PAGE->set_heading(get_string('report_number_student'));
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$time_from = optional_param('time_from', '', PARAM_TEXT);
$time_to = optional_param('time_to', '', PARAM_TEXT);


echo $OUTPUT->header();
require_login(0, false);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='report';
$name1='report_number_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
<!-- <div class="col-md-2">
<a href="<?php echo new moodle_url('/manage/student/add_student.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
</div> -->
<div class="col-md-12 pull-right"> 
	<form action="<?php echo new moodle_url('/manage/report/number_student_report_download.php'); ?>" method="get" accept-charset="utf-8" class="form-row">
		
		<!-- <div class="col-md-4">
	       <label class="control-label"><?php echo get_string('day_from'); ?>:</label>
	       <input type="text" name="time_from" id="time_from" class=" datepicker form-control"  placeholder="dd/mm/yyyy" >
    	</div>
    	<div class="col-md-4">
	       <label class="control-label"><?php echo get_string('day_to'); ?>:</label>
	       <input type="text" name="time_to" id="time_to" class=" datepicker form-control"  placeholder="dd/mm/yyyy">
    	</div> -->
      <div class="col-4">
        <label for=""><?php echo get_string('schools'); ?> </label>
        <select name="schoolid" id="schoolid" class="form-control">
          <option value=""><?php echo get_string('schools'); ?></option>
          <?php 
          $schools = get_all_shool();
          foreach ($schools as $key => $value) {
             ?>
            <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
            <?php 
          }
          ?>
        </select>
      </div>
      <div class="col-md-4 form-group">
        <label class="control-label"><?php print_string('day_from');?></label>
          <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="time_from" id="time_from" class="form-control" placeholder="dd/mm/yyyy" value="">
          </div>
      </div>
      <div class="col-md-4 form-group">
        <label class="control-label"><?php print_string('day_to');?></label>
          <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="time_to" id="time_to" class="form-control" placeholder="dd/mm/yyyy" value="">
          </div>
      </div>
		<div class="col-4">
			<div style="    margin-top: 29px;"></div>
			<button type="submit" class="btn btn-success"><?php echo get_string('export_excel'); ?></button>
		</div>
	</form>
</div>
</div>
<div class="table-responsive" data-pattern="priority-columns">

</div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
?>
<script>
  
   $('#time_from').datetimepicker({
    format: 'DD/MM/YYYY'
  }); 
   $('#time_to').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
<script>
// 	var school = $('#schoolid').find(":selected").val();
//  if (school) {
//   var blockedit = "<?php echo $block_student; ?>";
//   $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
//     $("#block_student").html(data);
//   });
//   if(blockedit){
//     var groupedit="<?php echo $groupid; ?>";
//       // var courseedit = "<?php echo $course->id; ?>";
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
//         $("#groupid").html(data);
//       });
//     }
//     if(groupedit){
//       var courseedit="<?php echo $idcourse; ?>";
//       if(courseedit){
//        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
//         $("#courseid").html(data);
//       });
//      }else{
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
//         $("#courseid").html(data);
//       });
//     }
//   }
// }
//   // ajax add
//   $('#schoolid').on('change', function() {
//     var cua = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
//         $("#block_student").html(data);
//       });
//     }
//   });
//   $('#block_student').on('change', function() {
//     var block = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
//         $("#groupid").html(data);
//       });
//     }
//   });
//   $('#groupid').on('change', function() {
//     var groupid = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
//         $("#courseid").html(data);
//       });
//     }
//   });
</script>
<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css'>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script type="text/javascript">
  // $("#time_from").datepicker({
  //       // startDate: "dateToday",
  //       // minDate: 0,
  //       minDate: new Date(25, 10 - 1,2000 ),
  //       maxDate: "",
  //       numberOfMonths: 1,
  //       onSelect: function(selected) {
  //         $("#time_to").datepicker("option","minDate", selected)
  //       },
  //       disableTouchKeyboard: true,
  //       Readonly: true
  //     }).attr("readonly", "readonly");
  // // $("#time_to").datepicker({ 
  //   minDate: 0,
  //   maxDate:"",
  //   numberOfMonths: 1,
  //   onSelect: function(selected) {
  //     $("#time_from").datepicker("option","maxDate", selected)
  //   },
  //   disableTouchKeyboard: true,
  //   Readonly: true
  // }).attr("readonly", "readonly");
  // jQuery(function($){
  //   $.datepicker.regional['vi'] = {
  //     closeText: 'Đóng',
  //     prevText: '<Trước',
  //     nextText: 'Tiếp>',
  //     currentText: 'Hôm nay',
  //     monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
  //     'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
  //     monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
  //     'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
  //     dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
  //     dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
  //     dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
  //     weekHeader: 'Tu',
  //     dateFormat: 'dd/mm/yy',
  //     firstDay: 0,
  //     isRTL: false,
  //     showMonthAfterYear: false,
  //     yearSuffix: ''};
  //     $.datepicker.setDefaults($.datepicker.regional['vi']);
  //   });
  </script>