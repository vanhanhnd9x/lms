<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
global $CFG,$DB,$USER;

$PAGE->set_title('Báo cáo số lượng học sinh ');
$PAGE->set_heading('Báo cáo số lượng học sinh ');
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$time_from = optional_param('time_from', '', PARAM_TEXT);
$time_to = optional_param('time_to', '', PARAM_TEXT);

// dem so hoc sinh dang hoc tai truong do
function hoc_sinh_theo_hoc_theo_dk($idkhoi,$time_from, $time_to=null){
	global $DB;
	$sql = "SELECT COUNT( DISTINCT `user`.`id`) as total 
	FROM user 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN groups_members ON `user`.id = `groups_members`.`userid`
	JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid`
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `groups`.`id_khoi` ={$idkhoi}
	";
	// if(!empty($time_from)){
	// 	$sql.=" AND (`user`.`time_start_int` >= {$time_from}  OR `user`.`time_start_int` =0 OR `user`.`time_start_int` is null) ";
	// }
	// if(!empty($time_to)){
	// 	$sql.=" AND (`user`.`time_start_int` <={$time_to} OR `user`.`time_start_int` =0 OR `user`.`time_start_int` is null)";
	// }

	if(!empty($time_from)&&empty($time_to)){
		$sql.=" AND (`user`.`time_start_int` >= {$time_from} OR `user`.`time_start_int` =0  OR `user`.`time_start_int` is null)  AND (`user`.`time_end_int` > {$time_from} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)";
	}
	if(empty($time_from)&&!empty($time_to)){
		
		$sql.=" AND (`user`.`time_start_int` <={$time_to} OR `user`.`time_start_int` =0 OR `user`.`time_start_int` is null)  AND (`user`.`time_end_int` > {$time_to} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)";
	}
	if(!empty($time_from)&&!empty($time_to)){
		
		$sql.=" AND ((`user`.`time_start_int` >= {$time_from} AND `user`.`time_start_int` <= {$time_to}) OR `user`.`time_start_int` =0 OR `user`.`time_start_int` is null)  AND (`user`.`time_end_int` > {$time_to} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null) ";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_hoc_sinh_nghi_theo_dieu_kien($idkhoi,$time_from, $time_to=null){
	global $DB;
	$sql = "SELECT COUNT( DISTINCT `user`.`id`) as total 
	FROM user 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN groups_members ON `user`.id = `groups_members`.`userid`
	JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid`
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `groups`.`id_khoi` ={$idkhoi}
	";
	if(!empty($time_from)){
		$sql.=" AND `user`.`time_end_int` >= {$time_from} AND `user`.`time_end_int` >0";
	}
	if(!empty($time_to)){
		$sql.=" AND `user`.`time_end_int` <= {$time_to}  AND `user`.`time_end_int` >0";
	}
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
// dem so hoc sinh trong truong 
function dem_so_hoc_sinh_cua_khoi($idkhoi){
	global $DB;
	$sql = "SELECT COUNT( DISTINCT `user`.`id`) as total 
	FROM user 
	JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
	JOIN groups_members ON `user`.id = `groups_members`.`userid`
	JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid`
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `groups`.`id_khoi` ={$idkhoi}
	";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function dem_so_nhom_lop_cua_khoi($idkhoi){
	global $DB;
	$sql="SELECT COUNT(DISTINCT `group_course`.`id`) FROM `group_course` WHERE `group_course`.`group_id` IN (SELECT `groups`.`id` FROM `groups` WHERE `groups`.`id_khoi`={$idkhoi})";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function lay_khoi_theo_truong($idschool){
	global $DB;
	$sql="SELECT id, name FROM block_student WHERE del =0 AND schoolid={$idschool}";
	$data = $DB->get_records_sql($sql); 
	return $data;
}

	list($day1, $month1, $year1) = explode('/', $time_from);
	$time_from_int=mktime(0, 0, 0, $month1, $day1, $year1);
  	list($day, $month, $year) = explode('/', $time_to);
  	$time_to_int=mktime(23, 59, 59, $month, $day, $year); 

	
	if(!empty($schoolid)){
		// $cua=get_all_shool();
		$sql_report=" SELECT * FROM schools WHERE del =0 AND id ={$schoolid}";
		$cua=$DB->get_records_sql($sql_report);
	}else{
		$cua=get_all_shool();
	}
	// $objPHPExcel = new PHPExcel(); 
	// $objPHPExcel->getActiveSheet()->setTitle('Báo cáo danh sách học sinh');
	// $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STT');
	// $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Từ ngày');
	// $objPHPExcel->getActiveSheet()->SetCellValue('C1','Đến ngày'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Trường');
	// $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Khối'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Số nhóm lớp'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Số học sinh'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Học sinh đã nghỉ'); 
	// $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Học sinh đang học'); 
	// $numRow = 2;
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/number_student_report.xlsx');
	$numRow = 3;
	$i=0;
	$tongnhom=0;
	$tongconghs=0;
	$tongconghsnghi=0;
	$tongconghshoc=0;
	foreach ($cua as $value) {
		$khoi=lay_khoi_theo_truong($value->id); 
		foreach ($khoi as  $dl) {
			$i++;
			$nhom=dem_so_nhom_lop_cua_khoi($dl->id);
			$nghi=dem_so_hoc_sinh_nghi_theo_dieu_kien($dl->id,$time_from_int, $time_to_int);
			$hoc=hoc_sinh_theo_hoc_theo_dk($dl->id,$time_from_int, $time_to_int);
			if(empty($time_from_int)&&empty($time_to_int)){
  				$nghi=0;
  				$hoc=0;
  			}
			$tonghocsinh=dem_so_hoc_sinh_cua_khoi($dl->id);

			$tongnhom=$tongnhom+$nhom;
			$tongconghs=$tongconghs+$tonghocsinh;
			$tongconghsnghi=$tongconghsnghi+$nghi;
			$tongconghshoc=$tongconghshoc+$hoc;

			# code...
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $i);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $time_from);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $time_to);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $value->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $dl->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $nhom);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $tonghocsinh);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $nghi);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $hoc);
			$numRow ++;
		}
	}
	// tinh tong
	$sodem=$i+3;
	$objPHPExcel->getActiveSheet()->SetCellValue('A' . $sodem, 'Tổng');
	$objPHPExcel->getActiveSheet()->SetCellValue('B' . $sodem, '');
	$objPHPExcel->getActiveSheet()->SetCellValue('C' . $sodem, '');
	$objPHPExcel->getActiveSheet()->SetCellValue('D' . $sodem, '');
	$objPHPExcel->getActiveSheet()->SetCellValue('E' . $sodem, '');
	$objPHPExcel->getActiveSheet()->SetCellValue('F' . $sodem, $tongnhom);
	$objPHPExcel->getActiveSheet()->SetCellValue('G' . $sodem, $tongconghs);
	$objPHPExcel->getActiveSheet()->SetCellValue('H' . $sodem, $tongconghsnghi);
	$objPHPExcel->getActiveSheet()->SetCellValue('I' . $sodem, $tongconghshoc);
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	ob_end_clean();
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="Bao-cao-so-luong-hoc-sinh.xlsx"');
	$objWriter->save('php://output');
	exit;
?>