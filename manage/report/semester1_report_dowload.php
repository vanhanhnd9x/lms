<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
require_once($CFG->dirroot . '/manage/report/in.php');
global $CFG,$DB,$USER;
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$schoolstatus = optional_param('schoolstatus', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);

function show_tongket_hsthcua($diem){
	$xeploai="Chờ kết quả thi";
	if($diem>=84.5){
		$xeploai= 'Giỏi'; 
	}
	if(($diem>=64.5)&&($diem<84.5)){
		$xeploai='Khá'; 
	}
	if(($diem>=49.5)&&($diem<64.5)){
		$xeploai= 'TB'; 
	}
	if($diem<49.5){
		$xeploai= 'Dưới TB'; 
	}
	return $xeploai;
}
function show_tongke_hsthptcua($diem){
	$xeploai="Chờ kết quả thi";
	if($diem>=79.5){
		$xeploai='Giỏi'; 
	}
	if(($diem>=64.5)&&($diem<79.5)){
		$xeploai= 'Khá'; 
	}
	if(($diem>=49.5)&&($diem<64.5)){
		$xeploai= 'TB'; 
	}
	if($diem<49.5){
		$xeploai= 'Dưới TB'; 
	}
	return $xeploai;

}
function hien_thi_ten_sach_cua_nhom($idcourse,$idschool_year){
	global $DB;
	 $sql = "SELECT DISTINCT `schedule`.`book` 
    FROM `schedule` 
    WHERE `schedule`.`courseid`={$idcourse}
    AND `schedule`.`school_yearid`={$idschool_year}
    AND `schedule`.`del`=0
    ";
    $data = $DB->get_field_sql($sql,NULL,$strictness=IGNORE_MISSING);
    return $data;

}
function lay_danh_sach_xuattieuhocbycua($school_yearid,$schoolid,$courseid=null){
	global $DB;
	$sql=" 
		SELECT `semester`.`id`, 
		`semester`.`s1_1`, 
		`semester`.`s2_1`, 
		`semester`.`s3_1`, 
		`semester`.`s4_1`, 
		`semester`.`s5_1`, 
		`semester`.`noi_1`, 
		`semester`.`nghe_1`,
		`semester`.`doc_viet_1`,
		`semester`.`tonghocki_1`,
		`semester`.`tongket`, 
		`semester`.`courseid`, 
		`semester`.`note`,
		`user`.`firstname`, 
		`user`.`lastname`, 
		`user`.`code`, 
		`course`.`fullname`,
		`history_course_name`.`course_name`
		FROM `semester` 
		JOIN `user` ON `user`.`id` = `semester`.`userid` 
		JOIN `course` ON `course`.`id` =`semester`.`courseid` 
		JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
		WHERE `semester`.`school`=1 AND `semester`.`del`=0 
		AND `semester`.`school_yearid` ={$school_yearid}
		AND `history_course_name`.`school_year_id`={$school_yearid}
	";
	// if(!empty($school_yearid)){
	// 	$sql.=" AND `semester`.`school_yearid` ={$school_yearid}";
	// }
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid` ={$schoolid}";
	}
	if(!empty($courseid)){
		$sql.=" AND `semester`.`courseid` ={$courseid}";
	}
	$sql.=" ORDER BY `user`.`firstname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function lay_danh_sach_xuattrunghocbycua($school_yearid,$schoolid,$courseid=null){
	global $DB;
	$sql=" 
		SELECT 
		`semester`.`id`,
		`semester`.`s1_1`, 
		`semester`.`s2_1`, 
		`semester`.`s3_1`, 
		`semester`.`s4_1`, 
		`semester`.`s5_1`,
		`semester`.`noi_1`,
		`semester`.`viet_luan_1`,
		`semester`.`doc_hieu_1`,
		`semester`.`nghe_1`,
		`semester`.`tv_np_1`,
		`semester`.`tvnp_dochieu_viet_1`,
		`semester`.`tonghocki_1`,
		`semester`.`tongket`,
		`semester`.`courseid`,
		`semester`.`note`,
		`user`.`firstname`, 
		`user`.`lastname`,
		`user`.`code`,
		`course`.`fullname`,
		`history_course_name`.`course_name`
		FROM `semester` 
		JOIN `user` ON `user`.`id` = `semester`.`userid` 
		JOIN `course` ON `course`.`id` =`semester`.`courseid` 
		JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
		WHERE `semester`.`school`=2 AND `semester`.`del`=0 
		AND `semester`.`school_yearid` ={$school_yearid}
		AND `history_course_name`.`school_year_id`={$school_yearid}
	";
	// if(!empty($school_yearid)){
	// 	$sql.=" AND `semester`.`school_yearid` ={$school_yearid}";
	// }
	if(!empty($schoolid)){
		$sql.=" AND `semester`.`schoolid` ={$schoolid}";
	}
	if(!empty($courseid)){
		$sql.=" AND `semester`.`courseid` ={$courseid}";
	}
	$sql.=" ORDER BY `user`.`firstname` ASC";
	$data = $DB->get_records_sql($sql);
	return $data;
}
$school=$DB->get_record('schools', array(
  'id' => $schoolid
));
$namhoc=$DB->get_record('school_year', array(
  'id' => $school_yearid
));
$schedule=$DB->get_record('schedule', array(
  'courseid' => $courseid,
  'school_yearid' => $school_yearid,
  'del'=>0
));
$course=$DB->get_record('course', array(
  'id' => $courseid,
));
$history_course_name=$DB->get_record('history_course_name', array(
  'school_year_id' => $school_yearid,
  'courseid' => $courseid,
  'groupid' => $groupid,
));
// $nhomvasach=$course->fullname;
$nhomvasach=$history_course_name->course_name;
$day_report=date('d-M-y',time());
$teacher=ten_giao_vien_xuat_bao_cao_theo_nhom($courseid); 
$title1="SEMESTER 1 REPORT ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$title2="BÁO CÁO KẾT QUẢ HKI NĂM HỌC ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$text2='SPREADSHEET '.$namhoc->sy_start.'-'.$namhoc->sy_end;
$text3=' BẢNG ĐIỂM  '.$namhoc->sy_start.'-'.$namhoc->sy_end;
$texten=$school->name." - FINAL REPORT ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$texten=mb_convert_case($texten, MB_CASE_UPPER, "UTF-8");
$textvi=$school->name." - BÁO CÁO KẾT QUẢ HỌC TẬP HK I NĂM HỌC ".$namhoc->sy_start.'-'.$namhoc->sy_end;
$textvi=mb_convert_case($textvi, MB_CASE_UPPER, "UTF-8");
// $tentruong=strtoupper($school->name);
if($schedule->book){$nhomvasach=$nhomvasach.' & '.$schedule->book; }
if($schoolstatus==1){
	$data=lay_danh_sach_xuattieuhocbycua($school_yearid,$schoolid,$courseid); 
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/semester1_report_tieuhoc.xlsx');
		$objWorkSheet = $objPHPExcel->createSheet();
		$numRow = 7; 
		// $objPHPExcel->getActiveSheet()->SetCellValue('A1', $title) ;
		
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A1', $title2);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A4', $text3);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A5', $text2);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A6', $school->name);

		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A8', $nhomvasach);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A10', $day_report);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A12', $school->name);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A14', $teacher);

		$i=0;
		foreach ($data as $key => $row) {
			$i++;
			$name = $row->lastname.' '.$row->firstname;
			if($row->tonghocki_1>0){$xeploai=show_tongket_hsthcua($row->tonghocki_1);}else{$xeploai='Chờ kết quả thi';} 
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('B' . $numRow, $i);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('C' . $numRow, $row->code);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('D' . $numRow, $name);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('E' . $numRow, $row->nghe_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('F' . $numRow, $row->doc_viet_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('G' . $numRow, $row->noi_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('H' . $numRow, $row->tonghocki_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('I' . $numRow, $xeploai);
			$numRow++;
			# code...
			$nxnoi=show_nhan_xet_ky_nang_noihk1($row->noi_1); 
			$nxnghe=show_nx_ky_nang_nghehk1($row->nghe_1);
			$nxndochieuviet=show_nx_ky_nang_dochieuvaviethk1($row->doc_viet_1);
			// cua add
			$objWorkSheet->setTitle("Worksheet$work_sheet");
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A1',$texten );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A2',$textvi );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A6',$school->name );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D3',$school->name );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('K3',$i );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D4',$name );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D5',$nhomvasach );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D6',$day_report );


         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J4',$row->noi_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J5',$row->nghe_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J6',$row->doc_viet_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J7',$row->tonghocki_1 );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E9',$nxnoi );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E10',$nxnghe );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E11',$nxndochieuviet );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A13',$row->note );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('I15',$teacher );

		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Bao-cao-hoc-ky-1.xlsx"');
		$objWriter->save('php://output');
		exit;
}
if($schoolstatus==2){
		$data=lay_danh_sach_xuattrunghocbycua($school_yearid,$schoolid,$courseid);
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/semester1_report_trunghoc.xlsx');
		$objWorkSheet = $objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A1', $title2);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A4', $text3);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A5', $text2);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A6', $school->name);

		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A8', $nhomvasach);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A10', $day_report);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A12', $school->name);
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('A14', $teacher);
		$numRow = 7;
		$i=0;
		foreach ($data as $key => $row) {
			$i++;
			$name = $row->lastname.' '.$row->firstname;
			$book=hien_thi_ten_sach_cua_nhom($row->courseid,$school_yearid);
			$row1=$book.' '.$row->fullname;
			if($row->tonghocki_1>0){$xeploai=show_tongke_hsthptcua($row->tonghocki_1);}else{$xeploai='Chờ kết quả thi';}

			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('B' . $numRow, $i);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('C' . $numRow, $row->code);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('D' . $numRow, $name);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('E' . $numRow, $row->viet_luan_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('F' . $numRow, $row->nghe_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('G' . $numRow, $row->tv_np_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('H' . $numRow, $row->doc_hieu_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('I' . $numRow, $row->tvnp_dochieu_viet_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('J' . $numRow, $row->noi_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('K' . $numRow, $row->tonghocki_1);
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue('L' . $numRow, $xeploai);
			$numRow++;
			# code...

			$nxnoi=show_nhan_xet_ky_nang_noihk1($row->noi_1); 
			$nxnghe=show_nx_ky_nang_nghehk1($row->nghe_1);
			$nxndochieuviet=show_nx_ky_nang_dochieuvaviethk1($row->tvnp_dochieu_viet_1);
			// cua add
			$objWorkSheet->setTitle("Worksheet$work_sheet");
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A1',$texten );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A2',$textvi );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D3',$school->name );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('K3',$i );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D4',$name );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D5',$nhomvasach );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('D6',$day_report );


         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J4',$row->noi_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J5',$row->nghe_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J6',$row->tvnp_dochieu_viet_1 );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('J7',$row->tonghocki_1 );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E9',$nxnoi );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E10',$nxnghe );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('E11',$nxndochieuviet );

         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('A13',$row->note );
         	$objPHPExcel->setActiveSheetIndex($i)->SetCellValue('I15',$teacher );
		}

		// $objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Bao-cao-hoc-ky-1.xlsx"');
		$objWriter->save('php://output');
		exit;
}
// if($schoolstatus==1){
// 	$data=lay_danh_sach_xuattieuhocbycua($school_yearid,$schoolid); 
// 		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
// 		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/semester1_report_tieuhoc.xlsx');
// 		$numRow = 7; 
// 		// $objPHPExcel->getActiveSheet()->SetCellValue('A1', $title) ;
// 		$text='BÁO CÁO KẾT QUẢ HỌC TẬP HK I NĂM HỌC'.$namhoc->sy_start.'-'.$namhoc->sy_end;
// 		$text2='SPREADSHEET '.$namhoc->sy_start.'-'.$namhoc->sy_end;
// 		$text3=' BẢNG ĐIỂM  '.$namhoc->sy_start.'-'.$namhoc->sy_end;
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $text);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A4', $text3);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A5', $text2);

// 		$i=0;
// 		foreach ($data as $key => $row) {
// 			$i++;
// 			$name = $row->lastname.' '.$row->firstname;
// 			$row1=$row->fullname;
// 			$book =hien_thi_ten_sach_cua_nhom($row->courseid,$school_yearid);
// 			if($book)$row1.=' & '.$book;
// 			if($row->tonghocki_1>0){$xeploai=show_tongket_hsthcua($row->tonghocki_1);}else{$xeploai='Chờ kết quả thi';} 
// 			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $row1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $i);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $name);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->s1_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->s2_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->s3_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->s4_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->s5_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->nghe_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->doc_viet_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->noi_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->tonghocki_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $xeploai);
// 			$numRow++;
// 			# code...
// 		}
// 		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 		ob_end_clean();
// 		// We'll be outputting an excel file
// 		header('Content-type: application/vnd.ms-excel');
// 		header('Content-Disposition: attachment; filename="Bao-cao-hoc-ky-1.xlsx"');
// 		$objWriter->save('php://output');
// 		exit;
// }
// if($schoolstatus==2){
// 		$data=lay_danh_sach_xuattrunghocbycua($school_yearid,$schoolid);
// 		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
// 		$objPHPExcel = $objReader->load($CFG->dirroot. '/manage/report/tem/semester1_report_trunghoc.xlsx');
// 		$text='BÁO CÁO KẾT QUẢ HỌC TẬP HK I NĂM HỌC'.$namhoc->sy_start.'-'.$namhoc->sy_end;
// 		$text2='SPREADSHEET '.$namhoc->sy_start.'-'.$namhoc->sy_end;
// 		$text3=' BẢNG ĐIỂM  '.$namhoc->sy_start.'-'.$namhoc->sy_end;
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A1', $text);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A4', $text3);
// 		$objPHPExcel->getActiveSheet()->SetCellValue('A5', $text2);
// 		$numRow = 7;
// 		$i=0;
// 		foreach ($data as $key => $row) {
// 			$i++;
// 			$name = $row->lastname.' '.$row->firstname;
// 			$book=hien_thi_ten_sach_cua_nhom($row->courseid,$school_yearid);
// 			$row1=$book.' '.$row->fullname;
// 			if($row->tonghocki_1>0){$xeploai=show_tongke_hsthptcua($row->tonghocki_1);}else{$xeploai='Chờ kết quả thi';}
// 			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $numRow, $row1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $numRow, $i);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $numRow, $name);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $numRow, $row->s1_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $numRow, $row->s2_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $numRow, $row->s3_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $numRow, $row->s4_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $numRow, $row->s5_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $numRow, $row->viet_luan_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $numRow, $row->nghe_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $numRow, $row->tv_np_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $numRow, $row->doc_hieu_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $numRow, $row->tvnp_dochieu_viet_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('N' . $numRow, $row->noi_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('O' . $numRow, $row->tonghocki_1);
// 			$objPHPExcel->getActiveSheet()->SetCellValue('P' . $numRow, $xeploai);
// 			$numRow++;
// 			# code...
// 		}

// 		// $objPHPExcel->setActiveSheetIndex(0);

// 		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 		ob_end_clean();
// 		// We'll be outputting an excel file
// 		header('Content-type: application/vnd.ms-excel');
// 		header('Content-Disposition: attachment; filename="Bao-cao-hoc-ky-1.xlsx"');
// 		$objWriter->save('php://output');
// 		exit;
// }
?>