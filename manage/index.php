<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/evaluation/lib.php');
// require_once($CFG->dirroot . '/manage/schedule/lib.php');
// require_once($CFG->dirroot . '/teams/lib.php');
global $USER, $CFG;
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// Require Login.


require_login();
if(is_student()){
    echo "is student manage index";
    header('location:home.php');
}
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/dashboard.php'));
    $PAGE->set_button($buttons);
}

$unique_login_values = prepare_data_for_chart(30,0);
$course_sales_values = prepare_data_for_chart(30,1);
$course_complete_values = prepare_data_for_chart(30,2);
$activityfor = get_string('activityfor');
$uniquelog = get_string('uniquelog');
$coursesale = get_string('coursesale');
$coursecomple = get_string('coursecomple');
$PAGE->requires->js('/manage/js/jquery.min.js');
$PAGE->requires->js('/manage/js/jquery.jqplot.min.js');            
$PAGE->requires->js('/manage/js/jqplot/jqplot.highlighter.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.cursor.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.dateAxisRenderer.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.enhancedLegendRenderer.js');
$PAGE->requires->css('/manage/css/jquery.jqplot.min.css');
if ($unique_login_values != "") {
    $PAGE->requires->js('/manage/js/js.php?unique_login_values="'.$unique_login_values.'"&course_sales_values="'.$course_sales_values.'"&course_complete_values="'
                               .$course_complete_values.'"&activityfor="'.$activityfor.'"&uniquelog="'.$uniquelog.'"&coursesale="'.$coursesale.'"&coursecomple="'.$coursecomple.'"');
}  

$PAGE->set_title(get_string('dashboards'));
$PAGE->set_heading(get_string('dashboards'));
//now the page contents
$PAGE->set_pagelayout(get_string('dashboards'));

echo $OUTPUT->header();

// require_once($CFG->dirroot . '/common/lib.php');

//$mobile = detect_mobile();
//   
//if($mobile === true){
//    echo displayJsAlert('', $CFG->wwwroot.'/mobile/index.php');
//}   
    
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '15', PARAM_INT); // how many per page

// add_to_log($USER->id, 'course', 'view', "view.php?id=123", "123");

?>

<?php 
$course_records = get_course_user($USER->id); // lấy id của user đăng nhập
 ?>

<!-- Giáo viên----------------------------------------------------------------------------------------- -->

<?php 
    if(is_teacher() || is_teacher_tutors()): 
    $action = optional_param('action', '', PARAM_TEXT);    
    $dk = optional_param('dk', '', PARAM_TEXT);    
    $schoolid = optional_param('schoolid', '', PARAM_TEXT);    
    $roleid=lay_role_id_cua_user_dang_nhap($USER->id);
    $idnamhoc=get_id_school_year_now();
    $school_list=hien_thi_ds_truong_duoc_them_gan_cho_gv($roleid,$USER->id);
    $dk_sql_id_course=array();
    switch ($roleid) {
        case 8:  $list_id_course=lay_ds_id_nhom_cua_gvnn_bycua_dggv($USER->id); break;
        case 10: $list_id_course=lay_ds_id_nhom_cua_gvtg_bycua_dggv($USER->id); break;
    }
    foreach ($list_id_course as $key => $value) {
        # code...
        array_push($dk_sql_id_course, $key);
    }
    $dk_sql_id_course=implode(',', $dk_sql_id_course);
    if($action=='search'){
        $dk_sql_id_course=$dk;
    }
    if(empty($dk_sql_id_course)){
        $dk_sql_id_course=0;
    }
    // var_dump($dk_sql_id_course);
    $sql_schedule="SELECT `schedule`.*,`schedule_info`.*, `course`.`fullname`, `schools`.`name`  FROM `schedule`
    JOIN `schedule_info` ON `schedule`.`id`= `schedule_info`.`scheduleid`
    JOIN `course` ON `course`.`id` = `schedule`.`courseid`
    JOIN `groups` ON `groups`.`id` =`schedule`.`groupid`
    JOIN `schools` on `schools`.`id`= `groups`.`id_truong`
    WHERE `schedule`.`del` =0
    AND `schools`.`del`=0
    AND `schedule`.`courseid` IN ({$dk_sql_id_course})
    AND `schedule`.`school_yearid`={$idnamhoc}";
    if(!empty($schoolid)){
        $sql_schedule.=" AND `schools`.`id`={$schoolid} ";
    }
    $list_schedule=$DB->get_records_sql($sql_schedule);
    
    $name_rht=$DB->get_record('user', array(
      'id' => $USER->name_rht
    ));
    $rht=$name_rht->lastname.' '.$name_rht->firstname;
    
?>
<div class=" card-box">
    <form action="" class="row">
        <div class="col-nmd-12">
            <h4 class="header-title mb-4"><?php echo get_string('lecture') ?></h4>
        </div>
        <input type="text" hidden="" name="action"  value="search">
        <input type="text" hidden="" name="dk"  value="<?php echo $dk_sql_id_course; ?>">
        <div class="col-md-4" style="margin-bottom: 10px;">
            <label for="" class="control-label"><?php echo get_string('schools'); ?></label>
            <select name="schoolid" id="schoolid" class="form-control" onchange="this.form.submit()">
                    <option value=""><?php echo get_string('schools'); ?></option>
                <?php 
                    if(!empty($school_list)){
                        foreach ($school_list as $value) {
                            ?>
                            <option value="<?php echo $value->id; ?>" <?php if(!empty($schoolid)&&($schoolid==$value->id)) echo 'selected'; ?>><?php echo $value->name; ?></option>
                            <?php 
                        }
                    }
                 ?>
            </select>
        </div>
    </form>
    <div class="row">
        <?php 
        switch ($roleid) {
            case 8:
                ?>
                <div class="col-md-12" id="">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th data-priority="6">STT</th>
                              <th data-priority="6"><?php echo get_string('schools'); ?></th>
                              <th data-priority="6"><?php echo get_string('classgroup'); ?></th>
                              <th data-priority="6"><?php echo get_string('day'); ?></th>
                              <th data-priority="6"><?php echo get_string('time'); ?></th>
                              <th data-priority="6"><?php echo get_string('syllabus_name'); ?></th>
                              <th data-priority="6"><?php echo get_string('class_room'); ?></th>
                              <th data-priority="6"><?php echo get_string('GVTG'); ?></th>
                              <th data-priority="6">RHT</th>
                              <th data-priority="6"><?php echo get_string('note'); ?></th>
                              
                              
                            </tr>
                        </thead>
                        <?php 
                            if(!empty($list_schedule)){
                                $i=0;
                                foreach ($list_schedule as  $value) {
                                    $idgvtg=lay_id_gvtg_thuoc_nhom_duocgan_manhinhdieukien($value->courseid);
                                    $gvtg=$DB->get_record('user', array(
                                      'id' => $idgvtg
                                    ));
                                    $gvtgname=$gvtg->lastname.' '.$gvtg->firstname;
                                    $i++;
                                    switch ($value->day) {
                                        case 2: $day= get_string('monday'); break;
                                        case 3: $day= get_string('tuesday'); break;
                                        case 4: $day=  get_string('wednesday'); break;
                                        case 5: $day=  get_string('thursday'); break;
                                        case 6: $day=  get_string('friday'); break;
                                        case 7: $day=  get_string('saturday'); break;
                                        case 8: $day=  get_string('sunday'); break;
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php echo $value->fullname; ?></td>
                                        <td><?php echo $day; ?></td>
                                        <td><?php echo $value->time_start.' - '.$value->time_end; ?></td>
                                        <td><?php echo $value->book; ?></td>
                                        <td><?php echo $value->location; ?></td>
                                        <td><?php echo $gvtgname; ?></td>
                                        <td><?php echo $rht; ?></td>
                                        <td><?php echo $value->note; ?></td>
                                    </tr>
                                    <?php 
                                }
                            }
                        ?>
                    </table>
                </div>
                <?php 
            break;
            case 10:
                ?>
                <div class="col-md-12" id="">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th data-priority="6">STT</th>
                              <th data-priority="6"><?php echo get_string('schools'); ?></th>
                              <th data-priority="6"><?php echo get_string('classgroup'); ?></th>
                              <th data-priority="6"><?php echo get_string('day'); ?></th>
                              <th data-priority="6"><?php echo get_string('time'); ?></th>
                              <th data-priority="6"><?php echo get_string('syllabus_name'); ?></th>
                              <th data-priority="6"><?php echo get_string('class_room'); ?></th>
                              <th data-priority="6"><?php echo get_string('GVNN'); ?></th>
                              <th data-priority="6">RHTA</th>
                              <th data-priority="6"><?php echo get_string('note'); ?></th>
                              
                              
                            </tr>
                        </thead>
                         <?php 
                            if(!empty($list_schedule)){
                                $i=0;
                                foreach ($list_schedule as  $value) {
                                    $idgvnn=lay_id_gvnn_thuoc_nhom_duocgan_manhinhdieukien($value->courseid);
                                    $gvnn=$DB->get_record('user', array(
                                      'id' => $idgvnn
                                    ));
                                    $gvnnname=$gvnn->lastname.' '.$gvnn->firstname;
                                    $i++;
                                    switch ($value->day) {
                                        case 2: $day= get_string('monday'); break;
                                        case 3: $day= get_string('tuesday'); break;
                                        case 4: $day=  get_string('wednesday'); break;
                                        case 5: $day=  get_string('thursday'); break;
                                        case 6: $day=  get_string('friday'); break;
                                        case 7: $day=  get_string('saturday'); break;
                                        case 8: $day=  get_string('sunday'); break;
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php echo $value->fullname; ?></td>
                                        <td><?php echo $day; ?></td>
                                        <td><?php echo $value->time_start.' - '.$value->time_end; ?></td>
                                        <td><?php echo $value->book; ?></td>
                                        <td><?php echo $value->location; ?></td>
                                        <td><?php echo $gvnnname; ?></td>
                                        <td><?php echo $rht; ?></td>
                                        <td><?php echo $value->note; ?></td>
                                    </tr>
                                    <?php 
                                }
                            }
                        ?>
                    </table>
                </div>
                <?php 
            break;
            
        }
     ?>
    </div>
    
    
</div>


<div class="row">
    <div class="col-md-9">
    </div>
</div>
<?php endif; ?>


<!-- CBNV ADMIN ---------------------------------------------------------------------------------------- -->

<?php
    $roleid=lay_role_id_cua_user_dang_nhap($USER->id);
    if(in_array($roleid,array(3,13,14,15,16,17,18,19,20,21,11,12))):
 // if(is_admin()):
  ?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="header-title"><?php echo get_string('action7day') ?></h4>

            <div class="chart mt-4" id="line-chart"></div>
        </div>
    </div>
</div>
<!-- end row -->

<div class="row">
    <div class="col-md-12">
        <!-- Left Content -->
        <div class="card-box">
            <ul class="nav nav-tabs tabs-bordered">
                <li class="nav-item">
                    <a class="nav-link" href="<?php print new moodle_url('/manage'); ?>" class="selected"><?php print_r(get_string('pluginname','block_recent_activity'))?></a>
                </li>
            </ul>
            <div class="tab-content">
                <?php 
                    view_logs(' `action` != "error"');
                 ?>
            </div>
        </div>
    </div>
</div>
<?php endif ?>


<?php
    echo $OUTPUT->footer();
?>
<!-- Google Charts js -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
    // hien thi thoi khoa bieu theo tung lop doi voi gv dang nhap
    // var course_id = $('#courseid_to_schedule').find(":selected").val();
    // if(course_id){
    //     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid_to_schedule=" + course_id, function(data) {
    //             $("#show_schedule").html(data);
    //         });
    // }
    // $('#courseid_to_schedule').on('change', function() {
    //     var courseid = this.value;
    //     if (this.value) {
    //         $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid_to_schedule=" + courseid, function(data) {
    //             $("#show_schedule").html(data);
    //         });
    //     }
    // });
</script>
<script type="text/javascript">
! function($) {
    "use strict";

    var GoogleChart = function() {
        this.$body = $("body")
    };

    //creates line graph
    GoogleChart.prototype.createLineChart = function(selector, data, axislabel, colors) {
        var options = {
            fontName: 'Roboto',
            height: 340,
            curveType: 'function',
            fontSize: 12,
            chartArea: {
                left: '5%',
                width: '90%',
                height: 250
            },
            pointSize: 2,
            tooltip: {
                textStyle: {
                    fontName: 'Roboto',
                    fontSize: 14
                }
            },
            vAxis: {
                title: axislabel,
                titleTextStyle: {
                    fontSize: 12,
                    italic: false
                },
                gridlines:{
                    color: '#f5f5f5',
                    count: 5
                },
                minValue:0,
                viewWindow: {
                    min: 0
                }
            },
            legend: {
                position: 'top',
                alignment: 'center',
                textStyle: {
                    fontSize: 14
                }
            },
            lineWidth: 3,
            colors: colors
        };

        var google_chart_data = google.visualization.arrayToDataTable(data);
        var line_chart = new google.visualization.LineChart(selector);
        line_chart.draw(google_chart_data, options);
        return line_chart;
    },

    //init
    GoogleChart.prototype.init = function () {
        var $this = this;
        <?php
          $count_login = get_counts_login(date('d', time())); 
          $day6 = date('d-m-Y', time()-86400*6);
          $day5 = date('d-m-Y', time()-86400*5);
          $day4 = date('d-m-Y', time()-86400*4);
          $day3 = date('d-m-Y', time()-86400*3);
          $day2 = date('d-m-Y', time()-86400*2);
          $day1 = date('d-m-Y', time()-86400);
          $day  = date('d-m-Y', time());
        ?>
        //creating line chart
        var common_data = [
            ['Day', "Số lần truy cập"],
            ['<?php echo $day6; ?>',  <?php echo get_counts_login($day6) ?>],
            ['<?php echo $day5; ?>',  <?php echo get_counts_login($day5) ?>],
            ['<?php echo $day4; ?>',  <?php echo get_counts_login($day4) ?>],
            ['<?php echo $day3; ?>',  <?php echo get_counts_login($day3) ?>],
            ['<?php echo $day2; ?>',  <?php echo get_counts_login($day2) ?>],
            ['<?php echo $day1; ?>',  <?php echo get_counts_login($day1) ?>],
            ['<?php echo $day; ?>',   <?php echo get_counts_login($day) ?>]
        ];
        $this.createLineChart($('#line-chart')[0], common_data, '', ['#4eb7eb', '#f1556c']);

    },
    //init GoogleChart
    $.GoogleChart = new GoogleChart, $.GoogleChart.Constructor = GoogleChart
}(window.jQuery),

//initializing GoogleChart
function($) {
    "use strict";
    //loading visualization lib - don't forget to include this
    google.load("visualization", "1", {packages:["corechart"]});
    //after finished load, calling init method
    google.setOnLoadCallback(function() {$.GoogleChart.init();});
}(window.jQuery);

</script>

<script>
    console.log(<?php echo md5('123456') ?>);
</script>