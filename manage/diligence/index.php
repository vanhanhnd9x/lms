<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;

require_once($CFG->dirroot . '/teams/lib.php');
require_once('lib.php');

require_login(0, false);
$PAGE->set_title(get_string('attendance'));
echo $OUTPUT->header();

$id = optional_param('id', '', PARAM_TEXT);
$list_members = get_members_in_team_second($id);
$schoolyearid = get_id_schoolyear($id);
$allday = getAllDayByMonth(date('m'));

$schoolid = get_info_groups($id);

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='course';
$name1='attendance';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
// if(is_teacher() || is_admin()){
?>

<div class="row">
    <div class="col-md-12">
            
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mr_bottom15">

                        <div class="col-md-3">
                            <label><?php print_r(get_string('attendancebook')) ?>: <?php echo get_info_groups($id)->fullname ?></label>
                            
                        </div>
                        
                        <div class="col-md-3">
                            <label><?php print_r(get_string('choose_month')) ?>:</label>
                            <select class="form-control" id="thang">
                                <option <?php echo date('m')==01 ? "selected" : "" ?> value="01">Tháng 1</option>
                                <option <?php echo date('m')==02 ? "selected" : "" ?> value="02">Tháng 2</option>
                                <option <?php echo date('m')==03 ? "selected" : "" ?> value="03">Tháng 3</option>
                                <option <?php echo date('m')==04 ? "selected" : "" ?> value="04">Tháng 4</option>
                                <option <?php echo date('m')==05 ? "selected" : "" ?> value="05">Tháng 5</option>
                                <option <?php echo date('m')==06 ? "selected" : "" ?> value="06">Tháng 6</option>
                                <option <?php echo date('m')==07 ? "selected" : "" ?> value="07">Tháng 7</option>
                                <option <?php echo date('m')==08 ? "selected" : "" ?> value="08">Tháng 8</option>
                                <option <?php echo date('m')==09 ? "selected" : "" ?> value="09">Tháng 9</option>
                                <option <?php echo date('m')==10 ? "selected" : "" ?> value="10">Tháng 10</option>
                                <option <?php echo date('m')==11 ? "selected" : "" ?> value="11">Tháng 11</option>
                                <option <?php echo date('m')==12 ? "selected" : "" ?> value="12">Tháng 12</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label><?php print_r(get_string('chooseyear')) ?>:</label>
                            <select class="form-control" id="nam">
                                <?php $dd = $DB->get_records('diemdanh'); $nammin=date('Y'); foreach ($dd as $val): ?>
                                <!-- $nammin lấy tất cả các năm trong DB từ năm bé nhất đến năm hiện tại-->
                                <?php if ($nammin>$val->year): ?>
                                    <?php $nammin=$val->year; ?>
                                <?php endif ?>
                                <?php endforeach ?>
                                <?php
                                for($i=$nammin; $i<=date("Y");$i++)
                                {
                                    ?>
                                    <option <?php echo date('Y')==$i ? "selected" : "" ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                } 
                                 ?>
                                
                            </select>
                        </div>
                        
                            <div class="col-md-3">
                                <?php 
                                    $get_st = get_sotiet(date('m'),date('Y'),$id);
                                 ?>
                                <label><?php print_r(get_string('numberofperiods')) ?>:</label>
                                <input type="text" id="sotiet" class="form-control" data-st="<?php echo get_sotiet(date('m'),date('Y'),$id)->id ?>" value="<?php echo !empty($get_st) ? $get_st->sotiet :  '' ?>">

                            </div>
                            <div class="col-md-3">
                                <?php $get_status = get_status(date('m'),date('Y'),$id);?>
                                <label>Hoàn thành:</label>
                                <input type="checkbox" name="status" id="status" value="1" <?= $get_status==1 ? 'checked' : '' ?> >
                            
                            </div>
                        
   
                    </div>
                    <div class="text-warning"> <h5 class="p-2 font-weight-bold"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php print_r(get_string('notice')) ?>: <?php print_r(get_string('notedd')) ?></h5></div>

                    <div class="table-responsive" data-pattern="priority-columns" id="return_thang">
                        <table id="tech-companies-1" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="align-middle" rowspan="3"><?php print_r(get_string('codestudent'))?></th>
                                    <th class="align-middle" rowspan="3"><?php print_r(get_string('namestudent'))?></th>
                                    <?php foreach($allday as $key=>$value) { ?>
                                        <th colspan="" rowspan="" headers="" scope=""> <?php  echo $value["ngay"] ?></th>
                                    <?php } ?>
                                    <th class="align-middle" rowspan="3"><?php print_r(get_string('total')) ?></th>
                                </tr>

                                <tr>

                                    <?php foreach($allday as $key=>$value) {?>
                                        <th class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
                                        if($value['thu']=='Sat'){echo 'bg-warning';} ?>" colspan="" rowspan="" headers="" scope=""> <?php  echo $value["thu"] ?></th>
                                    <?php } ?>

                                </tr>

                            </thead>
       
                            <tbody id="bgh">
                                <?php
                                    foreach ($list_members as $key => $val) {
                                        $user_dd = getDiemDanh($val->id,$id,date('m'));
                                ?>
                                <tr>
                                    <td><?php echo $val->code ?></td>
                                    <td><?php echo $val->lastname . ' '. $val->firstname ?></td>

                                    <?php foreach($allday as $key=>$value) {?>
                                        <td class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
                                                    if($value['thu']=='Sat'){echo 'bg-warning';} ?>">
                                            <?php if($value['thu']=='Sun'){echo '';} 
                                                else{ ?> 
                                                <input type="text" name="" value="<?php echo $val->id ?>" placeholder="" hidden="">
                                                <input class="kk" type="checkbox" name="user<?php echo $val->id ?>[]" value="<?php echo $value["ngay"] ?>"
                                                <?php 
                                                $day = $value["ngay"];
                                                $month= date('m');
                                                $year = date('Y');
                                                // var_dump($ss);
                                                foreach ($user_dd as $item) {
                                                    if ($item->day == $day && $item->month == $month && $item->year == $year) {
                                                        echo "checked='checked' data-dd=".$item->id;
                                                    }
                                                }
                                                 ?>
                                                >
                                            <?php  }?>
                                        </td>
                                    <?php } ?>

                                    <td>
                                        <?php echo (1-count($user_dd)/get_sotiet(date('m'),date('Y'),$id)->sotiet)*100 .'%' ?>
                                    </td>
                                 
                                    
                                </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    
                    </div>
                    <div class="form-group">
                        <a class="btn btn-success" id="save" href=""><?php print_string('save','admin') ?></a>
                        <a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                    </div>
                    <div id="kkk">
                        
                    </div>
                </div>
            </div>
           
            <!-- end content-->
     
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
// }else
    // echo displayJsAlert('Không có quyền truy cập', $CFG->wwwroot);
    echo $OUTPUT->footer();
?>
<script type="text/javascript">
    $('#save').on('click',function(){
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var status = $('#status').val();
        $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/sotiet.php?month="+month+"&year="+year+"&sotiet="+$('#sotiet').val()+"&status="+status+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>");
        alert('Lưu thành công!');
    });

    $('#status').change(function(){
        if ($(this).is(':checked')) {
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var status = $('#status').val();
            $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/test.php?month="+month+"&year="+year+"&sotiet="+$('#sotiet').val()+"&status="+status+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>");
            $("input[type='checkbox']").attr('disabled','disabled');
        }

    });
    <?php if ($get_status==1): ?>
        $("input[type='checkbox']").attr('disabled','disabled');
    <?php endif ?>
</script>
<script type="text/javascript">
    $(':checkbox').click(function(){
        if($(this).is(':checked')){
            var day     = $(this).val();
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var userid  = $(this).siblings().val();
            var y = $(this);
            $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/ajax.php?day="+day+"&month="+month+"&year="+year+"&userid="+userid+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>",function(data){
                y.attr('data-dd', data);
            });
        }
        else{
            var id_dd   = ($(this).attr('data-dd'));
            var day     = $(this).val();
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var userid =$(this).siblings().val();
            $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/ajax.php?day="+day+"&month="+month+"&year="+year+"&userid="+userid+"&id_dd="+id_dd+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>");
        }
    });
</script>
<script>
    $('#thang').on('change', function() {
        var thang = $(this).val();
        // var status = $(this).val();
        if (this.value) {
          $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/load_ajax.php?thang="+thang+"&nam="+$('#nam').val()+"&status="+status+"&id=<?php echo $id ?>",function(data){
            $("#return_thang").html(data);
          });
          $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/sotiet.php?month="+thang+"&year="+$('#nam').val()+"&status="+status+"&courseid=<?php echo $id ?>",function(sotiet){
            $("#sotiet").val(sotiet);
          });
          $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/test.php?month="+thang+"&year="+$('#nam').val()+"&courseid=<?php echo $id ?>",function(status){
            $('#kkk').html(status);
            $("#status").val(status);
            if (status == '1') {
                $('#status').attr('checked', 'checked');
            }else{
                $('#status').removeAttr('checked');
            }
          });
          
        }
    });
    $('#nam').on('change', function() {
    var nam = $(this).val() ;
    var status = $(this).val();
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/load_ajax.php?nam="+nam+"&thang="+$('#thang').val()+"&status="+status+"&id=<?php echo $id ?>",function(data){
        $("#return_thang").html(data);
      });
      $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/sotiet.php?month="+thang+"&status="+status+"&year="+$('#nam').val(),function(sotiet){
            $("#sotiet").val(sotiet);
          });
    }
  });

</script>