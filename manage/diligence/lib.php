<?php
function getAllDayByMonth($month, $year = null)
    {
    	$year = ($year) ? $year : date('Y');
    	$list = array();
		for($day = 1; $day <= 31; $day++)
		{
		    $time = mktime(12, 0, 0, $month, $day, $year);          
		    if (date('m', $time)==$month)       
		        //$list[]=date('Y-m-d-D', $time);
		        $list[] = [
		    		'thu' 	=> date('D', $time),
		    		'ngay' 	=> date('d', $time),
		    		'thang' => date('m', $time),
		    		'nam'	=> date('Y', $time)
		    	];
		}
		return $list;
    }

function getDiemDanh($userid,$courseid,$month,$year=null)
{
	global $DB;
	if(!empty($year)){
		return $DB->get_records('diemdanh', array('userid'=>$userid,'course_id'=>$courseid,'month'=>$month,'year'=>$year));
	}
	else{
		return $DB->get_records('diemdanh', array('userid'=>$userid,'course_id'=>$courseid,'month'=>$month));
	}
	
}

function get_id_schoolyear($id)
{
    global $DB;
    $sql = "SELECT
                groups.id_namhoc
            FROM
                course
                JOIN group_course ON group_course.course_id = course.id
                JOIN groups ON group_course.group_id = groups.id
            WHERE
                course.id = ".$id;
    return $DB->get_record_sql($sql);
}
function get_sotiet($month,$year,$courseid = null)
{
	global $DB;
	$sql ="SELECT DISTINCT sotiet,status,id FROM diemdanh WHERE sotiet IS NOT NULL AND month={$month} AND year={$year} AND course_id={$courseid}";
	return $DB->get_record_sql($sql);
}

function get_status($month,$year,$courseid = null)
{
	global $DB;
	$sql ="SELECT DISTINCT status,id FROM diemdanh WHERE status IS NOT NULL AND month={$month} AND year={$year} AND course_id={$courseid}";
	return $DB->get_record_sql($sql);
}


?>