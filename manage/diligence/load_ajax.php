<?php 
	require("../../config.php");
	global $CFG, $DB;
	require_once($CFG->dirroot . '/teams/lib.php');
	require("lib.php");
	
	
    $id = $_GET['id'];
	$list_members = get_members_in_team_second($id);
	// var_dump($list_members);
	
    // Tháng
	if (!empty($_GET['thang']) || !empty($_GET['nam'])) {
		$thang = $_GET['thang'];
        $nam    = $_GET['nam'];
		$allday = getAllDayByMonth($thang,$nam);
        $get_status = get_status($thang,$nam ,$id);
        // var_dump($allday);
		if($thang>0){ ?>
			<table id="tech-companies-1" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="align-middle" rowspan="3"><?php print_r(get_string('codestudent'))?></th>
                        <th class="align-middle" rowspan="3"><?php print_r(get_string('namestudent'))?></th>
                        <?php foreach($allday as $key=>$value) { ?>
                            <th colspan="" rowspan="" headers="" scope=""> <?php  echo $value["ngay"] ?></th>
                        <?php } ?>

                        <th rowspan="3"><?php print_r(get_string('total')) ?></th>
                        <!-- <th rowspan="3">Nhận xét</th> -->
                    </tr>

                    <tr>

                        <?php foreach($allday as $key=>$value) {?>
                            <th class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
                            if($value['thu']=='Sat'){echo 'bg-warning';} ?>" colspan="" rowspan="" headers="" scope=""> <?php  echo $value["thu"] ?></th>
                        <?php } ?>

                    </tr>

                </thead>

                <tbody id="page">
                    <?php
                        foreach ($list_members as $key => $val) {
                            $user_dd = getDiemDanh($val->id,$id,$thang,$nam);
                    ?>
                    <tr>
                        <td><?php echo $val->code ?></td>
                        <td><?php echo $val->lastname .' '.$val->firstname ?></td>

                        <?php foreach($allday as $key=>$value) {?>
                            <td class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
                                        if($value['thu']=='Sat'){echo 'bg-warning';} ?>">
                                <?php if($value['thu']=='Sun'){echo '';} 
                                else{ ?> 
                                    <input type="text" name="" value="<?php echo $val->id ?>" placeholder="" hidden="">
                                    <input class="kk" type="checkbox" name="user<?php echo $val->id ?>[]" value="<?php echo $value["ngay"] ?>"
                                    <?php 
                                    $ss = $value["ngay"].$thang.$nam;
                                    // var_dump($ss);
                                    foreach ($user_dd as $item) {
                                        if ($item->day.$item->month.$item->year == $ss) {
                                            echo "checked data-dd=".$item->id;
                                        }
                                        else{
                                            echo "";
                                        }
                                    }
                                     ?>
                                    >
                                <?php  }?>
                            </td>
                        <?php } ?>
                     
                        
                        
                        <td>
                            <?php echo (1-count($user_dd)/get_sotiet($thang,$nam,$id)->sotiet)*100 .'%' ?>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
	<?php	}
	}

?>


<script type="text/javascript">
    $('#save').on('click',function(){
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var status = $('#status').val();
        $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/sotiet.php?month="+month+"&year="+year+"&sotiet="+$('#sotiet').val()+"&status="+status+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>");
    });

    $('#status').change(function(){
        if ($(this).is(':checked')) {
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var status = $('#status').val();
            $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/test.php?month="+month+"&year="+year+"&sotiet="+$('#sotiet').val()+"&status="+status+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>");
            $("input[type='checkbox']").attr('disabled','disabled');
        }

    });
    <?php if ($get_status==1): ?>
        $("input[type='checkbox']").attr('disabled','disabled');
    <?php endif ?>
</script>
<script type="text/javascript">
    $(':checkbox').click(function(){
        if($(this).is(':checked')){
            var day     = $(this).val();
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var userid  = $(this).siblings().val();
            var y = $(this);
            $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/ajax.php?day="+day+"&month="+month+"&year="+year+"&userid="+userid+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>",function(data){
                y.attr('data-dd', data);
            });
        }
        else{
            var id_dd   = ($(this).attr('data-dd'));
            var day     = $(this).val();
            var month   = $('#thang').val();
            var year    = <?php echo date('Y') ?>;
            var userid =$(this).siblings().val();
            $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/ajax.php?day="+day+"&month="+month+"&year="+year+"&userid="+userid+"&id_dd="+id_dd+"&courseid=<?php echo $id ?>&schoolyearid=<?php echo $schoolyearid->id_namhoc ?>");
        }
    });
</script>
<script>
    $('#thang').on('change', function() {
        var thang = $(this).val();
        // var status = $(this).val();
        if (this.value) {
          $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/load_ajax.php?thang="+thang+"&nam="+$('#nam').val()+"&status="+status+"&id=<?php echo $id ?>",function(data){
            $("#return_thang").html(data);
          });
          $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/sotiet.php?month="+thang+"&year="+$('#nam').val()+"&status="+status+"&courseid=<?php echo $id ?>",function(sotiet){
            $("#sotiet").val(sotiet);
          });
          $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/test.php?month="+thang+"&year="+$('#nam').val()+"&courseid=<?php echo $id ?>",function(status){
            $('#kkk').html(status);
            $("#status").val(status);
            if (status == '1') {
                $('#status').attr('checked', 'checked');
            }else{
                $('#status').removeAttr('checked');
            }
          });
          
        }
    });
    $('#nam').on('change', function() {
    var nam = $(this).val() ;
    var status = $(this).val();
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/load_ajax.php?nam="+nam+"&thang="+$('#thang').val()+"&status="+status+"&id=<?php echo $id ?>",function(data){
        $("#return_thang").html(data);
      });
      $.get("<?php echo $CFG->wwwroot ?>/manage/diligence/sotiet.php?month="+thang+"&status="+status+"&year="+$('#nam').val(),function(sotiet){
            $("#sotiet").val(sotiet);
          });
    }
  });

</script>