<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;

require_once($CFG->dirroot . '/common/lib.php');

// Require Login.
require_login();

$PAGE->set_title(get_string('parent'));
$PAGE->set_heading(get_string('parent'));
echo $OUTPUT->header();


$PAGE->requires->js('/manage/manage.js');
$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/search.js');

// load user for admin level 1
if (!is_teacher()) {
    $sql = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
    $params['ownerid'] = $USER->id;
    $rows = $DB->get_records_sql($sql, $params);
    foreach ($rows as $row) {
        $id_list .= $row->user_id . ",";
    }
    $id_list = $id_list . "0";
    $sql2 = "SELECT user_id FROM map_user_admin WHERE owner_id in (" . $id_list . ")";
    $rows2 = $DB->get_records_sql($sql2);
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list = $id_list . $USER->id;
} else {
    $sql = "SELECT owner_id FROM map_user_admin where user_id = :userid";
    $params['userid'] = $USER->id;
    $rows = $DB->get_records_sql($sql, $params);
    foreach ($rows as $row) {
        $owner_id = $row->owner_id;
    }
    $sql2 = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
    $params['ownerid'] = $owner_id;
    $rows2 = $DB->get_records_sql($sql2, $params);
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
//    $id_list = $id_list."0";
    $id_list = $id_list . $owner_id;
}
//end fix
//List People
$usercount = get_parent();

// $sql = "SELECT * FROM user";
// $rows = $DB->get_records_sql($sql);
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '30', PARAM_INT); // how many per page
$userid      = optional_param('id', 0, PARAM_INT);
$action      = optional_param('action', '', PARAM_TEXT);

if($action=='deactive'){
   $DB->set_field('user', 'confirmed', 0, array('id' => $userid)); 
}
if($action=='active'){
   $DB->set_field('user', 'confirmed', 1, array('id' => $userid)); 
}
if($action=='delete'){
    $DB->delete_records('user',array('id' => $userid));
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/user.php");
}


$userid          = (is_numeric($userid) && $userid > 0) ? $userid : $USER->id;
$user = $DB->get_record('user', array('id' => $userid));

// $path->achievements  = $userid ? '/manage/people/achievements.php?id='.$userid:'/manage';
// $path->recent  = $userid ? '/manage/people/profile.php?id='.$userid:'/manage';
// $path->courses  = $userid ? '/manage/people/courses.php?id='.$userid:'/manage';
// $path->teams  = $userid ? '/manage/people/teams.php?id='.$userid:'/manage';


?>

<script src="<?php print new moodle_url('/manage/search.js'); ?>"></script>
<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mr_bottom15">
                        <div class="col-md-2">
                            <a href="<?php echo new moodle_url('/manage/parent/new.php'); ?>" class="btn btn-info"><?php echo get_string('add') ?></a>
                        </div>
                        <div class="col-md-9">
                            <form action="" method="get" accept-charset="utf-8" class="form-row">
                                <div class="col-4">
                                    <input type="text" class="form-control" placeholder="<?php echo get_string('search') ?>..." name="searchParent" id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                                </div>
                                <!-- <div class="col">
                                    <button type="submit" class="btn btn-info">Tìm kiếm</button>
                                </div> -->
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
                        <table id="tech-companies-1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th data-priority="6">Họ tên</th>
                                    <th data-priority="6">Email</th>
                                    <th data-priority="3">Số điện thoại</th>
                                    <th data-priority="6">Công ty</th>
                                    <th data-priority="6">Địa chỉ</th>
                                    <th data-priority="3">Trạng thái</th>
                                    <th class="text-right">Hành động</th>
                                </tr>
                            </thead>

                            <tbody id="page">
                                <?php foreach ($usercount as $parent) { ?>
                                <tr>
                                    <td><a href="<?php echo new moodle_url('/manage/people/profile.php', array('id' => $parent->id)); ?>" style="color: #000">
                                        <?php echo $parent->lastname.' '.$parent->firstname ?></a>
                                    </td>
                                    <td><?php echo $parent->email ?></td>
                                    <td><?php echo $parent->phone2 ?></td>
                                    <td><?php echo $parent->company ?></td>
                                    <td><?php echo $parent->address ?></td>

                                    <?php $active = $parent->confirmed ? 'Active' : 'Inactive'; ?>
                                    <td class="">
                                        <span class="badge label-table <?php echo $active == 'Active' ? 'badge-success' : 'badge-danger' ?>">
                                            <?php echo $active == 'Active' ? get_string('active') : get_string('inactive') ?>
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <a href="<?php print new moodle_url('/manage/parent/edit.php',array('parent_id'=>$parent->id)); ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="<?php echo new moodle_url('/manage/parent/profile.php', array('id' => $parent->id)); ?>" class="btn btn-warning "><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="<?php print new moodle_url('?action=delete',array('id'=>$parent->id)); ?>" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end content-->
     
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->

<?php
    echo $OUTPUT->footer();
?>