<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/dashboard.php'));
    $PAGE->set_button($buttons);
}

$PAGE->set_title(get_string('people'));
$PAGE->set_heading(get_string('people'));
//now the page contents
$PAGE->set_pagelayout(get_string('people'));
echo $OUTPUT->header();


$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '30', PARAM_INT); // how many per page
$userid      = optional_param('id', 0, PARAM_INT);
$action      = optional_param('action', '', PARAM_TEXT);

if($action=='deactive'){
   $DB->set_field('user', 'confirmed', 0, array('id' => $userid)); 
}
if($action=='active'){
   $DB->set_field('user', 'confirmed', 1, array('id' => $userid)); 
}


$userid			 = (is_numeric($userid) && $userid > 0) ? $userid : $USER->id;
$user = $DB->get_record('user', array('id' => $userid));

$path->achievements  = $userid ? '/manage/people/achievements.php?id='.$userid:'/manage';
$path->recent  = $userid ? '/manage/people/profile.php?id='.$userid:'/manage';
$path->courses  = $userid ? '/manage/people/courses.php?id='.$userid:'/manage';
$path->teams  = $userid ? '/manage/people/teams.php?id='.$userid:'/manage';
?>

<div class="row">
    <div class="col-md-9">
        <!-- Left Content -->
        <div class="card-box">
	        <div class="panel-head clearfix">
				<div class="media-box">
					<div class="img">
                        <?php 
                        //echo $OUTPUT->user_picture($user, array('size'=>100));
                        if($user->profile_img!='') { ?>
                            <img src="<?php echo $user->profile_img ?>" width="100" height="100"/>
                        <?php } ?>                           
                    </div>
					<div class="blurb">
						<div class="float-right">
							<div class="subtitle italic"><?php print_r(get_string('lastloginwas'))." ";print time_ago($user->lastlogin);?></div>
							<div class="align-right">
							<span class="box-tag <?php echo ($user->confirmed ? 'box-tag-green':'box-tag-grey')?> " title=""><span><?php echo ($user->confirmed ? get_string('active') : get_string('inactive'))?></span></span>
							<span class="box-tag box-tag-orange " title=""><span><?php print people_get_roles($user->id);?></span></span>
							</div>
						</div>
						<h3><?php echo $user->lastname.' '.$user->firstname?></h3>
						<div class="subtitle">
							<?php print obfuscate_mailto($user->email, '');?>
						</div>
					</div>
				</div>
			</div>

            <div class='wrapper-inner'>
                <div class="grid-tabs">
                    <ul class="nav nav-pills navtab-bg nav-justified pull-in">
                        <li class="nav-item">
                            <a href="<?php print new moodle_url($path->recent); ?>"  class="nav-link active">
                                <i class="fi-monitor mr-2"></i> <?php print_r(get_string('pluginname','block_recent_activity'))?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php print new moodle_url($path->achievements); ?>" class="nav-link">
                                <i class="fi-head mr-2"></i> <?php print_r(get_string('achievements')) ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php print new moodle_url($path->courses); ?>"  class="nav-link">
                                <i class="fi-mail mr-2"></i> <?php print_r(get_string('courses')) ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php print new moodle_url($path->teams); ?>"  class="nav-link">
                                <i class="fi-cog mr-2"></i> <?php print_r(get_string('teams')) ?>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="item-page-list">
                    <div id ='dashboard'>
                        <?php
                            echo manage_print_log_of_selected_person($order="l.time ASC", $page, $perpage,"profile.php?", $modname="", $modid=0, $modaction="", $groupid=0,$userid);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Left Content -->

    <!-- Right Block -->
    <div class="col-md-3">
        <div class="card-box">
            <div class="side-panel">
               <div class="action-buttons">
    				        <ul>
               <?php if($user->confirmed==1){ ?> 
               <li>
                 <a alt="" onclick="" href="<?php echo $CFG->wwwroot ?>/manage/people/profile.php?action=deactive&id=<?php echo $userid ?>" class="big-button drop" id="changeStatus"><span class="left"><span class="mid"><span class="right"><span class="icon_cross"><?php print_r(get_string('deactivate')) ?></span></span></span></span></a>
                </li>
                <?php } else {?>
                
                <li>
                  <a alt="" onclick="" href="<?php echo $CFG->wwwroot ?>/manage/people/profile.php?action=active&id=<?php echo $userid ?>" class="big-button drop" id="changeStatus"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print_string('activate') ?></span></span></span></span></a>
                </li>
                <?php } ?>

                <li><a href="<?php print new moodle_url('/manage/people/edit.php',array('user_id'=>$userid)); ?>"><?php print_r(get_string('edit')); print_r(get_string('sprofile')." "); print $user->lastname.' '.$user->firstname;?></a></li>

                <li><a href="<?php print new moodle_url('/manage/people/password.php',array('user_id'=>$userid)); ?>"><?php print_r(get_string('resetpass')) ?></a></li>
    				            <?php if(!$user->lastlogin):?><li><div class="tip"><?php print_string('thispersonhasnever') ?></div></li><?php endif;?>
                      <li><a class="warning resend-email" href="<?php print new moodle_url('/manage/people/password.php',array('user_id'=>$userid)); ?>">? <?php print_r(get_string('sendloginemail')) ?></a></li>

    				        </ul>
    				    </div>
            </div>

            <div class="side-panel">
                <h3><?php print_r(get_string('contactdetail')) ?></h3>
                <table class="side-form">
                    <tbody>
                        <tr>
                            <th><?php print_r(get_string('username')) ?></th>
                            <td><?php echo $user->username; ?></td>
                        </tr> 
                        <tr>
                            <th>Email :</th>
                            <td>
                                <a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a>
                                <div class="tip italic">- <?php print_r(get_string('emailnotification')) ?></div>
                            </td>
                        </tr> 
                        <tr>
                          <th><?php print_r(get_string('phone')) ?> :</th>
                          <td><?php echo $user->phone1; ?></td>
                        </tr> 
                        <tr>
                            <th><?php print_r(get_string('phone2')) ?> :</th>
                            <td></td>
                        </tr> 
                        <tr>
                            <th>Skype :</th><td><?php echo $user->skype; ?> </td>
                        </tr> 
                        <tr>
                            <th>Twitter :</th><td><?php echo $user->twitter; ?></td>
                        </tr>      
                        <tr>
                            <th>Id :</th><td><?php echo $user->id; ?></td>
                        </tr>                    
                    </tbody>
                </table>
                <hr>
                <table class="side-form">
                    <tbody>
                        <tr>
                            <th><?php print_r(get_string('address')) ?></th>
                            <td><?php echo $user->address; ?></td>
                        </tr>
                        <tr>
                            <th><?php print_r(get_string('timezone')) ?></th><td><?php echo $user->timezone; ?></td>
                        </tr> 
                    </tbody>
                </table>                
            </div>
        </div>
    </div>  <!-- End Right -->
</div>

<?php
	echo $OUTPUT->footer();
?>
