<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_login(0, false);
$PAGE->set_title(get_string('newprofile'));
$PAGE->set_heading(get_string('newprofile'));
global $USER;
$user_id = optional_param('user_id', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
echo $OUTPUT->header();
$user = get_user_from_id($user_id);

if ($action == 'add_profile') {
    $firstname = optional_param('firstname', '', PARAM_TEXT);
    $state = optional_param('state', '', PARAM_TEXT);
    $postcode = optional_param('postcode', '', PARAM_TEXT);
    $website = optional_param('website', '', PARAM_TEXT);
    $lastname = optional_param('lastname', '', PARAM_TEXT);
    $username = optional_param('username', '', PARAM_TEXT);
    $password = optional_param('password', '', PARAM_TEXT);
    $address = optional_param('address', '', PARAM_TEXT);
    $city = optional_param('city', '', PARAM_TEXT);
    $zip = optional_param('zip', '', PARAM_TEXT);
    $country = optional_param('country', '', PARAM_TEXT);
    $timezone = optional_param('timezone', '', PARAM_TEXT);
    $title = optional_param('title', '', PARAM_TEXT);
    $company = optional_param('company', '', PARAM_TEXT);
    $email = $username;
    $phone1 = optional_param('phone1', '', PARAM_TEXT);
    $phone2 = optional_param('phone2', '', PARAM_TEXT);
    $skype = optional_param('skype', '', PARAM_TEXT);
    $twitter = optional_param('twitter', '', PARAM_TEXT);
    $website = optional_param('website', '', PARAM_TEXT);
    $role_id = optional_param('access_level', '', PARAM_TEXT);
    $send_notif = optional_param('send_notif', 1, PARAM_TEXT);
    $parentId = 0;
    switch ($role_id) {
        case 'admin':
            $role_id = 3;
            break;
        case 'learner':
            $role_id = 5;
            break;
        case 'teacher':
            $role_id = 8;
            break;
        default:
            break;
    }
//    If is superadmin create admin level 1
    if ($USER->id == 2 && $role_id == 3) {
        $parentId = $USER->id;
    }
    $new_user_id = add_profile($firstname, $state, $postcode, $website, $lastname, $username, $password, $address, $city, $state, $zip, $country, $timezone, $title, $company, $email, $phone1, $phone2, $skype, $twitter, $website, $parentId);
    $context = get_context_instance(CONTEXT_SYSTEM);
    role_assign($role_id, $new_user_id, $context->id);
    $newPass = update_user_password($new_user_id);
    //    Add to map user
    $new_user_map_owner = add_map_user_admin($USER->id, $new_user_id);
    
    if ($send_notif == 1) {
        $mailto = $email;
        $invitationContenNoEmail = $emailContentNo;
        $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
        $invitationContenNoEmail = str_replace("{PASSWORD}", $newPass, $invitationContenNoEmail);
        $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
        sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
        $invitationContenNoEmail = $emailContentNo;
    }
    redirect($CFG->wwwroot . "/manage/people/profile.php?id=" . $new_user_id);
}

$countries = "SELECT * FROM apps_countries";
$row = $DB->get_records_sql($countries);
?>

<script type="text/javascript" src="js/validate_profile.js"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form action="<?php echo $CFG->wwwroot ?>/manage/people/new.php?action=add_profile" onsubmit="return validate();" id="userForm" method="post">

              <input id="user_id" name="user_id" type="hidden" value="<?php echo $user_id; ?>">

              <div class="form-group row">
                  <label class="col-md-2 col-form-label"><?php print_r(get_string('firstname')) ?></label>
                  <div class="col-md-10">
                    <div id="erFirst" class="alert alert-danger" role="alert" style="display: none;">
                      <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button> -->
                        <?php print_string('enter_firstname') ?>
                    </div>
                    <input class="form-control" id="firstname" name="firstname" type="text" value="">

                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('lastname')) ?></label>
                
                <div class="col-md-10">
                  <div id="erLast" class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php print_string('enter_lastname') ?>   
                  </div>
                  <input class="form-control" id="lastname" name="lastname" type="text" value="">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('username')) ?></label>
                
                <div class="col-md-10">
                  <div id="erUserName" class="alert alert-danger" style="display: none;"><?php print_string('enter_emailid') ?></div>
                  <input class="form-control" id="username" name="username" type="emailHelp" value="">
                  <span id="emailHelp" class="form-text text-muted"><?php print_r(get_string('mostpeopleuse')) ?></span>
                </div>
              </div>

              <div class="form-group row" hidden="hidden">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('accesslevel')) ?></label>
                <div class="col-md-10">
                  <?php if (!is_teacher()): ?> 
                  <select name="access_level" id="access_level" class="form-control">
                    <option value="parent"></option>
                  </select>
                  <?php endif; ?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Thành phố <span style="color:red">*</span></label>
                <div class="col-md-10">
                  <select name="city" id="tinhThanhChange" class="form-control" required>
                    <option value="-1">---None---</option>
                    <?php 
                      foreach ($city as $val) {
                        echo '<option value="'.$val->id.'">'.$val->name.'</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Huyện <span style="color:red">*</span></label>
                <div class="col-md-10">
                  <select name="state" id="quanHuyenChange" class="form-control" required>
                    <option value="-1">---None---</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Xã <span style="color:red">*</span></label>
                <div class="col-md-10">
                  <select name="xaphuong" id="xaPhuongChange" class="form-control" required>
                    <option value="-1">---None---</option>
                  </select>
                </div>
              </div>


              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('address')) ?></label>
                <div class="col-md-10">
                  <input class="form-control" type="text" name="address" placeholder="Nhập địa chỉ...">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('zip')) ?></label>
                <div class="col-md-10">
                  <input class="form-control" id="postcode" name="postcode" type="text" value="">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('country')) ?></label>
                  <div class="col-md-10">
                      <select name="country" id="country" class="form-control">

                        <?php foreach ($row as  $country) { ?>
                          <option value="<?php echo $country->country_code ?>">
                            <?php echo $country->country_name ?>
                          </option>
                        <?php } ?>
                        
                      </select>
                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('timezone')) ?></label>                    
                  <div class="col-md-10">
                      <select name="timezone" id="timezone" class="form-control">
                        <option value="Dateline Standard Time">(UTC-12:00) International Date Line West</option>
                        <option value="UTC-11">(UTC-11:00) Coordinated Universal Time-11</option>
                        <option value="Hawaiian Standard Time">(UTC-10:00) Hawaii</option>
                        <option value="Alaskan Standard Time">(UTC-09:00) Alaska</option>
                        <option value="Pacific Standard Time (Mexico)">(UTC-08:00) Baja California</option>
                        <option value="Pacific Standard Time">(UTC-08:00) Pacific Time (US &amp; Canada)</option>
                        <option value="US Mountain Standard Time">(UTC-07:00) Arizona</option>
                        <option value="Mountain Standard Time (Mexico)">(UTC-07:00) Chihuahua, La Paz, Mazatlan</option>
                        <option value="Mountain Standard Time">(UTC-07:00) Mountain Time (US &amp; Canada)</option>
                        <option value="Central America Standard Time">(UTC-06:00) Central America</option>
                        <option value="Central Standard Time">(UTC-06:00) Central Time (US &amp; Canada)</option>
                        <option value="Central Standard Time (Mexico)">(UTC-06:00) Guadalajara, Mexico City, Monterrey</option>
                        <option value="Canada Central Standard Time">(UTC-06:00) Saskatchewan</option>
                        <option value="SA Pacific Standard Time">(UTC-05:00) Bogota, Lima, Quito</option>
                        <option value="Eastern Standard Time">(UTC-05:00) Eastern Time (US &amp; Canada)</option>
                        <option value="US Eastern Standard Time">(UTC-05:00) Indiana (East)</option>
                        <option value="Venezuela Standard Time">(UTC-04:30) Caracas</option>
                        <option value="Paraguay Standard Time">(UTC-04:00) Asuncion</option>
                        <option value="Atlantic Standard Time">(UTC-04:00) Atlantic Time (Canada)</option>
                        <option value="Central Brazilian Standard Time">(UTC-04:00) Cuiaba</option>
                        <option value="SA Western Standard Time">(UTC-04:00) Georgetown, La Paz, Manaus, San Juan</option>
                        <option value="Pacific SA Standard Time">(UTC-04:00) Santiago</option>
                        <option value="Newfoundland Standard Time">(UTC-03:30) Newfoundland</option>
                        <option value="E. South America Standard Time">(UTC-03:00) Brasilia</option>
                        <option value="Argentina Standard Time">(UTC-03:00) Buenos Aires</option>
                        <option value="SA Eastern Standard Time">(UTC-03:00) Cayenne, Fortaleza</option>
                        <option value="Greenland Standard Time">(UTC-03:00) Greenland</option>
                        <option value="Montevideo Standard Time">(UTC-03:00) Montevideo</option>
                        <option value="Bahia Standard Time">(UTC-03:00) Salvador</option>
                        <option value="UTC-02">(UTC-02:00) Coordinated Universal Time-02</option>
                        <option value="Mid-Atlantic Standard Time">(UTC-02:00) Mid-Atlantic</option>
                        <option value="Azores Standard Time">(UTC-01:00) Azores</option>
                        <option value="Cape Verde Standard Time">(UTC-01:00) Cape Verde Is.</option>
                        <option value="Morocco Standard Time">(UTC) Casablanca</option>
                        <option selected="selected" value="UTC">(UTC) Coordinated Universal Time</option>
                        <option value="GMT Standard Time">(UTC) Dublin, Edinburgh, Lisbon, London</option>
                        <option value="Greenwich Standard Time">(UTC) Monrovia, Reykjavik</option>
                        <option value="W. Europe Standard Time">(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                        <option value="Central Europe Standard Time">(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                        <option value="Romance Standard Time">(UTC+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                        <option value="Central European Standard Time">(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                        <option value="W. Central Africa Standard Time">(UTC+01:00) West Central Africa</option>
                        <option value="Namibia Standard Time">(UTC+01:00) Windhoek</option>
                        <option value="Jordan Standard Time">(UTC+02:00) Amman</option>
                        <option value="GTB Standard Time">(UTC+02:00) Athens, Bucharest</option>
                        <option value="Middle East Standard Time">(UTC+02:00) Beirut</option>
                        <option value="Egypt Standard Time">(UTC+02:00) Cairo</option>
                        <option value="Syria Standard Time">(UTC+02:00) Damascus</option>
                        <option value="South Africa Standard Time">(UTC+02:00) Harare, Pretoria</option>
                        <option value="FLE Standard Time">(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                        <option value="Turkey Standard Time">(UTC+02:00) Istanbul</option>
                        <option value="Israel Standard Time">(UTC+02:00) Jerusalem</option>
                        <option value="E. Europe Standard Time">(UTC+02:00) Nicosia</option>
                        <option value="Arabic Standard Time">(UTC+03:00) Baghdad</option>
                        <option value="Kaliningrad Standard Time">(UTC+03:00) Kaliningrad, Minsk</option>
                        <option value="Arab Standard Time">(UTC+03:00) Kuwait, Riyadh</option>
                        <option value="E. Africa Standard Time">(UTC+03:00) Nairobi</option>
                        <option value="Iran Standard Time">(UTC+03:30) Tehran</option>
                        <option value="Arabian Standard Time">(UTC+04:00) Abu Dhabi, Muscat</option>
                        <option value="Azerbaijan Standard Time">(UTC+04:00) Baku</option>
                        <option value="Russian Standard Time">(UTC+04:00) Moscow, St. Petersburg, Volgograd</option>
                        <option value="Mauritius Standard Time">(UTC+04:00) Port Louis</option>
                        <option value="Georgian Standard Time">(UTC+04:00) Tbilisi</option>
                        <option value="Caucasus Standard Time">(UTC+04:00) Yerevan</option>
                        <option value="Afghanistan Standard Time">(UTC+04:30) Kabul</option>
                        <option value="Pakistan Standard Time">(UTC+05:00) Islamabad, Karachi</option>
                        <option value="West Asia Standard Time">(UTC+05:00) Tashkent</option>
                        <option value="India Standard Time">(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                        <option value="Sri Lanka Standard Time">(UTC+05:30) Sri Jayawardenepura</option>
                        <option value="Nepal Standard Time">(UTC+05:45) Kathmandu</option>
                        <option value="Central Asia Standard Time">(UTC+06:00) Astana</option>
                        <option value="Bangladesh Standard Time">(UTC+06:00) Dhaka</option>
                        <option value="Ekaterinburg Standard Time">(UTC+06:00) Ekaterinburg</option>
                        <option value="Myanmar Standard Time">(UTC+06:30) Yangon (Rangoon)</option>
                        <option value="SE Asia Standard Time">(UTC+07:00) Bangkok, Hanoi, Jakarta</option>
                        <option value="N. Central Asia Standard Time">(UTC+07:00) Novosibirsk</option>
                        <option value="China Standard Time">(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                        <option value="North Asia Standard Time">(UTC+08:00) Krasnoyarsk</option>
                        <option value="Singapore Standard Time">(UTC+08:00) Kuala Lumpur, Singapore</option>
                        <option value="W. Australia Standard Time">(UTC+08:00) Perth</option>
                        <option value="Taipei Standard Time">(UTC+08:00) Taipei</option>
                        <option value="Ulaanbaatar Standard Time">(UTC+08:00) Ulaanbaatar</option>
                        <option value="North Asia East Standard Time">(UTC+09:00) Irkutsk</option>
                        <option value="Tokyo Standard Time">(UTC+09:00) Osaka, Sapporo, Tokyo</option>
                        <option value="Korea Standard Time">(UTC+09:00) Seoul</option>
                        <option value="Cen. Australia Standard Time">(UTC+09:30) Adelaide</option>
                        <option value="AUS Central Standard Time">(UTC+09:30) Darwin</option>
                        <option value="E. Australia Standard Time">(UTC+10:00) Brisbane</option>
                        <option value="AUS Eastern Standard Time">(UTC+10:00) Canberra, Melbourne, Sydney</option>
                        <option value="West Pacific Standard Time">(UTC+10:00) Guam, Port Moresby</option>
                        <option value="Tasmania Standard Time">(UTC+10:00) Hobart</option>
                        <option value="Yakutsk Standard Time">(UTC+10:00) Yakutsk</option>
                        <option value="Central Pacific Standard Time">(UTC+11:00) Solomon Is., New Caledonia</option>
                        <option value="Vladivostok Standard Time">(UTC+11:00) Vladivostok</option>
                        <option value="New Zealand Standard Time">(UTC+12:00) Auckland, Wellington</option>
                        <option value="UTC+12">(UTC+12:00) Coordinated Universal Time+12</option>
                        <option value="Fiji Standard Time">(UTC+12:00) Fiji</option>
                        <option value="Magadan Standard Time">(UTC+12:00) Magadan</option>
                        <option value="Kamchatka Standard Time">(UTC+12:00) Petropavlovsk-Kamchatsky - Old</option>
                        <option value="Tonga Standard Time">(UTC+13:00) Nuku'alofa</option>
                        <option value="Samoa Standard Time">(UTC+13:00) Samoa</option>
                      </select>
                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('uploadlabel')) ?></label>
                  <div class="col-md-10">
                      <input class="form-control" id="title" name="title" type="text" value="">
                  </div>
              </div>
              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('company')) ?></label>
                  <div class="col-md-10">
                      <input class="form-control" id="company" name="company" type="text" value="">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-2 col-form-label"><?php print_r(get_string('workph')) ?></label>
                  <div class="col-md-10">
                      <input class="form-control" id="phone1" name="phone1" type="number" value="">
                  </div>
              </div>

              <div class="form-group row">
                  <label class="col-md-2 col-form-label"><?php print_r(get_string('mobileph')) ?></label>
                  <div class="col-md-10">
                      <input class="form-control" id="phone2" name="phone2" type="number" value="">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-2 col-form-label">Skype</label>
                  <div class="col-md-10">
                      <input class="form-control" id="skype" name="skype" type="text" value="">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-2 col-form-label">Twitter</label>
                  <div class="col-md-10">
                      <input class="form-control" id="twitter" name="twitter" type="text" value="">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-2 col-form-label">Website</label>
                  <div class="col-md-10">
                      <input class="form-control" id="website" name="website" type="url" value="">
                  </div>
              </div>

              <?php
              if (is_siteadmin()) {
                  ?>
                  <tr>
                      <td><?php print_string('trial') ?></td>
                      <td><input <?php echo $user->trial == 'false' ? "checked='checked'" : "" ?> id="notifications" name="trial" type="checkbox" value="false"></td>
                  </tr>
                  <?php
              }
              ?>

              <div class="checkbox checkbox-success row">
                <div class="col-md-10 offset-2">
                  <input id="checkbox3" type="checkbox" id="notifications" name="Emails">
                  <label for="checkbox3">
                      <?php print_r(get_string('sendemailnoti')) ?>
                  </label>
                </div>
              </div> 

              <hr>
              <div class="form-group mb-0 justify-content-end row">
                <div class="col-md-10">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                  <?php print_r(get_string('or')) ?> 
                  <a href="<?php echo $CFG->wwwroot . '/manage/parent' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('cancel')) ?>
                  </a>
                </div>
              </div>
        </form>   
      </div>
  </div>
</div>

<?php
    echo $OUTPUT->footer();
?>
