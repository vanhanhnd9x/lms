<?php

// Displays different views of the logs.

require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
// Require Login.
require_login();
$PAGE->set_title(get_string('dashboards'));
$PAGE->set_heading(get_string('dashboards'));
//now the page contents
$PAGE->set_pagelayout(get_string('dashboards'));
echo $OUTPUT->header();


$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '30', PARAM_INT); // how many per page

$toprow = array();
$toprow[] = new tabobject(get_string('recentactivity'), new moodle_url('/manage/dashboard.php'), get_string('recentactivity'));
$toprow[] = new tabobject(get_string('lastestnew'), new moodle_url('/manage/news.php'), get_string('lastestnew'));
$tabs = array($toprow);

print_tabs($tabs, get_string('lastestnew'));

echo $OUTPUT->footer();

exit;

