<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');

// Require Login.
require_login();
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/dashboard.php'));
    $PAGE->set_button($buttons);
}

$unique_login_values = prepare_data_for_chart(30,0);
$course_sales_values = prepare_data_for_chart(30,1);
$course_complete_values = prepare_data_for_chart(30,2);
$PAGE->requires->js('/manage/js/jquery.min.js');
$PAGE->requires->js('/manage/js/jquery.jqplot.min.js');            
$PAGE->requires->js('/manage/js/jqplot/jqplot.highlighter.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.cursor.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.dateAxisRenderer.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.enhancedLegendRenderer.js');
$PAGE->requires->css('/manage/css/jquery.jqplot.min.css');
if ($unique_login_values != "") {
    $PAGE->requires->js('/manage/js/js.php?unique_login_values="'.$unique_login_values.'"&course_sales_values="'.$course_sales_values.'"&course_complete_values="'.$course_complete_values.'"');
}  

$PAGE->set_title("Dashboard");
$PAGE->set_heading("Dashboard");
//now the page contents
$PAGE->set_pagelayout('Dashboard');
echo $OUTPUT->header();
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '30', PARAM_INT); // how many per page
?>

<div id="admin-fullscreen">

    <div id="admin-fullscreen-left" class="focus-panel dashboard"> 
        <!-- Left Content -->
        <div class="body">
            <div class="wrapper-inner">
                <div id="chart"></div>
                <div class="grid-tabs">
                    <ul>
                        <li><a href="<?php print new moodle_url('/manage/dashboard.php'); ?>" class="selected"><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                        <li><a href="<?php print new moodle_url('/manage/news/'); ?>"><?phpprint_r(get_string('pluginname','block_news_items'))?></a></li>         

                    </ul>
                    <div class="clearfix"></div>


                </div>

                <div class="item-page-list">
                    <div id ='dashboard'>
                        <?php 

    //                    $toprow = array();
    //                    $toprow[] = new tabobject('recentactivity', new moodle_url('/manage/dashboard.php'), 'Recent Activity');
    //                    $toprow[] = new tabobject('lastestnews', new moodle_url('/manage/news'), 'Latest News');
    //                    $tabs = array($toprow);
    //                    print_tabs($tabs, 'recentactivity');
                            echo manage_print_log($order="l.time ASC", $page, $perpage,
                    "dashboard.php?", $modname="", $modid=0, $modaction="", $groupid=0);
                        ?>
                    </div>

                </div>
            </div>
        </div> 
    </div><!-- Left Content -->
        <!-- Right Block -->
        <div id="admin-fullscreen-right">

            <div class="side-panel">
               
            </div>
        </div>  <!-- End Right -->

</div>      
<?php
	echo $OUTPUT->footer();
?>