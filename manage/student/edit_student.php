<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
global $USER;
require_once($CFG->dirroot . '/manage/lib.php');
$idnamhoc=get_id_school_year_now();
require_login();
$action = optional_param('action', '', PARAM_TEXT);
if($action=='view'){
  $PAGE->set_title(get_string('view_student'));
  $PAGE->set_heading(get_string('view_student'));
}else{
  $PAGE->set_title(get_string('edit_student'));
  $PAGE->set_heading(get_string('edit_student'));
}

echo $OUTPUT->header();

$id_edit=optional_param('id_edit', '', PARAM_TEXT);
$student=$DB->get_record('user', array(
  'id' => $id_edit
)); 
$idcourse=get_idcourse_by_id_user($id_edit);
$course=$DB->get_record('course', array(
  'id' => $idcourse
));
// $groupid=$DB->get_record('group_course', array(
//   'course_id' => $idcourse
// ));
$groupid=$DB->get_record('groups_members', array(
  'userid' => $id_edit
));
$group=$DB->get_record('groups', array(
  'id' => $groupid->groupid
));
$block_student=$DB->get_record('block_student', array(
  'id' => $group->id_khoi
));
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='student';
if($action=='view'){
  $name1='view_student';
}else{
  $name1='edit_student';
}
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

function show_history_student($userid){
  global $DB;
  $sql_history="
  SELECT DISTINCT `history_student`.`id`,`schools`.`name` as school_name , `block_student`.`name` as block_name, `groups`.`name` as groups_name  , `school_year`.`sy_start`, `school_year`.`sy_end`,`history_student`.`idschoolyear`,`school_year`.`id`
  FROM history_student
  JOIN `user` ON `user`.`id` =`history_student`.`iduser`
  JOIN `schools` ON `schools`.`id` = `history_student`.`idschool`
  JOIN `block_student` ON `block_student`.`id` = `history_student`.`idblock`
  JOIN `groups` ON `groups`.`id` = `history_student`.`idclass`
  -- JOIN `course` ON `course`.`id` = `history_student`.`idgroups`
  -- JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
  JOIN `school_year` ON `school_year`.`id` =`history_student`.`idschoolyear`
  WHERE `user`.`id`={$userid}   ORDER BY `school_year`.`sy_end` DESC";
  $history_student=$DB->get_records_sql($sql_history);
  ?>
  <div class="col-md-12" id="show_history_student">
      <table class="table table-bordered">
        <thead>
           <tr>
            <th scope="col">#</th>
            <th scope="col"><?php echo get_string('school_year'); ?></th>
            <th scope="col"><?php echo get_string('schools'); ?></th>
            <th scope="col"><?php echo get_string('block_student'); ?></th>
            <th scope="col"><?php echo get_string('class'); ?></th>
            <th scope="col"><?php echo get_string('classgroup'); ?></th>
          </tr>
        </thead>
        <tbody>
        <?php 
              if($history_student){
              $i=0;
              foreach ($history_student as  $value) {
                $i++;
                $course_name=show_name_course_by_cua2($userid,$value->idschoolyear);
                ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $value->sy_start.'-'.$value->sy_end; ?></td>
                     <td><?php echo $value->school_name; ?></td>
                    <td><?php echo $value->block_name; ?></td>
                    <td><?php echo $value->groups_name; ?></td>
                    <td><?php echo $course_name; ?></td>
                  </tr>
                <?php 
              }
              }
              ?>
      </tbody>
    </table>
  </div>
  <?php 
}
if ($action=='edit_student') {
  $lastname = optional_param('lastname', '', PARAM_TEXT);
  $firstname = optional_param('firstname', '', PARAM_TEXT);
  $confirmed = optional_param('confirmed', '', PARAM_TEXT);
  $birthday = optional_param('birthday', '', PARAM_TEXT);
  $email = optional_param('email', '', PARAM_TEXT);
  $phone1 = optional_param('phone1', '', PARAM_TEXT);
  $username = optional_param('username', '', PARAM_TEXT);
  $city = optional_param('city', '', PARAM_TEXT);
  $id_district = optional_param('id_district', '', PARAM_TEXT);
  $id_wards = optional_param('id_wards', '', PARAM_TEXT);
  $address = optional_param('address', '', PARAM_TEXT);
  $note_for_teacher = optional_param('note_for_teacher', '', PARAM_TEXT);
  $sex = optional_param('sex', '', PARAM_TEXT);
  $name_parent = optional_param('name_parent', '', PARAM_TEXT);
  $description = optional_param('description', '', PARAM_TEXT);
  $input_score = optional_param('input_score', '', PARAM_TEXT);
  $time_start = optional_param('time_start', '', PARAM_TEXT);
  $time_end = optional_param('time_end', '', PARAM_TEXT);
  // $time_start_int=strtotime($time_start);
  list($day1, $month1, $year1) = explode('/', $time_start);
  $time_start_int=mktime(0, 0, 0, $month1, $day1, $year1);
  // $time_end_int=strtotime($time_end);
  list($day, $month, $year) = explode('/', $time_end);
  $time_end_int=mktime(23, 59, 59, $month, $day, $year);
  $save=update_my_profile_3($id_edit,$firstname,$lastname,$email,$phone1,$address,$birthday,$sex,$name_parent,$description,$time_start,$time_end,$input_score,$time_start_int,$time_end_int,$note_for_teacher);
  if($save){
    echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/student/index.php");
  }else{
    echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/student/edit_student.php?id_edit=".$id_edit);
  }
}
?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">
          <form  action="" onsubmit="return check_thoi_gian_bt_kt()" method="post">
            <input type="text" name="action" value="edit_student" hidden="">
            <!-- <input type="text" name="user_id"  hidden="" value="<?php $student->id; ?>"> -->
            <div class="row">
              <div class="col-md-12">
                <div class="alert-info" id="show">
                  <h4><?php echo get_string('history_student'); ?> <i class="fa fa-chevron-down" aria-hidden="true"></i></h4>
                </div>
              </div>
              <?php echo show_history_student($id_edit); ?>
             <!--  <div class="col-md-6">
                <label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                <select name="schoolid" id="schoolid" class="form-control" required="">
                 <option value=""><?php echo get_string('schools'); ?></option>
                 <?php 
                 $schools = get_all_shool();
                 foreach ($schools as $key => $value) {
                        # code...
                  ?>
                  <option value="<?php echo $value->id; ?>" <?php if (!empty($block_student->schoolid)&&($block_student->schoolid==$value->id)) { echo'selected';} ?>><?php echo $value->name; ?></option>
                  <?php 
                }
                ?>
              </select>
            </div>
            <div class="col-md-6">
              <label class="control-label"><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
              <select name="block_student" id="block_student" class="form-control" required="">
               <option value=""><?php echo get_string('block_student'); ?></option>
             </select>
           </div>
           <div class="col-md-6">
            <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
            <select name="groupid" id="groupid" class="form-control" required="">
              <option value=""><?php echo get_string('class'); ?></option>
            </select>
          </div>
          <div class="col-md-6">
           <label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
           <select name="courseid" id="courseid" class="form-control" >
            <option value=""><?php echo get_string('classgroup'); ?></option>
          </select>
        </div> -->
        <div class="col-md-12">
          <div class="alert-info">
            <h4><?php echo get_string('info') ?></h4>
          </div>
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('lastname'); ?><span style="color:red">*</span></label>
          <input class="form-control" type="text" name="lastname" required placeholder="<?php print_string('lastname');?> " value="<?php echo $student->lastname; ?>">
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('firstname'); ?><span style="color:red">*</span></label>
          <input class="form-control" type="text" name="firstname" required placeholder="<?php print_string('firstname');?>" value="<?php echo $student->firstname; ?>">
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('codestudent'); ?></label>
          <input class="form-control" type="text" name="" required placeholder="<?php print_string('codestudent');?>" value="<?php echo $student->code; ?>" disabled>
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('birthday'); ?></label>
          <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->birthday; ?>">
          </div>
        </div>
        <div class="col-md-6">
         <label class="control-label"><?php echo get_string('sex'); ?></label>
         <select name="sex" id="" class="form-control">
          <option value="1"><?php echo get_string('women'); ?></option>
          <option value="2" <?php if ($student->sex==2) { echo 'selected';} ?>><?php echo get_string('man'); ?></option>
        </select>
      </div>
      <div class="col-md-6">
       <label class="control-label"><?php echo get_string('input_score'); ?></label>
       <input class="form-control" type="text" name="input_score" placeholder="<?php echo get_string('input_score'); ?>"  value="<?php echo $student->input_score; ?>">
     </div>
     <div class="col-md-6">
       <label class="control-label"><?php echo get_string('parent'); ?></label>
       <input class="form-control" type="text" name="name_parent" placeholder="<?php echo get_string('parent'); ?>"  value="<?php echo $student->name_parent; ?>">
     </div>
     <div class="col-md-6">
       <label class="control-label"><?php echo get_string('email_new'); ?></label>
       <input class="form-control" type="emial" name="email" placeholder="<?php print_string('email_new') ?>"  value="<?php echo $student->email; ?>" >
     </div>
     <div class="col-md-6">
      <label class="control-label"><?php echo get_string('phone'); ?></label>
      <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('phone')) ?>"  value="<?php echo $student->phone1; ?>" pattern="[0-9]{1,11}" title="<?php echo get_string('invalidphone') ?>">
      <!-- <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('phone')) ?>"  value="<?php echo $student->phone1; ?>" pattern="^(01[2689]|02[0-9]|09|08|03)[0-9]{8}"> -->
    </div>
  <div class="col-md-6">
     <label class="control-label"><?php echo get_string('home_address'); ?></label>
     <input class="form-control" type="text" name="address" placeholder="<?php print_r(get_string('home_address')) ?>" value="<?php echo $student->address; ?>">
   </div>
     <!-- <div class="col-md-6">
    <label class="control-label"><?php echo get_string('time_start'); ?></label>
    <input type="text" name="time_start" id="date_start" class=" datepicker form-control" placeholder="dd/mm/yyyy"  value="<?php echo $student->time_start; ?>">
  </div>
  <div class="col-md-6">
    <label class="control-label"><?php echo get_string('time_end'); ?></label>
    <input type="text" name="time_end" id="date_end" class="  datepicker form-control"  placeholder="dd/mm/yyyy" value="<?php echo $student->time_end; ?>">
  </div> -->
  <div class="col-md-6 form-group">
    <label class="control-label"><?php print_string('time_start');?></label>
      <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
        <div class="input-group-addon">
          <span class="glyphicon glyphicon-th"></span>
        </div>
        <input type="text" name="time_start" id="time_start" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->time_start; ?>">
      </div>
  </div>
  <div class="col-md-6 form-group">
    <label class="control-label"><?php print_string('time_end');?></label>
      <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
        <div class="input-group-addon">
          <span class="glyphicon glyphicon-th"></span>
        </div>
        <input type="text" name="time_end" id="time_end" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->time_end; ?>">
      </div>
  </div>
  
  <div class="col-6">
     <label class="control-label"><?php echo get_string('note'); ?></label>
      <textarea name="description" id="" cols="30" rows="10" class="form-control" value="<?php echo $student->description; ?>"><?php echo $student->description; ?></textarea>
  </div>
  <div class="col-md-6 form-group">
    <label class="control-label"><?php print_string('note_for_teacher');?></label>
    <textarea name="note_for_teacher" id="" cols="30" rows="10" class="form-control" value="<?php echo $student->note_for_teacher; ?>"><?php echo $student->note_for_teacher; ?></textarea>
  </div>
  <div class="col-md-12" style="margin-top: 10px;">
    <?php 
        if($action=='view'){
          ?>
           <a  href="<?php echo $CFG->wwwroot ?>/manage/student/index.php" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
          <?php 

        }else{
          ?>
          <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
          <?php print_r(get_string('or')) ?>
          <a  href="<?php echo $CFG->wwwroot ?>/manage/student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
          <?php 
        }
     ?>
    
  </div>
</div> 


</form>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<script>
  // Date picker only
  $('#birthday').datetimepicker({
    format: 'DD/MM/YYYY'
  });
  $('#time_start').datetimepicker({
    format: 'DD/MM/YYYY'
  }); 
   $('#time_end').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
<?php 
echo $OUTPUT->footer();

?>
<script type="text/javascript" src="js/validate_profile.js"></script>
<script>
  // ajax edit
  var school = $('#schoolid').find(":selected").val();
  if (school) {
    var blockedit = "<?php echo $block_student->id; ?>";
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    if(blockedit){
      var groupedit="<?php echo $groupid->groupid; ?>";
      // var courseedit = "<?php echo $course->id; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
        $("#groupid").html(data);
      });
    }
    if(groupedit){
      var courseedit="<?php echo $idcourse; ?>";
      if(courseedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
        $("#courseid").html(data);
      });
     }else{
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
        $("#courseid").html(data);
      });
    }
  }
}
  // ajax add
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
        $("#courseid").html(data);
      });
    }
  });
  $(document).ready(function() {
    $("#username").keydown(function() {
      var dInput = $('#username').val();
      if(dInput!=''){
        $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?username="+dInput,function(data){
          $("#alert").html(data);

        });
      }
    });
    $("#username").keyup(function() {
      var dInput = $('#username').val();
      if(dInput!=''){
        $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?username="+dInput,function(data){
          $("#alert").html(data);

        });
      }
    });
  });

</script>
<script>
  $("#show_history_student").hide();
  var dem=0;
  $("#show").click(function(){
    dem ++;
    if(dem%2==0){
      $("#show_history_student").hide();
    }else{
      $("#show_history_student").show();
    }
  });
</script>
<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css'>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script type="text/javascript">
  $("#date_start").datepicker({
        // startDate: "dateToday",
        // minDate: 0,
        minDate: new Date(25, 10 - 1,2000 ),
        maxDate: "",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_end").datepicker("option","minDate", selected)
        },
        disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
  $("#date_end").datepicker({ 
    minDate: 0,
    maxDate:"",
    numberOfMonths: 1,
    onSelect: function(selected) {
      $("#date_start").datepicker("option","maxDate", selected)
    },
    disableTouchKeyboard: true,
    Readonly: true
  }).attr("readonly", "readonly");
  jQuery(function($){
    $.datepicker.regional['vi'] = {
      closeText: 'Đóng',
      prevText: '<Trước',
      nextText: 'Tiếp>',
      currentText: 'Hôm nay',
      monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
      'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
      monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
      'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
      dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
      dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      weekHeader: 'Tu',
      dateFormat: 'dd/mm/yy',
      firstDay: 0,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''};
      $.datepicker.setDefaults($.datepicker.regional['vi']);
    });
  </script>
     <script>
    function check_thoi_gian_bt_kt(){
      var time_start = document.getElementById('time_start').value;
      var time_end = document.getElementById('time_end').value;
      var birthday = document.getElementById('birthday').value;
      var d = new Date();
      // var strDate =(d.getDate()-1)+""+(d.getMonth()+1)+""+d.getFullYear();
      
      if((time_start!='')&&(time_end!='')){
        time_start=time_start.replace('/','');
        time_start=time_start.replace('/','');
        time_end=time_end.replace('/','');
        time_end=time_end.replace('/','');
        if(time_end <= time_start){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
      }
      if(birthday!=''){
        var res = birthday.split("/", 3);
        if(d.getFullYear()<res[2]){
          alert('Ngày sinh phải nhỏ hơn thời gian hiện tại');
          return false;
        }
        if(d.getFullYear()==res[2]){
          if((d.getMonth()+1)<res[1]){
            alert('Ngày sinh phải nhỏ hơn thời gian hiện tại');
            return false;
          }
          if((d.getMonth()+1)==res[1]){
            if((d.getDate()-1)<=res[0]){
              alert('Ngày sinh phải nhỏ hơn thời gian hiện tại');
              return false;
            }
          }
        }
      }
      
    }
  </script>
  <!-- Propeller Bootstrap datetimepicker -->

