<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
global $USER;

require_login(); 
$PAGE->set_title(get_string('add_student'));
$PAGE->set_heading(get_string('add_student'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='student';
$name1='add_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
function update_code_student($number){
 global $DB;
 $record = new stdClass();
 $record->id = 1;
 $record->number = $number;
 $record->time_update =time();
 return $DB->update_record('code_student', $record, false);
}
function get_number_code_student(){
  global $CFG, $DB;
  $number_code=$DB->get_record('code_student', array(
    'id' => '1'
  ));
  return $number_code->number;
}
$lay_code_student=$DB->get_record('code_student', array(
  'id' => '1'
));
$numer=get_number_code_student();
function sinh_code($number){
  $key=strlen($number);
  switch ($key) {
    case 1:  $code_number='000000'.$number; return $code_number; break;
    case 2:  $code_number='00000'.$number; return $code_number; break;
    case 3:  $code_number='0000'.$number; return $code_number; break;
    case 4:  $code_number='000'.$number; return $code_number; break;
    case 5:  $code_number='00'.$number; return $code_number; break;
    case 6:  $code_number='0'.$number; return $code_number; break;
    case 7:  $code_number=$number; return $code_number; break;
  }
}

$code_student=sinh_code($numer+1);
if ($action=='add_student') {
  # code...
  $lastname = optional_param('lastname', '', PARAM_TEXT);
  $firstname = optional_param('firstname', '', PARAM_TEXT);
  $confirmed = 1;
  $birthday = optional_param('birthday', '', PARAM_TEXT);
  $email = optional_param('email', '', PARAM_TEXT);
  $phone1 = optional_param('phone1', '', PARAM_TEXT);
  
  $city = optional_param('city', '', PARAM_TEXT);
  $id_district = optional_param('id_district', '', PARAM_TEXT);
  $id_wards = optional_param('id_wards', '', PARAM_TEXT);
  $address = optional_param('address', '', PARAM_TEXT);
  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $block_student = optional_param('block_student', '', PARAM_TEXT);
  $schoolid = optional_param('schoolid', '', PARAM_TEXT);
  $groupid = optional_param('groupid', '', PARAM_TEXT);

  $sex = optional_param('sex', '', PARAM_TEXT);
  $name_parent = optional_param('name_parent', '', PARAM_TEXT);
  $description = optional_param('description', '', PARAM_TEXT);
  $note_for_teacher = optional_param('note_for_teacher', '', PARAM_TEXT);

  $time_start = optional_param('time_start', '', PARAM_TEXT);
  // $time_start_int=strtotime($time_start);
  $time_end = optional_param('time_end', '', PARAM_TEXT);
  $yearh = optional_param('yearh', '', PARAM_TEXT);
  // $time_end_int=strtotime($time_end);

  list($day1, $month1, $year1) = explode('/', $time_start);
  $time_start_int=mktime(0, 0, 0, $month1, $day1, $year1);
  
  list($day, $month, $year) = explode('/', $time_end);
  $time_end_int=mktime(23, 59, 59, $month, $day, $year);
  $input_score = optional_param('input_score', '', PARAM_TEXT);
  // $code=date('Y').''.$code_student;

  $code=$yearh.''.$code_student;

  $school_year= get_sy_groups($groupid)->id_namhoc;

  $username = $code;
  if(!empty($phone1)){
      $password = $phone1;
  }else{
    $password = '0123456789';
  }
  // $password = optional_param('password', '', PARAM_TEXT);

  $role_id=5;
  if($lay_code_student->time_update<time()){
    
   $new_user_id=add_profile_2($firstname,$lastname,$email,$phone1,$username,$password,$confirmed,$address,$birthday,$sex,$name_parent,$description,$time_start,$time_end,$input_score,$time_start_int,$time_end_int,$code,$note_for_teacher);
   insert_history_student($new_user_id,$schoolid,$block_student,$groupid,$courseid,$school_year);
   if ($new_user_id) { 
    // update code học sinh
    update_code_student($numer+1);
  // gan luat cho hoc sinh
    $context = get_context_instance(CONTEXT_SYSTEM);
    role_assign($role_id, $new_user_id, $context->id);
    // add học sinh vào lớp
    if(!empty($groupid)){
     insert_member_to_group_2($new_user_id, $groupid);
   }
   // add học sinh vào nhóm lớp
   if (!empty($courseid)) {
     assign_user_to_course_2($new_user_id, $courseid);
   }
   add_logs('student','add','/manage/student/edit_student.php?id_edit='.$new_user_id->id,$lastname.' '.$firstname); 
   echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/student/index.php");
    # code...
 }else{
   echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/student/add_student.php");
 }
}else{
  echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/student/add_student.php");
}

}


?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">
          <form  action="<?php echo $CFG->wwwroot ?>/manage/student/add_student.php"  method="post" onsubmit="return check_thoi_gian_bt_kt()">
            <input type="text" name="action" value="add_student" hidden="">
            <div class="row">
              <div class="col-md-6 form-group">
                <label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                <select name="schoolid" id="schoolid" class="form-control" required="">
                 <option value=""><?php echo get_string('schools'); ?></option>
                 <?php 
                 $schools = get_all_shool();
                 foreach ($schools as $key => $value) {
                        # code...
                  ?>
                  <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                  <?php 
                }
                ?>
              </select>
            </div>
            <div class="col-md-6 form-group">
             <label class="control-label"><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
             <select name="block_student" id="block_student" class="form-control" required="">
               <option value=""><?php echo get_string('block_student'); ?></option>
             </select>
           </div>
           <div class="col-md-6 form-group">
             <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
             <select name="groupid" id="groupid" class="form-control" required="">
              <option value=""><?php echo get_string('class'); ?></option>
            </select>
          </div>
          <div class="col-md-6 form-group">
            <label class="control-label"><?php echo get_string('classgroup'); ?></label>
            <select name="courseid" id="courseid" class="form-control" >
              <option value=""><?php echo get_string('classgroup'); ?></option>
            </select>
          </div>
          <div class="col-md-6 form-group">
            <label class="control-label"><?php print_string('lastname');?><span style="color:red">*</span></label>
            <input class="form-control" type="text" name="lastname" required placeholder="<?php print_string('lastname');?> ">
          </div>
          <div class="col-md-6 form-group">
            <label class="control-label"><?php print_string('firstname');?><span style="color:red">*</span></label>
            <input class="form-control" type="text" name="firstname" required placeholder="<?php print_string('firstname');?>">
          </div>
          <div class="col-md-6 form-group">
            <label class="control-label"><?php print_string('codestudent');?><span style="color:red">*</span></label>
            <div class="row">
              <div class="col-md-6 form-group">
                <input class="form-control" type="number" name="yearh" required value="<?php echo date('Y'); ?>" style="width: " >
              </div>
              <div class="col-md-6 form-group">
                <input class="form-control" type="text" value="<?php echo $code_student; ?>" disabled="">
              </div>
            </div>
          </div>
          <div class="col-md-6 form-group">
           <label class="control-label"><?php print_string('birthday');?></label>
           <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->birthday; ?>">
          </div>
        </div>
        <div class="col-md-6 form-group">
         <label class="control-label"><?php print_string('sex');?></label>
         <select name="sex" id="" class="form-control">
          <option value="1"><?php echo get_string('women'); ?></option>
          <option value="2"><?php echo get_string('man'); ?></option>
        </select>
      </div>
      <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('input_score');?></label>
       <input class="form-control" type="number" name="input_score" placeholder="<?php echo get_string('input_score'); ?>" min="0" >
     </div>
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('parent');?></label>
       <input class="form-control" type="text" name="name_parent" placeholder="<?php echo get_string('parent'); ?>" >
     </div>
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('email_new');?></label>
       <input class="form-control" type="email" name="email" placeholder="<?php print_string('email_new') ?>"  >
     </div>
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('phone');?></label>
       <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('phone')) ?>" pattern="[0-9]{1,11}" title="<?php echo get_string('invalidphone') ?>" >
       <!-- <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('phone')) ?>"  pattern="^(01[2689]|02[0-9]|09|08|03)[0-9]{8}"> -->
     </div>
     <!-- <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('username');?><span style="color:red">*</span></label>
       <input class="form-control" type="text" name="username" id="username" placeholder="<?php print_string('username') ?>" required >
       <div id="alert" >

       </div>
     </div>
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('password');?><span style="color:red">*</span></label>
       <input class="form-control" type="password" name="password" placeholder="<?php print_string('password') ?>" required>
     </div> -->
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('home_address');?></label>
       <input class="form-control" type="text" name="address" placeholder="<?php print_r(get_string('home_address')) ?>">
     </div>
      <div class="col-md-6 form-group">
        <label class="control-label"><?php print_string('time_start');?></label>
          <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="time_start" id="time_start" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->time_start; ?>">
          </div>
      </div>
      <div class="col-md-6 form-group">
        <label class="control-label"><?php print_string('time_end');?></label>
          <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="time_end" id="time_end" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->time_end; ?>">
          </div>
      </div>
     <!-- <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('time_start');?></label>
       <input type="text" name="time_start" id="date_start" class=" datepicker form-control"  placeholder="dd/mm/yyyy">
     </div>
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('time_end');?></label>
       <input type="text" name="time_end" id="date_end" class="  datepicker form-control"  placeholder="dd/mm/yyyy">
     </div> -->
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('note');?></label>
       <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
     </div>
     <div class="col-md-6 form-group">
       <label class="control-label"><?php print_string('note_for_teacher');?></label>
       <textarea name="note_for_teacher" id="" cols="30" rows="10" class="form-control"></textarea>
     </div>
     <div ></div>
     <div class="col-md-12" style="margin-top: 10px;">
      <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
      <?php print_r(get_string('or')) ?>
      <a href="<?php echo $CFG->wwwroot ?>/manage/student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
    </div>
  </div>
</form>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<?php 
echo $OUTPUT->footer();

?>
<script>
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?block_student="+block,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?groupid="+groupid,function(data){
        $("#courseid").html(data);
      });
    }
  });
  $('#courseid').on('change', function() {
    var courseid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?courseid="+courseid,function(data){
        $("#studentid").html(data);
      });
    }
  });
</script>
<script>
  $(document).ready(function() {
    $("#username").keydown(function() {
      var dInput = $('#username').val();
      if(dInput!=''){
        $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?username="+dInput,function(data){
          $("#alert").html(data);

        });
      }
    });
    $("#username").keyup(function() {
      var dInput = $('#username').val();
      if(dInput!=''){
        $.get("<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php?username="+dInput,function(data){
          $("#alert").html(data);

        });
      }
    });
  });

</script>
<script>
  // Date picker only
  // $('#date_end').datetimepicker({
  //   format: 'DD-MM-YYYY'
  // });
  // $('#date_start').datetimepicker({
  //   format: 'DD-MM-YYYY'
  // });
  $('#birthday').datetimepicker({
    format: 'DD/MM/YYYY'
  });
   $('#time_start').datetimepicker({
    format: 'DD/MM/YYYY'
  }); 
   $('#time_end').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
<!-- js css checkin checkout -->
<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css'>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script type="text/javascript">
  $("#date_start").datepicker({
        // startDate: "dateToday",
        // minDate: 0,
        minDate: new Date(25, 10 - 1,2000 ),
        maxDate: "",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_end").datepicker("option","minDate", selected)
        },
        disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
  $("#date_end").datepicker({ 
    minDate: 0,
    maxDate:"",
    numberOfMonths: 1,
    onSelect: function(selected) {
      $("#date_start").datepicker("option","maxDate", selected)
    },
    disableTouchKeyboard: true,
    Readonly: true
  }).attr("readonly", "readonly");
  jQuery(function($){
    $.datepicker.regional['vi'] = {
      closeText: 'Đóng',
      prevText: '<Trước',
      nextText: 'Tiếp>',
      currentText: 'Hôm nay',
      monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
      'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
      monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
      'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
      dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
      dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      weekHeader: 'Tu',
      dateFormat: 'dd/mm/yy',
      firstDay: 0,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''};
      $.datepicker.setDefaults($.datepicker.regional['vi']);
    });
  </script>
  <script>
    function check_thoi_gian_bt_kt(){
      var time_start = document.getElementById('time_start').value;
      var time_end = document.getElementById('time_end').value;
      var birthday = document.getElementById('birthday').value;
      var d = new Date();
      // var strDate =(d.getDate()-1)+""+(d.getMonth()+1)+""+d.getFullYear();
      
      if((time_start!='')&&(time_end!='')){
        time_start=time_start.replace('/','');
        time_start=time_start.replace('/','');
        time_end=time_end.replace('/','');
        time_end=time_end.replace('/','');
        if(time_end <= time_start){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
      }
      if(birthday!=''){
        var res = birthday.split("/", 3);
        if(d.getFullYear()<res[2]){
          alert('Ngày sinh phải nhỏ hơn thời gian hiện tại');
          return false;
        }
        if(d.getFullYear()==res[2]){
          if((d.getMonth()+1)<res[1]){
            alert('Ngày sinh phải nhỏ hơn thời gian hiện tại');
            return false;
          }
          if((d.getMonth()+1)==res[1]){
            if((d.getDate()-1)<=res[0]){
              alert('Ngày sinh phải nhỏ hơn thời gian hiện tại');
              return false;
            }
          }
        }
      }
      
    }
  </script>