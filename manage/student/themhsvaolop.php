<?php
require("../../config.php");
require("../../grade/lib.php");
require_once('../../user/lib.php');
require_once('../../course/lib.php');
global $CFG;
global $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
$PAGE->set_title('Thêm học sinh vào lớp');
echo $OUTPUT->header();
require_login(0, false);


function get_all_student_not_in_course($courseid,$page,$number,$name){
    global $DB;
   
    if (!empty($name)) {
        if ($page>1) {
             $start=(($page-1)*$number);
        }else{
            $start=1;
        }
       $sql="
       SELECT DISTINCT  `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`role_assignments`.`roleid`
       FROM `user` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=5
       AND `user`.`id`  NOT IN (SELECT `user`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` WHERE `course`.`id`={$courseid})
       AND `user`.`firstname`='{$name}'
       LIMIT {$start},{$number}
       ";
       $data=$DB->get_records_sql($sql);
       $sql_total="SELECT COUNT( DISTINCT `user`.`id`) as total 
       FROM `user` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=5
       AND `user`.`id`  NOT IN (SELECT `user`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` WHERE `course`.`id`={$courseid})
       AND `user`.`firstname`='{$name}'
       ";
       $total_row=$DB->get_records_sql($sql_total);

       foreach ($total_row as $key => $value) {
    # code...
        $total_page=ceil((int)$key / (int)$number);
        break;
    }
    $kq=array(
        'data'=>$data,
        'total_page'=>$total_page 
    );
    return $kq;
}else{
    if ($page>1) {
        $start=(($page-1)*$number);
    }else{
        $start=1;
    }
    $sql="
    SELECT DISTINCT  `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`role_assignments`.`roleid`
    FROM `user` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=5
    AND `user`.`id`  NOT IN (SELECT `user`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` WHERE `course`.`id`={$courseid})
     LIMIT {$start},{$number}
    ";
    $data=$DB->get_records_sql($sql);
    $sql_total="SELECT COUNT( DISTINCT `user`.`id`) as total 
    FROM `user` JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=5
    AND `user`.`id`  NOT IN (SELECT `user`.`id` FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `course` ON `enrol`.`courseid`=`course`.`id` WHERE `course`.`id`={$courseid})
    ";
    $total_row=$DB->get_records_sql($sql_total);

    foreach ($total_row as $key => $value) {
    # code...
        $total_page=ceil((int)$key / (int)$number);
        break;
    }
    $kq=array(
        'data'=>$data,
        'total_page'=>$total_page 
    );
    return $kq;
}
}
$action = optional_param('action', '', PARAM_TEXT);
$page = optional_param('page', '', PARAM_TEXT);
if ($action=='search') {
    $name = optional_param('name', '', PARAM_TEXT);
    if (function_exists('get_all_student')) {
        global $DB;
        $page1=isset($page)?$page:1;
        $number=20;
        $key=trim($name);
        $cua=get_all_student_not_in_course($courseid=23,$page1,$number,$name);
    }
    $url= $CFG->wwwroot.'/manage/student/themhsvaolop.php?name='.$key.'&action=search&page=';

}
if (empty($action)){
    if (function_exists('get_all_student')) {
        global $DB;
        $page1=isset($page)?$page:1;
        $number=20;
        $cua=get_all_student_not_in_course($courseid=23,$page1,$number,$name=null);
    }
    $url= $CFG->wwwroot.'/manage/student/themhsvaolop.php?page=';
}
$course = $DB->get_record('course', array(
    'id' => 23
));
?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h3>Thêm học sinh vào lớp <?php echo $course->fullname; ?></h3>
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <div class="col-md-9 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <div class="col-4">
                                <input type="" name="name" class="form-control" placeholder="Nhập tên..." value="<?php echo trim($_GET['name']); ?>">
                                <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-info">Tìm kiếm</button>
                            </div>
                        </form>
                    </div>
                </div>
                <form action="" method="POST">
                    <input type="text" hidden="" value="add_student_to_course" name="action">
                    <input type="text" hidden="" value="<?php echo $course->id; ?>" name="courseid">
                    <div class="deleteall" style="    margin-left: 100px;position: absolute;">
                        <button class=" btn btn-info ">Thêm vào lớp</button>
                    </div>
                    <div class="table-responsive" data-pattern="priority-columns">
                        <table id="tech-companies-1" class="table  table-striped table-hover">
                            <thead>
                                <tr>
                                    <th data-priority="6"><input type="checkbox" value="" name="listid[]" class="check" id="checkAll">Checkall</th>
                                    <th data-priority="6">Tên học sinh</th>
                                    <th data-priority="6">Ngày sinh</th>
                                    <th data-priority="6">Email</th>
                                    <th data-priority="6">Điện thoại</th>
                                    <th data-priority="6">Tài khoản</th>
                                   <!--  <th data-priority="6">Hành động</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if (!empty($cua['data'])) {
                                # code...
                                    $i=0;
                                    foreach ($cua['data'] as $student) { 
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" value="<?php echo $student->id ?>" name="listid[]" class="check" id="checkAll"></td>
                                            <td><?php echo $student->lastname,' ',$student->firstname; ?> </td>
                                            <td><?php echo  $student->birthday;?></td>
                                            <td><?php echo  $student->email;?></td>
                                            <td><?php echo  $student->phone1;?></td>
                                            <td><?php echo  $student->username;?></td>
                                            <!-- <td class="text-right">
                                                <a href="<?php echo $CFG->wwwroot ?>/manage/student/edit_student.php?id_edit=<?php echo $student->id ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <a href="<?php echo $CFG->wwwroot ?>/manage/student/edit_student.php?id_edit=<?php echo $student->id ?>&action=delete" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </td> -->
                                        </tr>
                                    <?php }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php 
                        if ($cua['total_page']) {
                            if ($page > 2) { 
                                $startPage = $page - 2; 
                            } else { 
                                $startPage = 1; 
                            } 
                            if ($cua['total_page'] > $page + 2) { 
                                $endPage = $page + 2; 
                            } else { 
                                $endPage =$cua['total_page']; 
                            }
                        }
                        ?>
                        <p>Tổng số trang: <?php echo $cua['total_page'] ?></p>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
                                    <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <?php 
                                for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
                                    ?>
                                    <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
                                    <?php 
                                }
                                ?>
                                <li class="page-item <?php if(($page==$cua['total_page'])) echo 'disabled'; ?>">
                                    <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </form>
                
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
$listid = optional_param('listid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
if ($action=='add_student_to_course') {
    $redirectPath = $CFG->wwwroot . "/manage/student/themhsvaolop.php";
    global $DB;
    foreach ($listid as $key => $val) {
        assign_user_to_course($val, $courseid);
        $course = $DB->get_record('course', array('id' => $courseid));
        if ($send_email_notification == 1) {
            $user = $DB->get_record('user', array('id' => $val));
            $mailto = $user->email;
            $username = $user->username;
            $invitationContenNoEmail = $team_invitation_content;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{TEAM_NAME}", '', $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{COURSE_TITLE}", $course->fullname, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{COURSE_DESCRIPTION}", $course->summary, $invitationContenNoEmail);
            send_email($mailto, $mailUsername, 'LMS', $team_invitation_title, $invitationContenNoEmail);
            $invitationContenNoEmail = $team_invitation_content;
        }
    }
    //echo displayJsAlert('assign users to course succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
?>

<script>
    $('.deleteall').hide();
    $("#checkAll").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));
    });
    $(document).ready(function() {
        $(":checkbox").click(function(event) {
            if ($(this).is(":checked"))
                $(".deleteall").show();
            else
                $(".deleteall").hide();
        });
    });

</script>