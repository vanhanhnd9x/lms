<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function add_profile_2($firstname,$lastname,$email,$phone1,$username,$password,$confirmed,$address,$birthday,$sex,$name_parent,$description,$time_start,$time_end,$input_score,$time_start_int,$time_end_int,$code,$note_for_teacher) {
  global $DB, $USER;
  $record = new stdClass();
 
  $record->firstname = trim($firstname) ;
  $record->lastname = trim($lastname);
  $record->birthday =trim($birthday) ;
  $record->email = trim($email);
  $record->phone1 = trim($phone1);

  $record->username = trim($username);
  $password = hash_internal_user_password($password); 
  $record->password = $password;
  $record->confirmed = $confirmed;

  $record->country = 'VN';
  $record->timezone = 'SE Asia Standard Time';
  $record->address = trim($address);
  $record->parent = 0;
  $record->mnethostid = 1;
  $record->input_score = $input_score;
  $record->sex = $sex;
  $record->name_parent = trim($name_parent);
  $record->description = trim($description);
  $record->note_for_teacher = trim($note_for_teacher);

  $record->time_start = $time_start;
  $record->time_start_int = $time_start_int;
  $record->time_end = $time_end;
  $record->time_end_int = $time_end_int;
  // $record->time_end_int = strtotime($time_end);

  $record->code = trim($code);
  $record->del = 0;

  $new_user_id = $DB->insert_record('user', $record);
  return $new_user_id;
}
function update_my_profile_3($user_id,$firstname,$lastname,$email,$phone1,$address,$birthday,$sex,$name_parent,$description,$time_start,$time_end,$input_score,$time_start_int,$time_end_int,$note_for_teacher) {
  global $DB;
  $record = new stdClass();
  $record->id = $user_id;
  $record->firstname = trim($firstname);
  $record->lastname = trim($lastname);
  $record->birthday = trim($birthday);
  $record->email =trim($email) ;
  $record->phone1 = trim($phone1);
  $record->address =trim($address);
  $record->sex = $sex;
  $record->name_parent = trim($name_parent);
  $record->description = trim($description);
  $record->note_for_teacher = trim($note_for_teacher);

  $record->time_start = $time_start;
  $record->time_end = $time_end;

  $record->time_start_int = $time_start_int;
  $record->time_end_int = $time_end_int;
  // $record->time_end_int =  strtotime($time_end);;

  $record->input_score = trim($input_score);
  return $DB->update_record('user', $record, false);
}
function delete_student($id){
  if (!empty($id)) {
    // global $DB;
    // $DB->delete_records_select('user', " id = $id ");
    // $DB->delete_records_select('role_assignments', " userid = $id ");
    // return 1;
        # code...
    global $DB;
    $record = new stdClass();
    $record->id = $id;
    $record->del = 1;
    $record->confirmed = 0;
    return $DB->update_record('user', $record, false);
  }
}
// lấy toàn bộ ds học sinh
function get_all_student($page,$number){
 global $DB, $USER;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }

$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0  ORDER BY `user`.`id` DESC  LIMIT {$start} , {$number}
";
$data1=$DB->get_records_sql($sql);
$sql_total="SELECT COUNT( DISTINCT `user`.`id`) as total FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0";
// $total_row=$DB->get_records_sql($sql_total);

// foreach ($total_row as $key => $value) {
//     # code...
//   $total_page=ceil((int)$key / (int)$number);
//   break;
// }
$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
  $total_page=ceil((int)$total_row / (int)$number);
$kq=array(
  'data'=>$data1,
  'total_page'=>$total_page 
);
return $kq;
}
// tìm kiếm học sinh
function search_student_by_cua($schoolid=null,$block_student=null,$groupid=null,$courseid=null,$code=null,$lastname=null,$firstname=null,$name_parent=null,$phone=null,$birthday=null,$page=null,$number=null){
 global $DB;
 if ($page>1) {
  $start=(($page-1)*$number);
}else{
  $start=0;
}
// $sql = "SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0";
// $sql_count="SELECT  COUNT( DISTINCT `user`.`id`)FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0";
$sql = "SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` 
FROM user 
JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` 
JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` 
WHERE `role_assignments`.`roleid`= 5 
AND `user`.`del`=0";
$sql_count="SELECT  COUNT( DISTINCT `user`.`id`)
FROM user 
JOIN role_assignments ON `user`.id = `role_assignments`.`userid` 
JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` 
JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` 
WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0";
if (!empty($schoolid)&&empty($block_student)&&empty($groupid)&&empty($courseid)) {
  $sql.=" AND `groups`.`id_truong`={$schoolid}";
  $sql_count.=" AND `groups`.`id_truong`={$schoolid}";
}elseif (!empty($schoolid)&&!empty($block_student)&&empty($groupid)&&empty($courseid)) {
  $sql .= " AND `groups`.`id_khoi`={$block_student} "; 
  $sql_count .= " AND `groups`.`id_khoi`={$block_student} ";
}elseif (!empty($schoolid)&&!empty($block_student)&&!empty($groupid)&&empty($courseid)) {
  $sql .= " AND `groups`.`id`={$groupid}";
  $sql_count .= " AND `groups`.`id`={$groupid}";
}elseif(!empty($schoolid)&&!empty($block_student)&&!empty($groupid)&&!empty($courseid)){
  $sql.=" AND `user`.`id` IN (SELECT DISTINCT `user`.`id` FROM `course` JOIN `enrol` ON `enrol`.`courseid` = `course`.`id` JOIN `user_enrolments` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `user` ON `user_enrolments`.`userid` = `user`.`id` WHERE  `user`.`del`=0 AND `course`.`id`={$courseid})";
  $sql_count.=" AND `user`.`id` IN (SELECT DISTINCT `user`.`id` FROM `course` JOIN `enrol` ON `enrol`.`courseid` = `course`.`id` JOIN `user_enrolments` ON `user_enrolments`.`enrolid` = `enrol`.`id` JOIN `user` ON `user_enrolments`.`userid` = `user`.`id` WHERE  `user`.`del`=0 AND `course`.`id`={$courseid})";
}
if (!empty($code)) {
  $sql .= " AND `user`.`code` LIKE '%{$code}%'";
  $sql_count .= " AND `user`.`code` LIKE '%{$code}%'";

}
if (!empty($name)) {
 $sql .= " AND (`user`.`firstname` LIKE '%{$name}%' OR `user`.`firstname` LIKE '%{$name}%')";
 $sql_count .= " AND (`user`.`firstname` LIKE '%{$name}%' OR `user`.`firstname` LIKE '%{$name}%')";
}
if (!empty($lastname)) {
 $sql .= " AND `user`.`lastname` LIKE '%{$lastname}%' ";
 $sql_count .= " AND `user`.`lastname` LIKE '%{$lastname}%'";
}
if (!empty($firstname)) {
 $sql .= " AND `user`.`firstname` LIKE '%{$firstname}%' ";
 $sql_count .= " AND `user`.`firstname` LIKE '%{$firstname}%'";
}
if (!empty($name_parent)) {
 $sql .= " AND  `user`.`name_parent` like '%{$name_parent}%'";
 $sql_count .= " AND  `user`.`name_parent` like '%{$name_parent}%'";
}
if (!empty($phone)) {
 $sql .= " AND `user`.`phone1` LIKE '%{$phone}%'";
 $sql_count .= " AND `user`.`phone1` LIKE '%{$phone}%'";
}
if (!empty($birthday)) {
 $sql .= " AND `user`.`birthday` LIKE '%{$birthday}%'";
 $sql_count .= " AND `user`.`birthday` LIKE '%{$birthday}%'";
  // $sql .= " AND `user`.`birthday` = '{$birthday}'";
 // $sql_count .= " AND `user`.`birthday` = '{$birthday}'";
}
$sql .= " ORDER BY `user`.`id` DESC LIMIT {$start} , {$number} ";
$data=$DB->get_records_sql($sql);
$total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
$total_page=ceil((int)$total_row / (int)$number);
$kq=array(
  'data'=>$data,
  'total_page'=>$total_page 
);
return $kq;
}
function remove_person_from_course_2($person_id, $course_id) {
  global $DB, $CFG;

  require_once($CFG->dirroot . '/lib/enrollib.php');
  $enrol = enrol_get_plugin('manual');
  $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
  $instance = reset($instances);

  $enrol->unenrol_user($instance, $person_id);
}
function assign_user_to_course_2($user_id, $course_id) {
  global $CFG, $DB;
  require_once($CFG->dirroot . '/lib/enrollib.php');

  $enrol = enrol_get_plugin('manual');

  $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');

  $instance = reset($instances);
  $enrol->enrol_user($instance, $user_id, 5, '', '');
}
// them hs vao nhom

function insert_member_to_group_2($user_id, $group_id) {
  global $DB;
  $groups_members = new stdClass();
  $groups_members->groupid = $group_id;
  $groups_members->userid = $user_id;
  $groups_members->timeadded = time();
  $DB->insert_record('groups_members', $groups_members);
}
// loại bỏ hs khỏi nhóm
function remove_team_member2($user_id, $team_id) {
  global $DB;
  $conditions = array(
    'userid' => $user_id,
    'groupid' => $team_id
  );

  $DB->delete_records('groups_members', $conditions);
}
// search học sinh

// tạ
function get_idcourse_by_id_user($id){
  global $CFG, $DB;
  if (!empty($id)) {
    $sql = "SELECT `enrol`.`courseid`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
    WHERE `user`.`id`=:userid";
    $params = array('userid' => $id);
    $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    return $data;
  }
}
function show_name_groupd($iduser){
 global $CFG, $DB;
 if (!empty($iduser)) {
  $sql = "SELECT `groups`.`name`
  FROM `groups`
  JOIN `groups_members`
  ON `groups`.`id`=`groups_members`.`groupid`
  WHERE `groups_members`.`userid` = :userid";
  $params = array('userid' => $iduser);
  $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
  return $data;
}
}
function get_id_user_by_code($code){
 global $CFG, $DB;
 if (!empty($code)) {
  $sql = "SELECT DISTINCT `user`.`id`
  FROM `user`
  WHERE `user`.`code` = :usercode";
  $params = array('usercode' => $code);
  $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
  return $data;
}
}
function show_name_course_by_cua($id){
  global $CFG, $DB;
  if (!empty($id)) {
    $sql = "SELECT DISTINCT `course`.`fullname`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` =`enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid` =`course`.`id` 
    WHERE `user`.`id`=:userid";
    $params = array('userid' => $id);
    $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    return $data;
  }
}
function show_name_school_by_cua_student($id){
  global $CFG, $DB;
  if (!empty($id)) {
    $sql = "SELECT DISTINCT `schools`.`name` 
    FROM `user` 
    JOIN `groups_members` ON `groups_members`.`userid` =`user`.`id` 
    JOIN `groups` ON `groups_members`.`groupid` =`groups`.`id` 
    JOIN `schools` ON `groups`.`id_truong` = `schools`.`id` 
    WHERE `user`.`id`=:userid";
    $params = array('userid' => $id);
    $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    return $data;
  }
}


// Chien add
function insert_history_student($idUser,$idSchool,$idBlock,$idClass,$idGroups="",$idYear="")
{
    global $DB;
    $data = new stdClass();

    $data->iduser   = $idUser;
    $data->idschool = $idSchool;
    $data->idblock  = $idBlock;
    $data->idclass  = $idClass;
    $data->idgroups = $idGroups;
    $data->idschoolyear = $idYear;

    $lastinsertid = $DB->insert_record('history_student', $data);
    return $lastinsertid;

}
function get_sy_groups($groupid)
{
  global $DB;
  $sql ="SELECT * FROM groups WHERE id=".$groupid;
  return $DB->get_record_sql($sql);
}

// code chinh sua
function search_student_by_cua2($schoolid=null,$block_student=null,$school_year=null,$groupid=null,$courseid=null,$code=null,$lastname=null,$firstname=null,$name_parent=null,$phone=null,$birthday=null,$page=null,$number=null){
     global $DB;
     if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
    }
    // $sql=" SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `history_student` ON `history_student`.`iduser`=`user`.`id` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 ";
    $sql="SELECT DISTINCT `user`.`id`,concat_ws(' ',`user`.`lastname`,`user`.`firstname`) as fullname ,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid`,`user`.`time_start`,`user`.`time_end`,`user`.`description`,`user`.`code`,`user`.`sex` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `history_student` ON `history_student`.`iduser`=`user`.`id` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0  ";
    $sql_count="SELECT  COUNT( DISTINCT `user`.`id`) FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `history_student` ON `history_student`.`iduser`=`user`.`id` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0";
    if (!empty($schoolid)) {
      $sql.=" AND `history_student`.`idschool`={$schoolid}";
      // $sql.=" AND `groups`.`id_truong`={$schoolid}";
      $sql_count.=" AND `history_student`.`idschool`={$schoolid}";
    }
    if (!empty($block_student)) {
      $sql .= " AND `history_student`.`idblock`={$block_student} "; 
      $sql_count .= " AND `history_student`.`idblock`={$block_student} ";
    }
    if (!empty($groupid)) {
      $sql .= " AND `history_student`.`idclass`={$groupid}";
      $sql_count .= " AND `history_student`.`idclass`={$groupid}";
    }
    if(!empty($courseid)){
      $sql.=" AND `history_student`.`idgroups`={$courseid} ";
      $sql_count.=" AND `history_student`.`idgroups`={$courseid}";
    }
    if(!empty($school_year)){
        $sql.=" AND `history_student`.`idschoolyear`={$school_year} ";
        $sql_count.=" AND `history_student`.`idschoolyear`={$school_year}";
    }
    if (!empty($code)) {
      $sql .= " AND `user`.`code` LIKE '%{$code}%'";
      $sql_count .= " AND `user`.`code` LIKE '%{$code}%'";

    }
    if (!empty($name)) {
     $sql .= " AND (`user`.`firstname` LIKE '%{$name}%' OR `user`.`firstname` LIKE '%{$name}%')";
     $sql_count .= " AND (`user`.`firstname` LIKE '%{$name}%' OR `user`.`firstname` LIKE '%{$name}%')";
    }
    // if (!empty($lastname)) {
    //  $sql .= " AND `user`.`lastname` LIKE '%{$lastname}%' ";
    //  $sql_count .= " AND `user`.`lastname` LIKE '%{$lastname}%'";
    // }
    if (!empty($firstname)) {
     $sql .= " AND (concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$firstname}%'   )";
     $sql_count .= " AND (concat_ws(' ',`user`.`lastname`,`user`.`firstname`) LIKE '%{$firstname}%'  ) ";
    }
    if (!empty($name_parent)) {
     $sql .= " AND  `user`.`name_parent` like '%{$name_parent}%'";
     $sql_count .= " AND  `user`.`name_parent` like '%{$name_parent}%'";
    }
    if (!empty($phone)) {
     $sql .= " AND `user`.`phone1` LIKE '%{$phone}%'";
     $sql_count .= " AND `user`.`phone1` LIKE '%{$phone}%'";
    }
    if (!empty($birthday)) {
     $sql .= " AND `user`.`birthday` LIKE '%{$birthday}%'";
     $sql_count .= " AND `user`.`birthday` LIKE '%{$birthday}%'";
    }
    $sql .= " ORDER BY `user`.`firstname` ASC LIMIT {$start} , {$number} ";
    $data=$DB->get_records_sql($sql);
    $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
    $total_page=ceil((int)$total_row / (int)$number);
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
}
function show_name_course_by_cua2($userid,$school_year){
  global $CFG, $DB;
    // $sql = "SELECT DISTINCT `history_course_name`.`course_name` FROM `history_student` JOIN `history_course_name` ON `history_student`.`idgroups`=`history_course_name`.`courseid` WHERE `history_student`.`iduser`={$userid} AND `history_course_name`.`school_year_id`={$school_year} AND `history_student`.`idschoolyear`={$school_year}";
     $sql = "SELECT DISTINCT `history_course_name`.`id` FROM `history_student` JOIN `history_course_name` ON `history_student`.`idgroups`=`history_course_name`.`courseid` WHERE `history_student`.`iduser`={$userid} AND `history_course_name`.`school_year_id`={$school_year} AND `history_student`.`idschoolyear`={$school_year} AND  `history_student`.`idgroups` IS NOT null";
    $id = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    $history_course_name=$DB->get_record('history_course_name', array(
      'id' => $id
    )); 
    return $history_course_name->course_name;
}
function show_name_school_by_cua_student2($id,$school_year){
  global $CFG, $DB;
    $sql = "SELECT DISTINCT `schools`.`name` 
    FROM `user` 
    JOIN `history_student` ON `history_student`.`iduser` =`user`.`id` 
    JOIN `schools` ON `schools`.`id` =`history_student`.`idschool` 
    WHERE `user`.`id`={$id}
    AND `history_student`.`idschoolyear`={$school_year}
    ";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
// function show_name_school_course_student_crebytrang($userid){
//   global $DB;
//   $sql_course="SELECT DISTINCT `course`.`id`
//   FROM `user` JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
//   JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
//   JOIN `course` ON `enrol`.`courseid`=`course`.`id`
//   WHERE `user`.`del`=0 
//   AND `user`.`id`={$userid}";
//   $idcourse = $DB->get_field_sql($sql_course,null,$strictness=IGNORE_MISSING);
//   $course=$DB->get_record('course', array(
//     'id' => $idcourse
//     ),
//     $fields='id,fullname', $strictness=IGNORE_MISSING
//   );
//   $sql_truog="SELECT DISTINCT `school`.`name`
//   FROM `schools`
//   JOIN `groups` ON `groups`.`id_truong`=`schools`.`id`
//   JOIN `groups_members` ON `groups_members`.`groupid`= `groups`.`id`
//   JOIN `user` ON `user`.`id` = `groups_members`.`userid`
//   WHERE `schools`.`del`=0
//   AND `user`.`id`= {$userid}";
//   $truong = $DB->get_field_sql($sql_truog,null,$strictness=IGNORE_MISSING);
//   $kq=array(
//     'course'=
//   );
// }
?>