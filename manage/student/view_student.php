<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
global $USER;
require_once($CFG->dirroot . '/manage/lib.php');
$idnamhoc=get_id_school_year_now();
require_login();
$PAGE->set_title(get_string('edit_student'));
$PAGE->set_heading(get_string('edit_student'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$id_edit=optional_param('id_edit', '', PARAM_TEXT);
function lay_lich_su_moi_nhat_cua_hs($userid){
  global $DB;
  $sql="SELECT MAX(id) FROM history_student WHERE iduser={$userid}";
  $id=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
  return $id;
}
function lay_ten_nhom_lop_theo_namhoc($courseid,$idnamhoc){
  global $DB;
  $sql=""
}
$historyid=lay_lich_su_moi_nhat_cua_hs($id_edit);
$student=$DB->get_record('user', array(
  'id' => $id_edit
));
$history_student=$DB->get_record('history_student', array(
  'id' => $historyid
));

$idcourse=get_idcourse_by_id_user($id_edit);
$course=$DB->get_record('course', array(
  'id' => $idcourse
));
// $groupid=$DB->get_record('group_course', array(
//   'course_id' => $idcourse
// ));
$groupid=$DB->get_record('groups_members', array(
  'userid' => $id_edit
));
$group=$DB->get_record('groups', array(
  'id' => $groupid->groupid
));
$block_student=$DB->get_record('block_student', array(
  'id' => $group->id_khoi
));
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='student';
if($action=='view'){
  $name1='view_student';
}else{
  $name1='edit_student';
}
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}


if ($action=='edit_student') {
  $user_id = optional_param('user_id', '', PARAM_TEXT);
  $lastname = optional_param('lastname', '', PARAM_TEXT);
  $firstname = optional_param('firstname', '', PARAM_TEXT);
  $confirmed = optional_param('confirmed', '', PARAM_TEXT);
  $birthday = optional_param('birthday', '', PARAM_TEXT);
  $email = optional_param('email', '', PARAM_TEXT);
  $phone1 = optional_param('phone1', '', PARAM_TEXT);
  $username = optional_param('username', '', PARAM_TEXT);
  $city = optional_param('city', '', PARAM_TEXT);
  $id_district = optional_param('id_district', '', PARAM_TEXT);
  $id_wards = optional_param('id_wards', '', PARAM_TEXT);
  $address = optional_param('address', '', PARAM_TEXT);
  $note_for_teacher = optional_param('note_for_teacher', '', PARAM_TEXT);

  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $block_student = optional_param('block_student', '', PARAM_TEXT);
  $schoolid = optional_param('schoolid', '', PARAM_TEXT);
  $groupid = optional_param('groupid', '', PARAM_TEXT);
  $school_year= get_sy_groups($groupid)->id_namhoc;

  $sex = optional_param('sex', '', PARAM_TEXT);
  $name_parent = optional_param('name_parent', '', PARAM_TEXT);
  $description = optional_param('description', '', PARAM_TEXT);
  $input_score = optional_param('input_score', '', PARAM_TEXT);
  $time_start = optional_param('time_start', '', PARAM_TEXT);
  $time_end = optional_param('time_end', '', PARAM_TEXT);
  $time_start_int=strtotime($time_start);
  // $time_end_int=strtotime($time_end);
  list($day, $month, $year) = explode('/', $time_end);
  $time_end_int=mktime(23, 59, 59, $month, $day, $year);
  $edit=update_my_profile_3($id_edit,$firstname,$lastname,$email,$phone1,$address,$birthday,$sex,$name_parent,$description,$time_start,$time_end,$input_score,$time_start_int,$time_end_int,$note_for_teacher);
  if($block_student!=$group->id_khoi){
     insert_history_student($id_edit,$schoolid,$block_student,$groupid,$courseid,$school_year);
  }
  if($schoolid!=$group->id_truong){
     insert_history_student($id_edit,$schoolid,$block_student,$groupid,$courseid,$school_year);
  }
  if ($edit){
   if($courseid!=$idcourse){
    remove_person_from_course_2($id_edit, $idcourse);
    assign_user_to_course_2($id_edit, $courseid);
    insert_history_student($id_edit,$schoolid,$block_student,$groupid,$courseid,$school_year);
  }
  if($groupid!=$group->id){
    remove_team_member2($id_edit, $group->id);
    insert_member_to_group_2($id_edit, $groupid);
    insert_history_student($id_edit,$schoolid,$block_student,$groupid,$courseid,$school_year);
  }
  add_logs('student','update','/manage/student/edit_student.php?id_edit='.$edit->id,$lastname.' '.$firstname);
 echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/student/index.php");
}else{
 echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/student/edit_student.php?id_edit=".$id_edit);
}
}
?><?php echo strtotime('31/12/2018'); ?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">
          <form  action="" onsubmit="return validate();" method="post">
            <input type="text" name="action" value="edit_student" hidden="">
            <input type="text" name="user_id"  hidden="" value="<?php $student->id; ?>">
            <div class="row">
              <div class="col-md-6">
                <label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                <select name="schoolid" id="schoolid" class="form-control" required="">
                 <option value=""><?php echo get_string('schools'); ?></option>
                 <?php 
                 $schools = get_all_shool();
                 foreach ($schools as $key => $value) {
                        # code...
                  ?>
                  <option value="<?php echo $value->id; ?>" <?php if (!empty($block_student->schoolid)&&($block_student->schoolid==$value->id)) { echo'selected';} ?>><?php echo $value->name; ?></option>
                  <?php 
                }
                ?>
              </select>
            </div>
            <div class="col-md-6">
              <label class="control-label"><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
              <select name="block_student" id="block_student" class="form-control" required="">
               <option value=""><?php echo get_string('block_student'); ?></option>
             </select>
           </div>
           <div class="col-md-6">
            <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
            <select name="groupid" id="groupid" class="form-control" required="">
              <option value=""><?php echo get_string('class'); ?></option>
            </select>
          </div>
          <div class="col-md-6">
           <label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
           <select name="courseid" id="courseid" class="form-control" >
            <option value=""><?php echo get_string('classgroup'); ?></option>
          </select>
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('lastname'); ?><span style="color:red">*</span></label>
          <input class="form-control" type="text" name="lastname" required placeholder="<?php print_string('lastname');?> " value="<?php echo $student->lastname; ?>">
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('firstname'); ?><span style="color:red">*</span></label>
          <input class="form-control" type="text" name="firstname" required placeholder="<?php print_string('firstname');?>" value="<?php echo $student->firstname; ?>">
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('codestudent'); ?></label>
          <input class="form-control" type="text" name="" required placeholder="<?php print_string('codestudent');?>" value="<?php echo $student->code; ?>" disabled>
        </div>
        <div class="col-md-6">
          <label class="control-label"><?php echo get_string('birthday'); ?></label>
          <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
            <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $student->birthday; ?>">
          </div>
        </div>
        <div class="col-md-6">
         <label class="control-label"><?php echo get_string('sex'); ?></label>
         <select name="sex" id="" class="form-control">
          <option value="1"><?php echo get_string('women'); ?></option>
          <option value="2" <?php if ($student->sex==2) { echo 'selected';} ?>><?php echo get_string('man'); ?></option>
        </select>
      </div>
      <div class="col-md-6">
       <label class="control-label"><?php echo get_string('input_score'); ?></label>
       <input class="form-control" type="text" name="input_score" placeholder="<?php echo get_string('input_score'); ?>"  value="<?php echo $student->input_score; ?>">
     </div>
     <div class="col-md-6">
       <label class="control-label"><?php echo get_string('parent'); ?></label>
       <input class="form-control" type="text" name="name_parent" placeholder="<?php echo get_string('parent'); ?>"  value="<?php echo $student->name_parent; ?>">
     </div>
     <div class="col-md-6">
       <label class="control-label"><?php echo get_string('email'); ?></label>
       <input class="form-control" type="text" name="email" placeholder="<?php print_string('email') ?>"  value="<?php echo $student->email; ?>" >
     </div>
     <div class="col-md-6">
      <label class="control-label"><?php echo get_string('mobileph'); ?></label>
      <input class="form-control" type="text" name="phone1" placeholder="<?php print_r(get_string('mobileph')) ?>"  value="<?php echo $student->phone1; ?>" pattern="^(01[2689]|02[0-9]|09|08|03)[0-9]{8}">
    </div>
    <div class="col-md-6">
     <label class="control-label"><?php echo get_string('home_address'); ?></label>
     <input class="form-control" type="text" name="address" placeholder="<?php print_r(get_string('home_address')) ?>" value="<?php echo $student->address; ?>">
   </div>
   <div class="col-md-6">
    <label class="control-label"><?php echo get_string('time_start'); ?></label>
    <input type="text" name="time_start" id="date_start" class=" datepicker form-control" placeholder="dd/mm/yyyy"  value="<?php echo $student->time_start; ?>">
  </div>
  <div class="col-md-6">
    <label class="control-label"><?php echo get_string('time_end'); ?></label>
    <input type="text" name="time_end" id="date_end" class="  datepicker form-control"  placeholder="dd/mm/yyyy" value="<?php echo $student->time_end; ?>">
  </div>
  <div class="col-6">
     <label class="control-label"><?php echo get_string('note'); ?></label>
      <textarea name="description" id="" cols="30" rows="10" class="form-control" value="<?php echo $student->description; ?>"><?php echo $student->description; ?></textarea>
  </div>
  <div class="col-md-6 form-group">
    <label class="control-label"><?php print_string('note_for_teacher');?></label>
    <textarea name="note_for_teacher" id="" cols="30" rows="10" class="form-control" value="<?php echo $student->note_for_teacher; ?>"><?php echo $student->note_for_teacher; ?></textarea>
  </div>
  <div class="col-md-12" style="margin-top: 10px;">
    <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
    <?php print_r(get_string('or')) ?>
    <a  href="<?php echo $CFG->wwwroot ?>/manage/student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
  </div>
</div> 


</form>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<script>
  // Date picker only
  $('#birthday').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
<?php 
echo $OUTPUT->footer();

?>


  <!-- Propeller Bootstrap datetimepicker -->

