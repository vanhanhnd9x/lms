<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');


$PAGE->set_title(get_string('list_student')); 
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$school_year = optional_param('school_year', '', PARAM_TEXT);
$code = trim(optional_param('code', '', PARAM_TEXT));
$name = trim(optional_param('name', '', PARAM_TEXT));
$name_parent = trim(optional_param('name_parent', '', PARAM_TEXT));
$phone = trim(optional_param('phone', '', PARAM_TEXT));
$birthday = trim(optional_param('birthday', '', PARAM_TEXT));
$lastname = trim(optional_param('lastname', '', PARAM_TEXT));
$firstname = trim(optional_param('firstname', '', PARAM_TEXT));
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
// nam hoc hien tai
$dk_namhoc_lay_tennhom=get_id_school_year_now();
if(!empty($school_year)){
    $dk_namhoc_lay_tennhom=$school_year;
}


$cua=search_student_by_cua2($schoolid,$block_student,$school_year,$groupid,$courseid,$code,$lastname,$firstname,$name_parent,$phone,$birthday,$page,$number);
if ($action=='search') {
  // $cua=search_student_by_cua($schoolid,$block_student,$groupid,$courseid,$code,$lastname,$firstname,$name_parent,$phone,$birthday,$page,$number);
  $url= $CFG->wwwroot.'/manage/student/index.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&school_year='.$school_year.'&groupid='.$groupid.'&courseid='.$courseid.'&code='.$code.'&lastname='.$lastname.'&firstname='.$firstname.'&name_parent='.$name_parent.'&phone='.$phone.'&birthday='.$birthday.'&page=';
}else{
  // $cua=get_all_student($page,$number); 
  $url= $CFG->wwwroot.'/manage/student/index.php?page=';
} 
// phan quyen
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='student';
$name1='list_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);

?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <?php 
                        if(!empty($checkthemmoi)){
                            ?>
                            <div class="col-md-2">
                                <a href="<?php echo new moodle_url('/manage/student/add_student.php'); ?>" class="btn btn-success">
                                    <?php echo get_string('new'); ?></a>
                            </div>
                            <?php 
                        }
                     ?>
                    
                    <div class="col-md-9 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <input type="" name="action" class="form-control" placeholder="Nhập tên..." value="search" hidden="">
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('schools'); ?> </label>
                                <select name="schoolid" id="schoolid" class="form-control">
                                    <option value="">
                                        <?php echo get_string('schools'); ?>
                                    </option>
                                    <?php 
                                   $schools = get_all_shool();
                                   foreach ($schools as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>>
                                        <?php echo $value->name; ?>
                                    </option>
                                    <?php 
                                      }
                                      ?>
                                </select>
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('block_student'); ?></label>
                                <select name="block_student" id="block_student" class="form-control">
                                    <option value="">
                                        <?php echo get_string('block_student'); ?>
                                    </option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
                                <select name="school_year" id="school_yearid" class="form-control" >
                                  <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                                          
                                </select>
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('class'); ?></label>
                                <select name="groupid" id="groupid" class="form-control">
                                    <option value="">
                                        <?php echo get_string('class'); ?>
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-4">
                              <label class="control-label"><?php echo get_string('classgroup'); ?> </label>
                              <select name="courseid" id="courseid" class="form-control" >
                                <option value=""><?php echo get_string('classgroup'); ?></option>
                              </select>
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('codestudent'); ?></label>
                                <input type="" name="code" class="form-control" placeholder="<?php echo get_string('codestudent'); ?>" value="<?php echo trim($code); ?>">
                            </div>
                           <!--  <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('lastname'); ?></label>
                                <input type="" name="lastname" class="form-control" placeholder="<?php echo get_string('lastname'); ?>" value="<?php echo trim($lastname); ?>">
                            </div> -->
                            <input type="" name="lastname" class="form-control" placeholder="<?php echo get_string('lastname'); ?>" hidden>
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('full_name'); ?></label>
                                <input type="" name="firstname" class="form-control" placeholder="<?php echo get_string('full_name'); ?>" value="<?php echo trim($firstname); ?>">
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('parent'); ?></label>
                                <input type="" name="name_parent" class="form-control" placeholder="<?php echo get_string('parent'); ?>" value="<?php echo trim($name_parent); ?>">
                            </div>
                            <div class="col-4 form-group">
                                <label for="">
                                    <?php print_r(get_string('phone')) ?></label>
                                <input type="" name="phone" class="form-control" placeholder="<?php print_r(get_string('phone')) ?>" value="<?php echo trim($phone); ?>">
                            </div>
                            <!-- <div class="col-4 form-group">
                                <label for="">
                                    <?php echo get_string('birthday'); ?></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker" name="birthday" value="<?php echo trim($birthday); ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
                            </div> -->
                             <div class="col-md-4 form-group">
                               <label class="control-label"><?php print_string('birthday');?></label>
                               <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-th"></span>
                                </div>
                                <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $birthday; ?>">
                              </div>
                            </div>
                            <div class="col-4 form-group">
                                <div style="    margin-top: 29px;"></div>
                                <button type="submit" class="btn btn-success">
                                    <?php echo get_string('search'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive" data-pattern="priority-columns">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo'<th class="text-center">'.get_string('action').'</th>';
                                    }
                                 ?>
                                <th>
                                    <?php echo get_string('codestudent'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('full_name'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('sex'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('birthday'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('parent'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('phone'); ?>
                                </th>
                                
                                <!-- <th>
                                    <?php echo get_string('time_start'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('time_end'); ?>
                                </th> -->
                                 <th>
                                    <?php echo get_string('schools'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('classgroup'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('note'); ?>
                                </th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (!empty($cua['data'])) {
                                if($page==1){
                                    $i=0;
                                }else{
                                    $i=($page-1)*$number;
                                } 
                              foreach ($cua['data'] as $student) { 
                                $i++;
                                // $course=show_name_course_by_cua($student->id);
                                // $school=show_name_school_by_cua_student($student->id);
                                $course2=show_name_course_by_cua2($student->id,$dk_namhoc_lay_tennhom);
                                $school=show_name_school_by_cua_student2($student->id,$dk_namhoc_lay_tennhom);
                                ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $i; ?>
                                </td>
                                <?php 
                                    if(!empty($hanhdong)||!empty($checkdelete)){
                                        echo' <td class="text-center">';
                                        $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$student->id);
                                        if(!empty($checkdelete)){
                                            echo'<a href="javascript:void(0)" onclick="delet_student('.$student->id.');" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                                        }
                                        echo'</td>';
                                    }
                                 ?>
                                <td>
                                    <?php echo  $student->code;?>
                                </td>
                                <td>
                                    <!-- <?php echo $student->lastname,' ',$student->firstname; ?> -->
                                    <?php echo $student->fullname; ?>
                                </td>
                                <td>
                                    <?php 
                                        switch ($student->sex) {
                                            case 1:echo get_string('women'); break;
                                            case 2:echo get_string('man'); break;
                                        }
                                     ?>
                                </td>
                                <td>
                                    <?php echo  $student->birthday;?>
                                </td>
                                <td>
                                    <?php echo  $student->name_parent;?>
                                </td>
                                <td>
                                    <?php echo  $student->phone1;?>
                                </td>
                                <!-- 
                                <td>
                                    <?php echo  $student->time_start;?>
                                </td>
                                <td>
                                    <?php echo  $student->time_end;?>
                                </td> -->
                                 <td><?php echo @$school; ?></td>
                                <td>
                                    <?php echo @$course2; ?>
                                </td>
                                <td>
                                    <?php echo  $student->description;?>
                                </td>
                                
                               <!--  <td class="text-right">
                                    
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/student/edit_student.php?id_edit=<?php echo $student->id ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="javascript:void(0)" onclick="delet_student(<?php echo $student->id;?>);" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td> -->
                            </tr>
                            <?php }
                        }
                        ?>
                        </tbody>
                    </table>
                    <?php 
                    if ($cua['total_page']) {
                      if ($page > 2) { 
                        $startPage = $page - 2; 
                      } else { 
                        $startPage = 1; 
                      } 
                      if ($cua['total_page'] > $page + 2) { 
                        $endPage = $page + 2; 
                      } else { 
                        $endPage =$cua['total_page']; 
                      }
                    }
                     ?>
                    <p>
                        <?php echo get_string('total_page'); ?>:
                        <?php echo $cua['total_page'] ?>
                    </p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                            for ($i=$startPage; $i <=$endPage ; $i++) { 
                                                            # code...
                              ?>
                            <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>">
                                    <?php echo $i; ?></a></li>
                            <?php 
                            }
                            ?>
                            <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
$action = optional_param('action', '', PARAM_TEXT);
if ($action='delete') {
    # code...
  $iddelete = optional_param('iddelete', '', PARAM_TEXT);
  $return=delete_student($iddelete);
  if($return){
    echo displayJsAlert('Xoa thanh cong', $CFG->wwwroot . "/manage/student/index.php");
  }
}
?>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<script>
  // Date picker only
  // $('#date_end').datetimepicker({
  //   format: 'DD-MM-YYYY'
  // });
  // $('#date_start').datetimepicker({
  //   format: 'DD-MM-YYYY'
  // });
  $('#birthday').datetimepicker({
    format: 'DD/MM/YYYY'
  });
</script>
<script>
// ajax add
$('#schoolid').on('change', function() {
    var cua = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
            $("#block_student").html(data);
        });
    }
});
$('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
        $("#school_yearid").html(data);
      });
    }
  });
   $('#school_yearid').on('change', function() {
    var blockid = $('#school_yearid option:selected').attr('status');
    var school_yearid = this.value ;
    // alert(courseid);
    $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
      $("#groupid").html(data);
    });
    
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    var school_yearid2 = $('#groupid option:selected').attr('status');
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
        $("#courseid").html(data);
      });
    }
  });
    var school = $('#schoolid').find(":selected").val();
    var blockedit = "<?php echo $block_student; ?>";
    var school_yearedit="<?php echo $school_year; ?>";
    var groupeit="<?php echo $groupid; ?>";
    var courseedit="<?php echo $courseid; ?>";
  if(school){
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    }
    if(blockedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+blockedit+"&school_yearedit="+school_yearedit,function(data){
            $("#school_yearid").html(data);
        });
    }
    if(school_yearedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockedit+"&school_year="+school_yearedit+"&groupedit="+groupeit, function(data) {
          $("#groupid").html(data);
        }); 
    }
    if(courseedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupeit+"&school_yearid2="+school_yearedit+"&courseedit="+courseedit,function(data){
            $("#courseid").html(data);
          });
    }
    
</script>
<script>
function delet_student(iddelete) {
    // var urlweb = "<?php echo $CFG->wwwroot ?>/manage/student/index.php";
    var urlweb = "<?php echo $url; ?>";
    var urldelte = "<?php echo $CFG->wwwroot ?>/manage/student/load_ajax.php";
    var e = confirm('<?php echo get_string('delete_student'); ?>');
    if (e == true) {
        $.ajax({
            url: urldelte,
            type: "post",
            data: { iddelete: iddelete },
            success: function(response) {
                window.location = urlweb;
            }
        });
    }
}
</script>