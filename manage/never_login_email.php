<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 *
 * @author
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

global $CFG;
require_once('../config.php');
include ($CFG->dirroot.'/teams/config.php');
require_once($CFG->dirroot . '/manage/people/lib.php');
// Require Login.
require_login();


$PAGE->set_title("People");
$PAGE->set_heading("People");
//now the page contents
$PAGE->set_pagelayout('people');
echo $OUTPUT->header();
$never_login_users=get_never_login_users();
$action = optional_param('action', '', PARAM_TEXT);
if($action=='send_email_to_never_login_user'){
    if(count($never_login_users)>0){
       
        foreach($never_login_users as $key=>$val){
            $mailto = $val->email;
            $username = $val->username;
            $newPass = update_user_password($val->id);
            $invitationContenNoEmail = $emailContentNo;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{PASSWORD}", $newPass, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            
            sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
            $invitationContenNoEmail = $emailContentNo; 
        }
            
    }
     
  //echo displayJsAlert('Send email successfully', $CFG->wwwroot."/manage/people.php");          
  echo displayJsAlert('', $CFG->wwwroot."/manage/people.php");          
}

?>

<div id="page-body">        
        

        <table class="admin-fullscreen">
            <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        
        <div id="courseList" class="focus-panel">
            <div class="panel-head">
              <h3><?php print_r(get_string('sendloginemail')) ?></h3>
            <div class="subtitle">Send a login email to every person that has never logged in</div>
            </div>
            <div class="body" style="margin-left:10px">
                <p>You currently have <b><?php echo count($never_login_users) ?></b> people that have never logged in.</p>
                <p>To send a login to all of these people click <b>Send</b></p>

                    <form method="post" action="<?php echo $CFG->wwwroot ?>/manage/never_login_email.php">
            <div class="form-buttons">
                <input type="submit" id="saveUser" class="btnlarge" value=" Send ">
                <input type="hidden" name="action" value="send_email_to_never_login_user"/>
                or
                <a href="<?php echo $CFG->wwwroot ?>/manage/people.php">cancel</a>                   
                </div>
                </form>
            </div>
        </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        

                    </div>
                </td>
            </tr>
        </tbody></table>
        

    </div>

    <?php echo $OUTPUT->footer(); ?>
