<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');


$PAGE->set_title('Lịch sử học sinh');
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
if($action=='search'){
  $idstudent = optional_param('idstudent', '', PARAM_TEXT);
  $sql_history="
  SELECT `history_student`.`id`,`schools`.`name` as school_name , `block_student`.`name` as block_name, `groups`.`name` as groups_name ,`course`.`fullname` , `school_year`.`sy_start`, `school_year`.`sy_end`
  FROM history_student
  JOIN `user` ON `user`.`id` =`history_student`.`iduser`
  JOIN `schools` ON `schools`.`id` = `history_student`.`idschool`
  JOIN `block_student` ON `block_student`.`id` = `history_student`.`idblock`
  JOIN `groups` ON `groups`.`id` = `history_student`.`idclass`
  JOIN `course` ON `course`.`id` = `history_student`.`idgroups`
  JOIN `school_year` ON `school_year`.`id` =`history_student`.`idschoolyear`
  WHERE `user`.`id`={$idstudent}   ORDER BY `school_year`.`sy_end` DESC";
  $history_student=$DB->get_records_sql($sql_history);
}
$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` JOIN `groups_members` ON `groups_members`.`userid`= `user`.`id` JOIN `groups` ON `groups`.`id` =`groups_members`.`groupid` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0  ORDER BY `user`.`id` DESC";
$data=$DB->get_records_sql($sql);
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
         <form action="" method="get">
            <div class="row mr_bottom15">
                <div class="col-md-6">
                <input type="text" class="form-control" hidden="" name="action" value="search">
                  <select name="idstudent" id="" class="form-control">
                    <option value=""><?php echo get_string('student'); ?></option>
                    <?php 
                          if($data){
                            foreach ($data as $value) {
                              ?>
                              <option value="<?php echo $value->id; ?>" <?php if(!empty($idstudent)&&($idstudent==$value->id)) echo 'selected'; ?>><?php echo $value->lastname.' '.$value->firstname; ?></option>
                              <?php 
                            }
                          }
                     ?>
                  </select>
                </div>
                <div class="col-md-6">
                  <button class="btn btn-success">Xem lịch sử</button>
                </div>
            </div>
        </form>
        <div class="table-responsive" data-pattern="priority-columns">
           <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>STT</th>
                               
                                <th>
                                    <?php echo get_string('school_year'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('schools'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('block_student'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('class'); ?>
                                </th>
                                <th>
                                    <?php echo get_string('classgroup'); ?>
                                </th>
                               
                            </tr>
                        </thead>
                        <tbody>
                           <?php 
                           if($history_student){
                            $i=0;
                            foreach ($history_student as  $value) {
                              $i++;
                              ?>
                                <tr>
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $value->sy_start.'-'.$value->sy_end; ?></td>
                                  <td><?php echo $value->school_name; ?></td>
                                  <td><?php echo $value->block_name; ?></td>
                                  <td><?php echo $value->groups_name; ?></td>
                                  <td><?php echo $value->fullname; ?></td>
                                </tr>
                              <?php 
                            }
                           }
                           ?>
                        </tbody>
                    </table>
        </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->

<?php
echo $OUTPUT->footer();
?>