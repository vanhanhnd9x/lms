<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/decentralize/lib.php');
global $USER;

require_login();
$action = optional_param('action', '', PARAM_TEXT);
$id_edit = optional_param('id_edit', '', PARAM_TEXT);
if($id_edit){
  $permissions=$DB->get_record('permissions', array(
  'id' => $id_edit
  ));
  $PAGE->set_title('Chỉnh sửa chức năng');
  $PAGE->set_heading('Chỉnh sửa chức năng');
}else{
  $PAGE->set_title('Thêm mới chức năng');
  $PAGE->set_heading('Thêm mới chức năng');
}
echo $OUTPUT->header();
$name = optional_param('name', '', PARAM_TEXT);
$pater = optional_param('pater', '', PARAM_TEXT);
$module = optional_param('module', '', PARAM_TEXT);
$type = optional_param('type', '', PARAM_TEXT);
$link = optional_param('link', '', PARAM_TEXT);
$order = optional_param('order', '', PARAM_TEXT);
$icon = optional_param('icon', '', PARAM_TEXT);

$modulell=show_module();
if($action=='add_permissions'){
  $save=add_permissions_by_cua($name,$type,$module,$link,$order,$pater,$icon);
  if($save){
     echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/decentralize/list_decentralize.php");
  }else{
     echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/decentralize/add_decentralize.php");
  }
}
if($action=='edit_permissions'){
  $update=update_permissions_by_cua($id_edit,$name,$type,$module,$link,$order,$pater,$icon);
  if($update){
     echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/decentralize/list_decentralize.php");
  }else{
     echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/decentralize/add_decentralize.php?id_edit=".$id_edit);
  }
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="card-content">
                <div class="material-datatables" style="clear: both;">
                    <form name="form1" action="" onsubmit="return matchpass()" method="post">
                        <?php 
                            if(!empty($id_edit)){
                              echo ' <input type="text" class="form-control" name="action" value="edit_permissions" hidden="">';

                            }else{
                              echo'<input type="text" class="form-control" name="action" value="add_permissions" hidden="">';
                            }
                         ?>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">Tên chức năng<span style="color:red">*</span></label>
                               <input type="text" class="form-control" placeholder="Tên để chuyển đổi song ngữ vd: schools, block_student..." name="name" value="<?php echo $permissions->name; ?>" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Thuộc<span style="color:red">*</span></label>
                                <select name="pater" id="pater" class="form-control">
                                   <option value="0">Chức năng cha</option>
                                   <?php 
                                      $pater=get_list_permissions_pater_by_cua();
                                      foreach ($pater as $value) {
                                        # code...
                                        ?>
                                        <option value="<?php echo $value->id; ?>" <?php if(!empty($permissions->pater)&&($permissions->pater==$value->id)) echo 'selected'; ?>><?php echo get_string($value->name); ?></option>
                                        <?php 
                                      }
                                    ?>
                                 </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Loại module<span style="color:red">*</span></label>
                                 <select name="module" id="module" class="form-control">
                                  <?php 
                                      foreach ($modulell as $value) {
                                        # code...
                                        ?>
                                         <option value="<?php echo $value['code']; ?>" <?php if(!empty($permissions->module)&&($permissions->module==$value['code'])) echo 'selected'; ?>><?php echo $value['name']; ?></option>
                                        <?php 
                                      }
                                   ?>
                                 </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Loại chức năng<span style="color:red">*</span></label>
                                <select name="type" id="" class="form-control">
                                  <option value="list" >Danh sách</option>
                                  <option value="add" <?php if(!empty($permissions->type)&&($permissions->type=='add')) echo 'selected'; ?>>Thêm mới</option>
                                  <option value="edit" <?php if(!empty($permissions->type)&&($permissions->type=='edit')) echo 'selected'; ?>>Chỉnh sửa</option>
                                  <option value="view" <?php if(!empty($permissions->type)&&($permissions->type=='view')) echo 'selected'; ?>>Xem chi tiết</option>
                                  <option value="delete" <?php if(!empty($permissions->type)&&($permissions->type=='delete')) echo 'selected'; ?>>Xóa</option>
                                  <option value="addassign" <?php if(!empty($permissions->type)&&($permissions->type=='addassign')) echo 'selected'; ?>>Gán</option>
                                  <option value="addassign_con" <?php if(!empty($permissions->type)&&($permissions->type=='addassign_con')) echo 'selected'; ?>>Gán trong chức năng con</option>
                                  <option value="popup" <?php if(!empty($permissions->type)&&($permissions->type=='popup')) echo 'selected'; ?>>popup</option>
                                   <option value="add_con" <?php if(!empty($permissions->type)&&($permissions->type=='add_con')) echo 'selected'; ?>>Thêm mới trong chức năng con</option>
                                   <option value="del_con" <?php if(!empty($permissions->type)&&($permissions->type=='del_con')) echo 'selected'; ?>>Xóa trong chức năng con</option>
                                    <option value="lock" <?php if(!empty($permissions->type)&&($permissions->type=='lock')) echo 'selected'; ?>>Khóa</option>
                                 </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Thứ tự sắp xếp<span style="color:red">*</span></label>
                                <input type="number" min="0" class="form-control" placeholder="Thứ tự sắp xếp" name="order" value="<?php echo $permissions->sapxep; ?>" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">Đường dẫn</label>
                                <input type="text" class="form-control" placeholder="Đường dẫn" name="link" value="<?php echo $permissions->link; ?>">
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <label class="control-label">Icon</label>
                                <input type="text" min="0" class="form-control" placeholder="VD: fa fa-clock-o;  fa-calendar" name="icon" value="<?php echo $permissions->icon; ?>">
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
                                <?php print_r(get_string('or')) ?>
                                <a href="<?php echo $CFG->wwwroot ?>/manage/decentralize/list_decentralize.php" class="btn btn-danger">
                                    <?php print_r(get_string('cancel')) ?></a>
                            </div>
                    </form>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
        <!-- Filter -->
    </div>
</div>
<?php 
  echo $OUTPUT->footer();

  ?>
  <script>
    var pater1 = $('#pater').find(":selected").val();
    if(pater1>0){
      $.get("<?php echo $CFG->wwwroot ?>/manage/decentralize/load_ajax.php?paterid=" +pater1, function(data) {
            $("#module").html(data);
      });
    }
    $('#pater').on('change', function() {
    var pater = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/decentralize/load_ajax.php?paterid=" +pater, function(data) {
            $("#module").html(data);
        });
    }
  });
  </script>


