<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/decentralize/lib.php');
$PAGE->set_title('Danh sách tác nhân');
echo $OUTPUT->header();
require_login(0, false);
// $role=lay_role_id_cua_user_dang_nhap($USER->id);

?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
       
  <div class="table-responsive" data-pattern="priority-columns">
    <table id="tech-companies-1" class="table  table-striped table-hover">
      <thead>
        <tr>
          <th class="text-center">STT</th>
          <th class="text-center">Tác nhân</th>
          <th class="text-center">Hành động</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          $tac_nhan=hien_thi_tac_nhan_he_thong();
          $i=0;
          foreach ($tac_nhan as  $value) {
            # code...
            $i++;
            echo'
              <tr>
                <td class="text-center">'.$i.'</td>
                <td class="text-center">'.$value['name'].'</td>
                <td class="text-center">
                  <a href="'.$CFG->wwwroot.'/manage/decentralize/add_decentralize_role.php?id_role='.$value['id'].'" class="btn btn-info" title="Chỉnh sửa">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                  </a>
              </tr>
            ';
          }
         ?>
            
      </tbody>
    </table>
    
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->


<?php 
echo $OUTPUT->footer();
?>
