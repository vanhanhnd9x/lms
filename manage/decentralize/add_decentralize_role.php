<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
global $USER;
require_once($CFG->dirroot . '/manage/decentralize/lib.php');
require_login();
$PAGE->set_title('Phân quyền');
$PAGE->set_heading('Phân quyền');
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$id_role = optional_param('id_role', '', PARAM_TEXT);
$permissions = optional_param('permissions', '', PARAM_TEXT);
if(empty($id_role)){
  echo displayJsAlert('', $CFG->wwwroot . "/manage/decentralize/index.php");
}
if($action=='add_permissions_role'){
  delete_permissions_role_by_cua($id_role);
  if(!empty($permissions)){
    foreach ($permissions as  $p) {
      add_permissions_role_by_cua($id_role,$p);
    }
  }
  echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/decentralize/index.php");
}


?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="card-content">
                <div class="material-datatables" style="clear: both;">
                    <form name="form1" action="" onsubmit="return matchpass()" method="post">
                      <input type="text" name="action" value="add_permissions_role" hidden="">
                        <div class="row">
                          <?php 
                            $i=0;
                            $pater=get_list_permissions_pater_by_cua();
                            foreach ($pater as  $value) {
                              $i++;
                              if($i%2==0){$class="tree";}
                              if($i%2!=0){$class="tree1";}
                              $check_p="";
                              $check_pater=kiem_tra_phan_quyen_by_cua($id_role,$value->id);
                              if($check_pater) $check_p="checked";
                              $son=get_list_son_of_permissions_pater_by_cua($value->id);

                              ?>
                              <div class="col-md-12" id="">  
                                <ul class="<?php echo $class; ?> list-unstyled " id="phanquyen">
                                   <li class=""><input type="checkbox" class="checkb " name="permissions[]" value="<?php echo $value->id; ?>" <?php echo $check_p; ?> ><?php echo get_string($value->name); ?>
                                     <?php 
                                      if($son){
                                        echo'<ul class="list-unstyled tree-son" id="">';
                                        foreach ($son as  $s) {
                                          $check_s="";
                                          $check_son=kiem_tra_phan_quyen_by_cua($id_role,$s->id);
                                          if($check_son) $check_s="checked";
                                          ?>
                                            <li>
                                              <input type="checkbox" class="checkb" name="permissions[]" value="<?php echo $s->id; ?>" <?php echo $check_s; ?>> 
                                              <a href="javascript:void(0)" class="cua"><?php echo get_string($s->name); ?></a> 
                                            </li>
                                          <?php 
                                        }
                                        echo' </ul>';
                                      }
                                     ?>
                                      
                                  </li>
                              </ul>
                            </div>
                              <?php 
                            }
                           ?>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
                                <?php print_r(get_string('or')) ?>
                                <a href="<?php echo $CFG->wwwroot ?>/manage/decentralize/index.php" class="btn btn-danger">
                                    <?php print_r(get_string('cancel')) ?></a>
                            </div>
                      </div>
                       
                    </form>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
        <!-- Filter -->
    </div>
</div>
<style>
  .tree{
    /*background: #f1556c;*/
    background: #acdea0;
    padding: 10px;
    box-shadow: 5px 5px #f3f6f8;

  }
  .tree1{
    background: #acdea0;
    padding: 10px;
    box-shadow: 5px 5px #f3f6f8;

  }
  .tree-son>li{
    padding:3px;
    border-bottom: 1px solid #ffff;
    margin-left: 15px;
  }
  .tree-son>li a{
    color:black;
  }
</style>
<script>
  $('.checkb').click(function(){
    $(this).next().find('.checkb').prop('checked',this.checked);
    $(this).parents('ul').prev('.checkb').prop('checked',function(){
      return $(this).next().find(':checked').length;
    });
  });
</script>
<?php 
  echo $OUTPUT->footer();

  ?>


