<?php
require_once('../../config.php');
global $USER, $CFG, $DB;

function show_module(){
	$modlue = array(
		'1'=>array('name'=>'Quản lý năm học','code'=>'schools-year'),
		'2'=>array('name'=>'Quản lý trường học','code'=>'schools'),
		'3'=>array('name'=>'Quản lý khối','code'=>'block-student'),
		'4'=>array('name'=>'Quản lý lớp học','code'=>'groups'),
		'5'=>array('name'=>'Quản lý nhóm lớp','code'=>'course'),
		'6'=>array('name'=>'Quản lý học sinh','code'=>'student'),
		'7'=>array('name'=>'Quản lý GVNN','code'=>'teacher'),
		'8'=>array('name'=>'Quản lý GVTG','code'=>'ta'),
		'9'=>array('name'=>'Quản lý tổ quản lý GVNN','code'=>'rht'),
		'10'=>array('name'=>'Quản lý tổ quản lý GVTG','code'=>'rhta'),
		'11'=>array('name'=>'Quản lý thời khóa biểu','code'=>'schedule'),
		'12'=>array('name'=>'Quản lý điểm unit','code'=>'unittest'),
		'13'=>array('name'=>'Quản lý điểm học kì','code'=>'semester'),
		'14'=>array('name'=>'Quản lý đánh giá GVNN','code'=>'evaluation-teacher'),
		'15'=>array('name'=>'Quản lý đánh giá GVTG','code'=>'evaluation-ta'),
		'16'=>array('name'=>'Quản lý đánh giá học sinh','code'=>'evaluation-student'),
		'17'=>array('name'=>'Quản lý tài liệu','code'=>'document'),
		'18'=>array('name'=>'Quản lý chuyên cần','code'=>'diligence'),
		'19'=>array('name'=>'Chuyên mục câu hỏi','code'=>'cat-question'),
		'20'=>array('name'=>'Ngân hàng câu hỏi','code'=>'question'),
		'21'=>array('name'=>'Quản lý bài test','code'=>'quiz'),
		'22'=>array('name'=>'Báo cáo','code'=>'report'),
		'23'=>array('name'=>'Cấu hình','code'=>'configuration'),
		'24'=>array('name'=>'Người dùng','code'=>'user'),
		'25'=>array('name'=>'Mở khóa đánh giá','code'=>'unlock_evaluation_student'),

	);
	return $modlue;
}
function hien_thi_tac_nhan_he_thong(){
  $tacnhan=array(
    '1'=>array('id'=>13,'name'=>'HOD'),
    '2'=>array('id'=>14,'name'=>'DOM'),
    '3'=>array('id'=>15,'name'=>'Report PIC'),
    '4'=>array('id'=>16,'name'=>'Scheduling'),
    '5'=>array('id'=>17,'name'=>'Admin'),
    '6'=>array('id'=>18,'name'=>'MO'),
    '7'=>array('id'=>11,'name'=>'RHT'),
    '8'=>array('id'=>8,'name'=>'GVNN'),
    '9'=>array('id'=>12,'name'=>'RHTA'),
    '10'=>array('id'=>10,'name'=>'GVTG'),
    '11'=>array('id'=>19,'name'=>'CS'),
    '12'=>array('id'=>20,'name'=>'Event'),
    '13'=>array('id'=>21,'name'=>'Administrator'),
    '14'=>array('id'=>5,'name'=>'Student'),
    // '15'=>array('id'=>3,'name'=>'SuperAdmin'),
  );
  return $tacnhan;
}
function add_permissions_by_cua($name,$type,$module,$link,$sapxep,$pater,$icon){
	global $CFG, $DB;
	$data = new stdClass();
	$data->name = trim($name);
	$data->type = $type;
	$data->module = trim($module);
	$data->link = trim($link);
	$data->sapxep = trim($sapxep);
	$data->pater = trim($pater);
	$data->icon = trim($icon);
	$lastinsertid = $DB->insert_record('permissions', $data);
	return $lastinsertid;
}
function update_permissions_by_cua($id,$name,$type,$module,$link,$sapxep,$pater,$icon){
	global $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->name = trim($name);
	$record->type = $type;
	$record->module = trim($module);
	$record->link = trim($link);
	$record->sapxep = trim($sapxep);
	$record->pater = trim($pater);
	$record->icon = trim($icon);
	return $DB->update_record('permissions', $record, false);
}
function get_list_permissions_pater_by_cua(){
	global $CFG, $DB;
	$sql="SELECT *
	FROM `permissions`
	WHERE `permissions`.`pater`=0
	ORDER BY sapxep ASC
	";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function get_list_son_of_permissions_pater_by_cua($paterid){
	global $CFG, $DB;
	$sql="SELECT *
	FROM `permissions`
	WHERE `permissions`.`pater`={$paterid}
	ORDER BY sapxep ASC
	";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function delete_permissions_by_cua($id){
	global $DB;
	$DB->delete_records_select('permissions', " pater = $id ");
	$DB->delete_records_select('permissions', " id = $id ");

}
function add_permissions_role_by_cua($role_id,$permissions_id){
	global $DB;
	$data = new stdClass();
	$data->role_id = trim($role_id);
	$data->permissions_id = $permissions_id;
	$lastinsertid = $DB->insert_record('permissions_role', $data);
	return $lastinsertid;
}
function delete_permissions_role_by_cua($role_id){
	global $DB;
	$DB->delete_records_select('permissions_role', " role_id = $role_id ");
}
function kiem_tra_phan_quyen_by_cua($role_id,$permissions_id){
	global $CFG, $DB;
    $sql = "SELECT DISTINCT `permissions_role`.`id` 
    FROM `permissions_role`
    WHERE `permissions_role`.`role_id`={$role_id}
    AND `permissions_role`.`permissions_id`={$permissions_id}
    ";
    $data = $DB->get_records_sql($sql);
  	return $data;
}

// hien thi menu o hearder lib trong common