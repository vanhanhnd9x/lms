<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/decentralize/lib.php');
$PAGE->set_title('Danh sách chức năng');
echo $OUTPUT->header();
require_login(0, false);
$page=isset($_GET['page'])?$_GET['page']:1;
?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/decentralize/add_decentralize.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
          </div>
          <div class="col-md-9 pull-right">
          
          </div>
  </div>
  <!-- <div class="table-responsive" data-pattern="priority-columns"> -->
  <div>
    <dvi class="row">
      <?php 
          $i=0;
          $pater=get_list_permissions_pater_by_cua();
          foreach ($pater as $value) {
            $i++;
            $son=get_list_son_of_permissions_pater_by_cua($value->id);
            ?>
            <div class="col-md-4 ">
              <div class="title-dau">
                 <h4 ><?php echo  get_string($value->name); ?> 
                 <a href="<?php echo $CFG->wwwroot; ?>/manage/decentralize/add_decentralize.php?id_edit=<?php echo $value->id; ?>" class="btn btn-info btn-sm tooltip-animation tooltipstered">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                  </a>
                  <a  href="javascript:void(0)" onclick="delet_permissions(<?php echo $value->id;?>);" class="btn btn-danger btn-sm tooltip-animation tooltipstered">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </a> 
                  <i class="fa fa-caret-down xem<?php echo $i; ?> cuacon" ></i>
                  <i class="fa fa-caret-up an<?php echo $i; ?> cuacon" aria-hidden="true"></i>
                 </h4>
              </div>
              <?php 
                if($son){
                  $dem=0;
                  ?>
                  <table class="table table-hover son<?php echo $i; ?>">
                      <tr>
                        <th>STT</th>
                        <th>Tên chức năng</th>
                        <th>Hành động</th>
                      </tr>
                      <?php 
                          foreach ($son as $s) {
                            $dem++;
                            echo'
                            <tr>
                              <td>'.$dem.'</td>
                              <td>'.get_string($s->name).'</td>
                              <td>
                                <a href="'.$CFG->wwwroot.'/manage/decentralize/add_decentralize.php?id_edit='.$s->id.'" class="btn btn-info btn-sm tooltip-animation tooltipstered">
                                  <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                <a href="javascript:void(0)" onclick="delet_permissions('.$s->id.');" class="btn btn-danger btn-sm tooltip-animation tooltipstered">
                                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                              </td>
                            </tr>
                            ';
                          }
                       ?>
                      
                </table>
                  <?php 
                }
               ?>
              
            </div>
            <script>
                $(".son<?php echo $i; ?>").hide();
                $(".an<?php echo $i; ?>").hide();
                $(".xem<?php echo $i; ?>").click(function(){
                    $(".son<?php echo $i; ?>").show();
                    $(".an<?php echo $i; ?>").show();
                    $(".xem<?php echo $i; ?>").hide();
                });
                 $(".an<?php echo $i; ?>").click(function(){
                    $(".son<?php echo $i; ?>").hide();
                    $(".an<?php echo $i; ?>").hide();
                    $(".xem<?php echo $i; ?>").show();
                });

            </script>
            
            <?php 
            # code...
          }
       ?>
    </dvi>
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->

<style>
 .title-dau{
    border: 1px solid #e3eaef;
    padding: 10px;
 }
 .diem-cha{
  position: absolute;
    top: 14px;
    right: 24px;
 }
 .cuacon {
    position: absolute;
    right: 25px;
}
</style>

<?php 
echo $OUTPUT->footer();
?>
<script>
    function delet_permissions(id_delete){
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/decentralize/list_decentralize.php";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/decentralize/load_ajax.php";
        var e=confirm('Bạn có muốn xóa chức năng này không?');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {id_delete:id_delete} ,
                success: function (response) {
                   window.location=urlweb;
               }
           });
        }
    }
</script>
