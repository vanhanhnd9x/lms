<?php 
require("../../config.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
// load khối theo trường
$idnamhoc=get_id_school_year_now();
if (!empty($_GET['schoolid'])) {
	$schoolid=$_GET['schoolid'];
	$sql="SELECT * from block_student where `block_student`.`schoolid`={$schoolid} AND `block_student`.`del`=0";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">'.get_string('block_student').'</option>';
	if (!empty($data)) {
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>"<?php if(!empty($_GET['blockedit'])&&$_GET['blockedit']== $value->id) echo'selected';?>><?php echo $value->name; ?></option>
			<?php 
		}
	}else{
		?>
		<script>
			$("#groupid").html('<option value=""><?php echo get_string('class'); ?></option>');
			$("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
		</script>
		<?php 
	}
}

// load lớp theo khối
if(!empty($_GET['block_student'])){
	$block_studentid=$_GET['block_student'];
	
	# code...
	$sql="SELECT `groups`.`id`,`groups`.`name` from groups
	JOIN `block_student` ON `block_student`.`id`=`groups`.`id_khoi`
	where `groups`.`id_khoi`={$block_studentid} AND `block_student`.`del`=0
	AND  `groups`.`id_namhoc`={$idnamhoc}
	";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">'. get_string('class').'</option>';
	if($data){
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" <?php if(!empty($_GET['groupedit'])&&$_GET['groupedit']== $value->id) echo'selected';?>><?php echo $value->name; ?></option>
			<?php 
		}
	}else{
		?>
		<script>
			$("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
		</script>
		<?php 
	}
	
	
}
// load nhom lop
if(!empty($_GET['groupid'])){
	$groupid=$_GET['groupid'];
	$groupss=$DB->get_record('groups', array(
	  'id' => $groupid
	));
	$sql="SELECT `course`.`id`,`course`.`fullname` FROM `course`
	JOIN `group_course` ON `group_course`.`course_id` = `course`.`id`
	WHERE `group_course`.`group_id`={$groupid}
	";
	$data = $DB->get_records_sql($sql);

	echo'<option value="">'.get_string('classgroup').'</option>';
	if ($data) { 
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" <?php if(!empty($_GET['courseedit'])&&($_GET['courseedit']== $value->id)) echo'selected'; ?>><?php echo $value->fullname; ?></option>
			<?php 
		}
		?>
		<script>
			var idschool_year="<?php echo  $groupss->id_namhoc;?>"
			$("#school_yearid_semester").val(idschool_year).change();
		</script>
		<?php
	}else{
		?>
		<script>
			$("#studentid").html('<option value=""><?php echo get_string('student') ?></option>');
		</script>
		<?php 
	}
}
// load  hoc sinh trong nhom
if (!empty($_GET['courseid'])) {
	# code...
	$courseid=$_GET['courseid'];
	$sql="SELECT DISTINCT `user`.`id`,`user`.`firstname`,`user`.`lastname`,`role_assignments`.`roleid`
	FROM `course` JOIN `enrol` ON `enrol`.`courseid` = `course`.`id` 
	JOIN `user_enrolments` ON `user_enrolments`.`enrolid` = `enrol`.`id`
	JOIN `user` ON `user_enrolments`.`userid` = `user`.`id`
	JOIN `role_assignments` ON `user`.id = `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 
	AND `course`.`id`={$courseid} ";
	$data = $DB->get_records_sql($sql);
	echo'<option value="">Chọn học sinh</option>';
	if ($data) { 
		foreach ($data as $key => $value) {
			?>
			<option value="<?php echo $value->id; ?>" ><?php echo $value->firstname,'',$value->lastname; ?></option>
			<?php 
		}
	}
}



// load số ngày học trong thời khóa biểu
if (!empty($_GET['number'])) {
	$number=$_GET['number'];
	# code...
	if (($number>0)) {
		# code...
		for ($i=0; $i < $number; $i++) { 
			# code...
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="alert-info">
						<h4><?php echo get_string('study_day'); ?> <?php echo $i+1; ?></h4>
					</div>
				</div>
				<div class="col-md-6">
					<label for=""><?php echo get_string('choose_the_date'); ?> <span style="color:red">*</span></label></label>
					<select name="day[]" id="" class="form-control" required="">
						<option value="2"><?php echo get_string('monday'); ?></option>
						<option value="3"><?php echo get_string('tuesday'); ?></option>
						<option value="4"><?php echo get_string('wednesday'); ?></option>
						<option value="5"><?php echo get_string('thursday'); ?></option>
						<option value="6"><?php echo get_string('friday'); ?></option>
						<option value="7"><?php echo get_string('saturday'); ?></option>
						<option value="8"><?php echo get_string('sunday'); ?></option>
					</select>
				</div>
				<div class="col-md-6">
					<label for=""><?php echo get_string('class_room'); ?><span style="color:red">*</span></label></label>
					<input type="text" name="location[]" class="form-control" required="">
				</div>
				<div class="col-md-6">
					<label for=""><?php echo get_string('time_start'); ?><span style="color:red">*</span></label></label>
					<input type="text" class="form-control" name="time_start[]" id="time_start<?php echo $i;?>">
				</div>


				<div class="col-md-6">
					<label for=""><?php echo get_string('time_end'); ?><span style="color:red">*</span></label></label>
					<input type="text" class="form-control" name="time_end[]" id="time_end<?php echo $i;?>" >
				</div>
				<div class="col-md-12">
					<label for=""><?php echo get_string('note'); ?></label>
					<textarea name="note[]" class="form-control"></textarea>
				</div>
			</div>

			<script type="text/javascript">
				// $(function () {
				// 	$('#time_start<?php echo $i;?>').datetimepicker({
				// 		format: 'H:m',        
				// 	});
				// 	$('#time_end<?php echo $i;?>').datetimepicker({
				// 		format: 'H:m',

				// 	});
				// 	$('#time_start').timepicker({});
				// });
				var d = new Date();
				$('#time_start<?php echo $i;?>').timepicker({
					timeFormat: 'H:mm ',
					interval: 10,
					minTime: '8:00am',
					maxTime: '10:00pm',
					defaultTime: d.getHours()+':00',
					startTime: '8:00',
					// dynamic: false,
					dropdown: true,
					scrollbar: true
				});
				$('#time_end<?php echo $i;?>').timepicker({
					timeFormat: 'H:mm ',
					interval: 10,
					minTime: '8:00am',
					maxTime: '10:00pm',
					defaultTime: d.getHours()+':00',
					startTime: '8:00',
					// dynamic: false,
					dropdown: true,
					scrollbar: true
				});
			</script>
			<?php 
		}
		?>

		

		<?php 
	}

}

if(!empty($_POST['idscheduledelete'])){
	global $DB;
	$del=1;
	$id=$_POST['idscheduledelete'];
	$delete=delete_schedule($id,$del);
} 

// load gvtg va gvnn cua nhom lop
if(!empty($_GET['courseforteacher'])){
	global $DB;
	$id=$_GET['courseforteacher'];
	$course =$DB->get_record('course', array(
	  'id' => $id
	));
	$idgvnn=lay_id_gvnn_thuoc_nhom_lop_by_cua($id);
	$idgvtg=lay_id_gvtg_thuoc_nhom_lop_by_cua($id); 
	if($course){
		?>
		<!-- <div class="row"> -->
		<div class="col-md-6">
                <label class="control-label"><?php echo get_string('GVNN'); ?></label>
                <select name="gvnn" id="gvnn" class="form-control">
                  <option value=""><?php echo get_string('GVNN'); ?></option>
                  <?php 
                      $gvnn=all_gvnn_schedule();
                      foreach ($gvnn as $key => $value) {
                        # code...
                        ?>
                        <option value="<?php echo $value->id; ?>" <?php if(!empty($idgvnn)&&($idgvnn==$value->id)) echo'selected'; ?>><?php echo $value->lastname.''.$value->firstname; ?></option>
                        <?php 
                       
                      }
                   ?>
                </select>
              </div>
              <div class="col-md-6">
                <label class="control-label"><?php echo get_string('GVTG'); ?></label>
                <select name="gvtg" id="gvtg" class="form-control" >
                  <option value=""><?php echo get_string('GVTG'); ?></option>
                   <?php 
                      $gvtg=all_gvtg_schedule();
                      foreach ($gvtg as $key => $tg) {
                         ?>
                        <option value="<?php echo $tg->id; ?>" <?php if(!empty($idgvtg)&&($idgvtg==$tg->id)) echo'selected'; ?>><?php echo $tg->lastname.''.$tg->firstname; ?></option>
                        <?php 
                      }
                   ?>
                </select>
              </div>
          <!-- </div> -->
		<?php 
	}

}


// load lich hoc theo tung nhom lop thuc hien chuc nang tren man hinh dang nhap cua gv
if(!empty($_GET['courseid_to_schedule'])){
	$courseid=$_GET['courseid_to_schedule'];
	$school_year=get_id_namhoc_theo_nhom_schedule($courseid);
	$lichhoc=hien_thi_lich_hoc_theo_nhom_cua_gv_login($courseid,$school_year);

	?> 
	<table class="table table-bordered">
	 <thead>
        <tr>
          <th data-priority="6">STT</th>
          <th data-priority="6"><?php echo get_string('day'); ?></th>
          <th data-priority="6"><?php echo get_string('time'); ?></th>
          <th data-priority="6"><?php echo get_string('syllabus_name'); ?></th>
          <th data-priority="6"><?php echo get_string('classgroup'); ?></th>
          <th data-priority="6"><?php echo get_string('class_room'); ?></th>
          <th data-priority="6"><?php echo get_string('GVNN'); ?></th>
          <th data-priority="6"><?php echo get_string('GVTG'); ?></th>
          
        </tr>
      </thead>
      <tbody>
      	<?php 
	      	if(!empty($lichhoc)){
	      		$i=0;
	      		foreach ($lichhoc as  $value) {
	      			$i++;
	      			switch ($value->day) {
				        case 2: $day= get_string('monday'); break;
				        case 3: $day= get_string('tuesday'); break;
				        case 4: $day=  get_string('wednesday'); break;
				        case 5: $day=  get_string('thursday'); break;
				        case 6: $day=  get_string('friday'); break;
				        case 7: $day=  get_string('saturday'); break;
				        case 8: $day=  get_string('sunday'); break;
				     }
	      			?>
	      			<tr>
	      				<td><?php echo $i; ?></td>
	      				<td><?php echo $day; ?></td>
	      				<td><?php echo $value->time_start,'-',$value->time_end; ?></td>
	      				<td><?php echo $value->book; ?></td>
	      				<td><?php echo $value->fullname; ?></td>
	      				<td><?php echo $value->location; ?></td>
	      				<?php echo show_name_gvnn_gvtg_course($value->courseid); ?>
	      			</tr>
	      			<?php 
	      		}
	      	} 
      	?>
      </tbody>
 	</table>
	<?php 

}
?>
