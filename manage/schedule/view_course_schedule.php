<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
global $USER;

require_login();
$PAGE->set_title(get_string('view_schedule'));
$PAGE->set_heading(get_string('view_schedule'));
echo $OUTPUT->header();
$courseid = optional_param('courseid', '', PARAM_TEXT);
function lay_id_lop_trong_thoikhoabieu($courseid){
	global $DB;
	$sql="SELECT DISTINCT `groups`.`id` FROM `groups` JOIN `group_course` on `group_course`.`group_id` =`groups`.`id` WHERE `group_course`.`course_id`={$courseid}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  	return $data;
}
$groupid=lay_id_lop_trong_thoikhoabieu($courseid);
$group=$DB->get_record('groups', array(
	'id' => $groupid
));
$schedule=$DB->get_record('schedule', array(
	'groupid' => $groupid,
	'courseid' => $courseid,
	'school_yearid' => $group->id_namhoc,
	'del' => 0,
));
$course=$DB->get_record('course', array(
	'id' => $courseid
));
$block_student=$DB->get_record('block_student', array(
	'id' => $group->id_khoi
));
$school=$DB->get_record('schools',array('id'=>$block_student->schoolid));
$list_schedule=$DB->get_records('schedule_info', array('scheduleid'=>$schedule->id), $sort='', $fields='*', $limitfrom=0, $limitnum=10);


function show_name_gvnn_gvtg_course_2($idcourse){
	global $DB;
	$sql_idteacher=" SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` ={$idcourse} AND `role_assignments`.`roleid`= 8";
	$idgvnn=$DB->get_field_sql($sql_idteacher,null,$strictness=IGNORE_MISSING);
	$gvnn=$DB->get_record('user', array(
		'id' => $idgvnn,
		'del'=>0
	));
	$sql_idta=" SELECT DISTINCT  `user`.`id`, `user`.`firstname`, `user`.`lastname`,`course`.`id` as courseid
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id` ={$idcourse} AND `role_assignments`.`roleid`= 10";
	$idgvtg=$DB->get_field_sql($sql_idta,null,$strictness=IGNORE_MISSING);
	$gvtg=$DB->get_record('user', array(
			'id' => $idgvtg,
			'del'=>0
		));
	
	echo'
	<div class="col-md-6">
	<label class="control-label">'.get_string('GVNN').'</label> 
	<input type="text" class="form-control" disabled="" value="'.$gvnn->lastname.' '.$gvnn->firstname.'">
	<div class="" id="return_teacher"></div>
	</div>
	<div class="col-md-6">
	<label class="control-label">'.get_string('GVTG').'</label>
	<input type="text" class="form-control" disabled="" value="'.$gvtg->lastname.' '.$gvtg->firstname.'">
	<div class="" id="return_ta"></div>
	</div>
	';
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id); 
$moodle='course';
$name1='view_schedule';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<div class="material-datatables" style="clear: both;">
					<form  action="" onsubmit="return validate();" method="post">
						<input type="text" class="form-control" name="action" value="add_schedule" hidden="">
						<div class="row">
							<div class="col-md-6">
								<label class="control-label"><?php echo get_string('schools'); ?></label>
								<input type="text" class="form-control" disabled="" value="<?php echo $school->name; ?>">
							</div>
							<div class="col-md-6">
								<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
								<input type="text" class="form-control" disabled="" value="<?php echo $block_student->name; ?>">
							</div>
							<div class="col-md-6">
								<label class="control-label"><?php echo get_string('class'); ?> <span style="color:red">*</span></label>
								<input type="text" class="form-control" disabled=""  value="<?php echo $group->name; ?>">
							</div>
							<div class="col-md-6">
								<label class="control-label"><?php echo get_string('classgroup'); ?> <span style="color:red">*</span></label>
								<input type="text" class="form-control" disabled=""  value="<?php echo $course->fullname; ?>">
							</div>
							
							<?php 
							echo show_name_gvnn_gvtg_course_2($course->id); 
							// $schedulelist=get_all_scheduleinfo_by_idschedule($schedule->id);
							$schedulelist=$DB->get_records('schedule_info', array('scheduleid'=>$schedule->id), $sort='', $fields='*', $limitfrom=0, $limitnum=10);
							?>
							<div class="col-md-12" id="">
								<label for="" class="control-label"><?php echo get_string('detail'); ?></label>
								<table class="table table-bordered">
									<tr>
										<th><?php echo get_string('day'); ?></th>
										<th><?php echo get_string('time'); ?></th>
										<th><?php echo get_string('syllabus_name'); ?></th>
										<th><?php echo get_string('class_room'); ?></th>
									</tr>
									<?php 
									if(!empty($schedulelist)){
										foreach ($schedulelist as $key1 => $list) {
											switch ($list->day) {
												case 2: $day= get_string('monday'); break;
												case 3: $day= get_string('tuesday'); break;
												case 4: $day=  get_string('wednesday'); break;
												case 5: $day=  get_string('thursday'); break;
												case 6: $day=  get_string('friday'); break;
												case 7: $day=  get_string('saturday'); break;
												case 8: $day=  get_string('sunday'); break;
											}

											echo'
											<tr>
											<td>'.$day.'</td>
											<td>'. $list->time_start.' - '.$list->time_end.'</td>
											<td>'.$schedule->book.'</td>
											<td>'.$list->location.'</td>
											</tr>
											';
										}
									}
									
									?>

								</table>
							</div>

							<br>
							<br>
							<div class="col-md-12">
								<a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
							</div>
						</form>
					</div>
					<!-- end content-->
				</div>
				<!--  end card  -->
			</div>
			<!-- end col-md-12 -->
		</div>
		
	</div>
	<?php 
	echo $OUTPUT->footer();

	?>

	<script>
  // var conceptName = $('#schoolid').find(":selected").val();
  // if (conceptName) {
  //     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+conceptName,function(data){
  //       $("#block_student").html(data);
  //     });
  //   }
  // alert(conceptName);
  $('#schoolid').on('change', function() {
  	var cua = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
  			$("#block_student").html(data);
  		});
  	}
  });
  // hiển thị lịch giảng của giáo viên 
  $('#teacherid').on('change', function() {
  	var teacher = this.value ;
  	if (this.value) {
  		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?teacherid="+teacher,function(data){
  			$("#return_teacher").html(data);
  		});
  	}
  });
// hiển thị lich giảng của trợ giảng viên
$('#taid').on('change', function() {
	var ta = this.value ;
	if (this.value) {
		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?taid="+ta,function(data){
			$("#return_ta").html(data);
		});
	}
});
$('#number').on('change keyup', function() {
	var sanitized = $(this).val();
	if(sanitized){
		$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?number="+sanitized,function(data){
			$("#return_number").html(data);
		});
	}
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
