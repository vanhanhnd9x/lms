<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;

function add_new_schedule($courseid,$groupid,$book,$school_yearid){
	global $CFG, $DB;
	$data = new stdClass();
	$data->courseid = $courseid;
	$data->groupid = $groupid;
	$data->book = trim($book);
	$data->school_yearid = trim($school_yearid);
	$data->del = 0;
	$lastinsertid = $DB->insert_record('schedule', $data);
	return $lastinsertid;
}
function update_schedulei($d,$courseid,$groupid,$book,$school_yearid){
	global $DB;
	$record = new stdClass();
	$record->id = $d;
	$record->courseid = $courseid;
	$record->groupid = $groupid;
	$record->book = trim($book);
	$record->school_yearid = trim($school_yearid);
	return $DB->update_record('schedule', $record, false);
}
function add_new_schedule_info($scheduleid,$day,$time_start,$time_end,$location,$note){
	global $CFG, $DB;
	$data = new stdClass();
	$data->scheduleid = $scheduleid;
	$data->day = $day;
	$data->time_start = $time_start;
	$data->time_end = $time_end;
  $data->location =trim($location);
	$data->note =trim($note);
	$lastinsertid = $DB->insert_record('schedule_info', $data);
	return $lastinsertid;
}
function update_schedule_info($id,$day,$time_start,$time_end,$location,$note){
	global $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->day = $day;
	$record->time_start = $time_start;
	$record->time_end = $time_end;
  $record->location = trim($location);
	$record->note = trim($note);
	return $DB->update_record('schedule_info', $record, false);
}
function update_gvnn_gvtg_course_in_schedule($id,$gvnn,$gvtg){
	global $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->id_gv = $gvnn;
	$record->id_tg = $gvtg;
	return $DB->update_record('course', $record, false);
}
function check_schedule($courseid,$school_year){
  global $DB;
  $sql="SELECT `schedule`.`id` FROM `schedule` WHERE `schedule`.`courseid`={$courseid} AND `schedule`.`school_yearid`={$school_year} AND `schedule`.`del`=0";
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
// lấy 1 duy nhất field từ bảng data muốn lấy
function get_info_course_in_schedule($id){
	global $CFG, $DB;
	if (!empty($id)) {
		$sql = "SELECT `course`.`fullname`
		FROM `course`
		WHERE `course`.`id` = :courseid";
		$params = array('courseid' => $id);
		$data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
		return $data;
	}
}
function get_info_course_in_schedule2($idcourse,$idschoolyear){
  global $DB;
  $sql="SELECT DISTINCT `history_course_name`.`course_name` 
  FROM `history_course_name`
  JOIN `course` ON `history_course_name`.`courseid`= `course`.`id`
  WHERE `course`.`id`={$idcourse} AND `history_course_name`.`school_year_id`={$idschoolyear}";
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function get_name_group_in_schedule($id){
	global $CFG, $DB;
	if (!empty($id)) {
		$sql = "SELECT `groups`.`name`
		FROM `groups`
		WHERE `groups`.`id` = :groupsid";
		$params = array('groupsid' => $id);
		$data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
		return $data;
	}
}

function get_all_scheduleinfo_by_idschedule($id){
	global $CFG, $DB;
	$sql="SELECT *
	FROM `schedule_info`
	WHERE `schedule_info`.`scheduleid`={$id}
	";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function delete_schedule($id,$del){
	global $CFG, $DB;
	$record = new stdClass();
	$record->id = $id;
	$record->del = $del;
	
	return $DB->update_record('schedule', $record, false);
}

function get_all_schedule($page=null,$number=null){
    global $CFG, $DB;
    if ($page>1) {
        $start=(($page-1)*$number);
    }else{
        $start=0;
    }
    $sql="SELECT `schedule`.* from schedule 
    JOIN `course` ON `course`.`id` = `schedule`.`courseid`
    JOIN `groups` ON `groups`.`id` = `schedule`.`groupid`
    JOIN `block_student` ON `groups`.`id_khoi` = `block_student`.`id`
    WHERE `schedule`.`del`=0 AND `block_student`.`del` = 0  LIMIT {$start} , {$number} ";
    $data=$DB->get_records_sql($sql);
    $sql_total="SELECT COUNT( DISTINCT `schedule`.`id`) from schedule WHERE `schedule`.`del`=0 ";
    $total_row=$DB->get_records_sql($sql_total);

    foreach ($total_row as $key => $value) {
        # code...
        $total_page=ceil((int)$key / (int)$number);
        break;
    }
    $kq=array(
        'data'=>$data,
        'total_page'=>$total_page,
    );
    return $kq;
}

function all_gvnn_schedule(){
	global $DB;
	$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`role_assignments`.`roleid` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 8 AND `user`.`del`=0 AND `user`.`id` NOT IN (2)";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function all_gvtg_schedule(){
	global $DB;
	$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`role_assignments`.`roleid` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 10 AND `user`.`del`=0 AND `user`.`id` NOT IN (2)";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function search_schedule($name=null,$page,$number,$schoolid=null,$block_student=null,$groupid=null,$courseid=null,$school_year=null){
  global $CFG, $DB;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="
  SELECT DISTINCT  `schedule`.`id`, `schedule`.`groupid`,`schedule`.`courseid`,`schedule`.`book`,`schedule`.`school_yearid`,`schools`.`name` from schedule
  JOIN `course` ON `course`.`id` = `schedule`.`courseid`
  JOIN `groups` ON `groups`.`id` = `schedule`.`groupid`
  JOIN `block_student` ON `groups`.`id_khoi` = `block_student`.`id`
  JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
  JOIN `schools` ON `schools`.`id` =`groups`.`id_truong`
  WHERE `schedule`.`del`=0 AND `block_student`.`del` = 0 AND `schools`.`del` =0
   ";
  $sql_total="SELECT COUNT( DISTINCT `schedule`.`id`) from schedule JOIN `course` ON `course`.`id` = `schedule`.`courseid`
  JOIN `groups` ON `groups`.`id` = `schedule`.`groupid`
  JOIN `block_student` ON `groups`.`id_khoi` = `block_student`.`id`
  JOIN `schools` ON `schools`.`id` =`groups`.`id_truong`
  JOIN `history_course_name` ON `history_course_name`.`courseid`= `course`.`id`
  WHERE `schedule`.`del`=0 AND `block_student`.`del` = 0 AND `schools`.`del` =0 ";
  if(!empty($schoolid)&&empty($block_student)&&empty($groupid)){
    // $sql.=" AND `groups`.`id_truong`={$schoolid} ";
    // $sql_total.=" AND `groups`.`id_truong`={$schoolid} ";
    $sql.="AND `schedule`.`id` IN (SELECT `schedule`.`id` FROM `schedule` JOIN `groups` ON `schedule`.`groupid`= `groups`.`id` JOIN `block_student`ON`block_student`.`id`=`groups`.`id_khoi` JOIN `schools` ON `schools`.`id` =`block_student`.`schoolid` WHERE `schools`.`id`= {$schoolid} )";
    $sql_total.=" AND `schedule`.`id` IN (SELECT `schedule`.`id` FROM `schedule` JOIN `groups` ON `schedule`.`groupid`= `groups`.`id` JOIN `block_student`ON`block_student`.`id`=`groups`.`id_khoi` JOIN `schools` ON `schools`.`id` =`block_student`.`schoolid` WHERE `schools`.`id`= {$schoolid} )";
  }
  if(!empty($schoolid)&&!empty($block_student)&&empty($groupid)){
    $sql.="AND `schedule`.`id` IN (SELECT `schedule`.`id` FROM `schedule` JOIN `groups` ON `schedule`.`groupid`= `groups`.`id` JOIN `block_student`ON`block_student`.`id`=`groups`.`id_khoi`  WHERE `block_student`.`id`= {$block_student} )";
    $sql_total.="AND `schedule`.`id` IN (SELECT `schedule`.`id` FROM `schedule` JOIN `groups` ON `schedule`.`groupid`= `groups`.`id` JOIN `block_student`ON`block_student`.`id`=`groups`.`id_khoi`  WHERE `block_student`.`id`= {$block_student} )";
  } 
  if(!empty($schoolid)&&!empty($block_student)&&!empty($groupid)){
    $sql.=" AND `schedule`.`groupid`={$groupid}";
    $sql_total.=" AND `schedule`.`groupid`={$groupid}";
  }   
  if(!empty($courseid)){
    $sql.=" AND `schedule`.`courseid`={$courseid}";
    $sql_total.=" AND `schedule`.`courseid`={$courseid}";
  }
  if(!empty($school_year)){
    $sql.=" AND `schedule`.`school_yearid`={$school_year}";
    $sql_total.=" AND `schedule`.`school_yearid`={$school_year}";
  }
  if(!empty($name)){
    $sql.="AND `schedule`.`book`LIKE '%{$name}%'";
    $sql_total.="AND `schedule`.`book`LIKE '%{$name}%'";
  }
  $sql.=" LIMIT {$start} , {$number}";
  $data=$DB->get_records_sql($sql);

  $total_row=$DB->get_records_sql($sql_total);

  foreach ($total_row as $key => $value) {
        # code...
    $total_page=ceil((int)$key / (int)$number);
    break;
  }
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page,
  );
  return $kq;
}
function hien_thi_lich_hoc_cu_the_cua_nhom_lop($idschedule){
  global $DB;
  $data=get_all_scheduleinfo_by_idschedule($idschedule);
  if($data){
    foreach ($data as $value) {
      switch ($value->day) {
        case 2: $day= get_string('monday'); break;
        case 3: $day= get_string('tuesday'); break;
        case 4: $day=  get_string('wednesday'); break;
        case 5: $day=  get_string('thursday'); break;
        case 6: $day=  get_string('friday'); break;
        case 7: $day=  get_string('saturday'); break;
        case 8: $day=  get_string('sunday'); break;
      }
      echo $day,'|',$value->time_start,'-',$value->time_end,'|',$value->location,'<br>';
    }
  }
}
function assign_user_to_course_3($user_id, $course_id,$roleid) {
  global $CFG, $DB;
  require_once($CFG->dirroot . '/lib/enrollib.php');

  $enrol = enrol_get_plugin('manual');

  $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');

  $instance = reset($instances);
  $enrol->enrol_user($instance, $user_id, $roleid, '', '');
}
function lay_id_gvnn_thuoc_nhom_lop_by_cua($courseid){
    global $DB;
    $sql="SELECT DISTINCT  `user`.`id`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id`={$courseid}
    AND `role_assignments`.`roleid`= 8 AND `user`.`id` NOT IN (2)";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function lay_id_gvtg_thuoc_nhom_lop_by_cua($courseid){
    global $DB;
    $sql="SELECT DISTINCT  `user`.`id`
    FROM `user` 
    JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
    JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
    JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
    WHERE `course`.`id`={$courseid}
    AND `role_assignments`.`roleid`= 10 AND `user`.`id` NOT IN (2)";
    $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}
function remove_person_from_course_3($person_id, $course_id) {
  global $DB, $CFG;

  require_once($CFG->dirroot . '/lib/enrollib.php');
  $enrol = enrol_get_plugin('manual');
  $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
  $instance = reset($instances);

  $enrol->unenrol_user($instance, $person_id);
}

function show_name_gvnn_gvtg_course($idcourse){
  global $DB;
  $course=$DB->get_record('course', array(
    'id' => $idcourse));
  $idgvnn=lay_id_gvnn_thuoc_nhom_lop_by_cua($idcourse);
  $idgvtg=lay_id_gvtg_thuoc_nhom_lop_by_cua($idcourse);
  if(!empty($idgvnn)){
    $gvnn=$DB->get_record('user', array(
      'id' => $idgvnn,
      'del'=>0
  ));
  }
  if(!empty($idgvtg)){
    $gvtg=$DB->get_record('user', array(
    'id' => $idgvtg,
    'del'=>0
  ));
  }
  echo'
    <td>'.$gvnn->lastname.' '.$gvnn->firstname.'</td>
    <td>'.$gvtg->lastname.' '.$gvtg->firstname.'</td>
  ';
}



?>