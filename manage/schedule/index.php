<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');


$PAGE->set_title(get_string('schedule_management'));
echo $OUTPUT->header();
require_login(0, false);
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$name = trim(optional_param('name', '', PARAM_TEXT));
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$school_year = optional_param('school_year', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
 $cua=  search_schedule($name,$page,$number,$schoolid,$block_student,$groupid,$courseid,$school_year);
if($action=='search'){
  // $cua= search_schedule($name,$page,$number,$schoolid,$block_student,$groupid);
 
  // $url=$CFG->wwwroot.'/manage/manage_score/schedule.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&groupid='.$groupid.'&name='.$name.'&page=';
  $url=$CFG->wwwroot.'/manage/schedule/index.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&school_year='.$school_year.'&groupid='.$groupid.'&courseid='.$courseid.'&name='.$name.'&page=';
}else{
  // $cua=get_all_schedule($page,$number);
  $url=$CFG->wwwroot.'/manage/schedule/index.php?page=';
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schedule';
$type='list';
$name1='list_schedule';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <?php 
              if(!empty($checkthemmoi)){
                ?>
                <div class="col-md-2">
                  <a href="<?php echo new moodle_url('/manage/schedule/addnew.php'); ?>" class=" btn btn-success"><?php echo get_string('new'); ?></a>
                </div>
                <?php 
              }
          ?>
          
          <div class="col-md-9 pull-right">
            <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
            <!--  <div class="col-4">
              <label for=""><?php echo get_string('schools'); ?></label>
              <select name="schoolid" id="schoolid" class="form-control">
               <option value=""><?php echo get_string('schools'); ?></option>
               <?php 
               $schools = get_all_shool();
               foreach ($schools as $key => $value) {
                ?>
                <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                <?php 
              }
              ?>
            </select>
          </div>
          <div class="col-4">
            <label for=""><?php echo get_string('block_student'); ?></label>
            <select name="block_student" id="block_student" class="form-control">
             <option value=""><?php echo get_string('block_student'); ?></option>
           </select>
         </div>
         <div class="col-4">
          <label for=""><?php echo get_string('class'); ?></label>
          <select name="groupid" id="groupid" class="form-control">
            <option value=""><?php echo get_string('class'); ?></option>
          </select>
        </div> -->
        <div class="col-4 form-group">
          <label for="">
              <?php echo get_string('schools'); ?> </label>
          <select name="schoolid" id="schoolid" class="form-control">
               <option value="">
                  <?php echo get_string('schools'); ?>
              </option>
              <?php 
              $schools = get_all_shool();
              foreach ($schools as $key => $value) {
              ?>
              <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>>
                  <?php echo $value->name; ?>
              </option>
              <?php 
                }
                ?>
          </select>
      </div>
      <div class="col-4 form-group">
          <label for="">
              <?php echo get_string('block_student'); ?></label>
          <select name="block_student" id="block_student" class="form-control">
              <option value="">
                  <?php echo get_string('block_student'); ?>
              </option>
          </select>
      </div>
      <div class="col-4">
          <label for=""><?php echo get_string('choose_the_school_year'); ?></label>
          <select name="school_year" id="school_yearid" class="form-control" >
            <option value=""><?php echo get_string('choose_the_school_year'); ?></option>
                                          
          </select>
      </div>
      <div class="col-4 form-group">
          <label for="">
              <?php echo get_string('class'); ?></label>
          <select name="groupid" id="groupid" class="form-control">
              <option value="">
                  <?php echo get_string('class'); ?>
              </option>
          </select>
      </div>
      <div class="col-md-4">
        <label class="control-label"><?php echo get_string('classgroup'); ?> </label>
        <select name="courseid" id="courseid" class="form-control" >
          <option value=""><?php echo get_string('classgroup'); ?></option>
        </select>
      </div>
        <div class="col-4">
          <label for=""><?php echo get_string('syllabus_name'); ?></label>
          <input type="" name="name" class="form-control" placeholder="<?php echo get_string('syllabus_name'); ?>" value="<?php echo trim($name); ?>">
        </div>
        <div class="col-4">
          <div style="    margin-top: 29px;"></div>
          <button type="submit" class="btn btn-success"><?php echo get_string('search'); ?></button>
        </div>
      </form>
    </div>
  </div>
  <div class="table-responsive" data-pattern="priority-columns">
    <table id="tech-companies-1" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th class="form-control">STT</th>
          <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-center">'.get_string('action').'</th>';} ?>
          <th><?php echo get_string('nameschools'); ?></th>
          <th><?php echo get_string('classgroup'); ?></th>
          <th><?php echo get_string('syllabus_name'); ?></th>
          <th><?php echo get_string('GVNN'); ?></th>
          <th><?php echo get_string('GVTG'); ?></th>
          <th><?php echo get_string('schedule'); ?></th>
          <th><?php echo get_string('school_year'); ?></th>
          
        </tr>
      </thead>
      <tbody>
        <?php 
        if (!empty($cua['data'])) {
                                # code...
          if($page==1){
              $i=0;
          }else{
              $i=($page-1)*$number;
          } 
          foreach ($cua['data'] as $schedule) { 
            $i++;
            // $course=get_info_course_in_schedule($schedule->courseid);
            $course=get_info_course_in_schedule2($schedule->courseid,$schedule->school_yearid);
            $groups=get_name_group_in_schedule($schedule->groupid);
            ?>
            <tr>
              <td class="text-center"><?php echo $i; ?></td>
              <?php 
                if(!empty($hanhdong)||!empty($checkdelete)){
                  echo'<td class="text-center">';
                  $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$schedule->id);
                  if(!empty($checkdelete)){
                   ?>
                  <a  href="javascript:void(0)" onclick="delet_schedule(<?php echo $schedule->id;?>);" class="mauicon " title="Xóa">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </a>
                   <?php 
                  }
                  echo'</td>';
                }
              ?>
              <td><?php echo  $schedule->name;?></td>
              <td><?php echo  $course;?></td>
              <td><?php echo $schedule->book; ?></td>
              <?php echo show_name_gvnn_gvtg_course($schedule->courseid); ?>
              <td><?php echo hien_thi_lich_hoc_cu_the_cua_nhom_lop($schedule->id); ?></td>
              <td><?php  echo show_show_yeah_in_unit($schedule->school_yearid);?></td>
             <!--  <td class="text-right">
                <a href="<?php echo $CFG->wwwroot ?>/manage/schedule/edit.php?id_edit=<?php echo $schedule->id ?>" class="btn btn-info" title="Chỉnh sửa">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="<?php echo $CFG->wwwroot ?>/manage/schedule/view.php?viewid=<?php echo $schedule->id ?>" class="btn btn-warning" title="Xem chi tiết">
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </a>
                <a  href="javascript:void(0)" onclick="delet_schedule(<?php echo $schedule->id;?>);" class="btn btn-danger" title="Xóa">
                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
              </td> -->
               
            </tr>
          <?php }
        }
        ?>
      </tbody>
    </table>
    <?php 
    if ($cua['total_page']) {
      if ($page > 2) { 
        $startPage = $page - 2; 
      } else { 
        $startPage = 1; 
      } 
      if ($cua['total_page'] > $page + 2) { 
        $endPage = $page + 2; 
      } else { 
        $endPage =$cua['total_page']; 
      }
    }
    ?>
    <p><?php echo get_string('total_page'); ?>: <?php echo $cua['total_page'] ?></p>
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <?php 
        for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
          ?>
          <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
          <?php 
        }
        ?>
        <li class="page-item <?php if(($page==$cua['total_page'])) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<script>
  function delet_schedule(idscheduledelete){
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/schedule/index.php";
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php";
    var e=confirm('<?php echo get_string('delete_schedule'); ?>');
    if(e==true){
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {idscheduledelete:idscheduledelete} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
//   var school = $('#schoolid').find(":selected").val();
//   if (school) {
//     var blockedit = "<?php echo $block_student; ?>";
//     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
//       $("#block_student").html(data);
//     });
//     if(blockedit){
//       var groupedit="<?php echo $groupid; ?>";
//       // var courseedit = "<?php echo $course->id; ?>";
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
//         $("#groupid").html(data);
//       });
//     }
//     if(groupedit){
//       var courseedit="<?php echo $idcourse; ?>";
//       if(courseedit){
//        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
//         $("#courseid").html(data);
//       });
//      }else{
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
//         $("#courseid").html(data);
//       });
//     }
//   }
// }
//   // ajax add
//   $('#schoolid').on('change', function() {
//     var cua = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
//         $("#block_student").html(data);
//         $("#groupid").html('<option value="">Chọn lớp</option>');
//       });
//     }
//   });
//   $('#block_student').on('change', function() {
//     var block = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
//         $("#groupid").html(data);
//       });
//     }
//   });
//   $('#groupid').on('change', function() {
//     var groupid = this.value ;
//     if (this.value) {
//       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
//         $("#courseid").html(data);
//       });
//     }
//   });
</script>
<script>
  // ajax add
$('#schoolid').on('change', function() {
    var cua = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
            $("#block_student").html(data);
        });
    }else{
      $("#block_student").html('<option value=""><?php echo get_string('block_student'); ?></option>');
      $("#school_yearid").html('<option value=""><?php echo get_string('school_year'); ?></option>');
      $("#groupid").html('<option value=""><?php echo get_string('class'); ?></option>');
      $("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
    }
});
$('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+block,function(data){
        $("#school_yearid").html(data);
      });
    }else{
      ("#school_yearid").html('<option value=""><?php echo get_string('school_year'); ?></option>');
      $("#groupid").html('<option value=""><?php echo get_string('class'); ?></option>');
      $("#courseid").html('<option value=""><?php echo get_string('classgroup'); ?></option>');
    }
  });
   $('#school_yearid').on('change', function() {
    var blockid = $('#school_yearid option:selected').attr('status');
    var school_yearid = this.value ;
    if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockid+"&school_year="+school_yearid, function(data) {
      $("#groupid").html(data);
    });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    var school_yearid2 = $('#groupid option:selected').attr('status');
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupid+"&school_yearid2="+school_yearid2,function(data){
        $("#courseid").html(data);
      });
    }
  });
    var school = $('#schoolid').find(":selected").val();
    var blockedit = "<?php echo $block_student; ?>";
    var school_yearedit="<?php echo $school_year; ?>";
    var groupeit="<?php echo $groupid; ?>";
    var courseedit="<?php echo $courseid; ?>";
  if(school){
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    }
    if(blockedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?block_student="+blockedit+"&school_yearedit="+school_yearedit,function(data){
            $("#school_yearid").html(data);
        });
    }
    if(school_yearedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?blockid=" +blockedit+"&school_year="+school_yearedit+"&groupedit="+groupeit, function(data) {
          $("#groupid").html(data);
        }); 
    }
    if(courseedit){
        $.get("<?php echo $CFG->wwwroot ?>/manage/report/load_ajax.php?groupid="+groupeit+"&school_yearid2="+school_yearedit+"&courseedit="+courseedit,function(data){
            $("#courseid").html(data);
          });
    }
    
</script>
<?php 
echo $OUTPUT->footer();
?>
