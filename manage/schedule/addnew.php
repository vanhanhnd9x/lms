<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/teams/config.php'); 
global $USER;

require_login();
$PAGE->set_title(get_string('add_schedule'));
$PAGE->set_heading(get_string('add_schedule'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schedule';
$type='list';
$name1='add_schedule';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
if ($action=='add_schedule') {
  # code...
  $groupid = optional_param('groupid', '', PARAM_TEXT);
  $group=$DB->get_record('groups', array(
  'id' => $groupid
  ));
  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $teacherid = optional_param('teacherid', '', PARAM_TEXT);
  $taid = optional_param('taid', '', PARAM_TEXT);
  $book = optional_param('book', '', PARAM_TEXT);
  $number = optional_param('number', '', PARAM_TEXT);
  $day = optional_param('day', '', PARAM_TEXT);
  $time_start = optional_param('time_start', '', PARAM_TEXT);
  $time_end = optional_param('time_end', '', PARAM_TEXT);
  $lesson = optional_param('lesson', '', PARAM_TEXT);
  $location = optional_param('location', '', PARAM_TEXT);
  $gvnn = optional_param('gvnn', '', PARAM_TEXT); 
  $gvtg = optional_param('gvtg', '', PARAM_TEXT);
  $cua = optional_param('cua', '', PARAM_TEXT);
  $note = optional_param('note', '', PARAM_TEXT);
  // $scheduleid=add_new_schedule($courseid,$groupid,$book);

    $idgvnn=lay_id_gvnn_thuoc_nhom_lop_by_cua($courseid);
    $idgvtg=lay_id_gvtg_thuoc_nhom_lop_by_cua($courseid);
  $check=check_schedule($courseid,$group->id_namhoc);
  if(!empty($check)){
    ?>
      <script>
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/schedule/edit.php?id_edit=<?php echo $check;?>";
        var e=confirm("Đã tồn tại thời khóa biểu cho nhóm lớp trong năm học này! Bạn có muốn chỉnh sửa thời khóa biểu?");
        if(e==true){
          window.location=urlweb;
        }
      </script>
    <?php 
  }else{
    $scheduleid=add_new_schedule($courseid,$groupid,$book,$group->id_namhoc);
    // update_gvnn_gvtg_course_in_schedule($courseid,$gvnn,$gvtg);
    // add gvnn
    if(!empty($idgvnn)){
        if(!empty($gvnn)&&($gvnn!=$idgvnn)){
            assign_user_to_course_3($gvnn, $courseid,8);
            remove_person_from_course_3($idgvnn, $courseid);
        }
        if(empty($gvnn)){
            remove_person_from_course_3($idgvnn, $courseid);
        }

    }else{
        if(!empty($gvnn)){
            assign_user_to_course_3($gvnn, $courseid,8);
        }
    }
    // add gvtg
    if(!empty($idgvtg)){
        if(empty($gvtg)){
            remove_person_from_course_3($idgvtg, $courseid);
        }
        if(!empty($gvtg)&&($gvtg!=$idgvtg)){
            assign_user_to_course_3($gvtg, $courseid,10);
            remove_person_from_course_3($idgvtg, $courseid);
        }
    }else{
        if(!empty($gvtg)){
            assign_user_to_course_3($gvtg, $courseid,10);
        }
    }
    
    $save=array();
    if(!empty($scheduleid)&&!empty($number)){
      foreach ($day as $key => $d) {
        $save[$key]=add_new_schedule_info($scheduleid,$day[$key],$time_start[$key],$time_end[$key],$location[$key],$note[$key]);
      }
      if(!empty($save)){
        echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/schedule/index.php");
      }else{
        echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/schedule/addnew.php");
      }
    }
  }
  
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schedule';
$name1='add_schedule';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}

// $idgvtg=lay_id_gvnn_thuoc_nhom_lop_by_cua(151);
// var_dump($idgvtg);

?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="card-content">
                <div class="material-datatables" style="clear: both;">
                    <form name="form1" action="" onsubmit="return matchpass()" method="post">
                        <!-- <form  action="" onsubmit="return validate();" method="post"> -->
                        <input type="text" class="form-control" name="action" value="add_schedule" hidden="">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label">
                                    <?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
                                <select name="schoolid" id="schoolid" class="form-control" required>
                                    <?php if (!empty($courseid)){ ?>
                                        <option value="<?php echo get_info_truong(get_info_course($courseid)->id_truong)->id; ?>">
                                            <?php echo get_info_truong(get_info_course($courseid)->id_truong)->name; ?>
                                        </option>
                                    <?php }else{ ?>
                                    <option value="">
                                        <?php echo get_string('schools'); ?>
                                    </option>
                                    <?php 
                                       $schools = get_all_shool();
                                       foreach ($schools as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id; ?>">
                                        <?php echo $value->name; ?>
                                    </option>
                                    <?php } }?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">
                                    <?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
                                <select name="block_student" id="block_student" class="form-control" required>
                                    <?php if (!empty($courseid)){ ?>
                                        <option value="<?php echo get_info_khoi(get_info_course($courseid)->id_khoi)->id; ?>">
                                            <?php echo get_info_khoi(get_info_course($courseid)->id_khoi)->name; ?>
                                        </option>
                                    <?php }else{ ?>
                                    <option value="">
                                        <?php echo get_string('block_student'); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">
                                    <?php echo get_string('class'); ?><span style="color:red">*</span></label>
                                <select name="groupid" id="groupid" class="form-control" required>
                                    <?php if (!empty($courseid)){ ?>
                                        <option value="<?php echo get_info_class(get_info_course($courseid)->id_lop)->id; ?>">
                                            <?php echo get_info_class(get_info_course($courseid)->id_lop)->name; ?>
                                        </option>
                                    <?php }else{ ?>
                                    <option value="">
                                        <?php echo get_string('class'); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">
                                    <?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
                                <select name="courseid" id="courseid" class="form-control" required>
                                    <?php if (!empty($courseid)){ ?>
                                        <option value="<?php echo $courseid ?>">
                                            <?php echo get_info_course($courseid)->fullname; ?>
                                        </option>
                                    <?php }else{ ?>
                                    <option value="">
                                        <?php echo get_string('classgroup'); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="showgv"  class="row">
                            <div class="col-md-6 form-group">
                                 <label class="control-label">
                                    <?php echo get_string('GVNN'); ?></label>
                                 <select type="text" id="gvnn" name="gvnn" class="saj form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                    <option value="">--None--</option>
                                    <?php 
                                    $teacher=all_gvnn_schedule();
                                    foreach ($teacher as $val) {
                                        ?>
                                        <option value="<?php echo $val->id; ?>" <?php if($id_gvtg==$val->id) echo'selected'; ?>><?php echo $val->lastname,' ',$val->firstname; ?></option>
                                        <?php 
                                      }
                                       ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label">
                                    <?php echo get_string('GVTG'); ?></label>
                                <select type="text" id="gvtg" name="gvtg" class="saj form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                    <option value="">--None--</option>
                                    <?php 
                                    $teacher=all_gvtg_schedule();
                                    foreach ($teacher as $val) {
                                        ?>
                                        <option value="<?php echo $val->id; ?>" <?php if($id_gvtg==$val->id) echo'selected'; ?>><?php echo $val->lastname,' ',$val->firstname; ?></option>
                                        <?php 
                                      }
                                       ?>
                                </select>
                            </div>
                                
                        </div>
                        <div class="row">
                            
                            <div class="col-md-6 form-group">
                                <label class="control-label">
                                    <?php echo get_string('syllabus_name'); ?><span style="color:red">*</span></label>
                                <input type="text" name="book" class="form-control" required="" placeholder="Tên giáo trình">
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label">
                                    <?php echo get_string('number_days_learn'); ?> <span style="color:red">*</span></label>(Tối đa 7 ngày)
                                <input type="number" id="number" name="number" class="form-control" required="" min="1" max="7">
                            </div>
                            <div class="col-md-12" id="return_number">
                            </div>
                        
                            <div class="col-md-12" style="margin-top: 10px">
                                <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
                                <?php print_r(get_string('or')) ?>
                                <a href="<?php echo $CFG->wwwroot ?>/manage/schedule/index.php" class="btn btn-danger">
                                    <?php print_r(get_string('cancel')) ?></a>
                            </div>
                    </form>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
        <!-- Filter -->
    </div>
</div>
<?php 
  echo $OUTPUT->footer();

  ?>
<script>
// var conceptName = $('#schoolid').find(":selected").val();
// if (conceptName) {
//     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+conceptName,function(data){
//       $("#block_student").html(data);
//     });
//   }
// alert(conceptName);
$('#schoolid').on('change', function() {
    var cua = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid=" + cua, function(data) {
            $("#block_student").html(data);
        });
    }
});
$('#block_student').on('change', function() {
    var block = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student=" + block, function(data) {
            $("#groupid").html(data);
        });
    }
});
$('#groupid').on('change', function() {
    var groupid = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid=" + groupid, function(data) {
            $("#courseid").html(data);
        });
    }
});
$('#courseid').on('change', function() {
    var courseid = this.value;
    if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseforteacher=" + courseid, function(data) {
            $("#showgv").html(data);
        });
    }
});

$('#number').on('change keyup', function() {
    var sanitized = $(this).val();
    if (sanitized < 8) {
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?number=" + sanitized, function(data) {
            $("#return_number").html(data);
        });
    }
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<!-- timepicker  -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
function checksoo(so1, so2) {
    so1 = so1.replace(":", "");
    so2 = so2.replace(":", "");
    if (Number(so1) >= Number(so2)) {
        return 1;
    }
    if (Number(so1) < Number(so2)) {
        return 2;
    }
}

function matchpass() {
    var so = document.getElementById('number').value;
    // so co gia tri tu 1 den 7
    if (so == 1) {
        var To_Time = $("#time_start0").val();
        var To_Time2 = $("#time_end0").val();
        var check = checksoo(To_Time, To_Time2);
        if (check == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
    }
    if (so == 2) {
        var time_start0 = $("#time_start0").val();
        var time_end0 = $("#time_end0").val();

        var time_start1 = $("#time_start1").val();
        var time_end1 = $("#time_end1").val();

        var check0 = checksoo(time_start0, time_end0);
        var check1 = checksoo(time_start1, time_end1);
        if (check0 == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
        if (check0 == 2) {
            if (check1 == 1) {
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
            }
        }
    }
    //end so =2
    if (so == 3) {
        var time_start0 = $("#time_start0").val();
        var time_end0 = $("#time_end0").val();

        var time_start1 = $("#time_start1").val();
        var time_end1 = $("#time_end1").val();

        var time_start2 = $("#time_start2").val();
        var time_end2 = $("#time_end2").val();

        var check0 = checksoo(time_start0, time_end0);
        var check1 = checksoo(time_start1, time_end1);
        var check2 = checksoo(time_start2, time_end2);
        if (check0 == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
        if (check0 == 2) {
            if (check1 == 1) {
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
            }
            if (check1 == 2) {
                if (check2 == 1) {
                    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                    return false;
                }
            }
        }
    }
    // end so = 3
    if (so == 4) {
        var time_start0 = $("#time_start0").val();
        var time_end0 = $("#time_end0").val();

        var time_start1 = $("#time_start1").val();
        var time_end1 = $("#time_end1").val();

        var time_start2 = $("#time_start2").val();
        var time_end2 = $("#time_end2").val();

        var time_start3 = $("#time_start3").val();
        var time_end3 = $("#time_end3").val();

        var check0 = checksoo(time_start0, time_end0);
        var check1 = checksoo(time_start1, time_end1);
        var check2 = checksoo(time_start2, time_end2);
        var check3 = checksoo(time_start3, time_end3);
        if (check0 == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
        if (check0 == 2) {
            if (check1 == 1) {
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
            }
            if (check1 == 2) {
                if (check2 == 1) {
                    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                    return false;
                }
                if (check2 == 2) {
                    if (check3 == 1) {
                        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                        return false;
                    }
                }
            }
        }
    }
    // end so ==4
    if (so == 5) {
        var time_start0 = $("#time_start0").val();
        var time_end0 = $("#time_end0").val();

        var time_start1 = $("#time_start1").val();
        var time_end1 = $("#time_end1").val();

        var time_start2 = $("#time_start2").val();
        var time_end2 = $("#time_end2").val();

        var time_start3 = $("#time_start3").val();
        var time_end3 = $("#time_end3").val();

        var time_start4 = $("#time_start4").val();
        var time_end4 = $("#time_end4").val();

        var check0 = checksoo(time_start0, time_end0);
        var check1 = checksoo(time_start1, time_end1);
        var check2 = checksoo(time_start2, time_end2);
        var check3 = checksoo(time_start3, time_end3);
        var check4 = checksoo(time_start4, time_end4);
        if (check0 == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
        if (check0 == 2) {
            if (check1 == 1) {
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
            }
            if (check1 == 2) {
                if (check2 == 1) {
                    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                    return false;
                }
                if (check2 == 2) {
                    if (check3 == 1) {
                        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                        return false;
                    }
                    if (check3 == 2) {
                        if (check4 == 1) {
                            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                            return false;
                        }
                    }
                }
            }
        }
    }
    // end so == 5
    if (so == 6) {
        var time_start0 = $("#time_start0").val();
        var time_end0 = $("#time_end0").val();

        var time_start1 = $("#time_start1").val();
        var time_end1 = $("#time_end1").val();

        var time_start2 = $("#time_start2").val();
        var time_end2 = $("#time_end2").val();

        var time_start3 = $("#time_start3").val();
        var time_end3 = $("#time_end3").val();

        var time_start4 = $("#time_start4").val();
        var time_end4 = $("#time_end4").val();

        var time_start5 = $("#time_start5").val();
        var time_end5 = $("#time_end5").val();

        var check0 = checksoo(time_start0, time_end0);
        var check1 = checksoo(time_start1, time_end1);
        var check2 = checksoo(time_start2, time_end2);
        var check3 = checksoo(time_start3, time_end3);
        var check4 = checksoo(time_start4, time_end4);
        var check5 = checksoo(time_start5, time_end5);
        if (check0 == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
        if (check0 == 2) {
            if (check1 == 1) {
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
            }
            if (check1 == 2) {
                if (check2 == 1) {
                    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                    return false;
                }
                if (check2 == 2) {
                    if (check3 == 1) {
                        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                        return false;
                    }
                    if (check3 == 2) {
                        if (check4 == 1) {
                            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                            return false;
                        }
                        if (check4 == 2) {
                            if (check5 == 1) {
                                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
    //end so =6
    if (so == 6) {
        var time_start0 = $("#time_start0").val();
        var time_end0 = $("#time_end0").val();

        var time_start1 = $("#time_start1").val();
        var time_end1 = $("#time_end1").val();

        var time_start2 = $("#time_start2").val();
        var time_end2 = $("#time_end2").val();

        var time_start3 = $("#time_start3").val();
        var time_end3 = $("#time_end3").val();

        var time_start4 = $("#time_start4").val();
        var time_end4 = $("#time_end4").val();

        var time_start5 = $("#time_start5").val();
        var time_end5 = $("#time_end5").val();

        var time_start6 = $("#time_start6").val();
        var time_end6 = $("#time_end6").val();

        var check0 = checksoo(time_start0, time_end0);
        var check1 = checksoo(time_start1, time_end1);
        var check2 = checksoo(time_start2, time_end2);
        var check3 = checksoo(time_start3, time_end3);
        var check4 = checksoo(time_start4, time_end4);
        var check5 = checksoo(time_start5, time_end5);
        var check6 = checksoo(time_start6, time_end6);
        if (check0 == 1) {
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
        }
        if (check0 == 2) {
            if (check1 == 1) {
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
            }
            if (check1 == 2) {
                if (check2 == 1) {
                    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                    return false;
                }
                if (check2 == 2) {
                    if (check3 == 1) {
                        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                        return false;
                    }
                    if (check3 == 2) {
                        if (check4 == 1) {
                            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                            return false;
                        }
                        if (check4 == 2) {
                            if (check5 == 1) {
                                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                                return false;
                            }
                            if (check5 == 2) {
                                if (check6 == 1) {
                                    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // end so =7
}
</script>