<div class="form-group row">
     <label class="col-md-2 col-form-label">Thời gian bắt đầu</label>
     <div class="col-md-10">
      <input type="text" name="time_start" id="date_start" class=" datepicker form-control"  placeholder="dd/mm/yyyy">
         <!-- <div class="input-group date" data-provide="datepicker" data-date-format="dd-mm-yyyy">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
          <input type="text" name="time_start" id="date_start" class="form-control" placeholder="dd-mm-yyyy" value="<?php echo $student->time_start; ?>">
        </div> -->
        <!-- <input type="text" name="date_start" id="date_start" class="datepicker form-control hasDatepicker" required="" readonly="readonly"> -->
      </div>
    </div>
    <div class="form-group row">
     <label class="col-md-2 col-form-label">Thời gian kết thúc</label>
     <div class="col-md-10">
       <input type="text" name="time_end" id="date_end" class="  datepicker form-control"  placeholder="dd/mm/yyyy">
       <!-- <div class="input-group date" data-provide="datepicker" data-date-format="dd-mm-yyyy">
        <div class="input-group-addon">
          <span class="glyphicon glyphicon-th"></span>
        </div>
        <input type="text" name="time_end" id="date_end" class="form-control" placeholder="dd-mm-yyyy" value="<?php echo $student->time_end; ?>">
      </div> -->
      <!-- <input type="text" name="date_end" id="date_end" class="datepicker form-control hasDatepicker" required="" readonly="readonly"> -->
    </div>
  </div>
  <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css'>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script type="text/javascript">
  $("#date_start").datepicker({
        // startDate: "dateToday",
        // minDate: 0,
        minDate: new Date(25, 10 - 1,2000 ),
        maxDate: "",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_end").datepicker("option","minDate", selected)
        },
        disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
  $("#date_end").datepicker({ 
    minDate: 0,
    maxDate:"",
    numberOfMonths: 1,
    onSelect: function(selected) {
      $("#date_start").datepicker("option","maxDate", selected)
    },
    disableTouchKeyboard: true,
    Readonly: true
  }).attr("readonly", "readonly");
  jQuery(function($){
    $.datepicker.regional['vi'] = {
      closeText: 'Đóng',
      prevText: '<Trước',
      nextText: 'Tiếp>',
      currentText: 'Hôm nay',
      monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
      'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
      monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
      'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
      dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
      dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
      weekHeader: 'Tu',
      dateFormat: 'dd/mm/yy',
      firstDay: 0,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''};
      $.datepicker.setDefaults($.datepicker.regional['vi']);
    });
  </script>
  