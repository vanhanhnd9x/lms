<?php 
require("../../config.php");
// require("../../grade/lib.php");
global $CFG, $DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
global $USER;

require_login();
$PAGE->set_title(get_string('edit_schedule'));
$PAGE->set_heading(get_string('edit_schedule'));
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$id_edit = optional_param('id_edit', '', PARAM_TEXT);
$schedule=$DB->get_record('schedule', array(
  'id' => $id_edit
));
$course=$DB->get_record('course', array(
  'id' => $schedule->courseid
));
$group=$DB->get_record('groups', array(
  'id' => $schedule->groupid
));
$block_student=$DB->get_record('block_student', array(
  'id' => $group->id_khoi
));
$idgvnn=lay_id_gvnn_thuoc_nhom_lop_by_cua($schedule->courseid);
$idgvtg=lay_id_gvtg_thuoc_nhom_lop_by_cua($schedule->courseid);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schedule';
$type='list';
$name1='edit_schedule';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);

if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
if ($action=='edit_schedule') {
  # code...
  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $groupid = optional_param('groupid', '', PARAM_TEXT);
  $teacherid = optional_param('teacherid', '', PARAM_TEXT);
  $taid = optional_param('taid', '', PARAM_TEXT);
  $book = optional_param('book', '', PARAM_TEXT);
  $number = optional_param('number', '', PARAM_TEXT);
  $day = optional_param('day', '', PARAM_TEXT);
  $time_start = optional_param('time_start', '', PARAM_TEXT);
  $time_end = optional_param('time_end', '', PARAM_TEXT);
  $lesson = optional_param('lesson', '', PARAM_TEXT);
  $location = optional_param('location', '', PARAM_TEXT);
  $cua = optional_param('cua', '', PARAM_TEXT);
  $id_edit_info = optional_param('id_edit_info', '', PARAM_TEXT);
  $note = optional_param('note', '', PARAM_TEXT);
  $gvnn = optional_param('gvnn', '', PARAM_TEXT);
  $gvtg = optional_param('gvtg', '', PARAM_TEXT);
  // if($courseid!=$schedule->courseid){
  //   update_schedulei($id_edit,$courseid,$groupid,$book);
  // }
  // update_schedulei($id_edit,$courseid,$groupid,$book);
  update_schedulei($id_edit,$courseid,$groupid,$book,$group->id_namhoc);
  // update_gvnn_gvtg_course_in_schedule($courseid,$gvnn,$gvtg);
  if(!empty($idgvnn)){
        if(!empty($gvnn)&&($gvnn!=$idgvnn)){
            assign_user_to_course_3($gvnn, $courseid,8);
            remove_person_from_course_3($idgvnn, $courseid);
        }
        if(empty($gvnn)){
            remove_person_from_course_3($idgvnn, $courseid);
        }

    }else{
        if(!empty($gvnn)){
            assign_user_to_course_3($gvnn, $courseid,8);
        }
    }
    // add gvtg
    if(!empty($idgvtg)){
        if(empty($gvtg)){
            remove_person_from_course_3($idgvtg, $courseid);
        }
        if(!empty($gvtg)&&($gvtg!=$idgvtg)){
            assign_user_to_course_3($gvtg, $courseid,10);
            remove_person_from_course_3($idgvtg, $courseid);
        }
    }else{
        if(!empty($gvtg)){
            assign_user_to_course_3($gvtg, $courseid,10);
        }
    }
  // update
  foreach ($id_edit_info as $key => $value) {
    # code...
    update_schedule_info($id_edit_info[$key],$day[$key],$time_start[$key],$time_end[$key],$location[$key],$note[$key]);
  }
  // echo displayJsAlert('Sửa thành công', $CFG->wwwroot . "/manage/schedule/edit.php?id_edit=".$id_edit);
  echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/schedule/index.php");
  
  //update
}
?>

<div class="row">

  <div class="col-md-12">
    <div class="card-box">
      <div class="card-content">

        <div class="material-datatables" style="clear: both;">
          <form   name="form1" action="" onsubmit="return matchpass()" method="post">
            <input type="text" class="form-control" name="action" value="edit_schedule" hidden="">
            <div class="row">
              <div class="col-md-6">
                <label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
                <select name="schoolid" id="schoolid" class="form-control" required>
                 <option value=""><?php echo get_string('schools'); ?> </option>
                 <?php 
                 $schools = get_all_shool();
                 foreach ($schools as $key => $value) {
                        # code...
                  ?>
                  <option value="<?php echo $value->id; ?>" <?php if(!empty($block_student->schoolid)&&($block_student->schoolid==$value->id)){echo'selected';} ?>><?php echo $value->name; ?></option>
                  <?php 
                }
                ?>
              </select>
            </div>
            <div class="col-md-6">
              <label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
              <select name="block_student" id="block_student" class="form-control" required>
               <option value=""><?php echo get_string('block_student'); ?></option>
             </select>
           </div>
           <div class="col-md-6">
            <label class="col-form-label"><?php echo get_string('class'); ?></label>
            <div class="">
              <select name="groupid" id="groupid" class="form-control" required>
                <option value=""><?php echo get_string('class'); ?></option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <label class="control-label"><?php echo get_string('classgroup'); ?> <span style="color:red">*</span></label>
            <select name="courseid" id="courseid" class="form-control" required>
              <option value=""><?php echo get_string('classgroup'); ?></option>
            </select>
          </div>
        </div>
          <div id="showgv"  class="row">
            <div class="col-md-6 form-group">
              <label class="control-label"><?php echo get_string('GVNN'); ?></label>
                <select type="text" id="gvnn" name="gvnn" class="saj form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                  <option value="">--None--</option>
                  <?php 
                  $teacher=all_gvnn_schedule();
                  foreach ($teacher as $val) {
                  ?>
                   <option value="<?php echo $val->id; ?>" <?php if(!empty($idgvnn)&&($idgvnn==$val->id)) echo'selected'; ?>><?php echo $val->lastname,' ',$val->firstname; ?></option>
                  <?php 
                  }
                  ?>
                </select>
              </div>
              <div class="col-md-6 form-group">
                  <label class="control-label"><?php echo get_string('GVTG'); ?></label>
                  <select type="text" id="gvtg" name="gvtg" class="saj form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                    <option value="">--None--</option>
                      <?php 
                      $teacher=all_gvtg_schedule();
                      foreach ($teacher as $val) {
                      ?>
                        <option value="<?php echo $val->id; ?>" <?php if(!empty($idgvtg)&&($idgvtg==$val->id)) echo'selected'; ?>><?php echo $val->lastname,' ',$val->firstname; ?></option>
                      <?php 
                      }
                      ?>
                   </select>
              </div>                 
          </div>
          <div class="row">
          <div class="col-md-6">
            <label for=""><?php echo get_string('syllabus_name'); ?><span style="color:red">*</span></label></label>
            <input type="text" name="book" class="form-control" required="" value="<?php echo $schedule->book; ?>">
          </div>
          <?php 
          $sql_total="SELECT  * from schedule_info
          WHERE `schedule_info`.`scheduleid`= {$id_edit} ";
          $data=$DB->get_records_sql($sql_total);
          $count=count($data);
          $i=0;
          foreach ( $data as $key => $value) {
              # code...
            $i++;
            ?>
            <!-- <div class="row"> -->
              <div class="col-md-12">
                <div class="alert-info">
                  <h4><?php echo get_string('study_day'); ?>  <?php echo $i; ?></h4>
                </div>
              </div>
              <input type="text" value="<?php echo $value->id; ?>" name="id_edit_info[]" hidden="">
              <div class="col-md-6">
                <label for=""><?php echo get_string('choose_the_date'); ?> <span style="color:red">*</span></label></label>
                <select name="day[]" id="" class="form-control" required="">
                  <option value="2" <?php if($value->day==2) echo'selected'; ?>><?php echo get_string('monday'); ?></option>
                  <option value="3" <?php if($value->day==3) echo'selected'; ?>><?php echo get_string('tuesday'); ?></option>
                  <option value="4" <?php if($value->day==4) echo'selected'; ?>><?php echo get_string('wednesday'); ?></option>
                  <option value="5" <?php if($value->day==5) echo'selected'; ?>><?php echo get_string('thursday'); ?></option>
                  <option value="6" <?php if($value->day==6) echo'selected'; ?>><?php echo get_string('friday'); ?></option>
                  <option value="7" <?php if($value->day==7) echo'selected'; ?>><?php echo get_string('saturday'); ?></option>
                  <option value="8" <?php if($value->day==8) echo'selected'; ?>><?php echo get_string('sunday'); ?></option>
                </select>
              </div>
              <div class="col-md-6">
                <label for=""><?php echo get_string('class_room'); ?> <span style="color:red">*</span></label></label>
                <input type="text" name="location[]" class="form-control" required="" value="<?php echo $value->location; ?>">
              </div>
              
              <div class="col-md-6">
                <label for=""><?php echo get_string('time_start'); ?><span style="color:red">*</span></label></label>
                <input type="text" class="form-control" name="time_start[]" id="time_start<?php echo $i; ?>">
              </div>


              <div class="col-md-6">
                <label for=""><?php echo get_string('time_end'); ?><span style="color:red">*</span></label></label>
                <input type="text" class="form-control" name="time_end[]" id="time_end<?php echo $i; ?>" >
              </div>
              
              <!-- </div> -->
              <script type="text/javascript">
                $(document).ready(function () {
                 var d = new Date();
                 $('#time_start<?php echo $i; ?>').timepicker({
                  timeFormat: 'H:mm ',
                  interval: 10,
                  minTime: '8:00am',
                  maxTime: '10:00pm',
                  defaultTime: '<?php echo $value->time_start; ?>',
                  startTime: '8:00',
                  dynamic: false,
                  dropdown: true,
                  scrollbar: true
                });
                 $('#time_end<?php echo $i; ?>').timepicker({
                  timeFormat: 'H:mm ',
                  interval: 10,
                  minTime: '8:00am',
                  maxTime: '10:00pm',
                  defaultTime: '<?php echo $value->time_end; ?>',
                  startTime: '8:00',
                  dynamic: false,
                  dropdown: true,
                  scrollbar: true
                });  
               });


             </script>
             <?php 
           }
           ?>
           <br>
           <br>
           <div class="col-md-12">
            <label for=""><?php echo get_string('note'); ?></label>
            <textarea name="note[]" class="form-control" value="<?php echo $value->note; ?>"><?php echo $value->note; ?></textarea>
          </div>
           <div class="col-md-12">
            <input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
            <?php print_r(get_string('or')) ?>
            <a href="<?php echo $CFG->wwwroot ?>/manage/schedule/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
          </div>
        </form>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
</div>
</div>
<?php 
echo $OUTPUT->footer();

?>

<!-- timepicker  -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
  // ajax edit
  var school = $('#schoolid').find(":selected").val();
  if (school) {
    var blockedit = "<?php echo $block_student->id; ?>";
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    if(blockedit){
      var groupedit="<?php echo $schedule->groupid; ?>";
      // var courseedit = "<?php echo $course->id; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
        $("#groupid").html(data);
      });
    }
    if(groupedit){
      var courseedit="<?php echo $schedule->courseid; ?>";
      if(courseedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
        $("#courseid").html(data);
      });
     }else{
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
        $("#courseid").html(data);
      });
    }
  }
}
  // ajax add
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
        $("#courseid").html(data);
      });
    }
  });
  $('#courseid').on('change', function() {
    var courseid = this.value ;
    if (courseid) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseforteacher="+courseid,function(data){
        $("#showgv").html(data);
      });
    }
  });
  var coursetoedit = $('#courseid').find(":selected").val();
  if(coursetoedit){
     $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseforteacher="+coursetoedit,function(data){
        $("#showgv").html(data);
      });
  }
</script>
<script type="text/javascript">
  function checksoo(so1,so2){
    so1=so1.replace(":", "");
    so2=so2.replace(":", "");
    if(Number(so1)>=Number(so2)){
     return 1;
   }
   if(Number(so1)<Number(so2)){
    return 2;
  }
}
function matchpass() {
  // var so = document.getElementById('number').value ;
  var so = '<?php echo $count; ?>' ;
 // so co gia tri tu 1 den 7
 if(so==1){
  var To_Time=$("#time_start1").val(); 
  var To_Time2=$("#time_end1").val(); 
  var check = checksoo(To_Time,To_Time2);
  if(check==1){
    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
    return false;
  }
}
if(so==2){
  var time_start0=$("#time_start1").val(); 
  var time_end0=$("#time_end1").val(); 

  var time_start1=$("#time_start2").val(); 
  var time_end1=$("#time_end2").val(); 

  var check0 = checksoo(time_start0,time_end0);
  var check1 = checksoo(time_start1,time_end1);
  if(check0==1){
    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
    return false;
  }
  if(check0==2){
    if(check1==1){
      alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
      return false;
    }
  }
}
    //end so =2
    if(so==3){
      var time_start0=$("#time_start1").val(); 
      var time_end0=$("#time_end1").val(); 

      var time_start1=$("#time_start2").val(); 
      var time_end1=$("#time_end2").val(); 

      var time_start2=$("#time_start3").val(); 
      var time_end2=$("#time_end3").val(); 

      var check0 = checksoo(time_start0,time_end0);
      var check1 = checksoo(time_start1,time_end1);
      var check2 = checksoo(time_start2,time_end2);
      if(check0==1){
        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        return false;
      }
      if(check0==2){
        if(check1==1){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
        if(check1==2){
          if(check2==1){
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
          }
        }
      }
    }
// end so = 3
if(so==4){
  var time_start0=$("#time_start1").val(); 
  var time_end0=$("#time_end1").val(); 

  var time_start1=$("#time_start2").val(); 
  var time_end1=$("#time_end2").val(); 

  var time_start2=$("#time_start3").val(); 
  var time_end2=$("#time_end3").val(); 

  var time_start3=$("#time_start4").val(); 
  var time_end3=$("#time_end4").val(); 

  var check0 = checksoo(time_start0,time_end0);
  var check1 = checksoo(time_start1,time_end1);
  var check2 = checksoo(time_start2,time_end2);
  var check3 = checksoo(time_start3,time_end3);
  if(check0==1){
    alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
    return false;
  }
  if(check0==2){
    if(check1==1){
      alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
      return false;
    }
    if(check1==2){
      if(check2==1){
        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        return false;
      }
      if(check2==2){
        if(check3==1){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
      }
    }
  }
}
  // end so ==4
  if(so==5){
    var time_start0=$("#time_start1").val(); 
    var time_end0=$("#time_end1").val(); 

    var time_start1=$("#time_start2").val(); 
    var time_end1=$("#time_end2").val(); 

    var time_start2=$("#time_start3").val(); 
    var time_end2=$("#time_end3").val(); 

    var time_start3=$("#time_start4").val(); 
    var time_end3=$("#time_end4").val(); 

    var time_start4=$("#time_start5").val(); 
    var time_end4=$("#time_end5").val(); 

    var check0 = checksoo(time_start0,time_end0);
    var check1 = checksoo(time_start1,time_end1);
    var check2 = checksoo(time_start2,time_end2);
    var check3 = checksoo(time_start3,time_end3);
    var check4 = checksoo(time_start4,time_end4);
    if(check0==1){
      alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
      return false;
    }
    if(check0==2){
      if(check1==1){
        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        return false;
      }
      if(check1==2){
        if(check2==1){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
        if(check2==2){
          if(check3==1){
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
          }
          if(check3==2){
            if(check4==1){
              alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
              return false;
            }
          }
        }
      }
    }
  }
  // end so == 5
  if(so==6){
    var time_start0=$("#time_start1").val(); 
    var time_end0=$("#time_end1").val(); 

    var time_start1=$("#time_start2").val(); 
    var time_end1=$("#time_end2").val(); 

    var time_start2=$("#time_start3").val(); 
    var time_end2=$("#time_end3").val(); 

    var time_start3=$("#time_start4").val(); 
    var time_end3=$("#time_end4").val(); 

    var time_start4=$("#time_start5").val(); 
    var time_end4=$("#time_end5").val(); 

    var time_start5=$("#time_start6").val(); 
    var time_end5=$("#time_end6").val();

    var check0 = checksoo(time_start0,time_end0);
    var check1 = checksoo(time_start1,time_end1);
    var check2 = checksoo(time_start2,time_end2);
    var check3 = checksoo(time_start3,time_end3);
    var check4 = checksoo(time_start4,time_end4);
    var check5 = checksoo(time_start5,time_end5);
    if(check0==1){
      alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
      return false;
    }
    if(check0==2){
      if(check1==1){
        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        return false;
      }
      if(check1==2){
        if(check2==1){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
        if(check2==2){
          if(check3==1){
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
          }
          if(check3==2){
            if(check4==1){
              alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
              return false;
            }
            if(check4==2){
              if(check5==1){
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
              }
            }
          }
        }
      }
    }
  }
  //end so =6
  if(so==6){
    var time_start0=$("#time_start1").val(); 
    var time_end0=$("#time_end1").val(); 

    var time_start1=$("#time_start2").val(); 
    var time_end1=$("#time_end2").val(); 

    var time_start2=$("#time_start3").val(); 
    var time_end2=$("#time_end3").val(); 

    var time_start3=$("#time_start4").val(); 
    var time_end3=$("#time_end4").val(); 

    var time_start4=$("#time_start5").val(); 
    var time_end4=$("#time_end5").val(); 

    var time_start5=$("#time_start6").val(); 
    var time_end5=$("#time_end6").val();

    var time_start6=$("#time_start7").val(); 
    var time_end6=$("#time_end7").val();

    var check0 = checksoo(time_start0,time_end0);
    var check1 = checksoo(time_start1,time_end1);
    var check2 = checksoo(time_start2,time_end2);
    var check3 = checksoo(time_start3,time_end3);
    var check4 = checksoo(time_start4,time_end4);
    var check5 = checksoo(time_start5,time_end5);
    var check6 = checksoo(time_start6,time_end6);
    if(check0==1){
      alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
      return false;
    }
    if(check0==2){
      if(check1==1){
        alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
        return false;
      }
      if(check1==2){
        if(check2==1){
          alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
          return false;
        }
        if(check2==2){
          if(check3==1){
            alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
            return false;
          }
          if(check3==2){
            if(check4==1){
              alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
              return false;
            }
            if(check4==2){
              if(check5==1){
                alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                return false;
              }
              if(check5==2){
                if(check6==1){
                  alert('Thời gian kết thúc phải lớn hơn thời gian bắt đầu');
                  return false;
                }
              }
            }
          }
        }
      }
    }
  }
  // end so =7
}

</script>

