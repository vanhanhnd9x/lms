<?php
// if (!file_exists('../config.php')) {
//     header('Location: ../install.php');
//     die;
// }

require('../../../config.php');
require_once($CFG->dirroot . '/common/lib.php');


require_login(0, false);

$PAGE->set_title('Chỉnh sửa chuyên cần');
$PAGE->set_heading('Chỉnh sửa chuyên cần');

//now the page contents
// $PAGE->set_pagelayout('teams');
$id= optional_param('id', 0, PARAM_TEXT);
echo $OUTPUT->header();
global $CFG;
$diligence=  get_diligence_id($id);



?>
<div class="row">
    <div class="col-md-9">
        <div class="card-box">
            <div class="ribbon-box">
                <div class="card-content">
                    <?php $student = get_info_student($diligence->id_hs) ?>
                    <form method="post" id="createTeamForm" action="<?php echo $CFG->wwwroot ?>/teams/team.php?action=edit_team&team_id=<?php echo $team_id ?>">
                        <!-- <input type="hidden" value="0" name="ParentTeamId" id="ParentTeamId"> -->
                        <div class="form-group">
                            <label>Học sinh</label>        
                            <input type="text" value="<?php echo $student->firstname .' '.$student->lastname ?>" name="id_hs" id="id_hs" class="form-control" disabled>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Chuyên cần</label>
                                <input type="text" value="<?php echo $diligence->diligence ?>" name="diligence" id="diligence" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Tháng đánh giá</label>        
                                <input type="text" value="<?php echo $diligence->monthrating ?>" name="monthrating" id="monthrating" class="form-control date-month">
                            </div>
                        </div>
                        

                        <div class="form-group">
                          <input type="submit" class="btn btn-success" value=" <?php print_string('save','admin') ?> ">  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
echo $OUTPUT->footer();
?>
