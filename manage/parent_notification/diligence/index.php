<?php
require("../../../config.php");
global $CFG;

require_once($CFG->dirroot . '/common/lib.php');

// Require Login.
require_login();

$PAGE->set_title('Chuyên cần theo tháng');
$PAGE->set_heading('Chuyên cần theo tháng');
echo $OUTPUT->header();

// $sql = "SELECT * FROM user";
// $rows = $DB->get_records_sql($sql);
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '30', PARAM_INT); // how many per page
$id          = optional_param('id', 0, PARAM_INT);
$action      = optional_param('action', '', PARAM_TEXT);


if($action=='delete'){
    $DB->delete_records('diligence',array('id' => $id));
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/parent_notification/diligence/");
}
// $sql = "select * from school_year where 1";
// $namhoc = $DB->get_records_sql($sql);
$chuyencan = get_diligence();

?>

<script src="<?php print new moodle_url('/manage/search.js'); ?>"></script>
<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mr_bottom15">
                        <div class="col-md-2">
                            <a href="<?php echo new moodle_url('/manage/parent_notification/diligence/new.php'); ?>" class="btn btn-info"><?php echo get_string('add') ?></a>
                        </div>
                        <div class="col-md-9">
                            <form action="" method="get" accept-charset="utf-8" class="form-row">
                                <div class="col-4">
                                    <input type="text" class="form-control" placeholder="<?php echo get_string('search') ?>..." name="searchParent" id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                                </div>
                                <!-- <div class="col">
                                    <button type="submit" class="btn btn-info">Tìm kiếm</button>
                                </div> -->
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
                        <table id="tech-companies-1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Tên học sinh</th>
                                    <th>Nhóm lớp</th>
                                    <th>Chuyên cần</th>
                                    <th>Tháng đánh giá</th>
                                    <th class="text-right">Hành động</th>
                                </tr>
                            </thead>

                            <tbody id="page">
                                <?php foreach ($chuyencan as $val) { ?>
                                <tr>
                                    <?php $student = get_info_student($val->id_hs) ?>
                                    <td><?php echo $student->firstname .' '.$student->lastname ?></td>
                                    <td><?php echo $val->name ?></td>
                                    <td><?php echo $val->diligence ?></td>
                                    <td><?php echo $val->monthrating ?></td>
                                    <td class="text-right">
                                        <a href="<?php print new moodle_url('/manage/parent_notification/diligence/edit.php',array('id'=>$val->id)); ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a onclick="return Confirm('Xóa đánh giá','Bạn có muốn xóa đánh giá của <?php echo $student->firstname .' '.$student->lastname ?> khỏi hệ thống?','Yes','Cancel','<?php print new moodle_url('?action=delete',array('id'=>$val->id)); ?>')" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end content-->
     
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->

<?php
    echo $OUTPUT->footer();
?>