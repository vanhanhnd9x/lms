<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// Require Login.
// $PAGE->set_title(get_string('dashboards'));
$PAGE->set_title('Tỷ lệ chuyên cần');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Tỷ lệ chuyên cần');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Tỷ lệ chuyên cần');
require_login(0, false);
echo $OUTPUT->header();

?>
<div class="row">
	<div class="col-md-10">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="<?php echo $CFG->wwwroot ?>/manage/parent_notification/diligence/new.php?action=add_diligence" onsubmit="return validate();" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Học sinh</label>
								<select name="hocsinh" id="hocsinh" class="form-control" required="">
									<?php 
									if (function_exists('get_student_diligence')) {
										$student=get_student_diligence();
										if ($student) {
											foreach ($student as $key => $value) {
												# code...
												echo'<option value="'.$value->id.'">'.$value->firstname. ' '. $value->lastname .'</option>';
											}
										}
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Tỷ lệ chuyên cần</label>
									<input type="text" id="chuyencan" name="chuyencan" class="form-control" value="" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Tháng đánh giá</label>
								<input type="text" id="thangdanhgia" name="thangdanhgia" class="form-control date-month" value="" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-buttons">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
										<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>

						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
		echo $OUTPUT->footer();
		$action = optional_param('action', '', PARAM_TEXT);
		if ($action=='add_diligence') {
	# code...
			$id_hs = optional_param('hocsinh', '', PARAM_TEXT);
			$diligence = optional_param('chuyencan', '', PARAM_TEXT);
			$monthrating = optional_param('thangdanhgia', '', PARAM_TEXT);
			$saveid=insert_diligence($id_hs,$diligence,$monthrating);
			if(!empty($saveid)){
				echo displayJsAlert('', $CFG->wwwroot . "/manage/parent_notification/diligence/new.php");
			}else{

			}
		}
		?>