<?php
require_once('../../config.php');
require_once('lib.php');
require_once('edit_form.php');





/// create form
$noteform = new news_edit_form();


/// if form was cancelled then return to the notes list of the note
if ($noteform->is_cancelled()) {
    redirect($CFG->wwwroot . '/manage/news');
}

/// if data was submitted and validated, then save it to database
if ($note = $noteform->get_data()){
     $note->userid = $USER->id;
    if (news_save($note)) {
        add_to_log($note->id, 'news', 'create', $note->id, 'Create a news');
    }
    // redirect to notes list that contains this note
    redirect($CFG->wwwroot . '/manage/news');
}

$PAGE->set_title($course->shortname . ': ' . $strnotes);
$PAGE->set_heading($course->fullname);
$PAGE->set_title("Lastest News");
$PAGE->set_heading("Lastest News");
//now the page contents
$PAGE->set_pagelayout('Dashboard');
echo $OUTPUT->header();


$noteform->display();
echo $OUTPUT->footer();
