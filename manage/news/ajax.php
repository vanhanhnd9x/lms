<?php

require_once('../../config.php');
require_once('lib.php');


$note = required_param('note', PARAM_INT);

if (!news_delete($note)) {
    $result = false;
}else $result = true;

echo json_encode((object) array('result' => $result,'id'=> $note));

