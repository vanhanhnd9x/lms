<?php

/**
 * Library of functions and constants for notes
 */

/**
 * Constants for states.
 */
define('NOTES_STATE_DRAFT', 'draft');
define('NOTES_STATE_PUBLIC', 'public');
define('NOTES_STATE_SITE', 'site');

/**
 * Constants for note parts (flags used by news_print and news_print_list).
 */
define('NOTES_SHOW_FULL', 0x07);
define('NOTES_SHOW_HEAD', 0x02);
define('NOTES_SHOW_BODY', 0x01);
define('NOTES_SHOW_FOOT', 0x04);

/**
 * Retrieves a list of note objects with specific atributes.
 *
 * @param int    $courseid id of the course in which the notes were posted (0 means any)
 * @param int    $userid id of the user to which the notes refer (0 means any)
 * @param string $state state of the notes (i.e. draft, public, site) ('' means any)
 * @param int    $author id of the user who modified the note last time (0 means any)
 * @param string $order an order to sort the results in
 * @param int    $limitfrom number of records to skip (offset)
 * @param int    $limitnum number of records to fetch
 * @return array of note objects
 */
function news_list($userid=0, $author = 0, $order='lastmodified DESC', $limitfrom=0, $limitnum=0) {
    global $DB;
    
    // setup filters
    $selects = array();
    $params = array();
    
    $selects[] = 'module=?';
    $params[]  = 'usernews';
    
    if ($userid) {
        $selects[] = 'userid=?';
        $params[]  = $userid;
    }
    if ($author) {
        $selects[] = 'usermodified=?';
        $params[]  = $author;
    }
    if ($state) {
        $selects[] = 'publishstate=?';
        $params[]  = $state;
    }

    $select = implode(' AND ', $selects);
    $fields = 'id,courseid,userid,content,format,created,lastmodified,usermodified,publishstate';
    
    // retrieve data
    return $DB->get_records_select('post', $select, $params, $order, $fields, $limitfrom, $limitnum);
}

/**
 * Retrieves a note object based on its id.
 *
 * @param int    $news_id id of the note to retrieve
 * @return note object
 */
function news_load($news_id) {
    global $DB;

    $fields = 'id,courseid,userid,content,format,created,lastmodified,usermodified,publishstate';
    return $DB->get_record('post', array('id'=>$news_id, 'module'=>'news'), $fields);
}

/**
 * Saves a note object. The note object is passed by reference and its fields (i.e. id)
 * might change during the save.
 *
 * @param note   $note object to save
 * @return boolean true if the object was saved; false otherwise
 */
function news_save(&$note) {
    global $USER, $DB;

    // setup & clean fields
    $note->module       = 'usernews';
    $note->lastmodified = time();
    $note->usermodified = $USER->id;
    if (empty($note->format)) {
        $note->format = FORMAT_PLAIN;
    }
    if (empty($note->publishstate)) {
        $note->publishstate = NOTES_STATE_PUBLIC;
    }
    // save data
    if (empty($note->id)) {
        // insert new note
        $note->created = $note->lastmodified;
        $id = $DB->insert_record('post', $note);
        $note = $DB->get_record('post', array('id'=>$id));
    } else {
        // update old note
        $DB->update_record('post', $note);
    }
    unset($note->module);
    return true;
}

/**
 * Deletes a note object based on its id.
 *
 * @param int    $news_id id of the note to delete
 * @return boolean true if the object was deleted; false otherwise
 */
function news_delete($newsid) {
    global $DB;

    return $DB->delete_records('post', array('id'=>$newsid, 'module'=>'usernews'));
}

/**
 * Converts a state value to its corespondent name
 *
 * @param string  $state state value to convert
 * @return string corespondent state name
 */
function news_get_state_name($state) {
    // cache state names
    static $states;
    if (empty($states)) {
        $states = news_get_state_names();
    }
    if (isset($states[$state])) {
        return $states[$state];
    } else {
        return null;
    }
}

/**
 * Returns an array of mappings from state values to state names
 *
 * @return array of mappings
 */
function news_get_state_names() {
    return array(
        NOTES_STATE_DRAFT => get_string('personal', 'notes'),
        NOTES_STATE_PUBLIC => get_string('course', 'notes'),
        NOTES_STATE_SITE => get_string('site', 'notes'),
    );
}

/**
 * Prints a note object
 *
 * @param note  $note the note object to print
 * @param int   $detail OR-ed NOTES_SHOW_xyz flags that specify which note parts to print
 */
function news_print($note) {
    global $CFG, $USER, $DB, $OUTPUT;

    echo '<div id="rows-' . $note->id . '">';
    echo '<div class="main-col">';
    echo '<div class="tip">'. news_ago($note->lastmodified).'</div>';

    echo '<div class="marking-user-answer">';
    
    echo '<div class="content">';
    echo format_text($note->content, $note->format, array('overflowdiv' => true));
    echo '</div>';

    echo '</div>';

    // print note options (e.g. delete, edit)
    
    if ($note->usermodified == $USER->id) {
        echo '<div class="float-right">';
        echo '<a href="' . new moodle_url("/manage/news/ajax.php", array("note" => $note->id)) . '" class="btn-remove"></a>';
        echo '</div>';
    }
    
    echo '</div>'; // End Main-col
    echo '</div>'; // End Row
}

/**
 * Prints a list of note objects
 *
 * @param array  $notes array of note objects to print
 * @param int   $detail OR-ed NOTES_SHOW_xyz flags that specify which note parts to print
 */
function news_print_list($notes) {

    /// Start printing of the note
    
    foreach ($notes as $note) {
        news_print($note);
    }
    
}

/**
 * Retrieves and prints a list of note objects with specific atributes.
 *
 * @param string  $header HTML to print above the list
 * @param int     $addcourseid id of the course for the add notes link (0 hide link)
 * @param boolean $viewnotes true if the notes should be printed; false otherwise (print notesnotvisible string)
 * @param int     $courseid id of the course in which the notes were posted (0 means any)
 * @param int     $userid id of the user to which the notes refer (0 means any)
 * @param string  $state state of the notes (i.e. draft, public, site) ('' means any)
 * @param int     $author id of the user who modified the note last time (0 means any)
 */
function news_print_notes($userid = 0, $author = 0) {
    global $CFG;

    echo '<div class="notesgroup">';
    $notes = news_list($userid, $author);

    if ($notes) {
        news_print_list($notes);
    }
    echo '</div>';  // notesgroup
}

/**
 * Delete all notes about users in course-
 * @param int $courseid
 * @return bool success
 */
function news_delete_all($courseid) {
    global $DB;

    return $DB->delete_records('post', array('module'=>'notes', 'courseid'=>$courseid));
}

/**
 * Return a list of page types
 * @param string $pagetype current page type
 * @param stdClass $parentcontext Block's parent context
 * @param stdClass $currentcontext Current context of block
 */
function news_page_type_list($pagetype, $parentcontext, $currentcontext) {
    return array('notes-*'=>get_string('page-notes-x', 'notes'));
}

function news_ago($time, $prefix = 'Đã đăng')
{
   $periods = array(get_string('second'), get_string('minute'), get_string('hour'), get_string('day'), get_string('week'), get_string('month'), get_string('year'), get_string('decade'));
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = get_string('ago');

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);
//hide for vietnamese

//   if($difference != 1) {
//       $periods[$j].= "s";
//   }

   return "$prefix $difference $periods[$j] $tense";
}