<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */
require_once('../../config.php');
require_once('lib.php');
require_once('../lib.php');

require_once('edit_form.php');

require_login(0, false);
/// create form
$noteform = new news_edit_form();

/// if form was cancelled then return to the notes list of the note
if ($noteform->is_cancelled()) {
    redirect($CFG->wwwroot . '/manage/news');
}
$PAGE->set_title(get_string('lastestnew'));
$PAGE->set_heading(get_string('lastestnew'));
//now the page contents
$PAGE->set_pagelayout(get_string('dashboards'));

$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/news/news.js');

$unique_login_values = prepare_data_for_chart(30, 0);
$course_sales_values = prepare_data_for_chart(30, 1);
$course_complete_values = prepare_data_for_chart(30, 2);
$activityfor = get_string('activityfor');
$uniquelog = get_string('uniquelog');
$coursesale = get_string('coursesale');
$coursecomple = get_string('coursecomple');
$PAGE->requires->js('/manage/js/jquery.min.js');
$PAGE->requires->js('/manage/js/jquery.jqplot.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.highlighter.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.cursor.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.dateAxisRenderer.min.js');
$PAGE->requires->js('/manage/js/jqplot/jqplot.enhancedLegendRenderer.js');
$PAGE->requires->css('/manage/css/jquery.jqplot.min.css');
if ($unique_login_values != "") {
    $PAGE->requires->js('/manage/js/js.php?unique_login_values="'.$unique_login_values.'"&course_sales_values="'.$course_sales_values.'"&course_complete_values="'
                               .$course_complete_values.'"&activityfor="'.$activityfor.'"&uniquelog="'.$uniquelog.'"&coursesale="'.$coursesale.'"&coursecomple="'.$coursecomple.'"');
} 

echo $OUTPUT->header();
?>
<?php
if (is_teacher() && !is_admin()) {
    ?>
<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel">
        <!-- Left Content -->
        <div class="body">
            <div class="wrapper-inner">
                <!-- <div id="chart"></div> -->
                <div class="grid-tabs">
                    <ul>
                        <li><a href="<?php print new moodle_url('/manage'); ?>" class=""><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                        <li><a href="<?php print new moodle_url('/manage/news/'); ?>" class="selected"><?php print_r(get_string('pluginname','block_news_items'))?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="item-page-list">
                    <div id="latestnews">
                        <?php
    /// create form
//    $noteform = new news_edit_form();
//
//    /// if form was cancelled then return to the notes list of the note
//    //                if ($noteform->is_cancelled()) {
//    //                    redirect($CFG->wwwroot . '/manage/news');
//    //                }
//    /// if data was submitted and validated, then save it to database
//    if ($note = $noteform->get_data()) {
//        $note->userid = $USER->id;
//        if (news_save($note)) {
//            add_to_log($note->id, 'usernews', 'create', $note->id, 'Create a news');
//        }
//        // redirect to notes list that contains this note
//        //redirect($CFG->wwwroot . '/manage/news');
//    }
//    $noteform->display();
    //Tung added load news
    $sql = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql);
    
    $id_list = display_parent($rows, $USER->id) ;
    $id_me = $id_list;
    if($id_me==0){
        $id_me = $USER->id;
    }
    if($id_list==0){
             $id_list = $USER->id;
            }  
            
    news_print_notes($id_list);
    ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div id="admin-fullscreen-right">
        <div class="side-panel">
            <div class="side-panel">
                <h3><?php print_r(get_string('recentlyviewed','moodle')) ?></h3>
                <?php
    manage_print_recently_viewed_second();
    ?>
            </div>
        </div>
    </div> <!-- End Right -->
</div>
<?php
} else {
    ?>
<div class="row">
    <div class="col-md-4">
        <div class="card card-chart">
            <div class="card-header" data-background-color="rose" data-header-animation="true">
                <div class="ct-chart" id="websiteViewsChart"></div>
            </div>
            <div class="card-content">
                <div class="card-actions">
                    <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                        <i class="material-icons">build</i> Fix Header!
                    </button>
                    <button type="button" class="btn btn-info btn-simple" rel="tooltip" data-placement="bottom" title="Refresh">
                        <i class="material-icons">refresh</i>
                    </button>
                    <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="Change Date">
                        <i class="material-icons">edit</i>
                    </button>
                </div>
                <h4 class="card-title">Website Views</h4>
                <p class="category">Last Campaign Performance</p>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-chart">
            <div class="card-header" data-background-color="green" data-header-animation="true">
                <div class="ct-chart" id="dailySalesChart"></div>
            </div>
            <div class="card-content">
                <div class="card-actions">
                    <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                        <i class="material-icons">build</i> Fix Header!
                    </button>
                    <button type="button" class="btn btn-info btn-simple" rel="tooltip" data-placement="bottom" title="Refresh">
                        <i class="material-icons">refresh</i>
                    </button>
                    <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="Change Date">
                        <i class="material-icons">edit</i>
                    </button>
                </div>
                <h4 class="card-title">Daily Sales</h4>
                <p class="category">
                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-chart">
            <div class="card-header" data-background-color="blue" data-header-animation="true">
                <div class="ct-chart" id="completedTasksChart"></div>
            </div>
            <div class="card-content">
                <div class="card-actions">
                    <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                        <i class="material-icons">build</i> Fix Header!
                    </button>
                    <button type="button" class="btn btn-info btn-simple" rel="tooltip" data-placement="bottom" title="Refresh">
                        <i class="material-icons">refresh</i>
                    </button>
                    <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="Change Date">
                        <i class="material-icons">edit</i>
                    </button>
                </div>
                <h4 class="card-title">Completed Tasks</h4>
                <p class="category">Last Campaign Performance</p>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="material-icons">weekend</i>
            </div>
            <div class="card-content">
                <p class="category">Bookings</p>
                <h3 class="card-title">184</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-danger">warning</i>
                    <a href="#pablo">Get More Space...</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="rose">
                <i class="material-icons">equalizer</i>
            </div>
            <div class="card-content">
                <p class="category">Website Visits</p>
                <h3 class="card-title">75.521</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">local_offer</i> Tracked from Google Analytics
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="green">
                <i class="material-icons">store</i>
            </div>
            <div class="card-content">
                <p class="category">Revenue</p>
                <h3 class="card-title">$34,245</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="fa fa-twitter"></i>
            </div>
            <div class="card-content">
                <p class="category">Followers</p>
                <h3 class="card-title">+245</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="focus-panel col-md-9">
        <!-- Left Content -->
        <div class="body">
            <div class="wrapper-inner">
                <div class="grid-tabs">
                    <ul>
                        <li><a href="<?php print new moodle_url('/manage'); ?>" class=""><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                        <li><a href="<?php print new moodle_url('/manage/news/'); ?>" class="selected"><?php print_r(get_string('pluginname','block_news_items'))?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="item-page-list">
                    <div id="latestnews">
                        <?php
                            $noteform = new news_edit_form();
                            if ($note = $noteform->get_data()) {
                                $note->userid = $USER->id;
                                if (news_save($note)) {
                                    add_to_log($note->id, 'usernews', 'create', $note->id, 'Create a news');
                                }
                            }
                            $noteform->display();
    
                            //Tung added for role superadmin
                            if(is_siteadmin()){
                                $sql = "SELECT * FROM user where parent = 0";
                                $rows = $DB->get_records_sql($sql);
                                foreach($rows as $row){
                                    $id_list = $row->id;
                                    news_print_notes($id_list);
                                }
                                //$id_list = $id_list."0";
                                
                            }
                            news_print_notes($USER->id);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div class="col-md-3">
        <div class="side-panel">
            <div class="side-panel">
                <h3><?php print_r(get_string('recentlyviewed','moodle')) ?></h3>
                <?php
                    manage_print_recently_viewed();
                ?>
            </div>
        </div>
    </div> <!-- End Right -->
</div>

<?php
    }
    echo $OUTPUT->footer();
?>