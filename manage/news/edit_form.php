<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/formslib.php');

class news_edit_form extends moodleform {

    function definition() {
        $mform =& $this->_form;
        
        $mform->addElement('textarea', 'content', get_string('postsomenewstoyourlearners','form'), array('rows'=>3, 'cols'=>'90'));
        $mform->setType('content', PARAM_RAW);
        $mform->addRule('content', get_string('nocontent', 'notes'), 'required', null, 'client');

        $this->add_action_buttons(FALSE,  get_string('post','form'));
        $mform->addElement('hidden', 'userid');
        $mform->setType('user', PARAM_INT);

    }
}
