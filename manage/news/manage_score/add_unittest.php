<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title('Thêm mới unit test');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Thêm mới unit test');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Thêm mới unit test');
require_login(0, false);
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
if ($action=='add_unittest') {
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	$groupid = optional_param('groupid', '', PARAM_TEXT);
	$studentid = optional_param('studentid', '', PARAM_TEXT);
	$number = optional_param('number', '', PARAM_TEXT);
	$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
	$month = optional_param('month', '', PARAM_TEXT);
	$note = optional_param('note', '', PARAM_TEXT);
	$score = optional_param('score', '', PARAM_TEXT);

	// $newid=save_unittest($studentid,$courseid,$groupid,$school_yearid,$month);
	$newid=save_unittest($studentid,$courseid,$groupid,$school_yearid,$month,$number);
	if(!empty($newid)){
		foreach ($score as $key => $value) {
			save_note_score($newid,$key+1,$note[$key],$score[$key]);
		}
		echo displayJsAlert('Success', $CFG->wwwroot . "/manage/manage_score/index.php");
	}else{
		echo displayJsAlert('Error', $CFG->wwwroot . "/manage/manage_score/add_unittest.php");
	}

}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<!-- <div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Tìm kiếm học sinh</label>
								<input type="text" id="key" name="key" class="form-control" value="">
							</div>
							<div class="" id="return_student">
								
							</div>
						</div> -->
						<input type="text" name="action" value="add_unittest" hidden="">
						<div class="col-md-6">
							<label class="control-label">Chọn trường <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control" required>
								<option value="">Chọn trường</option>
								<?php 
								$schools = get_all_shool();
								foreach ($schools as $key => $value) {
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Chọn khối <span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control" required>
								<option value="">Chọn khối</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Chọn lớp <span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value="">Chọn lớp</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Nhóm học tập<span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value="">Chọn nhóm</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Học sinh<span style="color:red">*</span></label>
							<select name="studentid" id="studentid" class="form-control" required>
								<option value="">Chọn học sinh</option>
							</select>
						</div>
						<div class="col-md-2">
							<div class="form-group label-floating">
								<label class="control-label">Số lượng điểm unit<span style="color:red">*</span></label>
								<input type="number" id="number" name="number" class="form-control" value="" min="1" max="12" required="">
							</div>
						</div>
						
						<div class="col-md-12" id="return_number">
							
						</div>
						<div class="col-md-12">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var course = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var group = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
				$("#studentid").html(data);
			});
		}
	});
	$('#number').on('change keyup', function() {
		var sanitized = $(this).val();
		if(sanitized){
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?number="+sanitized,function(data){
				$("#return_number").html(data);
			});
		}
	});
	$('#key').on('change keyup', function() {
		var search = $(this).val();
		if(search){
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?key="+search,function(data){
				$("#return_number").html(data);
			});
		}
	});
</script>