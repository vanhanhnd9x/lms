<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title('Thêm mới điểm học kì');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Thêm mới điểm học kì');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Thêm mới điểm học kì');
require_login(0, false);
echo $OUTPUT->header();

$school = optional_param('school', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
if ($action=='add_semeste') {
	// thong tin chung
	$schoolid = optional_param('schoolid', '', PARAM_TEXT);
	$block_student = optional_param('block_student', '', PARAM_TEXT);
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	$groupid = optional_param('groupid', '', PARAM_TEXT);
	$studentid = optional_param('studentid', '', PARAM_TEXT);
	$school_yearid = optional_param('school_yearid', '', PARAM_TEXT);
	$semester_status = optional_param('semester_status', '', PARAM_TEXT);
	$school_status = optional_param('school_status', '', PARAM_TEXT);
	//điểm học kì 1
	// diem chung
	$s1_1 = optional_param('s1_1', '', PARAM_TEXT);
	$s2_1 = optional_param('s2_1', '', PARAM_TEXT);
	$s3_1 = optional_param('s3_1', '', PARAM_TEXT);
	$s4_1 = optional_param('s4_1', '', PARAM_TEXT);
	$s5_1 = optional_param('s5_1', '', PARAM_TEXT);
	$noi_1=$s1_1+$s2_1+$s3_1+$s4_1+$s5_1;
	$nghe_1 = optional_param('nghe_1', '', PARAM_TEXT);
	//diem cua rieng tieu hoc
	$doc_viet_1 = optional_param('doc_viet_1', '', PARAM_TEXT);
	//diem cua rieng trung hoc
	$viet_luan_1 = optional_param('viet_luan_1', '', PARAM_TEXT);
	$tv_np_1 = optional_param('tv_np_1', '', PARAM_TEXT);
	$doc_hieu_1 = optional_param('doc_hieu_1', '', PARAM_TEXT);
	//điểm học kì 2
	// diem chung
	$s1_2 = optional_param('s1_2', '', PARAM_TEXT);
	$s2_2 = optional_param('s2_2', '', PARAM_TEXT);
	$s3_2 = optional_param('s3_2', '', PARAM_TEXT);
	$s4_2 = optional_param('s4_2', '', PARAM_TEXT);
	$s5_2 = optional_param('s5_2', '', PARAM_TEXT);
	$noi_2=$s1_2+$s2_2+$s3_2+$s4_2+$s5_2;
	$nghe_2 = optional_param('nghe_2', '', PARAM_TEXT);
	//diem cua rieng tieu hoc
	$doc_viet_2 = optional_param('doc_viet_2', '', PARAM_TEXT);
	//diem cua rieng trung hoc
	$viet_luan_2 = optional_param('viet_luan_2', '', PARAM_TEXT);
	$tv_np_2 = optional_param('tv_np_2', '', PARAM_TEXT);
	$doc_hieu_2 = optional_param('doc_hieu_2', '', PARAM_TEXT);
	// kiểm tra tồn tại của điểm

	$tvnp_dochieu_viet_1= $tv_np_1+$doc_hieu_1+$viet_luan_1;
	$tvnp_dochieu_viet_2= $tv_np_2+$doc_hieu_2+$viet_luan_2;
	switch ($school) {
		case 1:
		$tonghocki_1 =$noi_1+$nghe_1+$doc_viet_1;
		$tonghocki_2 =$noi_2+$nghe_2+$doc_viet_2;
		$url=$CFG->wwwroot . "/manage/manage_score/semester_score.php";
		break;
		case 2:
		$tonghocki_1 =$noi_1+$nghe_1+$tvnp_dochieu_viet_1;
		$tonghocki_2 =$noi_2+$nghe_2+$tvnp_dochieu_viet_2;
		$url=$CFG->wwwroot . "/manage/manage_score/semester_score_high_school.php";
		break;
	}
	$check=check_diem_hoc_ki($studentid,$school_yearid);
	if (!empty($check)) {
		?>
		<script>
			var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $check;?>";
			var e=confirm("Học sinh đã tồn tại điểm trong năm học này, tiến hành chỉnh sửa?");
			if(e==true){
				window.location=urlweb;
			}
		</script>
		<?php 
		# code...
	}else{
		$newid=save_semester($schoolid,$block_student,$courseid,$groupid,$studentid,$school_yearid,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2);
		if(!empty($newid)){
			echo displayJsAlert('Success',$url);
		}else{
			echo displayJsAlert('Error', $CFG->wwwroot . "/manage/manage_score/add_semeste.php");
		}
	}

	
	
}
$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="my_form_cua" action=""  method="post">
					<div class="row">
						<input type="text" name="action" value="add_semeste" hidden="">
						<div class="col-md-6">
							<label class="control-label">Chọn trường <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control">
								<option value="">Chọn trường</option>
								<?php 
								$schools = get_all_shool();
								foreach ($schools as $key => $value) {
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Chọn khối <span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control">
								<option value="">Chọn khối</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Chọn lớp <span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value="">Chọn lớp</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Nhóm học tập<span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value="">Chọn nhóm</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Học sinh<span style="color:red">*</span></label>
							<select name="studentid" id="studentid" class="form-control" required>
								<option value="">Chọn học sinh</option>
							</select>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Chọn năm học<span style="color:red">*</span></label>
								<select name="school_yearid" id="school_yearid" class="form-control" required>
									<?php if($data){
										foreach ($data as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id ?>"><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select>
							</div>
						</div>
						<!-- <div class="col-md-6" >
							<label class="control-label">Nhập điểm cho<span style="color:red">*</span></label>
							<select name="school_status" id="school_status" class="form-control" required>
								<option value="">Lựa chọn</option>
								<option value="1">Học sinh tiểu học</option>
								<option value="2">Học sinh trung học</option>
							</select>
						</div> -->
						<?php 
						switch ($school) {
							case 1:
							?>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;">Semester 1</h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Speaking Mark /25</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s1_1" value="" required="" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s2_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s3_1" value="" required=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s4_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s5_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Listening /20</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Listening /20<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="nghe_1" value="" required="" min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Reading & Writing /55</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Reading & Writing /55<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="doc_viet_1" value="" required="" min="0" max="55">
							</div>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;">Semester 2</h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Speaking Mark /25</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness</label>
								<input type="number" class="form-control" name="s1_2" value=""  max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
								<input type="number" class="form-control" name="s2_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences</label>
								<input type="number" class="form-control" name="s3_2" value=""   min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy </label>
								<input type="number" class="form-control" name="s4_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5</label>
								<input type="number" class="form-control" name="s5_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Listening /20</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Listening /20</label>
								<input type="number" class="form-control" name="nghe_2" value=""  min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Reading & Writing /55</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Reading & Writing /55</label>
								<input type="number" class="form-control" name="doc_viet_2" value=""  min="0" max="55">
							</div>
							<?php 
							break;
							case 2:
							?>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;">Semester 1</h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Speaking Mark /25</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s1_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s2_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s3_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s4_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="s5_1" value="" required="" min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Listening /20</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Listening /20<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="nghe_1" value="" required="" min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Essay /10</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Essay /10<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="viet_luan_1" value="" required="" min="0" max="10">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Vocabulary&Gramar /35</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary&Gramar /35<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="tv_np_1" value="" required="" min="0" max="35">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Reading  /10</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Reading  /10<span style="color:red">*</span></label>
								<input type="number" class="form-control" name="doc_hieu_1" value="" required="" min="0" max="10">
							</div>
							<div class="col-md-12">
								<div class="alert-danger">
									<h4 style="text-align: center;">Semester 2</h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Speaking Mark /25</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Fluency /5: Speed, naturalness</label>
								<input type="number" class="form-control" name="s1_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
								<input type="number" class="form-control" name="s2_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
								<input type="number" class="form-control" name="s3_2" value=""  min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary /5:Range and accuracy </label>
								<input type="number" class="form-control" name="s4_2" value="" min="0" max="5">
							</div>
							<div class="col-md-6">
								<label class="control-label">Communication&Interaction with others, teachers /5</label>
								<input type="number" class="form-control" name="s5_2" value="" min="0" max="5">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Listening /20</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Listening /20</label>
								<input type="number" class="form-control" name="nghe_2" value=""  min="0" max="20">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Essay /10</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Essay /10</label>
								<input type="number" class="form-control" name="viet_luan_2" value=""  min="0" max="10">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Vocabulary&Gramar /35</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Vocabulary&Gramar /35</label>
								<input type="number" class="form-control" name="tv_np_2" value=""  min="0" max="35">
							</div>
							<div class="col-md-12">
								<div class="alert-info">
									<h4>Reading  /10</h4>
								</div>
							</div>
							<div class="col-md-6">
								<label class="control-label">Reading  /10</label>
								<input type="number" class="form-control" name="doc_hieu_2" value=""  min="0" max="10">
							</div>
							<?php 
							break;
						}
						?>
						<div class="col-md-12" >
							<div class="row" id="return_school_status">
								
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " onclick="myFunction()">
								<?php print_r(get_string('or')) ?>
								<?php 
								switch ($school) {
									case 1:
										$urlRedrict=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
									break;
									case 2:
										$urlRedrict= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
									break;
								}
								?>
								<a href="<?php echo $urlRedrict ?>" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var course = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var group = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
				$("#studentid").html(data);
			});
		}
	});
	$('#school_status').on('change', function() {
		var status = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?school_status="+status,function(data){
				$("#return_school_status").html(data);
			});
		}
	});
</script>
<script>
	function myFunction() {
		var x, text;
		var userid = document.getElementById("studentid").value;
		var school_yearid = document.getElementById("school_yearid").value;
		$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?userid="+userid+"&school_yearid="+school_yearid,function(data){
			$("#my_form_cua").html(data);
		});

	}
</script>