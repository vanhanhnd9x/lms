<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title('Xem chi tiết điểm học kì');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Xem chi tiết  điểm học kì');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Xem chi tiết  điểm học kì');
require_login(0, false);
echo $OUTPUT->header();


$action = optional_param('action', '', PARAM_TEXT);
$idview = optional_param('idview', '', PARAM_TEXT);
$semester=$DB->get_record('semester', array(
	'id' => $idview
));


$sql="SELECT * FROM `school_year`";
$data = $DB->get_records_sql($sql);
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="edit_semeste" hidden="">
						<?php $info=show_info_student_in_unit($semester->userid,$semester->courseid,$semester->groupid); ?>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Chọn năm học</label>
								<select name="school_yearid" id="" class="form-control">
									<?php if($data){
										foreach ($data as $key => $value) {
 								# code...
											?>
											<option value="<?php echo $value->id; ?>" <?php if(!empty($semester->school_yearid)&&($semester->school_yearid==$value->id)) echo'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
											<?php 
										}
									} ?>

								</select>
							</div>
						</div>
						<?php 
						if (!empty($semester->school)) {
							switch ($semester->school) {
								case 1:
								?>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;">Semester 1</h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Speaking Mark /25</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s1_1" value="<?php echo  $semester->s1_1;?>" required="" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s2_1" value="<?php echo  $semester->s2_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s3_1" value="<?php echo  $semester->s3_1;?>" required=""  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s4_1" value="<?php echo  $semester->s4_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s5_1" value="<?php echo  $semester->s5_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Listening /20</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Listening /20<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="nghe_1" value="<?php echo  $semester->nghe_1;?>" required="" min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Reading & Writing /55</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Reading & Writing /55<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_viet_1" value="<?php echo  $semester->doc_viet_1;?>" required="" min="0" max="55">
								</div>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;">Semester 2</h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Speaking Mark /25</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness</label>
									<input type="number" class="form-control" name="s1_2" value="<?php echo  $semester->s1_2;?>" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
									<input type="number" class="form-control" name="s2_2" value="<?php echo  $semester->s2_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
									<input type="number" class="form-control" name="s3_2" value="<?php echo  $semester->s3_2;?>"   min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy </label>
									<input type="number" class="form-control" name="s4_2" value="<?php echo  $semester->s4_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5</label>
									<input type="number" class="form-control" name="s5_2" value="<?php echo  $semester->s5_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Listening /20</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Listening /20</label>
									<input type="number" class="form-control" name="nghe_2" value="<?php echo  $semester->nghe_2;?>"  min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Reading & Writing /55</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Reading & Writing /55</label>
									<input type="number" class="form-control" name="doc_viet_2" value="<?php echo  $semester->doc_viet_2;?>"  min="0" max="55">
								</div>
								<?php 
								break;
								case 2:
								?>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;">Semester 1</h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Speaking Mark /25</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s1_1" value="<?php echo  $semester->s1_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s2_1" value="<?php echo  $semester->s2_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s3_1" value="<?php echo  $semester->s3_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s4_1" value="<?php echo  $semester->s4_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="s5_1" value="<?php echo  $semester->s5_1;?>" required="" min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Listening /20</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Listening /20<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="nghe_1" value="<?php echo  $semester->nghe_1;?>" required="" min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Essay /10</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Essay /10<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="viet_luan_1" value="<?php echo  $semester->viet_luan_1;?>" required="" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Vocabulary&Gramar /35</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary&Gramar /35<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="tv_np_1" value="<?php echo  $semester->tv_np_1;?>" required="" min="0" max="35">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Reading  /10</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Reading  /10<span style="color:red">*</span></label>
									<input type="number" class="form-control" name="doc_hieu_1" value="<?php echo  $semester->doc_hieu_1;?>" required="" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-danger">
										<h4 style="text-align: center;">Semester 2</h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Speaking Mark /25</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Fluency /5: Speed, naturalness</label>
									<input type="number" class="form-control" name="s1_2" value="<?php echo  $semester->s1_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
									<input type="number" class="form-control" name="s2_2" value="<?php echo  $semester->s2_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
									<input type="number" class="form-control" name="s3_2" value="<?php echo  $semester->s3_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary /5:Range and accuracy</label>
									<input type="number" class="form-control" name="s4_2" value="<?php echo  $semester->s4_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-6">
									<label class="control-label">Communication&Interaction with others, teachers /5</label>
									<input type="number" class="form-control" name="s5_2" value="<?php echo  $semester->s5_2;?>"  min="0" max="5">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Listening /20</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Listening /20</label>
									<input type="number" class="form-control" name="nghe_2" value="<?php echo  $semester->nghe_2;?>"  min="0" max="20">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Essay /10</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Essay /10</label>
									<input type="number" class="form-control" name="viet_luan_2" value="<?php echo  $semester->viet_luan_2;?>" min="0" max="10">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Vocabulary&Gramar /35</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Vocabulary&Gramar /35</label>
									<input type="number" class="form-control" name="tv_np_2" value="<?php echo  $semester->tv_np_2;?>"  min="0" max="35">
								</div>
								<div class="col-md-12">
									<div class="alert-info">
										<h4>Reading  /10</h4>
									</div>
								</div>
								<div class="col-md-6">
									<label class="control-label">Reading  /10</label>
									<input type="number" class="form-control" name="doc_hieu_2" value="<?php echo  $semester->doc_hieu_2;?>" min="0" max="10">
								</div>
								<?php 
								break;
							}
						}
						?>
						<div class="col-md-12" >
							<div class="row" id="return_school_status">
								
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 10px;">
							<div class="form-buttons" style="border-top: none !important">
								
								<?php 
								switch ($semester->school) {
									case 1:
									$urlRedrict=$CFG->wwwroot.'/manage/manage_score/semester_score.php';
									break;
									case 2:
									$urlRedrict= $CFG->wwwroot.'/manage/manage_score/semester_score_high_school.php';
									break;
								}
								?>
								<a href="<?php echo $urlRedrict; ?>" class="btn btn-success"><?php print_r(get_string('back')) ?></a>
								<!-- <a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a> -->
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var course = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var group = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
				$("#studentid").html(data);
			});
		}
	});
	$('#school_status').on('change', function() {
		var status = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?school_status="+status,function(data){
				$("#return_school_status").html(data);
			});
		}
	});
</script>