<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;

/* 
	cần phải có
	function lưu lại, chỉnh sửa thông tin của một điểm thành phần
	cụ thể: userid,couresid,groupid,school_yearid,month,time_cre
	function lưu lại, cập nhật điểm số và ghi chú điểm cụ thể là
	note_1->12
	score_1->12
*/
	function save_unittest($userid,$courseid,$groupid,$school_yearid,$month,$number){
		global $USER, $CFG, $DB;
		$record = new stdClass();
		$record->userid =$userid;
		$record->courseid =$courseid;
		$record->groupid =$groupid;
		$record->school_yearid =$school_yearid;
		$record->month =$month;
		$record->time_cre = time();
		$record->number = $number;
		$record->del = 0;
		$id_unittest = $DB->insert_record('unittest', $record);
		return $id_unittest;
	}
	function update_school_year_month($unittestid,$school_yearid,$month){
		global $DB;
		$record = new stdClass();
		$record->id = $unittestid;
		$record->school_yearid =$school_yearid;
		$record->month =$month;
		return $DB->update_record('unittest', $record, false);
	}
	function save_note_score($unittestid,$number,$note,$score){
		global $DB;
		$record = new stdClass();
		$record->id = $unittestid;
		switch ($number) {
			case 1:
			$record->note_1 = $note;
			$record->score_1 = $score;
			break;
			case 2:
			$record->note_2 = $note;
			$record->score_2 = $score;
			break;
			case 3:
			$record->note_3 = $note;
			$record->score_3 = $score;
			break;
			case 4:
			$record->note_4 = $note;
			$record->score_4 = $score;
			break;
			case 5:
			$record->note_5 = $note;
			$record->score_5 = $score;
			break;
			case 6:
			$record->note_6 = $note;
			$record->score_6 = $score;
			break;
			case 7:
			$record->note_7 = $note;
			$record->score_7 = $score;
			break;
			case 8:
			$record->note_8 = $note;
			$record->score_8 = $score;
			break;
			case 9:
			$record->note_9 = $note;
			$record->score_9 = $score;
			break;
			case 10:
			$record->note_10 = $note;
			$record->score_10 = $score;
			break;
			case 11:
			$record->note_11 = $note;
			$record->score_11 = $score;
			break;
			case 12:
			$record->note_12 = $note;
			$record->score_12 = $score;
			break;
		}

		return $DB->update_record('unittest', $record, false);
	}
	function delete_unittest($id){
		// global $DB;
		// $DB->delete_records_select('unittest', " id = $id ");
		// return 1;
		global $DB;
		$record = new stdClass();
		$record->id = $id;
		$record->del = 1;
		return $DB->update_record('unittest', $record, false);
	}
	function show_info_student_in_unit($userid,$courseid,$groupid){
		global $CFG, $DB;
    // lấy tên học sinh
		$student=$DB->get_record('user', array(
			'id' => $userid
		));
    // lấy thông tin khóa học
		$course=$DB->get_record('course', array(
			'id' => $courseid
		));
    // thông tin khôi
		$block_student=$DB->get_record('block_student', array(
			'id' => $course->id_khoi
		));
    // thong tin  truong
		$school=$DB->get_record('schools', array(
			'id' => $block_student->schoolid
		)); 
       // lay ten nhom hoc tap
		$group =$DB->get_record('groups', array(
			'id' => $groupid
		)); 
		$yeah=$school_year->sy_start.' - '.$school_year->sy_end;
		$username= $student->lastname.' '.$student->firstname;
		?>
		<div class="col-md-6">
			<label class="control-label">Trường <span style="color:red">*</span></label>
			<input type="text" value="<?php echo $school->name;?>" class="form-control" disabled>
		</div>
		<div class="col-md-6">
			<label class="control-label">Khối <span style="color:red">*</span></label>
			<input type="text" value="<?php echo $block_student->name;?>" class="form-control" disabled>
		</div>
		<div class="col-md-6">
			<label class="control-label">Lớp <span style="color:red">*</span></label>
			<input type="text" value="<?php echo $course->fullname;?>" class="form-control" disabled>
		</div>
		<div class="col-md-6">
			<label class="control-label">Nhóm lớp <span style="color:red">*</span></label>
			<input type="text" value="<?php echo $group->name;?>" class="form-control" disabled>
		</div>
		<div class="col-md-6">
			<label class="control-label">Học sinh <span style="color:red">*</span></label>
			<input type="text" value="<?php echo $username;?>" class="form-control" disabled>
		</div>
		<?php 

	}
	function save_semester($schoolid,$block_studentid,$courseid,$groupid,$userid,$school_yearid,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2){
		global $USER, $CFG, $DB;
		$record = new stdClass();
		$record->schoolid =$schoolid;
		$record->block_studentid =$block_studentid;
		$record->courseid =$courseid;
		$record->groupid =$groupid;
		$record->userid =$userid;
		$record->school_yearid =$school_yearid;
		$record->school =$school;
		// tính điểm học kì 1
		$record->s1_1 =$s1_1;
		$record->s2_1 =$s2_1;
		$record->s3_1 =$s3_1;
		$record->s4_1 =$s4_1;
		$record->s5_1 =$s5_1;
		$record->noi_1 =$noi_1;
		$record->nghe_1 =$nghe_1;
		$record->doc_viet_1 =$doc_viet_1;
		$record->viet_luan_1 =$viet_luan_1;
		$record->tv_np_1 =$tv_np_1;
		$record->doc_hieu_1 =$doc_hieu_1;
		$record->tvnp_dochieu_viet_1 =$tvnp_dochieu_viet_1;
		$record->tonghocki_1 =$tonghocki_1;
		// tính tổng học kì 2
		$record->s1_2 =$s1_2;
		$record->s2_2 =$s2_2;
		$record->s3_2 =$s3_2;
		$record->s4_2 =$s4_2;
		$record->s5_2 =$s5_2;
		$record->noi_2 =$noi_2;
		$record->nghe_2 =$nghe_2;
		$record->doc_viet_2 =$doc_viet_2;
		$record->viet_luan_2 =$viet_luan_2;
		$record->tv_np_2 =$tv_np_2;
		$record->doc_hieu_2 =$doc_hieu_2;
		$record->tvnp_dochieu_viet_2 =$tvnp_dochieu_viet_2;
		$record->tonghocki_2 =$tonghocki_2;

		$record->tongket =0.25*$tonghocki_1+0.75*$tonghocki_2;

		$record->time_cre = time();
		$record->del = 0;
		$id_new = $DB->insert_record('semester', $record);
		return $id_new;
	}
	//update điểm tổng kết năm
	function update_semester($idsemester,$school_yearid,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2){
		global $USER, $CFG, $DB;
		$record = new stdClass();
		$record->id = $idsemester;
		$record->school_yearid =$school_yearid;
		// tính điểm học kì 1
		$record->s1_1 =$s1_1;
		$record->s2_1 =$s2_1;
		$record->s3_1 =$s3_1;
		$record->s4_1 =$s4_1;
		$record->s5_1 =$s5_1;
		$record->noi_1 =$noi_1;
		$record->nghe_1 =$nghe_1;
		$record->doc_viet_1 =$doc_viet_1;
		$record->viet_luan_1 =$viet_luan_1;
		$record->tv_np_1 =$tv_np_1;
		$record->doc_hieu_1 =$doc_hieu_1;
		$record->tvnp_dochieu_viet_1 =$tvnp_dochieu_viet_1;
		$record->tonghocki_1 =$tonghocki_1;
		// tính tổng học kì 2
		$record->s1_2 =$s1_2;
		$record->s2_2 =$s2_2;
		$record->s3_2 =$s3_2;
		$record->s4_2 =$s4_2;
		$record->s5_2 =$s5_2;
		$record->noi_2 =$noi_2;
		$record->nghe_2 =$nghe_2;
		$record->doc_viet_2 =$doc_viet_2;
		$record->viet_luan_2 =$viet_luan_2;
		$record->tv_np_2 =$tv_np_2;
		$record->doc_hieu_2 =$doc_hieu_2;
		$record->tvnp_dochieu_viet_2 =$tvnp_dochieu_viet_2;
		$record->tonghocki_2 =$tonghocki_2;

		$record->time_edit=time();

		$record->tongket =0.25*$tonghocki_1+0.75*$tonghocki_2;
		
		return $DB->update_record('semester', $record, false);
	}
	
	function delete_semester($id){
		global $DB;
		$record = new stdClass();
		$record->id = $id;
		$record->del = 1;
		return $DB->update_record('semester', $record, false);
	}
	
	function get_info_student_in_unit($userid,$courseid,$groupid,$school_yearid){
		global $CFG, $DB;
    // lấy tên học sinh
		$student=$DB->get_record('user', array(
			'id' => $userid
		));
    // lấy thông tin khóa học
		$course=$DB->get_record('course', array(
			'id' => $courseid
		));
    // thông tin khôi
		$block_student=$DB->get_record('block_student', array(
			'id' => $course->id_khoi
		));
    // thong tin  truong
		$school=$DB->get_record('schools', array(
			'id' => $block_student->schoolid
		)); 
       // lay ten nhom hoc tap
		$group =$DB->get_record('groups', array(
			'id' => $groupid
		)); 
    // thong tin nam hoc
		$school_year=$DB->get_record('school_year', array(
			'id' => $school_yearid
		));
		$yeah=$school_year->sy_start.' - '.$school_year->sy_end;
		$username= $student->lastname.' '.$student->firstname;
		echo'
		<td>'.$username.'</td>
		<td>'.$school->name.'</td>
		<td>'.$course->fullname.'</td>
		<td>'.$group->name.'</td>
		<td>'.$yeah.'</td>
		';
	}
	function check_diem_hoc_ki($userid,$school_yearid){
		global $CFG, $DB;
		$sql = "SELECT `semester`.`id`
		FROM `semester` 
		WHERE `semester`.`userid`={$userid}
		AND `semester`.`school_yearid`={$school_yearid}
		AND `semester`.`del`=0
		";
		$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
		return $data;
	}
	function get_all_semester_tieuhoc($page=null,$number=null){
		global $DB, $USER;
		if ($page>1) {
			$start=(($page-1)*$number);
		}else{
			$start=0;
		}
		$sql="SELECT * FROM `semester` WHERE `semester`.`school`=1 AND `semester`.`del`=0 LIMIT {$start} , {$number}";
		$sql_total="SELECT COUNT( DISTINCT `semester`.`id`) FROM `semester` WHERE  `semester`.`del`=0 AND `semester`.`school`=1  ";
		$data=$DB->get_records_sql($sql);
		$total_row=$DB->get_records_sql($sql_total);

		foreach ($total_row as $key => $value) {
        # code...
			$total_page=ceil((int)$key / (int)$number);
			break;
		}
		$kq=array(
			'data'=>$data,
			'total_page'=>$total_page
		);
		return $kq;
	}
	function search_semester($schoolid=null,$block_student=null,$courseid=null,$name=null,$school_year=null,$school_status,$page,$number){
		global $DB, $USER;
		if ($page>1) {
			$start=(($page-1)*$number);
		}else{
			$start=0;
		}
		$sql="SELECT * FROM `semester` WHERE  `semester`.`del`=0 AND `semester`.`school`=1 ";
		$sql_total="SELECT COUNT( DISTINCT `semester`.`id`) FROM `semester` WHERE  `semester`.`del`=0 AND `semester`.`school`=1  ";
		if(!empty($schoolid)){
			$sql.=" AND `semester`.`schoolid`={$schoolid}";
			$sql_total.=" AND `semester`.`schoolid`={$schoolid}";
		}
		if(!empty($block_student)){
			$sql.=" AND `semester`.`block_studentid`={$block_student}";
			$sql_total.=" AND `semester`.`block_studentid`={$block_student}";
		}
		if(!empty($courseid)){
			$sql.=" AND `semester`.`courseid`={$courseid}";
			$sql_total.=" AND `semester`.`courseid`={$courseid}";
		}
		if(!empty($name)){
			$sql.=" AND `semester`.`userid` IN (SELECT `user`.`id` FROM `user` WHERE `user`.`del`=0 AND( `user`.`firstname` LIKE '%{$name}%' OR `user`.`lastname` LIKE '%{$name}%' ))";
			$sql_total.=" AND `semester`.`userid` IN (SELECT `user`.`id` FROM `user` WHERE `user`.`del`=0 AND( `user`.`firstname` LIKE '%{$name}%' OR `user`.`lastname` LIKE '%{$name}%' ))";
		}
		if(!empty($school_year)){
			$sql.=" AND `semester`.`school_yearid`={$school_year}";
			$sql_total.=" AND `semester`.`school_yearid`={$school_year}";
		}
		$sql.=" LIMIT {$start} , {$number}";
		$data=$DB->get_records_sql($sql);

		$total_row=$DB->get_records_sql($sql_total);

		foreach ($total_row as $key => $value) {
        # code...
			$total_page=ceil((int)$key / (int)$number);
			break;
		}
		$kq=array(
			'data'=>$data,
			'total_page'=>$total_page
		);
		return $kq;

	}
	function get_all_semester_trunghoc($page=null,$number=null){
		global $DB, $USER;
		if ($page>1) {
			$start=(($page-1)*$number);
		}else{
			$start=0;
		}
		$sql="SELECT * FROM `semester` WHERE `semester`.`school`=2 AND `semester`.`del`=0 LIMIT {$start} , {$number}";
		$sql_total="SELECT COUNT( DISTINCT `semester`.`id`) FROM `semester` WHERE  `semester`.`del`=0 AND `semester`.`school`=2  ";
		$data=$DB->get_records_sql($sql);
		$total_row=$DB->get_records_sql($sql_total);

		foreach ($total_row as $key => $value) {
        # code...
			$total_page=ceil((int)$key / (int)$number);
			break;
		}
		$kq=array(
			'data'=>$data,
			'total_page'=>$total_page
		);
		return $kq;
	}
	function get_all_school_year_in_semester(){
		global $DB, $USER;
		$sql="SELECT * FROM `school_year` ";
		$data=$DB->get_records_sql($sql);
		return $data;
	}
	?>