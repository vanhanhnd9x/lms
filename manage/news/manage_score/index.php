<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_login(0, false);
$PAGE->set_title('Unit test');
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);


function get_all_unittest_admin($page=null,$number=null){
    global $DB, $USER;
    $sql="SELECT `unittest`.`id`, `unittest`.`userid`, `unittest`.`courseid`, `unittest`.`groupid`,`unittest`.`school_yearid`,`unittest`.`month`,`unittest`.`score_1` FROM `unittest` WHERE `unittest`.`del`=0";
    $data=$DB->get_records_sql($sql);
    $kq=array(
        'data'=>$data,
        'total_page'=>''
    );
    return $kq;
}
$data=get_all_unittest_admin($page=null,$number=null);

function show_month_in_unit($month){
    switch ($month) {
        case 1: echo'Tháng 1'; break;
        case 2: echo'Tháng 2'; break;
        case 3: echo'Tháng 3'; break;
        case 4: echo'Tháng 4'; break;
        case 5: echo'Tháng 5'; break;
        case 6: echo'Tháng 6'; break;
        case 7: echo'Tháng 7'; break;
        case 8: echo'Tháng 8'; break;
        case 9: echo'Tháng 9'; break;
        case 10: echo'Tháng 10'; break;
        case 11: echo'Tháng 11'; break;
        case 12: echo'Tháng 12'; break;
    }
}
?>


<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <div class="col-md-2">
                        <a href="<?php echo new moodle_url('/manage/manage_score/add_unittest.php'); ?>" class="btn btn-info">Thêm mới</a> 
                    </div>
                    <div class="col-md-10">
                       <form action="" method="get" accept-charset="utf-8" class="form-row">
                           <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
                           <div class="col-4">
                              <label for="">Chọn trường</label>
                              <select name="schoolid" id="schoolid" class="form-control">
                                 <option value="">Chọn trường</option>
                                 <?php 
                                 $schools = get_all_shool();
                                 foreach ($schools as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                                    <?php 
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="">Chọn khối</label>
                            <select name="block_student" id="block_student" class="form-control">
                               <option value="">Chọn khối</option>
                           </select>
                       </div>
                       <div class="col-4">
                        <label for="">Chọn lớp</label>
                            <select name="courseid" id="courseid" class="form-control">
                                 <option value="">Chọn lớp</option>
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="">Tên học sinh</label>
                            <input type="text" class="form-control" name="name">
                         </div>
                         <div class="col-4">
                            <label for="">Chọn tháng</label>
                            <select name="" id="" class="form-control">
                                 <option value="">Chọn tháng</option>
                                 <option value="1">Tháng 1</option>
                                 <option value="2">Tháng 2</option>
                                 <option value="3">Tháng 3</option>
                                 <option value="4">Tháng 4</option>
                                 <option value="5">Tháng 5</option>
                                 <option value="6">Tháng 6</option>
                                 <option value="7">Tháng 7</option>
                                 <option value="8">Tháng 8</option>
                                 <option value="9">Tháng 9</option>
                                 <option value="10">Tháng 10</option>
                                 <option value="11">Tháng 11</option>
                                 <option value="12">Tháng 12</option>
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="">Chọn năm học</label>
                            <select name="" id="" class="form-control">
                                 <option value="">Chọn tháng</option>
                                 <option value="1">Tháng 1</option>
                                 <option value="2">Tháng 2</option>
                                 <option value="3">Tháng 3</option>
                                 <option value="4">Tháng 4</option>
                                 <option value="5">Tháng 5</option>
                                 <option value="6">Tháng 6</option>
                                 <option value="7">Tháng 7</option>
                                 <option value="8">Tháng 8</option>
                                 <option value="9">Tháng 9</option>
                                 <option value="10">Tháng 10</option>
                                 <option value="11">Tháng 11</option>
                                 <option value="12">Tháng 12</option>
                            </select>
                        </div>
                      <div class="col-4">
                          <div style="    margin-top: 29px;"></div>
                          <button type="submit" class="btn btn-info">Tìm kiếm</button>
                      </div>
              </form>
          </div>
      </div>

      <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
        <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
            <table id="tech-companies-1" class="table table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th data-priority="6">Tên học sinh</th>
                        <th data-priority="6">Tên trường</th>
                        <th data-priority="6">Tên lớp</th>
                        <th data-priority="6">Tên nhóm học tập</th>
                        <th data-priority="6">Năm học</th>
                        <th data-priority="6">Tháng</th>
                        <th data-priority="6">Điểm unit</th>
                        <th class="disabled-sorting text-right">Hành động</th>
                    </tr>
                </thead>
                <tbody id="ajaxschools">
                    <?php 
                    $i=0;
                    if($data['data']){
                        foreach ($data['data'] as $unit) { 
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <?php 
                                $info=get_info_student_in_unit($unit->userid,$unit->courseid,$unit->groupid,$unit->school_yearid);
                                ?>
                                <td><?php show_month_in_unit($unit->month);?></td>
                                <td><?php echo $unit->score_1; ?></td>
                                <td class="text-right">
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editunit.php?idedit=<?php echo $unit->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewunit.php?idview=<?php echo $unit->id; ?>" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="javascript:void(0)" onclick="delet_unittest(<?php echo $unit->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php }
                    }
                    ?>
                </tbody>
            </table>
        </form>
    </div>
</div>
</div>
</div>
</div>
<?php

echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script>
    function delet_unittest(iddelete){
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
        var e=confirm('Bạn có muốn xóa điểm unittest này không?');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {iddelete:iddelete} ,
                success: function (response) {
                   window.location=urlweb;
               }
           });
        }
    }
</script>
<script>
   var school = $('#schoolid').find(":selected").val();
   if (school) {
      var blockedit = "<?php echo $block_student; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
        $("#block_student").html(data);
    });
      if(blockedit){
        var courseedit = "<?php echo $courseid; ?>";
        $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&courseedit="+courseedit,function(data){
          $("#courseid").html(data);
      });
    }

}
$('#schoolid').on('change', function() {
  var cua = this.value ;
  if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
      $("#block_student").html(data);
  });
}
});
$('#block_student').on('change', function() {
  var block = this.value ;
  if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
      $("#courseid").html(data);
  });
}
});
$('#courseid').on('change', function() {
  var course = this.value ;
  if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
      $("#groupid").html(data);
  });
}
});
</script>