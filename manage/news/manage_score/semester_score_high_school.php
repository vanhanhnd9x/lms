<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
$PAGE->set_title('Quản lý điểm học kì trung học');
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);

$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$action = optional_param('action', '', PARAM_TEXT);
if ($action=='search') {
  $schoolid = optional_param('schoolid', '', PARAM_TEXT);
  $block_student = optional_param('block_student', '', PARAM_TEXT);
  $courseid = optional_param('courseid', '', PARAM_TEXT);
  $name = optional_param('name', '', PARAM_TEXT);
  $school_year = optional_param('school_year', '', PARAM_TEXT);
  $school_status =2;
  $data=search_semester($schoolid,$block_student,$courseid,$name,$school_year,$school_status,$page,$number);
  $url= $CFG->wwwroot.'/manage/manage_score/semester_score.php?action=search&schoolid='.$schoolid.'&block_student='.$block_student.'&courseid='.$courseid.'&name='.$name.'&school_year='.$school_year.'&page=';
}else{
  $data=get_all_semester_trunghoc($page,$number);
  $url=$CFG->wwwroot.'/manage/manage_score/semester_score.php?page=';
}
?>


<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/manage_score/add_semeste.php?school=1'); ?>" class="btn btn-info">Thêm mới</a> 
            <p style="margin-top: 10px"></p>
            <a href="<?php echo new moodle_url('/manage/manage_score/import_semester.php?school=1'); ?>" class="btn btn-info">Nhập excel</a> 
          </div>
          <div class="col-md-10">
           <form action="" method="get" accept-charset="utf-8" class="form-row">
             <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
             <div class="col-4">
              <label for="">Chọn trường</label>
              <select name="schoolid" id="schoolid" class="form-control">
               <option value="">Chọn trường</option>
               <?php 
               $schools = get_all_shool();
               foreach ($schools as $key => $value) {
                ?>
                <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                <?php 
              }
              ?>
            </select>
          </div>
          <div class="col-4">
            <label for="">Chọn khối</label>
            <select name="block_student" id="block_student" class="form-control">
             <option value="">Chọn khối</option>
           </select>
         </div>
         <div class="col-4">
          <label for="">Chọn lớp</label>
          <select name="courseid" id="courseid" class="form-control">
           <option value="">Chọn lớp</option>
         </select>
       </div>
       <div class="col-4">
        <label for="">Tên học sinh</label>
        <input type="text" class="form-control" name="name" placeholder="Tên học sinh" value="<?php echo $name; ?>">
      </div>
      <div class="col-4">
        <label for="">Chọn năm học</label>
        <select name="school_year" id="" class="form-control" required>
         <option value="">Chọn năm học</option>
         <?php 
         if (function_exists('get_all_school_year_in_semester')) {
                        # code...
          $school_year1=get_all_school_year_in_semester();
          if($school_year1){
            foreach ($school_year1 as $key => $value) {
                                # code...
              ?>
              <option value="<?php echo $value->id ; ?>"  <?php if (!empty($school_year)&&$school_year==$value->id) { echo'selected'; } ?>><?php echo $value->sy_start,' - ',$value->sy_end ; ?></option>
              <?php 
            }
          }
        }
        ?>
      </select>
    </div>
    <div class="col-4">
      <div style="    margin-top: 29px;"></div>
      <button type="submit" class="btn btn-info">Tìm kiếm</button>
    </div>
  </form>
</div>
</div>



<div class="table-responsive" data-pattern="priority-columns" id="searchResults">
  <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
    <table id="tech-companies-1" class="table table-hover">
      <thead>
        <tr>
          <th>STT</th>
          <th data-priority="6">Tên học sinh</th>
          <th data-priority="6">Điểm nói</th>
          <th data-priority="6">Điểm nghe</th>
          <th data-priority="6">Điểm đọc & viết</th>
          <th data-priority="6">Tổng học kì</th>
          <th data-priority="6">Năm học</th>
          <th class="disabled-sorting text-right">Hành động</th>
        </tr>
      </thead>
      <tfoot>
      </tfoot>
      <tbody id="ajaxschools">
        <?php 
        if($data['data']){
         $i=0;
         foreach ($data['data'] as $sch) { 
          $i++;
          $student=$DB->get_record('user', array('id' => $sch->userid));
          $username= $student->lastname.' '.$student->firstname;
          $namehoc=$DB->get_record('school_year', array('id' => $sch->school_yearid));
          ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php  echo $username;?></td>
            <td><?php echo $sch->noi; ?></td>
            <td><?php echo $sch->nghe; ?></td>
            <td><?php echo $sch->doc_viet; ?></td>
            <td><?php echo $sch->tonghocki; ?></td>
            <td><?php echo $namehoc->sy_start,' - ',$namehoc->sy_end ; ?></td>
            <td class="text-right">
              <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/editsemester.php?idedit=<?php echo $sch->id; ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
              <a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/viewsemester.php?idview=<?php echo $sch->id; ?>" class="btn btn-warning" title="Xem chi tiết">
                <i class="fa fa-eye" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)" onclick="delet_semester(<?php echo $sch->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
          </tr>
          <?php 
        }
      }else{
        echo'

        <tr><td colspan="8" style="text-align: center;">Dữ liệu trống</td></tr>
        ';
      }

      ?>
    </tbody>
  </table>
</form>
</div>
<?php 
if ($data['total_page']) {
  if ($page > 2) { 
    $startPage = $page - 2; 
  } else { 
    $startPage = 1; 
  } 
  if ($data['total_page'] > $page + 2) { 
    $endPage = $page + 2; 
  } else { 
    $endPage =$data['total_page']; 
  }
}
if($data['data']){
  ?>
  <p>Tổng số trang: <?php echo $data['total_page'] ?></p>
  <nav aria-label="Page navigation example" id="phantrang">
    <ul class="pagination">
      <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php 
      for ($i=$startPage; $i <=$endPage ; $i++) { 
                                            # code...
        ?>
        <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
        <?php 
      }
      ?>
      <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
        <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
  <?php 
}
?>

</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->

<?php

echo $OUTPUT->footer();
?>
<script type="text/javascript" src="<?php print new moodle_url('/manage/schools/js/search.js'); ?>"></script>
<script>
  function delet_semester(iddelete_semester){
    var urlweb= "<?php echo $CFG->wwwroot ?>/manage/manage_score/semester_score_high_school.php";
    var urldelte= "<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php";
    var e=confirm('Bạn có muốn xóa điểm học kì này không?');
    if(e==true){
      alert(iddelete_semester);
      $.ajax({
        url:  urldelte,
        type: "post",
        data: {iddelete_semester:iddelete_semester} ,
        success: function (response) {
         window.location=urlweb;
       }
     });
    }
  }
</script>
<script>
 var school = $('#schoolid').find(":selected").val();
 if (school) {
  var blockedit = "<?php echo $block_student; ?>";
  $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
    $("#block_student").html(data);
  });
  if(blockedit){
    var courseedit = "<?php echo $courseid; ?>";
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&courseedit="+courseedit,function(data){
      $("#courseid").html(data);
    });
  }

}
$('#schoolid').on('change', function() {
  var cua = this.value ;
  if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
      $("#block_student").html(data);
    });
  }
});
$('#block_student').on('change', function() {
  var block = this.value ;
  if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
      $("#courseid").html(data);
    });
  }
});
$('#courseid').on('change', function() {
  var course = this.value ;
  if (this.value) {
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
      $("#groupid").html(data);
    });
  }
});
</script>