 <?php 
 require_once('../../config.php');
 require_once($CFG->dirroot . '/manage/manage_score/lib.php');
 global $USER, $CFG, $DB;
 if (!empty($_GET['number'])) {
 	$sql="SELECT * FROM `school_year`";
 	$data = $DB->get_records_sql($sql);
 	$number=$_GET['number'];
	# code...
 	if ($number>0) {
		# code...
 		?>
 		<div class="row">
 			<div class="col-md-6">
 				<div class="form-group label-floating">
 					<label class="control-label">Chọn năm học</label>
 					<select name="school_yearid" id="" class="form-control">
 						<?php if($data){
 							foreach ($data as $key => $value) {
 								# code...
 								?>
 								<option value="<?php echo $value->id ?>"><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
 								<?php 
 							}
 						} ?>
 						
 					</select>
 				</div>
 			</div>
 			<div class="col-md-6">
 				<div class="form-group label-floating">
 					<label class="control-label">Chọn tháng</label>
 					<select name="month" id="" class="form-control">
 						<option value="1">Tháng 1</option>
 						<option value="2">Tháng 2</option>
 						<option value="3">Tháng 3</option>
 						<option value="4">Tháng 4</option>
 						<option value="5">Tháng 5</option>
 						<option value="6">Tháng 6</option>
 						<option value="7">Tháng 7</option>
 						<option value="8">Tháng 8</option>
 						<option value="9">Tháng 9</option>
 						<option value="10">Tháng 10</option>
 						<option value="11">Tháng 11</option>
 						<option value="12">Tháng 12</option>
 					</select>
 				</div>
 			</div>
 		</div>
 		<?php 
 		for ($i=0; $i < $number; $i++) { 
 			?>
 			<div class="row">
 				<div class="col-md-12">
 					<div class="alert-info">
 						<h4>Điểm unit test thứ <?php echo $i+1; ?></h4>
 					</div>
 				</div>
 				<div class="col-md-6">
 					<div class="form-group label-floating">
 						<label class="control-label">Ghi chú điểm<span style="color:red">*</span></label>
 						<input type="text" id="note" name="note[]" class="form-control" value="" required="">
 					</div>
 				</div>
 				<div class="col-md-6">
 					<div class="form-group label-floating">
 						<label class="control-label">Điểm số<span style="color:red">*</span></label>
 						<input type="number" id="score" name="score[]" class="form-control" value="" required="" min="0" max="10">
 					</div>
 				</div>
 			</div>

 			<?php 
 		}
 	}
 }
 if (!empty($_GET['key'])) {
 		# code...
 	global $DB;
 	$key=trim($_GET['key']);
 	$sql="SELECT DISTINCT `user`.`id`,`user`.`lastname`,`user`.`firstname`,`user`.`birthday`,`user`.`email`,`user`.`phone1`,`user`.`username`,`user`.`name_parent`,`role_assignments`.`roleid` FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 5 AND (`user`.`firstname` LIKE '%{$key}%' OR `user`.`lastname` LIKE '%{$key}%' OR `user`.`username` LIKE '%{$key}%')";
 	$data = $DB->get_records_sql($sql);
 	if ($data) {
 		?>
 		<table id="tech-companies-1" class="table table-hover">
 			<thead>
 				<tr>
 					<th></th>
 					<th data-priority="6"></th>
 					<th data-priority="6">Tên học sinh</th>
 					<th data-priority="6">Ngày sinh</th>
 					<th data-priority="6">Email</th>
 					<th data-priority="6">Điện thoại</th>
 				</tr>
 			</thead>
 			<tbody>
 				<?php 
 				foreach ($data as $key => $value) {
 						# code...
 					?>
 					<tr>
 						<td></td>
 						<td><?php echo $value->lastname,' ',$value->firstname; ?></td>
 						<td><?php echo $value->birthday; ?></td>
 						<td><?php echo $value->email; ?></td>
 						<td><?php echo $value->phone1; ?></td>
 					</tr>
 					<?php 
 					
 				}
 				?>
 			</tbody>
 		</table>
 		<?php 
 	}
 }
 if (!empty($_POST['iddelete'])) {
 	# code...
 	global $DB;
 	$id=$_POST['iddelete'];
 	$delete=delete_unittest($id);
 }
 if (!empty($_GET['school_status'])) {
 	# code...
 	$status=$_GET['school_status'];
 	switch ($status) {
 		case 1:
 		?>
 		<div class="col-md-12">
 			<div class="alert-danger">
 				<h4 style="text-align: center;">Semester 1</h4>
 			</div>
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Speaking Mark /25</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s1_1" value="" required="" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s2_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s3_1" value="" required=""  min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s4_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s5_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Listening /20</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Listening /20<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="nghe_1" value="" required="" min="0" max="20">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Reading & Writing /55</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Reading & Writing /55<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="doc_viet_1" value="" required="" min="0" max="55">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-danger">
 				<h4 style="text-align: center;">Semester 2</h4>
 			</div>
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Speaking Mark /25</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Fluency /5: Speed, naturalness</label>
 			<input type="number" class="form-control" name="s1_2" value=""  max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
 			<input type="number" class="form-control" name="s2_2" value=""  min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences</label>
 			<input type="number" class="form-control" name="s3_2" value=""   min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Vocabulary /5:Range and accuracy </label>
 			<input type="number" class="form-control" name="s4_2" value=""  min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Communication&Interaction with others, teachers /5</label>
 			<input type="number" class="form-control" name="s5_2" value=""  min="0" max="5">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Listening /20</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Listening /20</label>
 			<input type="number" class="form-control" name="nghe_2" value=""  min="0" max="20">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Reading & Writing /55</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Reading & Writing /55</label>
 			<input type="number" class="form-control" name="doc_viet_2" value=""  min="0" max="55">
 		</div>
 		<?php 
 		break;
 		case 2:
 		?>
 		<div class="col-md-12">
 			<div class="alert-danger">
 				<h4 style="text-align: center;">Semester 1</h4>
 			</div>
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Speaking Mark /25</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Fluency /5: Speed, naturalness<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s1_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Accuracy /5: Grammar, basic sentence structures<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s2_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences <span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s3_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Vocabulary /5:Range and accuracy <span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s4_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Communication&Interaction with others, teachers /5<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="s5_1" value="" required="" min="0" max="5">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Listening /20</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Listening /20<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="nghe_1" value="" required="" min="0" max="20">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Essay /10</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Essay /10<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="viet_luan_1" value="" required="" min="0" max="10">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Vocabulary&Gramar /35</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Vocabulary&Gramar /35<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="tv_np_1" value="" required="" min="0" max="35">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Reading  /10</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Reading  /10<span style="color:red">*</span></label>
 			<input type="number" class="form-control" name="doc_hieu_1" value="" required="" min="0" max="10">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-danger">
 				<h4 style="text-align: center;">Semester 2</h4>
 			</div>
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Speaking Mark /25</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Fluency /5: Speed, naturalness</label>
 			<input type="number" class="form-control" name="s1_2" value=""  min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Accuracy /5: Grammar, basic sentence structures</label>
 			<input type="number" class="form-control" name="s2_2" value=""  min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Pronunciation /5: Clarity of speech from single words to sentences </label>
 			<input type="number" class="form-control" name="s3_2" value=""  min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Vocabulary /5:Range and accuracy </label>
 			<input type="number" class="form-control" name="s4_2" value="" min="0" max="5">
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Communication&Interaction with others, teachers /5</label>
 			<input type="number" class="form-control" name="s5_2" value="" min="0" max="5">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Listening /20</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Listening /20</label>
 			<input type="number" class="form-control" name="nghe_2" value=""  min="0" max="20">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Essay /10</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Essay /10</label>
 			<input type="number" class="form-control" name="viet_luan_2" value=""  min="0" max="10">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Vocabulary&Gramar /35</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Vocabulary&Gramar /35</label>
 			<input type="number" class="form-control" name="tv_np_2" value=""  min="0" max="35">
 		</div>
 		<div class="col-md-12">
 			<div class="alert-info">
 				<h4>Reading  /10</h4>
 			</div>
 		</div>
 		<div class="col-md-6">
 			<label class="control-label">Reading  /10</label>
 			<input type="number" class="form-control" name="doc_hieu_2" value=""  min="0" max="10">
 		</div>
 		<?php 
 		break;
 	}
 }
 if (!empty($_POST['iddelete_semester'])) {
 	# code...
 	global $DB;
 	$id=$_POST['iddelete_semester'];
 	$delete=delete_semester($id);
 }
 // if(!empty($_POST[''])&&!empty($_POST[])){
 	

 // }
 ?>