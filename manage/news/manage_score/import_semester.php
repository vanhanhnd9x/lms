<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/Classes/PHPExcel/IOFactory.php'); 
require_once($CFG->dirroot . '/manage/Classes/PHPExcel.php'); 
$PAGE->set_title('Nhập điểm học kì');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Nhập điểm học kì');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Nhập điểm học kì');
require_login(0, false);
echo $OUTPUT->header();
$yeah=date('Y');
$url=$CFG->dirroot . '/manage/manage_score/excel_for_semester/'.$yeah.'/';
if(!file_exists($url)){
	if (!mkdir($url, 0777, true)) {
		die('Tạo thư mục không thành công');
	}

}
$action = optional_param('action', '', PARAM_TEXT);
if ($action=='import_semester') {
	$schoolid = optional_param('schoolid', '', PARAM_TEXT);
	$block_student = optional_param('block_student', '', PARAM_TEXT);
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	$groupid = optional_param('groupid', '', PARAM_TEXT);
	$studentid = optional_param('studentid', '', PARAM_TEXT);
	$number = optional_param('number', '', PARAM_TEXT);
	$school_year = optional_param('school_year', '', PARAM_TEXT);
	$school = optional_param('school', '', PARAM_TEXT);
	if ($_FILES['excel']['error'] > 0)
		echo "Upload lỗi rồi!";
	else {
		// $url=$CFG->dirroot . '/manage/excel_for_semester/';
		if($_FILES['excel']['type']=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
			//Đường dẫn file
			$newparth= $url.time().$_FILES["excel"]["name"];
			move_uploaded_file($_FILES["excel"]["tmp_name"], $newparth); //Move the file from the temporary position till a new one.
			$file =  $newparth;
			$objFile = PHPExcel_IOFactory::identify($file);
			$objData = PHPExcel_IOFactory::createReader($objFile);

			$objData->setReadDataOnly(true);
			$objPHPExcel = $objData->load($file);
			$sheet = $objPHPExcel->setActiveSheetIndex(0);
			$Totalrow = $sheet->getHighestRow();
			$LastColumn = $sheet->getHighestColumn();
			$TotalCol = PHPExcel_Cell::columnIndexFromString($LastColumn);
			$data = [];
			switch ($school) {
				case 1:
				for ($i = 3; $i <= $Totalrow; $i++) {
					$uid=get_id_user_by_code($sheet->getCellByColumnAndRow(0, $i)->getValue());

					if(!empty($uid)){
						$check= check_diem_hoc_ki($uid,$school_year);
						if(empty($check)){
							$s1_1=$sheet->getCellByColumnAndRow(1, $i)->getValue();
							$s2_1=$sheet->getCellByColumnAndRow(2, $i)->getValue();
							$s3_1=$sheet->getCellByColumnAndRow(3, $i)->getValue();
							$s4_1=$sheet->getCellByColumnAndRow(4, $i)->getValue();
							$s5_1=$sheet->getCellByColumnAndRow(5, $i)->getValue();
							$noi_1=$s1_1+$s2_1+$s3_1+$s4_1+$s5_1;
							$nghe_1=$sheet->getCellByColumnAndRow(6, $i)->getValue();
							$doc_viet_1= $sheet->getCellByColumnAndRow(7, $i)->getValue();
							$tonghocki_1=$noi_1+$nghe_1+$doc_viet_1;
							save_semester($schoolid,$block_student,$courseid,$groupid,$uid,$school_year,$school,$s1_1,$s2_1,$s3_1,$s4_1,$s5_1,$noi_1,$nghe_1,$doc_viet_1,$viet_luan_1,$tv_np_1,$doc_hieu_1,$tvnp_dochieu_viet_1,$tonghocki_1,$s1_2,$s2_2,$s3_2,$s4_2,$s5_2,$noi_2,$nghe_2,$doc_viet_2,$viet_luan_2,$tv_np_2,$doc_hieu_2,$tvnp_dochieu_viet_2,$tonghocki_2);
						}
					}
				}
				echo displayJsAlert('', $CFG->wwwroot . "/manage/manage_score/semester_score.php");
				break;
				case 2:
					# code...
				break;
				
			}
			// for ($i = 2; $i <= $Totalrow; $i++) {
			// 	for ($j = 0; $j < $TotalCol; $j++) {
			// 		$data[$i - 2][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
			// 	}
			// }
			// echo '<pre>';
			
		}
	}

}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post" enctype="multipart/form-data">
					<div class="row">
						<!-- <div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Tìm kiếm học sinh</label>
								<input type="text" id="key" name="key" class="form-control" value="">
							</div>
							<div class="" id="return_student">
								
							</div>
						</div> -->
						<input type="text" name="action" value="import_semester" hidden="">
						<div class="col-md-6">
							<label class="control-label">Chọn trường <span style="color:red">*</span></label>
							<select name="schoolid" id="schoolid" class="form-control" required>
								<option value="">Chọn trường</option>
								<?php 
								$schools = get_all_shool();
								foreach ($schools as $key => $value) {
									?>
									<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Chọn khối <span style="color:red">*</span></label>
							<select name="block_student" id="block_student" class="form-control" required>
								<option value="">Chọn khối</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Chọn lớp <span style="color:red">*</span></label>
							<select name="courseid" id="courseid" class="form-control" required>
								<option value="">Chọn lớp</option>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label">Nhóm học tập<span style="color:red">*</span></label>
							<select name="groupid" id="groupid" class="form-control" required>
								<option value="">Chọn nhóm</option>
							</select>
						</div>
						<div class="col-6">
							<label for="">Chọn năm học</label>
							<select name="school_year" id="" class="form-control" required>
								<option value="">Chọn năm học</option>
								<?php 
								if (function_exists('get_all_school_year_in_semester')) {
                        # code...
									$school_year1=get_all_school_year_in_semester();
									if($school_year1){
										foreach ($school_year1 as $key => $value) {
                                # code...
											?>
											<option value="<?php echo $value->id ; ?>"  <?php if (!empty($school_year)&&$school_year==$value->id) { echo'selected'; } ?>><?php echo $value->sy_start,' - ',$value->sy_end ; ?></option>
											<?php 
										}
									}
								}
								?>
							</select>
						</div>
						<div class="col-md-6" id="return_number">
							<label class="control-label">Chọn file excel<span style="color:red">*</span></label>
							<input type="file" value="Chọn file" required="" class="form-control" name="excel">
							<div class="alert-link"><a href="">Xem file mẫu tại đây</a></div>
						</div>
						<div class="col-md-12" style="margin-top: 10px">
							<div class="form-buttons" style="border-top: none !important">
								<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
								<?php print_r(get_string('or')) ?>
								<a href="<?php echo $CFG->wwwroot ?>/manage/manage_score/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var course = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+course,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var group = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+group,function(data){
				$("#studentid").html(data);
			});
		}
	});
	$('#number').on('change keyup', function() {
		var sanitized = $(this).val();
		if(sanitized){
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?number="+sanitized,function(data){
				$("#return_number").html(data);
			});
		}
	});
	$('#key').on('change keyup', function() {
		var search = $(this).val();
		if(search){
			$.get("<?php echo $CFG->wwwroot ?>/manage/manage_score/load_ajax.php?key="+search,function(data){
				$("#return_number").html(data);
			});
		}
	});
</script>