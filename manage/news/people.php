<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

require_once('../../config.php');
require_once('lib.php');
require_once('../lib.php');

require_once('edit_form.php');

require_login(0, false);

/// create form
$noteform = new news_edit_form();


/// if form was cancelled then return to the notes list of the note
if ($noteform->is_cancelled()) {
    redirect($CFG->wwwroot . '/manage/news');
}
$PAGE->set_title("Lastest News");
$PAGE->set_heading("Lastest News");
//now the page contents
$PAGE->set_pagelayout('Dashboard');

$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/news/news.js');
echo $OUTPUT->header();
?>

<div id="admin-fullscreen">

    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            
            <div class="grid-tabs">
                <ul>
                    <li><a href="<?php print new moodle_url('/manage'); ?>" class=""><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                    <li><a href="<?php print new moodle_url('/manage/news/'); ?>" class="selected"><?php print_r(get_string('pluginname','block_news_items'))?></a></li>         
                    
                </ul>
                <div class="clearfix"></div>
                
                
            </div>

            <div class="item-page-list">
                
                 <?php
                
                /// create form
                $noteform = new news_edit_form();

                /// if form was cancelled then return to the notes list of the note
//                if ($noteform->is_cancelled()) {
//                    redirect($CFG->wwwroot . '/manage/news');
//                }

                /// if data was submitted and validated, then save it to database
                $userid = optional_param('uid', 0, PARAM_INT);
    						$userid          = $userid ? $userid : $USER->id;
    						
                if ($note = $noteform->get_data()) {
                    $userid = $USER->id;
                    if (news_save($note)) {
                        add_to_log($userid, 'usernews', 'create', $userid, 'Create a news');
                    }
                    // redirect to notes list that contains this note
                    //redirect($CFG->wwwroot . '/manage/news');
                }
                $noteform->display();
                
                news_print_notes($userid);
                
                ?>
                
                
            
            </div>


        </div> 
    </div><!-- Left Content -->
        <!-- Right Block -->
        <div id="admin-fullscreen-right">

            <div class="side-panel">
               <div class="side-panel">
                <h3><?php print_r(get_string('recentlyviewed','moodle')) ?></h3>
               <?php 
                    manage_print_recently_viewed();
               ?>
            </div>
            </div>
        </div>  <!-- End Right -->

</div>      
<?php
	echo $OUTPUT->footer();
?>