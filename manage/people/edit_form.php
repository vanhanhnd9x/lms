<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

define("ADMIN_ROLE", 3);
define("TEACHER_ROLE", 8);
define("LEARNER_ROLE", 5);

require_once($CFG->dirroot.'/lib/formslib.php');

class user_edit_form extends moodleform {

    // Define the form
    function definition () {
        global $CFG, $COURSE;
        $mform =& $this->_form;

      	$mform->addElement('html','<div id="peoplenew">');
        $mform->addElement('text', 'username', get_string('username'), 'maxlength="100" size="25" ');
      	$mform->addElement('html', '<div class="left">');
        $mform->addElement('text', 'firstname', get_string('firstname'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'lastname', get_string('lastname'), 'maxlength="100" size="25" ');
        $mform->addRule('username', null, 'required', null);
        $mform->addRule('firstname', null, 'required', null);
        $mform->addElement('html','</div><div class="right">');
        $mform->addElement('text', 'title', get_string('title'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'company', get_string('company'), 'maxlength="100" size="25" ');
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'email', get_string('email'), 'maxlength="100" size="25" ');
        $mform->addElement('checkbox', 'notifications', '&nbsp;'	,  get_string('sendemailnoti'));
        $mform->addElement('html','<hr />');
      	$mform->addElement('html','</div>');

        $mform->addElement(
					'html',
					'<div class="left">'
        );
        $roles = array();
       	//$roles[4] = 'Account Owner';
       	$roles[ADMIN_ROLE] = 'Administrator';
       	//$roles[3] = 'Team Leader';
       	$roles[TEACHER_ROLE] = 'Teacher';
        $roles[LEARNER_ROLE] = 'Learner';

        $mform->addElement('select', 'roleid', get_string('PersonAccessLevelID'), $roles);
        $mform->setDefault('roleid', LEARNER_ROLE);
        //Quang added to fix for 406
        if(isset($_REQUEST['user_id'])){
            if(people_get_roles($_REQUEST['user_id'])=='Administrator'){
            $mform->setDefault('roleid', ADMIN_ROLE);        
            }           
        $mform->addElement('hidden', 'userid', $_REQUEST['user_id']);
            
        }
        
        //Finish QUang added
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'address', get_string('address'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'address2', "&nbsp;", 'maxlength="100" size="25" ');
        $mform->addElement('text', 'city', get_string('city'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'state', get_string('state'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'zip', get_string('zip'), 'maxlength="100" size="25" ');

        $choices = get_string_manager()->get_list_of_countries();
		    $choices= array(''=>get_string('selectacountry').'...') + $choices;

		    $mform->addElement('select', 'country', get_string('selectacountry'), $choices);
		    if (!empty($CFG->country)) {
		        $mform->setDefault('country', $CFG->country);
		    }

		    $choices = get_list_of_timezones();
		    $choices['99'] = get_string('serverlocaltime');
		   	$mform->addElement('select', 'timezone', get_string('timezone'), $choices);
		    $mform->setDefault('timezone', '99');

       	$mform->addElement(
					'html',
					'</div>' .
					'<div class="right">'
        );
        $mform->addElement('text', 'phone1', get_string('phonework'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'phone2', get_string('phonemobile'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'skype', get_string('skypeid'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'twitter', get_string('twitter'), 'maxlength="100" size="25" ');
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'url', get_string('website'), 'maxlength="100" size="25" ');
        $mform->addElement(
					'html',
					'</div>'
        );
      	$mform->addElement('html','</div>');
        $buttonarray=array();
//Quang change when edit person profile, button is still Add Person				
//$buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('addperson'));
                                $buttonarray[] = &$mform->createElement('submit', 'submitbutton', 'Save');
				$buttonarray[] = &$mform->createElement('cancel');
				$mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
				$mform->closeHeaderBefore('buttonar');

        $mform->addElement('hidden', 'id');
        $mform->setType('user', PARAM_INT);
    }

    function validation($usernew, $files) {
        global $CFG, $DB;

        $errors = parent::validation($usernew, $files);

        $usernew = (object)$usernew;
        $user    = $DB->get_record('user', array('id'=>$usernew->id));

        // validate email
        if (!isset($usernew->email)) {
            // mail not confirmed yet
        } else if (!validate_email($usernew->email)) {
            $errors['email'] = get_string('invalidemail');
        } else if (($usernew->email !== $user->email) and $DB->record_exists('user', array('email'=>$usernew->email, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['email'] = get_string('emailexists');
        }

        if (isset($usernew->email) and $usernew->email === $user->email and over_bounce_threshold($user)) {
            $errors['email'] = get_string('toomanybounces');
        }

        if (isset($usernew->email) and !empty($CFG->verifychangedemail) and !isset($errors['email']) and !has_capability('moodle/user:update', get_context_instance(CONTEXT_SYSTEM))) {
            $errorstr = email_is_not_allowed($usernew->email);
            if ($errorstr !== false) {
                $errors['email'] = $errorstr;
            }
        }

        /// Next the customisable profile fields
        $errors += profile_validation($usernew, $files);

        return $errors;
    }
}



