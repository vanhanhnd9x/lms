$(document).ready(function(){
 

    if ($("#to").length) {
        $("#to").autocomplete("livesearch.php", {
            width: 260,
            matchContains: true,
            selectFirst: false
        });
        $('#to').result(function (event, data, formatted) {
            item_autocomplete_select(data);
        });
    }

    $('#searchInBox').focus(function() {
        $('#searchInBoxLabel').html('');
    });

    $('#AddMore').click(function() {
        document.getElementById('AddAnother').value=1;
    });
    $('#assignCancel').click(function() {
        $('#facebox').hide();
        $('#overlay').fadeOut('fast');
  
    });
    $('#AssignAll').click(function(event) {   
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
            
                if($(this).attr('id')!='SendEmail'){
                    this.checked = true;                        
                }
            });            
        }
        else { 
            // Iterate each checkbox 
            $(":checkbox").each(function() {
                if($(this).attr('id')!='SendEmail'){
                    this.checked = false;
                }
            });
        }
    });
    $('#searchBox').focus(function() {
        $('#searchPrompt').html('');
    });
    $('#Person_UserName').keyup(function() {
        check_user_name_exist($(this).val());
    });


    //team course
    $('#assignSearchCourse').focus(function() {
        $('#searchPrompt').html('');
    });
    $('#assignSearchCourse').keyup(function() {
       
        if($(this).val().length>0){
            search_course_to_assign($(this).val());
        }
        else{
            $('#searchPrompt').html('Search for courses to assign');
            search_course_to_assign('');
        }
    });
    $("#assignSearchCourse").focusout(function() {
        document.getElementById('assignSearchCourse').value='';
        $('#searchPrompt').html('Search for courses to assign');
    });
    //finish team course

    //in viewteam.php, assign people to team
    $('#assignSearch').focus(function() {
        $('#searchPrompt').html('');
    });
    $('#assignSearch').keyup(function() {
        //var team_id=document.getElementById('team_id').value;
        var user_id=document.getElementById('user_id').value;
    
        if($(this).val().length>0){
            search_team_to_assign($(this).val(),user_id);
        }
        else{
            $('#searchPrompt').html('Search for team to assign');
            search_team_to_assign('',user_id);
        }
    });
    $("#assignSearch").focusout(function() {
        document.getElementById('assignSearch').value='';
        $('#searchPrompt').html('Search for people to assign');
    });
    //finish in viewteam.php, assign people to team
    $('#searchBox').keyup(function() {
        if($(this).val().length>0){
            search_team_when_key_up($(this).val());
        }
        else{
            //$('#searchPrompt').html('Search by team name');
            $('#searchPrompt').html('Tìm kiếm theo tên nhóm');
        }
    });
    $("#searchBox").focusout(function() {
        document.getElementById('searchBox').value='';
        //$('#searchPrompt').html('Search by team name');
        $('#searchPrompt').html('Tìm kiếm theo tên nhóm');
    });


    $(function() {
        $('#assignTeam').click(function(){
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    $(function() {
        $('#assignCourses').click(function(){
            
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    //assign course to subteam
    
    $('#SubTeams').click(function(event) {   
        if(this.checked) {
            document.getElementById('assign_course_to_subteam').value=1;
            
        }
        else { 
           document.getElementById('assign_course_to_subteam').value=0; 
        }
    });
    
    //sub team
    
    //send mail notification when assing user to team
    
     $('#SendEmail').click(function(event) {   
        if(this.checked) {
            document.getElementById('send_email_notification').value=1;
            
        }
        else { 
           document.getElementById('send_email_notification').value=0; 
        }
    });
    //finish send mail notification when assing user to team
    
    $('#InheritCourses').click(function(event) {   
        if(this.checked) {
            document.getElementById('inherit_course').value=1;
        }
        else { 
           document.getElementById('inherit_course').value=0; 
        }
    });
});

function check_user_name_exist(search_val){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("Person_UserName_validationMessage").innerHTML=xmlhttp.responseText;
            
        }
        if(document.getElementById("Person_UserName_validationMessage").innerHTML!=""){
            $("#Person_UserName").addClass("input-validation-error");
            $('#saveUser').attr("disabled", "disabled");

        }else{
            $("#Person_UserName").removeClass("input-validation-error");  
            $('#saveUser').removeAttr("disabled");
        }
        
    }
    
    xmlhttp.open("GET","livesearch_check_user_name_exist.php?q="+search_val,true);
    xmlhttp.send();
}

function search_sent_when_key_up(search_val){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("searchResults").innerHTML=xmlhttp.responseText;
        }
    }
    
    xmlhttp.open("GET","livesearch_sent_ms.php?q="+search_val,true);
    xmlhttp.send();
}

function search_course_to_assign(search_val){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("assignResults").innerHTML=xmlhttp.responseText;
            
        }
    }
    xmlhttp.open("GET","livesearch_course_assign_to_team.php?q="+search_val,true);
    xmlhttp.send();
}

function search_team_to_assign(search_val,user_id){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("assignResults").innerHTML=xmlhttp.responseText;
            
        }
    }
    xmlhttp.open("GET","livesearch_team_assign_to_people.php?q="+search_val+'&user_id='+user_id,true);
    xmlhttp.send();
}

function search_team_when_key_up(search_val){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("searchResults").innerHTML=xmlhttp.responseText;
            
        }
    }
    
    xmlhttp.open("GET","livesearch_team.php?q="+search_val,true);
    xmlhttp.send();
}


function item_autocomplete_select(data){
  
    if(document.getElementById('hidden_to')){
        receive=document.getElementById('hidden_to').value=data;
    }
   
   
}
function displayConfirmBox(redirectPath,id,ms){
    var where_to= confirm(ms);
    if (where_to== true)
    {
        window.location=redirectPath;
    }
    else
    {
	
        return false;
    }
}

function validate_form_add_member_to_team(){
    var first_name=document.getElementById('Person_FirstName').value;
    
    if(first_name==''){
        $("#Person_FirstName").addClass("input-validation-error");
        $('#Person_FirstName_validationMessage').show();
        $('#Person_FirstName_validationMessage').html('first name is required');
        $('#Person_FirstName_validationMessage').focus();
        return false;    
    }
    var username=document.getElementById('Person_UserName').value;
    if(username==''){
        $("#Person_UserName").addClass("input-validation-error");
        $('#Person_UserName_validationMessage').show();
        $('#Person_UserName_validationMessage').html('username is required');
        $('#Person_UserName_validationMessage').focus();
        return false;    
    }


    return true;
}

