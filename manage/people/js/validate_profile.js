function validate() {
    var error = 0;

    if (document.getElementById('firstname') && document.getElementById('firstname').value == '') {
        $("#erFirst").show();
        error = 1;
    } else {
        $("#erFirst").hide();
    }
    if (document.getElementById('lastname') && document.getElementById('lastname').value == '') {
        $("#erLast").show();
        error = 1;
    } else {
        $("#erLast").hide();
    }

    if (document.getElementById('magv') && document.getElementById('magv').value == '') {
        $("#erMaGV").show();
        error = 1;
    } else {
        $("#erMaGV").hide();
    }

    // if($('#file').get(0).files.length==0){
    //   $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Chọn ảnh đại diện</div>');
    //   error=1;
    // } 

    // if (document.getElementById('username') && document.getElementById('username').value == '') {
    //     $("#erUser").show();
    //     error = 1;
    // } else {
    //     $("#erUser").hide();
    // }

    if (document.getElementById('password') && document.getElementById('password').value == '') {
        $("#erPass").show();
        error = 1;
    } else {
        $("#erPass").hide();
    }

    // if (document.getElementById('password') && document.getElementById('confirm_password')) {
    //     password = document.getElementById('password').value;
    //     confirm_password = document.getElementById('confirm_password').value;
    //     if (password != confirm_password) {
    //         $("#erPass").show();
    //         $("#erPassConfirm").show();
    //         error = 1;
    //     }

    // } else {
    //     $("#erPass").hide();
    //     $("#erPassConfirm").hide();
    // }

    // var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    // if (document.getElementById("dupUsername") && (document.getElementById("dupUsername").value == 1 || document.getElementById("dupUsername").value == '1')) {
    //     document.getElementById("erUserName").innerHTML = 'Tài khoản này đã được sử dụng';
    //     $("#erUserName").show();
    //     error = 1;
    // } else if (document.getElementById('username') && document.getElementById('username').value == '') {
    //     $("#erUserName").show();
    //     error = 1;
    // } else if (reg.test(document.getElementById('username').value) == false) {
    //     document.getElementById("erUserName").innerHTML = 'Email nhập không đúng';
    //     $("#erUserName").show();
    //     error = 1;
    //     document.getElementById("errEmail").value == 1;
    // } else {
    //     $("#erUserName").hide();
    // }



    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (document.getElementById("email") && (document.getElementById("email").value == '')) {
        document.getElementById("erEmail").innerHTML = 'Vui lòng nhập email';
        $("#erEmail").show();
        error = 1;
    } else if (document.getElementById('email') && reg.test(document.getElementById('email').value) == false) {
        document.getElementById("erEmail").innerHTML = 'Email nhập sai định dạng';
        $("#erEmail").show();
        error = 1;
    } else {
        $("#erEmail").hide();
    }

    var reg =/^[0-9]{9,11}$/;
    if(document.getElementById('phone').value !='' && reg.test(document.getElementById('phone').value) == false) {
        document.getElementById("er_phone").innerHTML = 'Số điện thoại không hợp lệ';
        $("#er_phone").show();
        error = 1;
    }else {
        $("#er_phone").hide();
    }

    if (error == 1) {
        return false;
    }
    return true;
}




$(document).ready(function () {
    $('.mid').click(function () {

        $('#upPhoto').show();
    });
    $('#cancelUpload').click(function () {

        $('#upPhoto').hide();
    });

    $("#username").keyup(function () {
        var userName = $(this).val();

        searchWhenKeyUpUserName(userName);
    });
    $("#username").focusout(function () {
        var userName = $(this).val();
        searchWhenKeyUpUserName(userName);
    });
    $("#email").keyup(function () {
        var email = $(this).val();
        var userId = 0;
        if (document.getElementById('user_id')) {
            userId = document.getElementById('user_id').value;
        }

        searchWhenKeyUpEmail(email, userId);
    });
    $("#email").focusout(function () {
        var email = $(this).val();
        var userId = 0;
        if (document.getElementById('user_id')) {
            userId = document.getElementById('user_id').value;
        }
        searchWhenKeyUpEmail(email, userId);
    });

    $("#notifications").click(function (event) {
        if (this.checked) {
            document.getElementById('send_notif').value = 1;
        } else {
            document.getElementById('send_notif').value = 0;
        }
    });

});



function searchWhenKeyUpUserName(username)
{
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            if (xmlhttp.responseText == '1' || xmlhttp.responseText == 1) {
                document.getElementById("erUserName").innerHTML = 'Tài khoản này đã được sử dụng';
                $("#erUserName").show();
                document.getElementById("dupUsername").value = 1;
            } else {
                $("#erUserName").hide();
                document.getElementById("dupUsername").value = 0;
            }
        }
    }

    xmlhttp.open("GET", "ajaxcheckusername.php?q=" + username, true);
    xmlhttp.send();
}
function searchWhenKeyUpEmail(email, userId) {
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {

            if (xmlhttp.responseText == '1' || xmlhttp.responseText == 1) {

                document.getElementById("erEmail").innerHTML = 'Email is already existed';
                $("#erEmail").show();
                document.getElementById("dupEmail").value = 1;

            } else if (document.getElementById("errEmail") && document.getElementById("errEmail").value == 0) {
                $("#erEmail").hide();
                document.getElementById("dupEmail").value = 0;
            }


        }
    }

    xmlhttp.open("GET", "ajaxcheckemail.php?q=" + email + '&userId=' + userId, true);
    xmlhttp.send();
}



