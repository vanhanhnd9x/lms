<?php
/*
 * Created on Apr 26, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

define("ADMIN_ROLE", 3);
define("TEACHER_ROLE", 8);
define("LEARNER_ROLE", 5);

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

class people_new_form extends moodleform {

    function definition() {
        $mform =& $this->_form;
        
      	$mform->addElement('html','<div id="peoplenew">');
        $mform->addElement('text', 'firstname', get_string('firstname'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'lastname', get_string('lastname'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'username', get_string('username'), 'maxlength="100" size="25" ');
        $mform->addRule('username', null, 'required', null);
        $mform->addRule('firstname', null, 'required', null);
       	$mform->addElement('checkbox', 'loginemail',null, 'Send a welcome email to this person inviting them to login');
        $mform->setDefault('loginemail', 1);
        $mform->addElement(
					'html', 
					'<div class="form-sub-heading">'.  get_string('optionalsettings','assignment').'</div>' .
					'<div class="left">'
        );
        $roles = array();
       	//$roles[4] = 'Account Owner';
       	$roles[ADMIN_ROLE] = 'Administrator';
       	//$roles[3] = 'Team Leader';
       	$roles[TEACHER_ROLE] = 'Teacher';
        $roles[LEARNER_ROLE] = 'Learner';
       	
        $mform->addElement('select', 'roleid', get_string('PersonAccessLevelID'), $roles);
        $mform->setDefault('roleid', LEARNER_ROLE);
        $mform->addElement('passwordunmask', 'password', get_string('password'), 'maxlength="100" size="25" ');
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'address', get_string('address'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'address2', "&nbsp;", 'maxlength="100" size="25" ');
        $mform->addElement('text', 'city', get_string('city'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'state', get_string('state'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'zip', get_string('zip'), 'maxlength="100" size="25" ');
        
        $choices = get_string_manager()->get_list_of_countries();
		    $choices= array(''=>get_string('selectacountry').'...') + $choices;
		    
		    $mform->addElement('select', 'country', get_string('selectacountry'), $choices);
		    if (!empty($CFG->country)) {
		        $mform->setDefault('country', $CFG->country);
		    }
		
		    $choices = get_list_of_timezones();
		    $choices['99'] = get_string('serverlocaltime');
		   	$mform->addElement('select', 'timezone', get_string('timezone'), $choices);
		    $mform->setDefault('timezone', '99');
		    
       	$mform->addElement(
					'html', 
					'</div>' .
					'<div class="right">'
        );
        
        $mform->addElement('text', 'email', get_string('email'), 'maxlength="100" size="25" ');
        $mform->addElement('checkbox', 'DisableMessages', '&nbsp;'	,'Disable all email notifications');
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'title', get_string('title'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'company', get_string('company'), 'maxlength="100" size="25" ');
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'phone1', get_string('phonework'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'phone2', get_string('phonemobile'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'skype', get_string('skypeid'), 'maxlength="100" size="25" ');
        $mform->addElement('text', 'twitter', get_string('twitter'), 'maxlength="100" size="25" ');
        $mform->addElement('html','<hr />');
        $mform->addElement('text', 'url', get_string('website'), 'maxlength="100" size="25" ');
        $mform->addElement(
					'html', 
					'</div>'
        );
         //$mform->addElement('header', 'nameforyourheaderelement', get_string('titleforlegened', 'modulename'));
				 //$mform->closeHeaderBefore('buttonar');
				 
        $buttonarray=array();
//Quang change when edit person profile, button is still Add Person				
//$buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('addperson'));
                                $buttonarray[] = &$mform->createElement('submit', 'submitbutton', 'Save');
				//$buttonarray[] = &$mform->createElement('reset', 'resetbutton', get_string('revert'));
				$buttonarray[] = &$mform->createElement('cancel');
				$mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
				$mform->closeHeaderBefore('buttonar');
        
        $mform->addElement('hidden', 'userid');
        $mform->setType('user', PARAM_INT);

      	$mform->addElement('html','</div>');
    }
    
    
    function validation($data, $files) {
    	 global $CFG, $DB;
        $errors = parent::validation($data, $files);

        $authplugin = get_auth_plugin($CFG->registerauth);
        if ($DB->record_exists('user', array('username'=>$data['username'], 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['username'] = get_string('usernameexists');
        } else {
            //check allowed characters
            if ($data['username'] !== moodle_strtolower($data['username'])) {
                $errors['username'] = get_string('usernamelowercase');
            } else {
                if ($data['username'] !== clean_param($data['username'], PARAM_USERNAME)) {
                    $errors['username'] = get_string('invalidusername');
                }

            }
        }

			
        //check if user exists in external db
        //TODO: maybe we should check all enabled plugins instead
        if ($authplugin->user_exists($data['username'])) {
            $errors['username'] = get_string('usernameexists');
        }
        return $errors;
    	
    }
    
    /**
     * Checks if user exists on LDAP
     *
     * @param string $username
     */
    function user_exists($username) {
        $textlib = textlib_get_instance();
        $extusername = $textlib->convert($username, 'utf-8', $this->config->ldapencoding);

        // Returns true if given username exists on ldap
        $users = $this->ldap_get_userlist('('.$this->config->user_attribute.'='.ldap_filter_addslashes($extusername).')');
        return count($users);
    }
     /**
     * Sign up a new user ready for confirmation.
     * Password is passed in plaintext.
     *
     * @param object $user new user object
     * @param boolean $notify print notice with link and terminate
     */
    function people_create($user) {

        global $CFG, $DB;
        require_once($CFG->dirroot.'/user/profile/lib.php');

        $user->password = hash_internal_user_password($user->password);
        $roleid = $user->roleid;
        $loginemail = $user->loginemail;
        $user->email = $this->valid_email_address($user->email)?$user->email:$user->username;
        $user->id = $DB->insert_record('user', $user);
				list($context, $course, $cm) = get_context_info_array(1);
				role_assign($roleid, $user->id, $context->id);
        /// Save any custom profile field information
        profile_save_data($user);
      	$user = $DB->get_record('user', array('id'=>$user->id));
        events_trigger('people_created', $user);

   
        if ($loginemail && $this->valid_email_address($user->email)) {
            //Quang removed function send email of moodle
            //$this->send_confirmation_email($user);
            require_once($CFG->dirroot.'/teams/config.php');
            require_once($CFG->dirroot.'/login/encryption.php');
            require_once('lib.php');
            //$user->email = $emailalink->email;
		//if ($password_form_mail->send_confirmation_email($user)) {
                 
            $mailto = $user->email;
            
            $converter = new Encryption;
            $username = $user->username;
            $encoded = $converter->encode($username);
            $link=$CFG->wwwroot."/login/welcome.php?data=".$encoded;
            $link_html="<a href='$link'>".$link."</a>"           ;
            $welcome_content = str_replace("{CONFIRM_LINK_EMAIL}", $link_html, $welcome_content);
            
            sendEmail($mailto, $mailUsername, 'LMS', $welcome_title, $welcome_content);
            
            
            
        }
        
        redirect($CFG->wwwroot."/manage/people.php");
    }
    
		/**
		 * Send email to specified user with confirmation text and activation link.
		 *
		 * @global object
		 * @param user $user A {@link $USER} object
		 * @return bool Returns true if mail was sent OK and false if there was an error.
		 */
		 function send_confirmation_email($user) {
		    global $CFG;
		
		    $site = get_site();
		    $supportuser = generate_email_supportuser();
		
		    $data = new stdClass();
		    $data->firstname = fullname($user);
		    $data->sitename  = format_string($site->fullname);
		    $data->admin     = generate_email_signoff();
		
		    $subject = get_string('emailconfirmationsubject', '', format_string($site->fullname));
		
		    $data->link  = $CFG->wwwroot .'/login/confirm.php?data='. $user->secret .'/'. urlencode($user->username);
		    $message     = get_string('emailconfirmation', '', $data);
		    $messagehtml = text_to_html(get_string('emailconfirmation', '', $data), false, false, true);
		
		    $user->mailformat = 1;  // Always send HTML version as well
		
		    //directly email rather than using the messaging system to ensure its not routed to a popup or jabber
		    return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
		
		}
		
    function valid_email_address($mail) {
    	return (bool) filter_var($mail, FILTER_VALIDATE_EMAIL);
		}
  
}
