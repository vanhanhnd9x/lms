<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');

global $CFG;
require_once($CFG->dirroot . '/common/lib.php');

require_login(0, false);

$PAGE->set_title(get_string('editmyprofile'));
$PAGE->set_heading(get_string('editmyprofile'));
global $USER,$DB;
$user_id = optional_param('user_id', '', PARAM_TEXT);

$action = optional_param('action', '', PARAM_TEXT);
echo $OUTPUT->header();
$user = get_user_from_id($user_id);
$role = people_get_roles($user_id);
$roleid = get_role_assign_from_id($user_id);
// var_dump($roleid('roleid'));



if ($action == 'update_profile') {
    $firstname = optional_param('firstname', '', PARAM_TEXT);
    $lastname = optional_param('lastname', '', PARAM_TEXT);
    $username = optional_param('username', '', PARAM_TEXT);
    $code = optional_param('code', '', PARAM_TEXT);
    $birthday = optional_param('birthday', '', PARAM_TEXT);

    $country = optional_param('country', '', PARAM_TEXT);
    $name_rht = optional_param('rht', '', PARAM_TEXT);
    $name_ac = optional_param('ac', '', PARAM_TEXT);
    $time_start = optional_param('start', '', PARAM_TEXT);
    $time_end = optional_param('end', '', PARAM_TEXT);
    $schoolid = json_encode(optional_param('schoolid', '', PARAM_TEXT));
    $email = $username;
    $phone2 = optional_param('phone2', '', PARAM_TEXT);
    $description = optional_param('description', '', PARAM_TEXT);


    switch ($role_id) {
        case 'admin':
            $role_id = 3;
            break;
        case 'learner':
            $role_id = 5;
            break;
        case 'teacher':
            $role_id = 8;
            break;
        case 'teachertutors':
            $role_id = 10;
            break;
        default:
            break;
    }

    // var_dump($roleid['roleid']);
    
    $new_user_id = update_profile($user_id, $firstname,$lastname,$code,$schoolid,$email,$phone2,$username,$birthday,$country,$name_rht,$name_ac,$time_start,$time_end,$description);
    
    // Thêm ảnh đại diện
    if($_FILES["file"]["name"]!='')
    {
      if ($_FILES["file"]["error"] > 0)
      {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      }
      else
      {
        $fiel_extension=end(explode('.',$_FILES["file"]["name"]));
        if (strtolower($file_extension) == "jpg"||strtolower($file_extension) == "jpeg"
              ||strtolower($file_extension) == "gif"
              ||strtolower($file_extension) == "png") 
        {
                move_uploaded_file($_FILES["file"]["tmp_name"],
                $CFG->dirroot."/account/logo/" . $new_user_id."_".$_FILES["file"]["name"]);
                update_user_logo($new_user_id,$CFG->dirroot."/account/logo/" . $new_user_id."_".$_FILES["file"]["name"]);

        }
        else{ 
          //displayJserror("File type for logo image must be : .jpg, .jpeg, .gif, .png", '');
        }
      }
    }
    foreach ($roleid as $items) {
      if($items->roleid == 8){
        echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/teacher/foreignteacher/index.php");
      }
      if($items->roleid == 10){
        echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/teacher/tutorsteacher/index.php");
      }
    }
    
}


$listschools = $DB->get_records_sql('SELECT * FROM schools');

?>



<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form action="<?php echo $CFG->wwwroot ?>/manage/people/edit.php?action=update_profile" onsubmit="return validate();" method="post">

              <input id="user_id" name="user_id" type="hidden" value="<?php echo $user_id; ?>">
            
            <?php foreach ($roleid as $key => $val): ?>
              
            
              <div class="form-group row">
                  <label class="col-md-2 col-form-label"><?php print_r(get_string('firstname')) ?></label>
                  <div class="col-md-10">
                    <div id="erFirst" class="alert alert-danger" role="alert" style="display: none;">
                      <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button> -->
                        <?php print_string('enter_firstname') ?>
                    </div>
                    <input class="form-control" id="firstname" name="firstname" type="text" value="<?php echo $val->firstname ?>">

                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('lastname')) ?></label>
                <div class="col-md-10">
                  <div id="erLast" class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php print_string('enter_lastname') ?>   
                  </div>
                  <input class="form-control" id="lastname" name="lastname" type="text" value="<?php echo $val->lastname  ?>">
                </div>
              </div>
              
              <?php if($val->roleid == 8): ?>
              <div class="form-group row">
                  <label class="col-md-2 col-form-label">Mã giáo viên</label>
                  <div class="col-md-10">
                    <input class="form-control" id="magv" name="code" type="text" value="<?php echo $val->code  ?>">

                  </div>
                  <div class="form-group row">
                <label class="col-md-2 col-form-label">Quốc tịch</label>
                <div class="col-md-10">
                  <input class="form-control" type="text" name="country" placeholder="Nhập quốc tịch..." value="<?php echo $user->country  ?>">
                </div>
              </div>
              </div>
              <?php endif; ?>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Ảnh đại diện</label>
                <div class="controls col-md-10">
                    <input type="file" class="btn-light" name="file" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('username')) ?></label>
                
                <div class="col-md-10">
                  <div id="erUserName" class="alert alert-danger" style="display: none;"><?php print_string('enter_emailid') ?></div>
                  <input class="form-control" id="username" name="username" type="emailHelp" value="<?php echo $user->username  ?>">
                  <span id="emailHelp" class="form-text text-muted"><?php print_r(get_string('mostpeopleuse')) ?></span>
                </div>
              </div>
              <!-- <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('password')) ?></label>
                
                <div class="col-md-10">
                  <input class="form-control" id="password" name="password" type="password" value="">
                  <span id="erPass" class="text-danger" style="display: none;">Vui lòng nhập mật khẩu</span>
                </div>
              </div> -->

              <div class="form-group row">
                  <label class="col-md-2 col-form-label"><?php print_r(get_string('mobileph')) ?></label>
                  <div class="col-md-10">
                      <input class="form-control" id="phone2" name="phone2" type="number" value="<?php echo $user->phone2  ?>">
                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Ngày sinh</label>
                
                <div class="col-md-10">
                  <div class="input-group">
                    <input class="form-control datepicker" name="birthday" placeholder="dd/mm/yyyy" value="<?php echo $user->birthday  ?>">
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>


              <div class="form-group row">
                <label class="col-md-2 col-form-label">Trường học dạy</label>
                <div class="col-md-10">
                  
                  <select name="schoolid[]" id="schoolid" class="form-control multiple" multiple data-size="7">
                    <?php 
                    $schoool = json_decode($user->schoolid);
                    foreach ($listschools as $val) { ?>
                        <option value="<?php echo $val->id ?>" <?php  foreach ($schoool as $sch) { ?>
                                                                <?php  echo $sch == $val->id ? 'selected' : ''; }?> >
                            <?php echo $val->name ?>
                        </option>
                    <?php }?>
                  </select>
                  
                </div>
              </div>

            <?php if($val->roleid == 8): ?>
              
            <?php endif; ?>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Người quản lý (RHT)</label>
                <div class="col-md-10">
                  <input class="form-control" type="text" name="rht" placeholder="" value="<?php echo $user->name_rht  ?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Người điều phối (AC)</label>
                <div class="col-md-10">
                  <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $user->name_ac ?>">
                </div>
              </div>

            

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Ngày bắt đầu</label>
                  <div class="col-md-10">
                      <input class="form-control datepicker"  name="start" type="text" placeholder="dd/mm/yyyy" value="<?php echo $user->time_start  ?>">
                  </div>
              </div>
              <div class="form-group row">
                <label class="col-md-2 col-form-label">Ngày kết thúc</label>
                  <div class="col-md-10">
                      <input class="form-control datepicker" name="end" type="text" placeholder="dd/mm/yyyy" value="<?php echo $user->time_end  ?>">
                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label">Ghi chú</label>
                  <div class="col-md-10">
                    <textarea name="description" class="form-control"><?php echo $user->description  ?></textarea>
                  </div>
              </div>



              <hr>
              <div class="form-group mb-0 justify-content-end row">
                <div class="col-md-10">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                  <?php print_r(get_string('or')) ?> 
                  <a href="<?php echo $CFG->wwwroot . '/manage/teacher/foreingteacher/index.php' ?>" class="btn btn-danger"> 
                    <?php print_r(get_string('cancel')) ?>
                  </a>
                </div>
              </div>
              <?php endforeach ?>
        </form>   
      </div>
  </div>
</div>

<script type="text/javascript" src="js/validate_profile.js"></script>
<?php
    echo $OUTPUT->footer();
?>

