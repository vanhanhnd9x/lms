<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_login(0, false);
$PAGE->set_title(get_string('newprofile'));
$PAGE->set_heading(get_string('newprofile'));
global $USER;

echo $OUTPUT->header();

$role_id = optional_param('role_id', '', PARAM_TEXT);
$action = optional_param('action', '', PARAM_TEXT);
$user = get_role_from_id($role_id);

if ($action == 'add_profile') {
    $firstname = optional_param('firstname', '', PARAM_TEXT);
    $lastname = optional_param('lastname', '', PARAM_TEXT);
    $username = optional_param('username', '', PARAM_TEXT);
    $code = optional_param('code', '', PARAM_TEXT);
    // $birthday = optional_param('birthday', '', PARAM_TEXT);
    $password = optional_param('password', '', PARAM_TEXT);
    $area = optional_param('area', '', PARAM_TEXT);
    $country = optional_param('country', '', PARAM_TEXT);
    $name_rht = optional_param('rht', '', PARAM_TEXT);
    $name_ac = optional_param('ac', '', PARAM_TEXT);
    $time_start = optional_param('start', '', PARAM_TEXT);
    $time_end = optional_param('end', '', PARAM_TEXT);
    $schoolid = json_encode(optional_param('schoolid', '', PARAM_TEXT));
    $email = optional_param('email', '', PARAM_TEXT);
    // $avatar = optional_param('avatar', '', PARAM_TEXT);
    $profile_img = upload_img('avatar', $CFG->dirroot.'/account/logo/');
    $phone2 = optional_param('phone', '', PARAM_TEXT);
    $description = optional_param('description', '', PARAM_TEXT);
    $role_id = optional_param('access_level', '', PARAM_TEXT);
    $parentId = 0;
    switch ($role_id) {
        case 'admin':
            $role_id = 3;
            break;
        case 'learner':
            $role_id = 5;
            break;
        case 'teacher':
            $role_id = 8;
            break;
        case 'teachertutors':
            $role_id = 10;
            break;
        case 'acrht':
            $role_id = 11;
            break;
        case 'taacrhta':
            $role_id = 12;
            break;
        default:
            break;
    }
//    If is superadmin create admin level 1
    if ($USER->id == 2 && $role_id == 3) {
      $parentId = $USER->id;
    }


      $new_user_id = add_profile($firstname,$lastname,$code,$schoolid,$email,$profile_img,$phone2,$username,$password,$area,$country,$name_rht,$name_ac,$time_start,$time_end,$description,$parentId);
      $context = get_context_instance(CONTEXT_SYSTEM);
      role_assign($role_id, $new_user_id, $context->id);
      // $newPass = update_user_password($new_user_id);
      //    Add to map user
      $new_user_map_owner = add_map_user_admin($USER->id, $new_user_id);
      

      if($role_id==8){
        echo displayJsAlert('Thêm mới thành công', $CFG->wwwroot . "/manage/teacher/foreignteacher/index.php");
      }
      if($role_id==10){
        echo displayJsAlert('Thêm mới thành công', $CFG->wwwroot . "/manage/teacher/tutorsteacher/index.php");
      }

    // redirect($CFG->wwwroot . "/manage/people/profile.php?id=" . $new_user_id);
}


$schools = $DB->get_records_sql('SELECT * FROM schools WHERE del=0');
?>

<script type="text/javascript" src="js/validate_profile.js"></script>
<div class="row">
  <div class="col-md-12">
      <div class="card-box">
          <form action="<?php echo $CFG->wwwroot ?>/manage/people/new.php?action=add_profile" id="add_profile" onsubmit="return validate();" method="post" enctype="multipart/form-data">
            <div class="row">
              <input id="user_id" name="user_id" type="hidden" value="<?php echo $user_id; ?>" class="input-group-sm"> 

              <div class="col-md-6 form-group">
                  <label class="col-form-label"><?php print_r(get_string('firstname')) ?> <span class="text-danger">*</span></label>    
                  <input class="form-control" id="firstname" name="firstname" type="text" value="<?php echo $firstname ?>">
                  <div id="erFirst" class="text-danger" role="alert" style="display: none;"><?php print_string('enter_firstname') ?>
                    </div>
              </div>

              <div class="col-md-6 form-group">
                <label class="col-form-label"><?php print_r(get_string('lastname')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="lastname" name="lastname" type="text" value="<?php echo $lastname ?>">
                  <span id="erLast" class="text-danger" style="display: none;"><?php print_string('enter_lastname') ?> </span>
              </div>
              <?php if($role_id==8){ ?>
              <div class="col-md-6 form-group">
                  <label class="col-form-label"><?php print_r(get_string('codeteacher')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="magv" name="code" type="text" value="<?php echo $code ?>">
                  <span id="erMaGV" class="text-danger" style="display: none;">Vui lòng nhập mã giáo viên </span>
                  <span id="er_t" class="text-danger"></span>
              </div>
            <?php } ?>
            
            <?php if($role_id==10): ?>
            <div class="form-group row hiden">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('accesslevel')) ?></label>
                <div class="col-md-10">
                  <select name="access_level" id="access_level" class="form-control">
                    <option value="teachertutors">Giáo viên trợ giảng</option>
                  </select>
                </div>
              </div>
            <?php endif; ?>
            <?php if($role_id==8): ?>
            <div class="form-group row hiden">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('accesslevel')) ?></label>
                <div class="col-md-10">
                  <select name="access_level" id="access_level" class="form-control">
                    <option value="teacher">Giáo viên nước ngoài</option>
                  </select>
                </div>
              </div>
            <?php endif; ?>
            <?php if($role_id==11): ?>
            <div class="form-group row hiden">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('accesslevel')) ?></label>
                <div class="col-md-10">
                  <select name="access_level" id="access_level" class="form-control">
                    <option value="acrht">AC/RHT</option>
                  </select>
                </div>
              </div>
            <?php endif; ?>
            <?php if($role_id==12): ?>
            <div class="form-group row hiden">
                <label class="col-md-2 col-form-label"><?php print_r(get_string('accesslevel')) ?></label>
                <div class="col-md-10">
                  <select name="access_level" id="access_level" class="form-control">
                    <option value="taacrhta">TAAC/RHTA</option>
                  </select>
                </div>
              </div>
            <?php endif; ?>

            
              <div class="col-md-6 form-group">
                <label class="col-form-label"><?php print_r(get_string('yourlogo')) ?></label><br>
                  <input type="file" class="btn-light" id="file" name="avatar" />
                  <div id="er_img"></div>
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('username')) ?> <span class="text-danger">*</span></label>
                
                  <div id="erUserName" class="alert alert-danger" style="display: none;"><?php print_string('enter_emailid') ?></div>
                  <input class="form-control" id="username" name="username" type="text" value="<?php echo $username ?>" required>
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('password')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="password" name="password" type="password" value="">
                  <span id="erPass" class="text-danger" style="display: none;">Vui lòng nhập mật khẩu</span>
              </div>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('email')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="email" name="email" type="text" value="<?php echo $email ?>">
                  <div id="erEmail" class="text-danger" style="display: none;"><?php print_string('enter_emailid') ?></div>
              </div>
              

              <div class="form-group col-md-6">
                  <label class="col-form-label"><?php print_r(get_string('mobileph')) ?> <span class="text-danger">*</span></label>
                  <input class="form-control" id="phone" name="phone" type="number" value="<?php echo $phone2 ?>">
                  <span id="er_phone" class="text-danger"></span>
              </div>
              
            
              <!-- <div class="form-group col-md-6">
                <label class="col-form-label">Ngày sinh</label>
                  <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                    <input type="text" name="birthday" id="birthday" class="form-control" placeholder="dd/mm/yyyy" value="<?php echo $birthday ?>">
                  </div>
              </div> -->
            


            <?php if($role_id==8 || $role_id==10) {?>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('schools')) ?></label>
                  <select name="schoolid[]" id="schoolid" class="form-control multiple" multiple>
                   <!-- <option value="-1">Chọn trường</option> -->
                    <?php 
                     foreach ($schools as $key => $value) {
                            # code...
                      ?>
                      <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                      <?php 
                    }
                    ?>
                  </select>
              </div>
            <?php } ?>

            <?php if($role_id==11) {?>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('area')) ?></label>
                  <select name="area" id="area" class="form-control ">
                    <option value="">--Chọn khu vực quản lý--</option>
                    <option value="1">34T Hoàng Đạo Thúy</option>
                    <option value="2">62 Yên Phụ</option>
                    <option value="3">2A Đại Cồ Việt</option>
                    <option value="4">Marie curie</option>
                  </select>
              </div>
            <?php } ?>
            
            <?php if($role_id == 8 || $role_id == 11) {?>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('nationality')) ?></label>
                <input class="form-control" type="text" name="country" placeholder="<?php print_r(get_string('nationality')) ?>..." value="<?php echo $country ?>">
              </div>
            <?php } ?>
            
            <?php if($role_id == 11) {?>
            <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('coordinator')) ?> (AC)</label>
                <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $name_ac ?>">
              </div>
            <?php }?>

            <?php if($role_id == 12) {?>
            <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('coordinator')) ?> (TAAC)</label>
                  <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $name_ac ?>">
              </div>
            <?php }?>

            <?php if($role_id == 8) {?>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('manager')) ?> (RHT)</label>
                  <?php $acrht = get_list_acrht(); ?>
                  <select name="rht" id="rht" class="form-control ">
                    <option value="">--Chọn người quản lý--</option>
                    <?php foreach ($acrht as $rht) { ?>
                        <option value="<?php echo $rht->id ?>"><?php echo $rht->firstname.' '.$rht->lastname ?></option>
                    <?php } ?>
                  </select>
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('coordinator')) ?> (AC)</label>
                  <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $name_ac ?>">
              </div>
            <?php }?>


              <?php if($role_id==10) {?>
              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('manager')) ?> (RHTA)</label>
                  <?php $taacrhta = get_list_taacrhta(); ?>
                  <select name="rht" id="rht" class="form-control ">
                    <option value="">--Chọn người quản lý--</option>
                    <?php foreach ($taacrhta as $rhta) { ?>
                        <option value="<?php echo $rhta->id ?>"><?php echo $rhta->firstname.' '.$rhta->lastname ?></option>
                    <?php } ?>
                  </select>
              </div>

              <div class="form-group col-md-6">
                <label class="col-form-label"><?php print_r(get_string('coordinator')) ?> (TAAC)</label>
              
                <input class="form-control" type="text" name="ac" placeholder="" value="<?php echo $name_ac ?>">
              </div>
            <?php } ?>

            
            <?php if($role_id==8 || $role_id==10) {?>
              <div class="col-md-12">
                <div class="row">
                  <div class="form-group col-md-6">
                    <label class="col-form-label"><?php print_r(get_string('startdate')) ?></label>
                          <input class="form-control"  name="start" id="date_start" type="text" placeholder="dd/mm/yyyy" value="<?php echo $time_start ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label class="col-form-label"><?php print_r(get_string('endate')) ?></label>
                    <input class="form-control" id="date_end"  name="end" type="text" placeholder="dd/mm/yyyy" value="<?php echo $time_end ?>">
                  </div>
                </div>
              </div>
            <?php } ?>

              <div class="form-group col-md-12">
                <label class="col-form-label"><?php print_r(get_string('description')) ?></label>
                <textarea name="description" class="form-control"></textarea>
              </div>



              <hr>
              <div class="form-group">
                <div class="col-md-12">
                  <input type="submit" value="<?php print_r(get_string('save', 'admin')) ?>" class="btn btn-success" id="saveUser">
                  <?php print_r(get_string('or')) ?>
                  <?php if($role_id==8): ?> 
                    <a href="<?php echo $CFG->wwwroot . '/manage/teacher/foreignteacher/index.php' ?>" class="btn btn-danger"> 
                      <?php print_r(get_string('cancel')) ?>
                    </a>
                  <?php endif; ?>
                  <?php if($role_id==10): ?> 
                    <a href="<?php echo $CFG->wwwroot . '/manage/teacher/tutorsteacher/index.php' ?>" class="btn btn-danger"> 
                      <?php print_r(get_string('cancel')) ?>
                    </a>
                  <?php endif; ?>
                  <?php if($role_id==11): ?> 
                    <a href="<?php echo $CFG->wwwroot . '/manage/teacher/managerteacher/rht.php' ?>" class="btn btn-danger"> 
                      <?php print_r(get_string('cancel')) ?>
                    </a>
                  <?php endif; ?>
                  <?php if($role_id==12): ?> 
                    <a href="<?php echo $CFG->wwwroot . '/manage/teacher/managerteacher/rhta.php' ?>" class="btn btn-danger"> 
                      <?php print_r(get_string('cancel')) ?>
                    </a>
                  <?php endif; ?>
                </div>
              </div>
            </div>
        </form> 
      </div>
  </div>
</div>

<?php
    echo $OUTPUT->footer();
?>
<script type="text/javascript">
$('#file').change(function(event) {
    var er=0;
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      // lay dung luong va kieu file tu the input file
        if($('#file').get(0).files.length==0){
          $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Chọn ảnh đại diện</div>');
          er=1;
        }else{
          var fname = $('#file')[0].files[0].name;
          var fsize = $('#file')[0].files[0].size;
          var ftype = $('#file')[0].files[0].type;

          if (ftype=='image/png' || ftype=='image/gif' || ftype=='image/jpeg' || ftype=='image/pjpeg') {
            
            if (fsize > <?php echo get_real_size(ini_get('upload_max_filesize')) ?>) {
              
              $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Dung lượng file < <?php echo ini_get('upload_max_filesize') ?></div>');
              er=1;
              
            }else{
              $("#er_img").html('<div class="alert alert-success" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>File hợp lệ</div>');
              er=0;
            }
          }else{
            $("#er_img").html('<div class="alert alert-danger" id="error"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Không đúng định dạng ảnh</div>');
            er=1;
          }

        }
         
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    }

    if(er==1){
      $("#saveUser").attr('disabled','disabled');
    }else{
      $("#saveUser").removeAttr('disabled');
    }
});
</script>
<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/base/jquery-ui.css'>
<script src='//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script type="text/javascript">
      $("#date_start").datepicker({
        minDate: new Date(25, 10 - 1,2000 ),
        maxDate: "",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_end").datepicker("option","minDate", selected)
        },
         disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
      $("#date_end").datepicker({
        minDate: 0,
        maxDate:"",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#date_start").datepicker("option","maxDate", selected)
        },
         disableTouchKeyboard: true,
        Readonly: true
      }).attr("readonly", "readonly");
      jQuery(function($){
        $.datepicker.regional['vi'] = {
          closeText: 'Đóng',
          prevText: '<Trước',
          nextText: 'Tiếp>',
          currentText: 'Hôm nay',
          monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
          'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
          monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
          'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
          dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
          dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
          dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
          weekHeader: 'Tu',
          dateFormat: 'dd/mm/yy',
          firstDay: 0,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''};
          $.datepicker.setDefaults($.datepicker.regional['vi']);
        });
</script>
<script type="text/javascript">
  $("#magv").keyup(function (e) {
    var magv = $(this).val();
    console.log(magv);
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/people/test.php?magv="+magv,function(data){
            $("#er_t").html(data);
      });
    } 
  });
</script>
