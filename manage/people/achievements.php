<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */
require_once('../../config.php');
require_once('../lib.php');
require_once('lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_login(0, false);
$strmessages = '';
$PAGE->set_title(get_string('people'));
$PAGE->set_heading(get_string('people'));
//now the page contents
//$PAGE->set_pagelayout('courses');
echo $OUTPUT->header();

$userid = optional_param('id', 0, PARAM_INT);
$user = $DB->get_record('user', array('id' => $userid));

$path->achievements = $userid ? '/manage/people/achievements.php?id=' . $userid : '/manage';
$path->recent = $userid ? '/manage/people/profile.php?id=' . $userid : '/manage';
$path->courses = $userid ? '/manage/people/courses.php?id=' . $userid : '/manage';
$path->teams = $userid ? '/manage/people/teams.php?id=' . $userid : '/manage';

$courses = get_achievements($userid);
?>
<?php
if (is_teacher() && !is_admin()) {
    ?>
    <div id="admin-fullscreen">

        <div id="admin-fullscreen-left" class="focus-panel"> 
            <!-- Left Content -->
            <div class="body">
                <div class="panel-head clearfix">
                    <div class="media-box">
                        <div class="img"><?php echo $OUTPUT->user_picture($user, array('size' => 100)); ?></div>
                        <div class="blurb">
                            <div class="float-right">
                              <div class="subtitle italic"><?php print_r(get_string('lastloginwas'))." "; print time_ago($user->lastlogin); ?></div>
                                <div class="align-right">
                                    <span class="box-tag <?php echo ($user->confirmed ? 'box-tag-green' : 'box-tag-grey') ?> " title=""><span><?php echo ($user->confirmed ? get_string('active') : get_string('inactive')) ?></span></span>
                                    <span class="box-tag box-tag-orange " title=""><span><?php print people_get_roles($user->id); ?></span></span>
                                </div>
                            </div>
                            <h3><?php print obfuscate_text(fullname($user, true), ''); ?></h3>
                            <div class="subtitle">
                                <?php print obfuscate_mailto($user->email, ''); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='wrapper-inner'>
                    <div class="grid-tabs">

                        <ul>
                            <li><a href="<?php print new moodle_url($path->recent); ?>"><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                            <li><a href="<?php print new moodle_url($path->achievements); ?>" class="selected"><?php print_r(get_string('achievements')) ?></a></li> 
                            <li><a href="<?php print new moodle_url($path->courses); ?>"><?php print_string('course') ?></a></li>
                            <li><a href="<?php print new moodle_url($path->teams); ?>"><?php print_string('teams') ?></a></li>         
                        </ul>
                        <div class="clearfix"></div>


                    </div>

                    <div class="item-page-list">
                        <table class="grid">
                            <tbody>
                                <?php foreach ($courses as $course) { ?>
                                    <tr>

                                        <td class="main-col">
                                            <div class="title"><?php echo $course->fullname; ?></div>
                                            <div class="tip">
                                                Achieved on {achieveddate} 
                                                | <span class="due-date">Compliant until {compliantdate}</span>

                                            </div>
                                        </td>
                                        <td align="right" class="nowrap">    

                                        </td>
                                    </tr>

                                <?php } ?>
                            </tbody></table>
                    </div>
                </div>
            </div> 
        </div><!-- Left Content -->
        <!-- Right Block -->
        <div id="admin-fullscreen-right">

            <div class="side-panel">
                <div class="side-panel">
                </div>
            </div>
        </div>  <!-- End Right -->

    </div>
    <?php
} else {
    ?>
    <div id="admin-fullscreen">

        <div id="admin-fullscreen-left" class="focus-panel"> 
            <!-- Left Content -->
            <div class="body">
                <div class="panel-head clearfix">
                    <div class="media-box">
                        <div class="img"><?php echo $OUTPUT->user_picture($user, array('size' => 100)); ?></div>
                        <div class="blurb">
                            <div class="float-right">
                                <div class="subtitle italic"><?php print_r(get_string('lastloginwas'))." ";print time_ago($user->lastlogin); ?></div>
                                <div class="align-right">
                                  <span class="box-tag <?php echo ($user->confirmed ? 'box-tag-green' : 'box-tag-grey') ?> " title=""><span><?php echo ($user->confirmed ? get_string('active') : get_string('inactive')) ?></span></span>
                                    <span class="box-tag box-tag-orange " title=""><span><?php print people_get_roles($user->id); ?></span></span>
                                </div>
                            </div>
                            <h3><?php print obfuscate_text(fullname($user, true), ''); ?></h3>
                            <div class="subtitle">
                                <?php print obfuscate_mailto($user->email, ''); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='wrapper-inner'>
                    <div class="grid-tabs">

                        <ul>
                            <li><a href="<?php print new moodle_url($path->recent); ?>"><?php print_r(get_string('pluginname','block_recent_activity'))?></a></li>
                            <li><a href="<?php print new moodle_url($path->achievements); ?>" class="selected"><?php print_r(get_string('achievements')) ?></a></li> 
                            <li><a href="<?php print new moodle_url($path->courses); ?>"><?php print_string('course') ?></a></li>
                            <li><a href="<?php print new moodle_url($path->teams); ?>"><?php print_string('teams') ?></a></li>         
                        </ul>
                        <div class="clearfix"></div>


                    </div>

                    <div class="item-page-list">
                        <table class="grid">
                            <tbody>
                                <?php foreach ($courses as $course) { ?>
                                    <tr>

                                        <td class="main-col">
                                            <div class="title"><?php echo $course->fullname; ?></div>
                                            <div class="tip">
                                                Achieved on {achieveddate} 
                                                | <span class="due-date">Compliant until {compliantdate}</span>

                                            </div>
                                        </td>
                                        <td align="right" class="nowrap">    

                                        </td>
                                    </tr>

                                <?php } ?>
                            </tbody></table>
                    </div>
                </div>
            </div> 
        </div><!-- Left Content -->
        <!-- Right Block -->
        <div id="admin-fullscreen-right">

            <div class="side-panel">
                <div class="side-panel">
                </div>
            </div>
        </div>  <!-- End Right -->

    </div>

    <?php
}
echo $OUTPUT->footer();
?>