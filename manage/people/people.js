/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function($){
$(document).ready(function() {
   
    var validate_username= function(self){
			//check the username exists or not from ajax
			$.post("/manage/people/availability.php",{username:self.val(),id:$("[name='id']").val(),name:'username'} ,function(data){
			  self.parents('#mform1').removeClass('error');
              self.parents('.felement.ftext').find('span.error').remove();
			  if(data.username == true) {
				  //add message and change the class of the box and start fading
				    if(data.email == true)$('#id_email').val(self.val());
				    self.parents('.error.felement').removeClass('error');
	          }else{
	            self.parents('.felement.ftext').addClass('error');
			  	self.after('<span class="title error">'+data.username+'</span>');
			  	self.parents('#mform1').addClass('error');
			  }	
	    },'JSON');

	  };
	  var validate_firstname = function(self){
	       self.parents('.felement.ftext').find('span.error').remove();
           if(self.val() ==''){
	            self.parents('.felement.ftext').addClass('error');
                self.after('<span class="title error">First name required</span>');
	       }else{
	            self.parents('.felement.ftext').removeClass('error');
                self.parents('#mform1').removeClass('error');
	       }
	  }
	  
    $(".felement.ftext [name='username']")
        .keyup(function () {validate_username($(this));})
          .blur(function(){validate_username($(this));});
          
    $(".felement.ftext [name='firstname']")
        .keyup(function () {validate_firstname($(this));})
          .blur(function(){validate_firstname($(this));});
    
    $('#mform1').submit(function() {
      if($(this).hasClass('error')){
        $('#id_username.error').focus();
        $('#id_firstname.error').focus();
        return false;
      }
    });

 
    

});
})(jQuery);
