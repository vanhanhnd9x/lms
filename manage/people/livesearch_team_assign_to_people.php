<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!file_exists('../../config.php')) {
    header('Location: ../../install.php');
    die;
}

require('../../config.php');
global $CFG;
require_once('lib.php');
require_once('../lib.php');
// Require Login.
require_login(0, false);
$user_id = optional_param('user_id', 0, PARAM_INT);
$q = optional_param('q', '', PARAM_TEXT);

$teams = search_team_from_name($q);
$list_teams_belong_to_selected_user = get_teams_belong_to_user_id($user_id);

$list_teams_belong_to_selected_user_arr_id = array();
foreach ($list_teams_belong_to_selected_user as $key => $val) {
    $list_teams_belong_to_selected_user_arr_id[] = $val->groupid;
}
//var_dump($list_teams_belong_to_selected_user_arr_id);
?>
<div>
    <table cellspacing="0" class="item-page-list">

        <tbody>


            <?php
            foreach ($teams as $key => $val) {
                ?>
                <tr>
                    <?php if (!in_array($val->id, $list_teams_belong_to_selected_user_arr_id)) { ?>

                        <td>
                            <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">

                        </td>

                    <?php } else { ?>
                        <td>
                            &nbsp;

                        </td>



                    <?php } ?>

                    <td class="main-col">
                        <label for="u134670">
                            <span class="title">
                                <?php echo $val->name ?></span>                                  

                        </label>
                    </td>
                    <?php if (in_array($val->id, $list_teams_belong_to_selected_user_arr_id)) { ?>

                        <td class="nowrap">&nbsp;<span title="" class="box-tag box-tag-green "><span><?php print_r(get_string('alreadyassigned')) ?></span></span>                
                        </td> 
                    <?php } ?>

                </tr>
                <?php
            }
            ?>


        </tbody></table>
</div>