<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot . '/lib/formslib.php');

class password_form_mail extends moodleform {

    // Define the form
    function definition() {
        global $CFG, $COURSE;
        $mform = & $this->_form;

        $mform->addElement(
                'html', '<div class="form-group mr_tb15">' .
                '<div class="card card-header">' . get_string('emaillink') . '</div>'
        );
        $mform->addElement('text', 'email', get_string('mailto'), 'maxlength="100" size="25" class="form-control" ');

        $mform->addElement('html', '</div>');

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('sendmail'),'class="btn btn-success"');
        $buttonarray[] = &$mform->createElement('cancel','','','class="btn btn-danger"');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');

        // $mform->addElement('hidden', 'id');
        $mform->setType('user', PARAM_INT);
    }

    function validation($usernew, $files) {
        global $CFG, $DB;

        $errors = parent::validation($usernew, $files);
        $usernew = (object) $usernew;
        if (!$this->ValidateAddress($usernew->email)) {
            $errors['email'] = get_string('invalidemail');
        }
        return $errors;
    }

    /**
     * Check that a string looks roughly like an email address should
     * Static so it can be used without instantiation
     * Tries to use PHP built-in validator in the filter extension (from PHP 5.2), falls back to a reasonably competent regex validator
     * Conforms approximately to RFC2822
     * @link http://www.hexillion.com/samples/#Regex Original pattern found here
     * @param string $address The email address to check
     * @return boolean
     * @static
     * @access public
     */
    function ValidateAddress($address) {
        if (function_exists('filter_var')) { //Introduced in PHP 5.2
            if (filter_var($address, FILTER_VALIDATE_EMAIL) === FALSE) {
                return false;
            } else {
                return true;
            }
        } else {
            return preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $address);
        }
    }

    /**
     * Send email to specified user with confirmation text and activation link.
     *
     * @global object
     * @param user $user A {@link $USER} object
     * @return bool Returns true if mail was sent OK and false if there was an error.
     */
    function send_confirmation_email($user) {

        global $CFG;

        $site = get_site();
        $supportuser = generate_email_supportuser();

        $data = new stdClass();

        $data->firstname = fullname($user);
        $data->sitename = format_string($site->fullname);
        $data->admin = generate_email_signoff();
        $subject = get_string('emailconfirmationsubject', '', format_string($site->fullname));

        $data->link = $CFG->wwwroot . '/login/confirm.php?data=' . $user->secret . '/' . urlencode($user->username);

        $message = get_string('emailconfirmation', '', $data);
        $messagehtml = text_to_html(get_string('emailconfirmation', '', $data), false, false, true);

        //directly email rather than using the messaging system to ensure its not routed to a popup or jabber
        return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
    }

}

class password_form_set extends moodleform {

    // Define the form
    function definition() {
        global $CFG, $COURSE;
        $mform = & $this->_form;

        $mform->addElement(
                'html', '<div class="form-group mr_tb15">' .
                '<div class="card card-header">' . get_string('orsetanewpass') . '</div>'
        );
        $mform->addElement('text', 'newpassword', get_string('password'), 'maxlength="100" size="25" class="form-control"');
        $mform->addElement('text', 'newpasswordconfirm', get_string('passwordconfirmchange'), 'maxlength="100" size="25" class="form-control"');

        $mform->addElement('html', '</div>');

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('passwordsave'),'class="btn btn-success"');
        $buttonarray[] = &$mform->createElement('cancel','','','class="btn btn-danger"');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');

        // $mform->addElement('hidden', 'id');
        $mform->setType('user', PARAM_INT);
    }

    function validation($usernew, $files) {
        global $CFG, $DB;

        $errors = parent::validation($usernew, $files);
        $usernew = (object) $usernew;
        if ($usernew->newpassword != $usernew->newpasswordconfirm) {
            $errors['newpassword'] = get_string('passwordconfirmation');
        } else {
            if (!empty($usernew->newpassword)) {
                $errmsg = ''; //prevent eclipse warning
                if (!check_password_policy($usernew->newpassword, $errmsg)) {
                    $errors['newpassword'] = $errmsg;
                }
            }
        }
        return $errors;
    }

    /**
     * Sign up a new user ready for confirmation.
     * Password is passed in plaintext.
     *
     * @param object $user new user object
     * @param boolean $notify print notice with link and terminate
     */
    function setfields(&$userset) {

        global $CFG, $DB;
        require_once($CFG->dirroot . '/user/profile/lib.php');
        if (!empty($userset->newpassword)) {
            $userset->password = hash_internal_user_password($user->newpassword);
        }
    }

}
