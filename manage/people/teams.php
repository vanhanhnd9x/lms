<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../../install.php');
    die;
}

require('../../config.php');
global $CFG;
require_once('lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once('../lib.php');

// Require Login.
require_login(0, false);

$PAGE->set_title(get_string('teams'));
$PAGE->set_heading('teams');

//now the page contents
// $PAGE->set_pagelayout('teams');
echo $OUTPUT->header();

$user_id = optional_param('id', 0, PARAM_INT);
$team_id = optional_param('team_id', 0, PARAM_INT);
$action = optional_param('action', 0, PARAM_TEXT);
$confirm = optional_param("confirm", '', PARAM_TEXT);
$to_assign = optional_param('to_assign', 0, PARAM_TEXT);

$user = get_user_from_id($user_id);

if ($action == 'assign_team_to_user') {
    foreach ($to_assign as $key => $val) {
        insert_member_to_group($user_id, $val);
        $team = get_team_from_id($val);
        if ($send_email_notification == 1) {
            $member = get_user_from_id($user_id);
            $mailto = $member->email;
            $username = $member->username;

            $invitationContenNoEmail = $team_update_content;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{TEAM_NAME}", $team->name, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            sendEmail($mailto, $mailUsername, 'LMS', $team_update_title, $invitationContenNoEmail);
            $invitationContenNoEmail = $team_update_content;
        }
    }
    //echo displayJsAlert('assign team to user succesfully', $CFG->wwwroot."/manage/people/teams.php?id=".$user_id);
}
if ($action == 'delete_team_member' && $confirm == 'yes') {
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    remove_team_member($user_id, $team_id);
    //echo displayJsAlert('remove team succesfully', $CFG->wwwroot."/manage/people/teams.php?id=".$user_id);
    echo displayJsAlert('', $CFG->wwwroot . "/manage/people/teams.php?id=" . $user_id);
}
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        
    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        <div id="courseList" class="focus-panel">
                            <div class="panel-head clearfix">
                                <div class="media-box">
                                    <div class="blurb">
                                        <div class="float-right">
                                            <div class="subtitle italic">
                                                <?php echo time_ago($user->lastlogin); ?>
                                            </div>
                                        </div>
                                        <h3><?php echo $user->username ?></h3>
                                        <div class="subtitle"><?php echo $user->firstname ?> <?php echo $user->lastname ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="body">
                                <div class="grid-tabs">
                                    <ul>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/manage/people/profile.php?id=<?php echo $user_id ?>" class=""><?php print_r(get_string('pluginname', 'block_recent_activity')) ?></a></li>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/manage/people/achievements.php?id=<?php echo $user_id ?>" class=""><?php print_r(get_string('achievements')) ?></a></li>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/manage/people/courses.php?id=<?php echo $user_id ?>" class=""><?php print_r(get_string('courses')) ?></a></li>
                                        <li><a href="<?php echo $CFG->wwwroot ?>/manage/people/teams.php?id=<?php echo $user_id ?>" class="selected"><?php print_r(get_string('teams')) ?></a></li>  
                                    </ul>
                                    <div class="clear">
                                    </div>
                                </div>  
                                <table class="item-page-list">
                                    <tbody>
                                        <?php
                                        $list_teams = get_teams_belong_to_user_id($user_id);
                                        foreach ($list_teams as $key => $val) {
                                            $team = get_team_from_id($val->groupid);
                                            ?>
                                            <tr id="t7675">
                                                <td class="main-col"> 
                                                    <div class="tip"></div>                                               
                                                    <div class="title"><a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $val->groupid ?>"><?php echo $team->name ?></a></div>
                                                </td>
                                                <td class="nowrap">
                                                </td>
                                                <td>                           
                                                    <a href="#" onclick="javascript:displayConfirmBox('<?php echo $CFG->wwwroot ?>/manage/people/teams.php?id=<?php echo $user_id ?>&action=delete_team_member&team_id=<?php echo $val->groupid ?>&confirm=yes', 33, '<?php print_r(get_string('areyousureremove')) ?>')" title="Delete" class="remove-msg btn-remove-ms float-right"></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody></table>
                                <div class="list-pager">
                                    <ul>
                                    </ul>
                                    <div class="clear"></div>    
                                </div>
                                <?php
                                $team_str = get_string('team');
                                if (count($list_teams) > 1) {
                                    $team_str = get_string('teams');
                                }
                                ?>
                                <div class="tip center-padded"><?php echo count($list_teams) ?>  <?php echo $team_str ?></div>                           
                            </div>
                        </div>
                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        <div class="side-panel">
                            <div class="action-buttons">
                                <ul>
                                    <li><a alt="Assign to teams" onclick="" href="#" class="big-button drop" id="assignTeam"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print_r(get_string('assigntoteams')) ?></span></span></span></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody></table>
    <div class="overlay" id="overlay" style="display:none;"></div>
    <div id="facebox" style="display: none;"> 
        <div class="popup">         
            <div class="content">
                <div id="assignItemBox">
                    <div class="assign-header">
                        <div class="assign-search">
                            <form action="" method="get">
                                <div class="field-help-box">
                                    <label id="searchPrompt" for="assignSearch"><?php print_r(get_string('searchfortoteam')) ?></label>
                                    <input type="text" autocapitialize="off" autocorrect="off" autocomplete="off" name="s" id="assignSearch">
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="<?php echo $CFG->wwwroot ?>/manage/people/teams.php?id=<?php echo $user_id ?>" method="post">
                        <input type="hidden" name="action" value="assign_team_to_user"/>
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>"/>
                        <div id="assignResults">
                            <div>
                                <table cellspacing="0" class="item-page-list">
                                    <tbody>
                                        <?php
                                        $list_teams = get_teams_belong_to_user_id($user_id);
                                        foreach ($list_teams as $key => $val) {
                                            $team = get_team_from_id($val->groupid);
                                            ?>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="main-col">
                                                    <label for="u134670">
                                                        <span class="title">
                                                            <?php echo $team->name ?></span>                                  
                                                    </label>
                                                </td>
                                                <td class="nowrap">&nbsp;<span title="" class="box-tag box-tag-green "><span><?php print_r(get_string('alreadyassigned')) ?></span></span>                
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        $list_teams_not_belong_to_user_id = get_teams_not_belong_to_user_id($user_id);
                                        foreach ($list_teams_not_belong_to_user_id as $key => $val) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                                                </td>
                                                <td class="main-col">
                                                    <label for="u135453">
                                                        <span class="title">
                                                            <?php echo $val->name ?>
                                                        </span>                                  
                                                    </label>
                                                </td>
                                                <td class="nowrap">&nbsp;
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody></table>
                            </div>
                        </div>
                        <div class="assign-footer-opt clearfix hidden">
                            <span id="assignTeams" style="display: none;"><input type="checkbox" name="SubTeams" id="SubTeams" value="true"> <label for="SubTeams"><?php print_string('alsoassignto') ?>&nbsp;&nbsp;</label></span>
                            <span id="assignEmails"><input type="checkbox" name="SendEmail" id="SendEmail" checked> <label for="SendEmail"><?php print_r(get_string('sendemailnoti')) ?></label></span>
                            <input type="hidden" name="send_email_notification" id="send_email_notification" value="1"/>
                        </div>
                        <div class="assign-footer clearfix">
                            <div class="float-left">
                                <input type="submit" class="big-button drop" value="<?php print_r(get_string('assign', 'cohort')) ?>"/><?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

