<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */
// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
require_once('lib.php');
require_once('password_form.php');
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/dashboard.php'));
    $PAGE->set_button($buttons);
}

$page = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage = optional_param('perpage', '30', PARAM_INT); // how many per page
$user_id = optional_param('user_id', 0, PARAM_INT);
if ($user_id == 0) {
    $user_id = optional_param('id', 0, PARAM_INT);
}
//$user_id = (is_numeric($user_id) && $user_id > 0) ? $user_id : $USER->id;
$user = $DB->get_record('user', array('id' => $user_id));

$path->achievements = $user_id ? '/manage/people/achievements.php?id=' . $user_id : '/manage';
$path->recent = $user_id ? '/manage/people/profile.php?id=' . $user_id : '/manage';
$path->courses = $user_id ? '/manage/people/courses.php?id=' . $user_id : '/manage';
$path->teams = $user_id ? '/manage/people/teams.php?id=' . $user_id : '/manage';

$password_form_mail = new password_form_mail();
$password_form_set = new password_form_set();

$password_form_mail->set_data($user);
$password_form_set->set_data($user);


$_qf__password_form_set = optional_param('_qf__password_form_set', 0, PARAM_INT);

if ($password_form_set->is_cancelled() || $password_form_mail->is_cancelled()) {
    redirect($CFG->wwwroot . '/manage/people/profile.php?id=' . $user_id);
} else if ($_qf__password_form_set == 1) {//change password, then send email
    $newpassword = optional_param('newpassword', 0, PARAM_TEXT);
    global $DB;
    //$password_form_set->setfields($setpassword);
    //$DB->update_record('user', $setpassword);
    //$user_id=optional_param('id', 0, PARAM_TEXT);
    $password = hash_internal_user_password($newpassword);
    $DB->set_field('user', 'password', $password, array('id' => $user_id));
    $user = $DB->get_record('user', array('id' => $user_id));
    //$newpassword = update_user_new_password($newpassword);

    $mailto = $user->email;
    $username = $user->username;

    $invitationContenNoEmail = $emailContentNo;
    $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
    $invitationContenNoEmail = str_replace("{PASSWORD}", $newpassword, $invitationContenNoEmail);
    $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
    $result = sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
    $invitationContenNoEmail = $emailContentNo;

    redirect($CFG->wwwroot . '/manage/people/profile.php?id=' . $user_id);
    exit; //never reached
} else if ($emailalink = $password_form_mail->get_data()) {//send login email
    //$user_id=optional_param('id', 0, PARAM_TEXT);
    $user->email = $emailalink->email;
    //if ($password_form_mail->send_confirmation_email($user)) {
    $newPass = update_user_password($user->id);
    $mailto = $user->email;
    $username = $user->username;

    $invitationContenNoEmail = $emailContentNo;
    $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
    $invitationContenNoEmail = str_replace("{PASSWORD}", $newPass, $invitationContenNoEmail);
    $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
    $result = sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
    $invitationContenNoEmail = $emailContentNo;

    if ($result) {
        redirect($CFG->wwwroot . '/manage/people/profile.php?id=' . $user_id);
    } else {
        redirect($CFG->wwwroot . '/manage/people/password.php?user_id=' . $user_id);
    }
    exit; //never reached
}

$PAGE->set_title(get_string('password'));
$PAGE->set_heading(get_string('password'));
//now the page contents
$PAGE->set_pagelayout(get_string('password'));
echo $OUTPUT->header();
?>

<div class="row">
    <div class="col-md-9">
        <!-- Left Content -->
        <div class="card-box">
            <div class="panel-head clearfix">
                <div class="media-box">
                    <div class="img">
                        <?php 
                        //echo $OUTPUT->user_picture($user, array('size'=>100));
                        if($user->profile_img!='') { ?>
                            <img src="<?php echo $user->profile_img ?>" width="100" height="100"/>
                        <?php } ?>                           
                    </div>
                    <div class="blurb">
                        <div class="float-right">
                            <div class="subtitle italic"><?php
                                print_r(get_string('lastloginwas')) . " ";
                                print time_ago($user->lastlogin);
                                ?></div>
                            <div class="align-right">
                                <span class="box-tag <?php echo ($user->confirmed ? 'box-tag-green' : 'box-tag-grey') ?> " title=""><span><?php echo ($user->confirmed ? get_string('active') : get_string('inactive')) ?></span></span>
                                <span class="box-tag box-tag-orange " title=""><span><?php print people_get_roles($user->id); ?></span></span>
                            </div>
                        </div>
                        <h3><?php echo $user->lastname.' '.$user->firstname?></h3>
                        <div class="subtitle">
                            <?php print obfuscate_mailto($user->email, ''); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class='wrapper-inner'>
                <?php $password_form_mail->display(); ?>
                <?php $password_form_set->display(); ?>
            </div>
        </div>
    </div><!-- Left Content -->
</div>
<?php
echo $OUTPUT->footer();
?>