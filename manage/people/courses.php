<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once('lib.php');
require_once($CFG->dirroot . '/common/lib.php');

// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
    $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin
require_login(0, false);
$strmessages = '';
$PAGE->set_title(get_string('courses'));
$PAGE->set_heading(get_string('courses'));
//now the page contents
//$PAGE->set_pagelayout('courses');
echo $OUTPUT->header();

$PAGE->requires->js('/manage/manage.js');
$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/search.js');
// Require Login.
require_login();
//List My Course

$userid = optional_param('id', 0, PARAM_INT);
$user = $DB->get_record('user', array('id' => $userid));
$path->achievements = $userid ? '/manage/people/achievements.php?id=' . $userid : '/manage';
$path->recent = $userid ? '/manage/people/profile.php?id=' . $userid : '/manage';
$path->courses = $userid ? '/manage/people/courses.php?id=' . $userid : '/manage';
$path->teams = $userid ? '/manage/people/teams.php?id=' . $userid : '/manage';

$courses = enrol_get_my_courses_for_people();
?>
<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <div class="panel-head clearfix">
                <div class="media-box">
                    <div class="img"><?php echo $OUTPUT->user_picture($user, array('size' => 100)); ?></div>
                    <div class="blurb">
                        <div class="float-right">
                            <div class="subtitle italic"><?php
                                print_r(get_string('lastloginwas')) . " ";
                                print time_ago($user->lastlogin);
                                ?></div>
                            <div class="align-right">
                                <span class="box-tag <?php echo ($user->confirmed ? 'box-tag-green' : 'box-tag-grey') ?> " title=""><span><?php echo ($user->confirmed ? get_string('active') : get_string('inactive')) ?></span></span>
                                <span class="box-tag box-tag-orange " title=""><span><?php print people_get_roles($user->id); ?></span></span>
                            </div>
                        </div>
                        <h3><?php print obfuscate_text(fullname($user, true), ''); ?></h3>
                        <div class="subtitle">
                            <?php print obfuscate_mailto($user->email, ''); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class='wrapper-inner'>
                <div class="grid-tabs">
                    <ul>        
                        <li><a href="<?php print new moodle_url($path->recent); ?>"><?php print_r(get_string('pluginname', 'block_recent_activity')) ?></a></li>
                        <li><a href="<?php print new moodle_url($path->achievements); ?>"><?php print_r(get_string('achievements')) ?></a></li> 
                        <li><a href="<?php print new moodle_url($path->courses); ?>" class="selected"><?php print_r(get_string('courses')) ?></a></li>
                        <li><a href="<?php print new moodle_url($path->teams); ?>"><?php print_r(get_string('teams')) ?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <?php
                $content = "<div id=\"searchResults\">";
                $content .="<ul>";
                $finded = 0;
                foreach ($courses as $course) {
                    $finded++;
                    $content .="<li>";
                    $content .="<div class=\"main-col\">";
                    $content . "<div class=\"title\">";
                    $link = new moodle_url('/course/view.php', array('id' => $course->id));
                    $content .= "<a href=\"$link\"><span class=\"nav-reports\">$course->fullname</span></a>";
                    $content .= "<div class=\"tip\">";
                    $content .= $course->idnumber;
                    $content .= "</div></div>"; // End main col
                    $content .="<div class='right-col'>";
                    //$edit = new moodle_url('/course/view.php', array('id' => $course->id));
                    $edit = $CFG->wwwroot . '/course/people/edit_result.php?id=' . $course->id . '&user_id=' . $userid;
                    $active = $course->visible ? 'Active' : 'Inactive';
                    $count = count_course_completion($course, $userid);
                    if ($count == 100) {
                        $content .= '<span class="box-tag box-tag-green " title="Tỷ lệ hoàn thành">100%<br>' . get_string('complete') . '</span>';
                    } else {
                        $content .= '<span class="box-tag box-tag-orange" title="Tỷ lệ hoàn thành">' . $count . '%<br>' . get_string('complete') . '</span>';
                    }
//                    $content .= '<span class="box-tag box-tag-orange " title="">course<br>updated</span>';
                    $content .= '<span class="box-tag box-tag-grey"><a href="' . $edit . '">' . get_string('editthis') . '</a></span>';
                    $content .="</div>"; // End right col
                    $content .="</li>";
                }
                $content .="</ul>";
                $content .="<div class=\"courses-found\">$finded " . get_string('coursefound', 'moodle') . "</div>";
                $content .="</div>"; // End div rearchReasult
                echo $content;
                ?>
            </div>
        </div> 
    </div><!-- Left Content -->
    <div id="admin-fullscreen-right">
        <div class="side-panel">
            <div class="action-buttons">
                <a href="<?php echo new moodle_url('/course/edit.php', array('category' => 1, 'returnto' => 'topcat')); ?>" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><b><?php print_r(get_string('createanewcourse')) ?></b></span></span></span></span></a>
            </div>
        </div>
        <div class="side-panel">
            <h3><?php print_r(get_string('recentlyviewedcourses')) ?></h3>
            <ul>
                <?php
                $logs = get_logs_view_course(0, 30);
                $logs = array_filter($logs);
                foreach ($logs as $log) {
                    ?>
                    <li><a href="<?php echo new moodle_url('/course/view.php', array('id' => $log->course)); ?>">
                            <?php echo $log->fullname; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>  <!-- End Right -->
</div>
<?php
echo $OUTPUT->footer();
?>
