<?php

/**
 * Library of functions and constants for notes
 */
/**
 * Constants for states.
 */
define('NOTES_STATE_DRAFT', 'draft');
define('NOTES_STATE_PUBLIC', 'public');
define('NOTES_STATE_SITE', 'site');

/**
 * Constants for note parts (flags used by news_print and news_print_list).
 */
define('NOTES_SHOW_FULL', 0x07);
define('NOTES_SHOW_HEAD', 0x02);
define('NOTES_SHOW_BODY', 0x01);
define('NOTES_SHOW_FOOT', 0x04);

/**
 * Retrieves a list of note objects with specific atributes.
 *
 * @param int    $courseid id of the course in which the notes were posted (0 means any)
 * @param int    $userid id of the user to which the notes refer (0 means any)
 * @param string $state state of the notes (i.e. draft, public, site) ('' means any)
 * @param int    $author id of the user who modified the note last time (0 means any)
 * @param string $order an order to sort the results in
 * @param int    $limitfrom number of records to skip (offset)
 * @param int    $limitnum number of records to fetch
 * @return array of note objects
 */

/**
 * Retrieves a note object based on its id.
 *
 * @param int    $news_id id of the note to retrieve
 * @return note object
 */
function people_load($pid) {
    global $DB;

    $fields = '*';
    return $DB->get_record('post', array('id' => $pid, 'module' => 'user'), $fields);
}

/**
 * Saves a note object. The note object is passed by reference and its fields (i.e. id)
 * might change during the save.
 *
 * @param note   $note object to save
 * @return boolean true if the object was saved; false otherwise
 */

function people_save(&$note) {
    global $USER, $DB;

    // save data
    if (empty($note->id)) {
        // insert new note
        $note->created = $note->lastmodified;
        $id = $DB->insert_record('post', $note);
        $note = $DB->get_record('post', array('id' => $id));
    } else {
        // update old note
        $DB->update_record('post', $note);
    }
    unset($note->module);
    return true;
}

function people_sort($a, $b) {
    if ($a->sortorder == $b->sortorder)
        return 0;
    return $a->sortorder < $b->sortorder ? -1 : 1;
}

