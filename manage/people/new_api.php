<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Tung added for add new user: from lmstuyendung to lmsdaotao
 * 
 */

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');

global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/teams/config.php');
//require_login(0, false);

$PAGE->set_title(get_string('newprofile'));
$PAGE->set_heading(get_string('newprofile'));
global $USER;
//$user_id = optional_param('user_id', '', PARAM_TEXT);

$action = optional_param('action', '', PARAM_TEXT);
//echo $OUTPUT->header();
//$user = get_user_from_id($user_id);

if ($action == 'add_profile') {
    $firstname = optional_param('firstname', '', PARAM_TEXT);
    $state = optional_param('state', '', PARAM_TEXT);
    $postcode = optional_param('postcode', '', PARAM_TEXT);
    $lastname = optional_param('lastname', '', PARAM_TEXT);
    $username = optional_param('username', '', PARAM_TEXT);
//    $username = $_GET['username'];
    $password = optional_param('password', '', PARAM_TEXT);
    $address = optional_param('address', '', PARAM_TEXT);
    $city = optional_param('city', '', PARAM_TEXT);
    $zip = optional_param('zip', '', PARAM_TEXT);
    $country = optional_param('country', '', PARAM_TEXT);
    $timezone = optional_param('timezone', '', PARAM_TEXT);
    $title = optional_param('title', '', PARAM_TEXT);
    $company = optional_param('company', '', PARAM_TEXT);
    $email = optional_param('email', '', PARAM_TEXT);
//    $email = $_GET('email');
    $phone1 = optional_param('phone1', '', PARAM_TEXT);
    $phone2 = optional_param('phone2', '', PARAM_TEXT);
//    $phone2 = $_GET('phone2');
    $skype = optional_param('skype', '', PARAM_TEXT);
    $twitter = optional_param('twitter', '', PARAM_TEXT);
    $website = optional_param('website', '', PARAM_TEXT);
    $role_id = optional_param('access_level', '', PARAM_TEXT);
    $send_notif = optional_param('send_notif', 1, PARAM_TEXT);
    $course_id = 104;
    
//    decode url parameter
    $firstname = urldecode($firstname);
    $username = urldecode($username);
    $email = urldecode($email);
    $phone2 = urldecode($phone2);
    
    $new_user_id = add_profile($firstname, $state, $postcode, $website, $lastname, $username, $password, $address, $city, $state, $zip, $country, $timezone, $title, $company, $email, $phone1, $phone2, $skype, $twitter, $website);
    
    $context = get_context_instance(CONTEXT_SYSTEM);
    
    role_assign($role_id, $new_user_id, $context->id);
    $newPass = update_user_password($new_user_id);
//    Assign course to user
    assign_user_to_course($new_user_id, $course_id);
    global $DB;
    $course = $DB->get_record('course', array('id' => $course_id));
    if ($send_notif == 1) {
        $mailto = $email;
        $invitationContenNoEmail = $emailContentNo;
        $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
        $invitationContenNoEmail = str_replace("{PASSWORD}", $newPass, $invitationContenNoEmail);
        $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
        $invitationContenNoEmail = str_replace("{COURSE_TITLE}", $course->fullname, $invitationContenNoEmail);
        $invitationContenNoEmail = str_replace("{COURSE_DESCRIPTION}", $course->summary, $invitationContenNoEmail);
        sendEmail($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
        $invitationContenNoEmail = $emailContentNo;
    }
//    redirect($CFG->wwwroot . "/manage/people/profile.php?id=" . $new_user_id);
}
?>

