<?php

require('../../config.php');
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

//value got from the get metho
$data['username']      = optional_param('username', "", PARAM_TEXT);
$data['name']      		 = optional_param('name', "", PARAM_TEXT);
$data['id']      		 	 = optional_param('id', "", PARAM_INT);
if($data['name'] == 'firstname'){
	
	
}elseif($data['name'] == 'username'){
	//checking weather user exists or not in $existing_users array
	$authplugin = get_auth_plugin($CFG->registerauth);
	$params	=	$data;
	$params['mnethostid']	=	$CFG->mnet_localhost_id;
	
	$select = 'mnethostid = :mnethostid';
	$select .= ' AND username = :username';
	$select .= $data['id'] > 0 ? ' AND id <> :id':'';
	if ($DB->record_exists_select('user',$select,$params)) {
		$errors['username'] = get_string('usernameexists');
	} else {
	            //check allowed characters
		if ($data['username'] !== moodle_strtolower($data['username'])) {
	  	$errors['username'] = get_string('usernamelowercase');
	  } else {
	  	if(empty($data['username'])){
	  		$a = 'Username';
	  		$errors['username'] = get_string('fieldrequired','',$a);
	  	}elseif ($data['username'] !== clean_param($data['username'], PARAM_USERNAME)) {
				$errors['username'] = get_string('invalidusername');
			}
		}
	}
	if(is_null($errors['username'])){
		$errors['username'] = true;
	}
	if (validate_email($data['username'])) {
		$errors['email'] = true;
	}else{
		$errors['email'] = false;
	}
}

die(json_encode($errors));

if(!function_exists('json_encode')){
  function json_encode($vars){
    $val = array();
    foreach($vars as $key =>$value){
      $val[]=$key.':'.$value;
    }
    return '{"'.implode('","', $val).'"}';
  }
}