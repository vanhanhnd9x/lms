<?php
Header("content-type: application/x-javascript;charset=utf-8");
$unique_login_values = $_GET['unique_login_values'];
$course_sales_values = $_GET['course_sales_values'];
$course_complete_values = $_GET['course_complete_values'];
$activityfor = $_GET['activityfor'];
$uniquelog = $_GET['uniquelog'];
$coursesale = $_GET['coursesale'];
$coursecomple = $_GET['coursecomple'];
echo "$(document).ready(function () {    
    var s1 = eval($unique_login_values);
    var s2 = eval($course_sales_values);
    var s3 = eval($course_complete_values);

    plot1 = $.jqplot('chart', [s1, s2, s3], {
        title:
            {
                text:$activityfor ,
                fontSize: '8pt',
                color: 'rgba(48, 45, 45, 0.68)'
            },
        // Turns on animatino for all series in this plot.
        animate: true,
        // Will animate plot on calls to plot1.replot({resetAxes:true})
        animateReplot: true,
        cursor: {
            show: true,
            zoom: false,
            looseZoom: false,
            showTooltip: false
        },
        series:[
            {                
                pointLabels: {
                    show: true
                },
                label: $uniquelog,
                renderer: $.jqplot.LineRenderer,
                showHighlight: true,
                rendererOptions: {
                    // Speed up the animation a little bit.
                    // This is a number of milliseconds. 
                    // Default for bar series is 3000. 
                    animation: {
                        speed: 1500
                    },
                    highlightMouseOver: true
                }
            },
            {
                label: $coursesale,
                rendererOptions: {
                    // speed up the animation a little bit.
                    // This is a number of milliseconds.
                    // Default for a line series is 2500.
                    animation: {
                        speed: 1200
                    }
                }
            },
            {
                label: $coursecomple,
                rendererOptions: {
                    // speed up the animation a little bit.
                    // This is a number of milliseconds.
                    // Default for a line series is 2500.
                    animation: {
                        speed: 800
                    }
                }
            }
        ],
        legend: {
            renderer: $.jqplot.EnhancedLegendRenderer, 
            show: true,
            placement: 'outsideGrid',
            location: 's', //nw, n, ne, e, se, s, sw, w
            rendererOptions: {
                numberRows: 1
            }
        },
        axes: {
            // These options will set up the x axis like a category axis.
            xaxis: {
                renderer:$.jqplot.DateAxisRenderer,
                tickOptions: {
                    formatString:'%b&nbsp;%#d'
                } 
            },
            yaxis: {
                tickOptions: {
                    formatString: \"%'d\"
                },                
            },            
        },
        highlighter: {
            show: true,
            sizeAdjust: 7.5
        }
    });
   
});";
 
?>