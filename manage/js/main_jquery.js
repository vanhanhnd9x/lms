//<![CDATA[ Chien edit Link JS in PHP
var M = {};
var pageURL = window.location;
// alert(pageURL);
// M.cfg = {
  // "wwwroot": "http:\/\/localhost\/elms",
  
//   "sesskey": "YxObCsBmfx",
//   "loadingicon": "http:\/\/localhost\/elms\/theme\/image.php?theme=nimble&image=i%2Floading_small",
//   "themerev": -1,
//   "theme": "nimble",
//   "jsrev": -1
// };
//]]>
//


// City
$(document).ready(function(){
  var url = '../../manage/manual/ajaxcity.php';
  $('#tinhThanhChange').change(function(){
    var idTinhThanh = $(this).val();
    // alert(idTinhThanh);
    $.get( url + '?id='+idTinhThanh,function(data){
      $('#quanHuyenChange').html(data);
    });
  });

  $('#quanHuyenChange').change(function(){
    var idQuanHuyen = $(this).val();
    // alert(idQuanHuyen);
    $.get(url + '?idqh='+idQuanHuyen,function(data){
      $('#xaPhuongChange').html(data);
    });
  });
});

// Schools
$(document).ready(function(){
  var url = '../../manage/manual/ajaxschools.php'
  $('#tinhThanhChange').change(function(){
    var idTinhThanh = $(this).val();
    $.get(url + '?city='+idTinhThanh,function(data){
        $('#ajaxschools').html(data); 
    });
  });

  $('#quanHuyenChange').change(function(){
    var idQuanHuyen = $(this).val();
    $.get(url + '?city='+$('#tinhThanhChange').val()+'&district='+idQuanHuyen,function(data){
        $('#ajaxschools').html(data); 
    });
  });
  $('#xaPhuongChange').change(function(){
    var idXaPhuong = $(this).val();
    $.get(url + '?city='+$('#tinhThanhChange').val()+'&district='+$('#quanHuyenChange').val()+'&commune='+idXaPhuong,function(data){
        $('#ajaxschools').html(data); 
    });
  });

  $("#timkiem").keyup(function() {
      $.get(url + '?key='+$('#timkiem').val(),function(data){
        $('#ajaxschools').html(data); 
    });
  });
});



function Confirm(title, msg, $true, $false, $link) { /*change*/
        var $content =  "<div class='dialog-ovelay'>" +
                        "<div class='dialog'><header>" +
                         " <h3> " + title + " </h3> " +
                         "<i class='fa fa-close'></i>" +
                     "</header>" +
                     "<div class='dialog-msg'>" +
                         " <p> " + msg + " </p> " +
                     "</div>" +
                     "<footer>" +
                         "<div class='controls'>" +
                             " <button class='button button-danger doAction'>" + $true + "</button> " +
                             " <button class='button button-default cancelAction'>" + $false + "</button> " +
                         "</div>" +
                     "</footer>" +
                  "</div>" +
                "</div>";
         $('body').prepend($content);
      $('.doAction').click(function () {
        window.open($link, "_self"); /*new*/
        $(this).parents('.dialog-ovelay').fadeOut(500, function () {
          $(this).remove();
        });
      });
      
      $('.cancelAction, .fa-close').click(function () {
        $(this).parents('.dialog-ovelay').fadeOut(500, function () {
          $(this).remove();
        });
      });   
}



