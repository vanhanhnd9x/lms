<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
// Require Login.
// $PAGE->set_title(get_string('dashboards'));
$PAGE->set_title('Thêm mới khối học sinh');
// $PAGE->set_heading(get_string('dashboards'));
$PAGE->set_heading('Thêm mới khối học sinh');
//now the page contents
// $PAGE->set_pagelayout(get_string('dashboards'));
$PAGE->set_pagelayout('Thêm mới khối học sinh');
require_login(0, false);
echo $OUTPUT->header();

?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-10">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="<?php echo $CFG->wwwroot ?>/manage/block-student/addnew.php?action=add_block_student" onsubmit="return validate();" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Trường học</label>
								<select name="schoolid" id="schoolid" class="form-control" required="">
									<?php 
									if (function_exists('get_all_shool')) {
										$schools=get_all_shool();
										if ($schools) {
											foreach ($schools as $key => $value) {
												# code...
												echo'<option value="'.$value->id.'">'.$value->name.'</option>';
											}
										}
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div id="alert" style="position: absolute;top: 0;right: 18px;">

							</div>
							<div class="form-group label-floating">
								<label class="control-label">Mã khối</label>
								<div id="erCompany" class="alert alert-danger" style="display: none;">
									<?php print_string('pleasecompany') ?></div>
									<input type="text" id="code" name="code" class="form-control" value="" required>

								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">Tên khối</label>
									<div id="erCompany" class="alert alert-danger" style="display: none;">
										<?php print_string('pleasecompany') ?></div>
										<input type="text" id="name" name="name" class="form-control" value="" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Ghi chú</label>
										<br>
										<textarea name="note" id="note" cols="30" rows="10" class="form-control">
										</textarea>
										<script>CKEDITOR.replace('note');</script>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-buttons" style="border-top: none !important">
										<input type="submit" id="submitBtn" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
										<?php print_r(get_string('or')) ?>
										<a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
		echo $OUTPUT->footer();
		$action = optional_param('action', '', PARAM_TEXT);
		if ($action=='add_block_student') {
	# code...
			$name = optional_param('name', '', PARAM_TEXT);
			$name1=trim($name);
			$schoolid = optional_param('schoolid', '', PARAM_TEXT);
			$note = optional_param('note', '', PARAM_TEXT);
			$note1=trim($note);
			$code = optional_param('code', '', PARAM_TEXT);
			$code1=trim($code);
			$id = '';
			$saveid=save_block_student($id,$name1,$schoolid,$note1,$code1);
			if(!empty($saveid)){
		// $url = new moodle_url('/manage/block-student/edit_block_student.php', array('id' => $saveid));
				echo displayJsAlert('', $CFG->wwwroot . "/manage/block-student/index.php");
			}else{

			}
		}
		?>
		<script>
			$(document).ready(function() {
				$("#code").keydown(function() {
					var dInput = $('#code').val();
					if(dInput!=''){
						$.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?code="+dInput,function(data){
							$("#alert").html(data);

						});
					}
				});
				$("#code").keyup(function() {
					var dInput = $('#code').val();
					if(dInput!=''){
						$.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?code="+dInput,function(data){
							$("#alert").html(data);

						});
					}
				});
			});
		</script>