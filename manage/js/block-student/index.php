<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_login(0, false);

$PAGE->set_title(get_string('block_student'));
$PAGE->set_heading(get_string('block_student'));
$PAGE->set_pagelayout(get_string('block_student'));
$search = optional_param('search', '', PARAM_TEXT);
echo $OUTPUT->header();

if (!empty($search)) {
	$name=isset($_GET['name'])?trim($_GET['name']):'';
	$schoolid=isset($_GET['schoolid'])?trim($_GET['schoolid']):'';
	$data = search_blockstudent($name,$schoolid,$page=1,$number=20);
	$url= $CFG->wwwroot.'/manage/block-student/index.php?search=timkiem&name='.$name.'&schoolid='.$schoolid.'&page=';
}else{
	if (function_exists('get_list_block_student_in_block')) {
		$page=isset($_GET['page'])?$_GET['page']:1;
		$number=20;
		$data=get_list_block_student_in_block($page,$number);
		$url= $CFG->wwwroot.'/manage/block-student/index.php?page=';
	}
}




?>
<script>
	
</script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="material-datatables">
					<div class="row mr_bottom15">
						<div class="col-md-2">
							<a href="<?php echo new moodle_url('/manage/block-student/addnew.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
						</div>
						<div class="col-md-10">
							<form action="" method="get" accept-charset="utf-8">
								<input type="text" name="search" value="timkiem" hidden> 
								<div class="row">
									<div class="col-md-4">
										<div class="form-group label-floating">
											<div id="erCompany" class="alert alert-danger" style="display: none;">
												<?php print_string('pleasecompany') ?></div>
												<input type="text" id="name" name="name" class="form-control" value="<?php echo $_GET['name']; ?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group label-floating">
												<select name="schoolid" id="schoolid" class="form-control" >
													<option value="">Trường Học</option>
													<?php 
													if (function_exists('get_all_shool')) {
														$schools=get_all_shool();
														if ($schools) {
															foreach ($schools as $key => $value) {
																?>
																<option value="<?php echo $value->id; ?>" <?php if (!empty($_GET['schoolid'])&&($_GET['schoolid']==$value->id)) {echo'selected';} ?>><?php echo $value->name; ?></option>
																<?php 
															}
														}
													}
													?>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-buttons">
												<button class="btn btn-success"><?php echo get_string('search'); ?></button> 
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="table-responsive" data-pattern="priority-columns">
							<form action="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php?action=deleteall" method="post">
								<div class="deleteall">
									<button class="btn ">Xóa</button>
								</div>
								<table id="tech-companies-1" class="table table-hover">
									<thead>
										<tr>
											<th data-priority="1">STT</th>
											<th data-priority="1">
												<input type="checkbox" class="check" id="checkAll" <?php if (empty($data)) {echo'disabled';} ?> > Check All
											</th>
											<th data-priority="1">Tên trường</th>
											<th data-priority="1">Tên khối</th>
											<th data-priority="1">Mã khối</th>
											<th data-priority="1">Tổng số lớp</th>
											<th data-priority="1">Tổng số học sinh</th>
											<th data-priority="1">Tổng số nhóm</th>
											<th class="disabled-sorting text-right">Hành động</th>
										</tr>
									</thead>

									<tbody id="ketquatimkiem">

										<?php 
									# code...
										if (!empty($data['data'])) {
										# code...
											$i=0;
											foreach ($data['data'] as $key => $value) {
											# code...
												$lop=get_count_class_in_block_student($value->id);
												$hocsinh=get_count_student_in_block_student($value->id);
												$nhom=get_count_group_in_block_student($value->id);
												$i++;
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td> <input type="checkbox" class="check" value="<?php echo $value->id; ?>" name="listid[]"></td>
													<td><?php 
													$school=get_info_school_with_block($value->schoolid); 
													echo $school->name; 
													?></td>
													<td><?php echo $value->name; ?></td>
													<td><?php echo $value->code; ?></td>
													<td><a href=""><?php echo $lop; ?></a></td>
													<td><a href=""><?php echo $hocsinh; ?></a></td>
													<td><a href=""><?php echo $nhom; ?></a></td>
													<td class="text-right">
														<a href="<?php echo $CFG->wwwroot ?>/manage/block-student/edit_block_student.php?id=<?php echo $value->id ?>" class="btn btn-info "><i class="fa fa-pencil" aria-hidden="true"></i></a>
														<!-- <a href="#" class="btn  btn-warning  edit"><i class="material-icons">dvr</i></a> -->
														<a href="javascript:void(0)" onclick="delete_block_student(<?php echo $value->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
													</td>
												</tr>
												<?php 
											}
										}else{
											echo'
											';
										}
										?>
										<tr>
											<td colspan="8" style="text-align: center;">Dữ liệu trống</td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
						<?php 
						if ($data['total_page']) {
							if ($page > 2) { 
								$startPage = $page - 2; 
							} else { 
								$startPage = 1; 
							} 
							if ($data['total_page'] > $page + 2) { 
								$endPage = $page + 2; 
							} else { 
								$endPage =$data['total_page']; 
							}
						}
						?>
						<p>Tổng số trang: <?php echo $data['total_page'] ?></p>
						<nav aria-label="Page navigation example">
							<ul class="pagination">
								<li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
									<a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>
								<?php 
								for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
									?>
									<li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
									<?php 
								}
								?>
								<li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
									<a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>
					<!-- end content-->
				</div>
				<!--  end card  -->
			</div>
			<!-- end col-md-12 -->
		</div>
	</div>
	<?php
	echo $OUTPUT->footer();
	$action = optional_param('action', '', PARAM_TEXT);
	
	
	if ($action=='deleteall') {
		$listid = optional_param('listid', '', PARAM_TEXT);
		# code...
		if(!empty($listid)){
			foreach ($listid as $key => $iddelete) {
				# code...
				delete_block_student($iddelete);
			}
			echo displayJsAlert('', $CFG->wwwroot . "/manage/block-student/index.php");
		}
	}
	
	?>
	<script>
		$('.deleteall').hide();
		$("#checkAll").click(function () {
			$(".check").prop('checked', $(this).prop('checked'));
		});
		$(document).ready(function() {
			$(":checkbox").click(function(event) {
				if ($(this).is(":checked"))
					$(".deleteall").show();
				else
					$(".deleteall").hide();
			});
		});
		
	</script>
	<script>
		function delete_block_student(iddelete){
			var urlweb= "<?php echo $CFG->wwwroot ?>/manage/block-student/index.php";
			var urldelte= "<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php";
			var e=confirm('Bạn có muốn xóa khối học sinh này không?');
			if(e==true){
				$.ajax({
					url:  urldelte,
					type: "post",
					data: {iddelete:iddelete} ,
					success: function (response) {
						window.location=urlweb;
					}
				});
			}
		}
	</script>