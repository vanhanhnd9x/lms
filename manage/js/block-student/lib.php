<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function save_block_student($id,$name,$schoolid,$note,$code){
	global $DB, $USER;
	if(!empty($id)){
		$DB->set_field('block_student', 'name', $name, array('id' => $id));
		$DB->set_field('block_student', 'schoolid', $schoolid, array('id' => $id));
		$DB->set_field('block_student', 'note', $note, array('id' => $id));
		$DB->set_field('block_student', 'code', $code, array('id' => $id));
		return $id;
		
	}else{
		//khoi tao doi tuong
		$data = new stdClass();
		$data->id = $id;
		$data->name = $name;
		$data->schoolid = $schoolid;
		$data->note = $note;
		$data->code = $code;
		$data->del = 0;
		$lastinsertid = $DB->insert_record('block_student', $data);
		return $lastinsertid;
	}
}
function delete_block_student($id){
	global $DB;
	// $DB->delete_records_select('block_student', " id = $id ");
	// return 1;
	$DB->set_field('block_student', 'del',1, array('id' => $id));

}

function get_block_student($id){
	if ($id) {
		# code...
		global $DB;
		$block = $DB->get_record('block_student', array(
			'id' => $id
		));

		return $block;
	}
}
function get_all_shool(){
	global $DB;
	$sql="SELECT id, name FROM schools WHERE del =0";
	$data = $DB->get_records_sql($sql); 
	return $data;
}
function get_info_school_with_block($id){
	if ($id) {
		# code...
		global $DB;
		$school = $DB->get_record('schools', array(
			'id' => $id,
		));

		return $school;
	}
}

// lay so lop cua mot khoi
function get_count_class_in_block_student($id){
	global $CFG, $DB;
	$sql = "SELECT COUNT(`course`.`id`) FROM `course` WHERE `course`.`id_khoi` = {$id}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_count_student_in_block_student($id){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `user`.`id`) FROM `user` 
	JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	JOIN `course` ON `enrol`.`courseid`=`course`.`id`
	JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
	WHERE `role_assignments`.`roleid`=5 
	AND `course`.`id` IN (SELECT `course`.`id` FROM `course` WHERE `course`.`id_khoi` = {$id})";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_count_group_in_block_student($id){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `groups`.`id`) FROM `groups` 
	JOIN`group_course` ON `groups`.`id`=`group_course`.`group_id` 
	JOIN `course` ON `group_course`.`course_id` = `course`.`id` 
	WHERE `course`.`id` IN (SELECT `course`.`id` FROM `course` WHERE `course`.`id_khoi` = {$id})";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}

function get_list_block_student_in_block($page='',$number=''){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}

	$sql = "SELECT  * FROM block_student  WHERE `block_student`.`del`= 0
	LIMIT {$start} , {$number}
	";
	$data1=$DB->get_records_sql($sql);
	$sql_total="SELECT COUNT( `block_student`.`id`) FROM block_student  WHERE `block_student`.`del`= 0 ";
	$total_row=$DB->get_records_sql($sql_total);

	foreach ($total_row as $key => $value) {
    # code...
		$total_page=ceil((int)$key / (int)$number);
		break;
	}
	$kq=array(
		'data'=>$data1,
		'total_page'=>$total_page 
	);
	return $kq;
}
function search_blockstudent($name=null,$schoolid=null,$page='',$number) {
	global $DB;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$rec = array();
	$sql="SELECT * FROM `block_student` WHERE del=0 ";
	$sql_total="SELECT COUNT(id) FROM `block_student` WHERE del=0 ";
	if(!empty($name)){
		$sql.="AND name LIKE '%{$name}%'";
		$sql_total.="AND name LIKE '%{$name}%'";
	}
	if(!empty($schoolid)){
		$sql.=" AND schoolid ={$schoolid}";
		$sql_total.=" AND schoolid ={$schoolid}";
	}
	$sql .= " LIMIT {$start} , {$number} ";
	$rec = $DB->get_records_sql($sql);

	$total_row=$DB->get_records_sql($sql_total);

	foreach ($total_row as $key => $value) {
    # code...
		$total_page=ceil((int)$key / (int)$number);
		break;
	}
	$kq=array(
		'data'=>$rec,
		'total_page'=>$total_page 
	);
	return $kq;

}
?>