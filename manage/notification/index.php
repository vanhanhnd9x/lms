<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../../config.php");
global $CFG, $USER;
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/message/message_lib.php');
require_once($CFG->dirroot . '/message/send_form.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');

if(!is_student()){
  echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
// $PAGE->set_url('/mod/noticeboard/view.php');
$PAGE->set_title(get_string('notifications'));
$PAGE->set_heading(get_string('notifications'));
echo $OUTPUT->header();
$thongbao=get_list_notifications_for_student($USER->id);
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <div class="col-md-12 pull-right">
              <?php 
                if(!empty($thongbao)){
                  foreach ($thongbao as  $value) {
                    # code...
                    if($value->link=='#'){
                      $url="#";
                    }else{
                      $url= $CFG->wwwroot.'/manage/notification/load_ajax.php?id='.$value->id.'&link='.$value->link;;
                    }
                    ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <strong><?php echo $value->title; ?></strong>
                      <a href="<?php echo $url; ?>"><?php echo $value->content; ?></a> 
                      <button type="button" class="close" onclick="close_notification(<?php echo $value->id; ?>)">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <?php 
                  }
                }else{
                  ?>
                  <div class="alert alert-success" role="alert">
                    Không có thông báo nào
                  </div>
                  <?php 
                }
              ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function close_notification(id){
    var urlweb = "<?php echo $CFG->wwwroot ?>/manage/notification/";
    var urldelte = "<?php echo $CFG->wwwroot ?>/manage/notification/load_ajax.php";
        $.ajax({
            url: urldelte,
            type: "post",
            data: { id: id },
            success: function(response) {
                window.location = urlweb;
            }
        });
  }
</script>
<?php
    echo $OUTPUT->footer();
?>