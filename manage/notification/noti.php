<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../../config.php");
global $CFG, $USER;
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/message/message_lib.php');
require_once($CFG->dirroot . '/message/send_form.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');

//Quang added to fix switch role to learner, when click in menu --> back to teacher role
$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
$id = optional_param('id', -1, PARAM_INT);//course id
$uid = optional_param('uid', $USER->id, PARAM_INT);//course id

if($current_switchrole!=-1&&$id !=-1){
$context = get_context_instance(CONTEXT_COURSE, $id);
role_switch($current_switchrole, $context);
require_login($id);

}
else{
require_login(0,false);
}

$PAGE->set_url('/mod/noticeboard/view.php');
$PAGE->set_title(get_string('notification'));
$PAGE->set_heading(get_string('notification'));
 
echo $OUTPUT->header();




$tc = get_c_u($USER->id);
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="form-group table-responsive">
        <table class="table table-bordered">
          <tr>
            <th>STT</th>
            <th><?php echo get_string('classgroup') ?></th>
            <th><?php echo get_string('namestudent') ?></th>
            <th><?php echo get_string('codestudent') ?></th>
            <th><?php echo get_string('attendance') ?></th>
            <th><?php echo get_string('point_unitest') ?></th>
          </tr>
          <?php $stt=1; foreach ($tc as $key => $val): ?>
          <?php $hs=get_st_c($val->id)?>
            <?php foreach ($hs as $key => $value): ?>
              <tr>
                <td><?php echo $stt; $stt++; ?></td>
                <td><?php echo $val->fullname ?></td>
                <td><?php echo $value->firstname." ".$value->lastname ?></td>
                <td><?php echo $value->code ?></td>
                <td>
                  <?php $d=0; ?>
                  <?php $nd  = get_n_t(date('m')); foreach ($nd as $key => $gg): ?>
                  <?php 
                    $user_dd = getDd($value->id,get_u_g($value->id),date('m'));
                      $day = $gg["ngay"];
                      $month= date('m');
                      $year = date('Y');
                      
                      // var_dump($ss);
                      foreach ($user_dd as $item) {
                          if ($item->day == $day && $item->month == $month && $item->year == $year) {
                              $d++;
                          }
                      }
                      
                   ?>   
                   <?php endforeach ?>
                   <?php echo (1-$d/$val->sotiet)*100; ?>%
                </td>
                <td>
                  <?php $unittestOfUser = get_d_u($value->id, date('Y')); ?>
                  <?php foreach ($unittestOfUser as $var): ?>
                      <?php if ($var->month==date('m')): ?>
                          <?php echo $var->score_1 ?>
                      <?php endif ?>              
                  <?php endforeach ?>
                </td>
              </tr>
            <?php endforeach ?>
          <?php endforeach ?>
        </table>
      </div>
    </div>
  </div>
</div>


<?php
    echo $OUTPUT->footer();
?>