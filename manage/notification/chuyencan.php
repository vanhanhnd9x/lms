<?php 
require("../../config.php");
global $CFG, $USER;
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/message/message_lib.php');
require_once($CFG->dirroot . '/message/send_form.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');

$thangcc = optional_param('thangcc', date('m'), PARAM_TEXT);
$namcc = optional_param('namcc', date('Y'), PARAM_TEXT);

// $thangcc = date('m');
// $namcc = date('Y');
 ?>
 <?php if(!empty($thangcc) && !empty($namcc)): ?>
<div class="form-group table-responsive">
  <label for="" class="col-md-2">Điểm danh</label><span>Tháng <?php echo $thangcc ?></span>
  <table class="table table-striped table-bordered">
    <tr>
    <?php 
    $nd  = get_n_t($thangcc);
    foreach ($nd as $key => $value): ?>
      <th><?php echo $value['ngay'] ?></th>
    <?php endforeach ?>
    </tr>
    <tr>
      <?php foreach($nd as $key=>$value) {?>
          <th class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
          if($value['thu']=='Sat'){echo 'bg-warning';} ?>" colspan="" rowspan="" headers="" scope=""> <?php  echo $value["thu"] ?></th>
      <?php } ?>
  </tr>
  <tr>
    <?php foreach($nd as $key=>$value) {?>
      <td><?php 
        $user_dd = getDd($USER->id,get_u_g($USER->id),$thangcc);
          $day = $value["ngay"];
          $month= $thangcc;
          $year = $namcc;
          // var_dump($ss);
          foreach ($user_dd as $item) {
            if ($item->day == $day && $item->month == $month && $item->year == $year) {
                echo get_string('absent');
            }
          }
           ?></td>
      <?php } ?>
  </tr>
  </table>
</div>
<?php endif ?>