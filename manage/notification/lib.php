<?php 
global $CFG, $DB, $USER;

//lấy thông tin ngày tháng
function get_n_t($month, $year = null)
{
  $year = ($year) ? $year : date('Y');
  $list = array();
for($day = 1; $day <= 31; $day++)
{
    $time = mktime(12, 0, 0, $month, $day, $year);          
    if (date('m', $time)==$month)       
        //$list[]=date('Y-m-d-D', $time);
        $list[] = [
        'thu'   => date('D', $time),
        'ngay'  => date('d', $time),
        'thang' => date('m', $time),
        'nam' => date('Y', $time)
      ];
}
return $list;
}

//lấy thông tin điểm danh
function getDd($userid,$courseid,$month,$year=null)
{
  global $DB;
  if(!empty($year)){
    return $DB->get_records('diemdanh', array('userid'=>$userid,'course_id'=>$courseid,'month'=>$month,'year'=>$year));
  }
  else{
    return $DB->get_records('diemdanh', array('userid'=>$userid,'course_id'=>$courseid,'month'=>$month));
  }
  
}


//lấy nhóm lớp từ user hs
function get_u_g($iduser)
{
  global $DB;
  $sql = "SELECT course_id AS 'id' FROM diemdanh WHERE userid = $iduser ";
  $result = $DB->get_record_sql($sql);
  return $result->id;
}

//lấy điểm unittest
function get_d_u($userid, $y=null)
{
  global $DB;
  if (!empty($y)) {
    return $DB->get_records('unittest', array('userid'=>$userid, 'school_yearid'=>$y));
  }
  return $DB->get_records('unittest', array('userid'=>$userid));
}

//lấy điểm học kỳ
function get_d_hk($userid, $y)
{
  global $DB;
  if(!empty($y)){
    return $DB->get_record('semester', array('userid'=>$userid, 'school_yearid'=>$y));
  }
  return $DB->get_records('semester', array('userid'=>$userid));
}

//lấy năm học
function get_n_h($id){
  global $DB, $USER;
  $sql="SELECT * FROM `school_year` WHERE id = $id";
  $data=$DB->get_record_sql($sql);
  return $data;
}

//lấy toàn bộ nhóm lớp do gv phụ trách
function get_c_u($usergv)
{
  global $DB, $USER;
  $sql="
    SELECT course.* FROM user_enrolments
    JOIN enrol ON user_enrolments.enrolid = enrol.id
    JOIN course ON enrol.courseid = course.id
    WHERE course.id_tg = $usergv OR course.id_gv = $usergv
  ";
  $data=$DB->get_records_sql($sql);
  return $data;
}
//lấy toàn bộ học sinh trong nhóm lớp
function get_st_c($idcourse)
{
  global $DB;
  $sql = "
    SELECT user.* FROM user
    JOIN user_enrolments ON user.id = user_enrolments.userid
    JOIN enrol ON user_enrolments.enrolid = enrol.id
    WHERE enrol.courseid = $idcourse AND user.id !=2
  ";
  $data=$DB->get_records_sql($sql);
  return $data;
}

// add thong bao 
function add_notifications_for_student($userid,$title,$content,$link){
  global $DB, $USER;
  $record = new stdClass();
  $record->userid = $userid;
  $record->title = $title;
  $record->content = $content;
  $record->view = 0;
  $record->link = $link;
  $record->time = time();
  $new_id = $DB->insert_record('notifications', $record);
  return $new_id;
}
function get_list_notifications_for_student($userid){
  global $DB;
  $sql="
    SELECT * FROM `notifications` 
    WHERE `notifications`.`userid`={$userid}
    AND `notifications`.`view` =0
    ORDER BY `notifications`.`time` DESC
  ";
  $data=$DB->get_records_sql($sql);
  return $data;
}
function conunt_notifications_student($userid){
  global $DB;
  $sql="
    SELECT COUNT(`notifications`.`id`) FROM `notifications` 
    WHERE `notifications`.`userid`={$userid}
    AND `notifications`.`view` =0
    ORDER BY `notifications`.`time` DESC
  ";
  $data=$DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}
function hidden_notifications_student($id){
  global $DB;
  $record = new stdClass();
  $record->id = $id;
  $record->view =1;
  return $DB->update_record('notifications', $record, false);
}
?>
