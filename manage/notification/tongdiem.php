<?php 
require("../../config.php");
global $CFG, $USER;
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/message/message_lib.php');
require_once($CFG->dirroot . '/message/send_form.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');

$nc = $_GET['nam'];
 ?>
<div class="col-md-12">
  <label for=""><?php echo get_string('semester'); ?></label>
  <div class="form-group table-responsive">
<!-- table -->    <table class="table table-bordered">
      <?php $dhk = get_d_hk($USER->id,$nc); ?>
      <tr>
        <th></th>
        <th><?php echo get_string('speaking'); ?></th>
        <th><?php echo get_string('essay'); ?></th>
        <th><?php echo get_string('reading'); ?></th>
        <th><?php echo get_string('listening'); ?></th>
        <th><?php echo get_string('vocabularyGramar'); ?></th>
        <th><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
        <th><?php echo get_string('totalMidtermMark1'); ?></th>
      </tr>
      <tr>
        <th><label for=""><?php echo get_string('semester1') ?></label></th>
        <td><?php echo $dhk->noi_1 ?></td>
        <td><?php echo $dhk->viet_luan_1 ?></td>
        <td><?php echo $dhk->doc_hieu_1 ?></td>
        <td><?php echo $dhk->nghe_1 ?></td>
        <td><?php echo $dhk->tv_np_1 ?></td>
        <td><?php echo $dhk->tvnp_dochieu_viet_1 ?></td>
        <td><?php echo $dhk->tonghocki_1 ?></td>
      </tr>
      <tr>
        <th><label for=""><?php echo get_string('semester2') ?></label></th>
        <td><?php echo $dhk->noi_2 ?></td>
        <td><?php echo $dhk->viet_luan_2 ?></td>
        <td><?php echo $dhk->doc_hieu_2 ?></td>
        <td><?php echo $dhk->nghe_2 ?></td>
        <td><?php echo $dhk->tv_np_2 ?></td>
        <td><?php echo $dhk->tvnp_dochieu_viet_2 ?></td>
        <td><?php echo $dhk->tonghocki_2 ?></td>
      </tr>
<!-- endtable --> </table>
  </div>
</div>
<div class="col-md-12">
  <label for=""><?php echo get_string('list_unitest') ?></label>
  <div class="form-group table-responsive">
    <table class="table table-bordered">
      <tr>
          <th></th>
          <th>Tháng 1</th>
          <th>Tháng 2</th>
          <th>Tháng 3</th>
          <th>Tháng 4</th>
          <th>Tháng 5</th>
          <th>Tháng 6</th>
          <th>Tháng 7</th>
          <th>Tháng 8</th>
          <th>Tháng 9</th>
          <th>Tháng 10</th>
          <th>Tháng 11</th>
          <th>Tháng 12</th>
      </tr>
      <tr>
          <th>Điểm</th>
          <?php $unittestOfUser = get_d_u($USER->id, $nc); ?>
          <?php for($i=1;$i<=12;$i++) : ?>
              <td>
              <?php foreach ($unittestOfUser as $val): ?>
                  <?php if ($i==$val->month): ?>
                      <?php echo $val->score_1 ?>
                  <?php endif ?>              
              <?php endforeach ?>
              </td>
          <?php endfor; ?>
      </tr>
    </table>
  </div>
</div>