<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../../config.php");
global $CFG, $USER;
require_once($CFG->dirroot . '/message/lib.php');
require_once($CFG->dirroot . '/message/message_lib.php');
require_once($CFG->dirroot . '/message/send_form.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');


//Quang added to fix switch role to learner, when click in menu --> back to teacher role
$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
$id = optional_param('id', -1, PARAM_INT);//course id
$uid = optional_param('uid', $USER->id, PARAM_INT);//course id

if($current_switchrole!=-1&&$id !=-1){
$context = get_context_instance(CONTEXT_COURSE, $id);
role_switch($current_switchrole, $context);
require_login($id);

}
else{
require_login(0,false);
}

$PAGE->set_url('/mod/noticeboard/view.php');
$PAGE->set_title(get_string('notifications'));
$PAGE->set_heading(get_string('notifications'));
 
echo $OUTPUT->header();
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div id="accordion" role="tablist" aria-multiselectable="true" class="m-b-30">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h5 class="mb-0 mt-0">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="text-dark collapsed" aria-expanded="false" aria-controls="collapseOne">
        <?php echo get_string('pointofstudent') ?>
        </a>
      </h5>
    </div>
    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="card-box">
              <div class="row form-group">
                <div class="col-md-10"></div>
                <div class="col-md-2">
                  <label for=""><?php echo get_string('school_year'); ?></label>
                  <select name="" id="namlc" class="form-control">
                    <?php $namhoc = get_d_hk($uid);?>
                    <?php $idn = (int) end($namhoc)->id ?>
                    <?php foreach ($namhoc as $key => $vl): ?>
                      <?php $v=get_n_h($vl->school_yearid) ?>
                      <option <?php echo $idn==$vl->id ? "selected" : "" ?> value="<?php echo $v->id ?>"><?php echo $v->sy_start."-".$v->sy_end ?></option>
                      <?php $nc = $v->id; ?>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="row" id="tongdiem">
                <div class="col-md-12">
                  <label for=""><?php echo get_string('semester'); ?></label>
                  <div class="form-group table-responsive">
  <!-- table -->    <table class="table table-bordered">
                      <?php $dhk = get_d_hk($uid,$nc); ?>
                      <tr>
                        <th></th>
                        <th><?php echo get_string('speaking'); ?></th>
                        <th><?php echo get_string('essay'); ?></th>
                        <th><?php echo get_string('reading'); ?></th>
                        <th><?php echo get_string('listening'); ?></th>
                        <th><?php echo get_string('vocabularyGramar'); ?></th>
                        <th><?php echo get_string('vocabularyGrammarReadingWriting'); ?></th>
                        <th><?php echo get_string('totalMidtermMark1'); ?></th>
                      </tr>
                      <tr>
                        <th><label for=""><?php echo get_string('semester1') ?></label></th>
                        <td><?php echo $dhk->noi_1 ?></td>
                        <td><?php echo $dhk->viet_luan_1 ?></td>
                        <td><?php echo $dhk->doc_hieu_1 ?></td>
                        <td><?php echo $dhk->nghe_1 ?></td>
                        <td><?php echo $dhk->tv_np_1 ?></td>
                        <td><?php echo $dhk->tvnp_dochieu_viet_1 ?></td>
                        <td><?php echo $dhk->tonghocki_1 ?></td>
                      </tr>
                      <tr>
                        <th><label for=""><?php echo get_string('semester2') ?></label></th>
                        <td><?php echo $dhk->noi_2 ?></td>
                        <td><?php echo $dhk->viet_luan_2 ?></td>
                        <td><?php echo $dhk->doc_hieu_2 ?></td>
                        <td><?php echo $dhk->nghe_2 ?></td>
                        <td><?php echo $dhk->tv_np_2 ?></td>
                        <td><?php echo $dhk->tvnp_dochieu_viet_2 ?></td>
                        <td><?php echo $dhk->tonghocki_2 ?></td>
                      </tr>
  <!-- endtable --> </table>
                  </div>
                </div>
                <div class="col-md-12">
                  <label for=""><?php echo get_string('list_unitest') ?></label>
                  <div class="form-group table-responsive">
                    <table class="table table-bordered">
                      <tr>
                          <th></th>
                          <?php for ($i=1; $i <=12 ; $i++) : ?>
                          <th><option><?php echo get_string('month')." ".$i ?></option></th>
                          <?php endfor ?>
                      </tr>
                      <tr>
                          <th><?php echo get_string('point') ?></th>
                          <?php $unittestOfUser = get_d_u($uid, $nc); ?>
                          <?php for($i=1;$i<=12;$i++) : ?>
                              <td>
                              <?php foreach ($unittestOfUser as $val): ?>
                                  <?php if ($i==$val->month): ?>
                                      <?php echo $val->score_1 ?>
                                  <?php endif ?>              
                              <?php endforeach ?>
                              </td>
                          <?php endfor; ?>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div><!-- end card-box -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0 mt-0">
        <a class="text-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <?php echo get_string('attendance') ?>
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" style="">
      <div class="card-body">
        <div class="row">
    <div class="col-12">
        <div class="card-box">
          <div class="row form-group">
            <div class="col-md-3">
              <label for="thangcc"><?php echo get_string('month') ?></label>
              <select name="" id="thangcc" class="form-control">
                <?php for ($i=1;$i<=9;$i++): ?>
                  <option <?php echo $i==date('m')? 'selected' : '' ?> value="0<?php echo $i ?>"><?php echo get_string('month').' '.$i ?></option>
                <?php endfor ?>
                <?php for ($i=10;$i<=12;$i++): ?>
                  <option <?php echo $i==date('m')? 'selected' : '' ?> value="<?php echo $i ?>"><?php echo get_string('month').' '.$i ?></option>
                <?php endfor ?>
              </select>
            </div>
            <div class="col-md-3">
              <label for="namcc"><?php echo get_string('year') ?></label>
              <select name="" id="namcc" class="form-control">
                <?php for ($i=2010;$i<=date('Y');$i++): ?>
                  <option <?php echo $i==date('Y')? 'selected' : '' ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php endfor ?>
              </select>
            </div>
          </div>
          <div id="ajcc">
            <div class="form-group table-responsive">
                <label for="" class="col-md-2"><?php echo get_string('attendance') ?></label><span><?php echo get_string('month')." ".date('m') ?></span>
                <table class="table table-striped table-bordered">
                  <tr>
                  <?php 
                  $nd  = get_n_t(date('m'));
                  foreach ($nd as $key => $value): ?>
                    <th><?php echo $value['ngay'] ?></th>
                  <?php endforeach ?>
                  </tr>
                  <tr>
                    <?php foreach($nd as $key=>$value) {?>
                        <th class="<?php if($value['thu']=='Sun'){echo 'bg-danger';} 
                        if($value['thu']=='Sat'){echo 'bg-warning';} ?>" colspan="" rowspan="" headers="" scope=""> <?php  echo $value["thu"] ?></th>
                    <?php } ?>
                </tr>
                <tr>
                  <?php foreach($nd as $key=>$value) {?>
                        <td><?php 
                          $user_dd = getDd($uid,get_u_g($uid),date('m'));
                            $day = $value["ngay"];
                            $month= date('m');
                            $year = date('Y');
                            // var_dump($ss);
                            foreach ($user_dd as $item) {
                                if ($item->day == $day && $item->month == $month && $item->year == $year) {
                                    echo get_string('absent');
                                }
                            }
                             ?></td>
                    <?php } ?>
                </tr>
                </table>
            </div>
          </div>
        </div>
    </div>
</div>
      </div>
    </div>
  </div>

  <!-- tin nhắn -->
  <div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <h5 class="mb-0 mt-0">
        <a class="text-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
        <?php echo get_string('message') ?>
        <span style="background: red; border-radius: 50%; color: #fff; padding: 3px 5px;"><?php echo count_unread_message($uid) ?></span>
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" style="">
      <div class="card-body">
        <div id="searchResults">
                            <?php
                                $count_unread = 0;
                                $mss = get_unread_messages($uid);
                                foreach ($mss as $key) {
                                    $from_name_obj = getUserFromId($key->useridfrom);
                                    $from_name = $from_name_obj->firstname . " " . $from_name_obj->lastname;
                            ?>
                            <div class="row">
                                <div class="col-md-10 float-left mb-2">
                                    <div class="title">
                                        <a href="<?php echo $CFG->wwwroot ?>/message/message.php?action=read_message&user_from=<?php echo $key->useridfrom ?>">
                                            <?php echo $from_name; ?>
                                        </a>
                                        <span class="box-tag box-tag-lightblue " title="">
                                            <span><?php print_r(get_string('unread')) ?></span>
                                        </span>
                                    </div>
                                    <div class="tip"><?php echo $key->subject; ?></div>
                                </div>
                                <div class="col-md-2 float-right mb-2">
                                    <div class="nowrap align-right">
                                        <span class="tip"><?php echo time_ago($key->timecreated); ?></span>
                                        <span>
                                            <a class="remove-msg btn-remove-ms float-right" title='<?php print_r(get_string('delete')) ?>' onclick='javascript:displayConfirmBox("<?php echo $CFG->wwwroot ?>/message/message.php?action=delete&message_id=<?php echo $key->id ?>&confirm=yes",<?php echo $key->useridfrom ?>,"<?php print_r(get_string('areyou')) ?>")' href='#'><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </span> 
                                    </div>   
                                </div>
                            </div>
                            <hr>                             

                            <?php
                                $count_unread++;
                                }
                            ?>

                            <?php
                                $count_read = 0;
                                $mss = get_read_messages($uid);
                                foreach ($mss as $key) {
                                    $from_name_obj = getUserFromId($key->useridfrom);
                                    $from_name = $from_name_obj->firstname . " " . $from_name_obj->lastname;
                            ?>
                            <div class="row">
                                <div class="col-md-10 float-left mb-2">
                                    <div class="title">
                                        <a href="<?php echo $CFG->wwwroot ?>/message/message.php?action=read_message&user_from=<?php echo $key->useridfrom ?>">
                                            <?php echo $from_name; ?>
                                        </a>
                                    </div>
                                    <div class="tip"><?php echo $key->subject; ?></div>
                                </div>

                                <div class="col-md-2 float-right mb-2">           
                                    <div class="nowrap align-right">
                                        <span class="tip"><?php echo time_ago($key->timecreated); ?></span>
                                        <span>
                                            <a class="remove-msg btn-remove-ms float-right" title='<?php print_r(get_string('delete')) ?>' onclick='javascript:displayConfirmBox("<?php echo $CFG->wwwroot ?>/message/message.php?action=delete&&message_id=<?php echo $key->id ?>&confirm=yes",<?php echo $key->useridfrom ?>,"<?php print_r(get_string('areyou')) ?>")' href='#'><i class="fa fa-times" aria-hidden="true" style="color:#f1556c;"></i></a>
                                        </span> 
                                    </div>
                                </div>
                            </div>
                            <hr>                  
                                

                            <?php
                                $count_read++;
                                }
                            ?>  

                            <div class="tip center-padded" id="count-ms">
                                <?php
                                    $ms_str = get_string('message');
                                    if ($count_read + $count_unread > 1) {
                                      $ms_str = get_string('messages');
                                    }
                                ?>
                                <?php echo $count_read + $count_unread . " " . $ms_str ?>
                            </div>
                        </div> 
      </div>
    </div>
  </div>
  <!-- <div class="card">
    <div class="card-header" role="tab" id="ccc">
      <h5 class="mb-0 mt-0">
        <a class="text-dark" data-toggle="collapse" data-parent="#accordion" href="#colll" aria-expanded="true" aria-controls="colll">
        Đánh giá học sinh
        </a>
      </h5>
    </div>
    <div id="colll" class="collapse" role="tabpanel" aria-labelledby="ccc" style="">
      <div class="card-body">
        ------------------------------
      </div>
    </div>
  </div> -->
</div>
    </div>
  </div>
</div>

<script>
  $('#namcc').change(function(event) {
    $.get('chuyencan.php?thangcc='+$('#thangcc').val()+'&namcc='+$('#namcc').val(), function(data){
      $('#ajcc').html(data);
    });
  });
  $('#thangcc').change(function(event) {
    $.get('chuyencan.php?thangcc='+$('#thangcc').val()+'&namcc='+$('#namcc').val(), function(data){
      $('#ajcc').html(data);
    });
  });
</script>
<script>
  $('#namlc').change(function(event) {
    $.get('tongdiem.php?nam='+$(this).val(), function(data){
      $("#tongdiem").html(data);
    });
  });
</script>
<?php
    echo $OUTPUT->footer();
?>