<?php 

require_once('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once ($CFG->dirroot. '/manage/notification/lib.php');
global $DB;
if(!empty($_POST['id'])){
	$id=$_POST['id'];
	hidden_notifications_student($id);
}
if(!empty($_GET['id'])&&!empty($_GET['link'])){
	$id=$_GET['id'];
	$link=$_GET['link'];
	hidden_notifications_student($id);
	echo displayJsAlert('', $CFG->wwwroot . "/".$link);
}
?>
