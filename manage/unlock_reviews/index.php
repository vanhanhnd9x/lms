<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;

require_once($CFG->dirroot . '/common/lib.php');

// Require Login.
require_login();

$PAGE->set_title(get_string('unlock_evaluation_student'));
$PAGE->set_heading(get_string('unlock_evaluation_student'));
echo $OUTPUT->header();


$page       = optional_param('page', 0, PARAM_INT);
$perpage    = optional_param('perpage', 20, PARAM_INT);
$search     = optional_param('search', '', PARAM_TEXT);

$usercount  = get_unlock_evaluation_student($page,$perpage,$search);
$totalcount = count(get_unlock_evaluation_student(null,$perpage,$search));

$idev       = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_TEXT);

if($action=='active'){
   $DB->set_field('evalucation_student', 'view', 0, array('id' => $idev));
   echo displayJsAlert('Mở khóa đánh giá thành công', $CFG->wwwroot . "/manage/unlock_reviews"); 
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='unlock_evaluation_student';
$name1='unlock_evaluation_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>



<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mb-4">
                        <div class="col-md-10">
                            <form action="" method="get" accept-charset="utf-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="search" placeholder="Tìm kiếm..." class="form-control" value="<?php echo !empty($search) ? $search : ""; ?>">
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-success waves-light waves-effect"><?php echo get_string('search'); ?></button>
                                    </div>
                                    <?php if(!empty($search)): ?>
                                        <div class="col-md-2">
                                            <a href="<?php print new moodle_url('/manage/user.php'); ?>" title="" class="btn btn-danger">Trở về trang danh sách</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php if(!empty($totalcount)){ ?>
                    <div class="table-responsive" data-pattern="priority-columns">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th class="text-center"><?php echo get_string('action'); ?></th>
                                    <th><?php echo get_string('codestudent'); ?></th>
                                    <th><?php echo get_string('schools'); ?></th>
                                    <th><?php echo get_string('class'); ?></th>
                                    <th><?php echo get_string('classgroup'); ?></th>
                                    <!-- <th>Trạng thái</th> -->
                                    
                                </tr>
                            </thead>

                            <tbody>
                                <?php $i=$page*$perpage+1; foreach ($usercount as $ev) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $i; $i++ ?> </td>
                                    <td class="text-center">
                                        <a href="<?php echo $CFG->wwwroot ?>/manage/unlock_reviews/index.php?action=active&id=<?php echo $ev->id ?>" class="mauicon"><i class="fa fa-unlock"></i></a>
                                    </td>
                                    <td><a href="<?php echo new moodle_url('/manage/people/profile.php', array('id' => $ev->id)); ?>" style="color: #000">
                                        <?php echo $ev->lastname.' '.$ev->firstname ?></a>
                                    </td>
                                    <?php $idschool = get_info_class($ev->groupid) ?>
                                    <td><?php echo get_info_truong($idschool->id_truong)->name ?></td>
                                    <td><?php echo $ev->name ?></td>
                                    <td><?php echo $ev->fullname ?></td>
                                    
                                    

                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="active">
                                    <td colspan="9">
                                        <div class="float-left">
                                            <?php
                                                if($search==""){
                                                    $url ="index.php?";
                                                }else{
                                                    $url ="index.php?search=".$search.'&';
                                                } 
                                                
                                                paginate($totalcount,$page,$perpage,$url); 
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php }else{ ?>
                        <h4 class="text-danger"><?php echo get_string('no_records'); ?> </h4>
                    <?php } ?>
                </div>
            </div>
            <!-- end content-->
     
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<style>
    .mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }
</style>
<?php
    echo $OUTPUT->footer();
?>
