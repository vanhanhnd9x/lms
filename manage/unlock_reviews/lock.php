<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;

require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');

// Require Login.
require_login();

// $PAGE->set_title('Khóa đánh giá theo nhóm lớp');
// $PAGE->set_heading('Khóa đánh giá theo nhóm lớp');
$PAGE->set_title(get_string('unlock_evaluation_student_groups'));
$PAGE->set_heading(get_string('unlock_evaluation_student_groups'));
echo $OUTPUT->header();


$action     = optional_param('action', '', PARAM_TEXT);

if($action=='lock_evaluation'){
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	lock_evaluation_course($courseid);
	echo displayJsAlert('Đã khóa tất cả đánh giá của học sinh!',$CFG->wwwroot . "/manage/unlock_reviews/lock.php");
	 
}
if($action=='unlock_evaluation'){
	$courseid = optional_param('courseid', '', PARAM_TEXT);
	global $DB;
 	$sql = "UPDATE evalucation_student SET view=0 where courseid={$courseid}";
  	$DB->execute($sql);

	echo displayJsAlert(get_string('success'),$CFG->wwwroot . "/manage/unlock_reviews/lock.php");
	 
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='unlock_evaluation_student';
$name1='unlock_evaluation_student_groups';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
        	<!-- <form action="<?php echo $CFG->wwwroot ?>/manage/unlock_reviews/lock.php?action=lock_evaluation" method="POST" accept-charset="utf-8"> -->
        	<form action="<?php echo $CFG->wwwroot ?>/manage/unlock_reviews/lock.php?action=unlock_evaluation" method="POST" accept-charset="utf-8">
        		<div class="row">
	        		<div class="form-group col-md-6">
						<label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
						<select name="schoolid" id="schoolid" class="form-control" required>
							<option value=""><?php echo get_string('schools'); ?></option>
							<?php 
							$schools = get_all_shool();
							foreach ($schools as $key => $value) {
								?>
								<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
								<?php 
							}
							?>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label class="control-label"><?php echo get_string('block_student'); ?> <span style="color:red">*</span></label>
						<select name="block_student" id="block_student" class="form-control" required>
							<option value=""><?php echo get_string('block_student'); ?></option>
						</select>
					</div>

					<div class="form-group col-md-6">
						<label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
						<select name="groupid" id="groupid" class="form-control" required>
							<option value=""><?php echo get_string('class'); ?></option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label class="control-label"><?php echo get_string('classgroup'); ?><span style="color:red">*</span></label>
						<select name="courseid" id="courseid" class="form-control" required>
							<option value=""><?php echo get_string('classgroup'); ?></option>
						</select>
					</div>
					<div class="form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Lock" class="btn btn-success" >
                        </div>
                    </div>
				</div>
        	</form>
        </div>
    </div>
</div>


<?php
    echo $OUTPUT->footer();
?>
<script>
	$('#schoolid').on('change', function() {
		var cua = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
				$("#block_student").html(data);
			});
		}
	});
	$('#block_student').on('change', function() {
		var block = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
				$("#groupid").html(data);
			});
		}
	});
	$('#groupid').on('change', function() {
		var groupid = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
				$("#courseid").html(data);
			});
		}
	});
	$('#courseid').on('change', function() {
		var courseid = this.value ;
		if (this.value) {
			$.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?courseid="+courseid,function(data){
				$("#studentid").html(data);
			});
		}
	});
</script>