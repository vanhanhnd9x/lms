<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB,$PAGE;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot.'/mod/quiz/mod_form.php'); 
require_once($CFG->dirroot.'/manage/examination/lib.php'); 

$PAGE->set_title(get_string('preparethequiz'));
$PAGE->set_heading(get_string('preparethequiz'));
echo $OUTPUT->header();
require_login(0, false);
function get_all_cat_question_by_cua_in_quiz(){
    global $CFG, $DB,$PAGE;
    $sql="SELECT id, name FROM question_categories ORDER BY id DESC";
    $data=$DB->get_records_sql($sql);
    return $data;
}
$idquiz= optional_param('idquiz', '', PARAM_TEXT);
$quiz=$DB->get_record('quiz',array('id'=>$idquiz));
$data=get_quetion_in_table_quiz_by_cua($idquiz); 
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='quiz';
$moodle2='course';
$name1='preparethequiz';
$name2='list_quiz_course';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
$check_in2=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle2,$name2);
if(empty($check_in)||empty($check_in2)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
function truncateString($str, $maxChars = 40, $holder = "...")
{
    if (strlen($str) > $maxChars) {
        return trim(substr($str, 0, $maxChars)) . $holder;
    } else {
        return $str;
    }
}
// function show_quetions_in_quiz_by_cua($id){
//  global $CFG, $DB,$PAGE;
//  $question=$DB->get_record('question',array('id'=>$id));
//  if($question){
//   echo'<td>'.$question->name.'<br>'.truncateString($question->questiontext,40,$holder='...').'</td>';
// }
// }
function get_only_grade_question_in_quiz_question_instances($idquiz,$idquestion){
   global $CFG, $DB;
   if (!empty($idquestion)) {
    $sql = "SELECT DISTINCT `quiz_question_instances`.`grade`
    FROM `quiz_question_instances` 
    WHERE `quiz_question_instances`.`question`=:idquestion
    AND `quiz_question_instances`.`quiz`=:idquiz
    ";

    $params = array('idquestion' => $idquestion,'idquiz'=>$idquiz);
    $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    return $data;
}
}
function get_only_id_question_in_quiz_question_instances($idquiz,$idquestion){
   global $CFG, $DB;
   if (!empty($idquestion)) {
    $sql = "SELECT DISTINCT `quiz_question_instances`.`id`
    FROM `quiz_question_instances` 
    WHERE `quiz_question_instances`.`question`=:idquestion
    AND `quiz_question_instances`.`quiz`=:idquiz
    ";

    $params = array('idquestion' => $idquestion,'idquiz'=>$idquiz);
    $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
    return $data;
}
}

$check=check_baiquiz_da_duoc_chamdiem_by_cua($idquiz);
$check_lambai='';
if(!empty($check)){
    $check_lambai=1;
}
?>
<div class="row">
    <div class="col-md-9">
        <div class="card-box">

            <div class="">
                <label for=""><?php echo get_string('max_score'); ?></label>
                <input type="text" id="grade_quiz" style="width: 80px;height: 30px" value="<?php echo round($quiz->grade); ?>">
                <input type="submit" id="save_grade_quiz" style="width: 80px;height: 30px;line-height: 13px;" value="Lưu trữ">
            </div>

            <div class="table-rep-plugin">
                <!-- <div class="table-responsive" data-pattern="priority-columns"> -->
                    <table id="tech-companies-1" class="table  table-striped table-hover">
                        <thead>
                            <tr>
                                <th data-priority="6">STT</th>
                                <th data-priority="6"><?php echo get_string('name_ques'); ?></th>
                                <th  data-priority="6"><?php echo get_string('score'); ?></th>
                                <?php if(empty($check)) echo' <th class="text-right">'.get_string('action').'</th>'; ?>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (!empty($data)) {
                                # code...
                                $allq=explode(',', $data);
                                $n=count($allq);
                                $i=0;
                                foreach ($allq as $key=>$value) { 
                                    if($value>0){
                                        $i++;
                                        $grade=get_only_grade_question_in_quiz_question_instances($idquiz,$value);
                                        $quiz_question_instances=get_only_id_question_in_quiz_question_instances($idquiz,$value);
                                        
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <?php 
                                            //echo show_quetions_in_quiz_by_cua($value); 
                                            $question=$DB->get_record('question',array('id'=>$value));
                                            if($question){
                                                echo'<td>'.$question->name.'<br>'.truncateString($question->questiontext,40,$holder='...').'</td>';
                                                ?>
                                                <td>
                                                    <div class="diem_question">
                                                        <input type="text" value="<?php echo  round($grade); ?>" style="width: 80px;height: 30px" id="grade_question_<?php echo $i;?>">
                                                        <input type="submit" style="width: 50px;height: 30px;line-height: 12px;" value="Lưu" id="update_question_in_quiz<?php echo $i;?>">
                                                    </div>
                                                    
                                                </td>
                                                <script>
                                                    $("#update_question_in_quiz<?php echo $i;?>").click(function(){
                                                       var grade_question = $("#grade_question_<?php echo $i;?>").val();
                                                       var id_quiz_question="<?php echo $idquiz; ?>";
                                                       var quiz_question_instances="<?php echo $quiz_question_instances; ?>";
                                                       var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz="+id_quiz_question+"";
                                                       var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
                                                       if(grade_question){
                                                        $.ajax({
                                                            url:  urladd,
                                                            type: "post",
                                                            data: {quiz_question_instances:quiz_question_instances,grade_question:grade_question,id_quiz_question:id_quiz_question} ,
                                                            success: function (response) {
                                                                window.location=urlweb;
                                                            }
                                                        });
                                                    }
                                                });
                                            </script>
                                            <?php 
                                        }
                                        ?>
                                        <?php 
                                            if(empty($check)):
                                        ?>
                                        <td class="text-right">
                                            <?php 
                                            if($key>0){

                                                ?>

                                                <a  href="javascript:void(0)" class="btn btn-info" title="vị trí len<?php echo $key-1; ?>" onclick="update_sort_up_question_in_quiz_ajax(<?php echo $key-1; ?>,<?php echo $key; ?>,<?php echo $value; ?>)">
                                                   <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                               </a>
                                               <?php 
                                           } 
                                           if($key<$n-2){
                                            ?>
                                            <a  href="javascript:void(0)" class="btn btn-info" onclick="update_sort_down_question_in_quiz_ajax(<?php echo $key+1; ?>,<?php echo $key; ?>,<?php echo $value; ?>)"  title=" vị trí xuong<?php echo $key+1; ?>">
                                                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                            </a>
                                            <?php 
                                        } 
                                        ?>
                                        <a  href="javascript:void(0)"  class="btn btn-danger" title="Xóa" onclick="delete_question_from_quiz_ajax('<?php echo $key; ?>','<?php echo $value; ?>');">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                <?php endif; ?>
                                </tr>
                                <?php 
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>

            <!-- </div> -->
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
</div>
<!-- end col-md-8 -->
<div class="col-md-3">
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <label for="" class="control-label"><?php echo get_string('question_categories'); ?></label>
                <select name="question_cat" id="question_cat" class="form-control">
                    <?php 
                    if (function_exists('get_all_cat_question_by_cua_in_quiz')) {
                        $data=get_all_cat_question_by_cua_in_quiz();
                        foreach ($data as $key => $value) {
                            echo'<option value="'.$value->id.'">'.$value->name.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-12">
                <label for="" class="control-label"><?php echo get_string('name_ques'); ?></label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="col-md-12">
                <label for="" class="control-label"><?php echo get_string('list_ques'); ?></label>
                <ul class="list-unstyled" id="">
                    <div id="return_question"></div>
                </ul>
            </div>
        </div>
    </div> 
</div>
</div>
<div class="" id="kequa"></div>
<style>
.cua{
    padding: 10px;
    border-bottom: 1px solid #f3f6f8;
    box-shadow: 2px 2px #f3f6f8;
    margin:2px auto;
    width: auto;
    overflow: hidden;
}
.cua a{
    text-decoration: none;
    color:black;
}
.icon_cua{
    color:#106c37;
}
</style>
<!-- end row -->


<?php 
echo $OUTPUT->footer();
?>
<script>
    var question_cat = $('#question_cat').find(":selected").val();
    var check_lambai="<?php echo $check_lambai; ?>";
    if(question_cat){
        $.get("<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php?cat="+question_cat+"&name=&check_lambai="+check_lambai,function(data){
            $("#return_question").html(data);
        });
        $('#name').on('change keyup', function() {
            var name = $(this).val();
            if(name){
                $.get("<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php?cat="+question_cat+"&name="+name+"&check_lambai="+check_lambai,function(data){
                    $("#return_question").html(data);
                });
            }
        });
    }
    $('#question_cat').on('change', function() {
        var question_cat = this.value ;
        if (this.value) {
            $.get("<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php?cat="+question_cat+"&name=&check_lambai="+check_lambai,function(data){
                $("#return_question").html(data);

            });
            $('#name').on('change keyup', function() {
                var name = $(this).val();
                if(name){
                    $.get("<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php?cat="+question_cat+"&name="+name+"&check_lambai="+check_lambai,function(data){
                        $("#return_question").html(data);
                    });
                }
            });
        }
    });
    function add_quetion_to_quiz_ajax(idquestion){
        var idadd= "<?php echo $idquiz; ?>";
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz="+idadd+"";
        var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        var e=confirm('<?php echo get_string('add_question_to_quiz'); ?>');
        if(e==true){
            $.ajax({
                url:  urladd,
                type: "post",
                data: {idadd:idadd,idquestion:idquestion} ,
                success: function (response) {
                    var cua= confirm(response);
                    if(cua==true){
                        window.location=urlweb;
                    }else{
                        window.location=urlweb;
                    }


                }
            });
        }
    }
    function update_sort_up_question_in_quiz_ajax(key,oldkey,value){
        var idchange= "<?php echo $idquiz; ?>";
        var change= "up";
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz="+idchange+"";
        var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        // var e=confirm('Bạn có muốn thêm câu hỏi vào bài test này không?');
        // if(e==true){
            $.ajax({
                url:  urladd,
                type: "post",
                data: {idchange:idchange,key:key,oldkey:oldkey,value:value,change:change} ,
                success: function (response) {
                 window.location=urlweb;
             }
         });
        // }
    }
    function update_sort_down_question_in_quiz_ajax(key,oldkey,value){
        var idchange= "<?php echo $idquiz; ?>";
        var change= "down";
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz="+idchange+"";
        var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        // var e=confirm('Bạn có muốn thêm câu hỏi vào bài test này không?');
        // if(e==true){
            $.ajax({
                url:  urladd,
                type: "post",
                data: {idchange:idchange,key:key,oldkey:oldkey,value:value,change:change} ,
                success: function (response) {
                 window.location=urlweb;
             }
         });
        // }
    }
    // xoa cau hoi khoi quiz
    function delete_question_from_quiz_ajax(keydel,idquestiondel){
        var idquiztodel= "<?php echo $idquiz; ?>";
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz="+idquiztodel+"";
        var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        var e=confirm('<?php echo get_string('del_question_form_quiz'); ?>');
        if(e==true){
            $.ajax({
                url:  urladd,
                type: "post",
                data: {idquiztodel:idquiztodel,keydel:keydel,idquestiondel:idquestiondel} ,
                success: function (response) {
                 window.location=urlweb;
             }
         });
        }
    }
</script>
<script>
    $("#save_grade_quiz").click(function(){
        var grade_quiz = $("#grade_quiz").val();
        var id_quiz_grade="<?php echo $idquiz; ?>";
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz="+id_quiz_grade+"";
        var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        if(grade_quiz){
            $.ajax({
                url:  urladd,
                type: "post",
                data: {id_quiz_grade:id_quiz_grade,grade_quiz:grade_quiz} ,
                success: function (response) {
                    window.location=urlweb;
                }
            });
        }
    });
</script>