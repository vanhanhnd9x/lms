<?php
require_once('../../config.php');
global $USER, $CFG, $DB;

function show_name_course_by_cua_in_quiz($id){
    global $CFG, $DB;
    if (!empty($id)&&($id>1)) {
        $sql = "
        SELECT DISTINCT `course`.`fullname`
        FROM `course` 
        JOIN `course_modules` ON `course`.`id` =`course_modules`.`course` JOIN `quiz` ON `quiz`.`id` = `course_modules`.`instance`
        WHERE `quiz`.`id`={$id}
        AND `course_modules`.`module`=13
        ";
        $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
        if($data){
            return $data;
        }else{
            return 'Chưa được gán cho nhóm';
        }

    }else{
        return 'Chưa được gán cho nhóm';
    }
}
function up_date_quiz_by_cua($idquiz,$name,$intro,$timeopen,$timeclose,$timelimit){
	global $DB;
	$record = new stdClass();
	$record->id = $idquiz;
	$record->name = $name;
	$record->intro = $intro;
	$record->timeopen = $timeopen;
	$record->timeclose = $timeclose;
	$record->timelimit = $timelimit;
	return $DB->update_record('quiz', $record, false);
}
function add_new_quiz_by_cua($name,$intro,$timeopen,$timeclose,$timelimit,$course){
	global $CFG, $DB,$PAGE;
	$record = new stdClass();

	$record->name = trim($name) ;
	$record->intro = trim($intro);
	$record->timeopen = isset($timeopen)?$timeopen:0;
	$record->timeclose = isset($timeclose)?$timeclose:0;
	$record->timelimit = isset($timelimit)?$timelimit:0;

	$record->course = isset($course)?$course:1;
	$record->questions = 0;
	$newid = $DB->insert_record('quiz', $record);
	return $newid;
}
// lay diem cua question
function get_grade_question_by_cua($idquestion){
    global $CFG, $DB;
    if (!empty($idquestion)) {
        $sql = "SELECT `question`.`defaultmark`
        FROM `question` 
        WHERE `question`.`id`=:idquestion";
        $params = array('idquestion' => $idquestion);
        $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
        return $data;
    }
}
// funtion lay truong question trong quiz
function get_quetion_in_table_quiz_by_cua($idquiz){
     global $DB;
     if (!empty($idquiz)) {
        $sql = "SELECT `quiz`.`questions`
        FROM `quiz` 
        WHERE `quiz`.`id`=:idquiz";
        $params = array('idquiz' => $idquiz);
        $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
        return $data;
    }
}
function get_only_id_section_course_by_cua($idcourse){
    global $CFG, $DB;
    if (!empty($idcourse)) {
        $sql = "SELECT DISTINCT `course_sections`.`id`
        FROM `course_sections` 
        WHERE `course_sections`.`course`=:idcourse";
        $params = array('idcourse' => $idcourse);
        $data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
        return $data;
    }
}

function lay_id_course_theo_hoc_sinh($studentid){
    global $DB;
    $sql="
    SELECT `course`.`id` 
        FROM `user`
        JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
        JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
        JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
        WHERE `user`.`id`={$studentid}
    ";
     $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
    return $data;
}

function check_baiquiz_da_duoc_chamdiem_by_cua($idquiz){
    global $DB;
    $sql = "SELECT DISTINCT `quiz_attempts`.`id`
    FROM `quiz_attempts` 
    WHERE `quiz_attempts`.`quiz`={$idquiz}
    AND `quiz_attempts`.`sumgrades` IS NOT NULL
    ORDER BY `quiz_attempts`.`id`, `quiz_attempts`.`attempt` DESC
    ";
    $data=$DB->get_records_sql($sql);
    return $data;
}
function ds_nhom_thuoc_giaovien_examination_cua($roleid,$userid,$page,$number,$name=null){
  global $DB;
  if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
  }
  $sql=" SELECT `course`.`fullname`, `course`.`id`,`schools`.`name`
  FROM `user` 
  JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
  JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
  JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
  JOIN `group_course` on `group_course`.`course_id`=`course`.`id`
  JOIN `groups` ON `groups`.`id` = `group_course`.`group_id`
  JOIN `schools` ON `groups`.`id_truong`= `schools`.`id`
  JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
  WHERE `user`.`id`={$userid} 
  ";
  $sql_count=" SELECT COUNT( DISTINCT `course`.`id` )
  FROM `user` 
  JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
  JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
  JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
  JOIN `group_course` on `group_course`.`course_id`=`course`.`id`
  JOIN `groups` ON `groups`.`id` = `group_course`.`group_id`
  JOIN `schools` ON `groups`.`id_truong`= `schools`.`id`
  JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
  WHERE `user`.`id`={$userid}
  ";

  if($roleid==8){
    $sql.=" AND `role_assignments`.`roleid`=8";
  }
  if($roleid==10){
    $sql.=" AND `role_assignments`.`roleid`=10";
  }
  $sql .= " ORDER BY `course`.`fullname` DESC LIMIT {$start} , {$number} ";
  $data=$DB->get_records_sql($sql);
  $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
  $total_page=ceil((int)$total_row / (int)$number);
  $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
}
function list_quiz_course_inexmiantion($courseid,$page,$number){
  global $DB;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="SELECT `quiz`.`id`,`quiz`.`name`, `quiz`.`course`,`course_modules`.`id` as cmid
   FROM `quiz` 
   JOIN `course_modules` ON `quiz`.`id` = `course_modules`.`instance` 
   JOIN `course` ON `course`.`id` = `course_modules`.`course` 
   WHERE `course`.`id`={$courseid} AND `course_modules`.`module`=13 
   ORDER BY `quiz`.`id` DESC LIMIT {$start}, {$number}
  ";
  $sql_total=" SELECT COUNT( DISTINCT `quiz`.`id` )
     FROM `quiz` 
    JOIN `course_modules` ON `quiz`.`id` = `course_modules`.`instance` 
    JOIN `course` ON `course`.`id` = `course_modules`.`course` 
    WHERE `course`.`id`={$courseid} AND `course_modules`.`module`=13 
  ";
  $data=$DB->get_records_sql($sql);

  $total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
  $total_page=ceil((int)$total_row / (int)$number);

    // $total_row=$DB->get_records_sql($sql_total);
    // foreach ($total_row as $key => $value) {
    //             # code...
    //     $total_page=ceil((int)$key / (int)$number);
    //     break;
    // }
  $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
  );
  return $kq;
}
function get_all_quiz_by_cua($page,$number,$name=null,$schoolid=null,$block_student=null,$groupid=null,$courseid=null){
    global $CFG, $DB,$PAGE;
    if ($page>1) {
        $start=(($page-1)*$number);
    }else{
        $start=0;
    }
    // $sql="SELECT * FROM `quiz`   ";
    $sql="
    SELECT `quiz`.*,`course`.`fullname`,`schools`.`name` as schoolname FROM `quiz` 
    JOIN `course_modules` ON `quiz`.`id` = `course_modules`.`instance` 
    JOIN `course` ON `course`.`id` = `course_modules`.`course` 
    JOIN `group_course` ON `group_course`.`course_id`= `course`.`id` 
    JOIN `groups` ON `group_course`.`group_id`= `groups`.`id` 
    JOIN `schools` ON `groups`.`id_truong`= `schools`.`id`
    WHERE `course_modules`.`module`=13 AND `schools`.`del`=0
     ";
    $sql_total="SELECT COUNT( DISTINCT `quiz`.`id`) as total FROM `quiz` 
    JOIN `course_modules` ON `quiz`.`id` = `course_modules`.`instance` 
    JOIN `course` ON `course`.`id` = `course_modules`.`course` 
    JOIN `group_course` ON `group_course`.`course_id`= `course`.`id` 
    JOIN `groups` ON `group_course`.`group_id`= `groups`.`id` 
    JOIN `schools` ON `groups`.`id_truong`= `schools`.`id`
    WHERE `course_modules`.`module`=13 AND `schools`.`del`=0";
    if(!empty($schoolid)){
        $sql.=" AND `groups`.`id_truong` ={$schoolid}";
        $sql_total.="  AND  `groups`.`id_truong`={$schoolid} ";
    }
    if(!empty($block_student)){
        $sql.=" AND `groups`.`id_khoi` ={$block_student}";
        $sql_total.="  AND  `groups`.`id_khoi` ={$block_student} ";
    }
    if(!empty($groupid)){
        $sql.=" AND `groups`.`id` ={$groupid}";
        $sql_total.="  AND  `groups`.`id` ={$groupid} ";
    }
    if(!empty($courseid)){
        $sql.=" AND `course`.`id` ={$courseid}";
        $sql_total.="  AND  `course`.`id` ={$courseid} ";
    }
    if(!empty($name)){
        $sql.=" AND `quiz`.`name` LIKE '%{$name}%'";
        $sql_total.="  AND  `quiz`.`name` LIKE '%{$name}%' ";

    }
    $sql.=" ORDER BY id DESC LIMIT {$start}, {$number}";
    $data=$DB->get_records_sql($sql);

    $total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
    $total_page=ceil((int)$total_row / (int)$number);

    // $total_row=$DB->get_records_sql($sql_total);
    // foreach ($total_row as $key => $value) {
    //             # code...
    //     $total_page=ceil((int)$key / (int)$number);
    //     break;
    // }
    $kq=array(
        'data'=>$data,
        'total_page'=>$total_page 
    );
    return $kq;
}
function hien_thi_diem_chuan_hs_examination($quizid,$diem){
  global $DB;
  $diemchuan=$diem;
  if($diem>0){
    $quiz=$DB->get_record('quiz',array('id'=>$quizid));
    if($quiz->grade < $quiz->sumgrades){
      $diemchuan= $diem-($diem/$quiz->sumgrades);
    }
     if($quiz->grade > $quiz->sumgrades){
      $diemchuan=$diem+($diem/$quiz->sumgrades);
    }
  }
  
  return round($diemchuan, 2);
}
?>