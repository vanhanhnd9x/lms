<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB,$PAGE;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot.'/mod/quiz/mod_form.php'); 
require_once($CFG->dirroot.'/manage/examination/lib.php'); 
require_once($CFG->dirroot.'/manage/block-student/lib.php'); 
$PAGE->set_title(get_string('quiz'));
echo $OUTPUT->header();
require_login(0, false);

$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$action = optional_param('action', '', PARAM_TEXT);
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$block_student = optional_param('block_student', '', PARAM_TEXT);
$groupid = optional_param('groupid', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$name=trim(optional_param('name', '', PARAM_TEXT));
if($action=='search'){
    $url=$CFG->wwwroot.'?schoolid'.$schoolid.'=&block_student='.$block_student.'&groupid'.$groupid.'=&courseid='.$courseid.'&name='.$name.'&action=search&page=';
}else{
    $url=$CFG->wwwroot.'/manage/examination/index.php?page=';
}

$data=get_all_quiz_by_cua($page,$number,$name,$schoolid,$block_student,$groupid,$courseid);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='quiz';
$name1='quiz';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                    <div class="col-md-12 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <div class="col-4">
                                <label for=""><?php echo get_string('schools'); ?> </label>
                                <select name="schoolid" id="schoolid" class="form-control">
                                    <option value=""><?php echo get_string('schools'); ?></option>
                                    <?php 
                                       $schools = get_all_shool();
                                       foreach ($schools as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value->id; ?>" <?php if (!empty($schoolid)&&$schoolid==$value->id) { echo'selected'; } ?>><?php echo $value->name; ?></option>
                                        <?php 
                                      }
                                    ?>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('block_student'); ?></label>
                                <select name="block_student" id="block_student" class="form-control">
                                    <option value=""><?php echo get_string('block_student'); ?></option>
                                </select>
                            </div>
                            <div class="col-4">
                                <label for=""><?php echo get_string('class'); ?></label>
                                <select name="groupid" id="groupid" class="form-control">
                                    <option value=""><?php echo get_string('class'); ?></option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('classgroup'); ?> </label>
                                <select name="courseid" id="courseid" class="form-control" >
                                    <option value=""><?php echo get_string('classgroup'); ?></option>
                                </select>
                            </div>
                            <div class="col-4">
                                 <label class="control-label"><?php echo get_string('name_quiz'); ?> </label>
                                <input type="" name="name" class="form-control" placeholder="<?php echo get_string('name_quiz'); ?>" value="<?php echo trim($_GET['name']); ?>">
                                <input type="" name="action" class="form-control" placeholder="<?php echo get_string('name_quiz'); ?>"  value="search" hidden="">
                            </div>
                            <div class="col">
                                <div style="    margin-top: 29px;"></div>
                                <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive" data-pattern="priority-columns">
                    <table id="tech-companies-1" class="table  table-striped table-hover">
                        <thead>
                            <tr>
                                <th data-priority="6">STT</th>
                                <th data-priority="6"><?php echo get_string('name_quiz'); ?></th>
                                <th data-priority="6"><?php echo get_string('classgroup'); ?></th>
                                <th data-priority="6"><?php echo get_string('schools'); ?></th>
                                <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-right">'.get_string('action').'</th>';} ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (!empty($data['data'])) {
                                # code...
                                $i=0;
                                foreach ($data['data'] as $quiz) { 
                                    $i++;
                                        $course=show_name_course_by_cua_in_quiz($quiz->id);
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $quiz->name; ?></td>
                                        <td><?php echo $quiz->fullname; ?></td>
                                        <td><?php echo $quiz->schoolname; ?></td>
                                        <?php 
                                            if(!empty($hanhdong)||!empty($checkdelete)){
                                              echo'<td class="text-right">';
                                              $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$quiz->id);
                                              if(!empty($checkdelete)){
                                               ?>
                                               <a  href="javascript:void(0)" onclick="delet_quiz(<?php echo $quiz->id;?>);" class="btn btn-danger btn-sm " title="Xóa">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                               <?php 
                                              }
                                              echo'</td>';
                                            }
                                          ?>
                                       <!--  <td class="text-right">
                                            <a href="<?php echo $CFG->wwwroot ?>/manage/examination/edit.php?id_edit=<?php echo $quiz->id ?>" class="btn btn-info" title="Chỉnh sửa">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href="<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz=<?php echo $quiz->id ?>" class="btn btn-warning" title="Thêm câu hỏi vào bài test">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </a>
                                            <a  href="javascript:void(0)" onclick="delet_quiz(<?php echo $quiz->id;?>);" class="btn btn-danger" title="Xóa">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                        </td> -->
                                    </tr>
                                <?php }
                            }
                            ?>
                        </tbody>
                    </table>
                    <?php 
                    if ($data['total_page']) {
                        if ($page > 2) { 
                            $startPage = $page - 2; 
                        } else { 
                            $startPage = 1; 
                        } 
                        if ($data['total_page'] > $page + 2) { 
                            $endPage = $page + 2; 
                        } else { 
                            $endPage =$data['total_page']; 
                        }
                    }
                    ?>
                    <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                            for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
                                ?>
                                <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
                                <?php 
                            }
                            ?>
                            <li class="page-item <?php if(($page==$cua['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
<script>
  var school = $('#schoolid').find(":selected").val();
  if (school) {
    var blockedit = "<?php echo $block_student; ?>";
    $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
      $("#block_student").html(data);
    });
    if(blockedit){
      var groupedit="<?php echo $groupid; ?>";
      // var courseedit = "<?php echo $course->id; ?>";
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
        $("#groupid").html(data);
      });
    }
    if(groupedit){
      var courseedit="<?php echo $courseid; ?>";
      if(courseedit){
       $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit+"&courseedit="+courseedit,function(data){
        $("#courseid").html(data);
      });
     }else{
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupedit,function(data){
        $("#courseid").html(data);
      });
    }
  }
}
  // ajax add
  $('#schoolid').on('change', function() {
    var cua = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?schoolid="+cua,function(data){
        $("#block_student").html(data);
        $("#groupid").html('<option value="">Chọn lớp</option>');
      });
    }
  });
  $('#block_student').on('change', function() {
    var block = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?block_student="+block,function(data){
        $("#groupid").html(data);
      });
    }
  });
  $('#groupid').on('change', function() {
    var groupid = this.value ;
    if (this.value) {
      $.get("<?php echo $CFG->wwwroot ?>/manage/schedule/load_ajax.php?groupid="+groupid,function(data){
        $("#courseid").html(data);
      });
    }
  });
</script>
<script>
    function delet_quiz(idquiz){
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/index.php";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        var e=confirm('<?php echo get_string('del_quiz'); ?>');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {idquiz:idquiz} ,
                success: function (response) {
                   window.location=urlweb;
               }
           });
        }
    }
</script>
<?php 
echo $OUTPUT->footer();
?>
