<?php 
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB,$PAGE;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot.'/mod/quiz/mod_form.php'); 
require_once($CFG->dirroot.'/mod/quiz/lib.php'); 
require_once($CFG->dirroot.'/manage/examination/lib.php'); 
$PAGE->set_title(get_string('add_quiz'));

echo $OUTPUT->header();
require_login(0, false);
$mform = new mod_quiz_mod_form();
$action = optional_param('action', '', PARAM_TEXT);
$course = 0;
// $course = optional_param('course', '', PARAM_TEXT);
$name =trim( optional_param('name', '', PARAM_TEXT));
$timelimit = optional_param('timelimit', '', PARAM_TEXT);
$timeopen = optional_param('timeopen', '', PARAM_TEXT);
$timeclose = optional_param('timeclose', '', PARAM_TEXT);
$intro = trim( optional_param('intro', '', PARAM_TEXT));
$error='';

if($action=='add_quiz'){
	list($day1, $month1, $year1) = explode('/', $timeopen[0]);
	list($hours1,$minute1)= explode(':', $timeopen[1]);
	$time1= mktime($hours1, $minute1, 0, $month1, $day1, $year1);

	list($day2, $month2, $year2) = explode('/', $timeclose[0]);
	list($hours2,$minute2)= explode(':', $timeclose[1]);
	$time2= mktime($hours2, $minute2, 0, $month2, $day2, $year2);
	if(!empty($timeopen)&&!empty($timeclose)){
		if($time2<$time1){
			$error=get_string('errortimequiz');
		}else{
			$newid=add_new_quiz_by_cua($name,$intro,$time1,$time2,$timelimit,$course);
		}
	}elseif(!empty($timeopen)&&empty($timeclose)){
		$newid=add_new_quiz_by_cua($name,$intro,$time1,$timeclose,$timelimit,$course);
	}elseif(empty($timeopen)&&!empty($timeclose)){
		$newid=add_new_quiz_by_cua($name,$intro,$timeopen,$time2,$timelimit,$course);
	}elseif(empty($timeopen)&&empty($timeclose)){
		$newid=add_new_quiz_by_cua($name,$intro,$timeopen,$timeclose,$timelimit,$course);
	}
	if($newid){
		echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/examination/index.php");
	}else{
		echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/examination/addnew.php");
	}
}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" onsubmit="return validate();" method="post">
					<div class="row">
						<input type="text" name="action" value="add_quiz" hidden="">
						<input type="text" name="course" value="1" hidden="">
						<div class="col-md-6">
							<label class="control-label"><?php echo get_string('name_quiz'); ?> <span style="color:red">*</span></label>
							<input type="text" class="form-control" name="name" required="" placeholder="Tên quiz" value="<?php echo $name ; ?>">
						</div>
						<div class="col-md-6">
							<label for="" class="control-label"><?php echo get_string('time_quiz'); ?></label>
							<div class="form-group">
								<input type='number' class="form-control" name="timelimit" placeholder="Thời gian làm bài" value="<?php echo $timelimit ; ?>" />
							</div>
						</div>
						<div class="col-md-12"><?php if(!empty($error)){echo'<div class="alert-danger">'.$error.'</div>';} ?></div>
						<div class="col-md-6">
							<label for="" class="control-label"><?php echo get_string('time_open'); ?> <span style="color:red">*</span></label> 
							<input type="checkbox" id="mo_timeopen" onclick="check_checked_time_opent()" <?php if(!empty($timeopen))echo'checked' ?>> <?php echo get_string('open'); ?>
							<div class="input-group date" id="date_open" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input disabledCheckboxes" data-target="#date_open" name="timeopen[]" placeholder="dd/mm/yyyy" disabled  required="" value="<?php echo $timeopen[0] ; ?>"/>
								<div class="input-group-append" data-target="#date_open" data-toggle="datetimepicker">
									<div class="input-group-text" st><i class="fa fa-calendar" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<label for="" class="control-label"></label>
							<div class="form-group">
								<div class="input-group date" id="time_open" data-target-input="nearest" style="margin-top: 7px;">
									<input type="text" class="form-control datetimepicker-input disabledCheckboxes" data-target="#time_open" name="timeopen[]"  placeholder="hh:mm" disabled  required="" value="<?php echo $timeopen[1] ; ?>"/>
									<div class="input-group-append" data-target="#time_open" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<label for="" class="control-label"><?php echo get_string('time_close'); ?> <span style="color:red">*</span></label>
							<input type="checkbox" id="mo_timeclose" onclick="check_checked_time_close()" <?php if(!empty($timeclose))echo'checked' ?>> <?php echo get_string('open'); ?>
							<div class="input-group date" id="date_close" data-target-input="nearest">
								<input type="text" class="form-control datetimepicker-input disabledCheckboxes2" data-target="#date_close" name="timeclose[]"  placeholder="dd/mm/yyyy"  disabled  required="" value="<?php echo $timeclose[0] ; ?>" />
								<div class="input-group-append" data-target="#date_close" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<label for="" class="control-label"></label>
							<div class="form-group">
								<div class="input-group date" id="time_close" data-target-input="nearest" style="margin-top: 7px;">
									<input type="text" class="form-control datetimepicker-input disabledCheckboxes2" data-target="#time_close" name="timeclose[]" placeholder="hh:mm"  disabled  required="" value="<?php echo $timeclose[1] ; ?>"/>
									<div class="input-group-append" data-target="#time_close" data-toggle="datetimepicker">
										<div class="input-group-text"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<label class="control-label"><?php echo get_string('info'); ?> </label>
							<textarea name="intro" id="" cols="30" rows="10" class="form-control" value="<?php echo $intro ; ?>"><?php echo $intro ; ?></textarea>
						</div>
						<br>
						<br>
						<div class="col-md-12" style="margin-top: 10px;">
							<input type="submit" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> " id="submitBtn">
							<?php print_r(get_string('or')) ?>
							<a href="<?php echo $CFG->wwwroot ?>/manage/schedule/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
<?php
echo $OUTPUT->footer();

?>
<script type="text/javascript">
	$(function () {
		$('#date_close').datetimepicker({
			// format: 'LT',
			format:'DD/MM/YYYY',

		});
		$('#time_close').datetimepicker({
			// format: 'LT',
			format: 'HH:mm',
		});
		$('#date_open').datetimepicker({
			format:'DD/MM/YYYY',

		});
		$('#time_open').datetimepicker({
			format: 'HH:mm',

		});
	});
</script>
<script>
	var checkBox = document.getElementById("mo_timeopen");
	if (checkBox.checked == true){
		$('.disabledCheckboxes').prop("disabled", false);
	}
	var checkBox2 = document.getElementById("mo_timeclose");
	if (checkBox2.checked == true){
		$('.disabledCheckboxes2').prop("disabled", false);
	}
	function check_checked_time_opent() {
		var checkBox = document.getElementById("mo_timeopen");
		if (checkBox.checked == true){
			$('.disabledCheckboxes').prop("disabled", false);
		} else {
			$('.disabledCheckboxes').prop("disabled", true);
		}
	}
	function check_checked_time_close() {
		var checkBox2 = document.getElementById("mo_timeclose");
		if (checkBox2.checked == true){
			$('.disabledCheckboxes2').prop("disabled", false);
		} else {
			$('.disabledCheckboxes2').prop("disabled", true);
		}
	}
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />