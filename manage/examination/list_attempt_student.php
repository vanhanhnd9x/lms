<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG,$DB;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
require_once($CFG->dirroot . '/manage/examination/lib.php');


require_login(0, false);
$PAGE->set_title('Tham dự bài quiz của học sinh');
echo $OUTPUT->header();
$action = optional_param('action', '', PARAM_TEXT);
$userid = optional_param('userid', '', PARAM_TEXT);
$cmid = optional_param('cmid', '', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);
$courseid = optional_param('courseid', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$cm=$DB->get_record('course_modules',array('id'=>$cmid,'module'=>13));
$quiz=$DB->get_record('quiz',array('id'=>$cm->instance));
$course=$DB->get_record('course',array('id'=>$courseid));
$student=$DB->get_record('user', array(
  'id' => $userid
));
$course=$DB->get_record('course', array(
  'id' => $courseid
));
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
if(empty($userid)||empty($cmid)){
    echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/examination/list_grade_student.php?id=".$courseid);
}

function get_list_attempt_student_examination($page,$number,$userid,$quiz){
  global $DB;
  if ($page>1) {
    $start=(($page-1)*$number);
  }else{
    $start=0;
  }
  $sql="
  SELECT `quiz_attempts`.*
  FROM `quiz` 
  JOIN `quiz_attempts` ON `quiz`.`id`=`quiz_attempts`.`quiz` 
  JOIN `question_attempts` ON `quiz_attempts`.`uniqueid` =`question_attempts`.`questionusageid` 
  WHERE `quiz_attempts`.`userid`={$userid} 
  AND `quiz_attempts`.`quiz`={$quiz}
  ORDER BY `quiz_attempts`.`id` 
  DESC LIMIT {$start} , {$number}

  ";
  $sql_count .= " SELECT COUNT(DISTINCT `quiz_attempts`.`id`) FROM `quiz` 
  JOIN `quiz_attempts` ON `quiz`.`id`=`quiz_attempts`.`quiz` 
  JOIN `question_attempts` ON `quiz_attempts`.`uniqueid` =`question_attempts`.`questionusageid` 
  WHERE `quiz_attempts`.`userid`={$userid} 
  AND `quiz_attempts`.`quiz`={$quiz}";
  $data=$DB->get_records_sql($sql);

  $total_row=$DB->get_field_sql($sql_count,null,$strictness=IGNORE_MISSING);
  $total_page=ceil((int)$total_row / (int)$number);
  $kq=array(
    'data'=>$data,
    'total_page'=>$total_page 
  );
  return $kq;
}
$data=get_list_attempt_student_examination($page,$number,$userid,$quiz->id);
$questionid=explode (',', $quiz->questions);
array_pop($questionid);

function get_slot_question_in_quiz_attempts($question,$questionusageid){
  global $DB;
  $sql="SELECT  DISTINCT `question_attempts`.`slot`  FROM `question_attempts`  WHERE `question_attempts`.`questionusageid`={$questionusageid} AND `question_attempts`.`questionid`={$question}";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
function check_cauhoidcchamtudong($question,$questionusageid){
  global $DB;
  $sql="SELECT DISTINCT `question_attempt_steps`.`fraction` FROM `question_attempts` JOIN `question_attempt_steps` ON `question_attempts`.`id`= `question_attempt_steps`.`questionattemptid` WHERE `question_attempts`.`questionusageid`={$questionusageid} AND `question_attempts`.`questionid`={$question} AND `question_attempt_steps`.`state`='gradedright'";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
// function check_cauhoiduocgiaoviencham($question,$questionusageid){
//   global $DB;
//   $sql="SELECT DISTINCT `question_attempt_steps`.`id` FROM `question_attempts` JOIN `question_attempt_steps` ON `question_attempts`.`id`= `question_attempt_steps`.`questionattemptid` WHERE `question_attempts`.`questionusageid`={$questionusageid} AND `question_attempts`.`questionid`={$question} AND `question_attempt_steps`.`state`='mangrpartial'";
//   $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
//   return $data1;
// }
function lay_diem_giao_vien_cham($idquestion_attempt_steps){
  global $DB;
  $sql="SELECT DISTINCT `question_attempt_step_data`.`value` FROM `question_attempt_step_data` JOIN `question_attempt_steps` ON `question_attempt_step_data`.`attemptstepid`= `question_attempt_steps`.`id` WHERE `question_attempt_step_data`.`name`='-mark' AND `question_attempt_steps`.`id`={$idquestion_attempt_steps}";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
function lay_diem_chamtudong($idquestion_attempt_steps){
  global $DB;
  $sql="SELECT DISTINCT `question_attempt_step_data`.`value` FROM `question_attempt_step_data` JOIN `question_attempt_steps` ON `question_attempt_step_data`.`attemptstepid`= `question_attempt_steps`.`id` WHERE `question_attempt_step_data`.`name`='-finish' AND `question_attempt_steps`.`id`={$idquestion_attempt_steps}";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
function lay_diem_question_khi_duoc_gan_vao_quiz($question,$quiz){
  global $DB;
  $sql="SELECT `quiz_question_instances`.`grade` FROM `quiz_question_instances` WHERE `quiz_question_instances`.`quiz`={$quiz}
    AND `quiz_question_instances`.`question`={$question}";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
function lay_max_question_attempt_stepsid($q,$uniqueid){
  global $DB;
  $sql="SELECT MAX(`question_attempt_steps`.`id`)
  FROM `question_attempt_steps` 
  JOIN `question_attempts` ON `question_attempts`.`id` =`question_attempt_steps`.`questionattemptid`
  JOIN `quiz_attempts` ON `quiz_attempts`.`uniqueid`=`question_attempts`.`questionusageid`
  WHERE `quiz_attempts`.`uniqueid`={$uniqueid} AND `question_attempts`.`questionid`={$q}";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
function lay_state_question_attempt_steps($question_attempt_steps){
  global $DB;
  $sql="SELECT  `question_attempt_steps`.`state`
  FROM `question_attempt_steps`
  WHERE `question_attempt_steps`.`id`={$question_attempt_steps}";
  $data1=$DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING); 
  return $data1;
}
function hien_thi_diem_cua_1_question_trong_quiz($question,$uniqueid,$quiz){
  global $DB;
  $diem=lay_diem_question_khi_duoc_gan_vao_quiz($question,$quiz);
  $question_attempt_stepsid=lay_max_question_attempt_stepsid($question,$uniqueid);
  $status=lay_state_question_attempt_steps($question_attempt_stepsid);
  $diem1=0;
  if(($status=='mangrpartial')||($status=='mangrright')){
    $diem1=lay_diem_giao_vien_cham($question_attempt_stepsid);
  }
  if($status=='gradedright'){
    $checkauto=check_cauhoidcchamtudong($question,$uniqueid);
    $diem1=$diem*(int)$checkauto;
    // $diem1=lay_diem_chamtudong($question_attempt_stepsid);
  }
  return $diem1;
}
// $checkauto=check_cauhoidcchamtudong(107,117); echo $checkauto;
?>
<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
       <div class="row mr_bottom15">
          <div class="col-md-12 pull-right">
            <form action="" method="get" accept-charset="utf-8" class="form-row">
            <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
            <input type="" name="roleid" class="form-control" placeholder="Nhập tên..."  value="<?php echo $roleid; ?>" hidden="">
            <div class="col-4 form-group">
                <label for=""><?php echo get_string('student'); ?> </label>
                <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="<?php echo $student->lastname.' '.$student->firstname; ?>" disabled >
            </div>
            <div class="col-4 form-group">
                <label for=""><?php echo get_string('classgroup'); ?> </label>
                <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="<?php echo $course->fullname ; ?>" disabled >
            </div>
            <div class="col-4 form-group">
                <label for=""><?php echo get_string('quiz'); ?> </label>
                <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="<?php echo $quiz->name ; ?>" disabled >
            </div>
            
          </form>
          </div>
  </div> 
  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>STT</th>
          <th><?php echo get_string('startedon','quiz') ?></th>
          <th><?php echo get_string('timecompleted','quiz') ?></th>
          <!-- <th><?php echo get_string('timetaken','quiz') ?></th> -->
          <?php 
          foreach ($questionid as $key => $value) {
            $diem=lay_diem_question_khi_duoc_gan_vao_quiz($value,$quiz->id);
            ?>
            <th>Q<?php echo $key+1; ?>/<?php echo round($diem, 2); ?></th>
            <?php 
          } 
          ?>
          <th><?php echo get_string('score'); ?><?php echo '/'. round($quiz->grade, 2); ?></th>
          
      </thead>
      <tbody>
        <?php 
        if (!empty($data['data'])) {
          $i=0;
              foreach ($data['data'] as $value) { 
                $i++;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><a target="_blank" href="<?php echo $CFG->wwwroot;  ?>/mod/quiz/review.php?attempt=<?php echo $value->id ?>"><?php echo date('H:i d/m/Y', $value->timestart); ?></a></td>
                  <td><a target="_blank" href="<?php echo $CFG->wwwroot;  ?>/mod/quiz/review.php?attempt=<?php echo $value->id ?>"><?php echo date('H:i d/m/Y', $value->timefinish); ?></a></td>
                  <?php 
                    $tong=0;
                    foreach ($questionid as $key => $q) {
                      $slot=get_slot_question_in_quiz_attempts($q,$value->uniqueid);
                      $diem1=hien_thi_diem_cua_1_question_trong_quiz($q,$value->uniqueid,$quiz->id);
                      $tong=$tong+$diem1;
                      ?>
                      <td><a href="<?php echo $CFG->wwwroot;  ?>/mod/quiz/reviewquestion.php?attempt=<?php echo $value->id ?>&slot=<?php echo $slot; ?>" onclick="window.open(this.href,'targetWindow','toolbar=no, location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=650,height=450');return false;"> <?php echo $diem1; ?></a></td>
                      <?php 
                    } 
                  ?>
                  <td><?php echo hien_thi_diem_chuan_hs_examination($quiz->id,$tong) ; ?></td>

                </tr>
              <?php }
        }
        ?>
      </tbody>
    </table>
    
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
$action = optional_param('action', '', PARAM_TEXT);
if ($action='delete') {
    # code...
  $iddelete = optional_param('iddelete', '', PARAM_TEXT);
  $return=delete_student($iddelete);
  if($return){
    echo displayJsAlert('Xoa thanh cong', $CFG->wwwroot . "/manage/student/index.php");
  }
}
?>


