<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
require_once($CFG->dirroot . '/manage/examination/lib.php');

$PAGE->set_title(get_string('classgroup'));
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$roleid = optional_param('roleid', '', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
if(is_student()){
  echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
function dem_so_hoc_sinh_cua_nhom_examination($idcourse){
  global $DB;
  $sql="SELECT COUNT(DISTINCT `user`.`id`)
  FROM `user` 
  JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
  JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
  JOIN `course` ON `enrol`.`courseid`=`course`.`id` WHERE `course`.`id`={$idcourse}
  AND `user`.`del`=0
  ";
  $data = $DB->get_field_sql($sql, null,$strictness=IGNORE_MISSING);
  return $data;
}
$data=ds_nhom_thuoc_giaovien_examination_cua($roleid,$USER->id,$page,$number,null); 
if($action=='search'){
  $url="";
}else{
  $url="";
}
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
        <div class="row mr_bottom15">
          <!-- <div class="col-md-2">
            <a href="<?php echo new moodle_url('/manage/evaluation_student/addnew.php?idstudent='.$idstudent); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
          </div> -->
          <div class="col-md-9 pull-right">
            <form action="" method="get" accept-charset="utf-8" class="form-row">
            <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
            <input type="" name="roleid" class="form-control" placeholder="Nhập tên..."  value="<?php echo $roleid; ?>" hidden="">
            <div class="col-4 form-group">
                <label for=""><?php echo get_string('classgroup'); ?> </label>
                <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="<?php echo get_string('classgroup'); ?>">
            </div>
           
            <div class="col-4">
              <div style="    margin-top: 29px;"></div>
              <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
            </div>
          </form>
          </div>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>STT</th>
          <th><?php echo get_string('classgroup'); ?></th>
          <th><?php echo get_string('schools'); ?></th>
          <th><?php echo get_string('student'); ?></th>
          <th class="text-right"><?php echo get_string('action'); ?></th> 
      </thead>
      <tbody>
        <?php 
        if (!empty($data['data'])) {
                                # code...
          // $i=0;
            $i=($page-1)*20;
              foreach ($data['data'] as $value) { 
                $i++;

                $hs=dem_so_hoc_sinh_cua_nhom_examination($value->id);
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $value->fullname; ?></td>
                  <td><?php echo $value->name; ?></td>
                  <td><?php echo $hs; ?></td>
                  <td class="text-right">
                    <a href="<?php echo $CFG->wwwroot ?>/manage/examination/list_grade_student.php?id=<?php echo $value->id ?>" class="btn btn-dark waves-effect waves-light btn-sm tooltip-animation tooltipstered" title="XEM ĐIỂM"><i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
              <?php }
        }
        ?>
      </tbody>
    </table>
    <?php 
    if ($data['total_page']) {
      if ($page > 2) { 
        $startPage = $page - 2; 
      } else { 
        $startPage = 1; 
      } 
      if ($data['total_page'] > $page + 2) { 
        $endPage = $page + 2; 
      } else { 
        $endPage =$data['total_page']; 
      }
    }
    ?>
    <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <?php 
        for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
          ?>
          <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
          <?php 
        }
        ?>
        <li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
          <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
$action = optional_param('action', '', PARAM_TEXT);
if ($action='delete') {
    # code...
  $iddelete = optional_param('iddelete', '', PARAM_TEXT);
  $return=delete_student($iddelete);
  if($return){
    echo displayJsAlert('Xoa thanh cong', $CFG->wwwroot . "/manage/student/index.php");
  }
}
?>


