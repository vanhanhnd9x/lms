<?php 
require("../../config.php");
require("../../grade/lib.php");
require_once($CFG->dirroot.'/manage/examination/lib.php'); 
require_once($CFG->dirroot.'/lib/accesslib.php'); 
global $CFG, $DB,$PAGE;

function delete_quiz_by_cua($id){
	if($id){
		global $DB;
		$DB->delete_records_select('quiz', " id = $id ");// xóa quiz
		$DB->delete_records_select('quiz_attempts', " quiz = $id ");// xóa so lan lam bai tap cua hs vs quiz
		$DB->delete_records_select('quiz_question_instances', " quiz = $id ");// xoa moi quan he giua quiz va question
		$DB->delete_records_select('quiz_grades', " quiz = $id ");// xoa diem cua user voi bai quiz
		$DB->delete_records('course_modules', array('module'=>13,'instance'=>$id)); 
		// $DB->delete_records_select('course_modules', " instance = $id ");// xoa moi quan he giua quiz va course module
		return 1;
	}
}
if(!empty($_POST['idquiz'])){
	$iddelete=$_POST['idquiz'];
	delete_quiz_by_cua($iddelete);
}
// load cau hoi de add vao bai quiz
if(isset($_GET['cat'])&&isset($_GET['name'])){
	$cat=$_GET['cat'];
	$name=$_GET['name'];
	$check=$_GET['check_lambai'];
	$sql="SELECT id, name FROM question WHERE  `question`.`parent`=0";
	if(!empty($_GET['cat'])){
		$sql.=" AND `question`.`category` ={$cat}";
	}
	if(!empty($name)){
		$sql.=" AND `question`.`name` LIKE '%{$name}%' ";
	}
	$data=$DB->get_records_sql($sql);

	if($data){
		if(!empty($check)){
			foreach ($data as $key => $value) {
				# code...
				?>
				<li class="cua"><?php echo $value->name; ?></li>
				<?php 
			}

		}else{
			foreach ($data as $key => $value) {
				# code...
				?>
				<li class="cua"><a  href="javascript:void(0)" onclick="add_quetion_to_quiz_ajax(<?php echo $value->id;?>);"><i class="fa fa-plus icon_cua" aria-hidden="true"></i></a> <?php echo $value->name; ?></li>
				<?php 
			}
		}
		
	}
}
// lay toan bo diem cua cac cau hoi trong bai quiz và update tong diem
function update_sumgrades_trong_quiz($quizid){
	global $DB;
	$sql="SELECT `quiz_question_instances`.`grade` FROM `quiz_question_instances` WHERE `quiz_question_instances`.`quiz`={$quizid}";
	$data=$DB->get_records_sql($sql);
	$tong=0;
	foreach ($data as  $value) {
		# code...
		$tong=$tong+$value->grade;
	}
	$record = new stdClass();
	$record->id = $quizid;
	$record->sumgrades = $tong;
	return $DB->update_record('quiz', $record, false);

}
function add_question_to_quiz_by_cua($idquiz,$idquestion){
	global $DB;
    // tao moi ban ghi ket noi gua quiz va question
	$diem=get_grade_question_by_cua($idquestion);
	$record = new stdClass();
	$record->quiz = $idquiz;
	$record->question = $idquestion;
	$record->grade = round($diem);
	$newid = $DB->insert_record('quiz_question_instances', $record);
    //update quetion trong bang quiz
	$allquetion=get_quetion_in_table_quiz_by_cua($idquiz);
	$s1=explode(',', $allquetion);
	echo $n=count($s1);
	$s1[$n-1]=$idquestion;
	$s1[$n]=0;
	$s3=implode(',',$s1);
	global $DB;
	$cua = new stdClass();
	$cua->id = $idquiz;
	$cua->questions = $s3;
	$DB->update_record('quiz', $cua, false);

	// update sumgrades cua quiz
	update_sumgrades_trong_quiz($idquiz);

}
// thuc hien them cau hoi vao bai quiz
if(!empty($_POST['idadd'])&&!empty($_POST['idquestion'])){
	global $DB;
	$idquiz=$_POST['idadd'];
	$idquestion=$_POST['idquestion'];
	$sql = "SELECT `quiz_question_instances`.`id`
	FROM `quiz_question_instances`
	WHERE `quiz_question_instances`.`quiz` = {$idquiz}
	AND `quiz_question_instances`.`question` = {$idquestion}";
	$data=$DB->get_records_sql($sql);
	if(empty($data)){
		add_question_to_quiz_by_cua($idquiz,$idquestion);
		echo get_string('succesadd_question_to_quiz');

	}else{
		echo get_string('isset_question_to_quiz');
	}
	
}
// dua cau hoi len
function update_sort_up_question_quiz_by_cua($idquiz,$key,$value,$oldkey){
	global $DB;
	//lay toa bo cau hoi trong quiz
	$allquetion=get_quetion_in_table_quiz_by_cua($idquiz);
	// chuyen no ve mang
	$s1=explode(',', $allquetion);
	//khai bao mang thu 2
	$s2=array();
	// them moi phan tu value vao vi tri thu moi key
	array_splice( $s1, $key, 0,$value );
	$new=$oldkey+1;
	// xoa bo vi tri cu oldkey cua value
	foreach ($s1 as $key1 => $value1) {
		if($key1==$new){
		}else{
			array_push($s2, $value1);
		}

	}
	$s3=implode(',',$s2);
	global $DB;
	$cua = new stdClass();
	$cua->id = $idquiz;
	$cua->questions = $s3;
	return $DB->update_record('quiz', $cua, false);
}
// dua cau hoi xuong
function update_sort_down_question_quiz_by_cua($idquiz,$key,$value,$oldkey){
	global $DB;
	//lay toa bo cau hoi trong quiz
	$allquetion=get_quetion_in_table_quiz_by_cua($idquiz);
	// chuyen no ve mang
	$s1=explode(',', $allquetion);
	//khai bao mang thu 2
	$s2=array();
	// them moi phan tu value vao vi tri thu moi key
	array_splice( $s1, $key+1, 0,$value );
	// xoa bo vi tri cu oldkey cua value
	foreach ($s1 as $key1 => $value1) {
		if($key1==$oldkey){
		}else{
			array_push($s2, $value1);
		}

	}
	$s3=implode(',',$s2);
	global $DB;
	$cua = new stdClass();
	$cua->id = $idquiz;
	$cua->questions = $s3;
	return $DB->update_record('quiz', $cua, false);
}
// update vị trí câu hỏi trong quiz
if(!empty($_POST['change'])&&!empty($_POST['idchange'])&&!empty($_POST['value'])&&isset($_POST['key'])&&is_numeric($_POST['key'])&&isset($_POST['oldkey'])&&is_numeric($_POST['oldkey'])){
	$idquiz=$_POST['idchange'];
	$value=$_POST['value'];
	$key=$_POST['key'];
	$oldkey=$_POST['oldkey'];
	if($_POST['change']=='up'){
		// thay doi cau hoi len 
		update_sort_up_question_quiz_by_cua($idquiz,$key,$value,$oldkey);
	}elseif($_POST['change']=='down'){
		// thay doi cau hoi xuong
		update_sort_down_question_quiz_by_cua($idquiz,$key,$value,$oldkey);
	}
}
// xoa cau hoi khoi bai test
function delete_question_from_quiz_by_cua($idquiz,$key){
	global $DB;
	$allquetion=get_quetion_in_table_quiz_by_cua($idquiz);
	// chuyen no ve mang
	$s1=explode(',', $allquetion);
	//khai bao mang thu 2
	$s2=array();
	foreach ($s1 as $key1 => $value) {
		# code...
		if($key!=$key1){
			array_push($s2, $value);
		}
	}
	$s3=implode(',',$s2);
	$cua = new stdClass();
	$cua->id = $idquiz;
	$cua->questions = $s3;
	return $DB->update_record('quiz', $cua, false);
}
if(!empty($_POST['idquiztodel'])&&isset($_POST['keydel'])&&is_numeric($_POST['keydel'])&&!empty($_POST['idquestiondel'])){
	global $DB;
	$idquiz=$_POST['idquiztodel'];
	$keydel=$_POST['keydel'];
	$idquestion=$_POST['idquestiondel'];
	delete_question_from_quiz_by_cua($idquiz,$keydel);
	$DB->delete_records_select('quiz_question_instances', " question = $idquestion ");
}

// update grade quiz
if(!empty($_POST['id_quiz_grade'])&&!empty($_POST['grade_quiz'])){
	global $DB;
	$idquiz=$_POST['id_quiz_grade'];
	$grade=$_POST['grade_quiz'];
	$record = new stdClass();
	$record->id = $idquiz;
	$record->grade = $grade;
	return $DB->update_record('quiz', $record, false);
}

// update grade question quiz
if(!empty($_POST['quiz_question_instances'])&&!empty($_POST['grade_question'])&&!empty($_POST['id_quiz_question'])){
	global $DB;
	$quiz_question_instances=$_POST['quiz_question_instances'];
	$grade_question=$_POST['grade_question'];
	$record = new stdClass();
	$record->id = $quiz_question_instances;
	$record->grade = $grade_question;
	$update= $DB->update_record('quiz_question_instances', $record, false);
	if($update){
		update_sumgrades_trong_quiz($_POST['id_quiz_question']);
	}

}
if(!empty($_GET['idcourse'])&&!empty($_GET['namequiz'])){
	$name=trim($_GET['namequiz']);
	global $DB;
	$sql="SELECT `quiz`.`name`,`quiz`.`id`FROM `quiz` WHERE `quiz`.`course` IN (0,1) AND `quiz`.`name` LIKE '%{$name}%'";
	$data=$DB->get_records_sql($sql);
	if($data){
		foreach ($data as $key => $value) {
			# code...
			?>
			<li class="cua"><a  href="javascript:void(0)" onclick="add_quiz_to_course_ajax_cua(<?php echo $value->id; ?>);"><i class="fa fa-plus icon_cua" aria-hidden="true"></i></a> <?php echo $value->name; ?></li>
			<?php 
		}
	}
}

// xoa quiz khoi course
function delete_quiz_out_course_by_cua($id){
	if($id){
		global $DB;
		$DB->delete_records_select('quiz', " id = $id ");// xóa quiz
		$DB->delete_records_select('quiz_attempts', " quiz = $id ");// xóa so lan lam bai tap cua hs vs quiz
		$DB->delete_records_select('quiz_question_instances', " quiz = $id ");// xoa moi quan he giua quiz va question
		$DB->delete_records_select('quiz_grades', " quiz = $id ");// xoa diem cua user voi bai quiz
		$DB->delete_records_select('course_modules', " instance = $id ");// xoa moi quan he giua quiz va course module
		return 1;
	}
}
function update_truong_course_in_quiz($idquiz,$value){
	global $DB;
	$record = new stdClass();
	$record->id = $idquiz;
	$record->course = $value;
	return $DB->update_record('quiz', $record, false);
}
if(!empty($_POST['idquiz_course'])&&!empty($_POST['idcourse_del'])){
	global $DB;
	$idquiz=$_POST['idquiz_course'];
	$idcourse=$_POST['idcourse_del'];
	$DB->delete_records('course_modules', array('course'=>$idcourse,'module'=>13,'instance'=>$idquiz)); 
	update_truong_course_in_quiz($idquiz,1);

}
function add_course_modules_by_cua($idcourse,$idquiz,$section){
	global $DB, $USER;
	$record = new stdClass();
	$record->course = $idcourse ;
	$record->module = 13;
	$record->instance =$idquiz ;
	$record->section = $section;
	$record->completion = 1;
	$record->completionview = 1;
	$record->completionexpected = 1;
	$record->added =time() ;
	$new_id = $DB->insert_record('course_modules', $record);
	return $new_id;
}
if(!empty($_POST['idquiz_course_add'])&&!empty($_POST['idcourse_add'])){
	$idquiz=$_POST['idquiz_course_add'];
	$idcourse=$_POST['idcourse_add'];
	// lay section cua course
	$section= get_only_id_section_course_by_cua($idcourse);
	// tạo course modules
	$cmid=add_course_modules_by_cua($idcourse,$idquiz,$section);
	if($cmid){
		update_truong_course_in_quiz($idquiz,$idcourse);
		create_context(70, $cmid, $strictness = IGNORE_MISSING);
	}
	// update course trong quiz
	
}

// hien thi so lan tham gia bai quiz cua hoc sinh
function lay_so_lan_lam_bai_cua_hoc_sinh_theo_bai_quiz($userid,$quizid){
	global $DB;
	$sql="SELECT * FROM `quiz_attempts` 
	WHERE `quiz_attempts`.`userid` ={$userid} 
	AND `quiz_attempts`.`quiz` ={$quizid}";
	$data=$DB->get_records_sql($sql);
    return $data;
}
if(!empty($_POST['quizid_student'])&&!empty($_POST['student_attempt'])){
	$quizid=$_POST['quizid_student'];
	$studentid=$_POST['student_attempt'];
	$data=lay_so_lan_lam_bai_cua_hoc_sinh_theo_bai_quiz($studentid,$quizid);
	?>
	 <table id="tech-companies-1" class="table  table-striped table-hover">
	 	<th>STT</th>
	    <th>Điểm</th>
	    <th>Thời gian bắt đầu</th>
	    <th>Thời gian kết thúc</th>
	    <?php 
	    if($data){
	    	$i=0;
	    	foreach ($data as $value) {
	    		# code...
	    		$i++;
	    		?>
	    		<tr>
			        <td><?php echo $i; ?></td>
			        <td><?php if($value->sumgrades==''){ echo 'Chưa chấm điểm';}else{echo $value->sumgrades;}; ?></td>
			        <td><?php echo date('d/m/Y - H:i:s',$value->timestart); ?></td>
			        <td><?php echo date('d/m/Y - H:i:s',$value->timefinish); ?></td>
			    </tr>
	    		<?php 
	    	}
	    }
	    ?>
	    
	 </table>
	
	<?php 
}

?>