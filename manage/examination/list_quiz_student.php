 <?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB,$PAGE,$USER;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot.'/mod/quiz/mod_form.php'); 
require_once($CFG->dirroot.'/manage/examination/lib.php'); 
$PAGE->set_title(get_string('list_quiz'));
$PAGE->set_heading(get_string('list_quiz'));
echo $OUTPUT->header();
require_login(0, false);
$idcourse= optional_param('idcourse', '', PARAM_TEXT);
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
if(!is_student()){
  echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$course=lay_id_course_theo_hoc_sinh($USER->id);
function ds_bai_test_cua_nhom_by_cua($courseid,$page,$number,$name=null){
    $time=time();
    global $DB;
    if ($page>1) {
      $start=(($page-1)*$number);
    }else{
      $start=0;
    }
    $sql="SELECT DISTINCT `quiz`.`id`, `quiz`.`name`,`quiz`.`course` ,`course_modules`.`id` as cmdid, `quiz`.`timeopen`, `quiz`.`timeclose` 
    FROM `quiz` 
    JOIN `course` on `course`.`id` = `quiz`.`course` 
    JOIN `course_modules` ON `course_modules`.`instance`= `quiz`.`id` 
    WHERE `quiz`.`course`={$courseid} AND `course_modules`.`module`=13  
    
    ";
    $sql_count="
    SELECT COUNT(DISTINCT `quiz`.`id`) 
    FROM `quiz` JOIN `course` on `course`.`id` = `quiz`.`course` 
    JOIN `course_modules` ON `course_modules`.`instance`= `quiz`.`id` 
    WHERE `quiz`.`course`={$courseid} 
    AND `course_modules`.`module`=13  

    ";
    if (!empty($name)) {
        # code...
        $sql.=" AND `quiz`.`name` LIKE '%{$name}%'";
        $sql_count.=" AND `quiz`.`name` LIKE '%{$name}%'";
    }
    $sql.=" ORDER BY `quiz`.`id` DESC LIMIT {$start} , {$number} ";
    $data=$DB->get_records_sql($sql);
    $total_row=$DB->get_records_sql($sql_count);
    foreach ($total_row as $key => $value) {
      $total_page=ceil((int)$key / (int)$number);
      break;
    }
    $kq=array(
      'data'=>$data,
      'total_page'=>$total_page 
    );
    return $kq;
}
if($course){
    $cua=ds_bai_test_cua_nhom_by_cua($course,$page,$number,$name=null);
}

?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <div class="table-rep-plugin">
                <table id="tech-companies-1" class="table  table-striped table-hover">
                    <thead>
                        <th>STT</th>
                        <th><?php echo get_string('quiz'); ?></th>
                        <th>Thời gian mở</th>
                        <th>Thời gian kết thúc</th>
                        <th><?php echo get_string('action'); ?></th>
                    </thead>
                    <tbody>
                        <?php 
                            if(!empty($cua['data'])){
                                $i=0;
                                foreach ($cua['data'] as  $value) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php 
                                            if($value->timeopen>0){
                                                echo date('H:i d-m-Y',$value->timeopen);
                                            }else{
                                                echo 'Đang mở';
                                            }
                                        
                                         ?></td>
                                        <td><?php 
                                            if($value->timeclose>0){
                                                echo date('H:i d-m-Y',$value->timeclose);
                                            }else{
                                                echo 'Chưa đóng';
                                            }
                                        
                                         ?></td>
                                        <td>
                                            
                                            <?php 
                                            if(($value->timeopen==0 || $value->timeopen <=time())&&($value->timeclose==0 || $value->timeclose >time())){
                                                ?>
                                                <a href="<?php echo $CFG->wwwroot ?>//mod/quiz/attempt.php?id=<?php echo $value->cmdid; ?>" class="btn btn-purple waves-effect waves-light btn-sm tooltip-animation tooltipstered" title="Tham gia bài quiz">
                                                       <i class="fa fa-sign-in" aria-hidden="true"></i>
                                                </a>
                                                <?php
                                            } 
                                            ?>
                                            
                                           <!--  <a href="#myModal" data-toggle="modal" class="btn btn-dark waves-effect waves-light btn-sm tooltip-animation tooltipstered" title="Xem điểm">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a> --> 
                                           <!--  <a href="javascript:void(0);" id="<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php?quizid_student=<?php echo $value->id ?>&student_attempt=<?php echo $USER->id; ?>"   class="btn btn-dark waves-effect waves-light btn-sm tooltip-animation tooltipstered openPopup" title="Xem điểm">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a> -->
                                            <a href="javascript:void(0);" onclick="show_history_score_student(<?php echo $value->id;  ?>);"   class="btn btn-dark waves-effect waves-light btn-sm tooltip-animation tooltipstered" title="Xem điểm">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php 
                                }
                                
                            }else{
                                echo'<tr><td colspan="5">Dữ liệu trống</td></tr>';
                            }
                        ?>
                    	
                    </tbody>
                </table>

                <!-- </div> -->
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    
</div>
<div class="" id="kequa"></div>
<style>
.cua{
    padding: 10px;
    border-bottom: 1px solid #f3f6f8;
    box-shadow: 2px 2px #f3f6f8;
    margin:2px auto;
    width: auto;
    overflow: hidden;
}
.cua a{
    text-decoration: none;
    color:black;
}
.icon_cua{
    color:#106c37;
}
</style>
<!-- end row -->


<?php 
echo $OUTPUT->footer();
?>

            <div class="modal fade" id="empModal" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?php echo get_string('score'); ?></h4>
                        </div>
                        <div class="modal-body" >
                          <div id="Concua">
                              
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                  
                </div>
            </div>
 
  <script>
    function show_history_score_student(quizid_student){
        var dataurl="<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        var student_attempt="<?php echo $USER->id; ?>";
            $.ajax({
                url: dataurl,
                type: 'post',
                data: {student_attempt: student_attempt,quizid_student:quizid_student },
                success: function(data){ 
                    // Add response in Modal body
                    $('.modal-body').html(data); 

                    // Display Modal
                    $('#empModal').modal('show'); 
                }
             });
        
    }
    
  </script>