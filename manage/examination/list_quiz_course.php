<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG, $DB,$PAGE;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot.'/mod/quiz/mod_form.php'); 
require_once($CFG->dirroot.'/manage/examination/lib.php'); 

$idcourse= optional_param('idcourse', '', PARAM_TEXT);
$course=$DB->get_record('course', array(
  'id' => $idcourse
));
$title=get_string('list_quiz_course').': '.$course->fullname;
$PAGE->set_title($title);
$PAGE->set_heading($title);
echo $OUTPUT->header();
require_login(0, false);
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='course';
$name1='list_quiz_course';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$data=list_quiz_course_inexmiantion($idcourse,$page,$number);
// function lay_ds_bai_quiz_chua_dc_gan(){
//     global $DB;
//     $sql="SELECT `quiz`.`name`,`quiz`.`id`FROM `quiz` WHERE `quiz`.`course` IN (0,1)";
//     $quiz=$DB->get_records_sql($sql);
//     return $quiz;
// }
// $quiz=lay_ds_bai_quiz_chua_dc_gan(); 

?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mr_bottom15">
                        <div class="col-md-2">
                            <a href="<?php echo  $CFG->wwwroot.'/course/modedit.php?add=quiz&type=&course='.$idcourse.'&section=0&return=0'; ?>" class="btn btn-info"><?php echo get_string('new') ?></a>
                        </div>
                    <div class="col-md-9 pull-right">
                        <form action="" method="get" accept-charset="utf-8" class="form-row">
                            <div class="col-4">
                                <input type="" name="name" class="form-control" placeholder="<?php echo get_string('name_quiz'); ?>" value="<?php echo trim($_GET['name']); ?>">
                                <input type="" name="action" class="form-control" placeholder="<?php echo get_string('name_quiz'); ?>"  value="search" hidden="">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive" data-pattern="priority-columns">
                    <table id="tech-companies-1" class="table  table-striped table-hover">
                         <thead>
                            <tr>
                                <th data-priority="6">STT</th>
                                <th data-priority="6"><?php echo get_string('quiz'); ?></th>
                                <th class="text-right"><?php echo get_string('action'); ?></th>
                            </tr>
                        </thead>
                    <tbody>
                        <?php 
                        if (!empty($data['data'])) {
                            $i=0;
                            foreach ($data['data'] as $key=>$value) {
                                $i++; 
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz=<?php echo $value->id; ?>"><?php echo $value->name; ?></a></td>

                                    <td class="text-right">
                                        <a href="<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz=<?php echo $value->id; ?>" class="btn btn-dark waves-effect waves-light btn-sm tooltip-animation tooltipstered" title="Soạn thảo bài quiz">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                        <a  href="javascript:void(0)"  class="btn btn-danger waves-effect waves-light btn-sm tooltip-animation tooltipstered" title="Xóa" onclick="delete_quiz_out_coursce_ajax(<?php echo $value->id; ?>);">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php 
                            }
                        }
                        ?>
                    </tbody>
                    </table>
                    <?php 
                    if ($data['total_page']) {
                        if ($page > 2) { 
                            $startPage = $page - 2; 
                        } else { 
                            $startPage = 1; 
                        } 
                        if ($data['total_page'] > $page + 2) { 
                            $endPage = $page + 2; 
                        } else { 
                            $endPage =$data['total_page']; 
                        }
                    }
                    ?>
                    <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <?php 
                            for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
                                ?>
                                <li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
                                <?php 
                            }
                            ?>
                            <li class="page-item <?php if(($page==$cua['total_page'])) echo 'disabled'; ?>">
                                <a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- <div class="col-md-12">
        <div class="card-box">

            <div class="table-rep-plugin">
                <table id="tech-companies-1" class="table  table-striped table-hover">
                    <thead>
                        <tr>
                            <th data-priority="6">STT</th>
                            <th data-priority="6"><?php echo get_string('quiz'); ?></th>
                            <th class="text-right"><?php echo get_string('action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($data)) {
                            $i=0;
                            foreach ($data as $key=>$value) {
                                $i++; 
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo $CFG->wwwroot ?>/manage/examination/list_question_quiz.php?idquiz=<?php echo $value->id; ?>"><?php echo $value->name; ?></a></td>

                                    <td class="text-right">

                                        <a  href="javascript:void(0)"  class="btn btn-danger" title="Xóa" onclick="delete_quiz_out_coursce_ajax(<?php echo $value->id; ?>);">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php 
                            }
                        }
                        ?>
                    </tbody>
                </table>

                 </div> -->
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div> -->
    <!-- end col-md-8 -->
    <!-- <div class="col-md-4">
        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <label for="" class="control-label">Tên bài quiz</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="col-md-12">
                    <label for="" class="control-label">Danh sách bài quiz</label>
                    <ul class="list-unstyled" id="">
                        <div id="return_quiz">
                            <?php 
                            
                            
                            if($quiz){
                                foreach ($quiz as $key => $value) {
            # code...
                                    ?>
                                    <li class="cua"><a  href="javascript:void(0)" onclick="add_quiz_to_course_ajax_cua(<?php echo $value->id; ?>);"><i class="fa fa-plus icon_cua" aria-hidden="true"></i></a> <?php echo $value->name; ?></li>
                                    <?php 
                                }
                            }
                            ?>
                        </div>
                    </ul>
                </div>
            </div>
        </div> 
    </div> -->
</div>
<div class="" id="kequa"></div>
<style>
.cua{
    padding: 10px;
    border-bottom: 1px solid #f3f6f8;
    box-shadow: 2px 2px #f3f6f8;
    margin:2px auto;
    width: auto;
    overflow: hidden;
}
.cua a{
    text-decoration: none;
    color:black;
}
.icon_cua{
    color:#106c37;
}
</style>
<!-- end row -->


<?php 
echo $OUTPUT->footer();
?>
<script>
    $('#name').on('change keyup', function() {
        var name = $(this).val();
        var idcourse = "<?php echo $idcourse; ?>";
        if(name){
            $.get("<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php?idcourse="+idcourse+"&namequiz="+name,function(data){
                $("#return_quiz").html(data);
            });
        }
    });
    function delete_quiz_out_coursce_ajax(idquiz){
        var idcourse_del="<?php echo $idcourse; ?>"
        var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_quiz_course.php?idcourse="+idcourse_del+"";
        var urldelte= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
        var e=confirm('Bạn có muốn xóa bài test này khỏi nhóm lớp không?');
        if(e==true){
            $.ajax({
                url:  urldelte,
                type: "post",
                data: {idquiz:idquiz} ,
                // data: {idquiz_course:idquiz,idcourse_del:idcourse_del} ,
                success: function (response) {
                 window.location=urlweb;
             }
         });
        }
    }
    function add_quiz_to_course_ajax_cua(idquiz){
       var idcourse_add="<?php echo $idcourse; ?>"
       var urlweb= "<?php echo $CFG->wwwroot ?>/manage/examination/list_quiz_course.php?idcourse="+idcourse_add+"";
       var urladd= "<?php echo $CFG->wwwroot ?>/manage/examination/load_ajax.php";
       var e=confirm('Bạn có muốn thêm bài test vào nhóm lớp không?');
       if(e==true){
        $.ajax({
            url:  urladd,
            type: "post",
            data: {idquiz_course_add:idquiz,idcourse_add:idcourse_add} ,
            success: function (response) {
             window.location=urlweb;
         }
     });
    }
}
</script>