<?php
require("../../config.php");
require("../../grade/lib.php");
global $CFG;
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/evaluation_student/lib.php');
require_once($CFG->dirroot . '/manage/examination/lib.php');

$PAGE->set_title('Xem điểm');
echo $OUTPUT->header();
require_login(0, false);
$action = optional_param('action', '', PARAM_TEXT);
$id = optional_param('id', '', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
if(is_student()){
  echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
function lay_ds_hoc_sinh_theo_nhom_examination($courseid){
  global $DB;
  $sql=" SELECT `user`.`id`,`user`.`lastname`,`user`.`firstname`
  FROM `user`
  JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
  JOIN role_assignments ON `user`.id = `role_assignments`.`userid`
  JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
  JOIN `course` ON `enrol`.`courseid`=`course`.`id` 
  WHERE  `role_assignments`.`roleid`=5
  AND `user`.`del`=0
  ORDER BY `user`.`firstname` ASC
  ";
  $data1=$DB->get_records_sql($sql);
  return $data1;
}
function lay_ds_bai_quiz_thuoc_nhom_examination($courseid){
  global $DB;
  $sql="
  SELECT `quiz`.`id`, `quiz`.`name`,`course_modules`.`id` as cmid
  FROM `quiz`
  JOIN `course_modules` ON `quiz`.`id` = `course_modules`.`instance` 
  JOIN `course` ON `course`.`id` = `course_modules`.`course`
  WHERE `course_modules`.`module`=13
  AND  `course_modules`.`course`={$courseid}
  ";
  $data1=$DB->get_records_sql($sql);
  return $data1;
}
function lay_diem_bai_quiz_theo_hs_quiz_examination($userid,$quizid){
  global $DB;
  $sql="
  SELECT `quiz_grades`.`grade`  FROM `quiz_grades`
  WHERE `quiz_grades`.`quiz`= {$quizid}
  AND  `quiz_grades`.`userid`={$userid}
  ";
  $data=$DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  return $data;
}

$data1=lay_ds_hoc_sinh_theo_nhom_examination($id); 
$list_quiz=lay_ds_bai_quiz_thuoc_nhom_examination($id);
if(empty($id)){
    echo displayJsAlert(get_string('error'), $CFG->wwwroot . "/manage/examination/list_course_teacher.php");
}
?>

<div class="row">
  <div class="col-md-12">
    <div class="card-box">
      <div class="table-rep-plugin">
       <!--  <div class="row mr_bottom15">
          <div class="col-md-9 pull-right">
            <form action="" method="get" accept-charset="utf-8" class="form-row">
            <input type="" name="action" class="form-control" placeholder="Nhập tên..."  value="search" hidden="">
            <input type="" name="roleid" class="form-control" placeholder="Nhập tên..."  value="<?php echo $roleid; ?>" hidden="">
            <div class="col-4 form-group">
                <label for=""><?php echo get_string('classgroup'); ?> </label>
                <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="<?php echo get_string('classgroup'); ?>">
            </div>
           
            <div class="col-4">
              <div style="    margin-top: 29px;"></div>
              <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
            </div>
          </form>
          </div>
  </div> -->
  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>STT</th>
          <th><?php echo get_string('student'); ?></th>
          <?php 
          foreach($list_quiz as $quiz){
            ?>
            <th><!-- <a href="<?php  echo $CFG->wwwroot; ?>/mod/quiz/report.php?id=<?php echo $quiz->cmid; ?>&mode=overview"> --><?php echo $quiz->name; ?><!-- </a> --></th>
            <?php 
          }
          ?>
          
      </thead>
      <tbody>
        <?php 
        if (!empty($data1)) {
          $i=0;
              foreach ($data1 as $value) { 
                $i++;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $value->lastname.' '.$value->firstname; ?></td>
                  <?php 
                    foreach($list_quiz as $quiz){
                      $diem=lay_diem_bai_quiz_theo_hs_quiz_examination($value->id,$quiz->id);
                      $diemchuan=hien_thi_diem_chuan_hs_examination($quiz->id,$diem);
                      ?>
                      <td>
                        <?php
                          if($diemchuan){
                            echo get_string('score').': '.round($diem, 2);
                            echo '<br>';
                            echo '<a href="'.$CFG->wwwroot.'/manage/examination/list_attempt_student.php?userid='.$value->id.'&cmid='.$quiz->cmid.'&courseid='.$id.'"  title="">Xem số lần làm bài
                            </a>';
                          }
                        ?>
                      </td>
                      <?php 
                    }
                  ?>
                </tr>
              <?php }
        }
        ?>
      </tbody>
    </table>
    
  </div>
</div>
<!-- end content-->
</div>
<!--  end card  -->
</div>
<!-- end col-md-12 -->
</div>
<!-- end row -->
<?php
echo $OUTPUT->footer();
$action = optional_param('action', '', PARAM_TEXT);
if ($action='delete') {
    # code...
  $iddelete = optional_param('iddelete', '', PARAM_TEXT);
  $return=delete_student($iddelete);
  if($return){
    echo displayJsAlert('Xoa thanh cong', $CFG->wwwroot . "/manage/student/index.php");
  }
}
?>


