<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 * 
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package noticeboard
 * */
require("../config.php");
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
//Quang added to fix switch role to learner, when click in menu --> back to teacher role
$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
$id = optional_param('id', -1, PARAM_INT);//course id
if($current_switchrole!=-1&&$id !=-1){
$context = get_context_instance(CONTEXT_COURSE, $id);
role_switch($current_switchrole, $context);
require_login($id);

}
else{
require_login();
}

$PAGE->set_title(get_string('courselibrary2'));
$PAGE->set_heading(get_string('courselibrary2'));

echo $OUTPUT->header();
$PAGE->requires->js('/manage/manage.js');
$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/leanersearch.js');



//List My Course

global $USER;
$courses=get_courses_library();

?>
<div id="admin-fullscreen">

    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            
            <div class="item-page-list">
                <div class="panel-head">
                    <form method="get" action="">

                        <div class="field-help">
                          <label id="searchPrompt" for="searchBox"><?php print_r(get_string('searchforcourse')) ?></label>
                            <input type="text" value="" name="searchCourse" id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                               
                        </div>
                    </form>
                </div>

                <div id="searchResults">
                    <ul>

                        <?php
                        $finded = 0;
                        foreach ($courses as $key=>$course) {
                            
                            
                            
                                $finded++;                            
                            ?>
                            <li>
                                <div class="main-col">
                                    <div class="icon-course-view">
                                             <a href="<?php echo new moodle_url('/course/user/view.php', array('id' => $course->id)); ?>" class="next-arrow"></a>
                                    </div>
                                    <div class="title">
                                         <a href="<?php echo new moodle_url('/course/user/view.php', array('id' => $course->id)); ?>">
                                            <?php echo $course->fullname; ?></a>
                                        <div class="tip">
                                           <?php  echo $course->idnumber; ?>
                                        </div>
                                        
                                    </div>
                                    <div class="right-col">
                                        <?php
                                          $edit = new moodle_url('/course/view.php', array('id' => $course->id));
                                          $active = $course->visible ? 'Active' : 'Inactive'; 
                                            ?>
                                        
                                    </div>
                            </li>
                        <?php } ?>
                    </ul>
             
                  <div class="courses-found"><?php echo $finded." ".($finded>1? get_string('courses'):  get_string('course') ); ?></div>
                </div>
            </div>

        </div> 
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div id="admin-fullscreen-right">

        <div class="side-panel">


          <h3><?php print_r(get_string('recentlyviewedcourses')) ?></h3>

            <ul>
                  <?php 
                  $logs = get_logs_view_course(0, 30);
                  $logs = array_filter($logs);
                   foreach ($logs as $log){ ?>
                        <li><a href="<?php echo new moodle_url('/course/view.php', array('id' => $log->course)); ?>">
                                 <?php echo $log->fullname; ?></a></li>
                <?php }?>

            </ul>



        </div>
    </div>  <!-- End Right -->

</div>      
<?php
echo $OUTPUT->footer();
?>