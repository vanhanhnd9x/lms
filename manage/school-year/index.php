<?php
require("../../config.php");
global $CFG;

require_once($CFG->dirroot . '/common/lib.php');

// Require Login.
require_login();

$PAGE->set_title(get_string('list_school_year'));
$PAGE->set_heading(get_string('list_school_year'));
echo $OUTPUT->header();

// $sql = "SELECT * FROM user";
// $rows = $DB->get_records_sql($sql);
$page        = optional_param('page', '0', PARAM_INT);     // which page to show
$perpage     = optional_param('perpage', '30', PARAM_INT); // how many per page
$userid      = optional_param('id', 0, PARAM_INT);
$action      = optional_param('action', '', PARAM_TEXT);


if($action=='delete'){
    $id      = optional_param('id', 0, PARAM_INT);
    $DB->delete_records('school_year',array('id' => $id));
    echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/school-year/index.php");
}
$namhoc = get_namhoc();

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='schools-year';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
?>

<script src="<?php print new moodle_url('/manage/search.js'); ?>"></script>
<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                   
                        <?php 
                            if(!empty($checkthemmoi)){
                                ?>
                                    <div class="row mr_bottom15">
                                        <div class="col-md-2">
                                            <a href="<?php echo new moodle_url('/manage/school-year/addnew.php'); ?>" class="btn btn-success"><?php echo get_string('add') ?></a>
                                        </div>
                                  
                                    </div>
                                <?php
                            }
                         ?>
                    
                    <div class="table-responsive" data-pattern="priority-columns" id="searchResults">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th><?php echo get_string('school_year');?></th>
                                    <?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="text-center align-middle">'.get_string('action').'</th>';} ?>
                                </tr>
                            </thead>

                            <tbody id="page">
                                <?php $id=0; foreach ($namhoc as $val) { $i++; ?>
                                <tr>
                                    <td class="text-center"><?php echo $i ?></td>
                                    <td><?php echo $val->sy_start . ' - ' .$val->sy_end ?></td>
                                    <?php 
                                        if(!empty($hanhdong)||!empty($checkdelete)){
                                            echo'<td class="text-center">';
                                            $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$val->id);
                                            if(!empty($checkdelete)){
                                            ?>
                                            <!-- <a href="<?php print new moodle_url('?action=delete',array('id'=>$val->id)); ?>" class="mauicon"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                                            <a href="javascript:void(0)" class="mauicon" onclick="delet_school_year(<?php echo $val->id; ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            <?php 
                                            }
                                            echo'</td>';
                                        }
                                    ?>
                                   <!--  <td class="text-center">
                                        <a href="<?php print new moodle_url('/manage/school-year/edit.php',array('id'=>$val->id)); ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="<?php print new moodle_url('?action=delete',array('id'=>$val->id)); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td> -->
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end content-->
     
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->

<?php
    echo $OUTPUT->footer();
?>
<script>
function delet_school_year(iddelete) {
    var urlweb = "<?php echo $CFG->wwwroot ?>/manage/school-year/index.php";
    var action ='delete';
    var e = confirm('<?php echo get_string('delete_school_year'); ?>');
    if (e == true) {
        $.ajax({
            url: urlweb,
            type: "post",
            data: { id: iddelete,action:action },
            success: function(response) {
                window.location = urlweb;
            }
        });
    }
}
</script>