<?php
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../../config.php');
global $CFG;
global $DB;
require_once($CFG->dirroot . '/common/lib.php');
// require_once($CFG->dirroot . '/teams/config.php');

require_login();
$PAGE->set_title(get_string('addnew_school_year'));
$PAGE->set_heading(get_string('addnew_school_year'));
echo $OUTPUT->header();


$action = optional_param('action', '', PARAM_TEXT);

if ($action == 'add') {
  $yearstart = optional_param('yearstart', '', PARAM_TEXT);
  $yearend = optional_param('yearend', '', PARAM_TEXT);
  $mota = optional_param('mota', '', PARAM_TEXT);
  $check=$DB->get_record('school_year', array(
    'sy_start' => $yearstart,
    'sy_end' => $yearend,
  )); 
  if(!empty($check)){
    ?>
    <script>
      alert('<?php echo get_string('isset_school_year'); ?>');
    </script>
    <?php 
    echo displayJsAlert('', $CFG->wwwroot . "/manage/school-year/addnew.php");
  }else{
    $new_class_id = insert_school_year($yearstart,$yearend,$mota);

  // redirect($CFG->wwwroot . "/manage/class/class.php");
    echo displayJsAlert('Thêm thành công', $CFG->wwwroot . "/manage/school-year/index.php");
  }
  
}
?>


<div class="row">
  <div class="col-md-10">
    <div class="card-box">
        <form id="TypeValidation" class="" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/manage/school-year/addnew.php?action=add" method="post">
            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                  <label><?php echo get_string('start_year'); ?> <span class="text-danger">*</span>:</label>
                  <input type="text" value="" name="yearstart" maxlength="255" id="yearstart" class="form-control" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label><?php echo get_string('end_year'); ?> <span class="text-danger">*</span>:</label>
                  <input type="text" value="" name="yearend" maxlength="255" id="yearend" class="form-control" required>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                    <label><?php echo get_string('note'); ?></label>
                    <textarea name="mota" class="form-control"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <input type="submit" class="btn btn-success" value="<?php print_r(get_string('save', 'admin')) ?>">
                  <?php print_r(get_string('or')) ?>
                <a href="<?php echo $CFG->wwwroot ?>/manage/school-year/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
              </div>
            </div>  
        </form>
    </div>
  </div>
</div>

<?php 
echo $OUTPUT->footer();
?>

<script type="text/javascript">
  $('#yearstart').datepicker({
        format: "yyyy",
        autoclose: true,
        minViewMode: "years"
    }).on('changeDate', function(selected){
        startDate =  $("#yearstart").val();
        $('#yearend').datepicker('setStartDate', startDate);
    }); ;

$('#yearend').datepicker({
        format: "yyyy",
        autoclose: true,
        minViewMode: "years"
});
</script>