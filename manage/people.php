<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 *
 * @author
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}
require_once('../config.php');

global $CFG, $USER;

require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/manage/people/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
// Require Login.
require_login();


$PAGE->set_title(get_string('people'));
$PAGE->set_heading(get_string('people'));
//now the page contents
$PAGE->set_pagelayout(get_string('people'));
echo $OUTPUT->header();
$PAGE->requires->js('/manage/manage.js');
$PAGE->requires->js('/theme/nimble/jquery.js');
$PAGE->requires->js('/manage/search.js');

// tung fix: load user for admin level 1
if (!is_teacher()) {
    $sql = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
    $params['ownerid'] = $USER->id;
    $rows = $DB->get_records_sql($sql, $params);
    foreach ($rows as $row) {
        $id_list .= $row->user_id . ",";
    }
    $id_list = $id_list . "0";
    $sql2 = "SELECT user_id FROM map_user_admin WHERE owner_id in (" . $id_list . ")";
    $rows2 = $DB->get_records_sql($sql2);
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list = $id_list . $USER->id;
} else {
    $sql = "SELECT owner_id FROM map_user_admin where user_id = :userid";
    $params['userid'] = $USER->id;
    $rows = $DB->get_records_sql($sql, $params);
    foreach ($rows as $row) {
        $owner_id = $row->owner_id;
    }
    $sql2 = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
    $params['ownerid'] = $owner_id;
    $rows2 = $DB->get_records_sql($sql2, $params);
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
//    $id_list = $id_list."0";
    $id_list = $id_list . $owner_id;
}
//end fix
//List People
 $usercount = get_users(true, '', false, null, 'firstname ASC', '', '', '', '', '*', " id in (" . $id_list . ")", array(''));
?>
<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel">
        <!-- Left Content -->
        <div class="body">
            <div class="panel-head">
                <form method="get" action="">
                    <div class="field-help">
                        <label id="searchPrompt" for="searchBox"><?php print_r(get_string('searchpeople')) ?></label>
                        <input type="text" value="" name="searchPeople" id="searchBox" autocorrect="off" autocomplete="off" autocapitialize="off">
                    </div>
                </form>
            </div>
            <div id="searchResults">
                <ul>
                    <?php
                    $finded = 0;
                    foreach ($usercount as $user) {
                        $finded++;
                        ?>
                        <li>
                            <div class="main-col">
                                <div class="title">
                                    <a href="<?php echo new moodle_url('/manage/people/profile.php', array('id' => $user->id)); ?>">
                                        <span class="nav-reports"><?php echo $user->firstname . ' ' . $user->lastname; ?></span></a>
                                    <div class="tip">
                                        <?php if ((bool) filter_var($user->email, FILTER_VALIDATE_EMAIL)) print obfuscate_mailto($user->email, '') . ' | '; ?>
                                        <?php echo time_ago($user->lastlogin); ?>
                                    </div>
                                </div>
                                <div class='right-col'>
                                    <span class="box-tag box-tag-blue"><?php echo $user->company; ?></span>
                                    <?php $active = $user->confirmed ? 'Active' : 'Inactive'; ?>
                                    <span class="box-tag box-tag-yellow" title=""><?php echo people_get_roles($user->id); ?></span>
                                    <span class="box-tag <?php echo $active == 'Active' ? 'box-tag-green' : 'box-tag-grey' ?> " title=""><?php echo $active == 'Active' ? get_string('active') : get_string('inactive') ?></span>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <div class="courses-found"><?php echo $finded . " " . get_string('peoplefund') ?></div>
            </div>
        </div>
    </div><!-- Left Content -->
    <!-- Right Block -->
    <div id="admin-fullscreen-right">
        <div class="side-panel">
            <div class="action-buttons">
                <ul>
                    <?php
                    if (!is_teacher()) {
                        ?>
                        <li>
                            <a href="<?php print new moodle_url('/manage/people/new.php'); ?>" class="big-button drop">
                                <span class="left"><span class="mid"><span class="right">
                                            <span class="icon_add"> <?php print_r(get_string('addanewperson')) ?></span>
                                        </span></span></span>
                            </a>

                        </li>
                        <li><a href="<?php echo $CFG->wwwroot ?>/manage/import_people.php">↑ <?php print_r(get_string('importpeople')) ?></a></li>

                        <!--<li><a href="<?php // echo $CFG->wwwroot   ?>/manage/never_login_email.php">→ Send login emails</a></li>-->
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>  <!-- End Right -->

</div>

<?php
echo $OUTPUT->footer();
?>
