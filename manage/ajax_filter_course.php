<?php
require('../config.php');
global $CFG,$DB,$USER;
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/common/header_lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');


$page       = optional_param('page', 0, PARAM_INT);
$perpage    = optional_param('perpage', 20, PARAM_INT);
$name       = optional_param('name', '', PARAM_TEXT);
$gvnn       = optional_param('gvnn', '', PARAM_TEXT);
$gvtg       = optional_param('gvtg', '', PARAM_TEXT);
$class       = optional_param('class', '', PARAM_TEXT);
$schools    = optional_param('schools', '', PARAM_TEXT);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
// $param      =" AND fullname LIKE '%".$name."%' AND groups.name LIKE '%".$class."%' AND groups.id_truong LIKE '%".$schools."%' ";
$param='';
if(!empty($name)){
    $param.=" AND fullname LIKE '%".$name."%' ";
}
if(!empty($class)){
    $param.=" AND groups.name LIKE '%".$class."%' ";
}
if(!empty($schools)){
    $param.=" AND groups.id_truong = {$schools} ";
}
if (!empty($gvnn)) {
    $param.= " AND (GV.giao_vien LIKE '%".$gvnn."%') ";
    
}
if (!empty($gvtg)) {
    // $param .= " AND (tro_giang LIKE '%".$gvtg."%')";
    $param.= " AND (TG.tro_giang LIKE '%{$gvtg}%') ";
}
// var_dump($param);die;sa
if(in_array($roleid,array(3,13,14,15,16,17,18,19,20,21,11,12))){
    $rows       = get_list_class($page,$perpage,$param);
    $totalcount = count(get_list_class(null,$perpage,$param));
}
if(is_teacher() || is_teacher_tutors()){
    $rows       = get_list_class($page,$perpage,$param,$USER->id);
    $totalcount = count(get_list_class(null,$perpage,$param,$USER->id));
} 


if ($action == 'delete') {
    $class_id = optional_param('id', '', PARAM_TEXT);
    delete_class($class_id);
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/courses.php");
}

$moodle='course';
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);


?>




<?php $i=$page*$perpage+1;  foreach ($rows as $class) { ?>
<tr>
    <!-- <td><?php echo $i; $i++; ?></td>
    <td><?php echo $class->coefficient_name.$class->fullname ?></td>

    <?php
        $gvnn = get_info_giaovien(get_idgvnn_course($class->id));
        $gvtg = get_info_giaovien(get_idgvtg_course($class->id));
    ?>
    <td><?php echo $gvnn->firstname.' '.$gvnn->lastname ?></td>
    <td><?php echo $gvnn->email ?></td>
    <td><?php echo $gvtg->firstname.' '.$gvtg->lastname ?></td>
    <td><?php echo $gvtg->email ?></td>
    

    <?php
        $members = get_members_enroll_of_course($class->id);
    ?>
    <td class="text-center"><?php echo $class->name ?></td>
    
    <td><?php echo $class->tentruong ?></td>
    <?php $book = get_book_schedule($class->id); ?>
    <td><?php foreach ($book as $bk) {
        echo count($book)>=2 ? $bk->book . ',' : $bk->book;                                 
    } ?></td>
    <td class="text-center"><?php echo count($members) ?></td> -->
    <td><?php echo $i; $i++; ?></td>
    <td><?php echo $class->fullname ?></td>

    <?php
        $gvnn = get_info_giaovien(get_idgvnn_course($class->id));
        $gvtg = get_info_giaovien(get_idgvtg_course($class->id));
    ?>
    <td><?php echo $gvnn->lastname.' '.$gvnn->firstname; ?></td>
    <td><?php echo $gvnn->email; ?></td>
    <td><?php echo $gvnn->phone2; ?></td>
    <td><?php echo $gvtg->lastname.' '.$gvtg->firstname; ?></td>
    <td><?php echo $gvtg->email; ?></td>
    <td><?php echo $gvtg->phone2; ?></td>
                                

    <?php
        $members = get_members_enroll_of_course($class->id);
    ?>
    <td class="text-center"><?php echo $class->name ?></td>
                                
    <td><?php echo $class->tentruong ?></td>
    <?php $book = get_book_schedule($class->id); ?>
    <td><?php foreach ($book as $bk) {
        echo count($book)>=2 ? $bk->book . ',' : $bk->book;                                 
    } ?></td>
    <td class="text-center"><?php echo count($members) ?></td>
    <?php 
        if(!empty($hanhdong)||!empty($checkdelete)){
            echo'<td class="text-right">';
            $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$class->id);
            if(!empty($checkdelete)){
            ?>
            <a onclick="return Confirm('Xóa lớp học','Bạn có muốn xóa <?php echo $class->fullname ?> khỏi hệ thống?','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/settings/del_course.php?id=<?php echo $class->id ?>&action=delete')" class="btn waves-effect waves-light btn-danger btn-sm tooltip-animation" title="Xóa nhóm lớp">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
            </a>
            <?php 
            }
            echo'</td>';
        }
    ?>
</tr>

<?php } ?>
                       

<tr class="active">
    <td colspan="11">
        <div class="float-left">
            <?php
                paginate($totalcount,$page,$perpage,"#"); 
            ?>
        </div>
    </td>
</tr>

<script>
    $('a').on('click', function(event) {
        var x=$(this).attr('href');
        var page=x.replace('#page=', '');
        $.get("ajax_filter_course.php?name="+$('#name').val()+"&gvnn="+$('#gvnn').val()+"&gvtg="+$('#gvtg').val()+"&class="+$('#class').val()+"&schools="+$('#schools').val()+"&giaotrinh="+$('#giaotrinh').val(),function(data){
            $('#page').html(data);
        });
        $.get("ajax_filter_course.php?name="+$('#name').val()+"&gvnn="+$('#gvnn').val()+"&gvtg="+$('#gvtg').val()+"&class="+$('#class').val()+"&schools="+$('#schools').val()+"&giaotrinh="+$('#giaotrinh').val(),function(data){
            $('#page').html(data);
        });
    });
</script>
