<?php
require("../config.php");
require("../grade/lib.php");
global $CFG;

require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/manage/people/lib.php');
require_once($CFG->dirroot . '/common/lib.php');

// Require Login.
require_login();

$PAGE->set_title(get_string('people'));
$PAGE->set_heading(get_string('people'));
echo $OUTPUT->header();

// $PAGE->requires->js('/manage/manage.js');
// $PAGE->requires->js('/theme/nimble/jquery.js');
// $PAGE->requires->js('/manage/search.js');

// load user for admin level 1
if (!is_teacher()) {
    $sql = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
    $params['ownerid'] = $USER->id;
    $rows = $DB->get_records_sql($sql, $params);
    foreach ($rows as $row) {
        $id_list .= $row->user_id . ",";
    }
    $id_list = $id_list . "0";
    $sql2 = "SELECT user_id FROM map_user_admin WHERE owner_id in (" . $id_list . ")";
    $rows2 = $DB->get_records_sql($sql2);
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list = $id_list . $USER->id;
} else {
    $sql = "SELECT owner_id FROM map_user_admin where user_id = :userid";
    $params['userid'] = $USER->id;
    $rows = $DB->get_records_sql($sql, $params);
    foreach ($rows as $row) {
        $owner_id = $row->owner_id;
    }
    $sql2 = "SELECT user_id FROM map_user_admin where owner_id = :ownerid";
    $params['ownerid'] = $owner_id;
    $rows2 = $DB->get_records_sql($sql2, $params);
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
//    $id_list = $id_list."0";
    $id_list = $id_list . $owner_id;
}
//end fix
//List People

$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage', 15, PARAM_INT);
$search      = optional_param('search', '', PARAM_TEXT);

$usercount = get_users($page,$perpage,$search);
$totalcount = count(get_users(null,$perpage,$search));


$userid      = optional_param('id', 0, PARAM_INT);
$action      = optional_param('action', '', PARAM_TEXT);


if($action=='deactive'){
   $DB->set_field('user', 'confirmed', 0, array('id' => $userid));
   echo displayJsAlert('Tài khoản đã bị khóa', $CFG->wwwroot . "/manage/user.php"); 
}
if($action=='active'){
   $DB->set_field('user', 'confirmed', 1, array('id' => $userid));
   echo displayJsAlert('Mở khóa tài khoản thành công', $CFG->wwwroot . "/manage/user.php"); 
}
if($action=='delete'){
    $DB->delete_records('user',array('id' => $userid));
    echo displayJsAlert('Xóa thành công', $CFG->wwwroot . "/manage/user.php");
}


$userid          = (is_numeric($userid) && $userid > 0) ? $userid : $USER->id;
$user = $DB->get_record('user', array('id' => $userid));

?>

<script src="<?php print new moodle_url('/manage/search.js'); ?>"></script>
<div class="row">
    <div class="col-md-12">
            <div class="card-box">
                <div class="table-rep-plugin">
                    <div class="row mb-4">
                        <div class="col-md-10">
                            <form action="" method="get" accept-charset="utf-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="search" placeholder="Tìm kiếm..." class="form-control" value="<?php echo !empty($search) ? $search : ""; ?>">
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-custom waves-light waves-effect">Tìm kiếm</button>
                                    </div>
                                    <?php if(!empty($search)): ?>
                                        <div class="col-md-2">
                                            <a href="<?php print new moodle_url('/manage/user.php'); ?>" title="" class="btn btn-danger">Trở về trang danh sách</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php if(!empty($totalcount)){ ?>
                    <div class="table-responsive" data-pattern="priority-columns">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Họ tên</th>
                                    <th>Email</th>
                                    <th>Tài khoản</th>
                                    <th>Số điện thoại</th>
                                    <th>Trạng thái</th>
                                    <th class="text-right">Hành động</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $i=$page*$perpage+1; foreach ($usercount as $user) { ?>
                                <tr>
                                    <td><?php echo $i; $i++ ?> </td>
                                    <td><a href="<?php echo new moodle_url('/manage/people/profile.php', array('id' => $user->id)); ?>" style="color: #000">
                                        <?php echo $user->lastname.' '.$user->firstname ?></a>
                                    </td>
                                    <td><?php echo $user->email ?></td>
                                    <td><?php echo $user->username ?></td>
                                    <td><?php echo $user->phone2 ?></td>

                                    <?php $active = $user->confirmed ? 'Active' : 'Inactive'; ?>
                                    <td class="text-center">
                                        <span class="badge label-table <?php echo $active == 'Active' ? 'badge-success' : 'badge-danger' ?>">
                                            <?php echo $active == 'Active' ? get_string('active') : get_string('inactive') ?>
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        
                                        <?php if($user->id==3 || $user->id==2){ ?> 
                                            
                                        <?php }elseif($user->confirmed==1 ){ ?>
                                        <a href="<?php echo $CFG->wwwroot ?>/manage/user.php?action=deactive&id=<?php echo $user->id ?>" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>
                                        <?php }else{ ?>
                                        <a href="<?php echo $CFG->wwwroot ?>/manage/user.php?action=active&id=<?php echo $user->id ?>" class="btn btn-info btn-sm"><i class="fa fa-unlock"></i></a>
                                        <!-- <a href="<?php print new moodle_url('?action=delete',array('id'=>$user->id)); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                                        <?php } ?>
                                    </td>
                                    <!-- <td class="text-right">
                                        <a href="<?php print new moodle_url('/manage/people/edit.php',array('user_id'=>$user->id)); ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="<?php echo new moodle_url('/manage/people/profile.php', array('id' => $user->id)); ?>" class="btn btn-warning btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="<?php print new moodle_url('?action=delete',array('id'=>$user->id)); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td> -->
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="active">
                                    <td colspan="9">
                                        <div class="float-left">
                                            <?php
                                                if($search==""){
                                                    $url ="user.php?";
                                                }else{
                                                    $url ="user.php?search=".$search.'&';
                                                } 
                                                
                                                paginate($totalcount,$page,$perpage,$url); 
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php }else{ ?>
                        <h4 class="text-danger">Không có kết quả tìm kiếm cho từ khóa <b><?php echo $search ?></b></h4>
                    <?php } ?>
                </div>
            </div>
            <!-- end content-->
     
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->

<?php
    echo $OUTPUT->footer();
?>



