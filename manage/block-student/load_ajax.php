<?php 
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/student/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
global $USER, $CFG, $DB;

function lay_toan_bo_hs_cua_khoi($id_block_student){
	global $DB;
	$sql="SELECT DISTINCT `user`.`id`FROM user JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`= 5 AND `user`.`del`=0 AND `user`.`id` IN (SELECT `groups_members`.`userid` FROM `groups_members` JOIN `groups` ON `groups`.`id` = `groups_members`.`groupid` WHERE `groups`.`id_khoi` ={$id_block_student})";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function lay_toan_bo_lop_cua_khoi($id_block_student){
	global $DB;
	$sql=" SELECT `groups`.`id`
	FROM `groups`
	WHERE `groups`.`id_khoi` ={$id_block_student}
	";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function lay_toan_bo_nhom_cua_khoi($id_block_student){
	global $DB;
	$sql="SELECT DISTINCT `course`.`id` 
	FROM `course` 
	JOIN `group_course` ON `group_course`.`course_id` = `course`.`id` 
	JOIN `groups` ON `group_course`.`group_id` = `groups`.`id`
	WHERE `groups`.`id_khoi` ={$id_block_student}";
	$data=$DB->get_records_sql($sql);
	return $data;
}
function lay_enrol_cua_nhom($idnhom){
	global $DB;
	$sql="SELECT DISTINCT `enrol`.`id` 
	FROM `enrol` 
	WHERE `enrol`.`courseid` ={$idnhom}";
	$data=$DB->get_records_sql($sql);
	return $data;
}
if(!empty($_POST['iddelete'])){
	global $DB;
	$iddelete=$_POST['iddelete'];
	$block_student=$DB->get_record('block_student', array(
	  'id' => $iddelete
	));
	add_logs('block_student','delete','',$block_student->name);
	delete_block_student($iddelete);
	// xoa toan bo hs cua khoi
	$hs=lay_toan_bo_hs_cua_khoi($iddelete);
	foreach ($hs as  $h) {
		delete_student($h->id);
	}
	// xoa nhom lop cua khoi
	$nhom=lay_toan_bo_nhom_cua_khoi($iddelete);
	foreach ($nhom as $n) {
		$enrol=lay_enrol_cua_nhom($n->id);
		if($enrol){
			foreach ($enrol as $e) {
				$DB->delete_records_select('user_enrolments', " enrolid = $e->id");
				$DB->delete_records_select('enrol', " id = $e->id");
			}
		}
		$DB->delete_records_select('course', " id = $n->id");
		$DB->delete_records_select('course_modules', " course = $n->id");
		$DB->delete_records_select('group_course', " course_id = $n->id");
	}
	// xoa lớp
	$lop=lay_toan_bo_lop_cua_khoi($iddelete);
	foreach ($lop as  $l) {
		$DB->delete_records_select('groups_members', " groupid = $l->id");
		$DB->delete_records_select('groups', " id = $l->id");
	}
}
if (!empty($_GET['code'])) {
		# code...
	$code=trim($_GET['code']);
	$sql="SELECT * FROM `block_student` WHERE del =0 AND code LIKE '%{$code}%'";
	$data=$DB->get_records_sql($sql);
	if(!empty($data)){
		echo'<div class="  alert-danger" >
		Mã khối đã tồn tại
		</div>';
		?>
		<script>
			$('#submitBtn').attr('disabled', true);
		</script>
		<?php 
	}
}
if(!empty($_GET['schoolid'])&&!empty($_GET['codecheck'])){
	$code=trim($_GET['codecheck']);
	$schoolid=trim($_GET['schoolid']);
	$sql="SELECT * FROM `block_student` WHERE del =0 AND code ='{$code}' AND schoolid={$schoolid}";
	$data=$DB->get_records_sql($sql);
	if(!empty($data)){
		echo'<div class="  alert-danger" >
		'.get_string('errors_code_student').'
		</div>';
		?>
		<script>
			$('#submitBtn').attr('disabled', true);
		</script>
		<?php 
	}else{
		?>
		<script>
			$('#submitBtn').attr('disabled', false);
		</script>
		<?php
	}
}
if(!empty($_GET['school_check'])){
	$id=$_GET['school_check'];
	$khoi=$_GET['khoi'];
	$school=$DB->get_record('schools', array(
	  'id' => $id
	));
	echo '<option value="">'.get_string('choise_the_block').'</option>';
	if($school->level==1){
		?>
		<option value="1" <?php if(!empty($khoi)&&($khoi==1)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_1'); ?></option>
		<option value="2" <?php if(!empty($khoi)&&($khoi==2)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_2'); ?></option>
		<option value="3" <?php if(!empty($khoi)&&($khoi==3)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_3'); ?></option>
		<option value="4" <?php if(!empty($khoi)&&($khoi==4)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_4'); ?></option>
		<option value="5" <?php if(!empty($khoi)&&($khoi==5)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_5'); ?></option>
		<?php 
	}
	if($school->level==2){
		?>
		<option value="6" <?php if(!empty($khoi)&&($khoi==6)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_6'); ?></option>
		<option value="7" <?php if(!empty($khoi)&&($khoi==7)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_7'); ?></option>
		<option value="8" <?php if(!empty($khoi)&&($khoi==8)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_8'); ?></option>
		<option value="9" <?php if(!empty($khoi)&&($khoi==9)) echo 'selected'; ?> status="<?php echo $id; ?>"><?php echo get_string('block_9'); ?></option>
		<?php 
	}
	
	
}
if(!empty($_GET['school_check2'])&&!empty($_GET['status'])){

	$school=$_GET['school_check2'];
	$status=$_GET['status'];
	$status_check=$DB->get_record('block_student', array(
	  'schoolid' => $school,
	  'status' => $status,
	  'del'=>0
	));
	if(!empty($status_check)){
		?>
		<div class="  alert-danger" >
			Trường đã tồn tại khối 
		</div>
		<script>
			document.getElementById("submitBtn").disabled = true;
		</script>
		<?php 
	}else{
		?>
		<script>
			document.getElementById("submitBtn").disabled = false;
		</script>
		<?php 
	}
}
?>