<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
global $USER;
$action = optional_param('action', '', PARAM_TEXT);
if($action=='view'){
	$PAGE->set_title(get_string('view_block_student'));
}else{
	$PAGE->set_title(get_string('edit_block_student'));
}

echo $OUTPUT->header();
require_login(0, false);

$id = optional_param('id', '', PARAM_TEXT);
$dataedit=get_block_student($id);
$name = trim(optional_param('name', '', PARAM_TEXT));
// $name ='Khối';
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$note =trim( optional_param('note', '', PARAM_TEXT));
$code =trim(optional_param('code', '', PARAM_TEXT)) ;
$status =trim(optional_param('status', '', PARAM_TEXT)) ;
$idedit = optional_param('idedit', '', PARAM_TEXT);
$error='';
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$module='block-student';
if($action=='view'){
	$name1='view_block_student';
}else{
	$name1='edit_block_student';
}
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$module,$name1);
if(empty($check_in)){
	echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}


?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-10">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" method="post">
					<input type="text" name="idedit" value="<?php echo $dataedit->id; ?>" hidden>
					<input type="text" name="action" value="edit_block_student" hidden>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
								<select name="schoolid" id="schoolid" class="form-control">
									<option value=""><?php echo get_string('schools'); ?></option>
									<?php 
									if (function_exists('get_all_shool')) {
										$schools=get_all_shool();
										if ($schools) {
											foreach ($schools as $key => $value) {
												?>
												<option value="<?php echo $value->id; ?>" <?php if (!empty($dataedit->schoolid)&&($dataedit->schoolid==$value->id)) {echo'selected';} ?>><?php echo $value->name; ?></option>
												<?php 
											}
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div id="alert_status" style="position: absolute;top: 0;right: 18px;">
							</div>
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('choise_the_block'); ?><span style="color:red">*</span></label>
								<select name="status" id="status" class="form-control" required="">
									<option value="1"><?php echo get_string('block_1'); ?></option>
									<option value="2"><?php echo get_string('block_2'); ?></option>
									<option value="3"><?php echo get_string('block_3'); ?></option>
									<option value="4"><?php echo get_string('block_4'); ?></option>
									<option value="5"><?php echo get_string('block_5'); ?></option>
									<option value="6"><?php echo get_string('block_6'); ?></option>
									<option value="7"><?php echo get_string('block_7'); ?></option>
									<option value="8"><?php echo get_string('block_8'); ?></option>
									<option value="9"><?php echo get_string('block_9'); ?></option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div id="alert" style="position: absolute;top: 0;right: 18px;" >
								<?php if(!empty($error)) echo $error; ?>
							</div>
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('code_block_student'); ?> <span style="color:red">*</span></label>
								<div id="erCompany" class="alert alert-danger" style="display: none;">
									<?php print_string('pleasecompany') ?></div>
									<input type="text" id="code" name="code" class="form-control" value="<?php echo $dataedit->code; ?>" required>
								</div>
							</div>
							<!-- <div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label"><?php echo get_string('name_block_student'); ?> <span style="color:red">*</span></label>
									<div id="erCompany" class="alert alert-danger" style="display: none;">
										<?php print_string('pleasecompany') ?></div>
										<input type="text" id="name" name="name" class="form-control" value="<?php echo $dataedit->name; ?>" required>
									</div>
								</div> -->
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label"><?php echo get_string('note'); ?></label>
										<br>
										<textarea name="note" id="note" cols="30" rows="10" class="form-control" value="<?php echo $dataedit->note; ?>">
											<?php echo $dataedit->note; ?>
										</textarea>

										<script>CKEDITOR.replace('note');</script>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-buttons" style="border-top: none !important">
										<?php 
											if($action=='view'){
												?>
												<a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-info"><?php print_r(get_string('back')) ?></a>
												<?php 
											}else{
												?>
												<input type="submit" id="submitBtn" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
												<?php print_r(get_string('or')) ?>
												<a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
												<?php 
											}
										 ?>
										
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
		echo $OUTPUT->footer();
		if($action=='edit_block_student'){ 
			$namesave='Khối '.$status;
			if(($schoolid!=$dataedit->schoolid)||($code!=$dataedit->code)){
				$check=check_code_block_student_school_by_cua($schoolid,$code);
				if($check==2){
					$saveid=save_block_student($idedit,$namesave,$schoolid,$note,$code,$status);
					// add_logs('block-student','update','/manage/block-student/edit_block_student.php?id='.$idedit, $name);
					if(!empty($saveid)){
						echo displayJsAlert('', $CFG->wwwroot . "/manage/block-student/index.php");
					}else{

					}
				}elseif($check==1){
					?>
					<script>
						var e=confirm('<?php echo get_string('errors_code_student'); ?>');
						var id= "<?php echo  $id;?>";
						var urlweb= "<?php echo $CFG->wwwroot ?>/manage/block-student/edit_block_student.php?id="+id+"";
						if(e==true){
							window.location=urlweb;
						}
					</script>
					<?php 

				}
			}else{
				$saveid=save_block_student($idedit,$namesave,$schoolid,$note,$code,$status);
				// add_logs('block-student','update','/manage/block-student/edit_block_student.php?id='.$idedit, $name);
				if(!empty($saveid)){
					echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/block-student/index.php");
				}else{

				}
			}

		}
		?>
		<script>
			var school = $('#schoolid').find(":selected").val();
			var khoi="<?php echo $dataedit->status; ?>"
  			if (school) {
  				$.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?school_check=" + school+"&khoi="+khoi, function(data) {
			            $("#status").html(data);
			        });

  			}
			$('#schoolid').on('change', function() {
			    var schoolid = this.value;
			    if (this.value) {
			        $.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?school_check=" + schoolid+"&khoi="+khoi, function(data) {
			            $("#status").html(data);
			        });
			        
			    }
			});
			$('#status').on('change', function() {
		    var school_id = $('#status option:selected').attr('status');
		    var status = this.value ;
		    // alert(school_id);
		    $.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?school_check2=" +school_id+"&status="+status, function(data) {
		      $("#alert_status").html(data);
		    });
		    
		  });
		    
		</script>