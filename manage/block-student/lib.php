<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function save_block_student($id,$name,$schoolid,$note,$code,$status){
	global $DB, $USER;
		$data = new stdClass();
		$data->id = $id;
		$data->name = $name;
		$data->schoolid = $schoolid;
		$data->note = $note;
		$data->code = $code;
		$data->status = $status;
	  	return $DB->update_record('block_student', $data, false);
		
	
}
function add_block_student($name,$schoolid,$note,$code,$status){
	global $DB;
	  $record = new stdClass();
	 
	  $record->name = trim($name) ;
	  $record->schoolid = trim($schoolid);
	  $record->note =trim($note) ;
	  $record->code = trim($code);
	  $record->del = 0;
	  $record->status =$status;

	  $newid = $DB->insert_record('block_student', $record);
	  
	  return $newid;
}
function delete_block_student($id){
	global $DB;
	// $DB->delete_records_select('block_student', " id = $id ");
	// return 1;
	$DB->set_field('block_student', 'del',1, array('id' => $id));

}

function get_block_student($id){
	if ($id) {
		# code...
		global $DB;
		$block = $DB->get_record('block_student', array(
			'id' => $id
		));

		return $block;
	}
}
function get_all_shool(){
	global $DB;
	$sql="SELECT id, name FROM schools WHERE del =0";
	$data = $DB->get_records_sql($sql); 
	return $data;
}
function get_info_school_with_block($id){
	if ($id) {
		# code...
		global $DB;
		$school = $DB->get_record('schools', array(
			'id' => $id,
		));

		return $school;
	}
}


// lay so nhom cua mot khoi
function get_count_class_in_block_student($id){
	global $CFG, $DB;
	$sql = "SELECT COUNT(`group_course`.`course_id`)  FROM `group_course` JOIN `groups` on `group_course`.`group_id`=`groups`.`id` WHERE `groups`.`id_khoi` = {$id}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_count_class_in_block_student2($id,$idschoolyear){
	global $CFG, $DB;
	$sql = "SELECT COUNT( DISTINCT `history_course_name`.`courseid`)
	FROM `history_course_name`
	JOIN `course` ON `history_course_name`.`courseid` =`course`.`id`
	JOIN `groups` ON `history_course_name`.`groupid`= `groups`.`id`
	WHERE `groups`.`id_khoi`={$id} AND `history_course_name`.`school_year_id`={$idschoolyear}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
//den so hs
function get_count_student_in_block_student($id){
	global $CFG, $DB;
	$time=time();

	$sql = "SELECT COUNT(DISTINCT `user`.`id`) FROM `user` 
	JOIN `history_student` on `history_student`.`iduser` = `user`.`id` 
	JOIN `groups` ON `groups`.`id` = `history_student`.`idclass` 
	JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid` =5 
	AND `groups`.`id_khoi`={$id} 
	AND `user`.`del`=0 
	AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0  OR `user`.`time_end_int` is null)

	";
	// $sql = "SELECT COUNT(DISTINCT `user`.`id`) FROM `user` 
	// JOIN `user_enrolments` ON `user`.`id`=`user_enrolments`.`userid` 
	// JOIN `enrol` ON `user_enrolments`.`enrolid` = `enrol`.`id` 
	// JOIN `course` ON `enrol`.`courseid`=`course`.`id`
	// JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
	// JOIN `group_course` ON `course`.`id` =`group_course`.`course_id	`
	// JOIN `groups` on `group_course`.`group_id`=`groups`.`id`
	// WHERE `groups`.`id_khoi` = {$id}
	// AND `role_assignments`.`roleid`=5 
	// AND `user`.`del`=0
	// -- AND `course`.`id` IN (SELECT `group_course`.`course_id` FROM `group_course` JOIN `groups` on `group_course`.`group_id`=`groups`.`id` WHERE `groups`.`id_khoi` = {$id})
	// AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)
	// ";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_count_student_in_block_student2($id,$idschool_year){
	$time=time();
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `user`.`id`) FROM `user` 
	JOIN `history_student` on `history_student`.`iduser` = `user`.`id` 
	JOIN `groups` ON `groups`.`id` = `history_student`.`idclass` 
	JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid` 
	WHERE `role_assignments`.`roleid` =5 
	AND `groups`.`id_khoi`={$id} 
	AND `history_student`.`idschoolyear`={$idschool_year}
	AND `user`.`del`=0 
	AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
// dem so lơp
function get_count_group_in_block_student($id){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `groups`.`id`) FROM `groups`  
	WHERE `groups`.`id_khoi` = {$id}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_count_group_in_block_student2($id,$idschool_year){
	global $CFG, $DB;
	$sql = "SELECT COUNT(DISTINCT `groups`.`id`) FROM `groups`  
	WHERE `groups`.`id_khoi` = {$id} AND  `groups`.`id_namhoc`={$idschool_year}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
function get_list_block_student_in_block($page='',$number=''){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}

	$sql = "SELECT  * FROM block_student  WHERE `block_student`.`del`= 0 ORDER BY id DESC
	LIMIT {$start} , {$number}
	";
	$data1=$DB->get_records_sql($sql);
	$sql_total="SELECT COUNT( `block_student`.`id`) FROM block_student  WHERE `block_student`.`del`= 0 ";
	// $total_row=$DB->get_records_sql($sql_total);

	// foreach ($total_row as $key => $value) {
 //    # code...
	// 	$total_page=ceil((int)$key / (int)$number);
	// 	break;
	// }
	$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
	$total_page=ceil((int)$total_row / (int)$number);
	$kq=array(
		'data'=>$data1,
		'total_page'=>$total_page 
	);
	return $kq;
}
function search_blockstudent($name=null,$schoolid=null,$page='',$number) {
	global $DB;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$rec = array();
	$sql="
	SELECT `block_student`.*,`schools`.`name` as school_name FROM `block_student` 
	JOIN `schools` ON `schools`.`id` =`block_student`.`schoolid`
	WHERE `block_student`.`del`=0 AND `schools`.`del`=0 ";
	$sql_total="
	SELECT COUNT(`block_student`.`id`) FROM `block_student` 
	JOIN `schools` ON `schools`.`id` =`block_student`.`schoolid`
	WHERE `block_student`.`del`=0 AND `schools`.`del`=0 ";
	if(!empty($name)){
		$sql.=" AND (`block_student`.`name` LIKE '%{$name}%' OR `block_student`.`code` LIKE  '%{$name}%' OR `block_student`.`schoolid` IN (SELECT DISTINCT`schools`.`id` FROM `schools` WHERE `schools`.`name` LIKE '%{$name}%') )";
		$sql_total.=" AND (`block_student`.`name` LIKE '%{$name}%' OR `block_student`.`code` LIKE  '%{$name}%' OR `block_student`.`schoolid` IN (SELECT DISTINCT`schools`.`id` FROM `schools` WHERE `schools`.`name` LIKE '%{$name}%') )";
	}
	if(!empty($schoolid)){
		$sql.=" AND `block_student`.`schoolid` ={$schoolid}";
		$sql_total.=" AND `block_student`.`schoolid` ={$schoolid}";
	}
	$sql .= " ORDER BY  `block_student`.`id` DESC LIMIT {$start} , {$number} ";
	$rec = $DB->get_records_sql($sql);

	// $total_row=$DB->get_records_sql($sql_total);

	// foreach ($total_row as $key => $value) {
 //    # code...
	// 	$total_page=ceil((int)$key / (int)$number);
	// 	break;
	// }
	$total_row=$DB->get_field_sql($sql_total,null,$strictness=IGNORE_MISSING);
	$total_page=ceil((int)$total_row / (int)$number);
	$kq=array(
		'data'=>$rec,
		'total_page'=>$total_page 
	);
	return $kq;

}
function check_code_block_student_school_by_cua($schoolid,$code){
	global $USER, $CFG, $DB;
	$sql="SELECT * FROM `block_student` WHERE del =0  AND schoolid={$schoolid} AND code ='{$code}'";
	$data=$DB->get_records_sql($sql);
	if($data){
		return 1;
	}else{
		return 2;
	}
}
?>