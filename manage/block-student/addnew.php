<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');

$PAGE->set_title(get_string('add_block_student'));
$PAGE->set_heading(get_string('add_block_student'));
$PAGE->set_pagelayout(get_string('add_block_student'));
require_login(0, false);
echo $OUTPUT->header();

?>
<?php
$action = optional_param('action', '', PARAM_TEXT);
$name = trim(optional_param('name', '', PARAM_TEXT));
// $name ='Khối';
$schoolid = optional_param('schoolid', '', PARAM_TEXT);
$note = trim(optional_param('note', '', PARAM_TEXT));
$code = (optional_param('code', '', PARAM_TEXT));
$status = (optional_param('status', '', PARAM_TEXT));
$error='';
if ($action=='add_block_student') {
	$namesave='Khối '.$status;
	// $saveid=add_block_student($name,$schoolid,$note,$code,$status);
	// add_logs('block-student','add','/manage/block-student/edit_block_student.php?id='.$saveid->id,''); 
	// echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/block-student/index.php");
	$check=check_code_block_student_school_by_cua($schoolid,$code);
	
	if($check==2){
		$saveid=add_block_student($namesave,$schoolid,$note,$code,$status);
		add_logs('block-student','add','/manage/block-student/edit_block_student.php?id='.$saveid->id,$name); 
		echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/manage/block-student/index.php");
		
	}elseif($check==1){
		$error='<div class="  alert-danger" >
		'.get_string('errors_code_student').'
		</div>';
	}
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='block-student';
$name1='add_block_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
	echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-10">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="<?php echo $CFG->wwwroot ?>/manage/block-student/addnew.php" onsubmit="return validate();" method="post">
					<input type="text" name="action" value="add_block_student" hidden="">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('schools'); ?> <span style="color:red">*</span></label>
								<select name="schoolid" id="schoolid" class="form-control" required="">
									<option value=""><?php echo get_string('schools'); ?></option>
									<?php 
									if (function_exists('get_all_shool')) {
										$schools=get_all_shool();
										if ($schools) {
											foreach ($schools as $key => $value) {
												?>
												<option value="<?php echo $value->id ?>" <?php if(!empty($schoolid)&&$schoolid==$value->id) echo'selected'; ?>><?php echo $value->name ?></option>
												<?php 
											}
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div id="alert_status" style="position: absolute;top: 0;right: 18px;">
							</div>
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('choise_the_block'); ?><span style="color:red">*</span></label>
								<select name="status" id="status" class="form-control" required="">
									<option value=""><?php echo get_string('choise_the_block'); ?></option>
									<option value="1"><?php echo get_string('block_1'); ?></option>
									<option value="2"><?php echo get_string('block_2'); ?></option>
									<option value="3"><?php echo get_string('block_3'); ?></option>
									<option value="4"><?php echo get_string('block_4'); ?></option>
									<option value="5"><?php echo get_string('block_5'); ?></option>
									<option value="6"><?php echo get_string('block_6'); ?></option>
									<option value="7"><?php echo get_string('block_7'); ?></option>
									<option value="8"><?php echo get_string('block_8'); ?></option>
									<option value="9"><?php echo get_string('block_9'); ?></option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div id="alert" style="position: absolute;top: 0;right: 18px;">
								<?php if(!empty($error)) echo $error; ?>
							</div>
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('code_block_student'); ?><span style="color:red">*</span></label>
								<div id="erCompany" class="alert alert-danger" style="display: none;">
									<?php print_string('pleasecompany') ?></div>
									<input type="text" id="code" name="code" class="form-control" value="<?php echo $code; ?>" required>

								</div>
							</div>
							
							<!-- <div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label"><?php echo get_string('name_block_student'); ?> <span style="color:red">*</span></label>
									<div id="erCompany" class="alert alert-danger" style="display: none;">
										<?php print_string('pleasecompany') ?></div>
										<input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>" required>
									</div>
								</div> -->
							
					</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label"><?php echo get_string('note'); ?></label>
										<br>
										<textarea name="note" id="note" cols="30" rows="10" class="form-control" value="<?php echo $note; ?>"><?php echo $note; ?>
										</textarea>
										<script>CKEDITOR.replace('note');</script>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-buttons" style="border-top: none !important">
										<input type="submit" id="submitBtn" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
										<?php print_r(get_string('or')) ?>
										<a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

		<?php echo $OUTPUT->footer();  ?>
		<script>
			var school = $('#schoolid').find(":selected").val();
  			if (school) {
  				$.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?school_check=" + school, function(data) {
			            $("#status").html(data);
			        });
  			}
			$('#schoolid').on('change', function() {
			    var schoolid = this.value;
			    if (this.value) {
			        $.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?school_check=" + schoolid, function(data) {
			            $("#status").html(data);
			        });
			    }
			});
			$('#status').on('change', function() {
		    var school_id = $('#status option:selected').attr('status');
		    var status = this.value ;
		    // alert(school_id);
		    $.get("<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php?school_check2=" +school_id+"&status="+status, function(data) {
		      $("#alert_status").html(data);
		    });
		    
		  });
		    
		</script>