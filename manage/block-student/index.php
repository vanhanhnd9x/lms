<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/lib/accesslib.php');
require_once($CFG->dirroot . '/manage/block-student/lib.php');
require_once($CFG->dirroot . '/manage/manage_score/lib.php');
require_login(0, false);
global $DB;
$PAGE->set_title(get_string('list_block_student'));
$PAGE->set_heading(get_string('list_block_student'));
$PAGE->set_pagelayout(get_string('list_block_student'));
$search = optional_param('search', '', PARAM_TEXT);
echo $OUTPUT->header();
$name=isset($_GET['name'])?trim($_GET['name']):'';
$schoolid=isset($_GET['schoolid'])?trim($_GET['schoolid']):'';
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$data = search_blockstudent($name,$schoolid,$page,$number);
if (!empty($search)) {
	$url= $CFG->wwwroot.'/manage/block-student/index.php?search=timkiem&name='.$name.'&schoolid='.$schoolid.'&page=';
}else{
	// if (function_exists('get_list_block_student_in_block')) {
		
	// 	$data=get_list_block_student_in_block($page,$number);
		
	// }
	$url= $CFG->wwwroot.'/manage/block-student/index.php?page=';
}
// 

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='block-student';
$name1='list_block_student';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
	echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,'block-student');
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
$idnamhoc=get_id_school_year_now();

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
?>
<script>
	
</script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="material-datatables">
					<div class="row mr_bottom15">
						<?php 
							if(!empty($checkthemmoi)){
								?>
								<div class="col-md-2">
									<a href="<?php echo new moodle_url('/manage/block-student/addnew.php'); ?>" class="btn btn-success"><?php echo get_string('new'); ?></a>
								</div>
								<?php 
							}
						 ?>
						
						<div class="col-md-10">
							<form action="" method="get" accept-charset="utf-8">
								<input type="text" name="search" value="timkiem" hidden> 
								<div class="row">
									<div class="col-md-4">
										<div class="form-group label-floating">
											<div id="erCompany" class="alert alert-danger" style="display: none;">
												<?php print_string('school_year') ?></div>
												<input type="text" value="<?php echo show_show_yeah_in_unit($idnamhoc); ?>" class="form-control" disabled>
											</div>
										</div>
									<div class="col-md-4">
										<div class="form-group label-floating">
											<div id="erCompany" class="alert alert-danger" style="display: none;">
												</div>
												<input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group label-floating">
												<select name="schoolid" id="schoolid" class="form-control" >
													<option value=""><?php echo get_string('schools'); ?></option>
													<?php 
													if (function_exists('get_all_shool')) {
														$schools=get_all_shool();
														if ($schools) {
															foreach ($schools as $key => $value) {
																?>
																<option value="<?php echo $value->id; ?>" <?php if (!empty($_GET['schoolid'])&&($_GET['schoolid']==$value->id)) {echo'selected';} ?>><?php echo $value->name; ?></option>
																<?php 
															}
														}
													}
													?>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-buttons">
												<button class="btn btn-success"><?php echo get_string('search'); ?></button> 
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="table-responsive" data-pattern="priority-columns">
							<form action="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php?action=deleteall" method="post">
								<div class="deleteall">
									<button class="btn btn-danger mb-3">Xóa</button>
								</div>
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>
												<input type="checkbox" class="check" id="checkAll" <?php if (empty($data)) {echo'disabled';} ?> >
											</th>
											<th class="text-center">STT</th>
											<?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-center">'.get_string('action').'</th>';} ?>
											<th><?php echo get_string('schools'); ?></th>
											<th><?php echo get_string('name_block_student'); ?></th>
											<th><?php echo get_string('code_block_student'); ?></th>
											<th><?php echo get_string('total_class'); ?></th>
											<th><?php echo get_string('total_class_group'); ?></th>
											<th><?php echo get_string('total_student'); ?></th>
											
											
										</tr>
									</thead>

									<tbody id="ketquatimkiem">

										<?php 
									# code...
										if (!empty($data['data'])) {
										# code...
											if($page==1){
			                                    $i=0;
			                                }else{
			                                    $i=($page-1)*$number;
			                                } 
											foreach ($data['data'] as $key => $value) {
											# code...
												// $lop=get_count_group_in_block_student($value->id);
												// $hocsinh=get_count_student_in_block_student($value->id);
												$hocsinh=get_count_student_in_block_student2($value->id,$idnamhoc);
												// $nhom=get_count_class_in_block_student($value->id);
												$nhom=get_count_class_in_block_student2($value->id,$idnamhoc);
												$lop=get_count_group_in_block_student2($value->id,$idnamhoc);
												$i++;
												?>
												<tr>
													
													<td> <input type="checkbox" class="check" value="<?php echo $value->id; ?>" name="listid[]"></td>
													<td class="text-center"><?php echo $i; ?></td>
													<?php 
														if(!empty($hanhdong)||!empty($checkdelete)){
															echo'<td class="text-center">';
															$hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$value->id);
															if(!empty($checkdelete)){
																echo'<a href="javascript:void(0)" onclick="delete_block_student('.$value->id.');" class="mauicon" title="Xóa khối"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
															}
															echo'</td>';
														}
													 ?>
													<td><?php echo $value->school_name;?></td>
													<td> <?php echo $value->name; ?></td>
													<td><?php echo $value->code; ?></td>
													<td><?php echo $lop; ?></td>
													<td><?php echo $nhom; ?></td>
													<td><?php echo $hocsinh; ?></td>
													
													
												</tr>
												<?php 
											}
										}else{
											echo'
											<tr>
											<td colspan="10" style="text-align: center;">Dữ liệu trống</td>
											</tr>
											';
										}
										?>
										
									</tbody>
								</table>
							</form>
						</div>
						<?php 
						if ($data['total_page']) {
							if ($page > 2) { 
		                      $startPage = $page - 2; 
		                    } else { 
		                      $startPage = 1; 
		                    } 
		                    if ($data['total_page'] > $page + 2) { 
		                      $endPage = $page + 2; 
		                    } else { 
		                      $endPage =$data['total_page']; 
		                    }
		                    ?>
		                    <p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
							<nav aria-label="Page navigation example">
								<ul class="pagination">
									<li class="page-item <?php if(isset($page)&&($page==1)) echo 'disabled'; ?>">
										<a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
											<span aria-hidden="true">&laquo;</span>
											<span class="sr-only">Previous</span>
										</a>
									</li>
									<?php 
									if(!empty($startPage)):
									for ($i=$startPage; $i <=$endPage ; $i++) {  
	                                        # code...
										?>
										<li class="page-item <?php if($page==$i) echo'active1'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
										<?php 
									}
								endif;
									?>
									<li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
										<a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
											<span aria-hidden="true">&raquo;</span>
											<span class="sr-only">Next</span>
										</a>
									</li>
								</ul>
							</nav>
		                    <?php
						}
						?>
						
					</div>
					<!-- end content-->
				</div>
				<!--  end card  -->
			</div>
			<!-- end col-md-12 -->
		</div>
	</div>
	<style>
		.active1 a{
			background: #4bb747;
    		border: 1px solid #4bb747;
    		color:white !important;
		}
		.active1 a:hover{
			background: #218838 !important;
    		border: 1px solid #218838;
    		color:white;
		}
	</style>
	<?php
	echo $OUTPUT->footer();
	$action = optional_param('action', '', PARAM_TEXT);
	
	
	if ($action=='deleteall') {
		$listid = optional_param('listid', '', PARAM_TEXT);
		# code...
		if(!empty($listid)){
			foreach ($listid as $key => $iddelete) {
				# code...
				delete_block_student($iddelete);
			}
			echo displayJsAlert('', $CFG->wwwroot . "/manage/block-student/index.php");
		}
	}
	
	?>
	<script>
		$('.deleteall').hide();
		$("#checkAll").click(function () {
			$(".check").prop('checked', $(this).prop('checked'));
		});
		$(document).ready(function() {
			$(":checkbox").click(function(event) {
				if ($(this).is(":checked"))
					$(".deleteall").show();
				else
					$(".deleteall").hide();
			});
		});
		
	</script>
	<script>
		function delete_block_student(iddelete){
			var urlweb= "<?php echo $CFG->wwwroot ?>/manage/block-student/index.php";
			var urldelte= "<?php echo $CFG->wwwroot ?>/manage/block-student/load_ajax.php";
			var e=confirm('<?php echo get_string('delete_block_student'); ?>');
			if(e==true){
				$.ajax({
					url:  urldelte,
					type: "post",
					data: {iddelete:iddelete} ,
					success: function (response) {
						window.location=urlweb;
					}
				});
			}
		}
	</script>