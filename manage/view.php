<?php


if (!file_exists('../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../config.php');

require_once("lib.php");
$site = get_site();

$systemcontext = get_context_instance(CONTEXT_SYSTEM);

$PAGE->set_url('/course/index.php');
$PAGE->set_context($systemcontext);
$PAGE->set_pagelayout('admin');