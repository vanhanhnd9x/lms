<?php
global $PAGE;
// Gets all assigned system roles
$assignedroles = get_user_roles($PAGE->context);

//print "<pre>";
//print_r($assignedroles);
//
//print_r($CFG);
//exit;

global $USER, $CFG,$DB;
/*thông tin user*/
// $user_id = $USER->id;
// $user = get_user_from_id($user_id);

require_once($CFG->dirroot . "/common/header_lib.php");
// require_once($CFG->dirroot . '/manage/decentralize/lib.php');
//require_once($CFG->dirroot.'/lib/accesslip.php');
define("LEARNER_ROLE", 5);

$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
$course = $this->page->course;
$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepost = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('side-post', $OUTPUT));

$showsidepost = ($hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT));

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$bodyclasses = array();
if ($showsidepost) {
  $bodyclasses[] = 'side-post-only';
}
else if (!$showsidepost) {
  $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
  $bodyclasses[] = 'has_custom_menu';
}

if (!empty($PAGE->theme->settings->footertext)) {
  $footnote = $PAGE->theme->settings->footertext;
}
else {
  $footnote = '<!-- There was no custom footnote set -->';
}
require_login();
// $parent = $DB->get_record('user', array('id'=>$user_id));
date_default_timezone_set('Asia/Ho_Chi_Minh');
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo $PAGE->title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php print new moodle_url('/theme/nimble/assets/images/favicon.ico');?>">

        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/responsive-table/css/rwd-table.min.css'); ?>" rel="stylesheet" type="text/css" media="screen">

        <!-- Plugins css -->
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">

        <!-- Tooltipster css -->
        <link rel="stylesheet" href="<?php print new moodle_url('/theme/nimble/assets/plugins/tooltipster/tooltipster.bundle.min.css');?>">

        <!-- DataTables -->
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/datatables/dataTables.bootstrap4.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/datatables/buttons.bootstrap4.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!--Footable-->
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/footable/css/footable.core.css');?>" rel="stylesheet">

        <!-- Bootstrap fileupload css -->
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css');?>" rel="stylesheet" />

        <!-- CSS Moodle -->
        <link href="<?php print new moodle_url('/theme/nimble/assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" />
       
        <!-- App css -->

        <link href="<?php print new moodle_url('/theme/nimble/assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php print new moodle_url('/theme/nimble/assets/css/bootstrap-duallistbox.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php print new moodle_url('/theme/nimble/assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php print new moodle_url('/theme/nimble/assets/css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php print new moodle_url('/theme/nimble/assets/css/question.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php print new moodle_url('/theme/nimble/assets/css/style.css'); ?>" rel="stylesheet" type="text/css" />

        <script src="<?php print new moodle_url('/theme/nimble/assets/js/modernizr.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/jquery.min.js'); ?>"></script>

    </head>

    <body data-keep-enlarged="true">
    <div id="wrapper">

      <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">

            <div class="slimscroll-menu" id="remove-scroll">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="<?php echo new moodle_url('/manage'); ?>" class="logo">
                        <div style="
                            background-repeat: no-repeat;
                            background: url(<?php print new moodle_url('/theme/nimble/assets/images/logo-llv.png') ?>);
                            height: 55px;
                            background-repeat: no-repeat;
                            background-position: center;
                        ">
                            
                        </div>
                        <i>
                            <!-- SM -->
                        </i>
                    </a>
                </div>

                <!-- User box -->
                <div class="user-box">
                <?php
                    $img_user_login=$DB->get_record('user', array(
                         'id' =>$USER->id
                        ),
                        $fields='id,profile_img', $strictness=IGNORE_MISSING
                    );
                    if(!empty($img_user_login->profile_img)){

                        $path= $img_user_login->profile_img;
                        // var_dump($path1);die;
                        // if( file_exists($path1)){
                        // }else{
                        //     $path= $CFG->wwwroot. '/account/logo/avata.jpg';
                        // }
                    }else{
                        $path= $CFG->wwwroot. '/account/logo/avata.jpg';
                    }
                ?>
                    <div class="user-img" style="margin: 0 auto;">
                        <img src="<?php echo $path; ?>" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                    </div>
                    
                    <h5 class="text-center"><a href="<?php echo $CFG->wwwroot .'/my_profile/index.php' ?>"><?php echo $USER->lastname.' '.$USER->firstname ?></a> </h5>
                    <!-- <p class="text-muted">Admin Head</p> -->
                </div>

                <!--- Sidemenu -->
                <?php
                    $currentTab = $_SERVER['REQUEST_URI'];

                ?>
                <div id="sidebar-menu">

                    <ul class="metismenu" id="side-menu">

                        <!--<li class="menu-title">Navigation</li>-->

                        <li class="<?php 
                              print strpos($currentTab, "manage/index.php") ? 'active' : ''; 
                              print strpos($currentTab, "manage/news") ? 'active' : ''; 
                            ?>">
                            <a href="<?php print new moodle_url('/manage'); ?>" class="<?php 
                              print strpos($currentTab, "manage/index.php") ? 'active' : ''; 
                              print strpos($currentTab, "manage/news") ? 'active' : ''; 
                            ?>">
                                <i class="fi-air-play"></i>
                                <span><?php print_r(get_string('dashboards')) ?> </span>
                                <!-- <span class="badge badge-danger badge-pill pull-right">7</span> -->
                            </a>

                        </li>
                        <!-- <li class="<?php 
                                print strpos($currentTab, "/manage/decentralize/index.php") ? 'active' : '';
                            ?>">
                            <a href="javascript: void(0);" class="<?php print strpos($currentTab, "/manage/decentralize/index.php") ? 'active' : ''; ?>">
                                <i class="fi-head"></i>
                                <span> Phân quền </span> <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level collapse <?php 
                                              print strpos($currentTab, "/manage/decentralize/index.php") ? 'in' : '';
                                              print strpos($currentTab, "/manage/decentralize/index.php") ? 'in' : '';
                                            ?>" aria-expanded="false">
                                <li class="<?php print strpos($currentTab, "/manage/decentralize/index.php") ? 'active' : ''; ?>">
                                    <a href="<?php print new moodle_url('/manage/decentralize/index.php'); ?>" class="<?php print strpos($currentTab, "/manage/decentralize/index.php") ? 'active' : ''; ?>"> Phân quyền tác nhân</a>
                                </li>
                                <li class="<?php print strpos($currentTab, "/manage/decentralize/list_decentralize.php") ? 'active' : ''; ?>">
                                    <a href="<?php print new moodle_url('/manage/decentralize/list_decentralize.php'); ?>" class="<?php print strpos($currentTab, "/manage/decentralize/list_decentralize.php") ? 'active' : ''; ?>">Quản lý danh sách quyền</a>
                                </li>
                            </ul>
                        </li>  -->
                        
                        <?php //if(is_admin()): ?>

                        <!-- <li class="<?php 
                                print strpos($currentTab, "/admin/roles/manage.php") ? 'active' : ''; 
                            ?>">
                            <a href="javascript: void(0);" class="">
                                <i class="fi-cog"></i> 
                                <span> <?php print_r(get_string('configuration')) ?> </span> <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level collapse <?php 
                                              print strpos($currentTab, "/admin/roles/manage.php") ? 'in' : '';
                                            ?>" aria-expanded="false">
                                <li class="<?php print strpos($currentTab, "/admin/roles/manage.php") ? 'active' : ''; ?>">
                                    <a href="<?php print new moodle_url('/admin/roles/manage.php'); ?>" class="<?php 
                                              print strpos($currentTab, "/admin/roles/manage.php") ? 'active' : ''; 
                                    ?>"> <?php print_r(get_string('role')) ?> </a>
                                </li>
                                
                            </ul>
                        </li>


                        <li class="<?php 
                              print strpos($currentTab, "manage/user.php") ? 'active' : ''; 
                              print strpos($currentTab, "manage/people/new.php") ? 'active' : ''; 
                              ?>">
                            <a href="<?php print new moodle_url('/manage/user.php'); ?>" class="<?php 
                              print strpos($currentTab, "manage/user.php") ? 'active' : ''; 
                              print strpos($currentTab, "manage/people/new.php") ? 'active' : ''; 
                              ?>">
                                  <i class="fi-head"></i>
                                  <span> <?php print_r(get_string('user')) ?>  </span>
                            </a>
                        </li> -->
                        <?php //endif; ?>
                        <!-- <li class="<?php 
                              print strpos($currentTab, "manage/user.php") ? 'active' : ''; 
                              print strpos($currentTab, "manage/people/new.php") ? 'active' : ''; 
                              ?>">
                            <a href="<?php print new moodle_url('/manage/user.php'); ?>" class="<?php 
                              print strpos($currentTab, "manage/user.php") ? 'active' : ''; 
                              print strpos($currentTab, "manage/people/new.php") ? 'active' : ''; 
                              ?>">
                                  <i class="fi-head"></i>
                                  <span> <?php print_r(get_string('user')) ?>  </span>
                            </a>
                        </li> -->
                        <?php
                        $roleid=lay_role_id_cua_user_dang_nhap($USER->id);
                        // hien_thi_menu_theo_role($roleid); 
                        hien_thi_menu_khoi_caidathethong_theo_role_dang_nhap($roleid);
                        hien_thi_menu_khoi_quan_ly_nha_truong_theo_role_dang_nhap($roleid);
                        hien_thi_menu_khoi_quan_ly_giao_vien_theo_role_dang_nhap($roleid);
                        hien_thi_menu_khoi_quan_ly_danh_gia_theo_role_dang_nhap($roleid);
                        hien_thi_menu_khoi_quan_ly_ket_qua_hoc_tap_theo_role_dang_nhap($roleid);
                        hien_thi_menu_khoi_quan_ly_ket_tai_lieu_hoc_tap_theo_role_dang_nhap($roleid);
                        hien_thi_menu_khoi_quan_ly_baocao_theo_role_dang_nhap($roleid);
                        ?>
                        <!-- <li class="<?php 
                                print strpos($currentTab, "/manage/schools/index.php") ? 'active' : '';
                                print strpos($currentTab, "/manage/schools/new.php") ? 'active' : ''; 
                                print strpos($currentTab, "/manage/schools/edit.php") ? 'active' : ''; 
                            ?>">
                            <a href="javascript: void(0);" class="<?php print strpos($currentTab, "/manage/schools/edit.php") ? 'active' : ''; ?>">
                                <i class="fi-layers"></i> 
                                <span> <?php print_r(get_string('schools')) ?> </span> <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level collapse <?php 
                                              print strpos($currentTab, "/manage/schools/index.php") ? 'in' : '';
                                              print strpos($currentTab, "/manage/schools/new.php") ? 'in' : '';
                                            ?>" aria-expanded="false">
                                <li class="<?php print strpos($currentTab, "/manage/schools/index.php") ? 'active' : ''; ?>">
                                    <a href="<?php print new moodle_url('/manage/schools/index.php'); ?>" class="<?php print strpos($currentTab, "/manage/schools/index.php") ? 'active' : ''; ?>"> <?php print_r(get_string('listschools')) ?></a>
                                </li>
                                <li class="<?php print strpos($currentTab, "/manage/schools/new.php") ? 'active' : ''; ?>">
                                    <a href="<?php print new moodle_url('/manage/schools/new.php'); ?>" class="<?php print strpos($currentTab, "/manage/schools/new.php") ? 'active' : ''; ?>"> <?php print_r(get_string('addschools')) ?></a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="fi-box"></i>
                                <span> <?php print_r(get_string('blocks')) ?> </span> <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/block-student/index.php'); ?>"><?php print_r(get_string('listblocks')) ?></a></li>
                                <li><a href="<?php print new moodle_url('/manage/block-student/addnew.php'); ?>"><?php print_r(get_string('addblocks')) ?></a></li>
                            </ul>
                        </li>

                        <li class="<?php 
                              print strpos($currentTab, "/teams/view_team.php") ? 'active' : '';
                              print strpos($currentTab, "/teams/team_settings.php") ? 'active' : '';
                              print strpos($currentTab, "/course/team/groups.php") ? 'active' : '';
                            ?>">
                            <a href="javascript: void(0);" class="<?php
                                print strpos($currentTab, "/teams/view_team.php") ? 'active' : '';
                                print strpos($currentTab, "/teams/team_settings.php") ? 'active' : '';
                                print strpos($currentTab, "/course/team/groups.php") ? 'active' : '';
                                

                                 ?>">
                                <i class="fi-layout"></i><span> <?php print_r(get_string('class')) ?> </span> <span class="menu-arrow"></span>
                            </a>
                            
                            <ul class="nav-second-level collapse <?php 
                                                  
                                                ?>" aria-expanded="false">
                                <li>
                                    <a href="<?php print new moodle_url('/teams/'); ?>"><?php print_r(get_string('classlist')) ?></a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/teams/new_team.php'); ?>"><?php print_r(get_string('classnew')) ?></a>
                                </li>
                                  
                            </ul>
                        </li>

                        <li class="<?php
                                print strpos($currentTab, "/course/settings/index.php") ? 'active' : '';
                                print strpos($currentTab, "/course/people/index.php") ? 'active' : '';
                            ?>">
                            <a href="javascript: void(0);" class="<?php
                                  print strpos($currentTab, "/course/settings/index.php") ? 'active' : '';
                                  print strpos($currentTab, "/course/people/index.php") ? 'active' : '';  
                                ?>">
                                <i class="mdi mdi-group"></i>
                                <span> <?php print_r(get_string('classgroup')) ?> </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="<?php print new moodle_url('/manage/courses.php'); ?>"><?php print_r(get_string('listclassgroup')) ?></a>
                                </li>
                                <li><a href="<?php echo new moodle_url('/course/new_course/index.php'); ?>"><?php print_r(get_string('newclassgroup')) ?></a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span> Học Sinh </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/student/index.php'); ?>">Danh sách học sinh</a></li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/student/add_student.php'); ?>">Thêm mới học sinh</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <span> Năm học </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/school-year/index.php'); ?>">Danh sách năm học</a></li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/school-year/addnew.php'); ?>">Thêm mới năm học</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="mdi mdi-human-pregnant"></i> 
                                <span> <?php print_r(get_string('teacher')) ?><span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/teacher/foreignteacher/index.php'); ?>"><?php print_r(get_string('foreignteacher')) ?></a></li>
                                <li><a href="<?php print new moodle_url('/manage/teacher/tutorsteacher/index.php'); ?>"><?php print_r(get_string('tutorsteacher')) ?></a></li>
                                <li><a href="javascript: void(0);" aria-expanded="false"><?php print_r(get_string('reasonableteam')) ?><span class="menu-arrow"></span></a>
                                    <ul class="nav-third-level nav" aria-expanded="false">
                                        <li><a href="<?php print new moodle_url('/manage/teacher/managerteacher/rht.php'); ?>"><?php print_r(get_string('foreignteacher')) ?></a></li>
                                        <li><a href="<?php print new moodle_url('/manage/teacher/managerteacher/rhta.php'); ?>"><?php print_r(get_string('tutorsteacher')) ?></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="mdi mdi-av-timer"></i> 
                                <span> Thời khóa biểu <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/schedule/index.php'); ?>">Danh sách thời khóa biểu</a></li>
                            </ul>
                        </li>
                        <li class="<?php 
                              print strpos($currentTab, "/manage/manage_score/index.php") ? 'active' : ''; 
                              print strpos($currentTab, "/manage/manage_score/semester_score.php") ? 'active' : ''; 
                              print strpos($currentTab, "/manage/manage_score/semester_score_high_school.php") ? 'active' : ''; 
                              print strpos($currentTab, "/manage/evaluation_student/index.php") ? 'active' : ''; 
                              print strpos($currentTab, "/manage/evaluation_student/score_input.php") ? 'active' : ''; 
                            ?>">
                            <a href="javascript: void(0);">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <span> Kết quả học tập </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/manage_score/score_input.php'); ?>" class="<?php print strpos($currentTab, "/manage/manage_score/score_input.php") ? 'active' : ''; ?>">Điểm đầu vào</a></li>
                                <li><a href="<?php print new moodle_url('/manage/manage_score/index.php'); ?>" class="<?php print strpos($currentTab, "/manage/manage_score/index.php") ? 'active' : ''; ?>">Điểm unit test</a></li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/manage_score/semester_score.php'); ?>" class="<?php print strpos($currentTab, "/manage/manage_score/semester_score.php") ? 'active' : ''; ?>">Điểm học kì tiểu học</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/manage_score/semester_score_high_school.php'); ?>"> Điểm học kì trung học</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/evaluation_student/index.php'); ?>" class="<?php print strpos($currentTab, "/manage/evaluation_student/index.php") ? 'active' : ''; ?>">Đánh giá học sinh</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-check-square-o"></i> 
                                <span> Đánh giá <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/evaluation/teacher.php'); ?>">Giáo viên nước ngoài</a></li>
                                <li><a href="<?php print new moodle_url('/manage/evaluation/teachertutors.php'); ?>">Giáo viên trợ giảng</a></li>
                            </ul>
                        </li> -->
                        <!-- <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                                <span> Thông báo phụ huynh</span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/parent_notification/diligence/'); ?>">Thông báo chuyên cần</a></li>
                            </ul>
                        </li> -->

                       <!--  <li>
                            <a href="javascript: void(0);">
                                <i class="fa fa-book" aria-hidden="true"></i>
                                <span> Tài liệu </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/manage/document/index.php'); ?>">Danh sách tài liệu</a></li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/document/add_document.php'); ?>">Thêm mới tài liệu</a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?php 
                              print strpos($currentTab, "/question/new/listcategory.php") ? 'active' : ''; 
                              print strpos($currentTab, "/question/index.php") ? 'active' : ''; 
                              print strpos($currentTab, "/manage/examination/index.php") ? 'active' : ''; 
                            ?>">
                            <a href="javascript: void(0);">
                                <i class="icon-question" aria-hidden="true"></i>
                                <span>Ngân hàng câu hỏi </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="<?php print new moodle_url('/question/new/listcategory.php'); ?>" class="<?php print strpos($currentTab, "/question/new/listcategory.php") ? 'active' : ''; ?>">Chuyên mục câu hỏi</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/question/index.php'); ?>" class="<?php print strpos($currentTab, "/question/index.php") ? 'active' : ''; ?>">Ngân hàng câu hỏi</a>
                                </li>
                                 <li>
                                    <a href="<?php print new moodle_url('/manage/examination/index.php'); ?>" class="<?php print strpos($currentTab, "/manage/examination/index.php") ? 'active' : ''; ?>">Quản lý bài test</a>
                                </li>
                            </ul>
                        </li>

                        <li class="<?php 
                            print strpos($currentTab, "/manage/report/class_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/school_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/student_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/number_student_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/teacher_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/ta_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/report_semester_1_below_50.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/schedule_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/report_outstanding_students.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/year_end_academic_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/semester_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/certificate_eport.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/ranking_student_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/ranking_school_report.php") ? 'active' : ''; 
                            print strpos($currentTab, "/manage/report/total_certificate_school _eport.php") ? 'active' : ''; 
                            ?>">
                            <a href="javascript: void(0);">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                <span>Báo cáo </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/number_student_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/number_student_report.php") ? 'active' : ''; ?>">BC số lượng học sinh</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/school_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/school_report.php") ? 'active' : ''; ?>">Báo cáo trường học</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/class_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/class_report.php") ? 'active' : ''; ?>">Báo cáo lớp học</a>
                                </li>
                                 <li>
                                    <a href="<?php print new moodle_url('/manage/report/student_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/student_report.php") ? 'active' : ''; ?>">Báo cáo học sinh</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/teacher_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/teacher_report.php") ? 'active' : ''; ?>">Báo cáo GVNN</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/ta_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/ta_report.php") ? 'active' : ''; ?>">Báo cáo GVTG</a>
                                </li>
                                 <li>
                                    <a href="<?php print new moodle_url('/manage/report/schedule_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/schedule_report.php") ? 'active' : ''; ?>">Báo cáo lịch học</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/report_outstanding_students.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/report_outstanding_students.php") ? 'active' : ''; ?>">BC học sinh xuất sắc</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/year_end_academic_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/year_end_academic_report.php") ? 'active' : ''; ?>">BC học tập cuối năm</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/semester_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/semester_report.php") ? 'active' : ''; ?>">Báo cáo kết quả học tập theo năm</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/report_semester_1_below_50.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/report_semester_1_below_50.php") ? 'active' : ''; ?>">BC DSHS  điểm học kỳ I dưới 50 điểm</a>
                                </li> 
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/total_certificate_school.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/total_certificate_school.php") ? 'active' : ''; ?>">BC tổng chứng chỉ</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/certificate_eport.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/certificate_eport.php") ? 'active' : ''; ?>">BC DSHS nhận chứng chỉ</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/ranking_school_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/ranking_school_report.php") ? 'active' : ''; ?>">BC Ranking tổng hợp</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/report/ranking_student_report.php'); ?>" class="<?php print strpos($currentTab, "/manage/report/ranking_student_report.php") ? 'active' : ''; ?>">BC Ranking học sinh</a>
                                </li>
                            </ul>
                        </li>


                        <li class="<?php 
                              print strpos($currentTab, "manage/unlock_reviews") ? 'active' : ''; 
                              print strpos($currentTab, "manage/unlock_reviews/index.php") ? 'active' : ''; 
                              ?>">
                            <a href="javascript: void(0);">
                                <i class="fa fa-unlock" aria-hidden="true"></i>
                                <span> Mở/Khóa đánh giá học sinh </span> <span class="menu-arrow"></span>
                            </a>

                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="<?php print new moodle_url('/manage/unlock_reviews'); ?>">Mở khóa đánh giá</a>
                                </li>
                                <li>
                                    <a href="<?php print new moodle_url('/manage/unlock_reviews/lock.php'); ?>">Khóa đánh giá</a>
                                </li>
                            </ul>
                        </li>


                        <li class="menu-title">More</li>

                        <li>
                            <a href="javascript: void(0);"><i class="mdi mdi-google-circles-extended"></i> <span> Tiện ích </span> <span class="menu-arrow"></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="<?php print new moodle_url('/message/index.php'); ?>">Tin nhắn</a></li>
                            </ul>
                        </li> -->
                        <?php 
                            //if($roleid==5){
                              ?>
                               <!--  <li>
                                    <a href="javascript: void(0);"><i class="mdi mdi-google-circles-extended"></i> <span><?php echo get_string('quiz'); ?></span> <span class="menu-arrow"></span></a>
                                    <ul class="nav-second-level" aria-expanded="false">
                                        <li><a href="<?php print new moodle_url('/manage/examination/list_quiz_student.php'); ?>"><?php echo get_string('list_quiz'); ?></a></li>
                                    </ul>
                                </li> -->
                              <?php   
                           //} 
                            //if(($roleid==8)||($roleid==10)){
                              ?>
                                <!-- <li>
                                    <a href="javascript: void(0);"><i class="mdi mdi-google-circles-extended"></i> <span><?php echo get_string('quiz'); ?></span> <span class="menu-arrow"></span></a>
                                    <ul class="nav-second-level" aria-expanded="false">
                                        <li><a href="<?php print new moodle_url('/manage/examination/list_course_teacher.php'); ?>"><?php echo get_string('classgroup'); ?></a></li>
                                    </ul>
                                </li> -->
                              <?php   
                           // }
                        ?>
                    </ul>

                </div>
                <!-- Sidebar -->

                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->

        

        <div class="content-page">

            <style type="text/css" media="screen">
                .img-content{
                    width: 100%;
                    height: 160px;
                    background-image: url(<?php print new moodle_url('/theme/nimble/assets/images/imageHeader.jpg');?>);
                    background-position: center;
                }
            </style>
            <div class="col-md-12">
                <div class="img-content">
                       
                </div>
            </div>
            <!-- Top Bar Start -->
            <div class="topbar">

                <nav class="navbar-custom">

                    <ul class="list-unstyled topbar-right-menu float-right mb-0">

                        <!-- <li class="hide-phone app-search">
                            <form>
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </li> -->
                        <?php
                         $url_now=get_url_now_cre_by_trang();
                         $url_now1=explode ( '?' ,$url_now);
                         if(!empty($url_now1[1])){
                            $urlen=$url_now.'&lang=en';
                            $urlvi=$url_now.'&lang=vi';
                         }else{
                            $urlen=$url_now.'lang=en';
                            $urlvi=$url_now.'lang=vi';
                         }
                         ?>

                        <li class="dropdown notification-list">
                            <?php echo $OUTPUT->login_info(); ?>
                            <!-- <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">Maxine K <i class="mdi mdi-chevron-down"></i> </span>
                            </a> -->
                            
                        </li>
                        <style type="text/css">
                            .language{margin-left: 5px}
                        </style>
                        <li class="language">
                            <a href="<?php echo $urlvi; ?>" class="nav-link"><img src="<?php print new moodle_url('/theme/nimble/assets/vietnam.png'); ?>"></a>
                        </li>
                        <li class="language">
                            <a href="<?php echo $urlen; ?>" class="nav-link"><img src="<?php print new moodle_url('/theme/nimble/assets/en.png'); ?>"></a>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li>
                            <div class="page-title-box">
                                <h4 class="page-title"><?php echo $PAGE->title ?></h4>
                                <!-- <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Highdmin</a></li>
                                    <li class="breadcrumb-item"><a href="#">Layouts</a></li>
                                    <li class="breadcrumb-item active">Menu Collapsed</li>
                                </ol> -->
                            </div>
                        </li>

                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->



            <!-- Start Page content -->
            <div class="content">
                <div class="container-fluid">

                    <?php echo core_renderer::MAIN_CONTENT_TOKEN ?>

                </div> <!-- container -->

            </div> <!-- content -->

            <footer class="footer text-right">
                2018 © Schools Management - Schools Link English Program
            </footer>

        </div>

        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
    </div>


        <!-- jQuery  -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/popper.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/metisMenu.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/waves.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/jquery.bootstrap-duallistbox.js'); ?>"></script>

        <!-- Flot chart -->
        <!-- <script src="../plugins/flot-chart/jquery.flot.min.js"></script>
        <script src="../plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="../plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="../plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="../plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="../plugins/flot-chart/jquery.flot.crosshair.js"></script>
        <script src="../plugins/flot-chart/curvedLines.js"></script>
        <script src="../plugins/flot-chart/jquery.flot.axislabels.js"></script> -->
        
        <!-- KNOB JS -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/jquery-knob/jquery.knob.js'); ?>"></script>
        <!--[if IE]>
        <script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->

        <!-- plugin js -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/moment/moment.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>"></script>


        <!-- Required datatable js -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/datatables/dataTables.bootstrap4.min.js'); ?>"></script>

        <!-- Tooltipster js -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/tooltipster/tooltipster.bundle.min.js');?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/pages/jquery.tooltipster.js');?>"></script>
        <!--FooTable-->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/footable/js/footable.all.min.js');?>"></script>
        <!--FooTable Example-->
        <script src="<?php print new moodle_url('/theme/nimble/assets/pages/jquery.footable.js');?>"></script>

        <!-- Bootstrap fileupload js -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js');?>"></script>

        <script src="<?php print new moodle_url('/theme/nimble/assets/pages/jquery.form-pickers.init.js'); ?>"></script>

        <!-- Table Responsive -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/responsive-table/js/rwd-table.min.js'); ?>"></script>
        <!-- Select 2 -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/plugins/select2/js/select2.min.js'); ?>"></script>
        
        <!-- Validate -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php print new moodle_url('/theme/nimble/assets/css/bootstrap-select.min.css'); ?>">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

        
        

        <!-- App js -->
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/jquery.core.js'); ?>"></script>
        <script src="<?php print new moodle_url('/theme/nimble/assets/js/jquery.app.js'); ?>"></script>
        <script src="<?php print new moodle_url('/manage/js/main_jquery.js'); ?>"></script>
        <script type="text/javascript">
            function result_search_content(element, actiontype){
                var url ='<?php echo $CFG->wwwroot ?>/manage/manual/ajax.php' ;
                
                var xTriggered = 0;
                $('input[name="'+element+'"]').keyup(function(event) {
                    xTriggered++;
                    var key = $(this).val();
                    $.post( url, {
                        s: key,
                        action:actiontype
                    },
                    function( data ) {
                        //var content = $( data ).find( '#coursefound' );
                        $( "#searchResults" ).empty().html( data);
                        
                    }
                    );

                });
            }

            // validate image
            
        </script>
        <script>
            $(function() {
                $('.table-responsive').responsiveTable({
                    addDisplayAllBtn: false,
                    addFocusBtn :false,
                    stickyTableHeader: false,

                });
            });
        </script>
        <script>
            // $(document).ready(function() {
            //     $('.multiple').select2();
            //     placeholder: "Chọn nội dung";
            // });
            $(".multiple").select2({
                placeholder: "Select a state"
            });
        </script>
        <script type="text/javascript">
            $('.date-own').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                changeYear: true,
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('.date-month').datepicker( {
                format: "mm-yyyy",
                viewMode: "months", 
                minViewMode: "months"
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('.datepicker').datepicker({
                    format:'dd/mm/yyyy'
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();
                $('.datatable').DataTable();
                $('#datatable2').DataTable();

            } );

        </script>
        <!-- trang add -->
        <script>
            var dem=0;
            $('.dripicons-menu').click(function(){
                dem ++;
                if(dem%2==0){
                    $('.topbar-left').show();
                }else{
                    $('.topbar-left').hide();
                }
            });
        </script>
        <style>
            .nav-second-level > li > a {
              white-space: pre-line;
            }

        </style>
    </body>
</html>

