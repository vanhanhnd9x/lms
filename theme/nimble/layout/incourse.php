<?php
global $DB;

require_once($CFG->dirroot.'/common/lib.php');

// Course Module ID
$id      = optional_param('id', 0, PARAM_INT)?optional_param('id', 0, PARAM_INT):optional_param('cm', '', PARAM_INT); 
$reply   = optional_param('reply', 0, PARAM_INT);
$forum   = optional_param('forum', 0, PARAM_INT);
$edit    = optional_param('edit', 0, PARAM_INT);
$delete  = optional_param('delete', 0, PARAM_INT);
$d       = optional_param('d',0, PARAM_INT);    
$f       = optional_param('f', 0, PARAM_INT);

$mobile = detect_mobile();

$current_switchrole = optional_param('current_switchrole',-1, PARAM_INT);

// Forum ID

if(!empty($f)){
    if (!$forum = $DB->get_record('forum', array('id' => $f))) {
        print_error('invalidforumid', 'forum');
    }
}else if (!empty($forum)) {      // User is starting a new discussion in a forum
    if (!$forum = $DB->get_record('forum', array('id' => $forum))) {
        print_error('invalidforumid', 'forum');
    }
} else if (!empty($reply)) {      // User is writing a new reply
    if (!$parent = forum_get_post_full($reply)) {
        print_error('invalidparentpostid', 'forum');
    }
    if (!$discussion = $DB->get_record('forum_discussions', array('id' => $parent->discussion))) {
        print_error('notpartofdiscussion', 'forum');
    }
    if (!$forum = $DB->get_record('forum', array('id' => $discussion->forum))) {
        print_error('invalidforumid');
    }
} else if (!empty($edit)) {  // User is editing their own post
    if (!$post = forum_get_post_full($edit)) {
        print_error('invalidpostid', 'forum');
    }
    if ($post->parent) {
        if (!$parent = forum_get_post_full($post->parent)) {
            print_error('invalidparentpostid', 'forum');
        }
    }
    if (!$discussion = $DB->get_record("forum_discussions", array("id" => $post->discussion))) {
        print_error('notpartofdiscussion', 'forum');
    }
    if (! $forum = $DB->get_record("forum", array("id" => $discussion->forum))) {
        print_error('invalidforumid', 'forum');
    }
}else if (!empty($delete)) {  // User is deleting a post
    if (! $post = forum_get_post_full($delete)) {
        print_error('invalidpostid', 'forum');
    }
    if (! $discussion = $DB->get_record("forum_discussions", array("id" => $post->discussion))) {
        print_error('notpartofdiscussion', 'forum');
    }
    if (! $forum = $DB->get_record("forum", array("id" => $discussion->forum))) {
        print_error('invalidforumid', 'forum');
    }
}else if (!empty($d)) {  // User is deleting a post

    $discussion = $DB->get_record('forum_discussions', array('id' => $d), '*', MUST_EXIST);
    $forum = $DB->get_record('forum', array('id' => $discussion->forum), '*', MUST_EXIST);
}
 

$course_modules = $DB->get_record('course_modules', array('id' => $id, 'visible'=>1));



//$cm = get_coursemodule_from_id('resource', $id);
//$cm = get_coursemodule_from_id('scorm', $id);
//$resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
//$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
//$context = get_context_instance(CONTEXT_MODULE, $cm->id);
//$sec = get_all_sections($cm->course);
 //var_dump($course_modules);
$sequence = $DB->get_record('course_sections', array('id' => $course_modules->section, 'course'=> $course_modules->course, 'visible'=>1),'sequence');
//var_dump($course_modules->section);
//var_dump($course_modules->course);
//var_dump($sequence);
if($course_modules->course)$course_id = $course_modules->course ;
else $course_id = $forum->course ;
    

$url_next_module = '';

if($sequence->sequence){
    $course_sections = explode(',', $sequence->sequence); 
    //Quang changed to fix error when run course --> next module --> invalid course module id
    //when re-order the module again, this error will not happend??!!!
    /*
     * array in sequence (table course_sections), which module run first, second,....
  0 => string '119' (length=3)
  1 => string '114' (length=3)
  2 => string '131' (length=3)
  3 => string '116' (length=3)
  4 => string '125' (length=3)
  5 => string '134' (length=3)
  6 => string '124' (length=3)
  7 => string '123' (length=3)
  8 => string '118' (length=3)
  9 => string '117' (length=3)
  10 => string '127' (length=3)
  11 => string '126' (length=3)
  12 => string '128' (length=3)
  13 => string '129' (length=3) --> bad vaule, because this number is not in couses_module belong to course
  14 => string '132' (length=3)
  15 => string '133' (length=3)
     
   array --> course modules belong to courses
  0 => string '119' (length=3)
  1 => string '114' (length=3)
  2 => string '131' (length=3)
  3 => string '116' (length=3)
  4 => string '125' (length=3)
  5 => string '134' (length=3)
  6 => string '124' (length=3)
  7 => string '123' (length=3)
  8 => string '118' (length=3)
  9 => string '117' (length=3)
  10 => string '127' (length=3)
  11 => string '126' (length=3)
  12 => string '128' (length=3)
  14 => string '132' (length=3)
  15 => string '133' (length=3)
      
    
   array --> new correct course modules which have: folow sequence and course modules belong to couses
  0 => string '119' (length=3)
  1 => string '114' (length=3)
  2 => string '131' (length=3)
  3 => string '116' (length=3)
  4 => string '125' (length=3)
  5 => string '134' (length=3)
  6 => string '124' (length=3)
  7 => string '123' (length=3)
  8 => string '118' (length=3)
  9 => string '117' (length=3)
  10 => string '127' (length=3)
  11 => string '126' (length=3)
  12 => string '128' (length=3)
  13 => string '132' (length=3)
  14 => string '133' (length=3) 
     */
    $course = $DB->get_record('course', array('id' => $course_modules->course));
    $modinfo =& get_fast_modinfo($course);
    //var_dump($course_sections);
    
    $new_sections=array_intersect($course_sections,$modinfo->sections[0]); 
    //var_dump($new_sections);
    
    $course_sections = array();
    $i=0;
    foreach ( $new_sections as $key => $val ){
    $course_sections[ $i ] = $val;
    $i++;
    }
//var_dump($course_sections);    
//Finish Quang added
    
    
    $key = array_search($id, $course_sections);
    
    if(count($course_sections) > 1){
        
        $next_course_modules = $DB->get_record('course_modules', array('id' => $course_sections[$key+1], 'visible'=>1));
        
        
        
        
        switch ($next_course_modules->module) {
            case 1:
                // assignment
                $url_next_module = new moodle_url('/mod/assignment/view.php', array('id'=>$next_course_modules->id));
                break;
            case 2:
                // chat
                $url_next_module = new moodle_url('/mod/chat/view.php', array('id'=>$next_course_modules->id));
                break;
            case 7:
                // forum
                $url_next_module = new moodle_url('/mod/forum/view.php', array('id'=>$next_course_modules->id));
                break;
            case 12:
                // page
                $url_next_module = new moodle_url('/mod/page/view.php', array('id'=>$next_course_modules->id));
                break;
            case 13://Quang fix for next module
                // quiz
                $url_next_module = new moodle_url('/mod/quiz/view.php', array('id'=>$next_course_modules->id));
                break;
            case 14:
                // Resource
                $url_next_module = new moodle_url('/mod/resource/view.php', array('id'=>$next_course_modules->id));
                break;
            case 15:
                // Scrom
                // CUONG: $url_next_module = new moodle_url('/mod/scorm/view.php', array('id'=>$next_course_modules->id));
                $url_next_module = new moodle_url('/mod/scorm/player.php', array('cm'=>$next_course_modules->id, 'scoid'=>0));
                break;
            case 16:
                // survey
                $url_next_module = new moodle_url('/mod/survey/view.php', array('id'=>$next_course_modules->id));
                break;
            case 17:
                // url
                $url_next_module = new moodle_url('/mod/url/view.php', array('id'=>$next_course_modules->id));
                break;
            case 22:
                // embed
                $url_next_module = new moodle_url('/mod/embeded/view.php', array('id'=>$next_course_modules->id));
                break;
        }
    
    }
    
}

?>

<?php echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
    <title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses) ?>">
    
    <div id="titlebar">
        <div class="module-title"><?php //echo $cm->name; ?></div>
        <div class="nav">                
            
            <?php if($url_next_module) : ?>
          <a class="next-module" href="<?php echo $url_next_module;?>&current_switchrole=<?php echo $current_switchrole ?>"><?php print_r(get_string('nextmodule')) ?></a>
            <?php endif; ?>
            <?php if($course_id):?>
             <?php 
             global $USER;
             require_once($CFG->dirroot . '/common/lib.php');
             if(people_get_roles($USER->id)=='Teacher'){?>
          <a class="" href="<?php echo new moodle_url('/course/view.php', array('id'=>$course_id,'current_switchrole'=>$current_switchrole));?>"><?php print_r(get_string('exit')) ?></a> 
            <?php }
            elseif (people_get_roles($USER->id)=='Administrator'){?>
            <a class="" href="<?php echo new moodle_url('/course/user/view.php', array('id'=>$course_id,'current_switchrole'=>$current_switchrole));?>"><?php print_r(get_string('exit')) ?></a>                
            <?php }else{ ?>
            <a class="" href="<?php if($mobile === true) { echo $CFG->wwwroot.'/mobile/view.php?id='.$course_id;  } else { echo $CFG->wwwroot.'/course/user/view.php?id='.$course_id; }?>"><?php print_r(get_string('exit')) ?></a>                
                <?php } ?>
            <?php endif ;?>
        </div>
    
        <div class="clear"></div>
    </div>
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="page">

<!-- END OF HEADER -->

    <div id="content" class="clearfix">
        <?php echo core_renderer::MAIN_CONTENT_TOKEN ?>
    </div>

<!-- START OF FOOTER -->
</div>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>