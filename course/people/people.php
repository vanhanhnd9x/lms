<?php

// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

require_once('../../config.php');
require_once('../../user/lib.php');
global $CFG;
require_once('lib.php');
require_once($CFG->dirroot . '/teams/config.php');
// require($CFG->dirroot . '/common/lib.php');

require_login(0, false);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    print_error('disabled', 'message');
}

$action = optional_param('action', 0, PARAM_TEXT);

$team_id = optional_param('team_id', 0, PARAM_TEXT);
$course_id = optional_param('course_id', 0, PARAM_TEXT);

if ($course_id == 0) {
    $course_id = optional_param('course_id', 0, PARAM_TEXT);
}
$username = optional_param('username', 0, PARAM_TEXT);
$user_id = optional_param('user_id', 0, PARAM_TEXT);
$confirm = optional_param("confirm", '', PARAM_TEXT);
$to_assign = optional_param('to_assign', 0, PARAM_TEXT);
$change_assign = optional_param('change_assign', 0, PARAM_TEXT);
$send_email_notification = optional_param('send_email_notification', 0, PARAM_TEXT);
$email_list = optional_param('email_list', 0, PARAM_TEXT);

$info_course = get_info_course_2($course_id);
if ($action == 'delete_team_member' && $confirm == '') {
    
}

if ($action == 'delete_team_member' && $confirm == 'yes') {
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    remove_team_member($user_id, $team_id);
    //echo displayJsAlert('remove member of team succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
// trang add
function update_nhom_lop_khi_xoa_hs_khoi_nhom($userid,$courseid,$groupid){
    global $DB;
    $thongtinlop=$DB->get_record('groups', array(
         'id' =>$groupid
        ),
        $fields='id,id_truong,id_khoi,id_namhoc', $strictness=IGNORE_MISSING
    );
    $history_student=$DB->get_record('history_student', array(
         'iduser' => $userid,
         'idschool' => $thongtinlop->id_truong,
         'idblock' => $thongtinlop->id_khoi,
         'idclass' => $groupid,
         'idgroups' => $courseid,
         'idschoolyear' => $thongtinlop->id_namhoc,
         ),
         $fields='id', $strictness=IGNORE_MISSING
    );
    if(!empty($history_student)){
        $update = new stdClass();
        $update->id = $history_student->id;
        $update->idgroups ='';
        $DB->update_record('history_student', $update, false);
    }
}
function update_nhom_lop_khi_them_hs_vao_nhom($userid,$courseid,$groupid){
    global $DB;
    $thongtinlop=$DB->get_record('groups', array(
         'id' =>$groupid
        ),
        $fields='id,id_truong,id_khoi,id_namhoc', $strictness=IGNORE_MISSING
    );
    $history_student=$DB->get_record('history_student', array(
         'iduser' => $userid,
         'idschool' => $thongtinlop->id_truong,
         'idblock' => $thongtinlop->id_khoi,
         'idclass' => $groupid,
         'idschoolyear' => $thongtinlop->id_namhoc,
         ),
         $fields='id', $strictness=IGNORE_MISSING
    );
    if(!empty($history_student)){
        $update = new stdClass();
        $update->id = $history_student->id;
        $update->idgroups =$courseid;
        $DB->update_record('history_student', $update, false);
    }
}
if ($action == 'remove_course_person' && $confirm == 'yes') {
    global $DB;
    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    update_nhom_lop_khi_xoa_hs_khoi_nhom($user_id,$course_id,$info_course->id_lop);
    remove_person_from_course($user_id, $course_id);
    echo displayJsAlert('', $redirectPath);
}

if ($action == 'remove_bulk_person_from_course') {

    $people_arr = optional_param('people_arr', '', PARAM_TEXT);

    $members = get_members_enroll_of_course($course_id, $USER->id);
    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    foreach ($members as $key => $val) {
        update_nhom_lop_khi_xoa_hs_khoi_nhom($val->id,$course_id,$info_course->id_lop);
        remove_person_from_course($val->id, $course_id);
    }
    echo displayJsAlert('', $redirectPath);
}


// Add people to groups
if ($action == 'assign_people_to_course') {
    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    global $DB;
    if (empty($to_assign)) {
        echo displayJsAlert('Không có học sinh nào được chọn để gán.', $redirectPath);
    }else{
        foreach ($to_assign as $key => $val) {
            assign_user_to_course($val, $course_id);

            // insert_history_student($val,$info_course->id_truong,$info_course->id_khoi,$info_course->id_lop,$course_id,$info_course->schoolyearid);
            update_nhom_lop_khi_them_hs_vao_nhom($val,$course_id,$info_course->id_lop);
            $course = $DB->get_record('course', array('id' => $course_id));
            if ($send_email_notification == 1) {
                $user = $DB->get_record('user', array('id' => $val));
                $mailto = $user->email;
                $username = $user->username;
                $invitationContenNoEmail = $team_invitation_content;
                $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{TEAM_NAME}", '', $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{COURSE_TITLE}", $course->fullname, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{COURSE_DESCRIPTION}", $course->summary, $invitationContenNoEmail);
                send_email($mailto, $mailUsername, 'LMS', $team_invitation_title, $invitationContenNoEmail);
                $invitationContenNoEmail = $team_invitation_content;
            }
        }
    }
    //echo displayJsAlert('assign users to course succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}

function assign_user_change_course($enrolid,$userid){
    global $CFG, $DB;
    $sql ="UPDATE user_enrolments
        JOIN enrol ON enrol.id = user_enrolments.enrolid 
        SET user_enrolments.enrolid =" .$enrolid. " WHERE
        user_enrolments.userid =".$userid;
    return $DB->execute($sql);
}
// trang add
function update_lich_su_hocsinh_khi_chuyen_nhom_cre_by_trang($courseold,$coursenew,$groupsid,$userid){
    global $DB;
    $thongtinlop=$DB->get_record('groups', array(
        'id' => $groupsid
        ),
        $fields='id,id_namhoc,id_truong,id_khoi', $strictness=IGNORE_MISSING
    );
    $lichsuhocsinh=$DB->get_record('history_student', array(
          'iduser' => $userid,
          'idschool' => $thongtinlop->id_truong,
          'idblock' => $thongtinlop->id_khoi,
          'idclass' => $thongtinlop->id,
          'idgroups' => $courseold,
          'idschoolyear' => $thongtinlop->id_namhoc,
        ),
        $fields='id', $strictness=IGNORE_MISSING
    );
    if(!empty($lichsuhocsinh)){
        $update = new stdClass();
        $update->id = $lichsuhocsinh->id;
        $update->idgroups = trim($coursenew);
        $DB->update_record('history_student', $update, false);
    }else{
        $record = new stdClass();
        $record->idschool = trim($thongtinlop->id_truong) ;
        $record->idblock = trim($thongtinlop->id_khoi) ;
        $record->idclass = trim($thongtinlop->id);
        $record->idgroups = trim($coursenew);
        $record->idschoolyear = trim($thongtinlop->id_namhoc);
        $record->iduser = trim($userid);
        $DB->insert_record('history_student', $record);
    }
}
// Change groups
if ($action == 'assign_people_change_course') {
    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    $course_change = optional_param('course_change', 0, PARAM_TEXT);
    global $DB;
    $thongtinnhom=get_info_course_2($course_id);
    assign_user_change_course($change_assign,$user_id);
    update_lich_su_hocsinh_khi_chuyen_nhom_cre_by_trang($course_id,$course_change,$thongtinnhom->id_lop,$user_id);
    //echo displayJsAlert('assign users to course succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);

}

if ($action == 'delete_course_team') {
    $redirectPath = $CFG->wwwroot . "/course/team/index.php?id=$course_id";
    remove_course_team($course_id, $team_id);

    //echo displayJsAlert('remove team of course succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}

if ($action == 'import_user_from_email') {
    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    import_user_from_email($email_list, $course_id, $send_email_notification);
    //echo displayJsAlert('import people succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
?>