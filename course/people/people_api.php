<?php

// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

require_once('../../config.php');
require_once('../../user/lib.php');
global $CFG;
require_once('lib.php');
require_once($CFG->dirroot.'/teams/config.php');

//require_login(0, false);

//if (isguestuser()) {
//    redirect($CFG->wwwroot);
//}

//if (empty($CFG->messaging)) {
//    print_error('disabled', 'message');
//}

$action = optional_param('action', 0, PARAM_TEXT);
$team_id = optional_param('team_id', 0, PARAM_TEXT);
$course_id = optional_param('id', 0, PARAM_TEXT);
$username = optional_param('username', 0, PARAM_TEXT);
$user_id = optional_param('user_id', 0, PARAM_TEXT);
$confirm = optional_param("confirm", '', PARAM_TEXT);
$to_assign = optional_param('to_assign', 0, PARAM_TEXT);
$send_email_notification = optional_param('send_email_notification', 0, PARAM_TEXT);
$email_list=optional_param('email_list', 0, PARAM_TEXT);



if ($action == 'assign_people_to_course') {
//    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    global $DB;
    
        assign_user_to_course($user_id,$course_id);
        $course = $DB->get_record('course', array('id' => $course_id));
            if ($send_email_notification == 1) {
                $user = $DB->get_record('user', array('id' => $val));
                $mailto = $user->email;
                $username = $user->username;
                $invitationContenNoEmail = $team_invitation_content;
                
                $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{TEAM_NAME}", '', $invitationContenNoEmail);
                
                $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
                
                $invitationContenNoEmail = str_replace("{COURSE_TITLE}", $course->fullname, $invitationContenNoEmail);
                
                $invitationContenNoEmail = str_replace("{COURSE_DESCRIPTION}", $course->summary, $invitationContenNoEmail);
                              
                send_email($mailto, $mailUsername, 'LMS', $team_invitation_title, $invitationContenNoEmail);
                
                $invitationContenNoEmail = $team_invitation_content;
            }
//    echo displayJsAlert('', $redirectPath);
}



//if($action=='import_user_from_email'){
//	$redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
//	import_user_from_email($email_list,$course_id,$send_email_notification);
//	
//	//echo displayJsAlert('import people succesfully', $redirectPath);
//        echo displayJsAlert('', $redirectPath);
//}
?>