<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');

require_once($CFG->dirroot . "/lib/modinfolib.php");
require_once($CFG->dirroot . "/lib/completionlib.php");
require_once($CFG->dirroot . "/lib/completion/completion_completion.php");
require($CFG->dirroot . '/common/lib.php');


require_login(0, false);
$id = optional_param('id', 0, PARAM_INT); //course_id
$user_id = optional_param('user_id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}
$user = $DB->get_record('user', array('id' => $user_id));
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);

require_once($CFG->dirroot . '/course/lib.php');

if ($action == 'set_course_as_completed') {
    $ccompletion = new completion_completion(array('course' => $id, 'userid' => $user_id));
    $ccompletion->mark_complete(time());
    update_all_cms_as_completed($id, $user_id);
}

if ($action == 'update_cm_status') {
    $text_completed = optional_param('text_completed', '', PARAM_TEXT);
    $user_id = optional_param('user_id', '', PARAM_TEXT);
    $cm_id = optional_param('cm_id', '', PARAM_TEXT);
    if (is_cmc_existed($cm_id, $user_id)) {
        update_state_course_module_completion($cm_id, $user_id, $text_completed);
    } else {
        insert_cmc($cm_id, $user_id);
    }
    $cm = new course_modinfo($course, $user_id);
    $cm_object = $cm->get_cm($cm_id);

    if ($cm_object->module == 13) {
        $quiz_attemp_obj = get_highest_score_quiz_obj($user_id, $cm_object->instance);
        $sumgrades = optional_param('score', '', PARAM_TEXT);
        $date_completed = optional_param('date_completed', '', PARAM_TEXT);
        $timefinish = strtotime($date_completed);
        update_quiz_attemp($quiz_attemp_obj->id, $sumgrades, $timefinish);
    }
}
echo $OUTPUT->header();
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script language="javascript" src="<?php echo $CFG->wwwroot ?>/common/calendar/calendar.js"></script>
<div id="page-body">        
    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">

                        <div id="courseList" class="focus-panel">
                            <div class="panel-head">
                                <h3><a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $user_id ?>" class=""><?php echo $user->firstname ?> <?php echo $user->lastname ?></a></h3>
                                <div class="subtitle"><span class="italic"><?php print_r(get_string('resultsforthe')) ?></span> <b><a href="<?php echo $CFG->wwwroot ?>/course/view.php?id=<?php echo $id ?>" class=""><?php echo $course->fullname ?></a></b></div>
                            </div>
                            <div class="body">

                                <table class="simple-form">
                                    <tbody>
                                        <tr>
                                            <th><?php print_r(get_string('coursestatus')) ?></th>

                                            <?php
                                            $completion = new completion_info($course);

                                            if (!$completion->is_course_complete($user_id) && count_course_completion($course, $user_id) != 100) {
                                                ?>
                                                <td><?php print_r(get_string('notcomplete')) ?>        
                                                    <span title="" class="box-tag box-tag-grey "><span><?php echo count_course_completion($course, $user_id); ?>%</span></span> <span class="tip"><?php print_r(get_string('ofthecourse')) ?></span>  <a id="set_status_link"  class="edit-row" href="#"><?php print_r(get_string('setstatus')) ?>  </a>                 
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td><span class="box-tag box-tag-green " title=""><span>Complete</span></span>                        

                                                    <span class="box-tag box-tag-orange " title=""><span>course updated</span></span> <span class="tip">This course has moved back to learners to-do list</span>
                                                </td>
                                            <?php } ?>

                                        </tr>
                                        <?php if ($completion->is_course_complete($user_id) || count_course_completion($course, $user_id) == 100) {
                                            ?>
                                            <tr>
                                                <th>Completed Date:</th>
                                                <td>

                                                    <span class="date-display">
                                                        <?php
                                                        if ($ccompletion->timecompleted != 0) {
                                                            echo date('F j, Y', $ccompletion->timecompleted);
                                                        } else {
                                                            $ccompletion = new completion_completion(array('course' => $id, 'userid' => $user_id));
                                                            $ccompletion->mark_complete(time());
                                                            echo date('F j, Y', $ccompletion->timecompleted);
                                                        }
                                                        ?>
                                                    </span>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody></table>

                                <div class="form-sub-heading"><?php print_r(get_string('achievementfor')) ?></div>

                                <div class="padded tip align-center"><?php print_r(get_string('learnerhasno')) ?></div>


                                <div class="form-sub-heading"><?php print_r(get_string('moduleresult')) ?></div>

                                <div class="warning align-center padded"><?php print_r(get_string('warningchanging')) ?></div>

                                <table class="item-page-list">
                                    <tbody><tr>
                                            <th>&nbsp;</th>
                                            <th><?php print_r(get_string('passmark')) ?></th>
                                            <th><?php print_r(get_string('score')) ?></th>
                                            <th><?php print_r(get_string('status')) ?></th>
                                            <th>&nbsp;</th>
                                        </tr>

                                        <?php
                                        $cm = new course_modinfo($course, $user_id);
                                        $modinfo = & get_fast_modinfo($course);
                                        $mods_in_course = $modinfo->sections;
                                        foreach ($mods_in_course as $key => $val) {
                                            foreach ($val as $key1 => $val1) {
                                                $cm_object = $cm->get_cm($val1);
                                                ?>
                                                <tr>                   
                                                    <td class="main-col">

                                                        <div class="title"><?php echo $cm_object->name; ?></div>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($cm_object->module == 13) {
                                                            $pass_mark = get_pass_mark_quiz($cm_object->instance);
                                                            echo $pass_mark;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($cm_object->module == 13) {
                                                            echo trim(trim(get_highest_score_quiz($user_id, $cm_object->instance), '0'), '.');
                                                        }
                                                        ?>
                                                    </td>

                                                    <?php
                                                    if (is_cm_complete($course, $val1, $user_id) == 1) {
                                                        ?>
                                                        <td class="nowrap">
                                                            <span title="" class="box-tag box-tag-green m-status"><span><?php print_r(get_string('complete')) ?></span></span>   
                                                        </td>
                                                    <?php } else { ?>
                                                        <td class="nowrap">
                                                            <span title="" class="box-tag box-tag-grey m-status"><span><?php print_r(get_string('notcomplete')) ?></span></span>   
                                                        </td>
                                                    <?php } ?>

                                                    <td><a class="edit_course_module_result_link" class="edit-row" onclick="javascript:open_popup_cmc_form(<?php echo $val1; ?>,<?php echo $user_id ?>,<?php echo $id ?>)" href="#"><?php print_r(get_string('edit')) ?></a></td>
                                            <input type="hidden" name="hidden_cm_id" value="0"/>
                                            </tr>
                                        <?php } ?>


                                        <?php
                                    }
                                    ?>
                                    </tbody></table>
                            </div>
                        </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                    </div>
                </td>
            </tr>
        </tbody></table>
</div>

<div class="overlay" id="overlay" style="display:none;"></div>
<div id="facebox_completion_course" style="display: none;"> 
    <div class="popup">        
        <div class="content">
            <div class="assign-header">
                <div class="tip"><?php print_string('changeresult') ?></div>
                <div style="color: white"><?php echo $course->fullname ?></div>
            </div>
            <div class="padded">

                <form action="<?php echo $CFG->wwwroot ?>/course/people/edit_result.php?id=<?php echo $course->id ?>&user_id=<?php echo $user_id ?>&action=set_course_as_completed" method="post">

                    <div class="tip align-center padded"><?php print_string('changestatus') ?></div>

                    <table class="simple-form">
                        <tbody><tr>
                                <th><?php print_string('changecompleted') ?>:</th>
                                <td><input id="Completed" name="Completed" type="checkbox" value="true"><input name="Completed" type="hidden" value="false"></td>
                            </tr>
                            <tr>
                                <th><?php print_string('reasonfor') ?>:</th>
                                <td><textarea cols="20" id="Note" name="Note" rows="2"></textarea></td>
                            </tr>
                        </tbody></table>
                    <div class="form-buttons">
                        <input class="btnlarge" type="submit" value="<?php print_string('save') ?>"><span> hoặc </span>
                        <a href="#" id="cancelUpdate"><?php print_string('cancel') ?></a>
                    </div>
                </form> 
            </div></div>       </div>
</div>
<div id="facebox_completion_course_module" style="display: none;">  

</div>



