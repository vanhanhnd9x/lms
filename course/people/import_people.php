<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');
require('lib.php');
require_login(0, false);
$id = optional_param('id', 0, PARAM_INT); //course_id
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}
$PAGE->set_title(get_string('importpeople'));
$PAGE->set_heading(get_string('importpeople'));
echo $OUTPUT->header();
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-body">        


    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">

                        <div id="courseList" class="focus-panel">
                            <div class="panel-head">
                                <h3><?php print_r(get_string('importpeople')) ?></h3>
                                <div class="subtitle"><?php print_r(get_string('createnew')) ?></div>
                            </div>
                            <div class="body">
                                <div class="tip"><p><?php print_r(get_string('exemail')) ?></p></div>
                                <form method="post" action="<?php echo $CFG->wwwroot ?>/course/people/people.php?action=import_user_from_email&id=<?php echo $id ?>">
                                    <table class="simple-form">
                                        <tbody><tr>
                                                <th>
                                                    <?php print_r(get_string('emailaddr')) ?>
                                                </th>
                                                <td>
                                                    <textarea rows="5" name="email_list" id="email_list" cols="20" class="full emails" onKeyDown="limit_text(1000);" 
                                                              onKeyUp="limit_text(1000);"></textarea><div class="tip" id="EmailList_counter">1000 character(s) left</div>
                                                </td>
                                            </tr>
                                            <tr>   
                                                <th>

                                                </th>
                                                <td>
                                                    <input type="checkbox" value="true" name="SendEmails" id="SendEmails" checked="checked"><input type="hidden" value="false" name="SendEmails"> <label class="tip" for="SendEmails"><?php print_r(get_string('sendthese')) ?> </label>
                                                    <input type="hidden" name="send_email_notification" id="send_email_notification" value="1"/>
                                                </td>                    
                                            </tr>                
                                        </tbody></table>
                                    <div class="form-buttons">
                                        <input type="submit" class="btnlarge" value=" <?php print_r(get_string('import')) ?> ">
                                        <?php print_r(get_string('or')) ?> 
                                        <a id="backButton" href="<?php echo $CFG->wwwroot ?>/course/people/index.php?id=<?php echo $id ?>"><?php print_r(get_string('cancel')) ?></a>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">

                        <div class="side-panel">
                            <div class="action-buttons">
                                <ul>
                                    <li></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </td>
            </tr>
        </tbody></table>


</div>
