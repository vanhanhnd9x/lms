<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once($CFG->dirroot . '/manage/lib.php');

function search_complete_status($status, $course_id, $creator_id) {
    global $DB;
    if ($status == "") {
        return get_members_enroll_of_course($course_id, $creator_id);
    } else {
        $course = $DB->get_record('course', array('id' => $course_id));
        $user_complete_arr = array();
        $user_un_complete_arr = array();

        //include($CFG->dirroot . '/lib/completionlib.php');

        $info = new completion_info($course);

        $members = get_members_enroll_of_course($course_id, $creator_id);
        $count_complete = 0;
        $count_in_complete = 0;
        foreach ($members as $key => $val) {
            $coursecomplete = $info->is_course_complete($val->id);
            if ($coursecomplete) {
                $user_complete_arr[] = $val->id;
                $count_complete++;
            } else {
                $user_un_complete_arr[] = $val->id;
                $count_in_complete++;
            }
        }

        if ($status == 0 && $count_in_complete == 0) {
            return null;
        }
        if ($status == 1 && $count_complete == 0) {
            return null;
        }


        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id";
        if ($status == 0) {
            $sql.=" AND user2.id in (" . implode(",", $user_un_complete_arr) . ')';
        }
        if ($status == 1) {
            $sql.=" AND user2.id in (" . implode(",", $user_complete_arr) . ')';
        }
        $params['course_id'] = $course_id;
        $params['creator_id'] = $creator_id;
        //echo 'sql='.$sql;
        $result = $DB->get_records_sql($sql, $params);

        return $result;
    }
}

//Tung added function for role teacher

function search_complete_status_second($status, $course_id, $creator_id) {
    global $DB, $CFG, $USER;

    if ($status == "") {
        return get_members_enroll_of_course_second($course_id, $creator_id);
    } else {
        $course = $DB->get_record('course', array('id' => $course_id));
        $user_complete_arr = array();
        $user_un_complete_arr = array();

        //include($CFG->dirroot . '/lib/completionlib.php');

        $info = new completion_info($course);

        $members = get_members_enroll_of_course_second($course_id, $creator_id);
        $count_complete = 0;
        $count_in_complete = 0;
        foreach ($members as $key => $val) {
            $coursecomplete = $info->is_course_complete($val->id);
            if ($coursecomplete) {
                $user_complete_arr[] = $val->id;
                $count_complete++;
            } else {
                $user_un_complete_arr[] = $val->id;
                $count_in_complete++;
            }
        }

        if ($status == 0 && $count_in_complete == 0) {
            return null;
        }
        if ($status == 1 && $count_complete == 0) {
            return null;
        }
        //Tung added
        $sql1 = "SELECT * FROM user ";
        $rows = $DB->get_records_sql($sql1);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

        $id_list = display_children($rows, $USER->id);
        $id_list .= $USER->id;

        //end fix

        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent=:creator_id OR user2.id in (" . $id_list . "))";
        if ($status == 0) {
            $sql.=" AND user2.id in (" . implode(",", $user_un_complete_arr) . ')';
        }
        if ($status == 1) {
            $sql.=" AND user2.id in (" . implode(",", $user_complete_arr) . ')';
        }
        $params['course_id'] = $course_id;
        $params['creator_id'] = $creator_id;
        //echo 'sql='.$sql;
        $result = $DB->get_records_sql($sql, $params);

        return $result;
    }
}

function search_member_of_course($name, $course_id, $creator_id) {
    global $DB;
    if ($name == "") {
        return get_members_enroll_of_course($course_id, $creator_id);
    } else {
        $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND user2.parent=:creator_id";
        $params['course_id'] = $course_id;
        $params['creator_id'] = $creator_id;
        $sql .="AND (";
        $sql .= $DB->sql_like('user2.lastname', ':lastname', false);
        $params['lastname'] = '%' . $name . '%';
        $sql.=" OR ";
        $sql .= $DB->sql_like('user2.firstname', ':firstname', false);
        $params['firstname'] = '%' . $name . '%';
        $sql.=" OR ";
        $sql .= $DB->sql_like('user2.username', ':username', false);
        $params['username'] = '%' . $name . '%';

        $sql .=")";

        $result = $DB->get_records_sql($sql, $params);
        return $result;
    }
}

function get_users_from_parent($creator_id) {
    global $DB;
    $sql = 'SELECT * FROM user
        WHERE parent=:creator_id';
    $params['creator_id'] = $creator_id;
    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function email_name($email_address, $split = '@') {
    return ucwords(strtolower(substr($email_address, 0, strripos($email_address, $split))));
}

include ($CFG->dirroot . '/lib/phpmailer/class.phpmailer.php'); //have to put this include here, out side function content in case send multiple email

function send_email($to, $from, $from_name, $subject, $body) {
    global $CFG;

    include($CFG->dirroot . '/teams/config.php');
    global $error;
    $mail = new PHPMailer(); // create a new object
    $mail->IsSMTP(); // enable SMTP
    //$mail->IsHTML=true;
    $mail->IsHTML(true);
    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 465;
    $mail->Username = $mailUsername;
    $mail->Password = $pass;
    $mail->SetFrom($from, $from_name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AddAddress($to);
    $mail->CharSet = "utf-8";

    if (!$mail->Send()) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return false;
    } else {
        $error = 'Message sent!';
        return true;
    }
}

//Tung edit for username to email
function import_user_from_email($email_list, $course_id, $send_email_notification) {
    global $DB, $CFG, $USER;
    $email_arr = explode(',', $email_list);
    include($CFG->dirroot . '/teams/config.php');

    foreach ($email_arr as $key => $val) {//$val=email
//        $name = email_name($val);
        $user = new stdClass();
        $user->id = -1;
        $user->auth = 'manual';
        $user->confirmed = 1;
        $user->deleted = 0;
        $user->timecreated = time();
        $password = randPass(8);
        $user->password = hash_internal_user_password($password);
        $user->username = $val;
        $user->mnethostid = 1;
        $user->firstname = $name;
        $user->parent = $USER->id;
        $user->email = $val;
        $lastinsertid = $DB->insert_record('user', $user);
        require_once($CFG->dirroot . '/lib/enrollib.php');
        $enrol = enrol_get_plugin('manual');
        $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
        $instance = reset($instances);
        $enrol->enrol_user($instance, $lastinsertid, 5, '', '');
        if ($send_email_notification == 1) {
            $mailto = $val;
            $invitationContenNoEmail = $emailContentNo;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $name, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{PASSWORD}", $password, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            send_email($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
            $invitationContenNoEmail = $emailContentNo;
        }
    }
}

function randPass($length, $strength = 8) {
    $vowels = 'aeuy';
    $consonants = 'bdghjmnpqrstvz';
    if ($strength >= 1) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
    }
    if ($strength >= 2) {
        $vowels .= "AEUY";
    }
    if ($strength >= 4) {
        $consonants .= '23456789';
    }
    if ($strength >= 8) {
        $consonants .= '@#$%';
    }

    $password = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $password;
}

function get_members_enroll_of_course($course_id, $creator_id) {
    global $DB, $USER;
    $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id";
    $params['course_id'] = $course_id;
    $params['creator_id'] = $creator_id;

    $result = $DB->get_records_sql($sql, $params);
    foreach ($result as $key => $val) {

        if (is_user_enroll_in_course($val->id, $course_id) === false) {
            unset($result[$val->id]);
        }
    }

    return $result;
}

//Tung added function for role teacher
function get_members_enroll_of_course_second($course_id, $creator_id) {
    global $DB, $USER;
    //Tung Added
    $sql1 = "SELECT * FROM user ";
    $rows = $DB->get_records_sql($sql1);

//    $id_list = display_parent($rows, $USER->id) ;
//    $id_me = $id_list;
//    
//    if($id_me==0){
//        $id_me = $USER->id;
//    }
//    if($id_list==0){
//             $id_list = $USER->id;
//            }  

    $id_list = display_children($rows, $USER->id);
    $id_list .= $USER->id;

    //end fix
    $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent in (" . $id_list . ") OR user2.id in (" . $id_list . "))";
    $params['course_id'] = $course_id;
    //$params['creator_id'] = $creator_id;

    $result = $DB->get_records_sql($sql, $params);
    foreach ($result as $key => $val) {

        if (is_user_enroll_in_course($val->id, $course_id) === false) {
            unset($result[$val->id]);
        }
    }

    return $result;
}

function is_user_enroll_in_course($user_id, $course_id) {
    global $DB;


    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);

    if (!$ue = $DB->get_record('user_enrolments', array('enrolid' => $instance->id, 'userid' => $user_id))) {


        return false;
    }
    return true;
}

function remove_person_from_course($person_id, $course_id) {
    global $DB, $CFG;

    require_once($CFG->dirroot . '/lib/enrollib.php');
    $enrol = enrol_get_plugin('manual');
    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);

    $enrol->unenrol_user($instance, $person_id);
}

function assign_user_to_course($user_id, $course_id) {


    global $CFG, $DB;
    require_once($CFG->dirroot . '/lib/enrollib.php');

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');

    $instance = reset($instances);
    $enrol->enrol_user($instance, $user_id, 5, '', '');
}

function displayJsAlert($ms, $redirectPath) {
    $html = '';
    if ($ms != '') {
        $html .= '<script language="javascript">alert("' . $ms . '")</script>';
    }
    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}


function insert_history_student($idUser,$idSchool,$idBlock,$idClass,$idGroups="",$idYear)
{
    global $DB;
    $data = new stdClass();

    $data->iduser   = $idUser;
    $data->idschool = $idSchool;
    $data->idblock  = $idBlock;
    $data->idclass  = $idClass;
    $data->idgroups = $idGroups;
    $data->idschoolyear = $idYear;

    $lastinsertid = $DB->insert_record('history_student', $data);
    return $lastinsertid;

}

function get_info_course_2($course_id){
    global $DB;
    $sql = "SELECT course.*, groups.id AS id_lop, groups.id_khoi, groups.id_truong, groups_year.schoolyearid FROM course
    JOIN group_course ON group_course.course_id = course.id
    JOIN groups ON group_course.group_id = groups.id
    JOIN groups_year ON groups.id=groups_year.groupid
    WHERE course.id=".$course_id;
    $groups = $DB->get_record_sql($sql);
    return $groups;
}

?>
