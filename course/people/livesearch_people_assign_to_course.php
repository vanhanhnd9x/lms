<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ("../../config.php");

//require_once ("lib.php");
require($CFG->dirroot . '/common/lib.php');
// Require Login.
require_login(0, FALSE);
global $USER;
$q = $_GET["q"];
$course_id = $_GET["course_id"];

$users_enroll_of_course = search_members_enroll_of_course($course_id, $USER->id, $q);
$users_enroll_of_course_arr = array();
foreach ($users_enroll_of_course as $key => $val) {
    $users_enroll_of_course_arr[] = $val->id;
}
//Cần sửa lại cho invite user dưới quyền quản lý teacher
$child_users = search_child_user($USER->id, $q);

//var_dump($search_member);
?>



<table id="tech-companies-1" class="table table-hover">
    <thead>
        <tr>
            <th><input type="checkbox" name="" id="checkAll"></th>
            <th data-priority="1">Tên học sinh</th>
            <!-- <th data-priority="1">Ban giám hiệu</th>
            <th data-priority="1">Địa chỉ</th> -->
            <th data-priority="1">Email</th>
            <th data-priority="1">Ngày sinh</th>
            <th class="disabled-sorting text-right">Trạng thái</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach ($child_users as $key => $val) {
        ?>
        <tr>
            <td>
                <?php if (in_array($val->id, $member_arr)) { ?>
                    &nbsp;
                <?php } else { ?>
                    <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                <?php } ?>
            </td>
            <td><?php echo $val->firstname ?> <?php echo $val->lastname ?></td>
            <!-- <td><?php echo $sch->representative_name ?></td> -->
            <!-- <?php
                $xaphuong = get_info_xaphuong($sch->commune);
                $quanhuyen = get_info_quanhuyen($sch->district);
                $tinhthanh = get_info_tinhthanh($sch->city);
            ?> -->
            <!-- <td><?php echo $sch->address .' - '. $xaphuong->name.' - '. $quanhuyen->name.' - '. $tinhthanh->name ?></td> -->
            <td><?php echo $val->email ?></td>
            <td><?php echo $val->birthday ?></td>
            <td class="text-right">&nbsp;
                <?php if (in_array($val->id, $users_enroll_of_course_arr)) { ?>
                    <span title="" class="badge badge-success"><span>
                            <?php print_r(get_string('alreadyassigned')) ?>
                        </span></span>                       
                <?php } else { ?>
                    <span title="" class=""><span>

                        </span></span>                       
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
