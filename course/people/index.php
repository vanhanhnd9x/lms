<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');
// require('lib.php');
require_once($CFG->dirroot . "/lib/modinfolib.php");
require_once($CFG->dirroot . "/lib/completionlib.php");
require_once($CFG->dirroot . "/lib/completion/completion_completion.php");
require($CFG->dirroot . '/common/lib.php');

require_login(0, false);
$id = optional_param('id', 0, PARAM_INT); //course_id
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}
$PAGE->set_title(get_string('classgroup') . ': ' .$course->fullname);
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();

function get_list_groups_class($group_id,$course_id){
    global $DB;
    $sql = 
        'SELECT
            course.id,
            course.fullname,
            groups.name,
            user_enrolments.enrolid as enrolid
        FROM
            course
            JOIN group_course ON course.id = group_course.course_id
            JOIN groups ON groups.id = group_course.group_id 
            JOIN enrol ON course.id = enrol.courseid
            JOIN user_enrolments ON user_enrolments.enrolid = enrol.id
        WHERE
            NOT course.id = '. $course_id .' AND group_course.group_id ='. $group_id;
        return $DB->get_records_sql($sql);
}

$members     = get_members_enroll_of_course($id);


// Gán lớp học vào nhóm
$teams_in_course = get_teams_from_course($id);
$teams_from_user_id_and_course = get_teams_from_user_id($id);
$teams_of_courses_arr = array();
foreach ($teams_in_course as $key => $val) {
    $teams_of_courses_arr[] = $key;
}


// thông tin nhóm lớp
$groups  = get_info_groups2($id);
add_logs('groups','view','/course/people/index.php?id='.$id,$groups->fullname);

// Chuyển nhóm
if(!empty($groups->id_lop)){
  $toGroup =  get_list_groups_class($groups->id_lop,$id);  
}

$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='course';
$check_xoacon=check_chuc_nang_xoachucnangcong($roleid,$moodle);
$check_gancon=check_chuc_nang_gan_con($roleid,$moodle);
?>



<div class="row">
    <div class="col-md-9">
        <div class="card-box">
            <div class="table-rep-plugin">
                <div class="row mb-3">
                    <?php 
                        $gvnn = get_info_giaovien(get_idgvnn_course($id));
                        $gvtg = get_info_giaovien(get_idgvtg_course($id)); ?>
                    <div class="col-md-6 form-group">
                        <label><?php print_r(get_string('foreignteacher'))?>: <?php echo $gvnn->lastname.' '.$gvnn->firstname ?></label>
                    </div>
                    <div class="col-md-6 form-group">
                        <label><?php print_r(get_string('tutorsteacher'))?>: <?php echo $gvtg->lastname.' '.$gvtg->firstname ?></label>
                    </div>
                    <div class="col-md-6 form-group">
                        <label><?php print_r(get_string('class'))?>: <?php echo !empty(get_info_class_of_course($id)) ? get_info_class_of_course($id)->name : "<span class=\"badge badge-secondary\">Chưa được gán cho lớp</span>" ?></label>
                    </div>
                    <div class="col-md-6 form-group">
                        <label><?php print_r(get_string('schools'))?>: <?php echo !empty(get_info_class_of_course($id)->id_truong) ? get_info_truong(get_info_class_of_course($id)->id_truong)->name : "<span class=\"badge badge-secondary\">Chưa được gán cho trường</span>" ?></label>
                    </div>
                </div>
                <form action="" method="get" accept-charset="utf-8">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <input id="foo-search" type="text" placeholder="<?php print_r(get_string('search'))?>..." class="form-control" autocomplete="on">
                        </div>
                    </div>
                </form>
                <?php 
                    if(in_array($roleid,array(8,10))){
                        ?>
                        <div class="table-responsive" data-pattern="priority-columns">
                            <!-- <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post"> -->

                            <table id="table-foo" class="table table-striped table-bordered" data-page-size="15">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th><?php print_r(get_string('codestudent'))?></th>
                                        <th><?php print_r(get_string('namestudent'))?></th>
                                        <th><?php print_r(get_string('sex'))?></th>
                                        <th><?php print_r(get_string('birthday'))?></th>
                                        <th><?php print_r(get_string('time_start'))?></th>
                                        <th><?php print_r(get_string('time_end'))?></th>
                                        <th><?php print_r(get_string('description'))?></th>
                                    </tr>
                                </thead>
                                
                                <tbody id="ajax_nhomhs">
                                    <?php 
                                    $i=1; 
                                    foreach ($members as $key => $val) {
                                        $member_arr = $val->id;
                                     ?>
                                    <tr>
                                        <td><?php echo $i; $i++ ?></td>
                                        <td><?php echo $val->code ?></td>
                                        <td><?php echo $val->lastname.' '.$val->firstname ?></td>
                                        <td><?php if($val->sex==1){
                                                echo "Nữ";
                                            }
                                            if($val->sex==2){
                                                echo "Nam";
                                            }?>
                                        </td>
                                        <td><?php echo $val->birthday ?></td>
                                        <td><?php echo $val->time_start ?></td>
                                        <td><?php echo $val->time_end ?></td>
                                        <td><?php echo $val->note_for_teacher ?></td>
                                       
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php

                    }else{

                        ?>
                        <div class="table-responsive" data-pattern="priority-columns">
                            <!-- <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post"> -->

                            <table id="table-foo" class="table table-striped table-bordered" data-page-size="15">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <?php 
                                            if(!empty($check_xoacon)){
                                                ?>
                                                <th class="text-right"><?php print_r(get_string('action')) ?></th>
                                                <?php 
                                            }
                                         ?>
                                        <th><?php print_r(get_string('codestudent'))?></th>
                                        <th><?php print_r(get_string('namestudent'))?></th>
                                        <th><?php print_r(get_string('sex'))?></th>
                                        <th><?php print_r(get_string('birthday'))?></th>
                                        <th><?php print_r(get_string('email'))?></th>
                                        <th><?php print_r(get_string('parent'))?></th>
                                        <th><?php print_r(get_string('phone'))?></th>
                                        <th><?php print_r(get_string('time_start'))?></th>
                                        <th><?php print_r(get_string('time_end'))?></th>
                                        <th><?php print_r(get_string('description'))?></th>
                                        
                                        
                                    </tr>
                                </thead>
                                
                                <tbody id="ajax_nhomhs">
                                    <?php $i=1; foreach ($members as $key => $val) {
                                                   $member_arr = $val->id;
                                                    ?>
                                    <tr>
                                        <td><?php echo $i; $i++ ?></td>
                                        <td class="text-right">
                                            <?php 
                                                if(!empty($check_gancon)){
                                                   ?>
                                                   <button data-toggle="modal" data-target="#groups" class="mauicon tooltip-animation userid btnid" title="Chuyển nhóm lớp" value="<?php echo $val->id ?>"><i class="fa fa-arrow-circle-left"></i></button>
                                                   <?php  
                                                }
                                            ?>
                                            
                                            <?php 
                                            if(!empty($check_xoacon)){
                                                ?>
                                                    <a onclick="return Confirm('Xóa học sinh','Bạn có muốn xóa <?php echo $val->firstname ?> <?php echo $val->lastname ?> khỏi nhóm lớp?','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/people/people.php?action=remove_course_person&user_id=<?php echo $val->id ?>&course_id=<?php echo $course->id ?>&confirm=yes')" class="mauicon tooltip-animation" id="" title="Xóa học sinh">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    </a>
                                                    <?php 
                                            }
                                         ?>
                                        </td>
                                        <td><?php echo $val->code ?></td>
                                        <td><?php echo $val->lastname.' '.$val->firstname ?></td>
                                        <td><?php if($val->sex==1){
                                                echo "Nữ";
                                            }
                                            if($val->sex==2){
                                                echo "Nam";
                                            }?>
                                        </td>
                                        <td><?php echo $val->birthday ?></td>
                                        <td><?php echo $val->email ?></td>
                                        <td><?php echo $val->name_parent ?></td>
                                        <td><?php echo $val->phone1 ?></td>
                                        
                                        <td><?php echo $val->time_start ?></td>
                                        <td><?php echo $val->time_end ?></td>
                                        <td><?php echo $val->description ?></td>
                                        
                                                
                                        
                                       <!--  <td class="text-right">
                                            <button data-toggle="modal" data-target="#groups" class="btn btn-custom waves-light waves-effect btn-sm tooltip-animation userid" title="Chuyển nhóm lớp" value="<?php echo $val->id ?>"><i class="fa fa-arrow-circle-left"></i></button>
                                            <a onclick="return Confirm('Xóa học sinh','Bạn có muốn xóa <?php echo $val->firstname ?> <?php echo $val->lastname ?> khỏi nhóm lớp?','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/people/people.php?action=remove_course_person&user_id=<?php echo $val->id ?>&course_id=<?php echo $course->id ?>&confirm=yes')" class="btn waves-effect waves-light btn-danger btn-sm tooltip-animation" id="" title="Xóa học sinh">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                        </td> -->
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <?php 
                                if(!empty($check_xoacon)){
                                ?>
                                    <tfoot>
                                        <tr class="active">
                                            <td colspan="11">
                                                <div class="float-right">
                                                     <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10 m-b-0"></ul>
                                                </div>
                                                <div class="float-left">
                                                    <a class="btn btn-danger" onclick="return Confirm('Xóa học sinh','Bạn có muốn xóa tất cả học sinh khỏi nhóm lớp','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/people/people.php?action=remove_bulk_person_from_course&id=<?php echo $course->id ?>')"><?php print_r(get_string('deleteall')) ?></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                <?php 
                                }
                                ?>
                                <!-- <tfoot>
                                    <tr class="active">
                                        <td colspan="11">
                                            <div class="float-right">
                                                <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10 m-b-0"></ul>
                                            </div>
                                            <div class="float-left">
                                                <a class="btn btn-danger" onclick="return Confirm('Xóa học sinh','Bạn có muốn xóa tất cả học sinh khỏi nhóm lớp','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/people/people.php?action=remove_bulk_person_from_course&id=<?php echo $course->id ?>')"><?php print_r(get_string('deleteall')) ?></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot> -->
                            </table>
                        </div>
                        <?php
                    }

                ?>
                
                
            </div>
            <span id="maincontent"></span>                 
        </div>
    </div>

    <!-- Danh sách nhóm lớp -->
    <div id="groups" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dcdcn" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">                                                         
                <form action="<?php echo $CFG->wwwroot ?>/course/people/people.php?action=assign_people_change_course&course_id=<?php echo $id ?>" method="post">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="cc"><?php echo get_string('listclassgroup'); ?></h4>
                            <input type="hidden" value="" name="user_id" id="user_id"/>
                        </div>
                        <div class="modal-body">
                            <div data-pattern="priority-columns" id="assignResults">
                                <table id="datatable2" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?php echo get_string('classgroup');?></th>
                                            <th><?php echo get_string('class'); ?></th>
                                            <!-- <th>Ngày sinh</th> -->
                                            <th class="text-right"><?php echo get_string('status'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php foreach ($toGroup as $gr) { ?>
                                        <tr>
                                            <td>
                                                <input type="radio" value="<?php echo $gr->enrolid; ?>" name="change_assign">
                                                <input type="text" value="<?php echo $gr->id; ?>" name="course_change" hidden>
                                            </td>
                                           <td><?php echo $gr->fullname; ?></td>
                                           <td><?php echo $gr->name; ?></td>
                                           <td></td>
                                        </tr>
                                        
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light waves-effect" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
                            <button type="submit" class="btn btn-success waves-effect waves-light"><?php echo get_string('transfer_group'); ?></button>
                        </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
    <div class="col-md-3">
        <div class="card-box ">
            <ul class="metismenu list-unstyled">
                <?php 
                     $checkpopup = check_chuc_nang_pop_up($roleid,$moodle);
                     if($checkpopup){
                        ?>
                        <li class="form-group">
                            <a href="#" data-toggle="modal" data-target="#overlay" class="btn btn-success btn-sm btn-block"> 
                            <?php print_r(get_string('assignpeopletothisteam')) ?></a>
                        </li>
                        
                        <?php 
                     }
                 ?>
                
                <!-- <li class="form-group">
                    <a alt="Import new people" href="<?php echo $CFG->wwwroot ?>/course/people/import_people.php?id=<?php echo $id ?>" class="btn btn-light btn-block btn-sm"> <?php print_r(get_string('importnew')) ?></a>
                </li> -->
                <!-- <li class="form-group">
                    <a alt="Thêm điểm unit" href="<?php echo $CFG->wwwroot ?>/manage/manage_score/add_unit_course.php?idcourse=<?php echo $id ?>" class="btn btn-light btn-block btn-sm"> Thêm điểm unit</a>
                </li>
                <li class="form-group">
                    <a alt="Danh sách bài test" href="<?php echo $CFG->wwwroot ?>/manage/examination/list_quiz_course.php?idcourse=<?php echo $id ?>" class="btn btn-light btn-block btn-sm"> Danh sách bài test</a>
                </li>
                <li class="form-group">
                    <a alt="<?php print_r(get_string('addclassschedule')) ?>" href="<?php echo $CFG->wwwroot ?>/manage/schedule/addnew.php?courseid=<?php echo $id ?>" class="btn btn-light btn-block btn-sm"> <?php print_r(get_string('addclassschedule')) ?></a>
                </li> -->
            </ul>
                      
        </div>                    
    </div><!-- End Right -->
</div>


<?php
    echo $OUTPUT->footer();
?>

<!-- Modal Học Sinh -->

<div id="overlay" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?php if(!empty(get_info_class_of_course($id))){ ?>                                                          
            <form action="<?php echo $CFG->wwwroot ?>/course/people/people.php?action=assign_people_to_course&course_id=<?php echo $id ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo get_string('list_student'); ?> <b><?php echo get_info_class_of_course($id)->name ?></b></h4>
                    <!-- <input type="text" class="form-control col-6" autocapitialize="off" autocorrect="off" autocomplete="off" name="s" id="assignSearchCourse" placeholder="Tìm kiếm..."> -->
                </div>
                <div class="modal-body">

                    <div id="assignResults">
                        <table class="datatable table table-bordered">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="" id="checkAll"></th>
                                    <th><?php echo get_string('full_name'); ?></th>
                                    <th><?php echo get_string('email_new'); ?></th>
                                    <th><?php echo get_string('birthday'); ?></th>
                                    <th class="disabled-sorting text-right"><?php echo get_string('status'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $users_of_parent = get_users_from_parent_second($id);
                                    foreach ($users_of_parent as $key => $val) {
                                ?>
                                <tr>
                                    <td>
                                        <?php if (!empty(get_members_in_groups($val->id))) { ?>
                                            &nbsp;
                                        <?php } else { ?>
                                            <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $val->firstname ?> <?php echo $val->lastname ?></td>
                             
                                    <td><?php echo $val->email ?></td>
                                    <td><?php echo $val->birthday ?></td>
                                    <td class="text-right">&nbsp;
                                        <?php if(!empty(get_members_in_groups($val->id))) { ?>
                                            <span title="" class="badge badge-success">
                                                <span>
                                                    <?php print_r(get_string('alreadyassigned')) .print_r(' cho '). print_r(get_members_in_groups($val->id)->fullname) ?>
                                                </span>
                                            </span>                       
                                        <?php } else {
                                            ?>
                                            <span title="" class=""><span>
                                                </span></span>                       
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
                    <button type="submit" class="btn btn-success waves-effect waves-light"><?php print_r(get_string('assign', 'cohort')) ?></button>
                </div>
            </form>
            <?php }else{ ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4 class="alert alert-warning">Vui lòng gán nhóm lớp cho Lớp học trước!</h4>
                </div>
            <?php }?>
        </div><!-- /.modal-content -->
   
    </div><!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    $(".userid").click(function(){
        $("#user_id").val($(this).val());
    })

</script>
<style>
    .mauicon{
        color:#106c37 !important;
    }
    .mauicon:hover{
        color:#4bb747 !important;
    }
</style>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
    .btnid{
      border: none;
      background: none !important;
    }
</style>