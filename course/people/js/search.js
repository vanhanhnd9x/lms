$(document).ready(function(){
    $('#li_dashboard').removeClass('active');
    $('#li_people').removeClass('active');
    $('#li_course').addClass('active');
    $('#assignCancel').click(function() {
        $('#facebox').hide();
        $('#overlay').fadeOut('fast');
  
    });

    $("#check_completed").click(function(event) {   
        if(this.checked) {
            document.getElementById('text_completed').value=1;
        
        }
        else { 
            document.getElementById('text_completed').value=0;
        
        }
    });
    
    $('#assignSearchCourse').focus(function() {
        $('#searchPrompt').html('');
    });
    $('#assignSearchCourse').keydown(function() {
        $('#searchPrompt').html('');
    });

     $('#assignSearchCourse').keyup(function() {
        var course_id=document.getElementById('course_id').value;
    
        if($(this).val().length>0){
            search_people_to_assign($(this).val(),course_id);
        }
        else{
            $('#searchPrompt').html('Tìm kiếm học viên để gán');
            search_people_to_assign('',course_id);
        }
    });

    $(function() {
        $('#set_status_link').click(function(){
            $('#overlay').fadeIn('fast',function(){
                $('#facebox_completion_course').show();
            });
        });
    });
 
 
    $(function() {
        $('.edit_course_module_result_link').click(function(){
            $('#overlay').fadeIn('fast',function(){
                $('#facebox_completion_course_module').show();
            });
        });
    });
 
 
    $('#cancelUpdateCourseModule').click(function() {
        
        $('#facebox_completion_course_module').hide();
        $('#overlay').fadeOut('fast');
  
    });


    $('#cancelUpdate').click(function() {
        $('#facebox_completion_course').hide();
        $('#facebox_completion_course_module').hide();
        $('#overlay').fadeOut('fast');
  
    });


    $(function() {
        $('#assignPeople').click(function(){
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    $(function() {
        $('#assignCourses').click(function(){
            
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    $(function() {
        $('#addTeams').click(function(){
            
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    
    //    $("#compliant").change(function() { 
    //    alert($("#compliant").val());    
    //});
    
    
    
    //send mail notification when assing user to course
    
    $('#SendEmail').click(function(event) {   
        if(this.checked) {
            document.getElementById('send_email_notification').value=1;
           
        }
        else { 
            document.getElementById('send_email_notification').value=0; 
           
        }
    });
//finish send mail notification when assing user to course
    
   
});

function search_complete(complete,course_id,url){
    window.location=url+"/course/people/search_complete_status.php?id="+course_id+"&status="+complete;
}

function displayConfirmBox(redirectPath,id,ms){
    var where_to= confirm(ms);
    if (where_to== true)
    {
        window.location=redirectPath;
    }
    else
    {
	
        return false;
    }
}

function limit_text(limitNum) {
	
	
    if (document.getElementById('email_list').value.length > limitNum) {
        document.getElementById('email_list').value = document.getElementById('email_list').value.substring(0, limitNum);
    } else {
        document.getElementById('EmailList_counter').innerHTML=limitNum - document.getElementById('email_list').value.length+" character left";
    }
	
}
function open_popup_cmc_form(hidden_cm_id,user_id,course_id){
        
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            
            document.getElementById("facebox_completion_course_module").innerHTML=xmlhttp.responseText;
               
            $("#facebox_completion_course_module").show();
            var searchElement = document.getElementById("facebox_completion_course_module");
            var scripts = searchElement.getElementsByTagName('script');
            for (var i=0;i<scripts.length;i++) {
                eval(scripts[i].innerHTML);
            }
                
        }
    }
    
    xmlhttp.open("GET","ajax_display_cmc_popup.php?hidden_cm_id="+hidden_cm_id+'&user_id='+user_id+'&course_id='+course_id,true);
    xmlhttp.send();
    
}


function search_people_to_assign(search_val,course_id){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("assignResults").innerHTML=xmlhttp.responseText;
            
        }
    }
    xmlhttp.open("GET","livesearch_people_assign_to_course.php?q="+search_val+'&course_id='+course_id,true);
    xmlhttp.send();
}
