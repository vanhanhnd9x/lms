<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 *
 * @author
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');
require($CFG->dirroot . '/common/lib.php');
//require('lib.php');
//require($CFG->dirroot . '/common/lib.php');
require_login(0, false);
$id = optional_param('id', 0, PARAM_INT); //course_id
$status = optional_param('status', '', PARAM_TEXT);
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
//require($CFG->dirroot . '/common/lib.php');
$member_arr = array();
foreach ($members_without_search as $key => $val) {
    $member_arr[] = $val->id;
}
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<div id="page-content">
    <div id="region-main-box">
        <div id="region-post-box">
            <div id="region-main-wrap">
                <div id="region-main">
                    <div class="region-content">
                        <span id="maincontent"></span>
                        <?php
                        $members_without_search = get_members_enroll_of_course($id, $USER->id);
                        $members = search_complete_status($status, $id, $USER->id);
                        ?>
                        <div id="admin-fullscreen">
                            <div class="focus-panel" id="admin-fullscreen-left">
                                <!-- Left Content -->
                                <div class="body">
                                    <div class="course-content"><div class="panel-head clearfix">
                                            <h3><?php echo $course->fullname ?></h3>
                                            <div class="subtitle"><?php echo $course->idnumber ?></div>
                                            <div class="subtitle">
                                                <?php echo $course->summary ?>
                                            </div>
                                        </div>


                                        <div class="tabtree">
                                            <ul class="tabrow0">
                                                <li><a title="Content" href="<?php echo $CFG->wwwroot ?>/course/view.php?id=<?php echo $id ?>"><span><?php print_r(get_string('content')) ?></span></a> </li>
                                                <li class="selected"><a title="People" href="<?php echo $CFG->wwwroot ?>/course/people/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('people')) ?></span></a> </li>
                                                <li><a title="Teams" href="<?php echo $CFG->wwwroot ?>/course/team/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('teams')) ?></span></a> </li>
                                                <li><a title="Noticeboard" href="<?php echo $CFG->wwwroot ?>/mod/noticeboard/view.php?id=<?php echo $id ?>"><span><?php print_r(get_string('noticeboard')) ?></span></a> </li>
                                                <li><a title="Documents" href="<?php echo $CFG->wwwroot ?>/course/document/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('documents')) ?></span></a> </li>
                                                <li><a title="Gradebook" href="<?php echo $CFG->wwwroot ?>/grade/report/grader/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('gradebook')) ?></span></a> </li>
                                                <?php
                                                if (!is_teacher()) {
                                                    ?>
                                                    <li class="last"><a title="Settings" href="<?php echo $CFG->wwwroot ?>/course/settings/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('settings')) ?></span></a> </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="grid-search">
                                            <form action="<?php echo $CFG->wwwroot ?>/course/people/search_people.php?id=<?php echo $id ?>" method="post">
                                                <div class="float-left">
                                                    <input type="text" name="search_name" id="s" class="grid-search-box"><input type="submit" class="grid-search-button" value="<?php print_string('search') ?>"></div>
                                                <div class="align-right">
                                                    <select name="status" onChange="search_complete(this.value,<?php echo $id ?>, '<?php echo $CFG->wwwroot ?>');" id="compliant" class="days-remaining grid-search-combo">
                                                        <?php
                                                        $select_all = '';
                                                        if ($status == '') {
                                                            $select_all = 'selected="selected"';
                                                        }

                                                        $select_completed = '';
                                                        if ($status == '1') {
                                                            $select_completed = 'selected="selected"';
                                                        }

                                                        $select_not_completed = '';
                                                        if ($status == '0') {
                                                            $select_not_completed = 'selected="selected"';
                                                        }
                                                        ?>
                                                        <option value="" <?php echo $select_all ?>><?php print_r(get_string('all')) ?></option>
                                                        <option value="1" <?php echo $select_completed ?>><?php print_r(get_string('complete')) ?></option>
                                                        <option value="0" <?php echo $select_not_completed ?>><?php print_r(get_string('notcomplete')) ?></option>
                                                    </select></div>
                                                <div class="clear"></div>
                                            </form>
                                        </div>

                                        <form method="post" id="UsersForm" action="/admin/courses/16298/bulkaction"><input type="hidden" value="" name="Action" id="Action">
                                            <table class="item-page-list">

                                                <tbody>
                                                    <?php
                                                    foreach ($members as $key => $val) {
                                                        $member_arr[] = $val->id;
                                                        ?>
                                                        <tr id="u137192">
                                                            <td><input type="checkbox" id="su137192" class="row-select" name="SelectedIds" value="137192"></td>
                                                            <td class="main-col">
                                                                <div class="title">
                                                                    <a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $val->id ?>">

                                                                        <?php echo $val->firstname ?> <?php echo $val->lastname ?>
                                                                    </a>
                                                                    <a href="<?php echo $CFG->wwwroot ?>/user/profile.php?id=<?php echo $val->id ?>" class="pop-profile"></a>
                                                                </div>
                                                                <div class="tip">
                                                                    <?php echo $val->email ?>
                                                                </div>
                                                            </td>
                                                            <td class="nowrap align-right">
                                                                <?php
                                                                $count = count_course_completion($course, $val->id);
                                                                if ($count == 100) {
                                                                    ?>
                                                                    <span title="" class="box-tag box-tag-green "><span><?php print_r(get_string('complete')) ?></span></span>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <span title="" class="box-tag box-tag-grey "><span><?php echo $count; ?>%<br><?php print_r(get_string('complete')) ?></span></span>
                                                                <?php } ?>
                                                            </td>
                                                            <td class="nowrap">
                                                                <a title="" class="box-tag box-tag-grey " href="<?php echo $CFG->wwwroot ?>/course/people/edit_result.php?id=<?php echo $id; ?>&user_id=<?php echo $val->id ?>"><span><?php print_r(get_string('editthis')) ?></span></a>
                                                            </td>

                                                        </tr>
                                                    <?php } ?>

                                                </tbody></table>
                                            <input type="hidden" value="" name="TimeLimitType" id="TimeLimitType">
                                            <input type="hidden" value="" name="TimeSpanType" id="TimeSpanType">
                                            <input type="hidden" value="" name="TimeSpanInterval" id="TimeSpanInterval">
                                            <input type="hidden" value="" name="TimeLimitDate" id="TimeLimitDate">
                                        </form>

                                        <div class="list-pager">
                                            <ul>

                                            </ul>
                                            <div class="clear"></div>
                                        </div>
                                        <?php
                                        $person_str = get_string('person');
                                        if (count($members) > 1) {
                                            $person_str = get_string('person');
                                        }
                                        ?>
                                        <div class="tip center-padded"><?php echo count($members) . " " . $person_str ?></div>
                                    </div>
                                </div>
                            </div><!-- Left Content -->
                            <script src="<?php echo $CFG->wwwroot ?>/blocks/course_addmodule/addmodules.js" type="text/javascript"></script>
                            <!-- Right Block -->
                            <div id="admin-fullscreen-right">

                                <div class="side-panel">
                                    <div class="action-buttons">

                                        <ul>
                                            <li><a alt="Assign to existing people" onclick="" href="#" class="big-button drop" id="addTeams"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print_r(get_string('assigntoexist')) ?></span></span></span></span></a></li>
                                            <?php
                                            if (!is_teacher()) {
                                                ?>
                                                <li>
                                                    <a alt="Import new people" onclick="" href="<?php echo $CFG->wwwroot ?>/course/people/import_people.php?id=<?php echo $id ?>" class="big-button drop" id="addTeams">
                                                        <span class="left">
                                                            <span class="mid">
                                                                <span class="right">
                                                                    <span class="icon_add"><?php print_r(get_string('importnew')) ?></span>
                                                                </span>

                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                        <!--                                        <ul>
                                                                                    <li><div class="tip">Perform a bulk action on all users in the selected teams</div></li>
                                                                                    <li>
                                                                                        <select name="bulkAction" id="bulkAction"><option value=""></option>
                                                                                            <option value="RESETTEAM">Reset course results</option>
                                        
                                                                                        </select>
                                                                                    </li>
                                                                                </ul>-->
                                    </div>
                                </div>
                            </div>
                        </div>  <!-- End Right -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay" id="overlay" style="display:none;"></div>
    <div id="facebox" style="display: none;">
        <div class="popup">         
            <div class="content">
                <div id="assignItemBox">
                    <div class="assign-header">
                        <div class="assign-search">
                            <form action="" method="get">
                                <div class="field-help-box">
                                    <label id="searchPrompt" for="assignSearch"><?php print_r(get_string('searchforpeople')) ?></label><input type="text" autocapitialize="off" autocorrect="off" autocomplete="off" name="s" id="assignSearchCourse">
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="<?php echo $CFG->wwwroot ?>/course/people/people.php?action=assign_people_to_course&course_id=<?php echo $id ?>" method="post">
                        <div id="assignResults">
                            <table cellspacing="0" class="item-page-list">
                                <tbody>
                                    <?php
                                    $users_of_parent = get_users_from_parent($USER->id);
                                    foreach ($users_of_parent as $key => $val) {
                                        ?>
                                        <tr class="c1">
                                            <td>
                                                <?php if (in_array($val->id, $member_arr)) { ?>
                                                    &nbsp;
                                                <?php } else { ?>
                                                    <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                                                <?php } ?>
                                            </td>
                                            <td class="main-col">
                                                <div><label for="u15539"><?php echo $val->firstname ?> <?php echo $val->lastname ?> <?php echo $val->email ?></label></div>
                                            </td>
                                            <td class="nowrap">&nbsp;
                                                <?php if (in_array($val->id, $member_arr)) { ?>
                                                    <span title="" class="box-tag box-tag-green "><span>
                                                            <?php print_r(get_string('alreadyassigned')) ?>
                                                        </span></span>
                                                <?php } else { ?>
                                                    <span title="" class=""><span>
                                                        </span></span>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="assign-footer-opt clearfix hidden">
                            <span id="assignEmails"><input type="checkbox" name="SendEmail" id="SendEmail" checked> <label for="SendEmail"><?php print_r(get_string('sendemailnoti')) ?></label></span>
                            <input type="hidden" name="send_email_notification" id="send_email_notification" value="1"/>
                        </div>
                        <div class="assign-footer clearfix">
                            <div class="float-left">
                                <input type="submit" class="big-button drop" value="<?php print_r(get_string('assign', 'cohort')) ?>" />
                                <?php print_r(get_string('or')) ?> <a id="assignCancel" href="#"><?php print_r(get_string('cancel')) ?></a></div>
                        </div>
                    </form>
                </div></div></div></div>
</div>
