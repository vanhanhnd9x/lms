<script type="text/javascript">
    
    $("#check_completed").click(function(event) {
        if(this.checked) {
            document.getElementById('text_completed').value=1;
        }
        else {
            document.getElementById('text_completed').value=0;
        }
    }); 
    
    $('#cancelUpdateCourseModule').click(function() {
        
        $('#facebox_completion_course_module').hide();
        $('#overlay').fadeOut('fast');
  
    });
    
</script>
<?php
global $CFG, $USER;
require('../../config.php');

require_once($CFG->dirroot . "/lib/modinfolib.php");
require_once($CFG->dirroot . "/lib/completionlib.php");
require_once($CFG->dirroot . "/lib/completion/completion_completion.php");
require($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot.'/common/calendar/classes/tc_calendar.php');
require($CFG->dirroot . '/mod/quiz/lib.php');
// Require Login.
require_login(0, FALSE);
global $DB;
$hidden_cm_id = $_GET["hidden_cm_id"];
$user_id = $_GET["user_id"];
$course_id = $_GET["course_id"];
$course = $DB->get_record('course', array('id' => $course_id));
$cm = new course_modinfo($course, $user_id);
$cm_object = $cm->get_cm($hidden_cm_id);
?>


<div class="popup2">         <div class="content">
        <div class="assign-header">
            <div class="tip">Change the result for</div>
            <div style="font-weight: bold;color: white"><?php echo $cm_object->name; ?></div>
        </div>
        <div class="padded">

            <form method="post" action="<?php echo $CFG->wwwroot ?>/course/people/edit_result.php?action=update_cm_status">
                <input type="hidden" name="id" id="id"  value="<?php echo $course_id ?>"/>
                <input type="hidden" name="cm_id" id="id"  value="<?php echo $hidden_cm_id ?>"/>
                <input type="hidden" name="user_id" id="id"  value="<?php echo $user_id ?>"/>
                <table class="simple-form">
                    <tbody>
<?php if ($cm_object->module == 13) { ?>
                            <tr>
                                <th>Attempts:</th>
                                <td>
                                    <?php 
                                    $quiz = $DB->get_record('quiz', array('id' => $cm_object->instance));
                                    $attempts = quiz_get_user_attempts($quiz->id, $user_id, 'finished', true);
                                    //var_dump($attempts);
                                    echo count($attempts);
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <th>Score:</th>
                                <td>
                                    <input type="text" value="<?php echo trim(trim(get_highest_score_quiz($user_id,$cm_object->instance), '0'), '.'); ?>" style="width:40px;" name="score" maxlength="4" id="score" class="align-right"></td>
                            </tr>

                            <tr>
                                <th>Passmark:</th>
                                <td><?php echo get_pass_mark_quiz($cm_object->instance); ?></td>
                            </tr>
<?php } ?>
                        <tr>
                            <th>Completed:</th>
                            <td>
                                <input type="checkbox" name="check_completed" id="check_completed" <?php echo is_cm_complete($course, $hidden_cm_id, $user_id) == 1 ? 'checked="checked"' : ''; ?>>
                                <input type="hidden" name="text_completed" id="text_completed"  value="<?php echo is_cm_complete($course, $hidden_cm_id, $user_id) == 1 ? '1' : '0'; ?>"/>
                            </td>
                        </tr>
<?php if ($cm_object->module == 13) { ?>
                            <tr>
                                <th>Date Completed:</th>
                                <?php 
                                $quiz = $DB->get_record('quiz', array('id' => $cm_object->instance));
                                $higest_score=get_highest_score_quiz_obj($user_id, $quiz->id);
                                //var_dump($higest_score);
                                ?>
                                <td>
                                
          <?php
          $default_date=date('Y-m-d',$higest_score->timefinish); 
          $default_date_arr=explode('-',$default_date);
          $default_day=$default_date_arr[2];
          $default_month=$default_date_arr[1];
          $default_year=$default_date_arr[0];
	  $myCalendar = new tc_calendar("date_completed", true, false);
          $myCalendar->setIcon($CFG->wwwroot."/common/calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($default_day, $default_month, $default_year);
	  $myCalendar->setPath($CFG->wwwroot."/common/calendar/");
	  $myCalendar->setDateFormat('j F Y');
	  $myCalendar->setAlignment('left', 'bottom');
	  $myCalendar->writeScript();
	  ?>
                                </td>
                                              </tr>
                            <tr>
                                <th>Time taken:</th>
                                <td>
                                    <?php 
                                    echo round(abs($higest_score->timefinish - $higest_score->timestart) / 60,2). " minute";

                                    ?>
                                </td>
                            </tr>
<?php } ?>
                        <tr>
                            <th>Reason for change:</th>
                            <td><textarea rows="2" name="Note" id="Note" cols="20"></textarea></td>
                        </tr>
                    </tbody></table>
                <div class="form-buttons">
                    <input type="submit" value=" Save " class="btnlarge"><span> or </span>
                    <a id="cancelUpdateCourseModule" href="#">
                        cancel
                    </a>
                </div>
            </form> 
        </div></div>       </div>