<?php
require_once('../config.php');
$PAGE->set_title("Teams");
$PAGE->set_heading("Teams");

$PAGE->set_pagelayout('team');
echo $OUTPUT->header();
// Long added

?>
<div id="admin-fullscreen">

    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <?php 
            $currenttab = 'teams';
                include_once('managetabs.php');
            ?>
            
                <h3>
                   Assign groups of people to your course

Click "Assign to teams" and all members of that team will be notified of the new course.

                </h3>

                
            
            </div>       
        </div><!-- Left Content -->
        <!-- Right Block -->
        <div id="admin-fullscreen-right">

            <div class="side-panel">
                <div class="action-buttons">
                    <ul>
                        <li>
                            <a href="<?php print new moodle_url('/'); ?>" class="big-button drop">
                                <span class="left">
                                    <span class="mid">
                                        <span class="right">
                                            <span class="icon_add"> Assign to teams</span>
                                        </span></span></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>  <!-- End Right -->

    </div>      

    <?php echo $OUTPUT->footer(); ?>
