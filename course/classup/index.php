<?php
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}
global $CFG, $USER,$DB;
require('../../config.php');
// require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/teams/lib.php');

require_login(0, false);
$team_id = optional_param('team_id', 0, PARAM_TEXT);

// require_once('lib.php');
require_login(0, false);
$PAGE->set_title('Chuyển học sinh lên lớp');
$PAGE->set_heading('Chuyển học sinh lên lớp');
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$list_members = get_members_in_class($team_id);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='groups';
$check_xoacon=check_chuc_nang_xoachucnangcong($roleid,$moodle);
$name1='toclass';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
?>


<?php if(count($list_members) >0) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <form id="demoform" action="<?php echo $CFG->wwwroot  ?>/teams/team.php" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card-box table-bordered">
                            <div class="p-2 text-white bg-success text-center">
                                <b>Thông tin lớp cũ</b>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Năm học:</label>
                                <div class="col-md-9">
                                    <select name="school_year_id" class="form-control" readonly>
                                        <option value="<?php echo get_info_course_of_group($team_id)->id_namhoc ?>"><?php echo get_info_namhoc(get_info_course_of_group($team_id)->id_namhoc)->sy_start .' - '. get_info_namhoc(get_info_course_of_group($team_id)->id_namhoc)->sy_end ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Trường:</label>
                                <div class="col-md-9">
                                    <select name="schoolid" class="form-control" readonly>
                                        <option value="<?php echo get_info_course_of_group($team_id)->id_truong ?>"><?php echo get_info_truong(get_info_course_of_group($team_id)->id_truong)->name ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Khối lớp:</label>
                                <div class="col-md-9">
                                    <select name="block_student_old" class="form-control" readonly>
                                        <option value="<?php echo get_info_course_of_group($team_id)->id_khoi ?>"><?php echo get_info_khoi(get_info_course_of_group($team_id)->id_khoi)->name ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Lớp học:</label>
                                <div class="col-md-9">
                                    <select name="class_old" class="form-control" readonly>
                                        <option value="<?php echo $team_id ?>"><?php echo get_info_course_of_group($team_id)->name ?></option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="mt-4 mb-4"><hr/></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã học sinh</th>
                                        <th>Họ tên</th>
                                        <th>Nhóm lớp</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        $i=1; foreach ($list_members as $key => $val) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i; $i++; ?></td>
                                        <td><?php echo $val->code ?></td>
                                        <td>
                                            <input type="text" name="studentid[]" value="<?php echo $val->id ?>" placeholder="" hidden>
                                            <?php echo $val->firstname ?> <?php echo $val->lastname ?> 
                                        </td>
                                        
                                        <td>
                                            <input type="text" name="groupsid[]" value="<?php echo get_user_to_groups($val->id)->id ?>" placeholder="" hidden>
                                            <?php echo get_user_to_groups($val->id)->fullname; ?>
                                                
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>

                            
                            </table>
                        </div>
                    </div>

                    <!-- Thông tin lớp mới -->

                    <div class="col-md-2">
                        <ul class="list-unstyled text-center" style="margin-top: 50vh;">
                           
                            <li class="mb-4">
                                <button type="submit" class="btn btn-light"><i class="mdi mdi-arrow-up-bold-box-outline" style="font-size: 2rem"></i></button>
                            </li>
                            <input type="hidden" value="groups_up" name="action">
                        </ul>
                    </div>

                    <div class="col-md-5">
                        <div class="card-box table-bordered">
                            <div class="p-2 text-white bg-success text-center">
                                <b>Thông tin lớp mới</b>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Năm học:</label>
                                <div class="col-md-9">
                                    <select name="" class="form-control" readonly>
                                        <?php $year_new = get_info_namhoc(get_info_course_of_group($team_id)->id_namhoc)->sy_end ?>
                                        <option value="">
                                            <?= $year_new .' - '. ($year_new+1); ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">
                                    <?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <select name="schoolid_new"  class="form-control" readonly>
                                        <option value="<?php echo get_info_course_of_group($team_id)->id_truong ?>">
                                            <?php echo get_info_truong(get_info_course_of_group($team_id)->id_truong)->name ?>
                                        </option>
                                        <!-- -->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">
                                    <?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <?php 
                                        $schoolid = get_info_course_of_group($team_id)->id_truong;
                                        $status = get_info_khoi(get_info_course_of_group($team_id)->id_khoi)->status;
                                    ?>
                                    <select name="block_student_new" id="block_student" class="form-control" readonly>
                                        <option value="<?php echo get_status_khoi($schoolid,$status+1)->id; ?>">
                                            <?php echo get_status_khoi($schoolid,$status+1)->name; ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">
                                    <?php echo get_string('class'); ?><span style="color:red">*</span></label>
                                <div class="col-md-9">
                                    <?php $name_class_old =  get_info_course_of_group($team_id)->name; ?>
                                    <input type="text" name="group_new" value="<?= substr($name_class_old,0,6).(substr($name_class_old,6,1)+1).(substr($name_class_old,7)) ?>" readonly class="form-control">
                                </div>
                            </div>
                            
                            <div class="mt-4 mb-4"><hr/></div>
                            
                        </div>
                    </div>
                </div>
                <!-- <button type="submit" class="btn btn-success btn-block">Chuyển lớp</button> -->
            </form>
        </div>
    </div>
</div>
<?php }else{ ?>
    <div class="alert alert-warning">
        Lớp học không tồn tại học sinh! <a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?= $team_id ?>">Quay lại</a>
    </div>
<?php } ?>


<?php 
    echo $OUTPUT->footer();
?>