<?php
//  Display the course home page.

require_once('../../config.php');
require_once('../lib.php');
require_once($CFG->dirroot . '/mod/forum/lib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/common/lib.php');
$id = optional_param('id', 0, PARAM_INT);
$mobile = detect_mobile();
   
if($mobile === true){
    echo displayJsAlert('', $CFG->wwwroot.'/mobile/view.php?id='.$id);
}   


$name = optional_param('name', '', PARAM_RAW);
$edit = optional_param('edit', -1, PARAM_BOOL);
$hide = optional_param('hide', 0, PARAM_INT);
$show = optional_param('show', 0, PARAM_INT);
$idnumber = optional_param('idnumber', '', PARAM_RAW);
$section = optional_param('section', 0, PARAM_INT);
$move = optional_param('move', 0, PARAM_INT);
$marker = optional_param('marker', -1, PARAM_INT);
$switchrole = optional_param('switchrole', -1, PARAM_INT);



$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);

if (empty($id) && empty($name) && empty($idnumber)) {
    print_error('unspecifycourseid', 'error');
}

if (!empty($name)) {
    if (!($course = $DB->get_record('course', array('shortname' => $name)))) {
        print_error('invalidcoursenameshort', 'error');
    }
} else if (!empty($idnumber)) {
    if (!($course = $DB->get_record('course', array('idnumber' => $idnumber)))) {
        print_error('invalidcourseid', 'error');
    }
} else {
    if (!($course = $DB->get_record('course', array('id' => $id)))) {
        print_error('invalidcourseid', 'error');
    }
}

$PAGE->set_url('/course/view.php', array('id' => $course->id)); // Defined here to avoid notices on errors etc

preload_course_contexts($course->id);
if (!$context = get_context_instance(CONTEXT_COURSE, $course->id)) {
    print_error('nocontext');
}

// Remove any switched roles before checking login
if ($switchrole == 0 && confirm_sesskey()) {
    role_switch($switchrole, $context);
}

require_login($course);

// Switchrole - sanity check in cost-order...
$reset_user_allowed_editing = false;
if ($switchrole > 0 && confirm_sesskey() &&
        has_capability('moodle/role:switchroles', $context)) {
    // is this role assignable in this context?
    // inquiring minds want to know...
    $aroles = get_switchable_roles($context);
    if (is_array($aroles) && isset($aroles[$switchrole])) {
        role_switch($switchrole, $context);
        // Double check that this role is allowed here
        require_login($course->id);
    }
    // reset course page state - this prevents some weird problems ;-)
    $USER->activitycopy = false;
    $USER->activitycopycourse = NULL;
    unset($USER->activitycopyname);
    unset($SESSION->modform);
    $USER->editing = 0;
    $reset_user_allowed_editing = true;
}

//If course is hosted on an external server, redirect to corresponding
//url with appropriate authentication attached as parameter
if (file_exists($CFG->dirroot . '/course/externservercourse.php')) {
    include $CFG->dirroot . '/course/externservercourse.php';
    if (function_exists('extern_server_course')) {
        if ($extern_url = extern_server_course($course)) {
            redirect($extern_url);
        }
    }
}


require_once($CFG->dirroot . '/calendar/lib.php');    /// This is after login because it needs $USER

add_to_log($course->id, 'course', 'view', "view.php?id=$course->id", "$course->id");

$course->format = clean_param($course->format, PARAM_ALPHA);
if (!file_exists($CFG->dirroot . '/course/format/' . $course->format . '/format.php')) {
    $course->format = 'weeks';  // Default format is weeks
}

//$PAGE->set_pagelayout('course');
$PAGE->set_pagetype('course-view-' . $course->format);
$PAGE->set_other_editing_capability('moodle/course:manageactivities');

if ($reset_user_allowed_editing) {
    // ugly hack
    unset($PAGE->_user_allowed_editing);
}

// LMS: Default Enable edit course
//    if (!isset($USER->editing)) {
$USER->editing = 1;
//    }
if ($PAGE->user_allowed_editing()) {
    if (($edit == 1) and confirm_sesskey()) {
        $USER->editing = 1;
        redirect($PAGE->url);
    } else if (($edit == 0) and confirm_sesskey()) {
        $USER->editing = 0;
        if (!empty($USER->activitycopy) && $USER->activitycopycourse == $course->id) {
            $USER->activitycopy = false;
            $USER->activitycopycourse = NULL;
        }
        redirect($PAGE->url);
    }

    if ($hide && confirm_sesskey()) {
        set_section_visible($course->id, $hide, '0');
    }

    if ($show && confirm_sesskey()) {
        set_section_visible($course->id, $show, '1');
    }

    if (!empty($section)) {
        if (!empty($move) and confirm_sesskey()) {
            if (!move_section($course, $section, $move)) {
                echo $OUTPUT->notification('An error occurred while moving a section');
            }
            // Clear the navigation cache at this point so that the affects
            // are seen immediately on the navigation.
            $PAGE->navigation->clear_cache();
        }
    }
} else {
    $USER->editing = 0;
}

$SESSION->fromdiscussion = $CFG->wwwroot . '/course/view.php?id=' . $course->id;


if ($course->id == SITEID) {
    // This course is not a real course.
    redirect($CFG->wwwroot . '/');
}

// AJAX-capable course format?
$useajax = false;
$formatajax = course_format_ajax_support($course->format);

if (!empty($CFG->enablecourseajax)
        and $formatajax->capable
        and !empty($USER->editing)
        and ajaxenabled($formatajax->testedbrowsers)
        and $PAGE->theme->enablecourseajax
        and has_capability('moodle/course:manageactivities', $context)) {
    $PAGE->requires->yui2_lib('dragdrop');
    $PAGE->requires->yui2_lib('connection');
    $PAGE->requires->yui2_lib('selector');
    $PAGE->requires->js('/lib/ajax/block_classes.js', true);
    $PAGE->requires->js('/lib/ajax/section_classes.js', true);

    // Okay, global variable alert. VERY UGLY. We need to create
    // this object here before the <blockname>_print_block()
    // function is called, since that function needs to set some
    // stuff in the javascriptportal object.
    $COURSE->javascriptportal = new jsportal();
    $useajax = true;
}

$CFG->blocksdrag = $useajax;   // this will add a new class to the header so we can style differently

$completion = new completion_info($course);
// Add link continue course
// Is course complete?
// Load criteria to display
$completions = $completion->get_completions($USER->id);							


// Has this user completed any criteria?
$criteriacomplete = $completion->count_course_user_data($USER->id);

$modinfo = get_fast_modinfo($course);
  
$sections = $modinfo->sections[0];

$cmid_first = current($sections);
$modinfo = get_fast_modinfo($course);

/// extra fast view mode
$url_start_course="";

if (!empty($modinfo->sections[0])) {

foreach ($modinfo->sections[0] as $cmid) {
    if($cmid_first == $cmid){
        $cm = $modinfo->cms[$cmid];
        if (!$cm->uservisible) {
            continue;
        }

        if (($url = $cm->get_url())) {
            $url_start_course = $url;
        }
    }

}
}

if ($completion->is_enabled() && ajaxenabled()) {
    $PAGE->requires->string_for_js('completion-title-manual-y', 'completion');
    $PAGE->requires->string_for_js('completion-title-manual-n', 'completion');
    $PAGE->requires->string_for_js('completion-alt-manual-y', 'completion');
    $PAGE->requires->string_for_js('completion-alt-manual-n', 'completion');

    $PAGE->requires->js_init_call('M.core_completion.init');
}

// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/course/view.php', array('id' => $course->id)));
    $PAGE->set_button($buttons);
}
$PAGE->requires->js('/blocks/course_addmodule/addmodules.js');
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
?>
<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
<?php
if ($completion->is_enabled() && ajaxenabled()) {
    // This value tracks whether there has been a dynamic change to the page.
    // It is used so that if a user does this - (a) set some tickmarks, (b)
    // go to another page, (c) clicks Back button - the page will
    // automatically reload. Otherwise it would start with the wrong tick
    // values.
    echo html_writer::start_tag('form', array('action' => '.', 'method' => 'get'));
    echo html_writer::start_tag('div');
    echo html_writer::empty_tag('input', array('type' => 'hidden', 'id' => 'completion_dynamic_change', 'name' => 'completion_dynamic_change', 'value' => '0'));
    echo html_writer::end_tag('div');
    echo html_writer::end_tag('form');
}

// Course wrapper start.
echo html_writer::start_tag('div', array('class' => 'course-content'));

$modinfo = & get_fast_modinfo($COURSE);
get_all_mods($course->id, $mods, $modnames, $modnamesplural, $modnamesused);
foreach ($mods as $modid => $unused) {
    if (!isset($modinfo->cms[$modid])) {
        rebuild_course_cache($course->id);
        $modinfo = & get_fast_modinfo($COURSE);
        debugging('Rebuilding course cache', DEBUG_DEVELOPER);
        break;
    }
}

if (!$sections = get_all_sections($course->id)) {   // No sections found
    // Double-check to be extra sure
    if (!$section = $DB->get_record('course_sections', array('course' => $course->id, 'section' => 0))) {
        $section->course = $course->id;   // Create a default section.
        $section->section = 0;
        $section->visible = 1;
        $section->summaryformat = FORMAT_HTML;
        $section->id = $DB->insert_record('course_sections', $section);
    }
    if (!$sections = get_all_sections($course->id)) {      // Try again
        print_error('cannotcreateorfindstructs', 'error');
    }
}
?>
<div class="grid-tabs">
    <ul>
      <li><a href="<?php print new moodle_url('/course/user/view.php', array('id' => $course->id)); ?>" class="selected"><?php print_r(get_string('content'))  ?></a></li>
        <li><a href="<?php print new moodle_url('/course/user/grade.php', array('id' => $course->id)); ?>" class=""><?php print_r(get_string('grade'))  ?></a></li>
    </ul>
    <div class="clear">
    </div>
</div>            
            
<?php
//var_dump($course->format);
// Include the actual course format.
require($CFG->dirroot . '/course/format/' . $course->format . '/format.php');
// Content wrapper end.

echo html_writer::end_tag('div');

// Use AJAX?
if ($useajax && has_capability('moodle/course:manageactivities', $context)) {
    // At the bottom because we want to process sections and activities
    // after the relevant html has been generated. We're forced to do this
    // because of the way in which lib/ajax/ajaxcourse.js is written.
    echo html_writer::script(false, new moodle_url('/lib/ajax/ajaxcourse.js'));
    $COURSE->javascriptportal->print_javascript($course->id);
}
?>

        </div> 
    </div><!-- Left Content -->

    <!-- Right Block -->
    <div id="admin-fullscreen-right">
<div class="side-panel">
            <div class="action-buttons">
                <ul>
                    <?php if(!$criteriacomplete): ?>
                  <li><a href="<?php echo $url_start_course ?>&current_switchrole=<?php echo $current_switchrole ;?>" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span><?php print_r(get_string('startthiscourse')) ?></span></span></span></span></a></li>
                    <?php endif; ?>
                        
                </ul>
            </div>
        </div>
        
			
			<div class="side-panel">
        <h3><?php print_r(get_string('noticeboard')) ?></h3>
				<?php 
				 global $DB,$USER;
    $sql = 'SELECT * from noticeboard where course = ' . $id;
    $result = $DB->get_records_sql($sql);
    
				?>
        <ul>
            <?php foreach($result as $key=>$val){ ?>
            <li>
            <div class="padded">
						<?php echo $val->content; ?>
						</div>
                <div class="tip padded italic">
									~ <?php echo news_ago($val->timemodified); ?>
								</div>
            </li>
            <?php } ?>
            
            
        </ul>
        
        <h3><?php print_r(get_string('additionalrefe')) ?></h3>
        <ul>
            	<?php 
				 global $DB,$USER;
    $sql = 'SELECT * from document where course_id = ' . $id;
    $result = $DB->get_records_sql($sql);
		//var_dump($result);
    
				?>
              
          <?php foreach($result as $key=>$val){ 
						$doc_title="";
						if($val->new_name!="") {
                                
                                $doc_title= $val->new_name;
                                    
                                } else {
                                $doc_title= $val->file_name;
                                }
						
						?>
            <li>                    
                    <a class="btn_pres" title="" href="<?php echo $CFG->wwwroot ?>/course/document/upload/<?php echo $val->file_name;  ?>"><?php echo $doc_title; ?></a>                                                             
            </li>
          
            <?php } ?>
                
        </ul>
        
        
    </div>
			
        

    </div>  <!-- End Right -->

</div>      
<?php
echo $OUTPUT->footer();

function news_ago($time, $prefix = 'Đã đăng')
{
   $periods = array(get_string('second'), get_string('minute'), get_string('hour'), get_string('day'), get_string('week'), get_string('month'), get_string('year'), get_string('decade'));
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = get_string('ago');

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   //hide for vietnamese
//   if($difference != 1) {
//       $periods[$j].= "s";
//   }

   return "$prefix $difference $periods[$j] $tense";
}
?>

