<?php
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG, $USER;


require_once($CFG->dirroot.'/common/calendar/classes/tc_calendar.php');

require_once($CFG->dirroot . "/lib/modinfolib.php");
require_once($CFG->dirroot . "/lib/completionlib.php");
require_once($CFG->dirroot . "/lib/completion/completion_completion.php");
require($CFG->dirroot . '/common/lib.php');

require_login(0, false);

$id = optional_param('id', 0, PARAM_INT); //course_id
$action          = optional_param('action', '', PARAM_TEXT);
$switchrole = optional_param('switchrole', -1, PARAM_INT);
$current_switchrole = optional_param('current_switchrole', -1, PARAM_INT);
if (! ($course = $DB->get_record('course', array('id'=>$id)))) {
            print_error('invalidcourseid', 'error');
        }


$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
$completion = new completion_info($course);
// Add link continue course
// Is course complete?
// Load criteria to display
					


// Has this user completed any criteria?
$criteriacomplete = $completion->count_course_user_data($USER->id);
$modinfo = get_fast_modinfo($course);
  
$sections = $modinfo->sections[0];

$cmid_first = current($sections);
$modinfo = get_fast_modinfo($course);

/// extra fast view mode
$url_start_course="";

if (!empty($modinfo->sections[0])) {

foreach ($modinfo->sections[0] as $cmid) {
    if($cmid_first == $cmid){
        $cm = $modinfo->cms[$cmid];
        if (!$cm->uservisible) {
            continue;
        }

        if (($url = $cm->get_url())) {
            $url_start_course = $url;
        }
    }

}
}
?>

<div id="page-body">
    
    
            <table class="trainee-fullscreen" style="width: 100%">
            <tbody><tr>
                <td class="trainee-fullscreen-left" style="width: 70%">
                    <div class="trainee-fullscreen-content">
                        
    <div class="focus-panel">
        <div class="panel-head">
            <h3><?php echo $course->fullname ?></h3>
            <div class="subtitle"><?php echo $course->idnumber ?></div>
            <div class="subtitle"><?php echo $course->summary ?></div>
       </div>     
        <div class="body">
       
        
                
        <table class="item-page-list">
                                    <tbody><tr>
                                            <th>&nbsp;</th>
                                            <th style="text-align: left">Modules</th>
                                            <th>Passmark</th>
                                            <th>Score</th>
                                            <th>Status</th>
                                            <th>&nbsp;</th>
                                        </tr>

                                        <?php
                                        $cm = new course_modinfo($course, $USER->id);
                                        $modinfo = & get_fast_modinfo($course);
                                        $mods_in_course = $modinfo->sections;
                                        foreach ($mods_in_course as $key => $val) {
                                            foreach ($val as $key1 => $val1) {
                                                $cm_object = $cm->get_cm($val1);
                                                

                                                ?>
                                                <tr>
                                                    <td>
                    <a href="<?php echo $cm_object->get_url(); ?>" class="next-arrow"></a>
                                        
                </td>
                                                    <td class="main-col">

                                                        <div class="title">
                                                            <a href="<?php echo $cm_object->get_url(); ?>">
                                                                <?php echo $cm_object->name; ?>
                                                            </a>
                                                            
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <?php 
                                                        if($cm_object->module==13){
                                                            $pass_mark=get_pass_mark_quiz($cm_object->instance);
                                                            echo $pass_mark;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                         <?php 
                                                        if($cm_object->module==13){
                                                        
                                                            echo trim(trim(get_highest_score_quiz($USER->id,$cm_object->instance), '0'), '.');
                                                        
                                                        }
                                                        ?>
                                                    </td>
                                                   
                                                    <?php 
                                                    
                                                    if (is_cm_complete($course,$val1,$USER->id)==1) { 
                                                    ?>
                                                        <td class="nowrap">
                                                            <span title="" class="box-tag box-tag-green m-status"><span>Complete</span></span>   
                                                        </td>
        <?php } else { ?>
                                                        <td class="nowrap">
                                                            <span title="" class="box-tag box-tag-grey m-status"><span>Not Complete</span></span>   
                                                        </td>
        <?php } ?>
        
               
                                                    <input type="hidden" name="hidden_cm_id" value="0"/>
                                                </tr>
        <?php } ?>
                                                    

        <?php
    
}
?>
                                    </tbody></table>
       
    </div>
 </div>

                    </div>
                </td>
                <td valign="top" class="trainee-fullscreen-right" style="width: 30%">
                    <div class="trainee-fullscreen-content">
                        
            
                <div class="side-panel">
    <div class="action-buttons">
                <ul>
                    <?php if(!$criteriacomplete): ?>
                    <li><a href="<?php echo $url_start_course ?>&current_switchrole=<?php echo $current_switchrole ;?>" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span>Start this course</span></span></span></span></a></li>
                    <?php endif; ?>
                        
                </ul>
            </div>
    </div>
            
    <div class="side-panel">
        
        
        <h3><?php print_r(get_string('noticeboard')) ?></h3>
        <?php 
				 global $DB,$USER;
    $sql = 'SELECT * from noticeboard where course = ' . $id;
    $result = $DB->get_records_sql($sql);
    
				?>
        <ul>
            <?php foreach($result as $key=>$val){ ?>
            <li>
            <div class="padded">
						<?php echo $val->content; ?>
						</div>
                <div class="tip padded italic">
									~ <?php echo time_ago($val->timemodified,'Đã đăng'); ?>
								</div>
            </li>
            <?php } ?>
            
            
        </ul>
        
        
        <h3>Additional Reference</h3>
        <ul>
            	<?php 
				 global $DB,$USER;
    $sql = 'SELECT * from document where course_id = ' . $id;
    $result = $DB->get_records_sql($sql);
		//var_dump($result);
    
				?>
              
          <?php foreach($result as $key=>$val){ 
						$doc_title="";
						if($val->new_name!="") {
                                
                                $doc_title= $val->new_name;
                                    
                                } else {
                                $doc_title= $val->file_name;
                                }
						
						?>
            <li>                    
                    <a class="btn_pres" title="" href="<?php echo $CFG->wwwroot ?>/course/document/upload/<?php echo $val->file_name;  ?>"><?php echo $doc_title; ?></a>                                                             
            </li>
          
            <?php } ?>
                
        </ul>
        
        
    </div>

                    </div>
                </td>
            </tr>
        </tbody></table>   
    </div>