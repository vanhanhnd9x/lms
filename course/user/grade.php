<?php
//  Display the course home page.

require_once('../../config.php');
require_once('../lib.php');
require_once($CFG->dirroot . '/mod/forum/lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('id', 0, PARAM_INT);
$course = $DB->get_record('course', array('id' => $id));

$PAGE->set_title($course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

$info = new completion_info($course);
// Load criteria to display
$completions = $info->get_completions($USER->id);							
// Is course complete?
$coursecomplete = $info->is_course_complete($USER->id);

// Has this user completed any criteria?
$criteriacomplete = $info->count_course_user_data($USER->id);

//if ($coursecomplete) {
//    echo '(' . get_string('complete') . ')';
//} else if (!$criteriacomplete) {
//    echo '<i>(' . get_string('notyetstarted', 'completion') . ')</i>';
//} else {
//    echo '<i>(' . get_string('inprogress', 'completion') . ')</i>';
//}
//exit;
 
  $modinfo = get_fast_modinfo($course);
  
  
  $sections = $modinfo->sections[0];
  
  $cmid_first = current($sections);
  $modinfo = get_fast_modinfo($course);
 
	/// extra fast view mode
	        
    if (!empty($modinfo->sections[0])) {
    
    foreach ($modinfo->sections[0] as $cmid) {
        if($cmid_first == $cmid){
            $cm = $modinfo->cms[$cmid];
            if (!$cm->uservisible) {
                continue;
            }

            if (($url = $cm->get_url())) {
                $url_start_course = $url;
            }
        }
        
    }
}
  
?>
<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <div class="grid-tabs">
                <ul>
                    <li><a href="<?php print new moodle_url('/course/user/view.php', array('id' => $course->id)); ?>" class=""><?php print_r(get_string('content'))  ?></a></li>
                    <li><a href="<?php print new moodle_url('/course/user/grade.php', array('id' => $course->id)); ?>" class="selected"><?php print_r(get_string('grade'))  ?></a></li>
                </ul>
                <div class="clear">
                </div>
            </div> 
            <div class="panel-head">
                <h3><?php echo $course->fullname; ?></h3>
                <div class="subtitle"></div>
            </div>
            
          <div class="heading-title"><?php print_r(get_string('ineedto'))  ?></div>
            <?php
            require_once $CFG->libdir . '/gradelib.php';
            require_once $CFG->dirroot . '/grade/lib.php';
            require_once $CFG->dirroot . '/grade/report/user/lib.php';

            
            grade_report_user_course_view($course, $USER);
                   
            
            
            ?>

        </div> 
    </div><!-- Left Content -->

    <!-- Right Block -->
    <div id="admin-fullscreen-right">
        <div class="side-panel">
            <div class="action-buttons">
                <ul>
                    <?php if(!$criteriacomplete): ?>
                    <li><a href="<?php echo $url_start_course ;?>" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span><?php print_r(get_string('startthiscourse')) ?></span></span></span></span></a></li>
                    <?php endif; ?>
                        
                </ul>
            </div>
        </div>
        <div class="side-panel">
          <h3><?php print_r(get_string('myachieve')) ?></h3>
            <ul>
                
                <li>   
                    
                    <div class="padded tip">                
                        <?php $ccompletion = new completion_completion(array('course' => $course->id, 'userid' => $USER->id));
                                     
                                     ?>
                                     <?php print_r(get_string('achievedon')) ?> <?php echo date('F j, Y',$ccompletion->timecompleted);  ?>
                            
                    </div>
                    <div class="clear"></div>
                </li>
                    
            </ul>
        </div>
    </div>  <!-- End Right -->

</div>      
<?php
    echo $OUTPUT->footer();
?>

