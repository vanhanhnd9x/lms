<?php
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}
require('../../config.php');
global $CFG, $USER,$DB;

require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');

require_login(0, false);


$id = optional_param('id', 0, PARAM_INT); //course_id
$action = optional_param('action', '', PARAM_TEXT);

$course = get_info_groups($id);
if(!empty($course->idgvnn) && !empty($course->idgvtg)){
    $course = get_info_groups($id);
}else{
  $course = get_info_groups1($id);
}
// trang add
function update_ten_nhom_lop_cre_by_trang($courseid,$groupid,$new_name,$sotiet){
  global $DB;
  $thongtinlop=$DB->get_record('groups', array(
    'id' => $groupid
    ),
    $fields='id,id_namhoc', $strictness=IGNORE_MISSING
  );
  $lichsutennhom=$DB->get_record('history_course_name', array(
      'courseid' => $courseid,
      'groupid' => $groupid,
      'school_year_id' => $thongtinlop->id_namhoc,
    ),
    $fields='id', $strictness=IGNORE_MISSING
  );
  if(!empty($lichsutennhom)){
    $update = new stdClass();
    $update->id = $lichsutennhom->id;
    $update->course_name = trim($new_name);
    $update->sotiet = trim($sotiet);
    $DB->update_record('history_course_name', $update, false);
  }else{
    $record = new stdClass();
    $record->courseid = trim($courseid) ;
    $record->groupid = trim($groupid) ;
    $record->school_year_id = trim($thongtinlop->id_namhoc);
    $record->course_name = trim($new_name);
    $record->sotiet = trim($sotiet);
    $DB->insert_record('history_course_name', $record);
  }
}
function check_trung_ten_nhom_lop_khi_update_trong_lop_khoi($groupid,$block_student,$name){
  global $DB;
  $sql="SELECT `course`.`id` FROM `course`
  JOIN `group_course` ON `group_course`.`course_id`= `course`.`id`
  JOIN `groups` ON `group_course`.`group_id` = `groups`.`id`
  WHERE `groups`.`id`= {$groupid}
  AND `groups`.`id_khoi` = {$block_student}
  AND `course`.`fullname`='{$name}'" ;
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  if($data){
    return 1;
  }else{
    return 2;
  }
}

$course_setting = get_course_settings($id);
$teacher = get_list_teacher();
$teachertutors = get_list_teachertutors();

$idgvnn_nhom=lay_id_gvnn_thuoc_nhom_lop_by_cua($id); 
$idgvtg_nhom=lay_id_gvtg_thuoc_nhom_lop_by_cua($id);

if ($action == 'update') { 
  $id = optional_param('id', 0, PARAM_INT); //course_id
  // trang add
  $schoolid = optional_param('schoolid', 0, PARAM_INT);
  $block_student = optional_param('block_student', 0, PARAM_INT);
  $groupid = optional_param('groupid', 0, PARAM_INT);
  $old_name = optional_param('old_name', '', PARAM_TEXT);

  $course = get_info_groups($id);
  $course_name = optional_param('course_name', 0, PARAM_TEXT);
  $course_last_name = optional_param('name', 0, PARAM_TEXT);
  $name = $course_name.$course_last_name;
  $sotiet = optional_param('sotiet', 0, PARAM_TEXT);
  $gvnn = optional_param('gvnn', '', PARAM_TEXT);
  $gvtg = optional_param('gvtg', '', PARAM_TEXT);
 
  
  $description = optional_param('description', 0, PARAM_TEXT);
  // check thay đổi thông tin gvtg, gvnn
  	if(!empty($idgvnn_nhom)){
        if(!empty($gvnn)&&($gvnn!=$idgvnn_nhom)){
            assign_user_to_course_3($gvnn, $id,8);
            remove_person_from_course_3($idgvnn_nhom, $id);
        }
        if(empty($gvnn)){
            remove_person_from_course_3($idgvnn_nhom, $id);
        }

    }else{
        if(!empty($gvnn)){
            assign_user_to_course_3($gvnn, $id,8);
        }
    }
    // add gvtg
    if(!empty($idgvtg_nhom)){
        if(empty($gvtg)){
            remove_person_from_course_3($idgvtg_nhom, $id);
        }
        if(!empty($gvtg)&&($gvtg!=$idgvtg_nhom)){
            assign_user_to_course_3($gvtg, $id,10);
            remove_person_from_course_3($idgvtg_nhom, $id);
        }
    }else{
        if(!empty($gvtg)){
            assign_user_to_course_3($gvtg, $id,10);
        }
    }
  // check sự thay đổi thông tin nhóm
  if($old_name!=$name){
   $checktennhom=check_trung_ten_nhom_lop_khi_update_trong_lop_khoi($groupid,$block_student,$name);
    if($checktennhom==1){
      ?>
      <script>
        alert('Tên nhóm đã tồn tại trong lớp, trường bạn chọn');
      </script>
      <?php
      echo displayJsAlert('', $CFG->wwwroot . "/course/settings/index.php?id=".$id);

    }elseif($checktennhom==2){
      update_course_settings($id, $name,$sotiet,$gvnn,$gvtg, $description);
      // trang add
      update_ten_nhom_lop_cre_by_trang($id,$groupid,$name,$sotiet);
      
      // if($gvnn != -1 || $gvtg != -1 ){
      //   if(empty($course->idgvnn) || empty($course->idgvtg)){
      //     assign_teacher_to_course($gvnn,$id);
      //     assign_teachertutor_to_course($gvtg,$id);
      //   }else{
      //     update_teacher_to_course($gvnn,$id);
      //     update_teachertutor_to_course($gvtg,$id);
      //   }
      // }

      echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/courses.php");
    }
  }else{
    update_course_settings($id, $name,$sotiet,$gvnn,$gvtg, $description);
    // trang add
    update_ten_nhom_lop_cre_by_trang($id,$groupid,$name,$sotiet);
    
    // if($gvnn != -1 || $gvtg != -1 ){
    //   if(empty($course->idgvnn) || empty($course->idgvtg)){
    //     assign_teacher_to_course($gvnn,$id);
    //     assign_teachertutor_to_course($gvtg,$id);
    //   }else{
    //     update_teacher_to_course($gvnn,$id);
    //     update_teachertutor_to_course($gvtg,$id);
    //   }
    // }

    echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/courses.php");
  }


  // $active_hidden = optional_param('active_hidden', '', PARAM_TEXT);
  // $hidden_library = optional_param('hidden_library', '', PARAM_TEXT);
  // $module_order = optional_param('complete_order_hidden', '', PARAM_TEXT);
  // $enable_full_screen_hidden = optional_param('enable_full_screen_hidden', '', PARAM_TEXT);
  // $complete_by = optional_param('complete_by', '', PARAM_TEXT);
  // $time_span = optional_param('time_span', '', PARAM_TEXT);
  // $time_spend_type = optional_param('time_spend_type', '', PARAM_TEXT);
  // $date2 = optional_param('date2', '', PARAM_TEXT);
  // $apply_due_date_hidden = optional_param('apply_due_date_hidden', '', PARAM_TEXT);
  // $compliant_for = optional_param('compliant_for', '', PARAM_TEXT);
  // $automatic_resit = optional_param('automatic_resit', '', PARAM_TEXT);
  // $sell_course = optional_param('sell_course', '', PARAM_TEXT);
  // $course_currency = optional_param('course_currency', '', PARAM_TEXT);
  // $course_fee = optional_param('course_fee', '', PARAM_TEXT);
  // $sale_description = optional_param('sale_description', '', PARAM_TEXT);
  // $sale_full_description = optional_param('sale_full_description', '', PARAM_TEXT);




  // if ($_FILES["file"]["error"] > 0) {
  //   //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
  // }
  // else {
  //   $file_extension = end(explode('.', $_FILES["file"]["name"]));
  //   if (strtolower($file_extension) == "doc" || strtolower($file_extension) == "docx" || strtolower($file_extension) == "rtf") {
  //     move_uploaded_file($_FILES["file"]["tmp_name"], $CFG->dirroot . "/course/document/upload/" . $_FILES["file"]["name"]);
  //   }
  //   else {
  //     echo displayJsAlert('Cập nhật thành công', $CFG->wwwroot . "/course/settings/index.php?id=" . $id);
  //   }
  // }

  // $file_name = $_FILES["file"]["name"];
  // var_dump($description);die;
  // update_course_settings($id, $name,$sotiet,$gvnn,$gvtg, $description);
  // // trang add
  // update_ten_nhom_lop_cre_by_trang($id,$course->id_lop,$name,$sotiet);
  // if($gvnn != -1 || $gvtg != -1 ){
  //   if(empty($course->idgvnn) || empty($course->idgvtg)){
  //     assign_teacher_to_course($gvnn,$id);
  //     assign_teachertutor_to_course($gvtg,$id);
  //   }else{
  //     update_teacher_to_course($gvnn,$id);
  //     update_teachertutor_to_course($gvtg,$id);
  //   }
  // }
  // echo displayJsAlert('Chỉnh sửa thành công', $CFG->wwwroot . "/manage/courses.php");
  
}


$PAGE->set_title(get_string('classgroup') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

?>

<script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/course/new_course/js/validate_class.js"></script>



<div class="row">
    <div class="col-md-9">
        <div class="card-box">
            <div class="ribbon-box">
                <div class="card-content">
                    <div class="ribbon ribbon-success">
                        <h4 class="m-t-0 header-title"><?php print_r(get_string('edit')) ?> <?php echo $course->fullname ?></h4>
                    </div>
                    <p></p>
                    <form method="post" id="createTeamForm" action="<?php echo $CFG->wwwroot ?>/course/settings/index.php?action=update" onsubmit="return validate();">
                        <input type="hidden" hidden="" value="<?php echo $id ?>" name="id" id="id">
                        <input type="hidden" hidden="" value="<?php echo $course->id_truong; ?>" name="schoolid" >
                        <input type="hidden" hidden="" value="<?php echo $course->id_khoi; ?>" name="block_student" id=>
                        <input type="hidden" hidden="" value="<?php echo $course->id_lop; ?>" name="groupid" id=>
                        <input type="hidden" hidden="" value="<?php echo $course->fullname; ?>" name="old_name" id=>

                         <div class="form-group row">
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                                <select name="schoolid" id="schoolid" class="form-control" disabled>
                                    <option value=""><?php echo get_string('schools'); ?></option>
                                     <?php 
                                     $schools = $DB->get_records('schools',['del'=>0]);
                                     foreach ($schools as $key => $value) {
                                            # code...
                                      ?>
                                      <option value="<?php echo $value->id; ?>" <?php echo $value->id == $course->id_truong ? 'selected="selected"' : ''; ?>><?php echo $value->name; ?></option>
                                      <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
                                <select name="block_student" id="block_student" class="form-control" disabled>
                                   <option value=""><?php echo get_string('block_student'); ?></option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
                                <select name="groupid" id="groupid" class="form-control" disabled>
                                    <option value=""><?php echo get_string('class'); ?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-8">
                            <div class="row">
                              
                              <div class="col-md-12">

                                <label><?php print_r(get_string('teamname')) ?> <span class="text-danger">*</span></label>   <div class="input-group"> 
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">
                                      <input type="text" id="course_name" name="course_name" readonly="" value="<?php echo substr($course->fullname,0,7) ?>">
                                    </div>
                                  </div>       
                                  <input type="text" value="<?php echo substr($course->fullname,7) ?>" name="name" id="name" class="form-control">
                                  <span class="text-danger" id="er_name" style="display: none;">Vui lòng nhập tên nhóm lớp</span>
                                </div>     
                               
                              </div>
                            </div>
                            <span class="text-white bg-warning"><?php echo get_string('define_group_name'); ?></span>
                          </div>
                          <div class="col-md-4">
                              <label><?php print_r(get_string('numberofperiods')) ?>:</label>        
                              <input type="text" value="<?php echo $course->sotiet ?>" name="sotiet" class="form-control">
                          </div>
                        </div>

                       

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label><?php print_r(get_string('foreignteacher')) ?></label>        
                                <select name="gvnn" id="gvnn" class="form-control" data-style="select-with-transition" title="" data-size="7" >
                                    <option value="" >-- <?php print_r(get_string('foreignteacher')) ?> --</option>
                                    <?php foreach ($teacher as $gvnn) { ?>
                                      <option value="<?php echo $gvnn->id ?>" <?php echo $gvnn->id == $idgvnn_nhom ? 'selected="selected"' : ''; ?> ><?php echo $gvnn->lastname .' '. $gvnn->firstname ?></option>
                                    <?php } ?>
                                </select>
                                <span class="text-danger" id="er_gvnn" style="display: none;">Vui lòng chọn giáo viên ngoại ngữ</span>
                            </div>
                            <div class="col-md-6">
                                <label><?php print_r(get_string('tutorsteacher')) ?></label>        
                                <select name="gvtg" id="gvtg" class="form-control" data-style="select-with-transition" title="" data-size="7" >
                                    <option value="" >-- <?php print_r(get_string('tutorsteacher')) ?> --</option>
                                    <?php foreach ($teachertutors as $gvtg) { ?>
                                      <option value="<?php echo $gvtg->id ?>" <?php echo $gvtg->id == $idgvtg_nhom ? 'selected="selected"' : ''; ?>><?php echo $gvtg->lastname .' '. $gvtg->firstname ?></option>
                                    <?php } ?>
                                </select>
                                <span class="text-danger" id="er_gvtg" style="display: none;">Vui lòng chọn giáo viên trợ giảng</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label><?php print_r(get_string('description')) ?></label>        
                            <textarea rows="10" name="description" id="Description" cols="20" class="form-control"><?php echo $course->summary ?></textarea>
                        </div>

                        <div class="form-group">
                          <input type="submit" class="btn btn-success" value=" <?php print_string('save','admin') ?> ">
                          <?php print_string('or') ?> 
                            <a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_string('cancel') ?></a>     
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
  echo $OUTPUT->footer();
?>
<script>
  // ajax edit
    var school = $('#schoolid').find(":selected").val();
    if (school) {
        var blockedit = "<?php echo $course ->id_khoi; ?>";
        $.get("<?php echo $CFG->wwwroot ?>/common/ajax.php?schoolid="+school+"&blockedit="+blockedit,function(data){
          $("#block_student").html(data);
        });
        if(blockedit){
          var groupedit="<?php echo $course->id_lop; ?>";
          $.get("<?php echo $CFG->wwwroot ?>/common/ajax.php?block_student="+blockedit+"&groupedit="+groupedit,function(data){
            $("#groupid").html(data);
          });
        }
    }

</script>