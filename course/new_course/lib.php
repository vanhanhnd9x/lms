<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function get_document_from_id($id) {
    global $DB;
    $document = $DB->get_record('document', array(
        'id' => $id
            ));

    return $document;
}

function insert_document($name, $file_name, $course_id) {
    global $DB;

    $document = new stdClass();
    $document->course_id = $course_id;
    $document->new_name = $name;
    $document->file_name = $file_name;
    $DB->insert_record('document', $document);
}

function get_documents_from_course($course_id) {
    global $DB, $USER;
    $sql = 'SELECT * from document where course_id = ' . $course_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function displayJsAlert($ms, $redirectPath) {
    $html = '';
    if($ms!=''){
        $html .= '<script language="javascript">alert("' . $ms . '")</script>';
    }
    
    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}

function delete_document($document_id) {
    global $DB;
    $DB->delete_records_select('document', " id = $document_id ");
}



function get_course_settings($course_id) {
    global $DB;
    $course_setting = $DB->get_record('course_settings', array(
        'course_id' => $course_id
            ));

    return $course_setting;
}
function del_course($course_id){
    global $DB;
    $course = $DB->get_record('course', array('id'=>$course_id));
    delete_course($course);
    $DB->delete_records('course_settings', array('course_id'=>$course_id));
    
}


function is_exist_course_settings($course_id) {
    global $DB;
    $course_setting = $DB->get_record('course_settings', array(
        'course_id' => $course_id
            ));

    return $course_setting->id;
}


function get_course_settings_select($course_id) {
    global $DB;
    $course_setting = $DB->get_record_select('course_settings','sell_course=1', array(
        'course_id' => $course_id
            ));

    return $course_setting;
}

// function get_list_teacher()
// {
//     global $DB;
//     $sql = "SELECT role_assignments.roleid, user.* FROM user INNER JOIN role_assignments ON `user`.id = `role_assignments`.`userid` WHERE `role_assignments`.`roleid`=8";
//     $rows = $DB->get_records_sql($sql);
//     return $rows;
// }
// function get_list_block_student($page='',$limit=''){
//     global $DB;
//     $sql="select * from block_student where 1";
//     $data = $DB->get_records_sql($sql);
//     return $data;
// }
?>
