<?php
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG,$USER;
require('../../config.php');
require_once('lib.php');
require_login(0, false);
$id          = optional_param('id', 0, PARAM_INT);//course_id
$action          = optional_param('action', '', PARAM_TEXT);


?>