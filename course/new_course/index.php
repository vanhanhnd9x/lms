<?php
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}
global $CFG, $USER,$DB;
require('../../config.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
// require_once($CFG->dirroot . '/manage/schools/lib.php');
require_once($CFG->dirroot . '/manage/schedule/lib.php');

// require_once('lib.php');
require_once('calendar/classes/tc_calendar.php');
require_login(0, false);
$PAGE->set_title(get_string('addanewteam'));
$PAGE->set_heading(get_string('addanewteam'));
echo $OUTPUT->header();

$idnamhochientai=get_id_school_year_now();
$teacher = get_list_teacher();
$teachertutors = get_list_teachertutors();
$sy = $DB->get_records_sql('SELECT * FROM school_year');
$action = optional_param('action', '', PARAM_TEXT);
// trang add 
function check_trung_ten_nhom_lop_trong_lop_khoi($groupid,$block_student,$name){
  global $DB;
  $sql="SELECT `course`.`id` FROM `course`
  JOIN `group_course` ON `group_course`.`course_id`= `course`.`id`
  JOIN `groups` ON `group_course`.`group_id` = `groups`.`id`
  WHERE `groups`.`id`= {$groupid}
  AND `groups`.`id_khoi` ={$block_student}
  AND `course`.`fullname`='{$name}'" ;
  $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
  if($data){
    return 1;
  }else{
    return 2;
  }
}
if ($action == 'add') {

  $block_student = optional_param('block_student', 0, PARAM_TEXT);
  $course_name = optional_param('course_name', 0, PARAM_TEXT);
  $course_last_name = optional_param('name', 0, PARAM_TEXT);
  $name = $course_name.$course_last_name;
  $sotiet = optional_param('sotiet', 0, PARAM_TEXT);
  $class = optional_param('groupid', 0, PARAM_TEXT);
  $gvnn = optional_param('gvnn', 0, PARAM_TEXT);
  $gvtg = optional_param('gvtg', 0, PARAM_TEXT);
  $description = optional_param('description', 0, PARAM_TEXT);
  $check_ten_nhom=check_trung_ten_nhom_lop_trong_lop_khoi($class,$block_student,$name);
  if($check_ten_nhom==1){
    ?>
      <script>
        alert('Tên nhóm đã tồn tại trong lớp, trường bạn chọn');
      </script>
    <?php 
    echo displayJsAlert('', $CFG->wwwroot . "/course/new_course/index.php");
  }elseif($check_ten_nhom==2){
    $course_id = insert_class_groups($name,$sotiet,$gvnn,$gvtg ,$description);
    assign_course_to_team($course_id->id,$class);

    $school_year = get_info_class($class)->id_namhoc;
    insert_history_namecourse($course_id->id,$name,$class, $school_year,$sotiet);
    
    if (!empty($gvnn) || !empty($gvtg)) {
      assign_teacher_to_course($gvnn,$course_id->id);
      assign_teachertutor_to_course($gvtg,$course_id->id);
    }

    echo displayJsAlert('Thêm thành công', $CFG->wwwroot . "/manage/courses.php");
  }
  // var_dump($check_ten_nhom);die;
  // $active_hidden = optional_param('active_hidden', '', PARAM_TEXT);
  // $hidden_library = optional_param('hidden_library', '', PARAM_TEXT);
  // $module_order = optional_param('complete_order_hidden', '', PARAM_TEXT);
  // $enable_full_screen_hidden = optional_param('enable_full_screen_hidden', '', PARAM_TEXT);
  // $complete_by = optional_param('complete_by', '', PARAM_TEXT);
  // $time_span = optional_param('time_span', '', PARAM_TEXT);
  // $time_spend_type = optional_param('time_spend_type', '', PARAM_TEXT);
  // $date2 = optional_param('date2', '', PARAM_TEXT);
  // $apply_due_date_hidden = optional_param('apply_due_date_hidden', '', PARAM_TEXT);
  // $compliant_for = optional_param('compliant_for', '', PARAM_TEXT);
  // $automatic_resit = optional_param('automatic_resit', '', PARAM_TEXT);
  // $course_currency = optional_param('course_currency', '', PARAM_TEXT);
  // $course_fee = optional_param('course_fee', '', PARAM_TEXT);
  // $sale_description = optional_param('sale_description', '', PARAM_TEXT);
  // $sale_full_description = optional_param('sale_full_description', '', PARAM_TEXT);



  // if ($_FILES["file"]["error"] > 0) {
  //   //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
  // }
  // else {
  //   $file_extension = end(explode('.', $_FILES["file"]["name"]));
  //   if (strtolower($file_extension) == "doc" || strtolower($file_extension) == "docx" || strtolower($file_extension) == "rtf") {
  //     move_uploaded_file($_FILES["file"]["tmp_name"], $CFG->dirroot . "/course/document/upload/" . $_FILES["file"]["name"]);
  //   }
  //   else {
  //     //echo displayJsAlert('Templates can only be .doc, .docx and .rtf files', $CFG->wwwroot."/course/settings/index.php?id=".$id);
  //     echo displayJsAlert('', $CFG->wwwroot . "/course/settings/index.php?id=" . $course_id);
  //   }
  // }

  // $file_name = $_FILES["file"]["name"];
  // 
//trang an di
  // $course_id = insert_class_groups($name,$sotiet,$gvnn,$gvtg ,$description);
  // assign_course_to_team($course_id->id,$class);

  // $school_year = get_info_class($class)->id_namhoc;
  // insert_history_namecourse($course_id->id,$name,$class, $school_year,$sotiet);
  
  // if (!empty($gvnn) || !empty($gvtg)) {
  //   assign_teacher_to_course($gvnn,$course_id->id);
  //   assign_teachertutor_to_course($gvtg,$course_id->id);
  // }

  // echo displayJsAlert('Thêm thành công', $CFG->wwwroot . "/manage/courses.php");
// echo 'path = '.$CFG->wwwroot."/course/view.php?id=".$course_id;

}

// $teacher = get_list_teacher();
// $teachertutors = get_list_teachertutors();

?>


<!-- Latest compiled and minified CSS & JS -->
<script type="text/javascript" src="js/validate_class.js"></script>


<div class="row">
    <div class="col-md-9">
        <div class="card-box">
            <div class="ribbon-box">
                <div class="card-content">
                    <div class="ribbon ribbon-success">
                        <h4 class="m-t-0 header-title"><?php print_r(get_string('addanewteam')) ?></h4>
                    </div>
                    <p></p>
                    <form method="post" id="createTeamForm" action="<?php echo $CFG->wwwroot ?>/course/new_course/index.php?action=add" onsubmit="return validate();">
                      
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('school_year'); ?><span style="color:red">*</span></label>
                                <select name="school_year" id="school_year" class="form-control" required="">
                                    <option value=""><?php echo get_string('school_year'); ?></option>
                                     <?php
                                      $sql="SELECT * FROM `school_year` ORDER BY sy_end DESC";
                                      $listschool_year = $DB->get_records_sql($sql);
                                     foreach ($listschool_year as $key => $value) {
                                      ?>
                                      <option value="<?php echo $value->id ?>" <?php if($idnamhochientai==$value->id) echo 'selected'; ?>><?php echo $value->sy_start,' - ',$value->sy_end; ?></option>
                                      <?php 
                                     }
                                     ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('schools'); ?><span style="color:red">*</span></label>
                                <select name="schoolid" id="schoolid" class="form-control" required="">
                                    <option value=""><?php echo get_string('schools'); ?></option>
                                     <?php 
                                     $schools = $DB->get_records('schools',['del'=>0]);
                                     foreach ($schools as $key => $value) {
                                            # code...
                                      ?>
                                      <option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
                                      <?php 
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('block_student'); ?><span style="color:red">*</span></label>
                                <select name="block_student" id="block_student" class="form-control" required="">
                                   <option value=""><?php echo get_string('block_student'); ?></option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label"><?php echo get_string('class'); ?><span style="color:red">*</span></label>
                                <select name="groupid" id="groupid" class="form-control" required="">
                                    <option value=""><?php echo get_string('class'); ?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-8">
                            <div class="row">
                              <div class="col-md-12">
                                  <label><?php print_r(get_string('teamname')) ?> <span class="text-danger">*</span></label>
                                  <div class="input-group"> 
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">
                                        <input type="text" id="course_name" name="course_name" readonly="" placeholder="<?php echo get_string('classgroup') ?>...">
                                      </div>
                                    </div>       
                                    <input type="text" value="" name="name" id="name" class="form-control">
                                    <span class="text-danger" id="er_name" style="display: none;">Vui lòng nhập tên nhóm lớp</span>
                                  </div>
                              </div>
                            </div>
                            <span class="text-white bg-warning"><?php echo get_string('define_group_name'); ?></span>
                          </div>
                          <div class="col-md-4">
                              <label><?php echo get_string('numberofperiods'); ?>:</label>        
                              <input type="text" value="" name="sotiet" class="form-control">
                          </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label><?php print_r(get_string('foreignteacher')) ?> </label>        
                                <select name="gvnn" id="gvnn" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                    <option value="" >-- <?php print_r(get_string('foreignteacher')) ?> --</option>
                                    <?php foreach ($teacher as $gvnn) { ?>
                                      <option value="<?php echo $gvnn->id ?>" ><?php echo $gvnn->lastname .' '. $gvnn->firstname ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label><?php print_r(get_string('tutorsteacher')) ?> </label>        
                                <select name="gvtg" id="gvtg" class="form-control selectpicker" data-style="select-with-transition" title="" data-size="7" data-live-search="true">
                                    <option value="" >-- <?php print_r(get_string('tutorsteacher')) ?> --</option>
                                    <?php foreach ($teachertutors as $gvtg) { ?>
                                      <option value="<?php echo $gvtg->id ?>" ><?php echo $gvtg->lastname .' '. $gvtg->firstname ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label><?php print_r(get_string('description')) ?></label>        
                            <textarea rows="10" name="description" id="Description" cols="20" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                          <input type="submit" class="btn btn-success" value="<?php print_string('addteam') ?>">
                           
                            
                            <?php print_string('or') ?> 
                            <a href="<?php echo $CFG->wwwroot ?>/manage/courses.php" class="btn btn-danger"><?php print_string('cancel') ?></a>      
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<?php 
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    var school_year1 = $('#school_year').find(":selected").val();
    if(school_year1){
      $.get("<?php echo $CFG->wwwroot ?>/course/new_course/load_ajax.php?school_year="+school_year1,function(data){
            $("#schoolid").html(data);
      });
    }
    $('#school_year').on('change', function() {
        var school_year = this.value ;
        if (this.value) {
          $.get("<?php echo $CFG->wwwroot ?>/course/new_course/load_ajax.php?school_year="+school_year,function(data){
            $("#schoolid").html(data);
          });
        }
    });
    $('#schoolid').on('change', function() {
        var cua = this.value ;
         var idnamhoc = $('#schoolid option:selected').attr('status');
        if (this.value) {
          $.get("<?php echo $CFG->wwwroot ?>/course/new_course/load_ajax.php?schoolid="+cua+"&school_year_check="+idnamhoc,function(data){
            $("#block_student").html(data);
          });
        }
      });
    $('#block_student').on('change', function() {
      var idnamhoc1 = $('#block_student option:selected').attr('status');
      var block = this.value ;
      if (this.value) {
        $.get("<?php echo $CFG->wwwroot ?>/course/new_course/load_ajax.php?block_student="+block+"&school_year_check="+idnamhoc1,function(data){
          $("#groupid").html(data);
        });
      }
    });
    
    // $('#schoolid').on('change', function() {
    //     var cua = this.value ;
    //     if (this.value) {
    //       $.get("<?php echo $CFG->wwwroot ?>/common/ajax.php?schoolid="+cua,function(data){
    //         $("#block_student").html(data);
    //       });
    //     }
    // });
    // $('#block_student').on('change', function() {
    //     var block = this.value ;
    //     if (this.value) {
    //       $.get("<?php echo $CFG->wwwroot ?>/common/ajax.php?block_student="+block,function(data){
    //         $("#groupid").html(data);
    //       });
    //     }
    // });

    $(document).on('change', '#groupid', function() {
      var text = $(this).find('option:selected').attr('data-course');
      console.log(text);
      $('#course_name').val(text);
    });
    
</script>
