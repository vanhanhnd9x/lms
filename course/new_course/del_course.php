<?php
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');

require_once('lib.php');

require_login(0, false);

$id = optional_param('id', 0, PARAM_INT); //course_id
$action          = optional_param('action', '', PARAM_TEXT);
if (! ($course = $DB->get_record('course', array('id'=>$id)))) {
            print_error('invalidcourseid', 'error');
        }
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
if($action=='delete'){
    del_course($id);
    redirect($CFG->wwwroot."/manage/courses.php");
}
?>
<div id="page-body">        
        
        
        <table class="admin-fullscreen">
            <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        
    <div id="courseList" class="focus-panel">
        
<div class="panel-head clearfix">
    <h3><?php echo $course->fullname ?></h3>
    <div class="subtitle"><?php echo $course->idnumber ?></div>
    <div class="subtitle"><?php echo $course->summary ?></div>
</div>
            

        <div class="body">
          <p class="warning"><b><?php print_r(get_string('youdeletecourse')) ?></b></p>
            <p>Once this course has been deleted it <b>cannot</b> be recovered - please choose carefully.</p>
            <div class="form-buttons">
                <form method="post" action="<?php echo $CFG->wwwroot ?>/course/settings/del_course.php?id=<?php echo $id ?>&action=delete">
                    <input type="submit" class="btnlarge" value="Delete"> or <a href="<?php echo $CFG->wwwroot ?>/course/settings/index.php?id=<?php echo $id ?>" class="">cancel</a>   
                </form>
            </div>
        </div>
    </div>

                    </div>
                </td>
                <td class="admin-fullscreen-right">
                    <div class="admin-fullscreen-content">
                        
                        
                    </div>
                </td>
            </tr>
        </tbody></table>
        
        
    </div>