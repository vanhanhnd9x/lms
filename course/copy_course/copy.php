<?php

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER,$DB;
require('../../config.php');
require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_login();

$old_course_id = optional_param('old_course_id', 0, PARAM_INT); //old_course_id
$new_course_id = optional_param('new_course_id', 0, PARAM_INT); //new_course_id

$PAGE->set_context(get_system_context());
$url = new moodle_url('/course/copy_course/copy.php', array('key' => 'value', 'old_course_id' => $old_course_id));
$PAGE->set_url($url);

$PAGE->set_title('Copy course');
$PAGE->set_heading('Copy course');

echo $OUTPUT->header();

//$new_course_id= copy_course($old_course_id);
copy_course_settings($old_course_id, $new_course_id);
copy_course_enrol($old_course_id, $new_course_id); //insert into enrol


$members_of_old_course = get_members_enroll_of_course($old_course_id, $USER->id);
//var_dump($members_of_old_course);
foreach ($members_of_old_course as $key => $val) {

    assign_user_to_course($val->id, $new_course_id); //assing users to new course
}
//echo 'aaa';
copy_group_course($old_course_id, $new_course_id); //team in group
copy_course_noticeboard($old_course_id, $new_course_id); //notice board
copy_course_document($old_course_id, $new_course_id); //document

// start copying modules
$course = $DB->get_record('course', array('id' => $old_course_id),'*', MUST_EXIST);
//require_login($course);

$modules_of_old_course = get_array_of_activities($old_course_id);
$unique_arr_sections = array();
foreach ($modules_of_old_course as $key => $val) {   
    // copy sections
    $all_fields = get_all_fields_from_table($val->mod);
    if (!in_array($val->sectionid, $unique_arr_sections)) {
        $new_section_id = copy_course_section($new_course_id, $val->sectionid);
        $unique_arr_sections[] = $val->sectionid;
    }
    
    /* old code to copy modules and course modules
    $instance = copy_module($all_fields, $val->mod, $val->id, $new_course_id); //instance = new module id
    //echo "val->cm=".$val->cm."<br/>";
    $new_cm = copy_course_module($new_course_id, $val->cm, $instance, $new_section_id);
    //echo "$new_cm=".$new_cm."<br/>";
    $arr_cm[] = $new_cm;
    */
    
    // copy modules
    $cm     = get_coursemodule_from_id('', $val->cm, 0, true, MUST_EXIST);
    
    // backup the activity
    $bc = new backup_controller(backup::TYPE_1ACTIVITY, $cm->id, backup::FORMAT_MOODLE,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id);

    $backupid       = $bc->get_backupid();
    $backupbasepath = $bc->get_plan()->get_basepath();

    $bc->execute_plan();

    $bc->destroy();

    // restore the backup immediately
    $rc = new restore_controller($backupid, $new_course_id,
        backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id, backup::TARGET_CURRENT_ADDING);
    
    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
            if (empty($CFG->keeptempdirectoriesonbackup)) {
                fulldelete($backupbasepath);
            }
            echo $OUTPUT->header();
            echo $OUTPUT->precheck_notices($precheckresults);
            echo $OUTPUT->continue_button(new moodle_url('/course/view.php', array('id' => $old_course_id)));
            echo $OUTPUT->footer();
            die();
        }
    }    

    $rc->execute_plan();

    $rc->destroy();

    if (empty($CFG->keeptempdirectoriesonbackup)) {
        fulldelete($backupbasepath);
    }
    //echo 'module:'.$val->cm.';type:'.$val->module;
}
$arr_cm = get_course_modules($new_course_id);
$str_sequence = implode(",", $arr_cm);
update_sequence_course_section($str_sequence, $new_course_id);

//rebuild_course_cache($new_course_id);
//die();
echo $OUTPUT->footer();
echo displayJsAlert('', $CFG->wwwroot . "/manage/courses.php");

?>