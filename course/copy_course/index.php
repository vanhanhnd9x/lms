<?php
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
require('../../config.php');
global $CFG, $USER;

require_once($CFG->dirroot.'/common/lib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/common/calendar/classes/tc_calendar.php');
$action          = optional_param('action', '', PARAM_TEXT);
require_login(0, false);
$old_course_id="";
if($action!='update'){
$id = optional_param('old_course_id', 0, PARAM_INT); //old_course_id
$old_course_id=$id;
if (! ($course = $DB->get_record('course', array('id'=>$id)))) {
            print_error('invalidcourseid', 'error');
        }
}



if($action=='update'){
$name          = optional_param('name', '', PARAM_TEXT);
$code          = optional_param('code', '', PARAM_TEXT);
$description          = optional_param('description', '', PARAM_TEXT);
$old_course_id=optional_param('old_course_id', 0, PARAM_INT); //old_course_id
//insert into course first to get new id of course
 $new_course_id= clone_course($old_course_id);
    
    
$id = $new_course_id; //course_id



$active_hidden = optional_param('active_hidden', '', PARAM_TEXT);
$hidden_library = optional_param('hidden_library', '', PARAM_TEXT);
$module_order = optional_param('complete_order_hidden', '', PARAM_TEXT);
$enable_full_screen_hidden = optional_param('enable_full_screen_hidden', '', PARAM_TEXT);
$complete_by= optional_param('complete_by', '', PARAM_TEXT);
$time_span = optional_param('time_span', '', PARAM_TEXT);
$time_spend_type= optional_param('time_spend_type', '', PARAM_TEXT);
$date2= optional_param('date2', '', PARAM_TEXT);
$apply_due_date_hidden= optional_param('apply_due_date_hidden', '', PARAM_TEXT);
$compliant_for= optional_param('compliant_for', '', PARAM_TEXT);
$automatic_resit= optional_param('automatic_resit', '', PARAM_TEXT);
$course_currency= optional_param('course_currency', '', PARAM_TEXT);
$course_fee= optional_param('course_fee', '', PARAM_TEXT);
$sale_description= optional_param('sale_description', '', PARAM_TEXT);
$sale_full_description= optional_param('sale_full_description', '', PARAM_TEXT);



if ($_FILES["file"]["error"] > 0)
    {
    //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
  else
    {
    $file_extension=end(explode('.',$_FILES["file"]["name"]));
    if (strtolower($file_extension) == "doc"||strtolower($file_extension) == "docx"||strtolower($file_extension) == "rtf") 
        {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      $CFG->dirroot."/course/document/upload/" . $_FILES["file"]["name"]);
      
      
    }
    else{
        //echo displayJsAlert('Templates can only be .doc, .docx and .rtf files', $CFG->wwwroot."/course/settings/index.php?id=".$id);
        echo displayJsAlert('', $CFG->wwwroot."/course/settings/index.php?id=".$id);
    }
    
    }

$file_name=$_FILES["file"]["name"];

update_course_settings($id,$name,$description,$code,$active_hidden,
        $hidden_library,$module_order,$enable_full_screen_hidden,$complete_by,
        $time_span,$time_spend_type,$date2,
        $apply_due_date_hidden,$compliant_for,$automatic_resit,
        $file_name,$course_currency,$course_fee,$sale_description,$sale_full_description);

echo displayJsAlert('', $CFG->wwwroot."/course/copy_course/copy.php?old_course_id=".$old_course_id."&new_course_id=".$new_course_id);

}
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}

$course_setting=get_course_settings($id);
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
//var_dump($course_setting);

?>

<script type="text/javascript" src="<?php echo $CFG->wwwroot?>/common/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script language="javascript" src="<?php echo $CFG->wwwroot?>/common/calendar/calendar.js"></script>

<script type="text/javascript" src="../../tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>

<div id="page-body">        


    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">


                        <div id="courseList" class="focus-panel">

                            <div class="panel-head clearfix">
                                <h3><?php echo $course->fullname ?></h3>
                                <div class="subtitle"><?php echo $course->idnumber ?></div>
                                <div class="subtitle"><?php echo $course->summary ?></div>
                            </div>


                            <div class="body">


                                <div class="tabtree">
                                    <ul class="tabrow0">
                                        <li><a title="Content" href="<?php echo $CFG->wwwroot ?>/course/view.php?id=<?php echo $id ?>"><span>Content</span></a> </li>
                                        <li><a title="People" href="<?php echo $CFG->wwwroot ?>/course/people/index.php?id=<?php echo $id ?>"><span>People</span></a> </li>
                                        <li><a title="Teams" href="<?php echo $CFG->wwwroot ?>/course/team/index.php?id=<?php echo $id ?>"><span>Teams</span></a> </li>
                                        <li><a title="Noticeboard" href="<?php echo $CFG->wwwroot ?>/mod/noticeboard/view.php?id=<?php echo $id ?>"><span>Noticeboard</span></a> </li>
                                        <li><a title="Documents" href="<?php echo $CFG->wwwroot ?>/course/document/index.php?id=<?php echo $id ?>"><span>Documents</span></a> </li>
                                        <li><a title="Gradebook" href="<?php echo $CFG->wwwroot ?>/grade/report/grader/index.php?id=<?php echo $id ?>"><span>Gradebook</span></a> </li>
                                        <li class="last selected"><a title="Settings" href="<?php echo $CFG->wwwroot ?>/course/settings/index.php?id=<?php echo $id ?>"><span>Settings</span></a> </li>
                                    </ul>
                                    <div class="clear">
                                    </div>
                                </div>
                                <form method="post" id="form0" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot ?>/course/copy_course/index.php?action=update&old_course_id=<?php echo $old_course_id; ?>">
                                    <input type="hidden" value="<?php echo $id ?>" name="id" id="id">
                                    <div style="margin-right:10px">   
                                    <table class="simple-form">
                                        <tbody><tr>
                                                <th>
                                                    Course Title:
                                                </th>
                                                <td>
                                                    <input type="text" value="<?php echo $course->fullname ?>" name="name" maxlength="255" id="name" class="full">
                                                    <span id="Course_Name_validationMessage" class="field-validation-valid"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Description:
                                                </th>
                                                <td>
                                                    <textarea rows="10" name="description" id="description" cols="20" class="full">
                                                        <?php echo $course->summary ?>
                                                    </textarea>
                                                </td>
                                            </tr>    
                                        </tbody>
                                    </table>
                            </div>
                                    <div class="form-sub-heading"><?php print_r(get_string('optionalsettings','assignment')) ?></div>
                                    <table class="simple-form">
                                        <tbody><tr>
                                                <th>
                                                    <?php print_r(get_string('active2')) ?>
                                                </th>
                                                <td>
                                                    <?php 
                                                    $str_active="";
                                                    if($course_setting->active==1){
                                                        $str_active='checked="checked"';
                                                    }                                                   
                                                    ?>
                                                    <input type="checkbox" value="true" name="active" id="active" <?php echo $str_active; ?> >
                                                    <input type="hidden" value="1" name="active_hidden" id="active_hidden">
                                                </td>
                                            </tr> 
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('referencecode')) ?>
                                                </th>
                                                <td>
                                                    <input type="text" value="<?php echo $course->idnumber ?>" name="code" maxlength="20" id="code">
                                                </td>
                                            </tr> 
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('courselibrary')) ?>
                                                </th>
                                                <td>
                                                   <?php 
                                                    $str_active="";
                                                    if($course_setting->course_library==1){
                                                        $str_active='checked="checked"';
                                                    }                                                   
                                                    ?>

                                                    <input type="checkbox" value="true" name="library" id="library" <?php echo $str_active; ?>>
                                                    
                                                    <input type="hidden" value="<?php echo $course_setting->course_library ?>" name="hidden_library" id="hidden_library"/> 
                                                    <span class="tip"><?php print_r(get_string('includethiscourse')) ?></span>
                                                </td>
                                            </tr>     


                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('moduleorder')) ?>
                                                </th>
                                                <td>
                                                    <?php 
                                                    $str_active="";
                                                    if($course_setting->module_order==1){
                                                        $str_active='checked="checked"';
                                                    }                                                   
                                                    ?>
                                                    <input type="checkbox" value="" name="complete_order" id="complete_order" <?php echo $str_active; ?>>
                                                    
                                                    <input type="hidden" value="<?php echo $course_setting->module_order ?>" name="complete_order_hidden" id="complete_order_hidden"/> 
                                                    <span class="tip"><?php print_r(get_string('makelearner')) ?></span>
                                                </td>
                                            </tr>   


                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('newwindow')) ?>
                                                </th>
                                                <td>
                                                    <?php 
                                                    $str_active="";
                                                    if($course_setting->new_window==1){
                                                        $str_active='checked="checked"';
                                                    }                                                   
                                                    ?>
                                                    <input type="checkbox" value="0" name="enable_full_screen" id="enable_full_screen" <?php echo $str_active; ?>>
                                                    <input type="hidden" value="<?php echo $course_setting->new_window ?>" name="enable_full_screen_hidden" id="enable_full_screen_hidden"/> <span class="tip"><?php print_r(get_string('modulesarelaunch')) ?></span>
                                                </td>
                                            </tr>   



                                            <tr>
                                                <td colspan="2"><hr></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>
                                                  <div class="tip"><?php print_r(get_string('doyouwant')) ?></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th valign="top"><?php print_r(get_string('mustcomplete')) ?></th>
                                                <td>
                                                    
                                                    <select name="complete_by" id="complete_by" class="time_limit_type">
                                                      <option value="0" <?php echo $course_setting->time_span_complete==0 ? 'selected="selected"' : '' ; ?> ><?php print_r(get_string('none')) ?></option>
                                                        <option value="1" <?php echo $course_setting->time_span_complete==1 ? 'selected="selected"' : '' ; ?> ><?php print_r(get_string('timespan')) ?></option>
                                                        <option value="2" <?php echo $course_setting->time_span_complete==2 ? 'selected="selected"' : '' ; ?> ><?php print_r(get_string('specificdate')) ?></option>
                                                    </select>
                                                    <input type="hidden" value="" name="time_limit_type_hidden" id="time_limit_type_hidden" class="hidden timeLimitDate"/>                    
                                                    
                                                    <?php if ($course_setting->time_span_complete==1) {?>
                                                    <input type="text" value="<?php echo $course_setting->time_span_complete_number; ?>" size="4"  name="time_span" maxlength="3" id="time_span" class="numbers-only hidden timeLimitSpan"> 
                                                    <select name="time_spend_type" id="time_spend_type" class="hidden timeSpanType">
                                                        <option value="days" <?php echo $course_setting->time_span_complete_type=='days' ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('days')) ?></option>
                                                        <option value="weeks" <?php echo $course_setting->time_span_complete_type=='weeks' ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('weeks')) ?></option>
                                                    </select>
                                                                                                       
                                                    <?php } else { ?>
                                                    <input type="text" value="<?php echo $course_setting->time_span_complete_number; ?>" size="4"  name="time_span" maxlength="3" id="time_span" class="numbers-only hidden timeLimitSpan" style="display:none"> 
                                                    <select name="time_spend_type" id="time_spend_type" class="hidden timeSpanType" style="display:none">
                                                        <option value="days" <?php echo $course_setting->time_span_complete_type=='days' ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('days')) ?></option>
                                                        <option value="weeks" <?php echo $course_setting->time_span_complete_type=='weeks' ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('weeks')) ?></option>
                                                    </select>
                                                    
                                                    <?php }  ?>
 <?php if ($course_setting->time_span_complete==2) {?>                                                 
<div id="datepicker_div" style="height: 200px;">
<?php
          //2012-05-24	  
          $default_date_arr=explode('-',$course_setting->complete_specific_date);
          $default_day=$default_date_arr[2];
          $default_month=$default_date_arr[1];
          $default_year=$default_date_arr[0];
          $myCalendar = new tc_calendar("date2");
	  $myCalendar->setIcon($CFG->wwwroot."/common/calendar/images/iconCalendar.gif");
	  
          //$myCalendar->setDate('31', '5', '2012');
          $myCalendar->setDate($default_day, $default_month, $default_year);
     
	  $myCalendar->setPath($CFG->wwwroot."/common/calendar/");
	  $myCalendar->setYearInterval(1970, 2020);
	  $myCalendar->dateAllow('2008-05-13', '2015-03-01', false);
	  $myCalendar->startMonday(true);
	  //$myCalendar->disabledDay("Sat");
	  //$myCalendar->disabledDay("sun");
	  $myCalendar->writeScript();
	  ?>


</div>
 <?php } else { ?>
     <div id="datepicker_div" style="display:none;height: 200px;">
<?php
          //2012-05-24	  
          
          
          $myCalendar = new tc_calendar("date2");
	  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
	  //$myCalendar->setDate('31', '5', '2012');
          $myCalendar->setPath($CFG->wwwroot."/common/calendar/");
	  $myCalendar->setYearInterval(1970, 2020);
	  $myCalendar->dateAllow('2008-05-13', '2015-03-01', false);
	  $myCalendar->startMonday(true);
	  //$myCalendar->disabledDay("Sat");
	  //$myCalendar->disabledDay("sun");
	  $myCalendar->writeScript();
	  ?>


</div>                                               
 <?php }  ?>                                                   
                                                   <input type="hidden" value="" name="specific_date_complete" id="specific_date_complete" class="hidden timeLimitDate"/>                    
                                                </td>
                                            </tr>     

                                            <tr>
                                                <th></th>
                                                <td>
                                                    <input type="checkbox" value="<?php echo $course_setting->update_complete_date ?>" <?php echo $course_setting->update_complete_date==1 ? 'checked="checked"' : '' ; ?> name="apply_due_date" id="apply_due_date">
                                                    <input type="hidden" value="<?php echo $course_setting->update_complete_date ?>" name="apply_due_date_hidden" id="apply_due_date_hidden"> 
                                                    <?php print_r(get_string('updatethisdate')) ?> </td>
                                            </tr>

                                            <tr>
                                                <td colspan="2"><hr></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>
                                                    <div class="tip"><?php print_r(get_string('whenastudent')) ?></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th><?php print_r(get_string('compliantfor')) ?></th>
                                                <td>
                                                    <select name="compliant_for" id="compliant_for">
                                                        <option value="" <?php echo $course_setting->compliant_for=='' ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('forever')) ?></option>
                                                        <option value="1"<?php echo $course_setting->compliant_for==1 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day1')) ?></option>
                                                        <option value="2"<?php echo $course_setting->compliant_for==2 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day2')) ?></option>
                                                        <option value="3"<?php echo $course_setting->compliant_for==3 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day3')) ?></option>
                                                        <option value="4"<?php echo $course_setting->compliant_for==4 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day4')) ?></option>
                                                        <option value="5"<?php echo $course_setting->compliant_for==5 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day5')) ?></option>
                                                        <option value="6"<?php echo $course_setting->compliant_for==6 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day6')) ?></option>
                                                        <option value="7"<?php echo $course_setting->compliant_for==7 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day7')) ?></option>
                                                        <option value="8"<?php echo $course_setting->compliant_for==8 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day8')) ?></option>
                                                    </select><span class="compliantOtherDays hidden tip" style="display: none;"> 
                                                        <input type="text" value="" size="5" name="CompliantForOther" maxlength="4" id="CompliantForOther" class="numbers-only align-right"> days</span>
                                                </td>
                                            </tr>   
                                            <tr>
                                                <th><?php print_r(get_string('automaticresit')) ?></th>
                                                <td><span class="tip">
                                                        <select name="automatic_resit" id="automatic_resit">
                                                            <option value="" <?php echo $course_setting->automatic_resit=='' ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('dontresit')) ?></option>
                                                            <option value="1"<?php echo $course_setting->automatic_resit==1 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day1')) ?></option>
                                                            <option value="7"<?php echo $course_setting->automatic_resit==7 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day2')) ?></option>
                                                            <option value="14"<?php echo $course_setting->automatic_resit==14 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day3')) ?></option>
                                                            <option value="21"<?php echo $course_setting->automatic_resit==21 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day9')) ?></option>
                                                            <option value="30"<?php echo $course_setting->automatic_resit==30 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day4')) ?></option>
                                                            <option value="60"<?php echo $course_setting->automatic_resit==60 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day5')) ?></option>
                                                            <option value="90"<?php echo $course_setting->automatic_resit==90 ? 'selected="selected"' : '' ; ?>><?php print_r(get_string('day6')) ?></option>
                                                        </select> <?php print_r(get_string('beforecompliant')) ?></span>

                                                </td>
                                            </tr>                                            
                                        </tbody></table>

                                    <div class="form-sub-heading"><?php print_r(get_string('certificatetemp')) ?></div>
                                    <div class="tip">
                                      <p><?php print_r(get_string('ifyouattach')) ?>
                                            <a href=""><?php print_r(get_string('howtocreate')) ?></a></p>
                                    </div>            
                                    <input type="hidden" value="False" name="Course.UseCertificate" id="Course_UseCertificate" class="user-cert">             



                                    <table>
                                        <tbody><tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>                 
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('uploadtemp')) ?>
                                                </th>
                                                <td>
                                                    <input type="file" name="file" id="file" /> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><div class="tip"><?php print_r(get_string('templatecanonly')) ?></div></td>
                                            </tr>                 
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr> 
                                        </tbody></table>    

                                    <div class="form-sub-heading">
                                        <input type="checkbox" <?php echo $course_setting->fee>0 ? 'checked="checked"' : '' ; ?> name="sell_course" id="sell_course" class="enableCart">
                                        <input type="hidden" value="false" name="sell_course_hidden"> <?php print_r(get_string('iwanttosell')) ?></div>

                                    <table id="ecommerceSettings" class="simple-form hidden" style="<?php echo $course_setting->fee==0 ? 'display: none;' : '' ; ?>">
                                        <tbody><tr>
                                                <th></th>
                                                <td><div class="tip"><?php print_r(get_string('formore')) ?> <a href="/settings/ecommerce"><?php print_r(get_string('accountmenu')) ?></a>.</div></td>
                                            </tr>  
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('coursefee')) ?>
                                                </th>
                                                <td>
                                                    <select name="course_currency" id="course_currency">
                                                        <option value="USD" <?php echo $course_setting->currency=='USD' ? 'selected="selected"' : '' ; ?>>USD</option>
                                                        <option value="AUD" <?php echo $course_setting->currency=='AUD' ? 'selected="selected"' : '' ; ?>>AUD</option>
                                                        <option value="CAD" <?php echo $course_setting->currency=='CAD' ? 'selected="selected"' : '' ; ?>>CAD</option>
                                                        <option value="EUR" <?php echo $course_setting->currency=='EUR' ? 'selected="selected"' : '' ; ?>>EUR</option>
                                                        <option value="GBP" <?php echo $course_setting->currency=='GBP' ? 'selected="selected"' : '' ; ?>>GBP</option>
                                                        <option value="NZD" <?php echo $course_setting->currency=='NZD' ? 'selected="selected"' : '' ; ?>>NZD</option>
                                                    </select>&nbsp;
                                                    <input type="text" value="<?php echo $course_setting->fee ?>" size="8" name="course_fee" maxlength="8" id="course_fee" class="numbers-only">
                                                    <span id="Course_Fee_validationMessage" class="field-validation-valid"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('briefdes')) ?>
                                                </th>
                                                <td>
                                                    <textarea rows="5" name="sale_description" id="sale_description" cols="20" class="full shortDesc">
                                                        <?php echo $course_setting->sale_description ?>
                                                    </textarea>
                                                    <div class="tip" id="Course_SalesShortDescription_counter"> <span>255</span> character(s) left</div>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <th>
                                                    <?php print_r(get_string('fulldes')) ?>
                                                </th>
                                                <td>
                                                    <textarea rows="20" name="sale_full_description" id="sale_full_description" cols="20" class="full salesLongDesc">
                                                        <?php echo $course_setting->sale_full_description ?>
                                                    </textarea>
                                                    
                                                </td>
                                            </tr>                                                    
                                        </tbody></table>

                                    <div class="form-buttons">
                                      <input type="submit" class="btnlarge" value=" <?php print_r(get_string('save','admin')) ?> ">
                                       <?php print_r(get_string('or')) ?>
                                        <a href="<?php echo $CFG->wwwroot?>/course/settings/index.php?id=<?php echo $id ?>" class=""><?php print_r(get_string('cancel')) ?></a>                  

                                    </div>
                                </form>
                                
                            </div>

                        </div>

                    </div>
                </td>
                
            </tr>
        </tbody></table>


</div>