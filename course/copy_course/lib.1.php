<?php

require_once("../lib.php");
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function get_document_from_id($id) {
    global $DB;
    $document = $DB->get_record('document', array(
        'id' => $id
            ));

    return $document;
}

function insert_document($name, $file_name, $course_id) {
    global $DB;

    $document = new stdClass();
    $document->course_id = $course_id;
    $document->new_name = $name;
    $document->file_name = $file_name;
    $DB->insert_record('document', $document);
}

function get_documents_from_course($course_id) {
    global $DB, $USER;
    $sql = 'SELECT * from document where course_id = ' . $course_id;
    $result = $DB->get_records_sql($sql);
    return $result;
}

function displayJsAlert($ms, $redirectPath) {
    $html = '';
    if ($ms != '') {
        $html .= '<script language="javascript">alert("' . $ms . '")</script>';
    }

    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}

function delete_document($document_id) {
    global $DB;
    $DB->delete_records_select('document', " id = $document_id ");
}

function copy_group_course($old_course_id, $new_course_id) {
    global $DB;
    $sql = "insert into group_course(course_id,group_id) select $new_course_id,group_id from group_course where course_id=" . $old_course_id;
    $DB->execute($sql);
}

function copy_course_section($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO course_sections(course,section,name,summary,        
summaryformat,sequence,visible)
SELECT $new_course_id,section,name,summary,        
summaryformat,sequence,visible FROM course_sections WHERE course=$old_course_id";

    $DB->execute($sql);
}

function copy_course_document($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO document(new_name,file_name,course_id)
		SELECT new_name,file_name,$new_course_id from document where course_id=$old_course_id";
    $DB->execute($sql);
}

function copy_course_noticeboard($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO noticeboard(course,title,content,timemodified)
		SELECT $new_course_id,title,content,timemodified  from noticeboard where course=$old_course_id";

    $DB->execute($sql);
}


function copy_course_enrol($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO enrol(enrol,status,courseid,sortorder,name,             
enrolperiod,enrolstartdate,enrolenddate,     
expirynotify,expirythreshold,notifyall,        
password,cost,currency,roleid,customint1,       
customint2,customint3,customint4,customchar1,     
customchar2,customdec1,customdec2,customtext1,      
customtext2,timecreated,timemodified)

SELECT enrol,status,$new_course_id,sortorder,name,             
enrolperiod,enrolstartdate,enrolenddate,     
expirynotify,expirythreshold,notifyall,        
password,cost,currency,roleid,customint1,       
customint2,customint3,customint4,customchar1,     
customchar2,customdec1,customdec2,customtext1,      
customtext2,timecreated,timemodified from enrol WHERE courseid=$old_course_id AND enrol='manual'";

    $DB->execute($sql);
}

function copy_course($old_course_id) {
    global $DB;
    $sql = "INSERT INTO course (category,sortorder,fullname,shortname,idnumber,summary,summaryformat,           
format,showgrades,modinfo,newsitems,startdate,numsections,marker,maxbytes,                
legacyfiles,showreports,visible,visibleold,hiddensections,groupmode,               
groupmodeforce,defaultgroupingid,lang,theme,timecreated,timemodified,            
requested,restrictmodules,enablecompletion,completionstartonenrol,completionnotify)
	(SELECT category,sortorder,fullname,shortname,idnumber,summary,summaryformat,           
format,showgrades,modinfo,newsitems,startdate,numsections,marker,maxbytes,                
legacyfiles,showreports,visible,visibleold,hiddensections,groupmode,               
groupmodeforce,defaultgroupingid,lang,theme,timecreated,timemodified,            
requested,restrictmodules,enablecompletion,completionstartonenrol,completionnotify FROM course WHERE id=$old_course_id)";
		
    $DB->execute($sql);
    return get_last_insert_course_id();
}

function copy_course_settings($old_course_id, $new_course_id) {
    global $DB;
    $sql = "INSERT INTO course_settings(course_id,course_library,module_order,new_window,                 
must_complete_by,update_complete_date,compliant_for,              
automatic_resit,certificate_template,sell_course,course_sales_date,                
active,fee,currency,sale_description,time_span_complete,         
time_span_complete_number,time_span_complete_type,    
complete_specific_date,sale_full_description,      
file_name) SELECT $new_course_id,course_library,module_order,new_window,                 
must_complete_by,update_complete_date,compliant_for,              
automatic_resit,certificate_template,sell_course,course_sales_date,                
active,fee,currency,sale_description,time_span_complete,         
time_span_complete_number,time_span_complete_type,    
complete_specific_date,sale_full_description,      
file_name   FROM course_settings WHERE course_id=$old_course_id";



    $DB->execute($sql);
}

function get_members_enroll_of_course($course_id, $creator_id) {
    global $DB;

    $sql = "SELECT user2.id,
user2.firstname AS Firstname,
user2.lastname AS Lastname,
user2.email AS Email,
user2.city AS City,
course.fullname AS Course
,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
FROM course AS course 
JOIN enrol AS en ON en.courseid = course.id
JOIN user_enrolments AS ue ON ue.enrolid = en.id
JOIN user AS user2 ON ue.userid = user2.id AND course.id=:course_id AND (user2.parent=:creator_id OR user2.parent=0)";
    $params['course_id'] = $course_id;
    $params['creator_id'] = $creator_id;

    $result = $DB->get_records_sql($sql, $params);
    return $result;
}

function assign_user_to_course($user_id, $course_id) {
    global $CFG, $DB;
    require_once($CFG->dirroot . '/lib/enrollib.php');
    $enrol = enrol_get_plugin('manual');
    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);

    $enrol->enrol_user($instance, $user_id, 5, '', '');
}

function get_last_insert_course_id() {
    global $DB;
    $record = $DB->get_record_sql("select max(id) as id from course");
    return $record->id;
}

function insert_course_settings($name, $description, $code, $active_hidden, $hidden_lirary, $module_order, $enable_full_screen_hidden, $complete_by, $time_span, $time_spend_type, $date2, $apply_due_date_hidden, $compliant_for, $automatic_resit, $file_name, $sell_course, $course_sales_date, $course_currency
, $course_fee, $sale_description, $sale_full_description) {
    global $DB, $CFG, $USER;

    $course = new stdClass();

    $course->fullname = $name;
    $course->shortname = $name;
    $course->idnumber = $code;
    $course->summary = $description;
    $course->category = 1;
    $course->timecreated = time();
    $course->numsections = 0;

    $course_obj = create_course($course);

    $setting = new stdClass();

    $setting->course_id = $course_obj->id;
    $setting->course_library = $hidden_lirary;
    $setting->active = $active_hidden;
    $setting->module_order = $module_order;
    $setting->new_window = $enable_full_screen_hidden;
    $setting->update_complete_date = $apply_due_date_hidden;
    $setting->compliant_for = $compliant_for;
    $setting->automatic_resit = $automatic_resit;
    $setting->file_name = $file_name;
    $setting->sell_course = ($sell_course === "on"?1:0);
    if (($sell_course != null) AND ($setting->sell_course == 1)) {
        
    }
    if ($setting->sell_course == 1) {
        $setting->currency = $course_currency;
        $setting->fee = $course_fee;
        $setting->sale_description = $sale_description;
        $setting->sale_full_description = $sale_full_description;
        $setting->course_sales_date = date("Y-m-d");
    } else {
        $setting->currency = $course_currency;
        $setting->fee = 0;
        $setting->sale_description = "";
        $setting->sale_full_description = "";
        $setting->course_sales_date = NULL;
    }   
    if ($complete_by == 1) {
        if ($time_span != '') {
            $setting->time_span_complete = 1;
            $setting->time_span_complete_number = $time_span;
            $setting->time_span_complete_type = $time_spend_type;  //days, weeks
            $setting->complete_specific_date = null;
        }
    }

    if ($complete_by == 2) {
        if ($date2) {
            $setting->time_span_complete = 2;
            $setting->complete_specific_date = $date2;
            $setting->time_span_complete_number = 0;
            $setting->time_span_complete_type = 0;
        }
    }

    $DB->insert_record('course_settings', $setting);

    //enroll creator to course
    require_once ($CFG->dirroot . "/lib/enrollib.php");

    $enrol = enrol_get_plugin('manual');

    $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_obj->id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
    $instance = reset($instances);
    $enrol->enrol_user($instance, $USER->id, $course_obj->id, '', '');

    return $course_obj->id;
}

function get_course_settings($course_id) {
    global $DB;
    $course_setting = $DB->get_record('course_settings', array(
        'course_id' => $course_id
            ));

    return $course_setting;
}

function del_course($course_id) {
    global $DB;
    $course = $DB->get_record('course', array('id' => $course_id));
    delete_course($course);
    $DB->delete_records('course_settings', array('course_id' => $course_id));
}

function is_exist_course_settings($course_id) {
    global $DB;
    $course_setting = $DB->get_record('course_settings', array(
        'course_id' => $course_id
            ));

    return $course_setting->id;
}

function duplicate_get_course_modules($course, $module = '', $s = '') {
    global $DB, $USER, $OUTPUT;

    $params['userid'] = $USER->id;
    $params['active'] = ENROL_USER_ACTIVE;
    $params['enabled'] = ENROL_INSTANCE_ENABLED;
    $params['now1'] = round(time(), -2); // improves db caching
    $params['now2'] = $params['now1'];
    $params['now2'] = $params['now1'];
    $params['courseduplicate'] = $course->id;

    $sql = "SELECT c.id,c.category,c.sortorder,c.shortname,c.fullname,c.idnumber,c.startdate,c.visible,c.groupmode,c.groupmodeforce , ctx.id AS ctxid, ctx.path AS ctxpath, ctx.depth AS ctxdepth, ctx.contextlevel AS ctxlevel, ctx.instanceid AS ctxinstance
              FROM {course} c
              JOIN (SELECT DISTINCT e.courseid
                      FROM {enrol} e
                      JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                     WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                   ) en ON (en.courseid = c.id)
           LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = 50)";

    if ($course) {
        $where = " WHERE c.id != :courseduplicate";
        $sql .= $where;
    }

    $sql .= ' ORDER BY c.visible DESC,c.sortorder ASC';

    $courses = $DB->get_records_sql($sql, $params, 0, 20);


    $modulename = array();

    foreach ($courses as $c) {
        if ($module != 0) {

            $course_modules = $DB->get_records('course_modules', array('course' => $c->id, 'module' => $module), 'id ASC');
        } else {
            $course_modules = $DB->get_records('course_modules', array('course' => $c->id), 'id ASC');
        }

        $modinfo = get_fast_modinfo($c);

        if (count($course_modules) > 1) {
            foreach ($course_modules as $cm) {
                //$cm_text = get_print_section_cm_text($modinfo->cms[$cm->id], $course);

                $cm_object = $modinfo->cms[$cm->id];

                if ($cm_object != null) {
                    //$info['info'] = get_print_section_cm_info($cm_object, $course);
                    //$info['icon_url'] = get_print_section_cm_icon_url($cm_object, $course); 

                    $info = get_print_section_cm_info($cm_object, $course);
                    if ($s != null) {
                        if (strpos($info['name'], $s) === 0) {
                            $modulename[$cm->id] = $info;
                        }
                    } else {
                        $modulename[$cm->id] = $info;
                    }
                }
            }
        }
    }

    return $modulename;
}



?>
