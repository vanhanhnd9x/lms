<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the tab bar used on the manage/allow assign/allow overrides pages.
 *
 * @package    core
 * @subpackage role
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from a Moodle page
}


$id = optional_param('id', 0, PARAM_INT);
//$id      = o_param('id', PARAM_INT); // course id
if($id){
    $course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
}

?>
<?php 

if($course): ?>

<div class="panel-head clearfix" >
    <h3><?php echo $course->fullname ;?></h3>
    <div class="subtitle"><?php echo $course->idnumber ;?></div>
    <div class="subtitle"><?php echo $course->summary ;?></div>
</div>
<?php endif;?>
<?php
$toprow = array();
$toprow[] = new tabobject('content', new moodle_url('/course/view.php', array('id'=> $_GET['id'])), get_string('content'));
//$toprow[] = new tabobject('people', new moodle_url('/enrol/users.php', array('id'=> $_GET['id'])), 'People');
$toprow[] = new tabobject('people', new moodle_url('/course/people/index.php', array('id'=> $_GET['id'])), get_string('people'));
$toprow[] = new tabobject('teams', new moodle_url('/course/team/index.php',array('id'=> $_GET['id'])), get_string('teams'));
$toprow[] = new tabobject('noticeboard', new moodle_url('/mod/noticeboard/view.php',array('id'=> $_GET['id'])), get_string('noticeboard'));
$toprow[] = new tabobject('documents', new moodle_url('/course/document/index.php',array('id'=> $_GET['id'])), get_string('documents'));

$toprow[] = new tabobject('gradebook', new moodle_url('/grade/report/grader/index.php',array('id'=> $_GET['id'])), get_string('gradebook'));
//$toprow[] = new tabobject('gradesetting', new moodle_url('/grade/edit/settings/index.php',array('id'=> $_GET['id'])), 'Grade settings');
//$toprow[] = new tabobject('setting', new moodle_url('/course/edit.php',array('id'=> $_GET['id'])), 'Settings');
    if(is_teacher()&&!is_admin()){
    }
 else {
$toprow[] = new tabobject('settings', new moodle_url('/course/settings/index.php',array('id'=> $_GET['id'])), get_string('settings'));
 
    }
//$toprow[] = new tabobject('completion', new moodle_url('/course/completion.php',array('id'=> $_GET['id'])), 'Completion settings');
$tabs = array($toprow);

print_tabs($tabs, $currenttab);

