<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');

require_once('lib.php');
require_login(0, false);
$id = optional_param('id', 0, PARAM_INT); //course_id
$action = optional_param('action', '', PARAM_TEXT);
$document_id = optional_param('document_id', '', PARAM_TEXT);
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

if ($action == "delete_document") {
    delete_document($document_id);
    $doc = get_document_from_id($document_id);
    unlink($CFG->dirroot . "/course/document/upload/" . $doc->file_name);
    //echo displayJsAlert('document was removed', $CFG->wwwroot."/course/document/index.php?id=".$id);
    echo displayJsAlert('', $CFG->wwwroot . "/course/document/index.php?id=" . $id);
}


$documents = get_documents_from_course($id);
?>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<div id="page-content">
    <div id="region-main-box">
        <div id="region-post-box">
            <div id="region-main-wrap">
                <div id="region-main">
                    <div class="region-content">
                        <span id="maincontent"></span><div id="admin-fullscreen">
                            <div class="focus-panel" id="admin-fullscreen-left"> 
                                <!-- Left Content -->
                                <div class="body">
                                    <div class="course-content"><div class="panel-head clearfix">
                                            <h3><?php echo $course->fullname ?></h3>
                                            <div class="subtitle"><?php echo $course->idnumber ?></div>
                                            <div class="subtitle">
                                                <?php echo $course->summary ?>
                                            </div>
                                        </div>
                                        <div class="tabtree">
                                            <ul class="tabrow0">
                                                <li><a title="Content" href="<?php echo $CFG->wwwroot ?>/course/view.php?id=<?php echo $id ?>"><span><?php print_r(get_string('content')) ?></span></a> </li>
                                                <li><a title="People" href="<?php echo $CFG->wwwroot ?>/course/people/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('people')) ?></span></a> </li>
                                                <li><a title="Teams" href="<?php echo $CFG->wwwroot ?>/course/team/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('teams')) ?></span></a> </li>
                                                <li><a title="Noticeboard" href="<?php echo $CFG->wwwroot ?>/mod/noticeboard/view.php?id=<?php echo $id ?>"><span><?php print_r(get_string('noticeboard')) ?></span></a> </li>
                                                <li class ="selected"><a title="Documents" href="<?php echo $CFG->wwwroot ?>/course/document/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('documents')) ?></span></a> </li>
                                                <li><a title="Gradebook" href="<?php echo $CFG->wwwroot ?>/grade/report/grader/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('gradebook')) ?></span></a> </li>
                                                <?php
                                                if (!is_teacher()) {
                                                    ?>
                                                    <li class="last"><a title="Settings" href="<?php echo $CFG->wwwroot ?>/course/settings/index.php?id=<?php echo $id ?>"><span><?php print_r(get_string('settings')) ?></span></a> </li>
                                                <?php } ?>
                                            </ul>
                                        </div><div class="clearer"> </div>
                                        <div class="team_content">
                                            <table class="item-page-list">
                                                <tbody>
                                                    <?php foreach ($documents as $key => $val) { ?>
                                                    <tr id="de99d8dcf-a433-4886-ae18-b5e7b38cd71e" style="padding: 10px;">
                                                            <td class="main-col">
                                                                <div class="title">
                                                                    <a style="padding: 10px;" class="btn_image" title="" target="_blank" href="<?php echo $CFG->wwwroot ?>/course/document/upload/<?php echo $val->file_name ?>">
                                                                        <?php
                                                                        if ($val->new_name != "") {
                                                                            echo $val->new_name;
                                                                        } else {
                                                                            echo $val->file_name;
                                                                        }
                                                                        ?>
                                                                    </a></div>
                                                            </td>
                                                            <td>
                                                                <a href="#" onclick="javascript:displayConfirmBox('<?php echo $CFG->wwwroot ?>/course/document/index.php?action=delete_document&document_id=<?php echo $val->id ?>&confirm=yes&id=<?php echo $id ?>', 27, 'Bạn chắc chắn muốn xóa?')" title="<?php print_r(get_string('delete')) ?>" class="remove-msg btn-remove-ms float-right"></a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>                       
                                        </div>
                                    </div>
                                </div> 
                            </div><!-- Left Content -->
                            <script src="<?php echo $CFG->wwwroot ?>/blocks/course_addmodule/addmodules.js" type="text/javascript"></script>
                            <!-- Right Block -->
                            <div id="admin-fullscreen-right">
                                <div class="side-panel">
                                    <div class="action-buttons">
                                        <ul>
                                            <li><a alt="Add a document" onclick="" href="<?php echo $CFG->wwwroot ?>/course/document/add_document.php?id=<?php echo $id ?>" class="big-button drop" id="addTeams"><span class="left"><span class="mid"><span class="right"><span class="icon_add"><?php print_r(get_string('addadoc')) ?></span></span></span></span></a></li>
                                        </ul>

                                    </div>                
                                </div>                    
                            </div>
                        </div>  <!-- End Right -->
                    </div>      
                </div>
            </div>
        </div>
    </div>




</div>