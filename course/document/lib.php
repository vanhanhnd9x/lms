<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function get_document_from_id($id) {
	global $DB;
	$document = $DB->get_record('document', array(
		'id' => $id
		));

	return $document;
}
function insert_document($name,$file_name,$course_id){
    global $DB;
     
     $document=new stdClass();
     $document->course_id = $course_id;
     $document->new_name = $name;
     $document->file_name = $file_name;
     $DB->insert_record('document', $document);
          
}
function get_documents_from_course($course_id){
     global $DB,$USER;
    $sql = 'SELECT * from document where course_id = ' . $course_id;
    $result = $DB->get_records_sql($sql);
    return $result;
    
}
function displayJsAlert($ms, $redirectPath) {
    $html = '';
     if($ms!=''){
        $html .= '<script language="javascript">alert("' . $ms . '")</script>';
    }
    if ($redirectPath != '') {
        $html .= '<script language="javascript">window.location = "' . $redirectPath . '"</script>';
    }
    return $html;
}
function delete_document($document_id){
    global $DB;
	$DB->delete_records_select('document', " id = $document_id ");
}
?>
