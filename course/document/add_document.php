<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER;
require('../../config.php');
require_once('lib.php');
require_login(0, false);
$course_id = optional_param('id', 0, PARAM_INT); //course_id
$action = optional_param('action', '', PARAM_TEXT);
$document_name = optional_param('document_name', '', PARAM_TEXT);
$id = optional_param('id', 0, PARAM_INT); //course_id
if ($action == 'upload') {

    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    } else {

        move_uploaded_file($_FILES["file"]["tmp_name"], $CFG->dirroot . "/course/document/upload/" . $_FILES["file"]["name"]);
        insert_document($document_name, $_FILES["file"]["name"], $course_id);
        //echo displayJsAlert('Document was added', $CFG->wwwroot."/course/document/index.php?id=".$course_id);
        echo displayJsAlert('', $CFG->wwwroot . "/course/document/index.php?id=" . $course_id);
    }
}

$PAGE->set_title(get_string('addadoc'));
$PAGE->set_heading(get_string('addadoc'));
echo $OUTPUT->header();
?>
<div id="page-body">        
    <table class="admin-fullscreen">
        <tbody><tr>
                <td class="admin-fullscreen-left">
                    <div class="admin-fullscreen-content">
                        <div id="courseList" class="focus-panel">
                            <div class="panel-head">
                                <h3><?php print_r(get_string('addareference')) ?></h3>          
                            </div>
                            <div class="body">
                                <form method="post" id="form0" action="<?php echo $CFG->wwwroot ?>/course/document/add_document.php?action=upload" enctype="multipart/form-data">
                                    <div class="tip">
                                        <p><?php print_r(get_string('uploadadd')) ?></p>
                                        <ul>
                                            <li><?php print_r(get_string('notealpha')) ?></li>
                                            <li>Định dạng được chấp nhận: .xml, .gif, .png, .jpg, .rar, .zip</li>
                                            <li><?php print_r(get_string('maxfile')) ?> 5MB</li>
                                        </ul> 
                                    </div>
                                    <div>
                                        <div class="form-buttons">
                                            <input type="hidden" value="<?php echo $course_id ?>" name="id" >
                                            <label for="file"><?php print_r(get_string('filename')) ?></label>
                                            <input type="file" name="file" id="file" /> 
                                            <br/><br/>
                                            <?php print_r(get_string('optionalchang')) ?>
                                            <input type="text" name="document_name" />
                                            <br/>
                                            <br/>
                                            <hr>
                                            <input type="submit" class="btnlarge" value="<?php print_r(get_string('save', 'admin')) ?> " id="submitUpload">
                                            <?php print_r(get_string('or')) ?> <a href="<?php echo $CFG->wwwroot ?>/course/document/index.php?id=<?php echo $id ?>"><?php print_r(get_string('cancel')) ?></a>  
                                        </div>
                                </form>

                            </div>
                        </div>




                    </div>
                </td>

            </tr>
        </tbody></table>


</div>
