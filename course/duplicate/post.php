<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Duplicates a given course module
 *
 * The script backups and restores a single activity as if it was imported
 * from the same course, using the default import settings. The newly created
 * copy of the activity is then moved right below the original one.
 *
 * @package    core
 * @subpackage course
 * @copyright  2011 David Mudrak <david@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
require_once($CFG->libdir . '/filelib.php');

$ModuleId = optional_param('ModuleId', 0, PARAM_INT);
$CourseId = optional_param('CourseId', 0, PARAM_INT);
$Duplicate = optional_param('Duplicate', TRUE, PARAM_BOOL);

$course = $DB->get_record('course', array('id' => $CourseId),'*', MUST_EXIST);

$cm     = get_coursemodule_from_id('', $ModuleId, 0, true, MUST_EXIST);

//
//$cmid       = required_param('cmid',    PARAM_INT);
//$courseid   = required_param('course',  PARAM_INT);

$cmid       = $cm->id;
//$courseid   = 28;


//$course     = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
//$cm         = get_coursemodule_from_id('', $cmid, $CourseId, true, MUST_EXIST);
$cmcontext  = get_context_instance(CONTEXT_MODULE, $cm->id);
$context    = get_context_instance(CONTEXT_COURSE, $CourseId);
$section    = $DB->get_record('course_sections', array('id' => $cm->section, 'course' => $cm->course));

require_login($course);

//require_capability('moodle/course:manageactivities', $context);
//// Require both target import caps to be able to duplicate, see make_editing_buttons()
//require_capability('moodle/backup:backuptargetimport', $context);
//require_capability('moodle/restore:restoretargetimport', $context);

$PAGE->set_title(get_string('duplicate'));
$PAGE->set_heading($course->fullname);
//$PAGE->set_url(new moodle_url('/course/modduplicate.php', array('cmid' => $cm->id, 'courseid' => $course->id)));
$PAGE->set_pagelayout('incourse');

$output = $PAGE->get_renderer('core', 'backup');

// backup the activity

$bc = new backup_controller(backup::TYPE_1ACTIVITY, $cmid, backup::FORMAT_MOODLE,
        backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id);

$backupid       = $bc->get_backupid();
$backupbasepath = $bc->get_plan()->get_basepath();

$bc->execute_plan();

$bc->destroy();

// restore the backup immediately

$rc = new restore_controller($backupid, $CourseId,
        backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id, backup::TARGET_CURRENT_ADDING);

if (!$rc->execute_precheck()) {
    $precheckresults = $rc->get_precheck_results();
    if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
        if (empty($CFG->keeptempdirectoriesonbackup)) {
            fulldelete($backupbasepath);
        }

        echo $output->header();
        echo $output->precheck_notices($precheckresults);
        echo $output->continue_button(new moodle_url('/course/view.php', array('id' => $CourseId)));
        echo $output->footer();
        die();
    }
}

$rc->execute_plan();


$rc->destroy();

if (empty($CFG->keeptempdirectoriesonbackup)) {
    fulldelete($backupbasepath);
}



echo $output->header();

redirect(new moodle_url('/course/view.php', array('id'=>$CourseId)));
echo $output->footer();
