<?php
 require_once("../lib.php");
/*
 * Fuction get course module in another course
 */
function duplicate_get_course_modules($course, $module = '', $s = ''){
     global $DB, $USER , $OUTPUT;
   
    $params['userid']  = $USER->id;
    $params['active']  = ENROL_USER_ACTIVE;
    $params['enabled'] = ENROL_INSTANCE_ENABLED;
    $params['now1']    = round(time(), -2); // improves db caching
    $params['now2']    = $params['now1'];
    $params['now2']    = $params['now1'];
    $params['courseduplicate']    = $course->id;
    
      $sql ="SELECT c.id,c.category,c.sortorder,c.shortname,c.fullname,c.idnumber,c.startdate,c.visible,c.groupmode,c.groupmodeforce , ctx.id AS ctxid, ctx.path AS ctxpath, ctx.depth AS ctxdepth, ctx.contextlevel AS ctxlevel, ctx.instanceid AS ctxinstance
              FROM {course} c
              JOIN (SELECT DISTINCT e.courseid
                      FROM {enrol} e
                      JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                     WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                   ) en ON (en.courseid = c.id)
           LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = 50)";
        
    if($course){
        $where = " WHERE c.id != :courseduplicate";
        $sql .= $where;
    }
    
    $sql .= ' ORDER BY c.visible DESC,c.sortorder ASC';
    
    $courses = $DB->get_records_sql($sql, $params, 0, 20);
    
    $modulename = array();
    
    foreach($courses as $c){
        if($module!=0){
            
            $course_modules = $DB->get_records('course_modules', array('course'=>$c->id, 'module'=> $module), 'id ASC');
            
        }else{
            $course_modules = $DB->get_records('course_modules', array('course'=>$c->id), 'id ASC');
            
        }
        
        $modinfo = get_fast_modinfo($c);
                              
        if(count($course_modules)> 1){
            foreach($course_modules as $cm){              
                //$cm_text = get_print_section_cm_text($modinfo->cms[$cm->id], $course);
                $cm_object = $modinfo->cms[$cm->id];
                if ($cm_object != null) {
                    //$info['info'] = get_print_section_cm_info($cm_object, $course);
                    //$info['icon_url'] = get_print_section_cm_icon_url($cm_object, $course); 
                    $info = get_print_section_cm_info($cm_object, $course);
                    if ($s != null) {     
                        if(strpos($info['name'], $s) === 0){
                            $modulename[$cm->id] = $info;
                        }                                                                            
                    } else {
                        $modulename[$cm->id] = $info;
                    }                    
                }                  
            }
        }        
    }
    
    return $modulename;

  }

  
  
  function duplicate_course_modules_submit($post){
      global $DB, $USER ;
      
      
      echo "ddd";
      $params = array('course_module' => 136);
      
      $cm = $DB->get_record('course_modules', array('id'=>$params['course_module']));
      
      $course = $DB->get_record('course', array('id' => 27));
      
      $str_modinfo =  $course->modinfo;
      
      
      print "<pre>";
      print_r($course_module);
      
      print_r($course);
      
      
      
      $modinfo = unserialize($str_modinfo);
      
      $clone_cm = clone $cm;
      $clone_cm->id = null;
      $clone_cm->course = 27;
      $clone_cm->added = time();
      //$dataid = $DB->insert_record('course_modules', $clone_cm);
      
      
      // Update object in course.
        $cm_info = new stdClass();
      
        
        $cm_info->id = 27;
        $cm_info->cm = 118;
        $cm_info->mod = page;
        $cm_info->section = 0;
        $cm_info->sectionid = 86;
        $cm_info->module = 12;
        $cm_info->added = time();
        $cm_info->visible = 1;
        $cm_info->visibleold = 1;
        $cm_info->completion = 2;
        $cm_info->completionview = 1;
        $cm_info->name = '';
        
        $modinfo[$cm_info_id] = $cm_info;
        $course->modinfo = serialize($modinfo);
        $DB->update_record('course', $course);
      
      
      
  }