<?php
require_once("../../config.php");
require_once("lib.php");

$id = optional_param('id', 0, PARAM_INT);

$s = optional_param('s');
$mt = optional_param('mt');

if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}

$PAGE->set_url('/duplicate/index.php', array('id' => $course->id)); // Defined here to avoid notices on errors etc
require_login($course);
$PAGE->set_title(get_string('course') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

$list_course_module = duplicate_get_course_modules($course, $mt, $s);

?>

<div id="admin-fullscreen">
    <div id="admin-fullscreen-left" class="focus-panel"> 
        <!-- Left Content -->
        <div class="body">
            <div class="panel-head clearfix">
                <h3>Select a module to duplicate or link to this course</h3>
                <form method="get" action="">
                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                    <input type="text" value="<?php echo ($s != null)?$s:"" ?>" name="s" id="s" class="input-box">
                    <select name="mt" id="mt" class="combo-box">
                        <option value="0" <?php echo ($mt==0)?'selected="selected"':''; ?>></option>
                        <option value="14" <?php echo ($mt==14)?'selected="selected"':''; ?>>Video, PowerPoint, Flash Movie, Audio</option>
                        <option value="15" <?php echo ($mt==15)?'selected="selected"':''; ?>>SCORM</option>
                        <option value="13"  <?php echo ($mt==13)?'selected="selected"':''; ?>>Assessment</option>
                        <option value="16" <?php echo ($mt==16)?'selected="selected"':''; ?>>Survey</option>                                                
                        <option value="12" <?php echo ($mt==12)?'selected="selected"':''; ?>>Page</option>
                        <option value="7" <?php echo ($mt==7)?'selected="selected"':''; ?>>Discussion</option>
                        <option value="2" <?php echo ($mt==2)?'selected="selected"':''; ?>>Chat</option>
                        <option value="1"  <?php echo ($mt==1)?'selected="selected"':''; ?>>Assignment</option>
                        <option value="22" <?php echo ($mt==22)?'selected="selected"':''; ?>>Embed</option>
                        <option value="17" <?php echo ($mt==17)?'selected="selected"':''; ?>>Link</option>                                         
                    </select>
                    <input type="submit" value="Search">
                </form>            
            </div>
            <div class="content" style="margin-left: 10px">
                <form method="post" action="<?php echo new moodle_url('/course/duplicate/post.php') ; ?>">
                    <table class="item-page-list" id="modulelist">

                        <tbody>
                            <?php foreach ($list_course_module as $key => $cm) : ?>
                                
                            <tr>
                                <td><input type="radio" value="<?php echo $key ;?>" name="ModuleId" id="m<?php echo $key ;?>">
                                </td>
                                <td class="main-col"> 
                                    <div class="title"><label for="m<?php echo $key ;?>"> <?php echo $cm['name'] ; ?></label></div>
                                </td>
                                <td class="nowrap"><span title="<?php echo $cm['mod'] ; ?>" class="box-tag box-tag- moduletype-11">
                                        
                                        <img alt="Page" class="activityicon" src="<?php echo $cm['icon_url'] ?>"></img>
                                        
                                    </span></td>                        
                            </tr>
                            
                            <?php endforeach; ?>
                        </tbody></table>
                    
                    <div class="form-buttons">
                        <p>I want to <select name="Duplicate" id="Duplicate"><option value="true">Duplicate</option><option value="false">Link</option></select> this module into my course.</p>
                        <p>&nbsp;</p>
                        <input type="hidden" value="<?php echo $id ?>" name="CourseId" id="CourseId" />
                        
                        <input type="submit" class="btnlarge" value=" Save ">
                        or 
                        <a href="<?php echo new moodle_url('course/view.php', array('id'=>$id)) ; ?>">cancel</a>
                    </div>
                </form>
            </div>
            
            
        </div> 
    </div><!-- Left Content -->
    <div id="admin-fullscreen-right">

        <div class="side-panel">
        </div>                    
    </div>
</div>  <!-- End Right -->
<?php
echo $OUTPUT->footer();
?>