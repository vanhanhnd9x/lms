<?php

// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

require_once('../../config.php');
require_once('../../user/lib.php');
global $CFG;
require_once('lib.php');
require_once($CFG->dirroot . '/teams/lib.php');
require_once($CFG->dirroot . '/teams/config.php'); //config for send mail

require_login(0, false);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    print_error('disabled', 'message');
}

$action = optional_param('action', 0, PARAM_TEXT);
$team_id = optional_param('team_id', 0, PARAM_TEXT);
$course_id = optional_param('course_id', 0, PARAM_TEXT);
$username = optional_param('username', 0, PARAM_TEXT);
$user_id = optional_param('user_id', 0, PARAM_TEXT);
$confirm = optional_param("confirm", '', PARAM_TEXT);
$to_assign = optional_param('to_assign', 0, PARAM_TEXT);
$send_email_notification = optional_param('send_email_notification', 0, PARAM_TEXT);
if ($action == 'delete_team_member' && $confirm == '') {
    
}
if ($action == 'delete_team_member' && $confirm == 'yes') {
    $redirectPath = $CFG->wwwroot . "/teams/view_team.php?team_id=$team_id";
    remove_team_member($user_id, $team_id);
    //echo displayJsAlert('remove member of team succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}

if ($action == 'assign_team_to_course') {
    $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    // foreach ($to_assign as $key => $val) {
        assign_team_to_course($to_assign,$course_id);
        $course = get_course_obj_from_id($course_id);
        $team = get_team_from_id($to_assign);
        $members_in_team = get_members_in_team($to_assign);
        foreach ($members_in_team as $key1 => $val1) {
            
            if ($send_email_notification == 1) {
                
                $mailto = $val1->email;
                $username = $val1->username;
                $invitationContenNoEmail = $team_invitation_content;
                $invitationContenNoEmail = str_replace("{USER_NAME}", $username, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{TEAM_NAME}", $team->name, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{COURSE_TITLE}", $course->fullname, $invitationContenNoEmail);
                $invitationContenNoEmail = str_replace("{COURSE_DESCRIPTION}", $course->summary, $invitationContenNoEmail);
                send_email($mailto, $mailUsername, 'LMS', $team_invitation_title, $invitationContenNoEmail);
                $invitationContenNoEmail = $team_invitation_content;
            }
        }
    // }
    //echo displayJsAlert('assign teams to course succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}

if ($action == 'delete_course_team') {
    $redirectPath = $CFG->wwwroot . "/course/team/groups.php?team_id=$team_id";
    remove_course_team($course_id, $team_id);

    $members = get_members_enroll_of_course($course_id, $USER->id);
    // $redirectPath = $CFG->wwwroot . "/course/people/index.php?id=$course_id";
    if(!empty($members)){
        foreach ($members as $key => $val) {
            remove_person_from_course($val->id, $course_id);
        }
    }
    //echo displayJsAlert('remove team of course succesfully', $redirectPath);
    echo displayJsAlert('', $redirectPath);
}
?>