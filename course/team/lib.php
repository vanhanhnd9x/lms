<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function get_teams_from_course($course_id){
    global $DB;
	$sql = 'SELECT group_id FROM group_course
        WHERE course_id = :course_id';
	$params['course_id'] = $course_id;
	$result = $DB->get_records_sql($sql, $params);
        return $result;
        
}

function get_course_from_teams($group_id){
    global $DB;
    $sql = 'SELECT course_id FROM group_course
        WHERE group_id = :group_id';
    $params['group_id'] = $group_id;
    $result = $DB->get_records_sql($sql, $params);
        return $result;
        
}

function get_course_obj_from_id($course_id){
    global $DB;
	$course = $DB->get_record('course', array(
		'id' => $course_id
		));

	return $course;
}


function get_teams_from_user_id($user_id, $course_id){
    global $DB;
    $sql2 = "SELECT user_id, owner_id FROM {map_user_admin} mua WHERE mua.owner_id = :owner_id OR mua.user_id = :user_id";
    $params2['owner_id'] = $user_id;
    $params2['user_id'] = $user_id;
    $rows2 = $DB->get_records_sql($sql2, $params2);
    $id_list = "";
    foreach ($rows2 as $row2) {
        $id_list .= $row2->user_id . ",";
    }
    $id_list .= $user_id;
    
    //   Get teams from course id
    $teams_in_courses = get_teams_from_course($course_id);
    $teamId = "";
    foreach ($teams_in_courses as $teams_in_course) {
        $teamId .= $teams_in_course->group_id . ",";
    }
    $teamId .= "0";
    
    $field = "g.id, g.name, g.id_truong, g.description";
    $sql = "SELECT ".$field." FROM {groups} g WHERE (g.userid in (".$id_list.") OR g.courseid = :courseid) OR g.id in (".$teamId.")";
    $params['courseid'] = $course_id;
    $result = $DB->get_records_sql($sql,$params);
    return $result;
}


function assign_team_to_course($team_id,$course_id){
    global $DB;
     $group_course=new stdClass();
     $group_course->course_id = $course_id;
     $group_course->group_id = $team_id;
     $DB->insert_record('group_course', $group_course);
          
}

function get_list_course(){
    global $DB;
    $sql ="SELECT * FROM course WHERE NOT id=1";
    return $DB->get_records_sql($sql);
}

function get_groups_in_class($courseid){
    global $DB;
    $sql = 'SELECT course.fullname,course.id, groups.name
    FROM group_course,course,groups
    WHERE 
    course.id = group_course.course_id
    AND groups.id = group_course.group_id
    AND group_course.course_id=' . $courseid;

    $result = $DB->get_record_sql($sql);
    return $result;
}



include ('../../lib/phpmailer/class.phpmailer.php');
function send_email($to, $from, $from_name, $subject, $body) {
	global $CFG;
        include($CFG->dirroot . '/teams/config.php');
	
        global $error;

	$mail = new PHPMailer();	// create a new object
        $mail->IsSMTP(); // enable SMTP
	//$mail->IsHTML=true;
	$mail->IsHTML(true);
	$mail->SMTPDebug = 1;	// debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true;	// authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465;
	$mail->Username = $mailUsername;
	$mail->Password = $pass;
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->AddAddress($to);
	$mail->CharSet = "utf-8";
        
	if (!$mail->Send()) {
		$error = 'Mail error: ' . $mail->ErrorInfo;
		return false;
	} else {
		$error = 'Message sent!';
		return true;
	}
}

function get_members_enroll_of_course($course_id,$key="") {
    global $DB;
    $sql = "SELECT user2.id,
        user2.sex,user2.code,user2.name_parent,
        user2.firstname AS Firstname,
        user2.lastname AS Lastname,
        user2.email AS Email,
        user2.city AS City,
        user2.birthday AS Birthday,
        course.fullname AS Course
        ,(SELECT shortname FROM role WHERE id=en.roleid) AS Role
        ,(SELECT name FROM role WHERE id=en.roleid) AS RoleName
 
        FROM course AS course 
        JOIN enrol AS en ON en.courseid = course.id
        JOIN user_enrolments AS ue ON ue.enrolid = en.id
        JOIN user AS user2 ON ue.userid = user2.id WHERE NOT user2.id=2 AND course.id=:course_id AND en.roleid=5";
    $params['course_id'] = $course_id;
    //$params['creator_id'] = $creator_id;

    $result = $DB->get_records_sql($sql, $params);

    return $result;
}

?>
