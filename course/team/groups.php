<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

    if (!file_exists('../../config.php')) {
        header('Location: ../install.php');
        die;
    }
    global $CFG, $USER,$DB;
    require('../../config.php');
    require_once($CFG->dirroot . '/course/team/lib.php');
    require_once($CFG->dirroot . '/manage/schedule/lib.php');
    require_once($CFG->dirroot . '/teams/lib.php');
    require_login(0, false);
    $team_id = optional_param('team_id', 0, PARAM_TEXT); //group_id (id lớp học)

    $page         = optional_param('page', 0, PARAM_INT);
    $perpage      = optional_param('perpage', 15, PARAM_INT);
    $search       = optional_param('search', '', PARAM_TEXT);
    // $totalcount   = count(get_list_groups_class($team_id,null,$perpage,$search));
    $PAGE->set_title(get_string('listclassgroup').' '.get_team_from_id($team_id)->name);

    // $teams_in_course = get_teams_from_course($id);
    // $teams_from_user_id_and_course = get_teams_from_user_id($USER->id, $id);

    // $courses = get_list_course();

    echo $OUTPUT->header();
    global $CFG,$DB;
    $roleid=lay_role_id_cua_user_dang_nhap($USER->id);
    $moodle='groups';
    $check_xoacon=check_chuc_nang_xoachucnangcong($roleid,$moodle);
    $name1='listclassgroup';
    $check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
    if(empty($check_in)){
        echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
    }
    $trangthailop=$DB->get_record('groups', array(
            'id' =>$team_id
        ),
        $fields='id,status,id_namhoc', $strictness=IGNORE_MISSING
    );
    function dem_so_hs_theo_lop_cu_moi_tu_course($courseid,$schoolyear=null){
        global $DB;
        $time = time();
        if(!empty($schoolyear)){
            $sql = "SELECT COUNT(DISTINCT `user`.`id`)
                FROM user
                JOIN `history_student` ON `history_student`.`iduser`=`user`.`id`
                WHERE `user`.`id`NOT IN(2)
                AND `user`.`del`=0
                AND `history_student`.`idgroups` = {$courseid}
                AND `history_student`.`idschoolyear` = {$schoolyear}
                AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)";
        }else{
            $sql = "SELECT COUNT(DISTINCT `user`.`id`)
                FROM user
                    JOIN `role_assignments`ON `user`.`id`= `role_assignments`.`userid`
                    JOIN user_enrolments ON `user_enrolments`.`userid` = `user`.`id`
                    JOIN enrol ON `enrol`.`id` = `user_enrolments`.`enrolid`
                    JOIN course ON `enrol`.`courseid` = `course`.`id`
                WHERE `user`.`id`NOT IN(2)
                    AND `role_assignments`.`roleid`=5
                    AND `user`.`del`=0
                    AND `course`.`id` = {$courseid}
                    AND (`user`.`time_end_int` > {$time} OR `user`.`time_end_int` =0 OR `user`.`time_end_int` is null)";
        }
        $data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
        return $data;
    }
    switch ($trangthailop->status) {
        case 0:
        $groups_class = get_list_groups_class($team_id,$page,$perpage,$search);
        $schoolyear='';
        break;
        case 1:
        $groups_class = lay_ds_nhom_khi_chua_tien_hanh_len_lop($team_id,$trangthailop->id_namhoc);
        $schoolyear=$trangthailop->id_namhoc;
        break;
    }
?>



<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="table-rep-plugin">
                
               <!--  <div class="row">
                    <div class="form-group col-md-12">
                        <input id="foo-search" type="text" placeholder="<?php print_r(get_string('search')) ?>..." class="form-control" autocomplete="on">
                    </div>
                </div> -->
                
                
                <div class="table-responsive" data-pattern="priority-columns">
                    
                    <table id="table-foo" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th><?php print_r(get_string('namegroups')) ?></th>
                                <th><?php print_r(get_string('foreignteacher')) ?></th>
                                <th><?php print_r(get_string('tutorsteacher')) ?></th>
                                <th class="text-center"><?php print_r(get_string('totalstudents')) ?></th>

                                <!-- <th class="disabled-sorting text-right"><?php print_r(get_string('action')) ?></th> -->
                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                            $i=0;
                            foreach ($groups_class as $key => $val) {
                                $i++;
                                $team = get_team_from_id($key);
                                $courseid = get_course_from_id($key);
                                ?>
                            <tr>
                                <td class="text-center"><?php echo $i; ?></td>
                                <td><?php echo $val->fullname ?></td>
                                <?php 
                                // $gvnn = get_info_giaovien($val->id_gv); 
                                // $gvtg = get_info_giaovien($val->id_tg);
                                      ?>
                                <!-- <td><?php echo $gvnn->firstname .' '.$gvnn->lastname ?></td>
                                <td><?php echo $gvtg->firstname .' '.$gvtg->lastname ?></td> -->
                                <?php echo show_name_gvnn_gvtg_course($val->id); ?>
                                <td class="text-center">
                                    <?php
                                        $hs=dem_so_hs_theo_lop_cu_moi_tu_course($val->id,$schoolyear);
                                        // echo count(get_members_gr($val->id)) 
                                        echo $hs;
                                    ?>
                                </td>
                                
                                <!-- <td class="text-right">
                                    <a href="<?php echo $CFG->wwwroot ?>/course/settings/index.php?id=<?php echo $val->id ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="<?php echo $CFG->wwwroot ?>/course/people/index.php?id=<?php echo $courseid->id ?>" class="btn btn-warning btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a class="btn btn-danger btn-sm" title='<?php print_r(get_string('delete')) ?>' href='#'
                                        onclick="return Confirm('Xóa nhóm lớp','Bạn có muốn xóa nhóm lớp khỏi lớp học và đồng thời xóa tất cả học sinh trong nhóm lớp (nếu có)','Yes','Cancel','<?php echo $CFG->wwwroot ?>/course/team/team.php?action=delete_course_team&team_id=<?php echo $team_id ?>&course_id=<?php echo $courseid->id ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td> -->
                            </tr>
                           
                            <?php } ?>
                        </tbody>

                        <tfoot>
                            <tr class="active">
                                <td colspan="10">
                                    <div class="float-right">
                                        <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10 m-b-0"></ul>
                                    </div>
                                    <div class="float-left">
                                        <a href="<?php echo $CFG->wwwroot ?>/teams/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <!-- <?php
                        $course_str = 'nhóm lớp';
                        if (count($groups_class) > 1) {
                            $course_str = 'nhóm lớp';
                        }
                    ?>
                    <div class="tip center-padded"><?php echo count($groups_class) . " " . $course_str ?></div> -->
                </div>
            </div>
            <span id="maincontent"></span>                 
        </div>
    </div>
    
</div>
<style>
    .pagination > .active > a{
        background: #4bb747 !important;
        border: 1px solid #4bb747 !important;
        color:white !important;
    }
    .pagination > .active >  a:hover{
        background: #218838 !important;
        border: 1px solid #218838 !important;
        color:white !important;
    }
</style>
<?php
    echo $OUTPUT->footer();
?>