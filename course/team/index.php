<?php
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * This page prints a particular instance of manage
 * 
 * @author 
 * @version $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
 * @package manage
 * */
/// (Replace manage with the name of your module)

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}
global $CFG, $USER,$DB;
require('../../config.php');
require_once($CFG->dirroot . '/course/team/lib.php');
require_once($CFG->dirroot . '/teams/lib.php');
require_login(0, false);
$id = optional_param('id', 0, PARAM_INT); //course_id
if (!($course = $DB->get_record('course', array('id' => $id)))) {
    print_error('invalidcourseid', 'error');
}
$PAGE->set_title(get_string('class') . ': ' . $course->fullname);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();
$teams_in_course = get_teams_from_course($id);
$teams_from_user_id_and_course = get_teams_from_user_id($USER->id, $id);
$teams_of_courses_arr = array();
?>
<!-- <script type="text/javascript" src="js/jquery-1.7.1.js"></script> -->
<script type="text/javascript" src="js/search.js"></script>
<!-- <script type="text/javascript" src="js/jquery.autocomplete.js"></script> -->


<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div>
                <div class="row mr_bottom15">
                    <?php if(!is_teacher()){
                    ?>
                    <div class="col-md-2">
                        <a href="<?php print new moodle_url('/teams/new_team.php'); ?>" class="btn btn-info">Thêm mới</a>
                    </div>
                    <div class="col-md-3">
                        
                            <a alt="Assign to teams" data-toggle="modal" data-target="#nhomlop" class="btn btn-success">
                                Gán nhóm cho lớp                            </a>
                                          
                    </div>
                    <?php } ?>

                </div>
                
                
                <div data-pattern="priority-columns" id="searchResults">
                    <form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
                    <table id="tech-companies-1" class="table table-hover">
                        <thead>
                            <tr>
                                <th data-priority="1">Tên nhóm lớp</th>
                                <th data-priority="1">Giáo viên ngoại ngữ</th>
                                <th data-priority="1">Giáo viên trợ giảng</th>
                                <th data-priority="1">Tên giáo trình</th>
                                <th data-priority="1">Phòng học</th>
                                <th data-priority="1">Giờ học</th>
                                <th data-priority="1">Thứ</th>
                                <th data-priority="1">Thời gian học</th>

                                <th class="disabled-sorting text-right">Hành động</th>
                            </tr>
                        </thead>

                        <tbody id="ajaxschools">
                            <?php foreach ($teams_in_course as $key => $val) {
                                            $teams_of_courses_arr[] = $key;
                                            $team = get_team_from_id($key);
                                            ?>
                            <tr>
                                <td><a href="<?php echo $CFG->wwwroot ?>/teams/view_team.php?team_id=<?php echo $team->id ?>"><?php echo $team->name ?></a></td>
                                <?php $gvnn = get_info_giaovien($team->id_gvnn); 
                                     $gvtg = get_info_giaovien($team->id_gvtg) ?>
                                <td><?php echo $gvnn->firstname .' '.$gvnn->lastname ?></td>
                                <td><?php echo $gvtg->firstname .' '.$gvtg->lastname ?></td>
                                <td><?php echo $team->tengt ?></td>
                                <td><?php echo $team->phonghoc ?></td>
                                <td><?php echo $team->giohoc ?></td>
                                <td><?php echo $team->thu ?></td>
                                <td><?php echo $team->time_start .' - '.$team->time_end ?></td>

                                
                                <td class="text-right">
                                    <a href="<?php echo $CFG->wwwroot ?>/teams/team_settings.php?team_id=<?php echo $team->id ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a class="btn btn-danger btn-sm" title='<?php print_r(get_string('delete')) ?>' onclick='javascript:displayConfirmBox("<?php echo $CFG->wwwroot ?>/course/team/team.php?action=delete_course_team&team_id=<?php echo $key ?>&course_id=<?php echo $id ?>&confirm=yes",<?php echo $course->id ?>, "Bạn chắc chắn muốn xóa nhóm này khỏi khóa học?")' href='#'><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </form>
                    <?php
                        $course_str = get_string('team');
                        if (count($teams_in_course) > 1) {
                            $course_str = get_string('teams');
                        }
                    ?>
                    <div class="tip center-padded"><?php echo count($teams_in_course) . " " . $course_str ?></div>
                </div>
            </div>
            <span id="maincontent"></span>                 
        </div>
    </div>
    
</div>


<!-- Modal Nhóm lớp -->
<div id="nhomlop" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="assign-search">                                                           
            <form action="<?php echo $CFG->wwwroot ?>/course/team/team.php?action=assign_team_to_course&course_id=<?php echo $id ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Danh sách học sinh</h4>
                    <input type="text" class="form-control col-6" autocapitialize="off" autocorrect="off" autocomplete="off" name="s" id="assignSearchTeams" placeholder="Tìm kiếm...">
                    <input type="hidden" value="<?php echo $id ?>" name="course_id" id="course_id" />
                </div>
                <div class="modal-body">
                    <div data-pattern="priority-columns" id="assignResults">
                        <table id="tech-companies-1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="" id="checkAll"></th>
                                    <th data-priority="1">Tên lớp</th>
                                    <th data-priority="1">Trường học</th>
                                    <!-- <th data-priority="1">Ngày sinh</th> -->
                                    <th class="disabled-sorting text-right">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody id="ajaxschools">
                                <?php
                                    foreach ($teams_from_user_id_and_course as $key => $val) {
                                ?>
                                <tr>
                                    <td>
                                        <?php if (in_array($val->id, $teams_of_courses_arr)) { ?>
                                            &nbsp;
                                        <?php } else {
                                            ?>
                                            <input type="checkbox" value="<?php echo $val->id ?>" name="to_assign[]" id="to_assign_<?php echo $val->id ?>">
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $val->name  ?></td>
                                    <td><?php echo get_info_truong($val->id_truong)->name  ?></td>
                                    <!-- -->
                                    
                                    <td class="text-right">&nbsp;
                                        <?php if (in_array($val->id, $teams_of_courses_arr)) { ?>
                                            <span title="" class="badge badge-success"><span>
                                                    <?php print_r(get_string('alreadyassigned')) ?>
                                                </span></span>                       
                                        <?php } else {
                                            ?>
                                            <span title="" class=""><span>
                                                </span></span>                       
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light waves-effect" data-dismiss="modal"><?php print_r(get_string('cancel')) ?></button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><?php print_r(get_string('assign', 'cohort')) ?></button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
    echo $OUTPUT->footer();
?>