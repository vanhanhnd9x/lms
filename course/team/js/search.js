$(document).ready(function(){
$('#li_dashboard').removeClass('active');
$('#li_course').addClass('active');
$('#assignCancel').click(function() {
        $('#facebox').hide();
        $('#overlay').fadeOut('fast');
  
    });
      
    
    $('#assignSearchTeams').focus(function() {
        $('#searchPrompt').html('');
    });
    $('#assignSearchTeams').keydown(function() {
        $('#searchPrompt').html('');
    });

     $('#assignSearchTeams').keyup(function() {
        var course_id=document.getElementById('course_id').value;
    
        if($(this).val().length>0){
            search_team_to_assign($(this).val(),course_id);
        }
        else{
            $('#searchPrompt').html('Tìm kiếm nhóm để gán');
            search_team_to_assign('',course_id);
        }
    });
    
    
    

    $(function() {
        $('#assignPeople').click(function(){
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    $(function() {
        $('#assignCourses').click(function(){
            
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    $(function() {
        $('#addTeams').click(function(){
            
            $('#overlay').fadeIn('fast',function(){
                $('#facebox').show();
            });
        });
    });
    
    //send mail notification when assing user to team
    
     $('#SendEmail').click(function(event) {   
        if(this.checked) {
            document.getElementById('send_email_notification').value=1;
            
        }
        else { 
           document.getElementById('send_email_notification').value=0; 
        }
    });
    //finish send mail notification when assing user to team
    
   
});


function displayConfirmBox(redirectPath,id,ms){
    var where_to= confirm(ms);
    if (where_to== true)
    {
        window.location=redirectPath;
    }
    else
    {
	
        return false;
    }
}

function search_team_to_assign(search_val,course_id){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("assignResults").innerHTML=xmlhttp.responseText;
            
        }
    }
    xmlhttp.open("GET","livesearch_team_assign_to_course.php?q="+search_val+'&course_id='+course_id,true);
    xmlhttp.send();
}

