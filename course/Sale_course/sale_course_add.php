<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
    $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin

require_login(0, false);
//$strmessages = '';
$PAGE->set_title("Course");
$PAGE->set_heading("Course");
//now the page contents
//$PAGE->set_pagelayout('courses');
//echo $OUTPUT->header();

//$PAGE->requires->js('/manage/manage.js');
//$PAGE->requires->js('/theme/nimble/jquery.js');
//$PAGE->requires->js('/manage/search.js');
// Require Login.
require_login();
//List My Course
$id = optional_param('id', 0, PARAM_INT);
$courses = enrol_get_my_courses_second();

?>
   <link href="../../theme/nimble/style/style_sale_course.css" rel="stylesheet" type="text/css">
<div class="container_16">
        <div class="grid_16">
         
        <h1 class="org-name"><?php echo $USER->company ?></h1>
    
        </div>
        </div>
    <div><?php 
                foreach ($courses as $course)
                {
                    $course_setting = get_course_settings($course->id);
                
                   if($course->id == $id)
                   {
                ?>
        
        <form action="checkout.php?action=add&id=<?php echo $course->id ?>&userid=<?php echo $userid ?>" method="post">
    <div class="container_16">
        <div class="grid_16">
            <div class="p-title">
                
            <h1>
                <?php echo $course->fullname ?></h1>
                </div>
        </div>
    </div>
    <div class="container_16">
        <div class="grid_11">
            <div class="course-description">
                &nbsp;
                <?php echo $course_setting->sale_full_description ?>
            </div>

             
        </div>
         <div class="grid_5">
            <div class="signup-box">
                
                <div>This course is <b><?php echo $course_setting->currency." ".$course_setting->fee ?></b></div>
                       
                <input type="submit" class="btn-green" value=" Add to cart "  /> or <a class="cancel-link" href="sale_course.php">cancel</a>
                <br />
                <div class="addthis_toolbox addthis_default_style">
                    <a class="addthis_button_tweet"></a><span class="addthis_separator"></span><a class="addthis_button_facebook_like"></a>
                </div> 
             </div> 
        </div>
    </div>
      <?php
                   }
            }?>
    <div class="container_16">
        <div class="grid_16">
            <div class="footsy">
            <div class="powered-by">
                <a href="">Learning
                    Management System</a> by <?php echo $USER->company ?></div>            
            </div>     
        </div>
    </div>
    </form>
    </div>

  