<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
  $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
  $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin

require_login(0, false);
$PAGE->set_title("Course");
$PAGE->set_heading("Course");
// Require Login.
require_login();
//List My Course
$courses = enrol_get_my_courses();
global $DB, $USER;
$sql = "SELECT * FROM enrol_paypal ";
$rows = $DB->get_records_sql($sql);
?>
<link href="../../theme/nimble/style/style_sale_course.css" rel="stylesheet" type="text/css">

<div class="container_16">
  <div class="grid_16">

    <h1 class="org-name"><?php echo $USER->company . "<p>"; //if($t=1){ echo "<b style='color:blue'>"."process success !"."</b>";}                ?></h1>

  </div>
</div>
<div>


  <div class="container_16">
    <div class="grid_16">
      <div class="p-title">
        <h1>Online Courses Admin</h1>
      </div>
    </div>
  </div>

  <div class="container_16">
    <div class="grid_11">

      <table style="width: 800px" class="grid">
        <?php
        ?>
        <?php
        foreach ($rows as $row) {
          $getuser = get_user_from_user_id($row->id);
          foreach ($courses as $course) {

            $course_setting = get_course_settings($course->id);
            // $enrol_paypal = get_enrol_paypal($course->id);
            if ($row->courseid == $course->id) {
              // $arr = explode(',', $i);
              //var_dump($arr);
              ?>

              <tr>
                <td class="maincol">
                  <div class="title"><?php echo $course->fullname ?></div>
                  <div class="desc-tip"><?php echo $course_setting->sale_description ?></div></td>  
                <td class="nowrap" align="right">
                  <a href="#" class="box-tag box-tag-green " title=""><span><?php echo $course_setting->currency . " " . $course_setting->fee ?></span></a>
                </td>
                <td>
                  <?php 
                    echo $row->receiver_email;
                  ?>
                </td>
                <?php
                if ($row->option_name2 == "1") {
                  ?>
                  <td>
                    <a href="#" class="box-tag box-tag-green " ><span>Complete</span></a>
                    
                  </td>
                  <?php
                }
                else {
                  ?>
                  <td>
                    In progress
                  </td>
                  <?php
                }
                ?>
                <td>
                  <a href="sale_course_admin_edit.php?enrol_paypal=<?php echo $row->id ?>">Complete</a>
                </td>

              </tr>
              <?php
            }
          }
          ?>

          <?php
        }
        ?>
      </table>






      <div class="powered-by"><a href="#">Learning  Management System</a> by <?php echo $USER->company ?></div>

    </div>

  </div>
</div>


