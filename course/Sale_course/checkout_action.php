<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
    $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin

require_login(0, false);
//$strmessages = '';
$PAGE->set_title("Course");
$PAGE->set_heading("Course");
//now the page contents
//$PAGE->set_pagelayout('courses');
//echo $OUTPUT->header();

//$PAGE->requires->js('/manage/manage.js');
//$PAGE->requires->js('/theme/nimble/jquery.js');
//$PAGE->requires->js('/manage/search.js');
// Require Login.
require_login();

$firstname = optional_param('FirstName', '', PARAM_TEXT);
$lastname = optional_param('LastName', '', PARAM_TEXT);
$email = optional_param('Email', '', PARAM_TEXT);
$confirmemail = optional_param('ConfimEmail', '', PARAM_TEXT);
$total = optional_param('total', '', PARAM_TEXT);
$userid = optional_param('userid','',PARAM_TEXT);
if($USER->firstname==$firstname && $USER->lastname==$lastname && $USER->email==$email )
{
    header("Location:payment.php?total=$total&userid=$userid ");
}
else{
    header("Location:checkout.php");
}
?>
