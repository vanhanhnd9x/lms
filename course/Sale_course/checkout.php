

<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
    $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin

require_login(0, false);
//$strmessages = '';
$PAGE->set_title("Course");
$PAGE->set_heading("Course");
//now the page contents
//$PAGE->set_pagelayout('courses');
//echo $OUTPUT->header();

//$PAGE->requires->js('/manage/manage.js');
//$PAGE->requires->js('/theme/nimble/jquery.js');
//$PAGE->requires->js('/manage/search.js');
// Require Login.
require_login();
//List My Course
$courses = enrol_get_my_courses_second();


$id = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$cart = $_SESSION["CART"];
switch ($action){
    case "add":
                if($cart){
                if(array_key_exists($id, $cart)){
                    $cart[$id] += 1;
                }  else {
                    $cart[$id] = 1;
                }
            }  else {
                    $cart[$id] = 1;
            }
                $_SESSION["CART"] = $cart;
                header("Location:checkout.php");
                break;
    case "delete":
        if ($cart){
            foreach(array_keys($cart) as $value){
                if($value != $id){
                    $newcart[$value] = $cart[$value];
                }
            }
            $_SESSION["CART"] = $newcart;
            $cart = $newcart;
        }
        header("Location:checkout.php");
        break;
}


?>
<link href="../../theme/nimble/style/style_sale_course.css" rel="stylesheet" type="text/css">
<script >
    function check(){
        with(document){
            
            if(f1.FirstName.value==""){
                 alert('not be blank !');
                f1.FirstName.focus();
                return false;
            }
            if(f1.LastName.value==""){
                 alert('not be blank !');
                f1.LastName.focus();
                return false;
            }
            if(f1.Email.value == ""){
                 alert('not be blank !');
                f1.Email.focus();
                return false;
            }
            if(f1.ConfirmEmail.value == ""){
                 alert('not be blankl !');
                f1.ConfirmEmail.focus();
                return false;
            }
            if(f1.ConfirmEmail.value != f1.Email.value){
                alert('Confirmemail must be the same Email !');
                f1.ConfirmEmail.focus();
                return false;
            }
            r = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (r.test(f1.Email.value)==false){
                alert('Please provide a valid Email address');
                f1.Email.focus();
                return false;
            }
            
        }
    }
</script>
 <div class="container_16">
        <div class="grid_16">
         
        <h1 class="org-name"><?php echo $USER->company ?></h1>
    
        </div>
        </div>
    <div>
        
 
    <div class="container_16">
        <div class="grid_16">
            <div class="p-title">
                <h1>Course Checkout</h1>
            </div>
        </div>
    </div>
    <div class="container_16">
        <div class="grid_11">
            

            <table class="grid">
            <?php 
                            if($cart){
                                  $total = 0;
                                foreach(array_keys($cart) as $value){
                                     foreach($courses as $course){
                                        $course_setting = get_course_settings($course->id);
                                        if($course->id == $value){
                                             
                                
                            
                        ?>
                <tr>
                    <td>
                        
                        <div class="title"> <?php echo $course->fullname ?> </div>
                        <div class="desc-tip">  <?php echo $course_setting->sale_description ?>
                        </div>
                    </td>
                    <td class="ecom-remove"><span class="box-tag box-tag-grey " title=""><span><?php echo $course_setting->currency."  ".$course_setting->fee.".00"; ?></span></span> <a href="checkout.php?action=delete&id=<?php echo $course->id ;$total += $course_setting->fee;?>" class="box-tag box-tag-red " title=""><span>remove</span></a></td>
                </tr>
            
                      <?php 
                    }
                                }
                            }
                            }
                ?>
                <tr>
                    <td colspan="2" class="ecom-footer"><a href="sale_course.php?userid=<?php echo $USER->id ?>">Add more courses to cart</a><span class="float-right"><?php echo $course_setting->currency."  ". $total.".00"  ?></span></td>
                </tr>
                
            </table>
            
        </div>
   
        <div class="grid_5">
            <div class="signup-box">
                <form action="payment.php?userid=<?php echo $USER->id ?>" method="post" onsubmit="return check();" name="f1">
              
                <table class="signup-form">
                    <tr>
                        <th>
                            <?php print_r(get_string('firstname')) ?>
                        </th>
                        <td>
                            <input id="FirstName" name="FirstName" type="text" value="" />
                            
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php print_r(get_string('lastname')) ?>
                        </th>
                        <td>
                            <input id="LastName" name="LastName" type="text" value="" />
                            
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Email Address:
                            <div class="help-text">
                                This will be your username</div>
                        </th>
                        <td>
                            <input id="Email" name="Email" type="text" value="" />
                            
                        </td>
                    </tr>
                    <tr>
                        <th nowrap="nowrap">
                            Email Address again:
                        </th>
                        <td>
                            <input id="ConfirmEmail" name="ConfirmEmail" type="text" value="" />
                            
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                        </th>
                        <td>
                           <input type="submit" class="green-btn" value="Check Out"  />
                           <input type="hidden" value="<?php echo $total ?>" name="total" />
                           <input type="hidden" value="<?php echo $idlist ?>" name="idlist"/>
                        </td>
                    </tr>
                </table>
                </form>
            </div>
        </div>
    </div>
    <div class="container_16">
        <div class="grid_16">
            <div class="footsy">
                <div class="powered-by">
                <a href="#">Learning
                    Management System</a> by <?php echo $USER->company ?></div>            
            </div> 
        </div>
    </div>
    
    </div>
