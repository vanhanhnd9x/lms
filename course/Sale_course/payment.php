<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    if (!file_exists('../../config.php')) {
        header('Location: ../install.php');
        die;
    }

    require('../../config.php');
    require_once($CFG->dirroot . '/manage/lib.php');
    require_once($CFG->dirroot . '/lib/pagelib.php');
    require_once($CFG->dirroot . '/common/lib.php');


    // We are currently keeping the button here from 1.x to help new teachers figure out
    // what to do, even though the link also appears in the course admin block.  It also
    // means you can back out of a situation where you removed the admin block. :)
    if ($PAGE->user_allowed_editing()) {
        $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
        $PAGE->set_button($buttons);
    }

    // Check for valid admin user - no guest autologin

    require_login(0, false);
    //$strmessages = '';
    $PAGE->set_title("Course");
    $PAGE->set_heading("Course");
    
    // Require Login.
    require_login();
$courses = enrol_get_my_courses_second();
foreach ($courses as $course){
$course_setting = get_course_settings($course->id);
}
//var_dump($course_setting);
$firstname = optional_param('FirstName', '', PARAM_TEXT);
$lastname = optional_param('LastName', '', PARAM_TEXT);
$email = optional_param('Email', '', PARAM_TEXT);
$confirmemail = optional_param('ConfimEmail', '', PARAM_TEXT);
$total = optional_param('total', '', PARAM_TEXT);
$userid = optional_param('userid','',PARAM_TEXT);
$cart = $_SESSION["CART"];
//echo $cart;

?>
<link href="../../theme/nimble/style/style_sale_course.css" rel="stylesheet" type="text/css">
<script >
    function check(){
        with(document){
            
            if(f1.Transaction_CardNumber.value==""){
                alert('not be blank !');
                f1.Transaction_CardNumber.focus();
                return false;
            }
            if(f1.Transaction_CardHolderName.value==""){
                alert('not be blank !');
                f1.Transaction_CardHolderName.focus();
                return false;
            }
            
          }
    }
</script>
<div class="container_16">
        <div class="grid_16">
     
        </div>
    </div>
   <div class="container_16">
        <div class="grid_16">
            <div class="p-title">
                <h1>Course Checkout: Secure Payment</h1>
             </div>
        </div>
    </div>

<form action="payment_action.php?userid=<?php echo $userid ?>" onsubmit="return check();" name="f1" method="post"><input name="__RequestVerificationToken" type="hidden" value="KD4WKHYYDBtZtXgPt8cyLEA6XsYrrzsQmKXUdLJFRQBiHTxaQkfTc+YAE2X0vXIMYKdUAOXvonuYQVgt/rq15mvdezCM8CJEWqYI3Zuz137FL+0pJWGYGl82Q/Wu4k77zenJ1DCLmc+0IRCA83scmg==" />
    <div class="container_16">
        <div class="grid_5">
            &nbsp;
        </div>
        
        <div class="grid_6">
    
       
        
             
            <input id="Transaction_AgreedToTerms" name="Transaction.AgreedToTerms" type="hidden" value="True" />
            <input id="ProspectId" name="ProspectId" type="hidden" value="5681" />

            <table class="signup-form">
   
                <tr>
                    <th>
                        Amount:
                    </th>
                    <td>
                        <b>
                            <?php $t = " $course_setting->currency $total.00 " ;echo $course_setting->currency." ".$total.".00" ?></b>
                    </td>
                </tr>
                <tr>
                    <th>
                        Card Type:
                    </th>
                    <td>
                        <select id="CardType" name="CardType">
                            <option value="Visa">Visa</option>
                            <option value="Mastercard">Mastercard</option>
                            <option value="American Express">American Express</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        Card Number:
                    </th>
                    <td> 
                        <input id="Transaction_CardNumber" name="Transaction_CardNumber" type="text" value="" />
                        
                    </td>
                </tr>
                <tr>
                    <th>
                        Expiry Date:
                    </th>
                    <td>
                        <select id="Transaction_CardExpiryMonth" name="Transaction_CardExpiryMonth">
                            <?php for($i=1;$i<=12;$i++)
                                    
                            
                            echo "<option>"." $i"."</option>";
                            ?>
                        </select>
                        /
                        <select id="Transaction_CardExpiryYear" name="Transaction_CardExpiryYear">
                            <?php for($i=2012;$i<=2050;$i++)
                            {        
                            
                            echo "<option>"." $i"."</option>";
                            }
                             
                            ?>
                            

                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        Card Holder Name:
                    </th>
                    <td>
                        <input id="Transaction_CardHolderName" name="Transaction_CardHolderName" type="text" value="" />
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" >
                        <input type="submit"  value=" Process Transaction "  class="green-btn" /> 
                        
                        <input type="hidden" value="<?php echo $firstname ?>" name="firstname"/>
                        <input type="hidden" value="<?php echo $lastname ?>" name="lastname"/>
                        <input type="hidden" value="<?php echo $email ?>" name="email"/>
                        <input type="hidden" value="<?php echo $userid ?>" name="userid"/>  
                        <input type="hidden" value="<?php echo $t ?>" name="total"/>
                        <input type="hidden" value="true" name="dk"/>
                    </td>
                </tr>
      
            </table>
     

    </div>
    <div class="grid_5">
            &nbsp;</div>
    </form>
       
    <div class="container_16">
        <div class="grid_16">
            <div class="footsy">
                <div class="powered-by">
                <a href="#">Learning
                    Management System</a> by <?php echo $USER->company ?></div>            
            </div> 
        </div>
    </div>
  


    </div>