<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
    $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin

require_login(0, false);
//$strmessages = '';
$PAGE->set_title("Course");
$PAGE->set_heading("Course");
// Require Login.
require_login();
//List My Course
$courses = enrol_get_my_courses_second();
// var_dump($courses);
//$t = optional_param('t',0,PARAM_INT);
$userid = optional_param('userid',0, PARAM_INT);
//var_dump($USER->id);
$getuser = get_user_from_user_id($userid);

?>
<link href="../../theme/nimble/style/style_sale_course.css" rel="stylesheet" type="text/css">
   
<div class="container_16">
        <div class="grid_16">
         
        <h1 class="org-name"><?php echo $USER->company."<p>"; //if($t=1){ echo "<b style='color:blue'>"."process success !"."</b>";} ?></h1>
    
        </div>
</div>
    <div>
        
        
    <div class="container_16">
        <div class="grid_16">
            <div class="p-title">
                <h1>Online Courses</h1>
            </div>
        </div>
    </div>

<div class="container_16">
<div class="grid_11">
    <table class="grid">
            <?php 
          foreach ($courses as $course) {
            $course_setting = get_course_settings($course->id);
            if($course_setting->sell_course == 1 && $USER->id == $userid){ 
                 
            ?>
                    
                        
            <tr>
                <td class="maincol"><div class="title"><a href="sale_course_add.php?id=<?php echo $course->id ?>&userid=<?php echo $userid ?>"><?php echo $course->fullname ?></a></div>
                  <div class="desc-tip"><?php echo $course_setting->sale_description ?></div></td>  
              <td class="nowrap" align="right">
                <a href="sale_course_add.php?id=<?php echo $course->id ?>&userid=<?php echo $userid ?>" class="box-tag box-tag-green " title=""><span><?php echo $course_setting->currency." ".$course_setting->fee ?></span></a>
                </td>
            </tr>
                        <?php 
             }
          }
                         ?>
                      
    </table>
    <div class="powered-by"><a href="#">Learning  Management System</a> by <?php echo $USER->company ?></div>
                    
</div>
<div class="grid_5">

    <div class="profile-box">

            

            <div class="addthis_toolbox addthis_default_style">
                <a class="addthis_button_tweet"></a><span class="addthis_separator"></span><a class="addthis_button_facebook_like">
                </a>
            </div>  
            <div class="widget-link">
<!--                <a href="/courses/rss.ashx"><img border="0" alt="RSS" src='' /></a>            -->
            </div>
    </div>
 
</div>
</div>
    </div>
  