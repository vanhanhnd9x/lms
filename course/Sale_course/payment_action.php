<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!file_exists('../../config.php')) {
    header('Location: ../install.php');
    die;
}

require('../../config.php');
//require('lib.php');
global $CFG;
require_once('../../user/lib.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
//if ($PAGE->user_allowed_editing()) {
//    $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
//    $PAGE->set_button($buttons);
//}

// Check for valid admin user - no guest autologin

require_login(0, false);
$courses = enrol_get_my_courses_second();

$card_number = optional_param('Transaction_CardNumber', '', PARAM_TEXT);
$cardtype = optional_param('CardType', '', PARAM_TEXT);
$card_name = optional_param('Transaction_CardHolderName', '',PARAM_TEXT);
$firstname = optional_param('firstname', '', PARAM_TEXT);
$lastname = optional_param('lastname', '', PARAM_TEXT);
$email = optional_param('email', '', PARAM_TEXT);
$total = optional_param('total', '', PARAM_TEXT);
$userid = optional_param('userid','',PARAM_TEXT);
$month = optional_param('Transaction_CardExpiryMonth', '', PARAM_TEXT);
$year = optional_param('Transaction_CardExpiryYear', '', PARAM_TEXT);
$time = 0;
$time = $month.$year;

$id = check_email_exist($email);

$USE = get_user_from_id($id);
$cart = $_SESSION["CART"];
if($id){
    $receiver_id = $id;
    foreach (array_keys($cart) as $value){
    $courseid = $value;
    foreach($courses as $course){
    $course_setting = get_course_settings($courseid);
    $total = $course_setting->currency."  ".$course_setting->fee.".00";                          
    }
    insert_enrol_paypal($email, $receiver_id, $courseid, $userid, $total, $cardtype, $time);
    
    // xử lý send mail
    $name = $USE->username;
    $password = $USE->password;
    $invitationContenNoEmail = $emailContentNo;
            $invitationContenNoEmail = "Hello,<br>Successful transaction account.<br>";
            $invitationContenNoEmail .=  "To login and complete your training courses please click on the following link:$CFG->wwwroot, $invitationContenNoEmail<br>";
            $emailTitleNo = "Sell Course";
            $mailUsername = $name;
            send_email($email, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
            $invitationContenNoEmail = $emailContentNo;
     
   }
   $link = $CFG->wwwroot;
header("Location:$link");
}
 else {
             $count = 0;
        foreach ($cart as $key => $value) {
            $courseid = $key;
            $send_email_notification = 1;
            $count++;
            
            if($count==1){
            import_user_from_email_paypal($email, $courseid, $send_email_notification,$count);
            }
             else {
                  foreach (array_keys($cart) as $value){
                        $courseid = $value;
                        $id = check_email_exist($email);
                        $USE = get_user_from_id($id);
                        $receiver_id = $id;
                        foreach($courses as $course){
                            $course_setting = get_course_settings($courseid);
                            $total = $course_setting->currency."  ".$course_setting->fee.".00";                          
                            }
                        insert_enrol_paypal($email, $receiver_id, $courseid, $userid, $total, $cardtype, $time);
                        
                        //  send mail
                        $invitationContenNoEmail = $emailContentNo;
                        $invitationContenNoEmail = "Hello,<br>An account has been created for you on the training platform.<br>";
                        $invitationContenNoEmail .=  "To login and complete your training courses please click on the following link:$CFG->wwwroot, $invitationContenNoEmail<br>";
                        $emailTitleNo = "Sell Course";
                        $mailUsername = $name;
                        
                        send_email($email, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
                        $invitationContenNoEmail = $emailContentNo;
     
   }
       }
            
       }
        header("Location:https://accounts.google.com");
}


function insert_enrol_paypal($email, $receiver_id, $courseid, $userid, $total, $cardtype, $time,$firstname,$lastname,$card_number,$card_name){
     global $DB, $CFG, $USER;
    include($CFG->dirroot . '/teams/config.php');
        echo "tung";
        $paypal = new stdClass();
        $paypal->id = -1;
        $paypal->receiver_email = $email;
        $paypal->receiver_id = $receiver_id;
        $paypal->courseid = $courseid;
        $paypal->userid = $userid;
        $paypal->memo = $total;               
        $paypal->payment_type = $cardtype;
        $paypal->timeupdated = $time;
         $DB->insert_record('enrol_paypal', $paypal);
        
}

function send_email($to, $from, $from_name, $subject, $body) {
    global $CFG;

    include($CFG->dirroot . '/teams/config.php');
    global $error;
    $mail = new PHPMailer(); // create a new object
    $mail->IsSMTP(); // enable SMTP
    //$mail->IsHTML=true;
    $mail->IsHTML(true);
    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 465;
    $mail->Username = $mailUsername;
    $mail->Password = $pass;
    $mail->SetFrom($from, $from_name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AddAddress($to);
    $mail->CharSet = "utf-8";

    if (!$mail->Send()) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return false;
    } else {
        $error = 'Message sent!';
        return true;
    }
}

function import_user_from_email_paypal($email, $course_id, $send_email_notification,$count) {
    global $DB, $CFG, $USER;
    include($CFG->dirroot . '/teams/config.php');
        $name = email_name($email);
        $user = new stdClass();
        $user->id = -1;
        $user->auth = 'manual';
        $user->confirmed = 1;
        $user->deleted = 0;
        $user->timecreated = time();
        $password = randPass(8);
        $user->password = hash_internal_user_password($password);
        $user->username = $name;
        $user->mnethostid = 1;
        $user->firstname = $name;
        $user->parent = $USER->id;
        $user->email = $email;
        
        $lastinsertid = $DB->insert_record('user', $user);
        
        require_once($CFG->dirroot . '/lib/enrollib.php');
        $enrol = enrol_get_plugin('manual');
        $instances = $DB->get_records('enrol', array('enrol' => 'manual', 'courseid' => $course_id, 'status' => ENROL_INSTANCE_ENABLED), 'sortorder,id ASC');
        $instance = reset($instances);
        $enrol->enrol_user($instance, $lastinsertid, 5, '', '');
        


        if ($send_email_notification == 1) {

            $mailto = $email;
            $invitationContenNoEmail = $emailContentNo;
            $invitationContenNoEmail = str_replace("{USER_NAME}", $name, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{PASSWORD}", $password, $invitationContenNoEmail);
            $invitationContenNoEmail = str_replace("{LINK_TO_WEB}", $CFG->wwwroot, $invitationContenNoEmail);
            send_email($mailto, $mailUsername, 'LMS', $emailTitleNo, $invitationContenNoEmail);
                        
            $invitationContenNoEmail = $emailContentNo;
       }
    
}

function email_name($email_address, $split = '@') {
    return ucwords(strtolower(substr($email_address, 0, strripos($email_address, $split))));
}
?>
