<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!file_exists('../../config.php')) {
  header('Location: ../install.php');
  die;
}

require('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/lib/pagelib.php');
require_once($CFG->dirroot . '/common/lib.php');


// We are currently keeping the button here from 1.x to help new teachers figure out
// what to do, even though the link also appears in the course admin block.  It also
// means you can back out of a situation where you removed the admin block. :)
if ($PAGE->user_allowed_editing()) {
  $buttons = $OUTPUT->edit_button(new moodle_url('/manage/courses.php'));
  $PAGE->set_button($buttons);
}

// Check for valid admin user - no guest autologin

require_login(0, false);
$PAGE->set_title("Course");
$PAGE->set_heading("Course");
// Require Login.
require_login();
//global $CFG;
//require_once($CFG->dirroot . '/common/lib.php');
$enrol_paypal_id = optional_param('enrol_paypal', 0, PARAM_INT);
$course_status = 1;
update_enrol_paypal($enrol_paypal_id, $course_status);
header("Location:sale_course_admin.php");
  //$rows = get_enrol_paypal($enrol_paypal_id);

?>
