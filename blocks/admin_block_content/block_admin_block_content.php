<?php

class block_admin_block_content extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_admin_block_content');
    }

    function specialization() {
        if($this->page->pagetype == PAGE_COURSE_VIEW && $this->page->course->id != SITEID) {
            $this->title = get_string('AdminBlockContent', 'block_admin_block_content');
        }
    }

    public function instance_allow_multiple() {
        return true;
    }
    function get_content() {
        global $CFG, $OUTPUT ,$DB, $PAGE;;
         
        require_once($CFG->libdir . '/filelib.php');
   
        
        if($this->content !== NULL) {
            return $this->content;
        }

        if (empty($this->instance)) {
            return '';
        }
        // Add JS
        $PAGE->requires->js('/blocks/admin_block_content/admin_block_content.js');
        
        
        $this->content = new stdClass();
        $options = new stdClass();
        $options->noclean = true;    // Don't clean Javascripts etc
        $options->overflowdiv = true;
        $context = get_context_instance(CONTEXT_COURSE, $this->page->course->id);
        $idcourse = $this->page->course->id;
        
        $this->content->text = '<div class="action-buttons">
                <a href="../group/group.php?courseid=1" class="big-button drop"><span class="left"><span class="mid"><span class="right"><span class="icon_add">'.  get_string('addanewteam').'</span></span></span></span></a>
            </div>';
            
        
        $this->content->footer = '';

        return $this->content;
    }

    function hide_header() {
        return FALSE;
    }

    function preferred_width() {
        return 210;
    }

}


