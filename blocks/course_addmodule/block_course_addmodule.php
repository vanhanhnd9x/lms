<?php

class block_course_addmodule extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_course_addmodule');
    }

    function specialization() {
        if($this->page->pagetype == PAGE_COURSE_VIEW && $this->page->course->id != SITEID) {
            $this->title = get_string('courseaddnodule', 'block_course_addmodule');
        }
    }
//
//    function instance_allow_config() {
//        return true;
//    }

    public function instance_allow_multiple() {
        return true;
    }
    function get_content() {
        global $CFG, $OUTPUT ,$DB, $PAGE;;
         
        require_once($CFG->libdir . '/filelib.php');
        
        if($this->content !== NULL) {
            return $this->content;
        }

        if (empty($this->instance)) {
            return '';
        }
        // Add JS
        $PAGE->requires->js('/blocks/course_addmodule/addmodules.js');
        
        
        $this->content = new stdClass();
        $options = new stdClass();
        $options->noclean = true;    // Don't clean Javascripts etc
        $options->overflowdiv = true;
        $context = get_context_instance(CONTEXT_COURSE, $this->page->course->id);
        $idcourse = $this->page->course->id;
        
        $this->content->text = '<div class="admin-fullscreen-content">
    <div class="side-panel">
        <div class="action-buttons">
                    <a alt="Add a module" onclick="" href="#" class="big-button drop" id="addModule"><span class="left"><span class="mid"><span class="right"><span class="icon_add">'. get_string('addmodule').'</span></span></span></span></a>
                            
                            <ul id="drop-menu-items" class="hidden" style="display: none; top: 115px; left: 10px;">
                                <li>
                                <a href="'.$CFG->wwwroot.'/course/modedit.php?add=resource&type=video&course='.$idcourse.'&section=0&return=0"><b>Video</b></a>
                                </li>
                                <li>
                                    <a href="'.$CFG->wwwroot.'/course/modedit.php?add=scorm&type=&course='.$idcourse.'&section=0&return=0" ><b>SCORM</b></a>
                                </li>
                                <li>
                                 <a href="'.$CFG->wwwroot.'/course/modedit.php?add=resource&type=powerpoint&course='.$idcourse.'&section=0&return=0" >
                                     <b>PowerPoint</b> or <b>Keynote</b> presentation</a>
                                 </li> 
                                <li>
                                    <a href="'.$CFG->wwwroot.'/course/modedit.php?add=resource&type=flashmovie&course='.$idcourse.'&section=0&return=0" ><b>Flash Movie</b> (.swf file)</b></a>
                                 </li>                   
                                <li>
                                 <a href="'.$CFG->wwwroot.'/course/modedit.php?add=resource&type=audio&course='.$idcourse.'&section=0&return=0" ><b>Audio</b></a>
                                </li>
                                <li><hr></li>
                                <li>
                                <a href="'.$CFG->wwwroot.'/course/modedit.php?add=quiz&type=&course='.$idcourse.'&section=0&return=0" /><b>Assessment</b></a>
                                </li>
                                
                                <li>
                                <a href="'.$CFG->wwwroot.'/course/modedit.php?add=survey&type=&course='.$idcourse.'&section=0&return=0" />Survey</a>
                                </li>
                                <li>
                                <a href="'.$CFG->wwwroot.'/course/modedit.php?add=page&type=&course='.$idcourse.'&section=0&return=0" /><b>Page</b> of information</a>
                                </li>
                                <li><hr></li>
                                <li><a href="" class=""><b>Duplicate</b> or <b>Link to</b> a module in another course</a></li>
                                <li>
                                <a href="" class=""><b>Embed</b> content from another website</a> 
                                </li>
                                <li>
                                <a href="'.$CFG->wwwroot.'/course/modedit.php?add=url&type=&course='.$idcourse.'&section=0&return=0" /><b>Link</b> to another website</a>
                                </li>                                
                            </ul>
        </div>
    
    
        <h3>'.  get_string('coursesummary').'</h3>
        <div class="padded">       
        '.  get_string('themodules').'
        </div>
        <div id="courseSummary">
<ul class="">
<li><b>0</b> '.get_string('peopleareass').'</li>
<li><b>0</b> '.get_string('peoplehave').'</li>

</ul>

</div>                
        
                            <span title="This course is active" class="box-tag box-tag-green "><span>'.  get_string('active').'</span></span> 
                                         
    </div>



                    </div>';
            


        
        $this->content->footer = '';

        return $this->content;
    }

    function hide_header() {
        return FALSE;
    }

    function preferred_width() {
        return 210;
    }

}


