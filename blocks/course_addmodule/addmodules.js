/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function init(){
    if(document.getElementById('addModule')){
        document.getElementById('addModule').onclick = function(){
      toggle_visibility('drop-menu-items');
    };
    }
    
}

function toggle_visibility(id) {
   var e = document.getElementById(id);
   if(e.style.display == 'block')
      e.style.display = 'none';
   else
      e.style.display = 'block';
}

/* for other browsers */

window.onload = init;