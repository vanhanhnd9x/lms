<?php


/**
 * Returns javascript for use in tags block and supporting pages
 * @param string $coursetagdivs comma separated divs ids
 * @uses $CFG
 */
function addmodulelib_get_jscript($coursetagdivs = '') {
    global $CFG, $DB, $PAGE;

    $PAGE->requires->js('/blocks/course_addmodule/addmodules.js');

    return '';
}

?>
