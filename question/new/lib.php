<?php 
require_once('../../config.php');
global $USER, $CFG, $DB;
function add_new_cat_question_by_cua($contextid,$name,$info){
	global $DB;
	$cat = new stdClass();
	$cat->parent = 0;
	$cat->contextid = $contextid;
	$cat->name = trim($name);
	$cat->info = trim($info);
	$cat->sortorder = 999;
	$cat->stamp = make_unique_id_code();
	$categoryid = $DB->insert_record("question_categories", $cat);
	return $categoryid;
}
function update_cat_question_by_cua($idedit,$name,$info){
	global $DB;
	$record = new stdClass();
	$record->id = $idedit;
	$record->name =$name;
	$record->info =$info;
	return $DB->update_record('question_categories', $record, false);
}
function get_all_cat_question($page,$number,$name=null){
	global $DB, $USER;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}

	// $sql = "SELECT  * FROM question_categories  WHERE `question_categories`.`contextid`= 2";
	$sql = "SELECT  * FROM question_categories  ";
	$sql_total="SELECT COUNT( `question_categories`.`id`) FROM question_categories ";
	// $sql_total="SELECT COUNT( `question_categories`.`id`) FROM question_categories  WHERE `question_categories`.`contextid`= 2";
	if(!empty($name)){
		$sql.=" WHERE (`question_categories`.`name` LIKE '%{$name}%' OR `question_categories`.`info` LIKE '%{$name}%')";
		$sql_total.=" WHERE (`question_categories`.`name` LIKE '%{$name}%' OR `question_categories`.`info` LIKE '%{$name}%')";
	}
	$sql.="  ORDER BY id DESC LIMIT {$start} , {$number}";
	$data=$DB->get_records_sql($sql);
	$total_row=$DB->get_records_sql($sql_total);
	foreach ($total_row as $key => $value) {
    # code...
		$total_page=ceil((int)$key / (int)$number);
		break;
	}
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page 
	);
	return $kq;
}
// funtion lay truong question trong quiz
function get_quetion_in_table_quiz_by_cua_2($idquiz){
	global $DB;
	if (!empty($idquiz)) {
		$sql = "SELECT `quiz`.`questions`
		FROM `quiz` 
		WHERE `quiz`.`id`=:idquiz";
		$params = array('idquiz' => $idquiz);
		$data = $DB->get_field_sql($sql, $params,$strictness=IGNORE_MISSING);
		return $data;
	}
}
function delete_question_from_quiz_by_cua_in_question($idquiz,$value){
	global $DB;
	$allquetion=get_quetion_in_table_quiz_by_cua_2($idquiz);
	// chuyen no ve mang
	$s1=explode(',', $allquetion);
	//khai bao mang thu 2
	$s2=array();
	foreach ($s1 as $key1 => $value1) {
		# code...
		if($value!=$value1){
			array_push($s2, $value1);
		}
	}
	$s3=implode(',',$s2);
	$cua = new stdClass();
	$cua->id = $idquiz;
	$cua->questions = $s3;
	return $DB->update_record('quiz', $cua, false);
}
function get_all_id_quiz_quetion_instances($idquestion){
	global $DB;
	if (!empty($idquestion)) {
		$sql = "SELECT `quiz_question_instances`.`quiz` FROM `quiz_question_instances` WHERE `quiz_question_instances`.`question`={$idquestion}";
		$data = $DB->get_records_sql($sql);
		return $data;
	}
}
function delete_question_by_cua($id){
	if($id){
		global $DB;
		$allquiz=get_all_id_quiz_quetion_instances($id);
		if($allquiz){
			foreach ($allquiz as $key => $value) {
				delete_question_from_quiz_by_cua_in_question($value->quiz,$id);
			}
		}
		$DB->delete_records_select('question', " id = $id ");
		$DB->delete_records_select('quiz_question_instances', " question = $id ");
		$DB->delete_records_select('question_answers', " question = $id ");
		return 1;
	}
}
function get_all_id_question_in_cat($idcat){
	global $DB;
	$sql="SELECT `question`.`id` FROM `question` WHERE `question`.`category`={$idcat}";
	$data = $DB->get_records_sql($sql);
	return $data;
}
function delete_category_question_by_cua($iddelete){
	if($iddelete){
		global $DB;
		$DB->delete_records_select('question_categories', " id = $iddelete ");

		// xoa cau hoi thuoc cate
		$allques=get_all_id_question_in_cat($iddelete);
		if(!empty($allques)){
			foreach ($allques as $key => $value) {
				delete_question_by_cua($value->id);
			}
		}
		// $DB->delete_records_select('question', " category = $iddelete ");
		return 1;
	}
}

?>