<?php 
require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/question/category_class.php');
require_once($CFG->dirroot . '/question/new/lib.php');

$idedit = optional_param('idedit', '', PARAM_TEXT);
if(!empty($idedit)){
	$PAGE->set_title(get_string('editquestion_categories'));
	$name1='editquestion_categories';
	global $DB;
	$dataedit= $DB->get_record('question_categories',array('id'=>$idedit));
}else{
	$PAGE->set_title(get_string('addquestion_categories'));
	$name1='addquestion_categories';
}
require_login(0, false);
echo $OUTPUT->header();
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='cat-question';

$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$action = optional_param('action', '', PARAM_TEXT);
$parent = optional_param('parent', '', PARAM_TEXT);
$contextid = optional_param('contextid', '', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);
$info = optional_param('info', '', PARAM_TEXT);

if($action=='add_category'){
	add_new_cat_question_by_cua($contextid,$name,$info);
	echo displayJsAlert('', $CFG->wwwroot . "/question/new/listcategory.php");
}elseif($action=='edit_category'){
	update_cat_question_by_cua($idedit,$name,$info);
	echo displayJsAlert(get_string('success'), $CFG->wwwroot . "/question/new/listcategory.php");
}
if(!empty($idedit)){
	global $DB;
	$dataedit= $DB->get_record('question_categories',array('id'=>$idedit));
}
?>
<script src="<?php print new moodle_url('/manage/block-student/ckeditor/ckeditor.js'); ?>"></script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="card-content">
				<form id="profile" action="" method="post">
					<?php 
					if(!empty($idedit)){
						echo '<input type="text" name="action" value="edit_category" hidden="">';
					}else{
						echo'<input type="text" name="action" value="add_category" hidden="">';
					}
					?>
					
					<div class="row">
						<!-- <input type="text" hidden="" name="parent" value="1"> -->
						<input type="text" hidden="" name="contextid" value="2">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('question_categories'); ?> <span style="color:red">*</span></label>
								<input type="text" class="form-control" name="name" required="" value="<?php echo  $dataedit->name;?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label"><?php echo get_string('info'); ?></label>
								<br>
								<textarea name="info" id="note" cols="30" rows="10" class="form-control" value="<?php echo $dataedit->info; ?>"><?php echo $dataedit->info; ?>
							</textarea>
							<script>CKEDITOR.replace('note');</script>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-buttons" style="border-top: none !important">
							<input type="submit" id="submitBtn" class="btn btn-success" value=" <?php print_r(get_string('save', 'admin')) ?> ">
							<?php print_r(get_string('or')) ?>
							<a href="<?php echo $CFG->wwwroot ?>/manage/block-student/index.php" class="btn btn-danger"><?php print_r(get_string('cancel')) ?></a>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>
</div>
<?php
echo $OUTPUT->footer();


?>
