<?php 
// $Id: view.php,v 1.4 2006/08/28 16:41:20 mark-nielsen Exp $
/**
 * noticeboard
 *
 * @author : phamvanlong
 * @version $Id: version.php,v 1.0 2007/07/01 16:41:20
 * @package news
 * */

// Displays different views of the logs.

require_once('../../config.php');
require_once($CFG->dirroot . '/manage/lib.php');
require_once($CFG->dirroot . '/common/lib.php');
require_once($CFG->dirroot . '/question/category_class.php');
require_once($CFG->dirroot . '/question/new/lib.php');
require_login(0, false);

$PAGE->set_title(get_string('question_categories'));
$PAGE->set_heading(get_string('question_categories'));
$PAGE->set_pagelayout(get_string('question_categories'));
echo $OUTPUT->header();

$name =trim( optional_param('name', '', PARAM_TEXT));

$action = optional_param('action', '', PARAM_TEXT);

if($action=='search'){
	$url = $CFG->wwwroot.'/question/new/listcategory.php?search=timkiem&name='.$name .'&page=';
}else{
	$url = $CFG->wwwroot.'/question/new/listcategory.php?page=';
}
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
$data=get_all_cat_question($page,$number,$name);
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='cat-question';
$name1='question_categories';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
    echo displayJsAlert(get_string('notaccess'), $CFG->wwwroot . "/manage/");
}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkthemmoi=check_chuc_nang_them_moi($roleid,$moodle);
?>
<script>
	
</script>
<div class="row">
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="material-datatables">
					<div class="row mr_bottom15">
						<?php 
							if(!empty($checkthemmoi)){
								?>
								<div class="col-md-2">
									<a href="<?php echo new moodle_url('/question/new/addnew.php'); ?>" class="btn btn-info"><?php echo get_string('new'); ?></a>
								</div>
								<?php 
							}
						?>
						
						<div class="col-md-10">
							<form action="" method="get" accept-charset="utf-8">
								<input type="text" name="action" value="search" hidden> 
								<div class="row">
									<div class="col-4">
							            <label for=""><?php echo get_string('question_categories'); ?></label>
							            <input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>" placeholder="<?php echo get_string('question_categories'); ?>">
							         </div>
							         <div class="col-4">
								        <div style="    margin-top: 29px;"></div>
								        <button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
								     </div>
									<!-- <div class="col-md-4">
										<div class="form-group label-floating">
											<label class="control-label"><?php echo get_string('question_categories'); ?></label>
											<div id="erCompany" class="alert alert-danger" style="display: none;">
												<?php print_string('pleasecompany') ?></div>
												<input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>">
											</div>
										</div> -->
										<!-- <div class="col-md-4">
											<div class="form-buttons">
												<button class="btn btn-info"><?php echo get_string('search'); ?></button> 
											</div>
										</div> -->
									</div>
								</form>
							</div>
						</div>

						<div class="table-responsive" data-pattern="priority-columns">
							<table id="tech-companies-1" class="table table-hover">
								<thead>
									<tr>
										<th data-priority="1">STT</th>
										<th data-priority="1"><?php echo get_string('question_categories'); ?></th>
										<th data-priority="1"><?php echo get_string('info'); ?></th>
										<?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-right">'.get_string('action').'</th>';} ?>
									</tr>
								</thead>

								<tbody id="ketquatimkiem">

									<?php 
									# code...
									if (!empty($data['data'])) {
										# code...
										$n=$page;
										$i=($n-1)*20; 
										foreach ($data['data'] as $key => $value) {
											# code...
											$i++;
											?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $value->name; ?></td>
												<td><?php echo $value->info; ?></td>
												<!-- <td class="text-right">
													<a href="<?php echo $CFG->wwwroot ?>//question/new/addnew.php?idedit=<?php echo $value->id ?>" class="btn btn-info "><i class="fa fa-pencil" aria-hidden="true"></i></a>
													<a href="javascript:void(0)" onclick="delete_cat_question(<?php echo $value->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												</td> -->
												<?php 
													if(!empty($hanhdong)||!empty($checkdelete)){
														echo'<td class="text-right">';
														$hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$value->id);
														if(!empty($checkdelete)){
															?>
															<a href="javascript:void(0)" onclick="delete_cat_question(<?php echo $value->id;?>);" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
															<?php 
														}
														echo'</td>';
													}
												?>
											</tr>
											<?php 
										}
									}else{
										echo'
										<tr>
										<td colspan="8" style="text-align: center;">Dữ liệu trống</td>
										</tr>
										';
									}
									?>

								</tbody>
							</table>
						</div>
						<?php 
						if ($data['total_page']) {
							if ($page > 2) { 
								$startPage = $page - 2; 
							} else { 
								$startPage = 1; 
							} 
							if ($data['total_page'] > $page + 2) { 
								$endPage = $page + 2; 
							} else { 
								$endPage =$data['total_page']; 
							}
						}
						?>
						<p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
						<nav aria-label="Page navigation example">
							<ul class="pagination">
								<li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
									<a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
										<span class="sr-only">Previous</span>
									</a>
								</li>
								<?php 
								for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
									?>
									<li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
									<?php 
								}
								?>
								<li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
									<a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
										<span class="sr-only">Next</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>
					<!-- end content-->
				</div>
				<!--  end card  -->
			</div>
			<!-- end col-md-12 -->
		</div>
	</div>
	<?php
	echo $OUTPUT->footer();
	?>
	<script>
		function delete_cat_question(iddelete){
			var urlweb= "<?php echo $CFG->wwwroot ?>/question/new/listcategory.php";
			var urldelte= "<?php echo $CFG->wwwroot ?>/question/new/load_ajax.php";
			var e=confirm('<?php echo get_string('del_question_categories'); ?>');
			if(e==true){
				$.ajax({
					url:  urldelte,
					type: "post",
					data: {iddelete:iddelete} ,
					success: function (response) {
						window.location=urlweb;
					}
				});
			}
		}
	</script>