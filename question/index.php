<?php
// require_once('../config.php');
require_once(dirname(__FILE__) . '/../config.php');
require_once(dirname(__FILE__) . '/editlib.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/formslib.php'); 
require_once($CFG->libdir . '/questionlib.php');
require_login(0, false);
$PAGE->set_title(get_string('question'));
echo $OUTPUT->header();

$action = optional_param('action', '', PARAM_TEXT);
$category = optional_param('category', '', PARAM_TEXT);
$name = optional_param('name', '', PARAM_TEXT);
$page=isset($_GET['page'])?$_GET['page']:1;
$number=20;
function get_all_question_by_cua($page,$number,$category=null,$name=null){
	global $DB;
	if ($page>1) {
		$start=(($page-1)*$number);
	}else{
		$start=0;
	}
	$sql="SELECT `question`.`id`, `question`.`name`,`question`.`category` FROM question WHERE `question`.`parent` =0  ";
	$sql_total="SELECT COUNT( `question`.`id`) FROM question  WHERE `question`.`parent` =0 ";
	if(!empty($category)){
		$sql.="AND `question`.`category` ={$category}";
		$sql_total.="AND `question`.`category` ={$category}";
	}
	if(!empty($name)){
		$sql.=" AND `question`.`name` LIKE'%{$name}%'";
		$sql_total.=" AND `question`.`name` LIKE'%{$name}%'";
	}
	
	$sql.=" ORDER BY id DESC LIMIT {$start}, {$number}";
	$data= $DB->get_records_sql($sql); 
	$total_row=$DB->get_records_sql($sql_total);
	foreach ($total_row as $key => $value) {
    # code...
		$total_page=ceil((int)$key / (int)$number);
		break;
	}
	$kq=array(
		'data'=>$data,
		'total_page'=>$total_page,
	);
	return $kq;
}
function show_all_category_in_question_by_cua(){
	global $DB;
	$sql="SELECT id, name FROM question_categories ";
	$data= $DB->get_records_sql($sql); 
	return $data;
}
if($action=='search'){
	$data=get_all_question_by_cua($page,$number,$category,$name);
	$url=$CFG->wwwroot.'/question/index.php?action=search&category='.$category.'&name='.$name.'&page=';
}else{
	$data=get_all_question_by_cua($page,$number,$category=null,$name=null);
	$url=$CFG->wwwroot.'/question/index.php?page=';
}
function show_only_name_cat_question_by_cua($id){
	global $CFG, $PAGE, $OUTPUT,$DB;
	$sql = "SELECT `question_categories`.`name` FROM `question_categories` WHERE `question_categories`.`id` = {$id}";
	$data = $DB->get_field_sql($sql,null,$strictness=IGNORE_MISSING);
	return $data;
}
$roleid=lay_role_id_cua_user_dang_nhap($USER->id);
$moodle='question';
$name1='question';
$check_in=check_nguoi_dung_duoc_phep_vao_chuc_nang_theoname_modlue($roleid,$moodle,$name1);
if(empty($check_in)){
   ?>
   <script type="text/javascript">
   	var url="<?php echo $CFG->wwwroot.'/manage/'  ?>";
         window.location=url;
    </script>
   <?php 

}
$hanhdong=lay_ds_hanh_dong_theo_role_by_cua($roleid,$moodle);
$checkdelete=check_chuc_nang_xoa($roleid,$moodle);
$checkpopup=check_chuc_nang_pop_up($roleid,$moodle)
?>


<div class="row"> 
	<div class="col-md-12">
		<div class="card-box">
			<div class="table-rep-plugin">
				<div class="row mr_bottom15">
					<?php 
						if(!empty($checkpopup)){
							?>
							<div class="col-md-2">
								<a href="#myModal" class="btn btn-info" data-toggle="modal" >Thêm mới</a> 
							</div>
							<?php 
						}
					 ?>
					
					<div class="col-md-10">
						<form action="" method="get" accept-charset="utf-8" class="form-row">
							<input type="text" hidden="" name="action" value="search">
							<div class="col-4">
								<label for=""><?php echo get_string('question_categories') ?></label>
								<select name="category" id="" class="form-control">
									<option value=""><?php echo get_string('question_categories') ?></option>
									<?php 
									if (function_exists('show_all_category_in_question_by_cua')) {
										$cat=show_all_category_in_question_by_cua();
										foreach ($cat as $key => $c) {
												# code...
											?>
											<option value="<?php echo $c->id; ?>" <?php if(!empty($category)&&($category==$c->id)) echo'selected'; ?>><?php echo $c->name; ?></option>
											<?php 
										}
									}
									?>
								</select>
							</div>
							<div class="col-4">
								<label for=""><?php echo get_string('name_ques'); ?></label>
								<input type="" name="name" class="form-control" placeholder="Tên câu hỏi" value="<?php echo trim($name); ?>">
							</div>
							<div class="col-4">
								<div style="    margin-top: 29px;"></div>
								<button type="submit" class="btn btn-info"><?php echo get_string('search'); ?></button>
							</div>
						</form>
					</div>
				</div>

				<div class="table-responsive" data-pattern="priority-columns" id="searchResults">
					<form action="<?php echo $CFG->wwwroot ?>/manage/schools/index.php?action=del_list" method="post">
						<table id="tech-companies-1" class="table table-hover">
							<thead>
								<tr>
									<th>STT</th>
									<th data-priority="6"><?php echo get_string('name_ques'); ?></th>
									<th data-priority="6"><?php echo get_string('question_categories') ?></th>
									<?php if(!empty($hanhdong)||!empty($checkdelete)){ echo'<th class="disabled-sorting text-right">'.get_string('action').'</th>';} ?>
								</tr>
							</thead>
							<tbody id="ajaxschools">
								<?php 
								$n=$page;
								$i=($n-1)*20;
								if($data['data']){
									foreach ($data['data'] as $question) { 
										$i++;
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $question->name; ?></td>
											<td><?php echo show_only_name_cat_question_by_cua($question->category); ?></td>
											<?php 
													if(!empty($hanhdong)||!empty($checkdelete)){
														echo'<td class="text-right">';
														// $hienthi=hien_thi_action_theo_tung_moodle_by_cua($moodle,$roleid,$question->id);
														if ($hanhdong) {
															?>
															<a href="<?php echo $CFG->wwwroot ?>/question/question.php?courseid=1&category=<?php echo $question->category; ?>&id=<?php echo $question->id; ?>&returnurl=/question/index.php" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
															<?php 
														}
														if(!empty($checkdelete)){
															?>
															<a href="javascript:void(0)" onclick="delet_question(<?php echo $question->id;?>);" class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
															<?php 
														}
														echo'</td>';
													}
												?>
											<!-- <td class="text-right">
												<a href="<?php echo $CFG->wwwroot ?>/question/question.php?courseid=1&category=1&id=<?php echo $question->id; ?>&returnurl=/question/index.php" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<a href="javascript:void(0)" onclick="delet_question(<?php echo $question->id;?>);" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</td> -->
										</tr>
									<?php }
								}
								?>
							</tbody>
						</table>
					</form>
				</div>
				<?php 
				if ($data['total_page']) {
					if ($page > 2) { 
						$startPage = $page - 2; 
					} else { 
						$startPage = 1; 
					} 
					if ($data['total_page'] > $page + 2) { 
						$endPage = $page + 2; 
					} else { 
						$endPage =$data['total_page']; 
					}
				}
				?>
				<p><?php echo get_string('total_page'); ?>: <?php echo $data['total_page'] ?></p>
				<nav aria-label="Page navigation example">
					<ul class="pagination">
						<li class="page-item <?php if(isset($page)||($page==1)) echo 'disabled'; ?>">
							<a class="page-link" href="<?php echo $url,$page-1;?>" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
								<span class="sr-only">Previous</span>
							</a>
						</li>
						<?php 
						for ($i=$startPage; $i <=$endPage ; $i++) { 
                                        # code...
							?>
							<li class="page-item <?php if($page==$i) echo'active'; ?>"><a class="page-link" href="<?php echo $url,$i;?>"><?php echo $i; ?></a></li>
							<?php 
						}
						?>
						<li class="page-item <?php if(($page==$data['total_page'])) echo 'disabled'; ?>">
							<a class="page-link" href="<?php echo $url,$page+1;?>" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
								<span class="sr-only">Next</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	
</div>
<div class="container">
<!-- <a href="#myModal" class="btn btn-primary" data-toggle="modal">Launch demo modal</a>
-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
				<!-- <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Hãy lựa chọn loại câu hỏi</h4>
				</div> -->
				<div class="modal-body">
					<?php echo print_choose_qtype_to_add_form2() ?>.
				</div>
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button> -->
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->


	</div>
	<?php

	echo $OUTPUT->footer();
	?>
	<script>
		function delet_question(iddelete){
			var urlweb= "<?php echo $CFG->wwwroot ?>/question/index.php";
			var urldelte= "<?php echo $CFG->wwwroot ?>/question/new/load_ajax.php";
			var e=confirm('<?php echo get_string('delete_ques'); ?>');
			if(e==true){
				$.ajax({
					url:  urldelte,
					type: "post",
					data: {idquestion:iddelete} ,
					success: function (response) {
						window.location=urlweb;
					}
				});
			}
		}
	</script>
